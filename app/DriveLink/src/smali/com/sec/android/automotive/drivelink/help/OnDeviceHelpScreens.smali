.class public Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "OnDeviceHelpScreens.java"


# static fields
.field public static final ENABLING_CAR_MODE:I = 0x1

.field public static final REGISTERING_A_CAR:I = 0x0

.field public static final USING_VOICE_COMMAND:I = 0x2


# instance fields
.field private mBackLayout:Landroid/widget/LinearLayout;

.field private mBackTextView:Landroid/widget/TextView;

.field private final mDataSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDivider1:Landroid/view/View;

.field private mDivider2:Landroid/view/View;

.field private mDivider3:Landroid/view/View;

.field private mLayout1:Landroid/widget/LinearLayout;

.field private mLayout2:Landroid/widget/LinearLayout;

.field private mLayout3:Landroid/widget/LinearLayout;

.field private mLayout4:Landroid/widget/LinearLayout;

.field private mScreenToLaunch:I

.field private mTitleLayout1:Landroid/widget/LinearLayout;

.field private mTitleLayout2:Landroid/widget/LinearLayout;

.field private mTitleLayout3:Landroid/widget/LinearLayout;

.field private mTitleLayout4:Landroid/widget/LinearLayout;

.field private mTitleText1:Landroid/widget/TextView;

.field private mTitleText2:Landroid/widget/TextView;

.field private mTitleText3:Landroid/widget/TextView;

.field private mTitleText4:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mScreenToLaunch:I

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDataSet:Ljava/util/ArrayList;

    .line 33
    return-void
.end method

.method private attachDataToViews()V
    .locals 8

    .prologue
    const v7, 0x7f090234

    const/4 v6, 0x1

    const v5, 0x7f09023d

    const v4, 0x7f090231

    const v3, 0x7f09022e

    .line 272
    iget v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mScreenToLaunch:I

    packed-switch v0, :pswitch_data_0

    .line 386
    :goto_0
    return-void

    .line 274
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 277
    const v2, 0x7f0a04b9

    .line 276
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 277
    const/4 v2, 0x0

    .line 275
    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getText(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 280
    const v2, 0x7f0a04ba

    .line 279
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 278
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 283
    const v2, 0x7f0a04bb

    .line 282
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 281
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090237

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 286
    const v2, 0x7f0a04bc

    .line 285
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f09023a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 289
    const v2, 0x7f0a04bd

    .line 288
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 292
    const v2, 0x7f0a04be

    .line 291
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 290
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 295
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 298
    const v2, 0x7f0a04bf

    .line 297
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 298
    const/4 v2, 0x0

    .line 296
    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getText(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 301
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 302
    const v2, 0x7f0a04c0

    .line 301
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 300
    invoke-direct {p0, v1, v6}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getText(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 305
    const v2, 0x7f0a04c1

    .line 304
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 307
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 308
    const v2, 0x7f0a04c2

    .line 307
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 311
    const v2, 0x7f0a04c3

    .line 310
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 309
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 314
    const v2, 0x7f0a04c4

    .line 313
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 317
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 319
    const v2, 0x7f0a04c5

    .line 318
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 317
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 323
    const v2, 0x7f0a04c6

    .line 322
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-direct {p0, v1, v6}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getTextForImage(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 326
    const v2, 0x7f0a04c7

    .line 325
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 329
    const v2, 0x7f0a04c8

    .line 328
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090240

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 332
    const v2, 0x7f0a04c9

    .line 331
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 334
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 335
    const v2, 0x7f0a04ca

    .line 334
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 338
    const v2, 0x7f0a04cb

    .line 337
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 341
    const v2, 0x7f0a04cc

    .line 340
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f090240

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 344
    const v2, 0x7f0a04cd

    .line 343
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 348
    const v2, 0x7f0a04c5

    .line 347
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 346
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 350
    invoke-direct {p0, v1, v6}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getTextForImage(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 353
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    const v1, 0x7f090237

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    const v1, 0x7f09023a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 358
    const v2, 0x7f0a04d1

    .line 357
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 361
    const v2, 0x7f0a04d2

    .line 360
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    const v1, 0x7f090240

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 364
    const v2, 0x7f0a04d3

    .line 363
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 362
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 368
    const v2, 0x7f0a04c5

    .line 367
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 366
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 370
    invoke-direct {p0, v1, v6}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getTextForImage(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    const v1, 0x7f090237

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 377
    const v2, 0x7f0a04d6

    .line 376
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 380
    const v2, 0x7f0a04d7

    .line 379
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    const v1, 0x7f090240

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 383
    const v2, 0x7f0a04d8

    .line 382
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 381
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getImage(I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 389
    const/4 v0, 0x0

    .line 390
    .local v0, "image":Landroid/graphics/drawable/Drawable;
    iget v1, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mScreenToLaunch:I

    packed-switch v1, :pswitch_data_0

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 409
    const v2, 0x7f020134

    .line 408
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 412
    :goto_0
    return-object v0

    .line 392
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 393
    const v2, 0x7f020137

    .line 392
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 394
    goto :goto_0

    .line 396
    :pswitch_1
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 398
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 399
    const v2, 0x7f020135

    .line 398
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 400
    goto :goto_0

    .line 402
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 403
    const v2, 0x7f020136

    .line 402
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 390
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 396
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getText(Ljava/lang/String;I)Landroid/text/SpannableString;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    const v7, 0x7f0a04d9

    const/4 v6, 0x0

    .line 434
    const/4 v0, 0x0

    .line 435
    .local v0, "imageToAdd":Landroid/graphics/drawable/Drawable;
    move-object v3, p1

    .line 436
    .local v3, "textToChange":Ljava/lang/String;
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 437
    new-instance v2, Landroid/text/SpannableString;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 438
    .local v2, "ss":Landroid/text/SpannableString;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 439
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 438
    invoke-virtual {v0, v6, v6, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 440
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v4, 0x1

    invoke-direct {v1, v0, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 443
    .local v1, "span":Landroid/text/style/ImageSpan;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x8

    .line 447
    const/16 v6, 0x11

    .line 441
    invoke-virtual {v2, v1, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 448
    return-object v2
.end method

.method private getTextForImage(Ljava/lang/String;I)Landroid/text/SpannableString;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    const v7, 0x7f0a04d9

    const/4 v6, 0x0

    .line 416
    const/4 v0, 0x0

    .line 417
    .local v0, "imageToAdd":Landroid/graphics/drawable/Drawable;
    move-object v3, p1

    .line 418
    .local v3, "textToChange":Ljava/lang/String;
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 419
    new-instance v2, Landroid/text/SpannableString;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 420
    .local v2, "ss":Landroid/text/SpannableString;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 421
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 420
    invoke-virtual {v0, v6, v6, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 422
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v4, 0x1

    invoke-direct {v1, v0, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 425
    .local v1, "span":Landroid/text/style/ImageSpan;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x7

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    .line 429
    const/16 v6, 0x11

    .line 423
    invoke-virtual {v2, v1, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 430
    return-object v2
.end method

.method private setDividerLayoutViews()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 133
    iget v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mScreenToLaunch:I

    packed-switch v0, :pswitch_data_0

    .line 162
    :goto_0
    return-void

    .line 135
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider1:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider2:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider3:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 144
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider1:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider2:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider3:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 153
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider1:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider2:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setMainTextLayout()V
    .locals 7

    .prologue
    const v6, 0x7f090232

    const v5, 0x7f09022f

    const v4, 0x7f09022c

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 165
    iget v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mScreenToLaunch:I

    packed-switch v0, :pswitch_data_0

    .line 269
    :goto_0
    return-void

    .line 167
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090235

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090238

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f09023b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 178
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 182
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090235

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090238

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f09023b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 193
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f090235

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f090238

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f09023b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 207
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 211
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090235

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f090238

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f09023b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 222
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 224
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f090235

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f090238

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f09023b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 236
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 238
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    const v1, 0x7f090235

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    const v1, 0x7f090238

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    const v1, 0x7f09023b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 250
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 252
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    const v1, 0x7f090235

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    const v1, 0x7f090238

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    const v1, 0x7f09023b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 264
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    const v1, 0x7f09023e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 266
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 165
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setTitleLayout()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 104
    iget v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mScreenToLaunch:I

    packed-switch v0, :pswitch_data_0

    .line 130
    :goto_0
    return-void

    .line 106
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 112
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 120
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText3:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText4:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f030049

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->setContentView(I)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "SCREEN_TO_LAUNCH"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mScreenToLaunch:I

    .line 70
    const v0, 0x7f090072

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mBackLayout:Landroid/widget/LinearLayout;

    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mBackLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens$1;-><init>(Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    const v0, 0x7f0901b9

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mBackTextView:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mBackTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "Screen_Name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const v0, 0x7f0901ba

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout1:Landroid/widget/LinearLayout;

    .line 83
    const v0, 0x7f0901bb

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText1:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0901bc

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout1:Landroid/widget/LinearLayout;

    .line 85
    const v0, 0x7f0901bd

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider1:Landroid/view/View;

    .line 86
    const v0, 0x7f0901be

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout2:Landroid/widget/LinearLayout;

    .line 87
    const v0, 0x7f0901bf

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText2:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0901c0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout2:Landroid/widget/LinearLayout;

    .line 89
    const v0, 0x7f0901c1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider2:Landroid/view/View;

    .line 90
    const v0, 0x7f0901c2

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout3:Landroid/widget/LinearLayout;

    .line 91
    const v0, 0x7f0901c3

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText3:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0901c4

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout3:Landroid/widget/LinearLayout;

    .line 93
    const v0, 0x7f0901c5

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mDivider3:Landroid/view/View;

    .line 94
    const v0, 0x7f0901c6

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleLayout4:Landroid/widget/LinearLayout;

    .line 95
    const v0, 0x7f0901c7

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mTitleText4:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0901c8

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->mLayout4:Landroid/widget/LinearLayout;

    .line 97
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->setTitleLayout()V

    .line 98
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->setDividerLayoutViews()V

    .line 99
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->setMainTextLayout()V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/help/OnDeviceHelpScreens;->attachDataToViews()V

    .line 101
    return-void
.end method
