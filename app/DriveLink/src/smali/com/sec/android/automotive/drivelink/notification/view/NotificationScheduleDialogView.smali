.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationScheduleDialogView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static final ALERT_DURATION:I = 0x14

.field private static MASK_ALLDAY:I = 0x0

.field private static MASK_ATTENDEES:I = 0x0

.field private static MASK_DATE:I = 0x0

.field private static MASK_DAY:I = 0x0

.field private static MASK_LOCATION:I = 0x0

.field private static MASK_NO_TITLE:I = 0x0

.field private static MASK_TIME:I = 0x0

.field private static MASK_TITLE:I = 0x0

.field private static SCHEDULE_TITLE:I = 0x0

.field private static SCHEDULE_TITLE_ATTENDEES:I = 0x0

.field private static SCHEDULE_TITLE_LOCATION:I = 0x0

.field private static SCHEDULE_TITLE_LOCATION_ATTENDEES:I = 0x0

.field private static final TAG:Ljava/lang/String; = "[NotificationScheduleDialogView]"


# instance fields
.field bUse24HourFormat:Z

.field mButton1:Landroid/widget/Button;

.field mButton2:Landroid/widget/Button;

.field mContext:Landroid/content/Context;

.field private mLatitude:D

.field mLlNotiBase:Landroid/widget/LinearLayout;

.field private mLongitude:D

.field private mNavigateListener:Landroid/view/View$OnClickListener;

.field private mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

.field private mOKListener:Landroid/view/View$OnClickListener;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mTts:Ljava/lang/String;

.field mTvAmPM:Landroid/widget/TextView;

.field mTvTime:Landroid/widget/TextView;

.field mTvTitle:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 329
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    .line 330
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    .line 331
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ATTENDEES:I

    .line 332
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    .line 334
    const/16 v0, 0x10

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    .line 335
    const/16 v0, 0x20

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    .line 336
    const/16 v0, 0x40

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DAY:I

    .line 337
    const/16 v0, 0x80

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    .line 338
    sget v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->SCHEDULE_TITLE:I

    .line 339
    sget v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v0, v1

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->SCHEDULE_TITLE_LOCATION:I

    .line 340
    sget v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ATTENDEES:I

    or-int/2addr v0, v1

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->SCHEDULE_TITLE_ATTENDEES:I

    .line 341
    sget v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    .line 342
    sget v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    .line 341
    or-int/2addr v0, v1

    .line 342
    sget v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ATTENDEES:I

    .line 341
    or-int/2addr v0, v1

    sput v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->SCHEDULE_TITLE_LOCATION_ATTENDEES:I

    .line 342
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 68
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    .line 40
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D

    .line 41
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTts:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->bUse24HourFormat:Z

    .line 234
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigateListener:Landroid/view/View$OnClickListener;

    .line 306
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mOKListener:Landroid/view/View$OnClickListener;

    .line 748
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    .line 40
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D

    .line 41
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTts:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->bUse24HourFormat:Z

    .line 234
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigateListener:Landroid/view/View$OnClickListener;

    .line 306
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mOKListener:Landroid/view/View$OnClickListener;

    .line 748
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 63
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    .line 40
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D

    .line 41
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTts:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->bUse24HourFormat:Z

    .line 234
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigateListener:Landroid/view/View$OnClickListener;

    .line 306
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mOKListener:Landroid/view/View$OnClickListener;

    .line 748
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 57
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->initSubClassNotification(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 300
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;D)V
    .locals 0

    .prologue
    .line 40
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;D)V
    .locals 0

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)D
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D

    return-wide v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)D
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D

    return-wide v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTts:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTts:Ljava/lang/String;

    return-object v0
.end method

.method private getDateInfo(Ljava/util/Date;I)Ljava/lang/String;
    .locals 20
    .param p1, "scheduleDate"    # Ljava/util/Date;
    .param p2, "isAllDay"    # I

    .prologue
    .line 597
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 598
    .local v2, "DateString":Ljava/lang/String;
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 599
    .local v9, "today":Ljava/util/Date;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getTomorrowDate(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v13

    .line 601
    .local v13, "tomorrow":Ljava/util/Date;
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v17, "yyyy"

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 602
    .local v5, "Year":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v17, "MMM"

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 603
    .local v4, "Mon":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v17, "d"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 605
    .local v3, "Day":Ljava/text/SimpleDateFormat;
    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v12

    .line 606
    .local v12, "todayYear":Ljava/lang/String;
    invoke-virtual {v4, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    .line 607
    .local v11, "todayMon":Ljava/lang/String;
    invoke-virtual {v3, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    .line 609
    .local v10, "todayDay":Ljava/lang/String;
    invoke-virtual {v5, v13}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    .line 610
    .local v16, "tomorrowYear":Ljava/lang/String;
    invoke-virtual {v4, v13}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    .line 611
    .local v15, "tomorrowMon":Ljava/lang/String;
    invoke-virtual {v3, v13}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v14

    .line 613
    .local v14, "tomorrowDay":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 614
    .local v8, "scheduleYear":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 615
    .local v7, "scheduleMon":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 617
    .local v6, "scheduleDay":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 619
    .local v1, "Date":Ljava/lang/String;
    const-string/jumbo v17, "[NotificationScheduleDialogView]"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string/jumbo v19, "Today :"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 620
    const-string/jumbo v19, " scheduleYear : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 621
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 619
    invoke-static/range {v17 .. v18}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 623
    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 624
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0a00b3

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 634
    :goto_0
    const-string/jumbo v17, "[NotificationScheduleDialogView]"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string/jumbo v19, "DateString : "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    return-object v2

    .line 626
    :cond_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 627
    invoke-virtual {v15, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 628
    invoke-virtual {v14, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 629
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0a00b4

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 630
    goto :goto_0

    .line 631
    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method private getTomorrowDate(Ljava/util/Date;)Ljava/util/Date;
    .locals 3
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 640
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 641
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 642
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 643
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method

.method private hasGoogleAccount(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 301
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 302
    .local v1, "accMan":Landroid/accounts/AccountManager;
    const-string/jumbo v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 303
    .local v0, "accArray":[Landroid/accounts/Account;
    array-length v3, v0

    if-lt v3, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private makeScheduleTTSString()Ljava/lang/String;
    .locals 15

    .prologue
    .line 345
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "makeScheduleTTSString"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    .local v10, "tts":Ljava/lang/StringBuilder;
    const-string/jumbo v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    const/4 v7, 0x0

    .line 349
    .local v7, "schedule_content":I
    const-string/jumbo v9, ""

    .line 350
    .local v9, "title":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 351
    .local v5, "location":Ljava/lang/String;
    const-string/jumbo v8, ""

    .line 352
    .local v8, "time":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 354
    .local v4, "date":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .line 357
    .local v6, "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getStartDate()Ljava/util/Date;

    move-result-object v0

    .line 359
    .local v0, "Startdate":Ljava/util/Date;
    if-eqz v0, :cond_3

    .line 361
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v9

    .line 363
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_4

    .line 364
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_4

    .line 365
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 366
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 367
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "title :"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    or-int/2addr v7, v11

    .line 374
    :goto_0
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v5

    .line 376
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 377
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v11

    .line 378
    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    .line 382
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v7, v11

    .line 385
    :cond_0
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getAllDay()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_6

    .line 386
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v7, v11

    .line 387
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getAllDay()I

    move-result v11

    invoke-direct {p0, v0, v11}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getDateInfo(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v4

    .line 389
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 390
    const v12, 0x7f0a00b3

    .line 389
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 390
    if-nez v11, :cond_1

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 392
    const v12, 0x7f0a00b4

    .line 391
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 392
    if-eqz v11, :cond_5

    .line 393
    :cond_1
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DAY:I

    or-int/2addr v7, v11

    .line 437
    :cond_2
    :goto_1
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_a

    .line 438
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 1"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 441
    const v12, 0x7f0a01fd

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 442
    aput-object v8, v13, v14

    .line 440
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 439
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    .end local v0    # "Startdate":Ljava/util/Date;
    .end local v6    # "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_3
    :goto_2
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "TTS : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11

    .line 370
    .restart local v0    # "Startdate":Ljava/util/Date;
    .restart local v6    # "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_4
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "MASK_NO_TITLE "

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    or-int/2addr v7, v11

    goto/16 :goto_0

    .line 395
    :cond_5
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v7, v11

    .line 396
    goto :goto_1

    .line 397
    :cond_6
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getAllDay()I

    move-result v11

    invoke-direct {p0, v0, v11}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getDateInfo(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v4

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 400
    const v12, 0x7f0a00b3

    .line 399
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 400
    if-nez v11, :cond_7

    .line 404
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v7, v11

    .line 407
    :cond_7
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v7, v11

    .line 409
    iget-boolean v11, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->bUse24HourFormat:Z

    if-eqz v11, :cond_8

    .line 410
    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 411
    const-string/jumbo v11, "HH:mm "

    .line 410
    invoke-direct {v1, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 412
    .local v1, "TimeFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 413
    goto/16 :goto_1

    .line 414
    .end local v1    # "TimeFormat":Ljava/text/SimpleDateFormat;
    :cond_8
    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 415
    const-string/jumbo v11, "hh:mm "

    .line 414
    invoke-direct {v1, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 416
    .restart local v1    # "TimeFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 419
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 420
    .local v3, "cal":Ljava/util/Calendar;
    invoke-virtual {v3, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 421
    const/16 v11, 0x9

    invoke-virtual {v3, v11}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 423
    .local v2, "am_pm":I
    if-nez v2, :cond_9

    .line 424
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 425
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 426
    const v13, 0x7f0a00e0

    .line 425
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 424
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 427
    goto/16 :goto_1

    :cond_9
    const/4 v11, 0x1

    if-ne v2, v11, :cond_2

    .line 428
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 430
    const v13, 0x7f0a00e1

    .line 429
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 428
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 443
    .end local v1    # "TimeFormat":Ljava/text/SimpleDateFormat;
    .end local v2    # "am_pm":I
    .end local v3    # "cal":Ljava/util/Calendar;
    :cond_a
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DAY:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_b

    .line 444
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 2"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 446
    const v12, 0x7f0a01fe

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 447
    aput-object v4, v13, v14

    .line 445
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 448
    :cond_b
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_c

    .line 449
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 3"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 451
    const v12, 0x7f0a01ff

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 452
    aput-object v4, v13, v14

    .line 450
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 453
    :cond_c
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_d

    .line 454
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 4"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 456
    const v12, 0x7f0a0200

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v8, v13, v14

    const/4 v14, 0x1

    .line 457
    aput-object v9, v13, v14

    .line 455
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 458
    :cond_d
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_e

    .line 459
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 5"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 461
    const v12, 0x7f0a0201

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 462
    aput-object v8, v13, v14

    const/4 v14, 0x1

    aput-object v4, v13, v14

    const/4 v14, 0x2

    aput-object v9, v13, v14

    .line 460
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 463
    :cond_e
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v11, v12

    if-eq v7, v11, :cond_f

    .line 464
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DAY:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_10

    .line 465
    :cond_f
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 6"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 467
    const v12, 0x7f0a0202

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 468
    aput-object v4, v13, v14

    const/4 v14, 0x1

    aput-object v9, v13, v14

    .line 466
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 469
    :cond_10
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_11

    .line 470
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 7"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 472
    const v12, 0x7f0a0203

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 473
    aput-object v8, v13, v14

    const/4 v14, 0x1

    aput-object v5, v13, v14

    .line 471
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 474
    :cond_11
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v11, v12

    .line 475
    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_12

    .line 476
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 8"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 479
    const v12, 0x7f0a0204

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 480
    aput-object v8, v13, v14

    const/4 v14, 0x1

    aput-object v4, v13, v14

    const/4 v14, 0x2

    aput-object v5, v13, v14

    .line 478
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 477
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 481
    :cond_12
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    .line 482
    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DAY:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_13

    .line 483
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 9"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 485
    const v12, 0x7f0a0205

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 486
    aput-object v4, v13, v14

    const/4 v14, 0x1

    aput-object v5, v13, v14

    .line 484
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 487
    :cond_13
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_NO_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    .line 488
    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_14

    .line 489
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 10"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 491
    const v12, 0x7f0a0206

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 492
    aput-object v4, v13, v14

    const/4 v14, 0x1

    aput-object v5, v13, v14

    .line 490
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 493
    :cond_14
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_15

    .line 494
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 11"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 496
    const v12, 0x7f0a0207

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 497
    aput-object v8, v13, v14

    const/4 v14, 0x1

    aput-object v5, v13, v14

    const/4 v14, 0x2

    aput-object v9, v13, v14

    .line 495
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 498
    :cond_15
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TIME:I

    or-int/2addr v11, v12

    .line 499
    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_16

    .line 500
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 12"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 503
    const v12, 0x7f0a0208

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 504
    aput-object v8, v13, v14

    const/4 v14, 0x1

    aput-object v4, v13, v14

    const/4 v14, 0x2

    aput-object v5, v13, v14

    const/4 v14, 0x3

    aput-object v9, v13, v14

    .line 502
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 501
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 505
    :cond_16
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    .line 506
    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DAY:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_17

    .line 507
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 13"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 509
    const v12, 0x7f0a0209

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 510
    aput-object v4, v13, v14

    const/4 v14, 0x1

    aput-object v5, v13, v14

    const/4 v14, 0x2

    aput-object v9, v13, v14

    .line 508
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 511
    :cond_17
    sget v11, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_ALLDAY:I

    or-int/2addr v11, v12

    .line 512
    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_DATE:I

    or-int/2addr v11, v12

    sget v12, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v11, v12

    if-ne v7, v11, :cond_18

    .line 513
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 14"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 516
    const v12, 0x7f0a020a

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 517
    aput-object v4, v13, v14

    const/4 v14, 0x1

    aput-object v5, v13, v14

    const/4 v14, 0x2

    aput-object v9, v13, v14

    .line 515
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 514
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 519
    :cond_18
    const-string/jumbo v11, "[NotificationScheduleDialogView]"

    const-string/jumbo v12, "schedule tts content 15"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 522
    const v12, 0x7f0a01fd

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 523
    aput-object v8, v13, v14

    .line 521
    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 520
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2
.end method

.method private makeTitleString()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 647
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 648
    .local v5, "titleString":Ljava/lang/StringBuilder;
    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    const/4 v3, 0x0

    .line 650
    .local v3, "schedule_content":I
    const-string/jumbo v4, ""

    .line 651
    .local v4, "title":Ljava/lang/String;
    const-string/jumbo v1, ""

    .line 652
    .local v1, "location":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 654
    .local v0, "attendees":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .line 658
    .local v2, "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 659
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 660
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 666
    :goto_0
    sget v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_TITLE:I

    or-int/2addr v3, v6

    .line 671
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 672
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v6

    .line 673
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 674
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v1

    .line 675
    sget v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->MASK_LOCATION:I

    or-int/2addr v3, v6

    .line 726
    :cond_0
    sget v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->SCHEDULE_TITLE:I

    if-ne v3, v6, :cond_4

    .line 727
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 728
    const v7, 0x7f0a0382

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v4, v8, v9

    .line 727
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741
    :cond_1
    :goto_1
    const-string/jumbo v6, "[NotificationScheduleDialogView]"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Title:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    .end local v2    # "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 662
    .restart local v2    # "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 663
    const v7, 0x7f0a04f8

    .line 662
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 729
    :cond_4
    sget v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->SCHEDULE_TITLE_LOCATION:I

    if-ne v3, v6, :cond_1

    .line 730
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 731
    const v7, 0x7f0a01f3

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v4, v8, v9

    aput-object v1, v8, v10

    .line 730
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->makeScheduleTTSString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initSubClassNotification(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    .line 73
    const-string/jumbo v2, "[NotificationScheduleDialogView]"

    const-string/jumbo v3, "initSubClassNotification"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 76
    const-class v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .line 79
    .local v1, "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    const/4 v0, 0x0

    .line 80
    .local v0, "geo":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 81
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 82
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    .end local v0    # "geo":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v2

    .line 83
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v4

    .line 82
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 87
    .restart local v0    # "geo":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    .line 86
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->bUse24HourFormat:Z

    .line 89
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getStartDate()Ljava/util/Date;

    move-result-object v2

    .line 90
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v4

    .line 91
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v4

    .line 89
    invoke-virtual {p0, v2, v3, v4, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->setScheduleInfo(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 93
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    .line 96
    .end local v0    # "geo":Lcom/google/android/gms/maps/model/LatLng;
    .end local v1    # "scheduleInfo":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->setUseAutoTimeout(Z)V

    .line 97
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 770
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 783
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 784
    return-void

    .line 772
    :pswitch_0
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 776
    :pswitch_1
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 779
    :pswitch_2
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 770
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public reponse(Landroid/location/Location;Z)V
    .locals 7
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 789
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    const-string/jumbo v1, "onResponseRequestGeoLocation"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    if-eqz v0, :cond_0

    .line 792
    if-nez p2, :cond_1

    if-eqz p1, :cond_1

    .line 793
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    .line 794
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    .line 793
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->setOrigin(DD)V

    .line 795
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLatitude:D

    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLongitude:D

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTts:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startNavigation(Landroid/content/Context;DDLjava/lang/String;)V

    .line 802
    :cond_0
    :goto_0
    return-void

    .line 797
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigation:Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMap(Landroid/content/Context;)V

    .line 798
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Timeout: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    const-string/jumbo v1, "Fail to get current location."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setReadoutFlag(Landroid/content/Context;Z)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flag"    # Z

    .prologue
    .line 123
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    const-string/jumbo v1, "setReadoutFlag"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const/4 v0, 0x1

    invoke-super {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setReadoutFlag(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public setScheduleInfo(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 12
    .param p1, "date"    # Ljava/util/Date;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "geo"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 131
    const-string/jumbo v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 130
    check-cast v2, Landroid/view/LayoutInflater;

    .line 133
    .local v2, "inflater":Landroid/view/LayoutInflater;
    if-eqz p4, :cond_5

    .line 134
    const/4 v3, 0x0

    .line 135
    .local v3, "layout":I
    iget-boolean v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->isMultiMode:Z

    if-eqz v9, :cond_4

    .line 136
    const v3, 0x7f0300c5

    .line 141
    :goto_0
    invoke-virtual {v2, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 143
    .local v8, "view":Landroid/view/View;
    const v9, 0x7f090304

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    .line 144
    const v9, 0x7f090303

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    .line 145
    const v9, 0x7f090306

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    .line 146
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    const v9, 0x7f0902ad

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    .line 151
    const v9, 0x7f0902ae

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    .line 152
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton1:Landroid/widget/Button;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mNavigateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mButton2:Landroid/widget/Button;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mOKListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    :goto_1
    const-string/jumbo v7, ""

    .line 177
    .local v7, "titleText":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 179
    iget-boolean v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->bUse24HourFormat:Z

    if-eqz v9, :cond_7

    .line 180
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "HH:mm"

    invoke-direct {v4, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 181
    .local v4, "sdfTime":Ljava/text/SimpleDateFormat;
    invoke-virtual {v4, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 182
    .local v5, "strTime":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 183
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :cond_0
    const-string/jumbo v9, "[NotificationScheduleDialogView]"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Date(24H):"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    .end local v4    # "sdfTime":Ljava/text/SimpleDateFormat;
    .end local v5    # "strTime":Ljava/lang/String;
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->makeTitleString()Ljava/lang/String;

    move-result-object v6

    .line 223
    .local v6, "titleString":Ljava/lang/String;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 224
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 228
    :cond_2
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 229
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :cond_3
    return-void

    .line 138
    .end local v6    # "titleString":Ljava/lang/String;
    .end local v7    # "titleText":Ljava/lang/String;
    .end local v8    # "view":Landroid/view/View;
    :cond_4
    const v3, 0x7f0300c4

    goto/16 :goto_0

    .line 156
    .end local v3    # "layout":I
    :cond_5
    const/4 v3, 0x0

    .line 157
    .restart local v3    # "layout":I
    iget-boolean v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->isMultiMode:Z

    if-eqz v9, :cond_6

    .line 158
    const v3, 0x7f0300c7

    .line 162
    :goto_3
    invoke-virtual {v2, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 164
    .restart local v8    # "view":Landroid/view/View;
    const v9, 0x7f090304

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    .line 165
    const v9, 0x7f090303

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    .line 166
    const v9, 0x7f090306

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    .line 167
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTitle:Landroid/widget/TextView;

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    const v9, 0x7f0902a8

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    .line 172
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mLlNotiBase:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_1

    .line 160
    .end local v8    # "view":Landroid/view/View;
    :cond_6
    const v3, 0x7f0300c6

    goto :goto_3

    .line 188
    .restart local v7    # "titleText":Ljava/lang/String;
    .restart local v8    # "view":Landroid/view/View;
    :cond_7
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "hh:mm"

    invoke-direct {v4, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 189
    .restart local v4    # "sdfTime":Ljava/text/SimpleDateFormat;
    invoke-virtual {v4, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 190
    .restart local v5    # "strTime":Ljava/lang/String;
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_8

    .line 191
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvTime:Landroid/widget/TextView;

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    :cond_8
    const-string/jumbo v9, "[NotificationScheduleDialogView]"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Date(12H):"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 200
    .local v1, "cal":Ljava/util/Calendar;
    invoke-virtual {v1, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 201
    const/16 v9, 0x9

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 203
    .local v0, "am_pm":I
    if-nez v0, :cond_a

    .line 204
    const-string/jumbo v9, "[NotificationScheduleDialogView]"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "AM:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 206
    const v10, 0x7f0a00e0

    .line 205
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 215
    :cond_9
    :goto_4
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 216
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->mTvAmPM:Landroid/widget/TextView;

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 208
    :cond_a
    const/4 v9, 0x1

    if-ne v0, v9, :cond_9

    .line 209
    const-string/jumbo v9, "[NotificationScheduleDialogView]"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "PM 1:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 211
    const v10, 0x7f0a00e1

    .line 210
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 212
    const-string/jumbo v9, "[NotificationScheduleDialogView]"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "PM 2:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method

.method protected startDefaultNotiTimeoutFlow()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 102
    const-string/jumbo v0, "[NotificationScheduleDialogView]"

    const-string/jumbo v1, "startDefaultNotiTimeoutFlow"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationScheduleDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 118
    :cond_0
    return-void
.end method
