.class public Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "WakeUpActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isCheckDrivingMode:I

.field private screenWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->isCheckDrivingMode:I

    .line 14
    return-void
.end method

.method private acquireWakeLock(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    .line 84
    const-string/jumbo v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 83
    check-cast v0, Landroid/os/PowerManager;

    .line 85
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x1000001a

    .line 86
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    .line 85
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 88
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 91
    :cond_0
    return-void
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 98
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 21
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onCreate()"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    const-string/jumbo v1, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 26
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 26
    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsWakeup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 30
    const-string/jumbo v2, "car_mode_on"

    .line 28
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->isCheckDrivingMode:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    invoke-direct {p0, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->acquireWakeLock(Landroid/content/Context;)V

    .line 38
    iget v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->isCheckDrivingMode:I

    if-ne v1, v3, :cond_2

    .line 39
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[Wakeup] - start recognition"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-static {p0, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsWakeup(Landroid/content/Context;Z)V

    .line 57
    :cond_1
    :goto_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onCreate() - finish"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->finish()V

    .line 59
    return-void

    .line 31
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 43
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[Wakeup] - start Application"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 53
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "car_mode_on"

    .line 52
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 78
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->releaseWakeLock()V

    .line 80
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 71
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 65
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    return-void
.end method
