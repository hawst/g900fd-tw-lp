.class public Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;
.super Landroid/os/Binder;
.source "PushMessageService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ServiceBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public registerPushMessageServiceListener(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;

    .prologue
    .line 138
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "registerPushMessageServiceListener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService$ServiceBinder;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageService;Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/IPushMessageServiceListener;)V

    .line 140
    return-void
.end method
