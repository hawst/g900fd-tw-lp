.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;
.source "DLPlaceONGroup.java"


# static fields
.field public static final SHARE_TYPE_ONETIME:I = 0x1

.field public static final SHARE_TYPE_REALTIME:I = 0x2

.field private static final serialVersionUID:J = 0x1e52d531d2c2f28eL


# instance fields
.field private mExpireTime:J

.field private mGroupId:I

.field private mGroupType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

.field private mGroupUrl:Ljava/lang/String;

.field private mMyBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

.field private mShareType:I

.field private mStatusInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 24
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;-><init>()V

    .line 15
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->SHARE_GROUPLOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    .line 17
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mShareType:I

    .line 18
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupUrl:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mMyBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 21
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mExpireTime:J

    .line 25
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;->SHARE_GROUPLOCATION:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupId:I

    .line 27
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mShareType:I

    .line 28
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupUrl:Ljava/lang/String;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    .line 30
    return-void
.end method


# virtual methods
.method public addGroupParticipant(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 0
    .param p1, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->addParticipant(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V

    .line 93
    return-void
.end method

.method public countStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V
    .locals 3
    .param p1, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    .line 114
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->getValue()I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()J
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getIsExpired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-wide/16 v0, 0x0

    .line 51
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mExpireTime:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public getExpireTime()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mExpireTime:J

    return-wide v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupId:I

    return v0
.end method

.method public getGroupType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    return-object v0
.end method

.method public getGroupUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getIsExpired()Z
    .locals 4

    .prologue
    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mExpireTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMyBuddy(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mMyBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    if-nez v1, :cond_0

    .line 105
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->getMyPhoneNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "phone":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getParticipantFromPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mMyBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 108
    .end local v0    # "phone":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mMyBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    return-object v1
.end method

.method public getParticipantsArrived()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 144
    .local v1, "destination":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mParticipants:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 151
    return-object v2

    .line 144
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 145
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v3

    .line 146
    .local v3, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isSamePosition(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 147
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getParticipantsJoined()I
    .locals 5

    .prologue
    .line 127
    const/4 v1, 0x0

    .line 128
    .local v1, "joined":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mParticipants:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 129
    const/4 v2, 0x0

    .line 137
    :goto_0
    return v2

    .line 131
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mParticipants:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    move v2, v1

    .line 137
    goto :goto_0

    .line 131
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 133
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v3

    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne v3, v4, :cond_1

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getShareType()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mShareType:I

    return v0
.end method

.method public getStatusCount(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)I
    .locals 1
    .param p1, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetStatusCount()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mStatusInfo:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 101
    return-void
.end method

.method public setDestination(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 85
    return-void
.end method

.method public setExpireTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 80
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mExpireTime:J

    .line 81
    return-void
.end method

.method public setGroupId(I)V
    .locals 0
    .param p1, "groupId"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupId:I

    .line 42
    return-void
.end method

.method public setGroupParticipants(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->setParticipants(Ljava/util/ArrayList;)V

    .line 97
    return-void
.end method

.method public setGroupType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupType:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroupType;

    .line 38
    return-void
.end method

.method public setGroupUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "groupUrl"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mGroupUrl:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setShareType(I)V
    .locals 0
    .param p1, "shareType"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->mShareType:I

    .line 61
    return-void
.end method
