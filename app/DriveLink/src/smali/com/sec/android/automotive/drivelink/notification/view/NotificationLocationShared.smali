.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationShared.java"


# static fields
.field protected static mContext:Landroid/content/Context;

.field private static mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mViewLocal:Landroid/view/View;

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mContext:Landroid/content/Context;

    .line 23
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 24
    const-string/jumbo v0, " "

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 26
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mImageView:Landroid/widget/ImageView;

    .line 47
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 48
    invoke-virtual {p4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->initIndicatorView(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 24
    const-string/jumbo v0, " "

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 26
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mImageView:Landroid/widget/ImageView;

    .line 39
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 40
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->initIndicatorView(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 24
    const-string/jumbo v0, " "

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 26
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mImageView:Landroid/widget/ImageView;

    .line 31
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 32
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->initIndicatorView(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v10, 0x7f02023b

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 53
    if-nez p1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 57
    :cond_0
    sput-object p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mContext:Landroid/content/Context;

    .line 59
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 58
    sput-object v5, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 61
    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 60
    check-cast v0, Landroid/view/LayoutInflater;

    .line 63
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    if-nez v5, :cond_2

    .line 64
    const v5, 0x7f0300b4

    invoke-virtual {v0, v5, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 66
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    .line 67
    const v6, 0x7f0902ce

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 66
    check-cast v1, Landroid/widget/TextView;

    .line 69
    .local v1, "mNameView":Landroid/widget/TextView;
    const v5, 0x7f0a021d

    new-array v6, v8, [Ljava/lang/Object;

    .line 70
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    aput-object v7, v6, v9

    .line 69
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 73
    const v6, 0x7f0a0603

    new-array v7, v8, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->name:Ljava/lang/String;

    aput-object v8, v7, v9

    .line 72
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 74
    .local v3, "prompt":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 76
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mViewLocal:Landroid/view/View;

    const v6, 0x7f0902cd

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mImageView:Landroid/widget/ImageView;

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 80
    const v6, 0x7f020238

    .line 78
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 82
    .local v4, "shareBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 81
    invoke-static {v5, v4, v10}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 84
    .local v2, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_1

    .line 85
    sget-object v5, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 86
    const v6, 0x7f02023d

    .line 85
    invoke-static {v5, v6, v10}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 89
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 90
    const v6, 0x7f02023c

    .line 89
    invoke-static {v5, v2, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 92
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 96
    .end local v1    # "mNameView":Landroid/widget/TextView;
    .end local v2    # "maskedBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "prompt":Ljava/lang/String;
    .end local v4    # "shareBitmap":Landroid/graphics/Bitmap;
    :cond_2
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared$1;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShared;)V

    .line 102
    const-wide/16 v7, 0xbb8

    .line 96
    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
