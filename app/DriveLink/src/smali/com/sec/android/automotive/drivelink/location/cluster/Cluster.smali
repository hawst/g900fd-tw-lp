.class public Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;
.super Ljava/lang/Object;
.source "Cluster.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private latitude:D

.field private longitude:D

.field private markers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(ILjava/util/ArrayList;IDD)V
    .locals 0
    .param p1, "size"    # I
    .param p3, "i"    # I
    .param p4, "latitude"    # D
    .param p6, "longitude"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<TT;>;IDD)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    .local p2, "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->markers:Ljava/util/ArrayList;

    .line 17
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->latitude:D

    .line 18
    iput-wide p6, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->longitude:D

    .line 19
    return-void
.end method


# virtual methods
.method public getLatitude()D
    .locals 2

    .prologue
    .line 30
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 38
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->longitude:D

    return-wide v0
.end method

.method public getMarkers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->markers:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "latitude"    # D

    .prologue
    .line 34
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->latitude:D

    .line 35
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "longitude"    # D

    .prologue
    .line 42
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->longitude:D

    .line 43
    return-void
.end method

.method public setMarkers(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<TT;>;"
    .local p1, "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;->markers:Ljava/util/ArrayList;

    .line 27
    return-void
.end method
