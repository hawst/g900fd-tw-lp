.class public Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
.super Ljava/lang/Object;
.source "FlowSet.java"


# static fields
.field protected static DmCommandFlowChanging:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected static DmCommandMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final DmFieldIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static DmFlowMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static DmPromptMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mDmFlow:Ljava/lang/String;

.field public mDmflowParams:Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

.field public mFlowDrivenType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$1;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFlowMap:Ljava/util/HashMap;

    .line 108
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$2;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFieldIdMap:Ljava/util/HashMap;

    .line 196
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmPromptMap:Ljava/util/HashMap;

    .line 349
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$4;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmCommandMap:Ljava/util/HashMap;

    .line 378
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$5;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$5;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmCommandFlowChanging:Ljava/util/HashMap;

    .line 405
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mFlowDrivenType:I

    .line 34
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmflowParams:Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .line 30
    return-void
.end method

.method protected static getFiledIdByFlowId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 181
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFieldIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected static getFlowIdByFieldId(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 185
    const/4 v1, 0x0

    .line 187
    .local v1, "flowID":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFieldIdMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 193
    return-object v1

    .line 187
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 188
    .local v0, "entity":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 189
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "flowID":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "flowID":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getPrompt(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 14
    .param p0, "flowID"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v13, 0x7f0a0147

    const/4 v7, 0x2

    const/4 v12, 0x0

    const/4 v10, 0x1

    .line 296
    const/4 v2, 0x0

    .line 297
    .local v2, "prompt":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmPromptMap:Ljava/util/HashMap;

    invoke-virtual {v6, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 299
    .local v5, "resoucreID":I
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 300
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 303
    :cond_0
    const-string/jumbo v6, "DM_DIAL_CONTACT"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 304
    new-array v3, v7, [I

    fill-array-data v3, :array_0

    .line 309
    .local v3, "prompts":[I
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    .line 310
    .local v4, "r":Ljava/util/Random;
    invoke-virtual {v4, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 312
    .local v0, "i":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    aget v7, v3, v0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 346
    .end local v0    # "i":I
    .end local v3    # "prompts":[I
    .end local v4    # "r":Ljava/util/Random;
    :cond_1
    :goto_0
    return-object v2

    .line 313
    :cond_2
    const-string/jumbo v6, "DM_SMS_CONTACT"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 314
    new-array v3, v7, [I

    fill-array-data v3, :array_1

    .line 318
    .restart local v3    # "prompts":[I
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    .line 319
    .restart local v4    # "r":Ljava/util/Random;
    invoke-virtual {v4, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 321
    .restart local v0    # "i":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    aget v7, v3, v0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 322
    goto :goto_0

    .end local v0    # "i":I
    .end local v3    # "prompts":[I
    .end local v4    # "r":Ljava/util/Random;
    :cond_3
    const-string/jumbo v6, "DM_SMS_INBOX"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 323
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v6

    .line 324
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getUnreadMessageCount()I

    move-result v1

    .line 326
    .local v1, "msgCount":I
    if-nez v1, :cond_4

    .line 327
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 328
    new-array v7, v10, [Ljava/lang/Object;

    const-string/jumbo v8, ""

    aput-object v8, v7, v12

    .line 327
    invoke-virtual {v6, v13, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 329
    goto :goto_0

    .line 330
    :cond_4
    if-ne v1, v10, :cond_5

    .line 331
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 333
    new-array v7, v10, [Ljava/lang/Object;

    .line 334
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 335
    const v9, 0x7f0a0181

    new-array v10, v10, [Ljava/lang/Object;

    .line 336
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    .line 334
    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    .line 332
    invoke-virtual {v6, v13, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 337
    goto :goto_0

    .line 338
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 339
    new-array v7, v10, [Ljava/lang/Object;

    .line 340
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 341
    const v9, 0x7f0a0180

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    .line 340
    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    .line 338
    invoke-virtual {v6, v13, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 304
    :array_0
    .array-data 4
        0x7f0a00b9
        0x7f0a00ba
    .end array-data

    .line 314
    :array_1
    .array-data 4
        0x7f0a0026
        0x7f0a019d
    .end array-data
.end method

.method public static getVideoCallSlotPrompt(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    .line 282
    const/4 v1, 0x0

    .line 283
    .local v1, "prompt":Ljava/lang/String;
    new-array v2, v4, [I

    fill-array-data v2, :array_0

    .line 287
    .local v2, "prompts":[I
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 288
    .local v3, "r":Ljava/util/Random;
    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 290
    .local v0, "i":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    aget v5, v2, v0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 292
    return-object v1

    .line 283
    nop

    :array_0
    .array-data 4
        0x7f0a013c
        0x7f0a013c
    .end array-data
.end method
