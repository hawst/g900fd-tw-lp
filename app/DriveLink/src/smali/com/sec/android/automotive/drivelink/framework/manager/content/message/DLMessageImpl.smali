.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
.source "DLMessageImpl.java"


# instance fields
.field private mAttachment:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

.field private mMsgBody:Ljava/lang/String;

.field private mMsgId:I

.field private mMsgThreadID:I

.field private mMsgType:I

.field private mPhoneNumber:Ljava/lang/String;

.field private mReadStatus:I

.field private mReceivedDate:J


# direct methods
.method public constructor <init>(IIJLjava/lang/String;IILjava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;)V
    .locals 4
    .param p1, "msgThreadID"    # I
    .param p2, "msgId"    # I
    .param p3, "receivedDate"    # J
    .param p5, "msgBody"    # Ljava/lang/String;
    .param p6, "msgType"    # I
    .param p7, "readStatus"    # I
    .param p8, "phoneNumber"    # Ljava/lang/String;
    .param p9, "attachment"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;-><init>()V

    .line 8
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgThreadID:I

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgId:I

    .line 10
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReceivedDate:J

    .line 11
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    .line 12
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgType:I

    .line 13
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReadStatus:I

    .line 14
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    .line 15
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mAttachment:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 36
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgThreadID:I

    .line 37
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgId:I

    .line 38
    iput-wide p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReceivedDate:J

    .line 39
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    .line 40
    iput p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgType:I

    .line 41
    iput p7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReadStatus:I

    .line 42
    iput-object p8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    .line 43
    iput-object p9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mAttachment:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "msgBody"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 18
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;-><init>()V

    .line 8
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgThreadID:I

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgId:I

    .line 10
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReceivedDate:J

    .line 11
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    .line 12
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgType:I

    .line 13
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReadStatus:I

    .line 14
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    .line 15
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mAttachment:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 19
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "msgBody"    # Ljava/lang/String;
    .param p3, "msgId"    # I
    .param p4, "receivedDate"    # J

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;-><init>()V

    .line 8
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgThreadID:I

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgId:I

    .line 10
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReceivedDate:J

    .line 11
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    .line 12
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgType:I

    .line 13
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReadStatus:I

    .line 14
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    .line 15
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mAttachment:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 26
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    .line 28
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgId:I

    .line 29
    iput-wide p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReceivedDate:J

    .line 30
    return-void
.end method


# virtual methods
.method public getAttachmentInfo()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mAttachment:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    return-object v0
.end method

.method public getMsgBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    return-object v0
.end method

.method public getMsgID()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgId:I

    return v0
.end method

.method public getMsgThreadID()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgThreadID:I

    return v0
.end method

.method public getMsgType()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgType:I

    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getReadStatus()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReadStatus:I

    return v0
.end method

.method public getReceivedTime()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mReceivedDate:J

    return-wide v0
.end method

.method public setAttachmentInfo(Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;)V
    .locals 0
    .param p1, "attachment"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mAttachment:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 97
    return-void
.end method

.method public setMsgBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mMsgBody:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->mPhoneNumber:Ljava/lang/String;

    .line 82
    return-void
.end method
