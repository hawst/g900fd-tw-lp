.class Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager$DateComparator;
.super Ljava/lang/Object;
.source "ContentManager.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DateComparator"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager$DateComparator;)V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager$DateComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8
    .param p1, "obj1"    # Ljava/lang/Object;
    .param p2, "obj2"    # Ljava/lang/Object;

    .prologue
    .line 322
    move-object v0, p1

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .local v0, "callLog1":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    move-object v1, p2

    .line 323
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 325
    .local v1, "callLog2":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getDate()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getDate()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 326
    .local v2, "val":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 327
    const/4 v4, 0x1

    .line 329
    :goto_0
    return v4

    :cond_0
    const/4 v4, -0x1

    goto :goto_0
.end method
