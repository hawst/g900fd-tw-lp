.class public Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;
.super Ljava/lang/Object;
.source "CarSpeedSampling.java"


# instance fields
.field private mSpeedSamplingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->mSpeedSamplingList:Ljava/util/ArrayList;

    .line 14
    return-void
.end method

.method public static toCarSpeedStatus(FFF)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    .locals 2
    .param p0, "speed"    # F
    .param p1, "minSpeedThreshold"    # F
    .param p2, "maxSpeedThreshold"    # F

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_STOPPED:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 20
    .local v0, "status":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    cmpl-float v1, p0, p1

    if-lez v1, :cond_1

    cmpg-float v1, p0, p2

    if-gtz v1, :cond_1

    .line 21
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_SLOW:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 26
    :cond_0
    :goto_0
    return-object v0

    .line 22
    :cond_1
    cmpl-float v1, p0, p2

    if-lez v1, :cond_0

    .line 23
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;->CAR_SPEED_STATUS_FAST:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    goto :goto_0
.end method


# virtual methods
.method public addData(F)V
    .locals 2
    .param p1, "speed"    # F

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->mSpeedSamplingList:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/Float;

    invoke-direct {v1, p1}, Ljava/lang/Float;-><init>(F)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public clearData()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->mSpeedSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 37
    return-void
.end method

.method public getAverageSpeed()F
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 48
    .local v0, "averageSpeed":F
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->mSpeedSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 53
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->mSpeedSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    return v2

    .line 50
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->mSpeedSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v0, v2

    .line 48
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/CarSpeedSampling;->mSpeedSamplingList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
