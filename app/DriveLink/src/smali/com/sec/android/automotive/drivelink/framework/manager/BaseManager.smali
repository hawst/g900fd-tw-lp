.class public Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.super Ljava/lang/Object;
.source "BaseManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;


# instance fields
.field private mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 9
    return-void
.end method


# virtual methods
.method public getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    return-object v0
.end method

.method protected getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x1

    return v0
.end method

.method public setMediator(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Mediator;)V
    .locals 0
    .param p1, "mediator"    # Lcom/sec/android/automotive/drivelink/framework/common/pattern/Mediator;

    .prologue
    .line 14
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .end local p1    # "mediator":Lcom/sec/android/automotive/drivelink/framework/common/pattern/Mediator;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->mDriveLinkServiceProvider:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .line 15
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    return-void
.end method
