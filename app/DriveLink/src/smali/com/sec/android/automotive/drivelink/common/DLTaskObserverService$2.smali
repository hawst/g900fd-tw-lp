.class Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;
.super Landroid/content/BroadcastReceiver;
.source "DLTaskObserverService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    .line 109
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 113
    const-string/jumbo v2, "DLTaskObserverService"

    const-string/jumbo v3, "onReceive"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    if-nez p2, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_TASK_OBSERVER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    const-string/jumbo v2, "EXTRA_TASK_STATE"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 121
    .local v1, "state":I
    const-string/jumbo v2, "EXTRA_TASK_PKG_NAME"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "pkgName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 127
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v3

    monitor-enter v3

    .line 128
    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 129
    :try_start_0
    const-string/jumbo v2, "DLTaskObserverService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Task state : start - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v2

    const-string/jumbo v4, "STATE_RUNNING"

    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$3(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 144
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-gtz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->isFullPolling:Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$5(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$3(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->DELAY_TIME:J
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$4()J

    move-result-wide v3

    invoke-virtual {v2, v6, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 132
    :cond_3
    :try_start_1
    const-string/jumbo v2, "DLTaskObserverService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Task state : stop - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->isFullPolling:Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$5(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 134
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v2

    const-string/jumbo v4, "STATE_NOT_RUNNING"

    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 127
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 136
    :cond_4
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
