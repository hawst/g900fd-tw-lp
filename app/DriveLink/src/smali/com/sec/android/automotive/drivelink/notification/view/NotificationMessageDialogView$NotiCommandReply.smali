.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;
.super Ljava/lang/Object;
.source "NotificationMessageDialogView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/INotiCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotiCommandReply"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V
    .locals 0

    .prologue
    .line 700
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public action(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 705
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$5(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 707
    .local v3, "displayName":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 708
    .local v6, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 709
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$4(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)Ljava/lang/String;

    move-result-object v2

    .line 708
    invoke-direct {v9, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 710
    .local v9, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    const-wide/16 v1, 0x0

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 714
    .local v0, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 716
    .local v8, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 717
    .local v7, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v8, v7, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 718
    const-string/jumbo v1, "DM_SMS_COMPOSE"

    invoke-static {v1, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 721
    return-void
.end method
