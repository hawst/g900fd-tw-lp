.class Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

.field private final synthetic val$btDevice:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    .line 640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 643
    const/4 v1, 0x0

    .line 644
    .local v1, "internalTimer":I
    :goto_0
    const/16 v2, 0x1388

    if-le v1, v2, :cond_2

    .line 660
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    iget-object v3, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    monitor-enter v3

    .line 661
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 660
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 664
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v3

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 665
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    .line 664
    iput v3, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    .line 666
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    if-ne v2, v7, :cond_3

    .line 667
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$7(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 668
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 688
    :cond_1
    :goto_1
    return-void

    .line 645
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v3

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 646
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    .line 645
    iput v3, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    .line 647
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    if-eq v2, v7, :cond_0

    .line 652
    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 653
    add-int/lit8 v1, v1, 0x64

    goto/16 :goto_0

    .line 654
    :catch_0
    move-exception v0

    .line 655
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v2

    .line 656
    const-string/jumbo v3, "connectBluetoothDevice"

    .line 655
    invoke-static {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 660
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 669
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->result:I

    if-nez v2, :cond_1

    .line 670
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 671
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 672
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1$1;->this$2:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3$1;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$7(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    goto :goto_1
.end method
