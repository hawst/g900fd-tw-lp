.class public Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;
.super Ljava/lang/Object;
.source "UpdateConstant.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;
    }
.end annotation


# static fields
.field public static final APP_ID:Ljava/lang/String; = "appId"

.field public static final CSC:Ljava/lang/String; = "csc"

.field public static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field public static final DEFAULT_MCC:Ljava/lang/String; = "505"

.field public static final DEVICE:Ljava/lang/String; = "deviceId"

.field public static final DL_APP_ID:Ljava/lang/String; = "com.sec.android.automotive.drivelink"

.field public static final DOWNLOAD_COMPLETE:I = 0x64

.field public static final DOWNLOAD_START:I = 0x0

.field public static final DownloadConnectTimeOut:I = 0x2710

.field public static final DownloadSocketTimeOut:I = 0x7530

.field public static final FILE_NAME:Ljava/lang/String; = ".trancsli"

.field public static final IMEI:Ljava/lang/String; = "encImei"

.field public static final MCC:Ljava/lang/String; = "mcc"

.field public static final MNC:Ljava/lang/String; = "mnc"

.field public static final NULL_STRING:Ljava/lang/String; = ""

.field public static final PD:Ljava/lang/String; = "pd"

.field public static final PD_TEST_PATH:Ljava/lang/String; = "/sdcard/go_to_andromeda.test"

.field public static final SAMSUNG_APPS_CHECK_URL:Ljava/lang/String; = "http://hub.samsungapps.com/product/appCheck.as"

.field public static final SAMSUNG_APPS_CLASS:Ljava/lang/String; = "com.sec.android.app.samsungapps.Main"

.field public static final SAMSUNG_APPS_ID:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final SDK_VER:Ljava/lang/String; = "sdkVer"

.field public static final SERVER_URL_CTC:Ljava/lang/String; = "http://cn-ms.samsungapps.com/getCNVasURL.as"

.field public static final STUB_CHECK_URL:Ljava/lang/String; = "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

.field public static final STUB_DOWNLOAD_URL:Ljava/lang/String; = "https://vas.samsungapps.com/stub/stubDownload.as"

.field private static final TAG:Ljava/lang/String; = "UpdateManager"

.field public static final URL_TYPE_DOWNLOAD:I = 0x1

.field public static final URL_TYPE_VERSION_CHECK:I = 0x0

.field protected static final UpdateConnectTimeOut:I = 0x1388

.field protected static final UpdateSocketTimeOut:I = 0x3a98

.field public static final V_CODE:Ljava/lang/String; = "versionCode"

.field private static mDownloadUriCTC:Ljava/lang/String;

.field private static mUpdateUriCTC:Ljava/lang/String;

.field private static final useMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 78
    const-string/jumbo v0, ""

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mUpdateUriCTC:Ljava/lang/String;

    .line 79
    const-string/jumbo v0, ""

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mDownloadUriCTC:Ljava/lang/String;

    .line 83
    const-string/jumbo v0, "user"

    .line 84
    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 83
    :goto_0
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->useMode:Z

    .line 84
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAPIVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 163
    const-string/jumbo v0, ""

    .line 164
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 167
    .local v1, "value":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->isCSCExistFile()Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    :goto_0
    return-object v0

    .line 171
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 173
    if-nez v1, :cond_1

    .line 174
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 178
    :cond_1
    const-string/jumbo v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 183
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getCSCVersion()Ljava/lang/String;
    .locals 8

    .prologue
    .line 191
    const/4 v5, 0x0

    .line 192
    .local v5, "s":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string/jumbo v7, "/system/csc/sales_code.dat"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 194
    .local v4, "mFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 195
    const/16 v7, 0x14

    new-array v0, v7, [B

    .line 196
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 199
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 201
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-eqz v7, :cond_1

    .line 202
    new-instance v6, Ljava/lang/String;

    const-string/jumbo v7, "UTF-8"

    invoke-direct {v6, v0, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .end local v5    # "s":Ljava/lang/String;
    .local v6, "s":Ljava/lang/String;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    move-object v5, v6

    .line 212
    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :goto_0
    if-eqz v2, :cond_0

    .line 214
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 223
    .end local v0    # "buffer":[B
    .end local v2    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    return-object v5

    .line 204
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_1
    :try_start_3
    new-instance v6, Ljava/lang/String;

    const-string/jumbo v7, "FAIL"

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .end local v5    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    move-object v5, v6

    .line 206
    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    goto :goto_0

    .line 215
    :catch_0
    move-exception v1

    .line 217
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 208
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    goto :goto_0

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_2
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_0

    .line 206
    :catch_3
    move-exception v7

    goto :goto_0

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_4
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public static getDeviceModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public static getIMEI(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 317
    const-string/jumbo v3, ""

    .line 319
    .local v3, "imei":Ljava/lang/String;
    const-string/jumbo v6, "phone"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 318
    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 320
    .local v5, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 322
    .local v4, "md5":Ljava/lang/StringBuffer;
    if-eqz v5, :cond_0

    .line 324
    const/16 v6, 0xc

    :try_start_0
    new-array v0, v6, [B

    const/4 v6, 0x0

    const/4 v7, 0x1

    aput-byte v7, v0, v6

    const/4 v6, 0x1

    const/4 v7, 0x2

    aput-byte v7, v0, v6

    const/4 v6, 0x2

    const/4 v7, 0x3

    aput-byte v7, v0, v6

    const/4 v6, 0x3

    const/4 v7, 0x4

    aput-byte v7, v0, v6

    const/4 v6, 0x4

    const/4 v7, 0x5

    aput-byte v7, v0, v6

    const/4 v6, 0x5

    const/4 v7, 0x6

    aput-byte v7, v0, v6

    const/4 v6, 0x6

    const/4 v7, 0x7

    aput-byte v7, v0, v6

    const/4 v6, 0x7

    const/16 v7, 0x8

    aput-byte v7, v0, v6

    const/16 v6, 0x8

    const/16 v7, 0x9

    aput-byte v7, v0, v6

    const/16 v6, 0xa

    const/4 v7, 0x1

    aput-byte v7, v0, v6

    const/16 v6, 0xb

    const/4 v7, 0x2

    aput-byte v7, v0, v6

    .line 326
    .local v0, "digest":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-lt v2, v6, :cond_1

    .line 335
    .end local v0    # "digest":[B
    .end local v2    # "i":I
    :cond_0
    :goto_1
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    .line 336
    const/4 v7, 0x0

    .line 335
    invoke-static {v6, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    .line 342
    :goto_2
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 327
    .restart local v0    # "digest":[B
    .restart local v2    # "i":I
    :cond_1
    :try_start_2
    aget-byte v6, v0, v2

    and-int/lit16 v6, v6, 0xf0

    shr-int/lit8 v6, v6, 0x4

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 328
    aget-byte v6, v0, v2

    and-int/lit8 v6, v6, 0xf

    shr-int/lit8 v6, v6, 0x0

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 326
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 337
    .end local v0    # "digest":[B
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 339
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_2

    .line 330
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v6

    goto :goto_1
.end method

.method public static getMCC(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 104
    .line 105
    const-string/jumbo v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 104
    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 106
    .local v3, "telMgr":Landroid/telephony/TelephonyManager;
    const-string/jumbo v0, ""

    .line 108
    .local v0, "mcc":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v1, v0

    .line 143
    .end local v0    # "mcc":Ljava/lang/String;
    .local v1, "mcc":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 112
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "networkOperator":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 127
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 128
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 129
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    move-object v1, v0

    .line 143
    .end local v0    # "mcc":Ljava/lang/String;
    .restart local v1    # "mcc":Ljava/lang/String;
    goto :goto_0

    .line 116
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :pswitch_0
    const-string/jumbo v0, ""

    .line 118
    goto :goto_1

    .line 121
    :pswitch_1
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 122
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 124
    goto :goto_1

    .line 131
    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getMNC(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    .line 227
    const-string/jumbo v0, ""

    .line 230
    .local v0, "mnc":Ljava/lang/String;
    const-string/jumbo v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 229
    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 232
    .local v3, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v3, :cond_0

    move-object v1, v0

    .line 275
    .end local v0    # "mnc":Ljava/lang/String;
    .local v1, "mnc":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 236
    .end local v1    # "mnc":Ljava/lang/String;
    .restart local v0    # "mnc":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "networkOperator":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 256
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 258
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 262
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    move-object v1, v0

    .line 275
    .end local v0    # "mnc":Ljava/lang/String;
    .restart local v1    # "mnc":Ljava/lang/String;
    goto :goto_0

    .line 241
    .end local v1    # "mnc":Ljava/lang/String;
    .restart local v0    # "mnc":Ljava/lang/String;
    :pswitch_0
    const-string/jumbo v0, ""

    .line 243
    goto :goto_1

    .line 247
    :pswitch_1
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 248
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 252
    goto :goto_1

    .line 267
    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1

    .line 238
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getPD()Ljava/lang/String;
    .locals 5

    .prologue
    .line 280
    const-string/jumbo v2, ""

    .line 281
    .local v2, "rtn_str":Ljava/lang/String;
    const/4 v1, 0x0

    .line 283
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string/jumbo v3, "/sdcard/go_to_andromeda.test"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 289
    if-eqz v1, :cond_0

    .line 290
    const-string/jumbo v3, "UpdateManager"

    const-string/jumbo v4, "pd.test is exist"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const-string/jumbo v2, "1"

    .line 304
    :goto_0
    return-object v2

    .line 293
    :cond_0
    const-string/jumbo v3, "UpdateManager"

    const-string/jumbo v4, "pd.test is not exist"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 295
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public static getUrlForCTC(Ljava/lang/String;)Z
    .locals 19
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 441
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "getUrlForCTC"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v1, 0x0

    .line 446
    .local v1, "bReturn":Z
    new-instance v6, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v6}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 448
    .local v6, "httpclient":Lorg/apache/http/client/HttpClient;
    const/16 v14, 0x78

    .line 449
    .local v14, "timeout":I
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    .line 450
    .local v5, "httpParams":Lorg/apache/http/params/HttpParams;
    const v16, 0x1d4c0

    move/from16 v0, v16

    invoke-static {v5, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 451
    const v16, 0x1d4c0

    move/from16 v0, v16

    invoke-static {v5, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 453
    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 454
    .local v7, "httpget":Lorg/apache/http/client/methods/HttpGet;
    const/4 v11, 0x0

    .line 457
    .local v11, "response":Lorg/apache/http/HttpResponse;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 458
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 461
    .local v9, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :try_start_1
    invoke-interface {v6, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 463
    const-string/jumbo v16, "UpdateManager"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string/jumbo v18, "getUrlForCTC() response : "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 464
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 463
    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 468
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v3, :cond_0

    .line 470
    :try_start_2
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v8

    .line 471
    .local v8, "instream":Ljava/io/InputStream;
    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-interface {v9, v8, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 482
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v8    # "instream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    :try_start_3
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v10

    .line 484
    .local v10, "parserEvent":I
    const-string/jumbo v12, ""
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 486
    .local v12, "serverUri":Ljava/lang/String;
    :goto_1
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v10, v0, :cond_2

    .line 509
    const/4 v1, 0x1

    .line 532
    if-eqz v6, :cond_1

    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    if-eqz v16, :cond_1

    .line 533
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 534
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "shutdown"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "parserEvent":I
    .end local v12    # "serverUri":Ljava/lang/String;
    :cond_1
    :goto_2
    return v1

    .line 472
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v2

    .line 473
    .local v2, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 478
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    :catch_1
    move-exception v2

    .line 479
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 511
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v2

    .line 512
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_6
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "xml parsing error"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 514
    const/4 v1, 0x0

    .line 532
    if-eqz v6, :cond_1

    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    if-eqz v16, :cond_1

    .line 533
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 534
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "shutdown"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 474
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_3
    move-exception v2

    .line 475
    .local v2, "e":Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 515
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_4
    move-exception v2

    .line 516
    .local v2, "e":Ljava/net/SocketException;
    :try_start_8
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    .line 517
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "network is unavailable"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 518
    const/4 v1, 0x0

    .line 532
    if-eqz v6, :cond_1

    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    if-eqz v16, :cond_1

    .line 533
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 534
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "shutdown"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 487
    .end local v2    # "e":Ljava/net/SocketException;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "parserEvent":I
    .restart local v12    # "serverUri":Ljava/lang/String;
    :cond_2
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v10, v0, :cond_3

    .line 488
    :try_start_9
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v13

    .line 489
    .local v13, "tag":Ljava/lang/String;
    const-string/jumbo v16, "serverURL"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 490
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v15

    .line 491
    .local v15, "type":I
    const/16 v16, 0x4

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 492
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v12

    .line 496
    .end local v13    # "tag":Ljava/lang/String;
    .end local v15    # "type":I
    :cond_3
    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v10, v0, :cond_4

    .line 497
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v13

    .line 498
    .restart local v13    # "tag":Ljava/lang/String;
    const-string/jumbo v16, "serverURL"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 499
    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "http://"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 500
    const-string/jumbo v17, "/stub/stubUpdateCheck.as"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 499
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    sput-object v16, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mUpdateUriCTC:Ljava/lang/String;

    .line 501
    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "https://"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 502
    const-string/jumbo v17, "/stub/stubDownload.as"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 501
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    sput-object v16, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mDownloadUriCTC:Ljava/lang/String;

    .line 506
    .end local v13    # "tag":Ljava/lang/String;
    :cond_4
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_9
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v10

    goto/16 :goto_1

    .line 519
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "parserEvent":I
    .end local v12    # "serverUri":Ljava/lang/String;
    :catch_5
    move-exception v2

    .line 520
    .local v2, "e":Ljava/net/UnknownHostException;
    :try_start_a
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 521
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "server is not response"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 522
    const/4 v1, 0x0

    .line 532
    if-eqz v6, :cond_1

    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    if-eqz v16, :cond_1

    .line 533
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 534
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "shutdown"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 523
    .end local v2    # "e":Ljava/net/UnknownHostException;
    :catch_6
    move-exception v2

    .line 524
    .local v2, "e":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 525
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "network error"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 526
    const/4 v1, 0x0

    .line 532
    if-eqz v6, :cond_1

    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    if-eqz v16, :cond_1

    .line 533
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 534
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "shutdown"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 527
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v2

    .line 528
    .local v2, "e":Ljava/lang/Exception;
    :try_start_c
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 529
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "Exception error"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 530
    const/4 v1, 0x0

    .line 532
    if-eqz v6, :cond_1

    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    if-eqz v16, :cond_1

    .line 533
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 534
    const-string/jumbo v16, "UpdateManager"

    const-string/jumbo v17, "shutdown"

    invoke-static/range {v16 .. v17}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 531
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v16

    .line 532
    if-eqz v6, :cond_5

    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v17

    if-eqz v17, :cond_5

    .line 533
    invoke-interface {v6}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 534
    const-string/jumbo v17, "UpdateManager"

    const-string/jumbo v18, "shutdown"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_5
    throw v16
.end method

.method public static getVesionCode(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    const/4 v2, 0x0

    .line 93
    .local v2, "verCode":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 94
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 93
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 95
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isCSCExistFile()Z
    .locals 4

    .prologue
    .line 147
    const/4 v1, 0x0

    .line 148
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string/jumbo v2, "/system/csc/sales_code.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 151
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 152
    if-nez v1, :cond_0

    .line 153
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "CSC is not exist"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :cond_0
    :goto_0
    return v1

    .line 155
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static makeURL(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;)Ljava/lang/String;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    .prologue
    .line 346
    const/4 v3, 0x0

    .line 349
    .local v3, "server_url":Ljava/lang/String;
    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 350
    .local v7, "szModel":Ljava/lang/String;
    const-string/jumbo v8, "SAMSUNG-"

    .line 352
    .local v8, "szPrefix":Ljava/lang/String;
    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 353
    const-string/jumbo v10, ""

    invoke-virtual {v7, v8, v10}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 357
    :cond_0
    const-string/jumbo v5, ""

    .line 358
    .local v5, "szMCC":Ljava/lang/String;
    const-string/jumbo v6, ""

    .line 360
    .local v6, "szMNC":Ljava/lang/String;
    const/4 v0, 0x0

    .line 366
    .local v0, "cManager":Landroid/net/ConnectivityManager;
    const-string/jumbo v10, "connectivity"

    invoke-virtual {p0, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 365
    .end local v0    # "cManager":Landroid/net/ConnectivityManager;
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 368
    .restart local v0    # "cManager":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    move-object v4, v3

    .line 436
    .end local v3    # "server_url":Ljava/lang/String;
    .local v4, "server_url":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 372
    .end local v4    # "server_url":Ljava/lang/String;
    .restart local v3    # "server_url":Ljava/lang/String;
    :cond_1
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 373
    .local v2, "mobile":Landroid/net/NetworkInfo;
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v9

    .line 374
    .local v9, "wifi":Landroid/net/NetworkInfo;
    const/16 v10, 0x9

    invoke-virtual {v0, v10}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 376
    .local v1, "ethernet":Landroid/net/NetworkInfo;
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 377
    const-string/jumbo v5, "505"

    .line 378
    const-string/jumbo v6, "00"

    .line 389
    :goto_1
    sget-object v10, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->CHECK:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    if-ne p1, v10, :cond_8

    .line 390
    const-string/jumbo v3, "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

    .line 392
    const-string/jumbo v10, "460"

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string/jumbo v10, "03"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 393
    const-string/jumbo v10, "http://cn-ms.samsungapps.com/getCNVasURL.as"

    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getUrlForCTC(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 394
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mUpdateUriCTC:Ljava/lang/String;

    .line 395
    const-string/jumbo v10, "UpdateManager"

    const-string/jumbo v11, "Update CTC get URL OK!! "

    invoke-static {v10, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_2
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "?appId=com.sec.android.automotive.drivelink"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 402
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&versionCode="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getVesionCode(Landroid/content/Context;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 403
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&deviceId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 404
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&mcc="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 405
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&mnc="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 406
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&csc="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getCSC()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 407
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&sdkVer="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 408
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&pd="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getPD()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 434
    :cond_3
    :goto_3
    const-string/jumbo v10, "UpdateManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "server_url :"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 436
    .end local v3    # "server_url":Ljava/lang/String;
    .restart local v4    # "server_url":Ljava/lang/String;
    goto/16 :goto_0

    .line 379
    .end local v4    # "server_url":Ljava/lang/String;
    .restart local v3    # "server_url":Ljava/lang/String;
    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 380
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getMCC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 381
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getMNC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 382
    goto/16 :goto_1

    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 383
    const-string/jumbo v5, "505"

    .line 384
    const-string/jumbo v6, "00"

    .line 385
    goto/16 :goto_1

    :cond_6
    move-object v4, v3

    .line 386
    .end local v3    # "server_url":Ljava/lang/String;
    .restart local v4    # "server_url":Ljava/lang/String;
    goto/16 :goto_0

    .line 397
    .end local v4    # "server_url":Ljava/lang/String;
    .restart local v3    # "server_url":Ljava/lang/String;
    :cond_7
    const-string/jumbo v10, "UpdateManager"

    const-string/jumbo v11, "Update CTC get URL fail!! "

    invoke-static {v10, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 409
    :cond_8
    sget-object v10, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->DONWLOAD:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    if-ne p1, v10, :cond_3

    .line 410
    const-string/jumbo v3, "https://vas.samsungapps.com/stub/stubDownload.as"

    .line 412
    const-string/jumbo v10, "460"

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    const-string/jumbo v10, "03"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 413
    sget-object v10, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mDownloadUriCTC:Ljava/lang/String;

    const-string/jumbo v11, ""

    if-eq v10, v11, :cond_a

    .line 414
    const-string/jumbo v10, "UpdateManager"

    const-string/jumbo v11, "mDownloadUriCTC is not null."

    invoke-static {v10, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mDownloadUriCTC:Ljava/lang/String;

    .line 424
    :cond_9
    :goto_4
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "?appId=com.sec.android.automotive.drivelink"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 425
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&encImei="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 426
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&deviceId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 427
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&mcc="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 428
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&mnc="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 429
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&csc="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getCSC()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 430
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&sdkVer="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 431
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, "&pd="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getPD()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 417
    :cond_a
    const-string/jumbo v10, "UpdateManager"

    const-string/jumbo v11, "mDownloadUriCTC is null."

    invoke-static {v10, v11}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    const-string/jumbo v10, "http://cn-ms.samsungapps.com/getCNVasURL.as"

    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->getUrlForCTC(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 419
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->mDownloadUriCTC:Ljava/lang/String;

    goto/16 :goto_4
.end method
