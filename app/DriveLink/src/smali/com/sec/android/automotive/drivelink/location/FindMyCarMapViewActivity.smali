.class public Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.source "FindMyCarMapViewActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;,
        Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;
    }
.end annotation


# static fields
.field public static final PARAM_NAME_FUNCTION:Ljava/lang/String; = "find_my_car_param_function"

.field public static final PARAM_VALUE_FUNCTION_SAVE_POSITION:Ljava/lang/String; = "save_my_car_position"

.field public static final PARAM_VALUE_FUNCTION_SEARCH_POSITION:Ljava/lang/String; = "search_car_position"

.field private static final STATE_HIDE_BUTTON:Ljava/lang/String; = "hide_btn"

.field private static final STATE_IS_PAIRED:Ljava/lang/String; = "dev_paired"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private barLayout:Landroid/widget/LinearLayout;

.field private btnImgBack:Landroid/widget/ImageView;

.field private mAddress:Ljava/lang/String;

.field mAddressSearchTask:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;

.field private mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

.field private mCarLocationTime:Ljava/util/Calendar;

.field private mDrawerMenuLayout:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

.field private mFunction:Ljava/lang/String;

.field private mHideSaveBtn:Z

.field private mIsCarConnected:Z

.field private mIsVisible:Z

.field private mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

.field mTimer:Ljava/util/Timer;

.field private mainLayout:Landroid/widget/RelativeLayout;

.field private tvTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    .line 59
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 58
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->TAG:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;-><init>()V

    .line 69
    const-string/jumbo v0, "search_car_position"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mFunction:Ljava/lang/String;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationTime:Ljava/util/Calendar;

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsVisible:Z

    .line 91
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;-><init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddressSearchTask:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;

    .line 97
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mTimer:Ljava/util/Timer;

    .line 55
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Z
    .locals 1

    .prologue
    .line 533
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->isValidLocationPoint(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;Z)V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshBtnView(Z)V

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshView()V

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 468
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->savePreferences()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 622
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateConnectionState()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->recreateMapView()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)Z
    .locals 1

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->hasChangeMyPosition()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateMyLocation()V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 373
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->resetMyLocationMark()V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->createAllCustomMarks()V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;Z)V
    .locals 0

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    return-void
.end method

.method private createView()V
    .locals 5

    .prologue
    .line 244
    const v2, 0x7f090014

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mainLayout:Landroid/widget/RelativeLayout;

    .line 245
    const v2, 0x7f090019

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mDrawerMenuLayout:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    .line 246
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mDrawerMenuLayout:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    .line 247
    const-class v3, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mainLayout:Landroid/widget/RelativeLayout;

    .line 246
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->setActionBar(Ljava/lang/String;Landroid/view/View;)V

    .line 248
    const v2, 0x7f090016

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->btnImgBack:Landroid/widget/ImageView;

    .line 249
    const v2, 0x7f090017

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->tvTitle:Landroid/widget/TextView;

    .line 250
    const v2, 0x7f090015

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->barLayout:Landroid/widget/LinearLayout;

    .line 252
    const v2, 0x7f090018

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 253
    .local v1, "saveButton":Landroid/widget/Button;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    const v2, 0x7f09001a

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 261
    .local v0, "myLocButton":Landroid/widget/Button;
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshView()V

    .line 270
    return-void
.end method

.method private disclaimer()V
    .locals 0

    .prologue
    .line 681
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->initPlaceOnDisclaimnerDialog()V

    .line 682
    return-void
.end method

.method private hasChangeMyPosition()Z
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const-wide/16 v0, 0x0

    .line 407
    new-array v8, v9, [F

    .line 408
    .local v8, "results":[F
    const/4 v2, 0x0

    aput v2, v8, v10

    move-wide v2, v0

    move-wide v4, v0

    move-wide v6, v0

    .line 409
    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 414
    aget v2, v8, v10

    float-to-double v2, v2

    cmpl-double v0, v2, v0

    if-lez v0, :cond_0

    move v0, v9

    .line 417
    :goto_0
    return v0

    :cond_0
    move v0, v10

    goto :goto_0
.end method

.method private initPlaceOnDisclaimnerDialog()V
    .locals 3

    .prologue
    .line 664
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;-><init>()V

    .line 665
    .local v0, "dialog":Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;)V

    .line 676
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 677
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog 3"

    .line 676
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 678
    return-void
.end method

.method private static isValidLocationPoint(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Z
    .locals 4
    .param p0, "point"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .prologue
    .line 535
    if-nez p0, :cond_0

    .line 536
    const/4 v0, 0x0

    .line 538
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    .line 539
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    .line 538
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords(DD)Z

    move-result v0

    goto :goto_0
.end method

.method private loadPreferences()V
    .locals 0

    .prologue
    .line 527
    return-void
.end method

.method private prepareParameters()V
    .locals 5

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 227
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_0

    .line 241
    :goto_0
    return-void

    .line 230
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 232
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v2, "ballon_disable"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 233
    const-string/jumbo v2, "userIndex"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 234
    const-string/jumbo v2, "custom_pin"

    const v3, 0x7f020181

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 235
    const-string/jumbo v2, "latitude"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 236
    const-string/jumbo v2, "longitude"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 237
    const-string/jumbo v2, "myloc_enable"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 239
    invoke-virtual {v1, v0}, Landroid/content/Intent;->replaceExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private recreateMapView()V
    .locals 1

    .prologue
    .line 313
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateAllLocations()V

    .line 316
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->prepareParameters()V

    .line 319
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->resetAllMarks()V

    .line 322
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->savePreferences()V

    .line 324
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsVisible:Z

    if-eqz v0, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->recreate()V

    .line 328
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshView()V

    .line 329
    return-void
.end method

.method private refreshBtnView(Z)V
    .locals 2
    .param p1, "isHideButton"    # Z

    .prologue
    .line 303
    const v0, 0x7f090018

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 304
    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 305
    return-void

    .line 304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshView()V
    .locals 9

    .prologue
    .line 277
    iget-boolean v6, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshBtnView(Z)V

    .line 280
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationTime:Ljava/util/Calendar;

    if-eqz v6, :cond_2

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 283
    const-string/jumbo v7, "time_12_24"

    .line 281
    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, "pattern":Ljava/lang/String;
    const-string/jumbo v3, "hh:mm a"

    .line 286
    .local v3, "patternHour":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 287
    const-string/jumbo v2, "24"

    .line 288
    :cond_0
    const-string/jumbo v6, "24"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 289
    const-string/jumbo v3, "HH:mm"

    .line 291
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 292
    .local v1, "formatter":Ljava/text/SimpleDateFormat;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationTime:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 294
    .local v0, "formattedHour":Ljava/lang/String;
    const v6, 0x7f09001c

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 295
    .local v5, "tvTime":Landroid/widget/TextView;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    .line 296
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationTime:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 295
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    .end local v0    # "formattedHour":Ljava/lang/String;
    .end local v1    # "formatter":Ljava/text/SimpleDateFormat;
    .end local v2    # "pattern":Ljava/lang/String;
    .end local v3    # "patternHour":Ljava/lang/String;
    .end local v5    # "tvTime":Landroid/widget/TextView;
    :cond_2
    const v6, 0x7f09000b

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 299
    .local v4, "tvAddress":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    return-void
.end method

.method private registerConnectivityListener()V
    .locals 2

    .prologue
    .line 614
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateConnectionState()V

    .line 617
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 616
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 617
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    .line 619
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;-><init>()V

    .line 618
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->setOnDriveLinkConnectivityListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;)V

    .line 620
    return-void
.end method

.method private requestProcess()V
    .locals 3

    .prologue
    .line 195
    const-string/jumbo v1, "search_car_position"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mFunction:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 196
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->loadPreferences()V

    .line 197
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateMyLocation()V

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 199
    const v2, 0x7f0a021b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "noAddress":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string/jumbo v1, ""

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 205
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->isValidLocationPoint(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 206
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateCarLocation()V

    .line 215
    .end local v0    # "noAddress":Ljava/lang/String;
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->resetMyLocationMark()V

    .line 216
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->savePreferences()V

    .line 217
    return-void

    .line 207
    .restart local v0    # "noAddress":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string/jumbo v1, ""

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 208
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 209
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateAddress()V

    goto :goto_0

    .line 212
    .end local v0    # "noAddress":Ljava/lang/String;
    :cond_5
    const-string/jumbo v1, "save_my_car_position"

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mFunction:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 213
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateCarLocation()V

    goto :goto_0
.end method

.method private resetAllMarks()V
    .locals 0

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->resetCarLocationMark()V

    .line 356
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->resetMyLocationMark()V

    .line 357
    return-void
.end method

.method private resetCarLocationMark()V
    .locals 6

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->clearAllMarks()V

    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    .line 367
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v3

    const v5, 0x7f020181

    move-object v0, p0

    .line 366
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->changeMainMarkLocation(DDI)V

    .line 368
    return-void
.end method

.method private resetMyLocationMark()V
    .locals 6

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    if-nez v0, :cond_0

    .line 379
    :goto_0
    return-void

    .line 376
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->clearAllCustomMarks()V

    .line 377
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    .line 378
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v3

    const v5, 0x7f020182

    move-object v0, p0

    .line 377
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->addCustomMark(DDI)V

    goto :goto_0
.end method

.method private savePreferences()V
    .locals 0

    .prologue
    .line 497
    return-void
.end method

.method private updateAddress()V
    .locals 6

    .prologue
    .line 439
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddressSearchTask:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;

    .line 440
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->setOnAddressSearchListener(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;)V

    .line 448
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddressSearchTask:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;

    .line 450
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    aput-object v5, v3, v4

    .line 449
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 460
    :goto_0
    return-void

    .line 452
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddressSearchTask:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;

    .line 453
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    aput-object v5, v3, v4

    .line 452
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 455
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mAddress:Ljava/lang/String;

    .line 457
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "getAddress error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 458
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateAllLocations()V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateMyLocation()V

    .line 386
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateCarLocation()V

    .line 387
    return-void
.end method

.method private updateCarLocation()V
    .locals 3

    .prologue
    const-wide v1, 0x40c3878000000000L    # 9999.0

    .line 426
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 429
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationTime:Ljava/util/Calendar;

    .line 431
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->updateAddress()V

    .line 432
    return-void
.end method

.method private updateConnectionState()V
    .locals 2

    .prologue
    .line 623
    .line 624
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 623
    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 625
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getBluetoothConnectionState(Landroid/content/Context;)Z

    move-result v0

    .line 623
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    .line 626
    return-void
.end method

.method private updateMyLocation()V
    .locals 3

    .prologue
    const-wide v1, 0x40c3878000000000L    # 9999.0

    .line 395
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 398
    return-void
.end method

.method private validFunctionParam(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 551
    if-nez p1, :cond_0

    .line 552
    const-string/jumbo v1, "search_car_position"

    .line 562
    :goto_0
    return-object v1

    .line 554
    :cond_0
    const-string/jumbo v1, "find_my_car_param_function"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 556
    .local v0, "functionParamValue":Ljava/lang/String;
    const-string/jumbo v1, "save_my_car_position"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 557
    const-string/jumbo v1, "save_my_car_position"

    goto :goto_0

    .line 558
    :cond_1
    const-string/jumbo v1, "search_car_position"

    .line 559
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 560
    const-string/jumbo v1, "search_car_position"

    goto :goto_0

    .line 562
    :cond_2
    const-string/jumbo v1, "search_car_position"

    goto :goto_0
.end method


# virtual methods
.method public disconnected()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 171
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    .line 172
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    .line 174
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->recreateMapView()V

    .line 176
    return-void
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 635
    const v0, 0x7f030004

    return v0
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 630
    const/4 v0, 0x0

    return-object v0
.end method

.method protected hasSimCard(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 162
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 641
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onActivityResult start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "requestCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " resultCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    packed-switch p1, :pswitch_data_0

    .line 659
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onActivityResult end"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 661
    return-void

    .line 645
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 647
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->TAG:Ljava/lang/String;

    .line 648
    const-string/jumbo v1, "onActivityResult [REQUEST_LOAD_SIGN_IN_SCREEN] Sign in SUCCESS"

    .line 647
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 652
    const-string/jumbo v1, "PREF_ACCEPT_DISCLAIMER_PLACE_ON"

    .line 653
    const/4 v2, 0x0

    .line 651
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v0

    .line 653
    if-nez v0, :cond_0

    .line 654
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->disclaimer()V

    goto :goto_0

    .line 643
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClickListener(Z)V
    .locals 0
    .param p1, "accept"    # Z

    .prologue
    .line 686
    if-eqz p1, :cond_0

    .line 687
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestSignInScreen(Landroid/app/Activity;)V

    .line 689
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    .line 102
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->registerConnectivityListener()V

    .line 104
    if-eqz p1, :cond_0

    .line 105
    const-string/jumbo v0, "hide_btn"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    .line 107
    const-string/jumbo v0, "dev_paired"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    .line 111
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 112
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mMyLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 116
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "save_my_car_position"

    :goto_0
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mFunction:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationTime:Ljava/util/Calendar;

    if-nez v0, :cond_1

    .line 121
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mCarLocationTime:Ljava/util/Calendar;

    .line 123
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->requestProcess()V

    .line 126
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->prepareParameters()V

    .line 127
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 129
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->createView()V

    .line 131
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V

    .line 153
    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3a98

    .line 131
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 155
    return-void

    .line 117
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->validFunctionParam(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 737
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onDestroy()V

    .line 738
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 340
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onPause()V

    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsVisible:Z

    .line 342
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 333
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onResume()V

    .line 334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsVisible:Z

    .line 335
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->refreshView()V

    .line 336
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 346
    const-string/jumbo v0, "hide_btn"

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 347
    const-string/jumbo v0, "dev_paired"

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 348
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 349
    return-void
.end method

.method public paired()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 184
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mIsCarConnected:Z

    .line 185
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mHideSaveBtn:Z

    .line 187
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->recreateMapView()V

    .line 188
    return-void
.end method

.method protected setDayMode()V
    .locals 3

    .prologue
    .line 716
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setDayMode()V

    .line 718
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->btnImgBack:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->btnImgBack:Landroid/widget/ImageView;

    const v1, 0x7f020046

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 722
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->tvTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 723
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 726
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->barLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 727
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->barLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f02000a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 730
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mainLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 731
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mainLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080016

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 733
    :cond_3
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    .line 693
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->setNightMode()V

    .line 695
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->btnImgBack:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->btnImgBack:Landroid/widget/ImageView;

    const v1, 0x7f020047

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->tvTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 701
    const v2, 0x7f080019

    .line 700
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 704
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->barLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 705
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->barLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f02000c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 708
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mainLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 709
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->mainLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 712
    :cond_3
    return-void
.end method
