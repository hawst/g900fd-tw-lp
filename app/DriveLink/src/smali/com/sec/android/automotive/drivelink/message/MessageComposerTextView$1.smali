.class Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;
.super Ljava/lang/Object;
.source "MessageComposerTextView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnContactChoice(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "displayedChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method public OnContactTypeInfo(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "ContactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 144
    return-void
.end method

.method public OnInboxChoice(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    .local p1, "mSearchedInboxList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    return-void
.end method

.method public OnInboxSearchChoice(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "mSearchedInboxList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    return-void
.end method

.method public OnInputTextUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->mTextView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 139
    return-void
.end method
