.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestCheckUpdateVersion.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 14
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->mContext:Landroid/content/Context;

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->mResult:Z

    .line 16
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->mContext:Landroid/content/Context;

    .line 17
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getUpdateManager()Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCheckUpdateVersion(Landroid/content/Context;)Z

    move-result v0

    .line 21
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->mResult:Z

    .line 23
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkUpdateListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    move-result-object v0

    .line 29
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;
    if-eqz v0, :cond_0

    .line 30
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCheckUpdateVersion;->mResult:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;->onResponseRequestCheckUpdateVersion(Z)V

    .line 32
    :cond_0
    return-void
.end method
