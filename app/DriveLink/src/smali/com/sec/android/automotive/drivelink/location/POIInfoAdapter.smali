.class public Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "POIInfoAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final layoutId:I

.field private final points:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "layoutId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p2, "points":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->context:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->points:Ljava/util/List;

    .line 39
    iput p3, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->layoutId:I

    .line 40
    return-void
.end method

.method private static shouldShowSeparator(Ljava/lang/String;)Z
    .locals 1
    .param p0, "info"    # Ljava/lang/String;

    .prologue
    .line 99
    if-eqz p0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    return v0
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 114
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 115
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->points:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 124
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 129
    const/4 v0, -0x1

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 48
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;

    move-result-object v2

    .line 50
    .local v2, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;
    if-nez p2, :cond_2

    .line 51
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->context:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 52
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget v4, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->layoutId:I

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 54
    .local v3, "row":Landroid/view/View;
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;

    invoke-direct {v0, v5}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;)V

    .line 55
    .local v0, "holder":Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;
    const v4, 0x7f0901d8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->setTitle(Landroid/widget/TextView;)V

    .line 57
    const v4, 0x7f0901d9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 56
    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->setAddress(Landroid/widget/TextView;)V

    .line 58
    const v4, 0x7f0901da

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->setInfo1(Landroid/widget/TextView;)V

    .line 59
    const v4, 0x7f0901dc

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->setInfo2(Landroid/widget/TextView;)V

    .line 60
    const v4, 0x7f0901dd

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->setDistance(Landroid/widget/TextView;)V

    .line 61
    const v4, 0x7f0901db

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->setSeparator1(Landroid/view/View;)V

    .line 62
    const v4, 0x7f0901de

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->setSeparator2(Landroid/view/View;)V

    .line 64
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 71
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getTitle()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getAddress()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getInfo1()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;->getInfo1()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getInfo2()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;->getInfo2()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getDistance()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;->getInfo3()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getSeparator1()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 80
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;->getInfo1()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->shouldShowSeparator(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 81
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getSeparator1()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 87
    :cond_0
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getSeparator2()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 88
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;->getInfo2()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->shouldShowSeparator(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 89
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getSeparator2()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 95
    :cond_1
    :goto_2
    return-object v3

    .line 67
    .end local v0    # "holder":Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;
    .end local v3    # "row":Landroid/view/View;
    :cond_2
    move-object v3, p2

    .line 68
    .restart local v3    # "row":Landroid/view/View;
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;

    .restart local v0    # "holder":Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;
    goto :goto_0

    .line 83
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getSeparator1()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 91
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->getSeparator2()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 105
    const/4 v1, 0x0

    invoke-virtual {p0, p2, v1, p1}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 106
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 108
    return-object v0
.end method

.method public bridge synthetic instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 154
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
