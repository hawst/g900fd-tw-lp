.class public Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
.super Landroid/app/Dialog;
.source "HomeListViewDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ListViewDialog"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsDriving:Z

.field private mListView:Landroid/widget/ListView;

.field private mListener:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;

.field private mStrListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "isDriving"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->requestWindowFeature(I)Z

    .line 36
    const v0, 0x7f03004c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->setContentView(I)V

    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mContext:Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->findViews()V

    .line 39
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mStrListItem:Ljava/util/ArrayList;

    .line 40
    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mIsDriving:Z

    .line 41
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->createListViewDialog()V

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mStrListItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mIsDriving:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mListener:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;

    return-object v0
.end method

.method private createListViewDialog()V
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mContext:Landroid/content/Context;

    .line 50
    const v2, 0x7f03004b

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mStrListItem:Ljava/util/ArrayList;

    .line 49
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;Landroid/content/Context;ILjava/util/List;)V

    .line 80
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$2;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 92
    return-void
.end method

.method private findViews()V
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f0901c9

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mListView:Landroid/widget/ListView;

    .line 46
    return-void
.end method


# virtual methods
.method public onOnSetItemClickListener(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mListener:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;

    .line 96
    return-void
.end method

.method public updateList(Z)V
    .locals 2
    .param p1, "isDriving"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mIsDriving:Z

    .line 104
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mIsDriving:Z

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mContext:Landroid/content/Context;

    const v1, 0x7f0a01bf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 108
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->createListViewDialog()V

    .line 109
    return-void
.end method
