.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;
.super Ljava/lang/Object;
.source "OnDriveLinkMusicPlayerListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

.field private final synthetic val$_extra:I

.field private final synthetic val$_mp:Landroid/media/MediaPlayer;

.field private final synthetic val$_obj:Ljava/lang/Object;

.field private final synthetic val$_what:I


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Landroid/media/MediaPlayer;IILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_mp:Landroid/media/MediaPlayer;

    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_what:I

    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_extra:I

    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_obj:Ljava/lang/Object;

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_mp:Landroid/media/MediaPlayer;

    .line 142
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_what:I

    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_extra:I

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$7;->val$_obj:Ljava/lang/Object;

    .line 141
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    .line 144
    :cond_0
    return-void
.end method
