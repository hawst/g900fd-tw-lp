.class public Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager$WeightComparator;
.super Ljava/lang/Object;
.source "RecommendationManager.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "WeightComparator"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4
    .param p1, "obj1"    # Ljava/lang/Object;
    .param p2, "obj2"    # Ljava/lang/Object;

    .prologue
    .line 502
    move-object v0, p1

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    .local v0, "callLog1":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    move-object v1, p2

    .line 503
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    .line 505
    .local v1, "callLog2":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->getWeight()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->getWeight()I

    move-result v3

    sub-int/2addr v2, v3

    return v2
.end method
