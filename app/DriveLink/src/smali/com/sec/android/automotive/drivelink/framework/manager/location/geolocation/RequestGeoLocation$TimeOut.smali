.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;
.super Landroid/os/AsyncTask;
.source "RequestGeoLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TimeOut"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;->doInBackground([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Long;

    .prologue
    .line 302
    const-string/jumbo v2, "RequestGeoLocation"

    const-string/jumbo v3, "TimeOut doInBackground(Long... params)"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const-wide/32 v2, 0xc350

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 305
    .local v1, "param":Ljava/lang/Long;
    if-eqz p1, :cond_0

    array-length v2, p1

    if-lez v2, :cond_0

    .line 306
    const/4 v2, 0x0

    aget-object v1, p1, v2

    .line 309
    :cond_0
    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 318
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 320
    const-string/jumbo v0, "RequestGeoLocation"

    const-string/jumbo v1, "TimeOut on get current location."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$TimeOut;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->currentLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;)Landroid/location/Location;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->doResponseRequestGeoLocation(Landroid/location/Location;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;Landroid/location/Location;Z)V

    .line 327
    return-void
.end method
