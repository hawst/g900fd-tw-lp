.class public final Lcom/sec/android/automotive/drivelink/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adUnitId:I = 0x1

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd

.field public static final QuotationMarksButton:[I

.field public static final QuotationMarksButton_background:I = 0x1

.field public static final QuotationMarksButton_btn_image:I = 0x4

.field public static final QuotationMarksButton_mark_show:I = 0x3

.field public static final QuotationMarksButton_orientation:I = 0x0

.field public static final QuotationMarksButton_style_type:I = 0x2

.field public static final QuotationMarksButton_txt_color:I = 0x7

.field public static final QuotationMarksButton_txt_color_selector:I = 0x8

.field public static final QuotationMarksButton_txt_string:I = 0x5

.field public static final QuotationMarksButton_txt_style:I = 0x6

.field public static final VoiceLocationActionBarLayout:[I

.field public static final VoiceLocationActionBarLayout_black_style:I = 0x0

.field public static final VoiceLocationActionBarLayout_small_mode:I = 0x1

.field public static final VoiceMusicPlayerActionBarLayout:[I

.field public static final VoiceMusicPlayerActionBarLayout_show_prev_btn:I = 0x0

.field public static final VoiceMusicPlayerActionBarLayout_show_search_btn:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 10269
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/automotive/drivelink/R$styleable;->AdsAttrs:[I

    .line 10344
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/automotive/drivelink/R$styleable;->MapAttrs:[I

    .line 10578
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/automotive/drivelink/R$styleable;->QuotationMarksButton:[I

    .line 10722
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/automotive/drivelink/R$styleable;->VoiceLocationActionBarLayout:[I

    .line 10765
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/automotive/drivelink/R$styleable;->VoiceMusicPlayerActionBarLayout:[I

    .line 10795
    return-void

    .line 10269
    nop

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
    .end array-data

    .line 10344
    :array_1
    .array-data 4
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
    .end array-data

    .line 10578
    :array_2
    .array-data 4
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
    .end array-data

    .line 10722
    :array_3
    .array-data 4
        0x7f01001b
        0x7f01001c
    .end array-data

    .line 10765
    :array_4
    .array-data 4
        0x7f010019
        0x7f01001a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
