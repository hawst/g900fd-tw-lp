.class public Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;
.super Ljava/lang/Object;
.source "PhoneNumberOrderTypeComparator.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final TYPE_ORDER_FAX_HOME:I = 0x4

.field static final TYPE_ORDER_FAX_WORK:I = 0x5

.field static final TYPE_ORDER_HOME:I = 0x2

.field static final TYPE_ORDER_MOBILE:I = 0x1

.field static final TYPE_ORDER_ORTHERS:I = 0x14

.field static final TYPE_ORDER_WORK:I = 0x3

.field private static final serialVersionUID:J = -0x1f802103ae5bd784L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getPhoneOrderType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)I
    .locals 1
    .param p1, "phone"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 61
    const/16 v0, 0x14

    :goto_0
    return v0

    .line 51
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 53
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 55
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 57
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 59
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public compare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)I
    .locals 2
    .param p1, "phone1"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    .param p2, "phone2"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;->getPhoneOrderType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;->getPhoneOrderType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 33
    const/4 v0, -0x1

    .line 37
    :goto_0
    return v0

    .line 34
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;->getPhoneOrderType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;->getPhoneOrderType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 35
    const/4 v0, 0x1

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    check-cast p2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;->compare(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;)I

    move-result v0

    return v0
.end method
