.class public Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "NotificationActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationActivity]"

.field private static isFinished:Z

.field private static mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

.field public static mNoti:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

.field private static nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

.field private static sFlowId:Ljava/lang/String;


# instance fields
.field bEndAnimationStarted:Z

.field private mAppearAnimation:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mDisappearAnimation:Landroid/view/animation/Animation;

.field private mLayoutNotification:Landroid/widget/LinearLayout;

.field private mLcdOffReceiver:Landroid/content/BroadcastReceiver;

.field private mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

.field protected mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

.field private mNotificationAppearListener:Landroid/view/animation/Animation$AnimationListener;

.field private mNotificationDisappearListener:Landroid/view/animation/Animation$AnimationListener;

.field private mNotificationDisappearListenerWithCommand:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    sput-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNoti:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 36
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->isFinished:Z

    .line 37
    sput-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 38
    sput-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .line 39
    sput-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->sFlowId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    .line 45
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    .line 475
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    .line 517
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotificationAppearListener:Landroid/view/animation/Animation$AnimationListener;

    .line 550
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    .line 552
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotificationDisappearListener:Landroid/view/animation/Animation$AnimationListener;

    .line 578
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotificationDisappearListenerWithCommand:Landroid/view/animation/Animation$AnimationListener;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 0

    .prologue
    .line 425
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 0

    .prologue
    .line 38
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->setFlowId(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Z)V
    .locals 0

    .prologue
    .line 390
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotification(Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finishNotiActivity()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 0

    .prologue
    .line 445
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    return-void
.end method

.method static synthetic access$8()Lcom/sec/android/automotive/drivelink/notification/INotiCommand;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private addNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 614
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 619
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 620
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Add Noti. Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 2
    .param p1, "noti"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "hasCommand"    # Z

    .prologue
    .line 426
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 427
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 431
    const v0, 0x7f04002d

    .line 430
    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    .line 432
    if-eqz p2, :cond_1

    .line 433
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    .line 434
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotificationDisappearListenerWithCommand:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 439
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 441
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setVisibility(I)V

    .line 443
    :cond_0
    return-void

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    .line 437
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotificationDisappearListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method private finishNotiActivity()V
    .locals 2

    .prologue
    .line 289
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "finishNotiActivity"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 293
    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->unregisterOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finish()V

    .line 295
    return-void
.end method

.method private notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 5
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
    .param p2, "notiView"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 449
    if-nez p2, :cond_1

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    move-object v0, p2

    .line 455
    .local v0, "changeListener":Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;
    :try_start_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_2

    .line 456
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillAppear()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 467
    :catch_0
    move-exception v1

    .line 468
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    const-string/jumbo v2, "[NotificationActivity]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "This NotificationView("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 469
    const-string/jumbo v4, ") does not support Change Listener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 468
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0

    .line 457
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_3

    .line 458
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationAppeared()V

    goto :goto_0

    .line 459
    :cond_3
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_4

    .line 460
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillDisappear()V

    goto :goto_0

    .line 461
    :cond_4
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_5

    .line 462
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationDisappeared()V

    goto :goto_0

    .line 463
    :cond_5
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ROTATING:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_0

    .line 464
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationRotating()V
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private removeNotiViewForRotating()V
    .locals 4

    .prologue
    .line 408
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 412
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 413
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ROTATING:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 423
    :cond_0
    :goto_1
    return-void

    .line 414
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 419
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string/jumbo v1, "[NotificationActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "ERROR!! notiView\'s parent:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 420
    const-string/jumbo v3, ", Layout:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 419
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 625
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 631
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Remove Noti. Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private removeNotification(Z)V
    .locals 4
    .param p1, "shouldCheckNewNoti"    # Z

    .prologue
    const/4 v3, 0x0

    .line 391
    const-string/jumbo v0, "[NotificationActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "removeNotification: check New Noti? "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->cancelNotiFlow()V

    .line 394
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 395
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 397
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 396
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 398
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 399
    sput-object v3, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->sFlowId:Ljava/lang/String;

    .line 400
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->checkNotification(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 401
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "no more notification exist! finish"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finishNotiActivity()V

    .line 405
    :cond_0
    return-void
.end method

.method private setFlowId(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 644
    sput-object p1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->sFlowId:Ljava/lang/String;

    .line 645
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 646
    return-void
.end method


# virtual methods
.method protected checkNotification(Z)Z
    .locals 6
    .param p1, "bAnimation"    # Z

    .prologue
    const/4 v5, 0x0

    .line 317
    const/4 v0, 0x0

    .line 318
    .local v0, "bNotified":Z
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    if-nez v2, :cond_0

    .line 319
    const-string/jumbo v2, "[NotificationActivity]"

    const-string/jumbo v3, "Notification Layout is Null!!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v0

    .line 387
    :goto_0
    return v2

    .line 323
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 325
    :try_start_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 326
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 327
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 328
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 334
    :cond_1
    :goto_1
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    .line 335
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationView()Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v2

    .line 334
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 337
    const-string/jumbo v2, "[NotificationActivity]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "NotiView:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_8

    .line 340
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 341
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v2

    const/16 v3, 0xe

    if-eq v2, v3, :cond_3

    .line 342
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v2

    const/16 v3, 0xf

    if-ne v2, v3, :cond_4

    .line 343
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    .line 344
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    .line 343
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 345
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 346
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->checkNotification(Z)Z

    move-result v2

    goto/16 :goto_0

    .line 329
    :catch_0
    move-exception v1

    .line 330
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 350
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    if-nez v2, :cond_5

    .line 351
    const v2, 0x7f0900e9

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    .line 353
    :cond_5
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setOnNotificationRequestListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;)V

    .line 355
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 357
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 360
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 361
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_6

    .line 362
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 365
    :cond_6
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 366
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v2

    .line 367
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V

    .line 369
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->isAppeared()Z

    move-result v2

    if-nez v2, :cond_9

    .line 370
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    if-nez v2, :cond_7

    .line 372
    const v2, 0x7f04002b

    .line 371
    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    .line 373
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    .line 374
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotificationAppearListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 375
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 376
    const-string/jumbo v2, "[NotificationActivity]"

    const-string/jumbo v3, "Noti Appear Animation will start."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_7
    :goto_2
    const/4 v0, 0x1

    :cond_8
    move v2, v0

    .line 387
    goto/16 :goto_0

    .line 379
    :cond_9
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->resumeNotiFlow()V

    goto :goto_2
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    if-nez v0, :cond_0

    .line 283
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Back button pressed."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    .line 286
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mContext:Landroid/content/Context;

    .line 81
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNoti:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 82
    const v0, 0x7f030022

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->setContentView(I)V

    .line 84
    const v0, 0x7f0900e9

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mIsSkipVoiceResume:Z

    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 88
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Notification Layout is null! finish"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finishNotiActivity()V

    .line 92
    :cond_0
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->isFinished:Z

    if-eqz v0, :cond_1

    .line 101
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->resetIsPrepareNotiActivity()V

    .line 102
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->isFinished:Z

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    .line 106
    const-string/jumbo v2, "android.intent.action.SCREEN_OFF"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 107
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 210
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mIsSkipVoiceResume:Z

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Finishing..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotification(Z)V

    .line 218
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 219
    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->unregisterOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 232
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    if-eqz v0, :cond_0

    .line 233
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 234
    invoke-interface {v0, v2}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V

    .line 235
    sput-object v3, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 238
    :cond_0
    sput-object v3, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->sFlowId:Ljava/lang/String;

    .line 240
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->isFinished:Z

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 244
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 246
    sput-object v3, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNoti:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 247
    return-void
.end method

.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 717
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandCall(Ljava/lang/String;)V

    .line 720
    :cond_0
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 750
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandCancel(Ljava/lang/String;)V

    .line 753
    :cond_0
    return-void
.end method

.method public onFlowCommandCustom(Ljava/lang/String;)Z
    .locals 1
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 738
    const/4 v0, 0x0

    return v0
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandExcute(Ljava/lang/String;)V

    .line 678
    :cond_0
    return-void
.end method

.method public onFlowCommandIgnore(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 682
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandIgnore(Ljava/lang/String;)V

    .line 685
    :cond_0
    return-void
.end method

.method public onFlowCommandLookup(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 689
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandLookup(Ljava/lang/String;)V

    .line 692
    :cond_0
    return-void
.end method

.method public onFlowCommandNext(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 731
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandNext(Ljava/lang/String;)V

    .line 734
    :cond_0
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 661
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandNo(Ljava/lang/String;)V

    .line 664
    :cond_0
    return-void
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 724
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 725
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandRead(Ljava/lang/String;)V

    .line 727
    :cond_0
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReply(Ljava/lang/String;)V

    .line 713
    :cond_0
    return-void
.end method

.method public onFlowCommandReset(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReset(Ljava/lang/String;)V

    .line 699
    :cond_0
    return-void
.end method

.method public onFlowCommandReturnMain()V
    .locals 1

    .prologue
    .line 743
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 744
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandReturnMain()V

    .line 746
    :cond_0
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 703
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandRoute(Ljava/lang/String;)V

    .line 706
    :cond_0
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 668
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandSend(Ljava/lang/String;)V

    .line 671
    :cond_0
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 654
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onFlowCommandYes(Ljava/lang/String;)V

    .line 657
    :cond_0
    return-void
.end method

.method public onNewNotification(Z)V
    .locals 2
    .param p1, "animation"    # Z

    .prologue
    .line 303
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "New Notification Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 306
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "higher priority noti is received."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->pauseNotiFlow()V

    .line 310
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->checkNotification(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 311
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "New Notification doesn\'t exist. finish"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finishNotiActivity()V

    .line 314
    :cond_1
    return-void
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 165
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPause()V

    .line 166
    const-string/jumbo v0, "[NotificationActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPause:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mIsRotation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Appear Animation is Canceled."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 170
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mAppearAnimation:Landroid/view/animation/Animation;

    .line 174
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 175
    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->unregisterOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    .line 179
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v0

    .line 178
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 181
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    if-eqz v0, :cond_1

    .line 182
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getmAFHighPriority()Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "onPlaybackRequest PLAYBACK_REQUEST_RESUME"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 185
    invoke-interface {v0, v4}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V

    .line 186
    sput-object v3, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 200
    :cond_1
    :goto_0
    return-void

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 191
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotification(Z)V

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    .line 193
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mDisappearAnimation:Landroid/view/animation/Animation;

    goto :goto_0

    .line 195
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotiViewForRotating()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 117
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 118
    const-string/jumbo v1, "[NotificationActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onResume:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mIsRotation:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->checkNotification(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    const-string/jumbo v1, "[NotificationActivity]"

    const-string/jumbo v2, "No Notificatino. exit Noti. Acitivity. finish"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->finishNotiActivity()V

    .line 124
    :cond_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->sFlowId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 125
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->sFlowId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->setFlowId(Ljava/lang/String;)V

    .line 129
    :cond_1
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    .line 130
    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->registerOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 132
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->addNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 135
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 136
    const-string/jumbo v1, "[NotificationActivity]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "MUSIC is Playing : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    sput-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 139
    const-string/jumbo v1, "[NotificationActivity]"

    const-string/jumbo v2, "onPlaybackRequest PLAYBACK_REQUEST_PAUSE"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 141
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v1, :cond_3

    .line 148
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->resumeNotiFlow()V

    .line 151
    :cond_3
    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 257
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    .line 258
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 259
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v2

    .line 260
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getTop()I

    move-result v3

    .line 261
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getRight()I

    move-result v4

    .line 262
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mLayoutNotification:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v5

    .line 259
    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 263
    .local v1, "r":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 264
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 263
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    .line 266
    .local v0, "bTouchInside":Z
    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    if-nez v2, :cond_0

    .line 267
    const-string/jumbo v2, "[NotificationActivity]"

    const-string/jumbo v3, "Outside of noti is touched."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    .line 272
    .end local v0    # "bTouchInside":Z
    .end local v1    # "r":Landroid/graphics/Rect;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 155
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "onUserLeaveHint"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->pauseNotiFlow()V

    .line 159
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onUserLeaveHint()V

    .line 160
    return-void
.end method

.method protected reloadLayout()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method
