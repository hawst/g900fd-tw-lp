.class public Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "LocationRequestWaitActivity.java"


# instance fields
.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private mBtnCancel:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const v0, 0x7f030015

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->setContentView(I)V

    .line 25
    const v0, 0x7f090098

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->mBtnCancel:Landroid/widget/Button;

    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->mBtnCancel:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v0, 0x7f0900a3

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 37
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v1, 0x7f0a0210

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTitleBar(I)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 48
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 53
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestWaitActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 54
    return-void
.end method
