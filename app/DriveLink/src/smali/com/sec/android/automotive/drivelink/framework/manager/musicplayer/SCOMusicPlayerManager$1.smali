.class Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;
.super Landroid/os/Handler;
.source "SCOMusicPlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    .line 22
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 24
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 51
    :goto_0
    return-void

    .line 26
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mSCOCheckTryCount:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)I

    move-result v1

    const/16 v2, 0xf

    if-ge v1, v2, :cond_0

    .line 28
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 29
    .local v0, "newMsg":Landroid/os/Message;
    iget v1, p1, Landroid/os/Message;->what:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 30
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 31
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mBTSCOCheckHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)Landroid/os/Handler;

    move-result-object v1

    .line 32
    const-wide/16 v2, 0xc8

    .line 31
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 37
    .end local v0    # "newMsg":Landroid/os/Message;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    iget v2, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->startPlayerWithPlayType(I)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;I)V

    .line 39
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;I)V

    .line 40
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;Z)V

    goto :goto_0

    .line 43
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    iget v2, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->startPlayerWithPlayType(I)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;I)V

    .line 45
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;I)V

    .line 46
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;Z)V

    goto :goto_0

    .line 24
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
