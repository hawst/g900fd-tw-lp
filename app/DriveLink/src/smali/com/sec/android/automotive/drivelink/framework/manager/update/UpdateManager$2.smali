.class Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$2;
.super Ljava/lang/Object;
.source "UpdateManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstallCall(I)V
    .locals 3
    .param p1, "arg1"    # I

    .prologue
    .line 81
    const-string/jumbo v0, "UpdateManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "returnCode"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_INSTALL_RESULT:Ljava/lang/String;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;Ljava/lang/String;Z)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendInstallStart()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->printtestcode()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V

    .line 88
    return-void
.end method
