.class public Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "MessageComposerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final DEBUG_LEVEL:I = 0x5

.field static final DEBUG_VALUE:[Ljava/lang/String;

.field private static final ERROR_STRING:Ljava/lang/String; = "ERROR_STRING"

.field private static final IS_ERROR_STATE:Ljava/lang/String; = "IS_ERROR_STATE"

.field private static final IS_TTS_STATE:Ljava/lang/String; = "IS_TTS_STATE"

.field private static final TAG:Ljava/lang/String; = "MessageComposerActivity"

.field private static isDebug:Z

.field private static mIndex:I


# instance fields
.field private final COMP_IME:Ljava/lang/String;

.field private final COMP_IME_UNKNOWN:Ljava/lang/String;

.field private final COMP_MESSAGE:Ljava/lang/String;

.field private final COMP_NAME:Ljava/lang/String;

.field private final COMP_NUMBER:Ljava/lang/String;

.field private final COMP_NUMBERSIZE:Ljava/lang/String;

.field private abnormalSIP:Z

.field private getAdjustSizeListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

.field private isNewintent:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

.field private mBtnCompleteLayout:Landroid/widget/LinearLayout;

.field private mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field mComposerMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mContactName:Landroid/widget/TextView;

.field mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

.field private mDrivingMode:Z

.field private mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

.field private mNumberListSize:I

.field private mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

.field private mPhoneNumber:Ljava/lang/String;

.field private mPowerReceiver:Landroid/content/BroadcastReceiver;

.field private mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

.field private mSearchBtn:Landroid/widget/ImageButton;

.field private mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field mWatcher:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    sput v2, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mIndex:I

    .line 107
    sput-boolean v3, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isDebug:Z

    .line 111
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "a"

    aput-object v1, v0, v2

    const-string/jumbo v1, "e"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string/jumbo v2, "w"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "i"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "v"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->DEBUG_VALUE:[Ljava/lang/String;

    .line 112
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 75
    const-string/jumbo v0, "COMP_MESSAGE"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->COMP_MESSAGE:Ljava/lang/String;

    .line 76
    const-string/jumbo v0, "COMP_NAME"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->COMP_NAME:Ljava/lang/String;

    .line 77
    const-string/jumbo v0, "COMP_NUMBER"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->COMP_NUMBER:Ljava/lang/String;

    .line 78
    const-string/jumbo v0, "COMP_NUMBERSIZE"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->COMP_NUMBERSIZE:Ljava/lang/String;

    .line 79
    const-string/jumbo v0, "COMP_IME"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->COMP_IME:Ljava/lang/String;

    .line 80
    const-string/jumbo v0, "COMP_IME_UNKNOWN"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->COMP_IME_UNKNOWN:Ljava/lang/String;

    .line 97
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    .line 100
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    .line 103
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mNumberListSize:I

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDrivingMode:Z

    .line 649
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mWatcher:Landroid/text/TextWatcher;

    .line 698
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isNewintent:Z

    .line 812
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    .line 935
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getAdjustSizeListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    .line 1036
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mComposerMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 1254
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 70
    return-void
.end method

.method private static Debug(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "level"    # I
    .param p1, "sub"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 118
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isDebug:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    if-ge p0, v0, :cond_0

    .line 119
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->DEBUG_VALUE:[Ljava/lang/String;

    aget-object v0, v0, p0

    const-string/jumbo v1, "MessageComposerActivity"

    invoke-static {v0, v1, p1, p2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V
    .locals 0

    .prologue
    .line 396
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setMessageComposerTextViewBtn(Z)V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDrivingMode:Z

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDensity:F

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOrientation:I

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V
    .locals 0

    .prologue
    .line 1289
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setSharedPreferences(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;I)V
    .locals 0

    .prologue
    .line 1309
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Z
    .locals 1

    .prologue
    .line 698
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isNewintent:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V
    .locals 0

    .prologue
    .line 698
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isNewintent:Z

    return-void
.end method

.method static synthetic access$6()I
    .locals 1

    .prologue
    .line 98
    sget v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mIndex:I

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    invoke-static {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private getComposerSendClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1021
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    return-object v0
.end method

.method private getComposerSipStateListener()Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;
    .locals 1

    .prologue
    .line 846
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    return-object v0
.end method

.method private getComposerTextClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 988
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 428
    const/4 v1, 0x3

    const-string/jumbo v2, "init"

    const-string/jumbo v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 431
    const v1, 0x7f0900bd

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 432
    const v1, 0x7f0900c0

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 433
    const v1, 0x7f0900c1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 434
    const v1, 0x7f0900bf

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 436
    const v1, 0x7f0900bb

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 438
    .local v0, "messageEditlayout":Landroid/widget/RelativeLayout;
    const v1, 0x7f0900bc

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    .line 439
    const v1, 0x7f0900be

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mBtnCompleteLayout:Landroid/widget/LinearLayout;

    .line 440
    const v1, 0x7f0900ba

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    .line 442
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 443
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 444
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->bringToFront()V

    .line 445
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->invalidate()V

    .line 448
    const v1, 0x7f090364

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    .line 449
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    .line 450
    const v2, 0x7f09035c

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 449
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSearchBtn:Landroid/widget/ImageButton;

    .line 470
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    .line 471
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getComposerSipStateListener()Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setOnComposerSipStateListener(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;)V

    .line 473
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSearchBtn:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getComposerTextClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    .line 475
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getComposerTextClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setOnComposerClickListener(Landroid/view/View$OnClickListener;)V

    .line 480
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 482
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 483
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 485
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 487
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 494
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getComposerSendClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setOnSendBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 496
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    .line 497
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mComposerMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setOnComposerMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V

    .line 498
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 503
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 504
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setInitUI(Landroid/content/Intent;)V

    .line 505
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isNewintent:Z

    .line 508
    :cond_0
    return-void
.end method

.method private removeMsgComposerFlowID()V
    .locals 1

    .prologue
    .line 1166
    const-string/jumbo v0, "DM_SMS_COMPOSING"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->removeFlowList(Ljava/lang/String;)V

    .line 1167
    const-string/jumbo v0, "DM_SMS_COMPOSE"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->removeFlowList(Ljava/lang/String;)V

    .line 1168
    const-string/jumbo v0, "DM_SMS_COMPLETE"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->removeFlowList(Ljava/lang/String;)V

    .line 1169
    return-void
.end method

.method private setInitUI(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x3

    .line 713
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 766
    :cond_0
    :goto_0
    return-void

    .line 717
    :cond_1
    const-string/jumbo v5, "setInitUI"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "action = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 718
    const/4 v2, 0x0

    .line 719
    .local v2, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string/jumbo v6, "EXTRA_FLOW_ID"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 720
    .local v1, "flowID":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 722
    if-eqz v2, :cond_0

    .line 725
    const-string/jumbo v5, "setInitUI"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "flow ID :: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 726
    const-string/jumbo v5, "DM_SMS_COMPOSE"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 727
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v0, v5, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 728
    .local v0, "contactName":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 729
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v0, v5, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 731
    :cond_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 733
    const-string/jumbo v5, "setInitUI"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "contactName :: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 735
    const-string/jumbo v3, ""

    .line 736
    .local v3, "mVoiceMsgText":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 738
    .end local v0    # "contactName":Ljava/lang/String;
    .end local v3    # "mVoiceMsgText":Ljava/lang/String;
    :cond_3
    const-string/jumbo v5, "DM_SMS_COMPOSING"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 740
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v4

    .line 742
    .local v4, "strMsg":Ljava/lang/String;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 743
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setText(Ljava/lang/CharSequence;)V

    .line 746
    :cond_4
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v5, v9}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 747
    const/4 v5, 0x4

    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V

    goto/16 :goto_0

    .line 748
    .end local v4    # "strMsg":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "DM_SMS_COMPLETE"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 750
    const-string/jumbo v5, "setInitUI"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "MessageText = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    iget-object v6, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 761
    :cond_6
    const-string/jumbo v5, "DM_SMS_SEND"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 762
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->sendSMS()V

    goto/16 :goto_0
.end method

.method private setMessageComposerTextViewBtn(Z)V
    .locals 5
    .param p1, "display"    # Z

    .prologue
    const/16 v4, 0x8

    .line 398
    const/4 v0, 0x5

    const-string/jumbo v1, "setMessageComposerTextViewBtn"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, " display = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 399
    if-eqz p1, :cond_1

    .line 401
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 407
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    .line 415
    const-wide/16 v2, 0x64

    .line 407
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 425
    :goto_0
    return-void

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 419
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V

    goto :goto_0

    .line 422
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 423
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V

    goto :goto_0
.end method

.method private setSharedPreferences(Z)V
    .locals 6
    .param p1, "UnKnown"    # Z

    .prologue
    const/4 v5, 0x1

    .line 1290
    const-string/jumbo v2, "setSharedPreferences"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "UnKnown="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1292
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 1293
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "MessageComposerActivity"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1294
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1296
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    .line 1297
    const-string/jumbo v2, "COMP_MESSAGE"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1298
    const-string/jumbo v2, "COMP_NAME"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 1299
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1298
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1300
    const-string/jumbo v2, "COMP_NUMBER"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1301
    const-string/jumbo v2, "COMP_NUMBERSIZE"

    iget v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mNumberListSize:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1303
    const-string/jumbo v2, "COMP_IME"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1305
    :cond_0
    const-string/jumbo v2, "COMP_IME_UNKNOWN"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1306
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1307
    return-void
.end method

.method private setStateCompleteLayout(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mBtnCompleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    if-eq v0, v1, :cond_0

    .line 1315
    if-nez p1, :cond_1

    .line 1316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setContentAreaHeightbyReplace(Z)V

    .line 1320
    :cond_0
    :goto_0
    return-void

    .line 1318
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setContentAreaHeightbyReplace(Z)V

    goto :goto_0
.end method


# virtual methods
.method CancelMessageFlow(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1139
    const-string/jumbo v0, "MessageComposerActivity"

    const-string/jumbo v1, "CancelMessageFlow(String flowID) finish"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->removeMsgComposerFlowID()V

    .line 1156
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->finish()V

    .line 1163
    return-void
.end method

.method CompleteMessageFlow(Ljava/lang/String;)V
    .locals 3
    .param p1, "strText"    # Ljava/lang/String;

    .prologue
    .line 1115
    const-string/jumbo v2, "DM_SMS_COMPOSE"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 1116
    .local v0, "curFlowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1117
    .local v1, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v0, :cond_0

    .line 1118
    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1119
    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    iput v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 1120
    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    iput v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 1122
    :cond_0
    iput-object p1, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 1123
    const-string/jumbo v2, "DM_SMS_COMPLETE"

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1125
    return-void
.end method

.method ReplaceMessageFlow()V
    .locals 4

    .prologue
    .line 1128
    const/4 v1, 0x3

    const-string/jumbo v2, "ReplaceMessageFlow"

    const-string/jumbo v3, "ReplaceMessageFlow "

    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1129
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setReplaceText()V

    .line 1131
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1132
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_SMS_COMPOSING"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1135
    return-void
.end method

.method SendMessageFlow()V
    .locals 2

    .prologue
    .line 1108
    const-string/jumbo v0, "DM_SMS_SEND"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1110
    return-void
.end method

.method backMainActivity()V
    .locals 2

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->hideInputMethod()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1089
    const-string/jumbo v0, "MessageComposerActivity"

    const-string/jumbo v1, "!mMessageComposerTextView.hideInputMethod() finish"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setOnComposerSipStateListener(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$OnComposerSipStateListener;)V

    .line 1102
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->removeMsgComposerFlowID()V

    .line 1103
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->finish()V

    .line 1105
    :cond_0
    return-void
.end method

.method enableQuotesButton(Z)V
    .locals 5
    .param p1, "isVisable"    # Z

    .prologue
    const v4, 0x7f0c00b7

    const v3, 0x7f0c00b6

    .line 1060
    const-string/jumbo v0, "MessageComposerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "enableQuotesButton! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 1062
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 1063
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 1064
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 1066
    if-eqz p1, :cond_0

    .line 1067
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1068
    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    .line 1069
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1070
    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    .line 1071
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1072
    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    .line 1073
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1074
    const v1, 0x7f0c00b5

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOn(I)V

    .line 1085
    :goto_0
    return-void

    .line 1076
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1077
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    .line 1078
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteCancellBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1079
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    .line 1080
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteSendBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1081
    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    .line 1082
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mCompleteReplaceBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 1083
    const v1, 0x7f0c00b4

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setButtonStyleOff(I)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 804
    const/4 v0, 0x3

    const-string/jumbo v1, "onBackPressed"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 805
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->backMainActivity()V

    .line 806
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onBackPressed()V

    .line 807
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 771
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 797
    :pswitch_0
    const/4 v0, 0x3

    const-string/jumbo v1, "onClick"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "default onClick v = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 800
    :goto_0
    return-void

    .line 774
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->backMainActivity()V

    goto :goto_0

    .line 779
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->backMainActivity()V

    goto :goto_0

    .line 786
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->SendMessageFlow()V

    goto :goto_0

    .line 793
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->ReplaceMessageFlow()V

    goto :goto_0

    .line 771
    nop

    :pswitch_data_0
    .packed-switch 0x7f0900bd
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x3

    .line 134
    const-string/jumbo v5, "onCreate"

    const-string/jumbo v6, ""

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 136
    const v5, 0x7f03001a

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setContentView(I)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 139
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-static {p0, v5}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiModeFullSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)Z

    .line 142
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v5

    .line 143
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 142
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->addDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 145
    invoke-direct {p0, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->init(Landroid/content/Context;)V

    .line 147
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->assistActivity(Landroid/app/Activity;)V

    .line 150
    if-eqz p1, :cond_2

    .line 151
    const-string/jumbo v5, "onCreate"

    const-string/jumbo v6, "savedInstanceState not null "

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string/jumbo v5, "COMP_MESSAGE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 153
    .local v3, "saveMsg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    const-string/jumbo v5, "COMP_NAME"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 155
    .local v4, "saveName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    const-string/jumbo v5, "COMP_NUMBER"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    .line 157
    const-string/jumbo v5, "COMP_NUMBERSIZE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mNumberListSize:I

    .line 158
    const-string/jumbo v5, "onCreate"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "saveName = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " mPhoneNumber = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 159
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 158
    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string/jumbo v5, "COMP_IME"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 163
    .local v1, "bfocus":Z
    const-string/jumbo v5, "onCreate"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "bfocus = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerFocusable(Z)V

    .line 165
    if-eqz v1, :cond_1

    .line 166
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    .line 167
    sget-object v6, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerStatechanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 168
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->showInputMethod()V

    .line 170
    :cond_1
    const-string/jumbo v5, "onCreate"

    .line 171
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "mSIPState = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " COMP_IME_UNKNOWN = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 172
    const-string/jumbo v7, "COMP_IME_UNKNOWN"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 171
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 170
    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    .line 174
    const-string/jumbo v6, "COMP_IME_UNKNOWN"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 173
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setUnknownSIP(Z)V

    .line 177
    .end local v1    # "bfocus":Z
    .end local v3    # "saveMsg":Ljava/lang/String;
    .end local v4    # "saveName":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v5

    .line 178
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v6, "MessageComposerActivity"

    invoke-virtual {v5, v6, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 179
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string/jumbo v5, "COMP_IME_UNKNOWN"

    invoke-interface {v2, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    .line 182
    const-string/jumbo v5, "onCreate"

    .line 183
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "abnormalSIP = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 184
    const-string/jumbo v7, "COMP_IME_UNKNOWN"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 183
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 181
    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    if-eqz v5, :cond_4

    if-nez p1, :cond_4

    .line 187
    const-string/jumbo v5, "onCreate"

    const-string/jumbo v6, "savedInstanceState is null & abnormalSIP  "

    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string/jumbo v5, "COMP_MESSAGE"

    const-string/jumbo v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 189
    .restart local v3    # "saveMsg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    const-string/jumbo v5, "COMP_NAME"

    const-string/jumbo v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 191
    .restart local v4    # "saveName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    const-string/jumbo v5, "COMP_NUMBER"

    const-string/jumbo v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    .line 193
    const-string/jumbo v5, "COMP_NUMBERSIZE"

    invoke-interface {v2, v5, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mNumberListSize:I

    .line 194
    const-string/jumbo v5, "onCreate"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "saveName = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " mPhoneNumber = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 195
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 194
    invoke-static {v8, v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string/jumbo v5, "COMP_IME"

    invoke-interface {v2, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 199
    .restart local v1    # "bfocus":Z
    if-eqz v1, :cond_3

    .line 200
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerFocusable(Z)V

    .line 201
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    .line 202
    sget-object v6, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setComposerStatechanged(Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;)V

    .line 203
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->requestFocus()Z

    .line 205
    :cond_3
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    iget-boolean v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setUnknownSIP(Z)V

    .line 206
    invoke-direct {p0, v9}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setSharedPreferences(Z)V

    .line 211
    .end local v1    # "bfocus":Z
    .end local v3    # "saveMsg":Ljava/lang/String;
    .end local v4    # "saveName":Ljava/lang/String;
    :cond_4
    if-eqz p1, :cond_5

    .line 212
    const-string/jumbo v5, "IS_ERROR_STATE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 213
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    .line 214
    const-string/jumbo v6, "ERROR_STRING"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 213
    invoke-virtual {v5, v6}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->displayError(Ljava/lang/CharSequence;)V

    .line 216
    :cond_5
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    iget v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOrientation:I

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    .line 217
    iget v8, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDensity:F

    .line 216
    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setContentAreaHeight(ILcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;F)V

    .line 218
    if-eqz p1, :cond_6

    .line 219
    const-string/jumbo v5, "IS_TTS_STATE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 220
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setTTSState()V

    .line 222
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->moveoutOverlayService()V

    .line 223
    new-instance v5, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$6;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPowerReceiver:Landroid/content/BroadcastReceiver;

    .line 234
    new-instance v0, Landroid/content/IntentFilter;

    .line 235
    const-string/jumbo v5, "android.intent.action.ACTION_POWER_DISCONNECTED"

    .line 234
    invoke-direct {v0, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 236
    .local v0, "IntentFilter":Landroid/content/IntentFilter;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPowerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 237
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 255
    const/4 v0, 0x3

    const-string/jumbo v1, "onDestroy"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->stopTTSBarAnimation()V

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    if-eqz v0, :cond_1

    .line 264
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 265
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->removeDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPowerReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPowerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 271
    :cond_2
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 280
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 281
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    if-eqz v0, :cond_0

    .line 1227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->stopTTSBarAnimation()V

    .line 1230
    :cond_0
    const/4 v0, 0x3

    const-string/jumbo v1, "onFlowCommandCancel "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1231
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->CancelMessageFlow(Ljava/lang/String;)V

    .line 1232
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1236
    const/4 v0, 0x3

    const-string/jumbo v1, "onFlowCommandNo "

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1237
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->CancelMessageFlow(Ljava/lang/String;)V

    .line 1238
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1249
    const/4 v0, 0x3

    const-string/jumbo v1, "onFlowCommandSend"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1250
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->sendSMS()V

    .line 1251
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 1242
    const/4 v0, 0x3

    const-string/jumbo v1, "onFlowCommandYes"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1243
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->sendSMS()V

    .line 1244
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 701
    const/4 v0, 0x3

    const-string/jumbo v1, "onNewIntent"

    const-string/jumbo v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 703
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 704
    if-eqz p1, :cond_0

    .line 705
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isNewintent:Z

    .line 707
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setInitUI(Landroid/content/Intent;)V

    .line 710
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 384
    const-string/jumbo v0, "onPause"

    const-string/jumbo v1, ""

    invoke-static {v2, v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    if-ne v0, v1, :cond_0

    .line 388
    const-string/jumbo v0, "onPause setUnknwonSIP "

    const-string/jumbo v1, ""

    invoke-static {v2, v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->setUnknownSIP(Z)V

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getAdjustSizeListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    if-eqz v0, :cond_1

    .line 392
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->setOnAdjustStateListener(Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;)V

    .line 393
    :cond_1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPause()V

    .line 394
    return-void
.end method

.method public onPrepareFlowChange(Ljava/lang/String;)V
    .locals 4
    .param p1, "nextFlowID"    # Ljava/lang/String;

    .prologue
    .line 1218
    const/4 v0, 0x3

    const-string/jumbo v1, "onPrepareFlowChange"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "nextFlowID ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1219
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPrepareFlowChange(Ljava/lang/String;)V

    .line 1220
    return-void
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const/4 v6, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 325
    const-string/jumbo v3, "onResume"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "mSIPState = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " abnormalSIP : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 326
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 325
    invoke-static {v6, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getAdjustSizeListener:Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround;->setOnAdjustStateListener(Lcom/sec/android/automotive/drivelink/message/AdjustResizeWorkaround$OnAdjustStateListener;)V

    .line 330
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getUnknownSIP()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    if-eqz v3, :cond_1

    .line 331
    :cond_0
    const-string/jumbo v3, "UnKnown SIP"

    const-string/jumbo v4, ""

    invoke-static {v6, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-direct {p0, v8}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setMessageComposerTextViewBtn(Z)V

    .line 334
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$7;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V

    .line 342
    const-wide/16 v5, 0x64

    .line 334
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 344
    invoke-virtual {p0, v7}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->enableQuotesButton(Z)V

    .line 346
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setInitValue()V

    .line 348
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "mFlowID":Ljava/lang/String;
    const-string/jumbo v3, "DM_SMS_COMPLETE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 350
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v2

    .line 351
    .local v2, "test":Ljava/lang/String;
    const/4 v0, 0x0

    .line 352
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_2

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 355
    const v4, 0x7f0a013d

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v7

    .line 354
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 362
    .end local v0    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v2    # "test":Ljava/lang/String;
    :cond_2
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 363
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 286
    const-string/jumbo v0, "onSaveInstanceState "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mMessageComposerTextView ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 287
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 286
    invoke-static {v4, v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string/jumbo v0, "COMP_MESSAGE"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const-string/jumbo v0, "COMP_NAME"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 291
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string/jumbo v0, "COMP_NUMBER"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string/jumbo v0, "COMP_NUMBERSIZE"

    iget v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mNumberListSize:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getUnknownSIP()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    const-string/jumbo v0, "COMP_IME"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 301
    :goto_0
    const-string/jumbo v0, "onSaveInstanceState "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " getUnknwonSIP = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 302
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getUnknownSIP()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 301
    invoke-static {v4, v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string/jumbo v0, "COMP_IME_UNKNOWN"

    .line 304
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getUnknownSIP()Z

    move-result v1

    .line 303
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 306
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isErrorState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    const-string/jumbo v0, "IS_ERROR_STATE"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 310
    const-string/jumbo v0, "ERROR_STRING"

    .line 311
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->getErrorCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 310
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->isTTSState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314
    const-string/jumbo v0, "IS_TTS_STATE"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 316
    :cond_1
    return-void

    .line 298
    :cond_2
    const-string/jumbo v0, "COMP_IME"

    .line 299
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->isComposerInputMethodShown()Z

    move-result v1

    .line 298
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected onStartResume()V
    .locals 4

    .prologue
    .line 367
    const/4 v1, 0x3

    const-string/jumbo v2, "onStartResume"

    const-string/jumbo v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v0

    .line 370
    .local v0, "flowID":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    sget-object v2, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    if-eq v1, v2, :cond_0

    .line 371
    const-string/jumbo v1, "DM_SMS_SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 372
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setMessageComposerTextViewBtn(Z)V

    .line 373
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onStartResume()V

    .line 375
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 242
    const/4 v0, 0x3

    const-string/jumbo v1, "onStop "

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, " abnormalSIP : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->abnormalSIP:Z

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setSharedPreferences(Z)V

    .line 245
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onStop()V

    .line 246
    return-void
.end method

.method public sendSMS()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 1176
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getText()Ljava/lang/String;

    move-result-object v2

    .line 1177
    .local v2, "strMessage":Ljava/lang/String;
    const-string/jumbo v3, "sendSMS"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "sendSMS mPhoneNumber ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1178
    const-string/jumbo v3, "sendSMS"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "sendSMS strMessage ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 1180
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->getListener()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1181
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasInstance()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1182
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->addPendingReq()V

    .line 1183
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;

    .line 1184
    const/4 v3, 0x1

    .line 1185
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    .line 1183
    invoke-direct {v0, v3, v4, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 1186
    .local v0, "info":Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener;->getListener()Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationSendingListener$OnMessageSendingNotiListener;->OnMessageSendingState(Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;)V

    .line 1198
    .end local v0    # "info":Lcom/sec/android/automotive/drivelink/notification/NotificationSendigInfo;
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1199
    const-string/jumbo v3, "MessageComposerActivity"

    const-string/jumbo v4, "isMultiWindow() finish"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->finish()V

    .line 1201
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1202
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x24000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1205
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 1213
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 1189
    :cond_0
    const-string/jumbo v3, "sendSMS"

    .line 1190
    const-string/jumbo v4, "sendSMS() NotificationSendingListener.getListener() == null"

    .line 1189
    invoke-static {v6, v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1210
    :cond_1
    const-string/jumbo v3, "DM_MAIN"

    const/4 v4, 0x0

    .line 1209
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_1
.end method

.method setInitValue()V
    .locals 19

    .prologue
    .line 511
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    const-string/jumbo v5, ""

    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 512
    const/4 v14, 0x0

    .local v14, "firstName":Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "lastName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 514
    .local v4, "displayName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v15

    .line 516
    .local v15, "flowID":Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v9

    .line 518
    .local v9, "composerParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v2, "DM_DIAL"

    if-eq v15, v2, :cond_9

    if-eqz v9, :cond_9

    .line 519
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v2, :cond_9

    .line 520
    iget v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    sput v2, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mIndex:I

    .line 521
    iget v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    if-gez v2, :cond_7

    const/16 v18, 0x0

    .line 523
    .local v18, "typeIndex":I
    :goto_0
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v14

    .line 524
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v16

    .line 525
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v4, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 526
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    move/from16 v0, v18

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 527
    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 526
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    .line 528
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mNumberListSize:I

    .line 530
    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 532
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 533
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-interface {v2, v0, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v1

    .line 535
    .local v1, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 536
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    .line 540
    :goto_1
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "user displayName = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 548
    .end local v1    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_0
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "displayName = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 549
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "mPhoneNumber = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 610
    .end local v18    # "typeIndex":I
    :goto_2
    const/16 v17, 0x0

    .line 611
    .local v17, "strName":Ljava/lang/String;
    if-eqz v4, :cond_e

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_e

    .line 612
    move-object/from16 v17, v4

    .line 620
    :cond_1
    :goto_3
    if-eqz v17, :cond_2

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_3

    .line 621
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 623
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 624
    const-string/jumbo v3, "country_detector"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    .line 623
    check-cast v11, Landroid/location/CountryDetector;

    .line 626
    .local v11, "detector":Landroid/location/CountryDetector;
    invoke-virtual {v11}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v2

    invoke-virtual {v2}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 625
    move-object/from16 v0, v17

    invoke-static {v0, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 627
    if-nez v17, :cond_3

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 633
    .end local v11    # "detector":Landroid/location/CountryDetector;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 637
    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v13

    .line 639
    .local v13, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mOnDriveLinkContactListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    invoke-interface {v13, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 641
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mIndex:I

    add-int/lit8 v3, v3, 0x1

    .line 640
    invoke-interface {v13, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestContactList(Landroid/content/Context;I)V

    .line 644
    .end local v13    # "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    .line 645
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 647
    .end local v17    # "strName":Ljava/lang/String;
    :cond_6
    :goto_4
    return-void

    .line 522
    :cond_7
    iget v0, v9, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    move/from16 v18, v0

    goto/16 :goto_0

    .line 538
    .restart local v1    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .restart local v18    # "typeIndex":I
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 551
    .end local v1    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v18    # "typeIndex":I
    :cond_9
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    const-string/jumbo v5, "composerParams = = null "

    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 553
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 554
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 556
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentActiviy()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    if-nez v2, :cond_a

    .line 557
    const-string/jumbo v2, "MessageComposerActivity"

    const-string/jumbo v3, "IntegratedFlowManager.getCurrentActiviy() == null finish"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->finish()V

    goto :goto_4

    .line 562
    :cond_a
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "getCurrentFlow ==  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 563
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 562
    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 564
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "getCurrentActiviy ==  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 565
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentActiviy()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 564
    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    .line 567
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 568
    const-string/jumbo v3, "userfirstname"

    .line 567
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 569
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 570
    const-string/jumbo v3, "usernumber"

    .line 569
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    .line 572
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 573
    .local v7, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    if-eqz v7, :cond_b

    .line 574
    new-instance v12, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 575
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    .line 574
    invoke-direct {v12, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 576
    .local v12, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    .end local v12    # "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    :cond_b
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 581
    .restart local v1    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v10

    .line 583
    .local v10, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v8}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 584
    .local v8, "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v10, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 585
    const-string/jumbo v2, "DM_SMS_COMPOSE"

    invoke-static {v2, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_2

    .line 603
    .end local v1    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v7    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v8    # "FlowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v10    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 604
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 606
    :cond_d
    const/4 v2, 0x3

    const-string/jumbo v3, "setInitValue"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "mPhoneNumber = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->Debug(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 614
    .restart local v17    # "strName":Ljava/lang/String;
    :cond_e
    if-eqz v14, :cond_f

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_f

    .line 615
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 616
    :cond_f
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 617
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_3
.end method
