.class public Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "SearchMusicPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;,
        Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20

.field private static mIsDayNightMode:Z


# instance fields
.field private mAlbumList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private mAllsongsViewed:I

.field private mArtistList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation
.end field

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

.field private mHandler:Landroid/os/Handler;

.field private final mImageDecodeThreadPool:Ljava/util/concurrent/ExecutorService;

.field private mInitialPosition:I

.field private mPageCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mIsDayNightMode:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;Landroid/view/View$OnClickListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 39
    iput v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAllsongsViewed:I

    .line 48
    invoke-static {v3}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mImageDecodeThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 49
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mHandler:Landroid/os/Handler;

    .line 53
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 55
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getArtistList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    .line 56
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getAlbumList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    .line 57
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->getPageCount()I

    move-result v0

    .line 60
    .local v0, "pageCount":I
    if-le v0, v3, :cond_0

    .line 62
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 61
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mInitialPosition:I

    .line 63
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mPageCount:I

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    iput v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mInitialPosition:I

    .line 66
    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private requestImageLoad(Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 2
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .prologue
    .line 137
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V

    .line 138
    .local v0, "runnable":Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mImageDecodeThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 139
    return-void
.end method

.method private requestImageLoad(Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 2
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .prologue
    .line 132
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V

    .line 133
    .local v0, "runnable":Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$imageLoadRunnable;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mImageDecodeThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 134
    return-void
.end method

.method public static setDayNightMode(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 44
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mIsDayNightMode:Z

    .line 45
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 363
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 364
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 368
    iget v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 405
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 380
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getResultItemCount()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 382
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getResultItemCount()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 384
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 385
    add-int/lit8 v0, v0, 0x1

    .line 388
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 12
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 143
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 145
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    const v7, 0x7f03008c

    const/4 v8, 0x0

    .line 144
    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 147
    .local v6, "v":Landroid/view/View;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v5, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v7, 0x7f09027f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    const v7, 0x7f090280

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    const v7, 0x7f090281

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    const v7, 0x7f090282

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->getPageCount()I

    move-result v7

    rem-int v7, p2, v7

    mul-int/lit8 v2, v7, 0x4

    .line 158
    .local v2, "index":I
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAllsongsViewed:I

    .line 159
    const/4 v1, 0x1

    .line 160
    .local v1, "i":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 356
    invoke-virtual {v6, p2}, Landroid/view/View;->setId(I)V

    .line 357
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v6}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 358
    return-object v6

    .line 160
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 162
    .local v3, "layout":Landroid/widget/RelativeLayout;
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;

    const/4 v7, 0x0

    invoke-direct {v0, v7}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;)V

    .line 164
    .local v0, "holder":Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;
    const v7, 0x7f090283

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 163
    iput-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicImage:Landroid/widget/ImageView;

    .line 166
    const v7, 0x7f090284

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 165
    iput-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    .line 168
    const v7, 0x7f090285

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 167
    iput-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    .line 170
    const v7, 0x7f090287

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 169
    iput-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    .line 172
    const v7, 0x7f090286

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 171
    iput-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountAlbum:Landroid/widget/TextView;

    .line 174
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v2, :cond_5

    const/16 v7, 0x27

    if-ge v2, v7, :cond_5

    .line 175
    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 176
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicImage:Landroid/widget/ImageView;

    .line 179
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 178
    invoke-direct {p0, v9, v7}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->requestImageLoad(Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V

    .line 190
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v9, "<unknown>"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 191
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "<"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 192
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    const v11, 0x7f0a036e

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 193
    const-string/jumbo v10, ">"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 191
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    :goto_1
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getAlbumCount()I

    move-result v7

    const/4 v9, 0x1

    if-ne v7, v9, :cond_2

    .line 200
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountAlbum:Landroid/widget/TextView;

    .line 201
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 202
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getAlbumCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 203
    const-string/jumbo v7, " "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 204
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 205
    const v11, 0x7f0a0366

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 204
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 206
    const-string/jumbo v10, ",  "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 201
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    :goto_2
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getSongCount()I

    move-result v7

    const/4 v9, 0x1

    if-ne v7, v9, :cond_3

    .line 218
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    .line 219
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 220
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getSongCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 221
    const-string/jumbo v7, " "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 222
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 223
    const v11, 0x7f0a0365

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 222
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 219
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    :goto_3
    sget-boolean v7, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mIsDayNightMode:Z

    if-eqz v7, :cond_4

    .line 234
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 235
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 236
    const v10, 0x7f08002f

    .line 235
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 234
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 237
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    .line 238
    const v9, 0x7f020209

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 352
    :goto_4
    add-int/lit8 v1, v1, 0x1

    .line 353
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 195
    :cond_1
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 196
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getName()Ljava/lang/String;

    move-result-object v7

    .line 195
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 208
    :cond_2
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountAlbum:Landroid/widget/TextView;

    .line 209
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 210
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getAlbumCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 211
    const-string/jumbo v7, " "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 212
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 213
    const v11, 0x7f0a0368

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 212
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 214
    const-string/jumbo v10, ",  "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 209
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 225
    :cond_3
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    .line 226
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 227
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getSongCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 228
    const-string/jumbo v7, " "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 229
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 230
    const v11, 0x7f0a0367

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 229
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 226
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 240
    :cond_4
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 241
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 242
    const v10, 0x7f080030

    .line 241
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 240
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 243
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    .line 244
    const v9, 0x7f02020c

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 245
    const v7, 0x7f020002

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 247
    :cond_5
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sub-int v9, v2, v9

    if-le v7, v9, :cond_9

    .line 248
    const/16 v7, 0x27

    if-ge v2, v7, :cond_9

    .line 249
    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 250
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicImage:Landroid/widget/ImageView;

    .line 253
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    sub-int v10, v2, v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 252
    invoke-direct {p0, v9, v7}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->requestImageLoad(Landroid/widget/ImageView;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V

    .line 263
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sub-int v9, v2, v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getName()Ljava/lang/String;

    move-result-object v7

    .line 264
    const-string/jumbo v9, "<unknown>"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 265
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "<"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 266
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    const v11, 0x7f0a036d

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 267
    const-string/jumbo v10, ">"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 265
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    :goto_5
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sub-int v9, v2, v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getSongCount()I

    move-result v7

    const/4 v9, 0x1

    if-ne v7, v9, :cond_7

    .line 274
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    .line 275
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    .line 276
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    sub-int v11, v2, v11

    .line 275
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 276
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getSongCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 277
    const-string/jumbo v7, " "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 278
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 279
    const v11, 0x7f0a0365

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 278
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 275
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    :goto_6
    sget-boolean v7, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mIsDayNightMode:Z

    if-eqz v7, :cond_8

    .line 290
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 291
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 292
    const v10, 0x7f08002f

    .line 291
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 290
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 293
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    .line 294
    const v9, 0x7f020209

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 269
    :cond_6
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    .line 270
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    sub-int v10, v2, v10

    .line 269
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 270
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getName()Ljava/lang/String;

    move-result-object v7

    .line 269
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 281
    :cond_7
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    .line 282
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    .line 283
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    sub-int v11, v2, v11

    .line 282
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 283
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getSongCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 284
    const-string/jumbo v7, " "

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 285
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 286
    const v11, 0x7f0a0367

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 285
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 282
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 296
    :cond_8
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 297
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 298
    const v10, 0x7f080030

    .line 297
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 296
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 299
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    .line 300
    const v9, 0x7f02020c

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 301
    const v7, 0x7f020002

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 304
    :cond_9
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v7

    if-lez v7, :cond_c

    .line 305
    iget v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAllsongsViewed:I

    if-nez v7, :cond_c

    .line 306
    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 307
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicImage:Landroid/widget/ImageView;

    .line 309
    const v9, 0x7f020224

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 311
    const v10, 0x7f0a0369

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 310
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v7

    const/4 v9, 0x1

    if-ne v7, v9, :cond_a

    .line 313
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    .line 314
    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 315
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 316
    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 317
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 318
    const v11, 0x7f0a0365

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 317
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 314
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    :goto_7
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAllsongsViewed:I

    .line 329
    sget-boolean v7, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mIsDayNightMode:Z

    if-eqz v7, :cond_b

    .line 330
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 331
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 332
    const v10, 0x7f08002f

    .line 331
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 330
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 333
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    .line 334
    const v9, 0x7f020209

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 320
    :cond_a
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    .line 321
    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 322
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 323
    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 324
    iget-object v10, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 325
    const v11, 0x7f0a0367

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 324
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 321
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 336
    :cond_b
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mContext:Landroid/content/Context;

    .line 337
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 338
    const v10, 0x7f080030

    .line 337
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 336
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 339
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    .line 340
    const v9, 0x7f02020c

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 341
    const v7, 0x7f020002

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 344
    :cond_c
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicImage:Landroid/widget/ImageView;

    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 345
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->ivSearchMusicMaskImage:Landroid/widget/ImageView;

    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 346
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicName:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 347
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountSong:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 348
    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter$ViewHolder;->tvSearchMusicCountAlbum:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 393
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected setDayMode()V
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mIsDayNightMode:Z

    .line 377
    return-void
.end method

.method public setMusicList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 2
    .param p1, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mDLMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 72
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getArtistList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mArtistList:Ljava/util/ArrayList;

    .line 73
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getAlbumList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mAlbumList:Ljava/util/ArrayList;

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->getPageCount()I

    move-result v0

    .line 75
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 77
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 76
    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mInitialPosition:I

    .line 78
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mPageCount:I

    .line 83
    :goto_0
    return-void

    .line 80
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mInitialPosition:I

    .line 81
    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method protected setNightMode()V
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->mIsDayNightMode:Z

    .line 373
    return-void
.end method
