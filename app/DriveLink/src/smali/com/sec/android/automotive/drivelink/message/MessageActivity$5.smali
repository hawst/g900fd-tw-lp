.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 270
    const-string/jumbo v1, "setBackButtonClickListener"

    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/util/DLLog;->iFace(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$41(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 276
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 278
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 279
    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 280
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$41(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$24(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->showVoiceLayout()V

    .line 285
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mListMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$43(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)V

    .line 286
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    .line 288
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$44(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 289
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$45(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/lang/String;)V

    .line 290
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$46(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 292
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 293
    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 297
    :cond_0
    const-string/jumbo v1, "MessageActivity"

    .line 298
    const-string/jumbo v2, "setOnBackBtnClickListener - setAutoShrink : true"

    .line 297
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 325
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 305
    :cond_1
    const-string/jumbo v1, "MessageActivity"

    const-string/jumbo v2, "setOnBackBtnClickListener - setAutoShrink : false"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 317
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->restorePrevFlow()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$48(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    goto :goto_0
.end method
