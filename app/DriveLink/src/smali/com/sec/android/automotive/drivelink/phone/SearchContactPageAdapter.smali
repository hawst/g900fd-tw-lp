.class public Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "SearchContactPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20

.field public static final TAG:Ljava/lang/String; = "SearchContactPageAdapter"


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mInitialPosition:I

.field private mIsNightMode:Z

.field private mPageCount:I

.field private mSearchList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchedText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "mContactChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 38
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchedText:Ljava/lang/String;

    .line 43
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mIsNightMode:Z

    .line 55
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    .line 57
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getPageCount()I

    move-result v0

    .line 60
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 62
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 61
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mInitialPosition:I

    .line 63
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mPageCount:I

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    iput v2, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mInitialPosition:I

    .line 66
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p4, "searchedText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    .local p2, "mContactChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V

    .line 50
    invoke-virtual {p0, p4}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->setSearchedText(Ljava/lang/String;)V

    .line 51
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 212
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 213
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 245
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 221
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 222
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 223
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 224
    add-int/lit8 v0, v0, 0x1

    .line 226
    :cond_0
    const/16 v2, 0xa

    if-le v0, v2, :cond_1

    .line 227
    const/16 v0, 0xa

    .line 228
    :cond_1
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 17
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    .line 95
    .local v13, "layoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030090

    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    .line 97
    .local v15, "v":Landroid/view/View;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v14, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v2, 0x7f090291

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    const v2, 0x7f090292

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    const v2, 0x7f090293

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    const v2, 0x7f090294

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getPageCount()I

    move-result v2

    rem-int v2, p2, v2

    mul-int/lit8 v11, v2, 0x4

    .line 106
    .local v11, "index":I
    const/4 v8, 0x0

    .line 107
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    const/4 v10, 0x1

    .line 109
    .local v10, "i":I
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 205
    move/from16 v0, p2

    invoke-virtual {v15, v0}, Landroid/view/View;->setId(I)V

    .line 206
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 207
    return-object v15

    .line 109
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout;

    .line 110
    .local v12, "layout":Landroid/widget/RelativeLayout;
    new-instance v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;

    const/4 v2, 0x0

    invoke-direct {v9, v2}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;)V

    .line 113
    .local v9, "holder":Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;
    const v2, 0x7f0901f7

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 112
    iput-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 115
    const v2, 0x7f09028f

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 114
    iput-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 117
    const v2, 0x7f0901f8

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 116
    iput-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 119
    const v2, 0x7f090290

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 118
    iput-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v11, :cond_5

    .line 123
    invoke-virtual {v12, v10}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 128
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    .line 129
    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v6, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 130
    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 126
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 132
    .local v1, "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 131
    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 133
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mContext:Landroid/content/Context;

    .line 132
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 135
    if-nez v8, :cond_3

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 137
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 138
    const v3, 0x7f02024e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    :goto_1
    const-string/jumbo v3, "SearchContactPageAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "name : "

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-static {v3, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const-string/jumbo v2, "SearchContactPageAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "searchedText : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v3, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v4, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 157
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchedText:Ljava/lang/String;

    .line 155
    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getInitialText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 159
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v5

    .line 160
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 162
    const v6, 0x7f08002c

    .line 161
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 163
    sget-object v6, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v6}, Landroid/graphics/Typeface;->getStyle()I

    move-result v6

    .line 153
    invoke-static {v4, v2, v5, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v3, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    .line 165
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 164
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mIsNightMode:Z

    if-eqz v2, :cond_4

    .line 168
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 169
    const v3, 0x7f02020a

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 170
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 171
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 172
    const v4, 0x7f080030

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 170
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 173
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 174
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 175
    const v4, 0x7f080030

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 173
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 176
    const v2, 0x7f020002

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 201
    .end local v1    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_1
    :goto_2
    add-int/lit8 v11, v11, 0x1

    .line 202
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 140
    .restart local v1    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_2
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 141
    const v3, 0x7f02023d

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 144
    :cond_3
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 178
    :cond_4
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 179
    const v3, 0x7f020209

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 180
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 181
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 182
    const v4, 0x7f08002f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 180
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 183
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 184
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 185
    const v4, 0x7f08002f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 183
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 190
    .end local v1    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_5
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 191
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 192
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    iget-object v2, v9, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter$ViewHolder;->tvNumber:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mIsNightMode:Z

    if-eqz v2, :cond_1

    .line 195
    const v2, 0x7f020002

    invoke-virtual {v12, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 233
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setNightMode(Z)V
    .locals 0
    .param p1, "isNightMode"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mIsNightMode:Z

    .line 90
    return-void
.end method

.method public setSearchList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchList:Ljava/util/List;

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->getPageCount()I

    move-result v0

    .line 74
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 76
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 75
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mInitialPosition:I

    .line 77
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mPageCount:I

    .line 82
    :goto_0
    return-void

    .line 79
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mInitialPosition:I

    .line 80
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method public setSearchedText(Ljava/lang/String;)V
    .locals 0
    .param p1, "searchedText"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/SearchContactPageAdapter;->mSearchedText:Ljava/lang/String;

    .line 86
    return-void
.end method
