.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestMusicList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "object"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mContext:Landroid/content/Context;

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .line 26
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mContext:Landroid/content/Context;

    .line 27
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .line 28
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mContext:Landroid/content/Context;

    .line 66
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .line 67
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 68
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 34
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 35
    return-void
.end method

.method protected response()V
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v0

    .line 41
    .local v0, "musicListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    if-eqz v0, :cond_0

    .line 42
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    if-nez v1, :cond_1

    .line 43
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicList(Ljava/util/ArrayList;)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    instance-of v1, v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v1, :cond_2

    .line 45
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 46
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 45
    invoke-interface {v0, v2, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicListByMusic(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    goto :goto_0

    .line 47
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    instance-of v1, v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    if-eqz v1, :cond_3

    .line 48
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 49
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 48
    invoke-interface {v0, v2, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicListByAlbum(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V

    goto :goto_0

    .line 50
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    instance-of v1, v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    if-eqz v1, :cond_4

    .line 51
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 52
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 51
    invoke-interface {v0, v2, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicListByArtist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V

    goto :goto_0

    .line 53
    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    instance-of v1, v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    if-eqz v1, :cond_5

    .line 54
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 55
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    .line 54
    invoke-interface {v0, v2, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicListByFolder(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V

    goto :goto_0

    .line 56
    :cond_5
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    instance-of v1, v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;

    if-eqz v1, :cond_0

    .line 57
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mMusicList:Ljava/util/ArrayList;

    .line 58
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicList;->mObject:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;

    .line 57
    invoke-interface {v0, v2, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestMusicListByPlaylist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V

    goto :goto_0
.end method
