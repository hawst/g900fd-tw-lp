.class Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;
.super Ljava/lang/Object;
.source "LocationRequestActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->setupListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 336
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getTextFromEditTextSearch()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 338
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$15(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 342
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->requestSearchedContactList:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$16(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$15(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 345
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 346
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getTextFromEditTextSearch()Ljava/lang/String;

    move-result-object v2

    .line 347
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 344
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    .line 350
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
