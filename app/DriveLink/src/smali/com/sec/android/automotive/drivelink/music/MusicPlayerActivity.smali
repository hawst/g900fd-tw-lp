.class public Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;,
        Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;,
        Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$SettingsContentObserver;
    }
.end annotation


# static fields
.field public static final CONTACT_CHOICE:Ljava/lang/String; = "CONTACT_CHOICE"

.field public static final CONTACT_TYPE:Ljava/lang/String; = "CONTACT_TYPE"

.field private static final DELAY_TIME_FOR_AUTO_DISAPPEAR:J = 0xfa0L

.field public static final DELAY_TIME_FOR_AUTO_SHRINK:J = 0x1388L

.field public static final DELAY_TIME_FOR_AUTO_SHRINK_PLAYING:J = 0x3a98L

.field private static final DELAY_TIME_FOR_NOTI_SHOW_CHECKER:J = 0x1f4L

.field private static final ERROR_STRING:Ljava/lang/String; = "ERROR_STRING"

.field private static final FFORWARD_REWIND_DELAY_TIME:I = 0x190

.field private static final IS_ERROR_STATE:Ljava/lang/String; = "IS_ERROR_STATE"

.field private static final IS_TTS_STATE:Ljava/lang/String; = "IS_TTS_STATE"

.field public static final MESSAGE_LIST_MODE:Ljava/lang/String; = "MESSAGE_LIST_MODE"

.field public static final MSG_AUTO_DISAPPEAR_SOUND:I = 0x1

.field public static final MSG_AUTO_SHRINK:I = 0x0

.field public static final MSG_AUTO_SHRINK_NOTI:I = 0x6

.field public static final MSG_PLAYER_DIM_OFF:I = 0x2

.field public static final MSG_PLAYER_DIM_ON:I = 0x3

.field public static final MSG_TOUCH_LOCK:I = 0x4

.field public static final MSG_TOUCH_UNLOCK:I = 0x5

.field private static final PROG_TIME_INTERVAL_MS:I = 0x384

.field public static final SIP_IME:Ljava/lang/String; = "SIP_IME"

.field public static final SIP_SEARCH_MODE:Ljava/lang/String; = "SIP_SEARCH_MODE"

.field public static final SIP_SEARCH_RESULT_TEXT:Ljava/lang/String; = "SIP_SEARCH_RESULT_TEXT"

.field public static final SIP_SEARCH_TEXT:Ljava/lang/String; = "SIP_SEARCH_TEXT"

.field private static final TAG:Ljava/lang/String; = "MusicPlayerActivity "

.field public static final TOUCH_LOCK_TIME:J = 0x7d0L


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private bmActive:Z

.field private isNight:Z

.field private item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

.field private longPressed:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

.field private mArtistName:Landroid/widget/TextView;

.field private mBTKProc:Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;

.field private mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

.field private mContext:Landroid/content/Context;

.field private mDefaultBitmap:Z

.field mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

.field private mDrivingMode:Z

.field private mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

.field private mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

.field private mHandler:Landroid/os/Handler;

.field private mLlSoundControl:Landroid/widget/LinearLayout;

.field private mLongClickState:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

.field private mMusicBackground:Landroid/widget/ImageView;

.field private mMusicBackgroundGradient:Landroid/widget/ImageView;

.field private mMusicDim:Landroid/widget/ImageView;

.field mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

.field private mMusicVoiceBackground:Landroid/widget/ImageView;

.field private mMusicVolume:I

.field private mNextBtn:Landroid/widget/Button;

.field private mPlayingTime:Landroid/widget/TextView;

.field private mPlaypauseBtn:Landroid/widget/Button;

.field private mPrevBtn:Landroid/widget/Button;

.field private mPrevOrNextBtnClicked:Z

.field private mPrevSearchText:Ljava/lang/String;

.field private mPrevStateValue:I

.field private mProgHandler:Landroid/os/Handler;

.field private mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

.field private mRlEmpty:Landroid/widget/RelativeLayout;

.field private mSearchClearBtn:Landroid/widget/ImageButton;

.field private mSearchIconText:Landroid/widget/TextView;

.field private mSearchMode:Z

.field private mSearchText:Landroid/widget/EditText;

.field protected mSearchedText:Ljava/lang/String;

.field private mShowSIPInMultiWindow:Z

.field private mShuffleBtn:Landroid/widget/Button;

.field private mSongThumbnail:Landroid/widget/ImageView;

.field private mSongTitle:Landroid/widget/TextView;

.field private mTextfieldLayout:Landroid/view/ViewGroup;

.field private mTotalTime:Landroid/widget/TextView;

.field private mTouchEvent:Z

.field private mTvVolumeInfo:Landroid/widget/TextView;

.field private mUnknownValue:Z

.field private mVoiceLayoutState:Z

.field private mVolumeBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

.field private mVolumeBtn:Landroid/widget/Button;

.field private multiWindowAccess:Z

.field private startTime:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 68
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 132
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    .line 134
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->multiWindowAccess:Z

    .line 135
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDrivingMode:Z

    .line 136
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShowSIPInMultiWindow:Z

    .line 141
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isNight:Z

    .line 143
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mBTKProc:Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;

    .line 149
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->startTime:J

    .line 151
    sget-object v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;->NONE:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLongClickState:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

    .line 152
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mUnknownValue:Z

    .line 154
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bmActive:Z

    .line 155
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDefaultBitmap:Z

    .line 156
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTouchEvent:Z

    .line 165
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    .line 166
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevSearchText:Ljava/lang/String;

    .line 168
    iput v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevStateValue:I

    .line 169
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z

    .line 170
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVoiceLayoutState:Z

    .line 171
    iput v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVolume:I

    .line 480
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchedText:Ljava/lang/String;

    .line 1066
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    .line 1262
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 1436
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 68
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isNotificationTopActivity()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShowSIPInMultiWindow:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShowSIPInMultiWindow:Z

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;I)V
    .locals 0

    .prologue
    .line 1600
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setTouchPlayingTime(I)V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 1900
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLongClickState:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1430
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->progressUpdate()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTouchEvent:Z

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1854
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bmActive:Z

    return v0
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 958
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setUnkownInfo()V

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1563
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setPlayingTime()V

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 1220
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 914
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDrivingMode:Z

    return-void
.end method

.method static synthetic access$29(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 1197
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$31(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    return-object v0
.end method

.method static synthetic access$32(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 165
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    return-void
.end method

.method static synthetic access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$34(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;I)V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->handleShrinkMessageOnMicState(I)V

    return-void
.end method

.method static synthetic access$35(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;I)V
    .locals 0

    .prologue
    .line 410
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->handleShrinkMessageOnSpottingState(I)V

    return-void
.end method

.method static synthetic access$36(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    return v0
.end method

.method static synthetic access$37(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;I)V
    .locals 0

    .prologue
    .line 171
    iput p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVolume:I

    return-void
.end method

.method static synthetic access$38(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mBTKProc:Lcom/sec/android/automotive/drivelink/music/MusicBTKeyboardProc;

    return-object v0
.end method

.method static synthetic access$39(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 1231
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    return-void
.end method

.method static synthetic access$40(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$41(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchIconText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$42(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevSearchText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V
    .locals 0

    .prologue
    .line 1242
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setCtrlsEnabled(Z)V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1011
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setMusicInfo()V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->multiWindowAccess:Z

    return v0
.end method

.method private static changeToMinute(I)Ljava/lang/String;
    .locals 8
    .param p0, "mseconds"    # I

    .prologue
    const v7, 0xea60

    const/16 v6, 0xa

    .line 1582
    const/4 v0, 0x0

    .line 1583
    .local v0, "min":I
    const/4 v2, 0x0

    .line 1584
    .local v2, "sec":I
    const-string/jumbo v1, ""

    .line 1585
    .local v1, "minStr":Ljava/lang/String;
    const-string/jumbo v3, ""

    .line 1587
    .local v3, "secStr":Ljava/lang/String;
    div-int v4, p0, v7

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 1588
    mul-int v4, v7, v0

    sub-int v4, p0, v4

    div-int/lit16 v4, v4, 0x3e8

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 1590
    if-ge v0, v6, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1591
    :goto_0
    if-ge v2, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1593
    :goto_1
    const-string/jumbo v4, "MusicPlayerActivity "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1594
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 1590
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1591
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private checkSearchLayoutState()V
    .locals 4

    .prologue
    .line 545
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    if-eqz v0, :cond_0

    .line 546
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->stopDmFlow()V

    .line 547
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->hideVoiceLayout()V

    .line 549
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 553
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 555
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    .line 557
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 564
    const-wide/16 v2, 0x12c

    .line 557
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 566
    :cond_0
    return-void
.end method

.method private drawAlbumBitmap()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1128
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "Album info : start"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1130
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1131
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    .line 1137
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 1139
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 1140
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 1139
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1145
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    .line 1146
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDefaultBitmap:Z

    .line 1147
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1148
    const v2, 0x7f020183

    .line 1147
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    .line 1152
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->recycleBitmap(Landroid/widget/ImageView;)V

    .line 1153
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1154
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1155
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDefaultBitmap:Z

    if-eqz v1, :cond_3

    .line 1156
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackground:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1157
    const v3, 0x7f080078

    .line 1156
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 1160
    :goto_2
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "Album info : end"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1162
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTotalTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getDuration()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->changeToMinute(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->progressUpdate()V

    .line 1164
    return-void

    .line 1141
    :catch_0
    move-exception v0

    .line 1142
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1150
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDefaultBitmap:Z

    goto :goto_1

    .line 1159
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackground:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getImage(Landroid/graphics/Bitmap;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_2
.end method

.method private drawBigImage(Landroid/widget/ImageView;I)V
    .locals 7
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "resId"    # I

    .prologue
    .line 1541
    if-eqz p1, :cond_0

    .line 1543
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1544
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v4, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1545
    const/4 v4, 0x4

    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1546
    const/4 v4, 0x1

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 1547
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1549
    .local v3, "src":Landroid/graphics/Bitmap;
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1550
    iget v5, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v6, 0x1

    .line 1549
    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1552
    .local v2, "resize":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1558
    .end local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v2    # "resize":Landroid/graphics/Bitmap;
    .end local v3    # "src":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 1555
    :catch_0
    move-exception v0

    .line 1556
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private drawUnknownBitmap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1167
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "Album info : start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1168
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1170
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    .line 1172
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1173
    const v1, 0x7f020183

    .line 1172
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    .line 1175
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->recycleBitmap(Landroid/widget/ImageView;)V

    .line 1176
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1177
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1178
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackground:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1179
    const v2, 0x7f080078

    .line 1178
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 1180
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "Album info : end"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1181
    return-void
.end method

.method private getMusicSipStateListener()Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;
    .locals 1

    .prologue
    .line 2157
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$20;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    return-object v0
.end method

.method private handleShrinkMessageOnMicState(I)V
    .locals 4
    .param p1, "stateValue"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 340
    packed-switch p1, :pswitch_data_0

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 342
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setPlayerDimOn(Z)V

    .line 344
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 348
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVolume:I

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicVolume(I)V

    .line 350
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 352
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->multiWindowAccess:Z

    if-eqz v0, :cond_4

    .line 355
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicSearchResultExist()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 357
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSearchResultFlag(Z)V

    .line 381
    :goto_1
    iget v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevStateValue:I

    if-ne v0, v3, :cond_1

    .line 382
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 385
    :cond_1
    iput v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevStateValue:I

    goto :goto_0

    .line 362
    :cond_2
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->ismVoiceMusicNoSearchInHomeActivity()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 364
    invoke-static {v2}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->setmVoiceMusicNoSearchInHomeActivity(Z)V

    goto :goto_1

    .line 366
    :cond_3
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 367
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->handleShrinkMessageOnSpottingState(I)V

    goto :goto_1

    .line 373
    :cond_4
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->ismVoiceMusicNoSearchInHomeActivity()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 375
    invoke-static {v2}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->setmVoiceMusicNoSearchInHomeActivity(Z)V

    goto :goto_1

    .line 377
    :cond_5
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    goto :goto_1

    .line 390
    :pswitch_1
    const-string/jumbo v0, "MusicPlayerActivity"

    .line 391
    const-string/jumbo v1, "OnVoiceMusicPlayerActionBarListener() = MIC_STATE_LISTENING"

    .line 390
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setPlayerDimOn(Z)V

    .line 393
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 394
    iput v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevStateValue:I

    goto :goto_0

    .line 399
    :pswitch_2
    const-string/jumbo v0, "MusicPlayerActivity"

    .line 400
    const-string/jumbo v1, "OnVoiceMusicPlayerActionBarListener() = MIC_STATE_THINKING"

    .line 399
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setPlayerDimOn(Z)V

    .line 402
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 404
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevStateValue:I

    goto :goto_0

    .line 340
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleShrinkMessageOnSpottingState(I)V
    .locals 3
    .param p1, "stateValue"    # I

    .prologue
    const/4 v2, 0x0

    .line 411
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 412
    packed-switch p1, :pswitch_data_0

    .line 425
    :goto_0
    return-void

    .line 414
    :pswitch_0
    const-string/jumbo v0, "MusicPlayerActivity"

    .line 415
    const-string/jumbo v1, "OnVoiceMusicPlayerActionBarListener() = SPOTTER_STATE_SPOTTING"

    .line 414
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 420
    :pswitch_1
    const-string/jumbo v0, "MusicPlayerActivity"

    .line 421
    const-string/jumbo v1, "OnVoiceMusicPlayerActionBarListener() = SPOTTER_STATE_NONE_SPOTTING"

    .line 420
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    goto :goto_0

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private initLayout()V
    .locals 4

    .prologue
    .line 593
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 595
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isErrorState()Z

    move-result v1

    .line 596
    .local v1, "isErrorState":Z
    const/4 v0, 0x0

    .line 597
    .local v0, "errorCS":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 598
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getErrorCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 601
    :cond_0
    const v2, 0x7f03001e

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setContentView(I)V

    .line 602
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-static {p0, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiModeFullSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)Z

    .line 604
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    if-eqz v2, :cond_1

    .line 605
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->stopTTSBarAnimation()V

    .line 608
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initView()V

    .line 610
    const-string/jumbo v2, "JINSEIL"

    const-string/jumbo v3, "JINSEIL initLayout setVolumeUI"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V

    .line 613
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    new-instance v3, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 643
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 644
    new-instance v3, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$9;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setOnVoiceMusicPlayerActionBarListener(Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;)V

    .line 661
    if-eqz v1, :cond_2

    .line 662
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->displayError(Ljava/lang/CharSequence;)V

    .line 665
    :cond_2
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 666
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    .line 667
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v3

    invoke-virtual {v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v3

    .line 666
    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 669
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 670
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setMusicInfo()V

    .line 672
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setContentDescription()V

    .line 674
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isNight:Z

    if-eqz v2, :cond_3

    .line 675
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setNightMode()V

    .line 678
    :goto_0
    return-void

    .line 677
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setDayMode()V

    goto :goto_0
.end method

.method private initSearchTextField()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1998
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 1999
    const v1, 0x7f09028a

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1998
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;

    .line 2000
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 2001
    const v1, 0x7f09028c

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 2000
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    .line 2002
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 2003
    const v1, 0x7f09028d

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 2002
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    .line 2004
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 2005
    const v1, 0x7f09037c

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2004
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchIconText:Landroid/widget/TextView;

    .line 2007
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevSearchText:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2008
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "mPrevSearchText != null setText"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2009
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevSearchText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2010
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevSearchText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 2011
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchIconText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2012
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2019
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;

    .line 2020
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getMusicSipStateListener()Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP;->setOnMusicSipStateListener(Lcom/sec/android/automotive/drivelink/music/EditTextForMusicSIP$OnMusicSipStateListener;)V

    .line 2021
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    const-string/jumbo v1, "disableEmoticonInput=true;"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 2023
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$16;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$16;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setOnSearchBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2058
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$17;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2089
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$18;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2138
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2154
    return-void

    .line 2014
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchIconText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2015
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchClearBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 717
    const v0, 0x7f0900cd

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 718
    const v0, 0x7f0900dc

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mRlEmpty:Landroid/widget/RelativeLayout;

    .line 719
    const v0, 0x7f0900dd

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLlSoundControl:Landroid/widget/LinearLayout;

    .line 720
    const v0, 0x7f0900ca

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackground:Landroid/widget/ImageView;

    .line 721
    const v0, 0x7f0900cb

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVoiceBackground:Landroid/widget/ImageView;

    .line 722
    const v0, 0x7f0900cc

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 723
    const v0, 0x7f0900db

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicDim:Landroid/widget/ImageView;

    .line 724
    const v0, 0x7f0900cf

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongTitle:Landroid/widget/TextView;

    .line 725
    const v0, 0x7f0900d0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mArtistName:Landroid/widget/TextView;

    .line 726
    const v0, 0x7f0900d3

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlayingTime:Landroid/widget/TextView;

    .line 727
    const v0, 0x7f0900d4

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTotalTime:Landroid/widget/TextView;

    .line 728
    const v0, 0x7f0900df

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTvVolumeInfo:Landroid/widget/TextView;

    .line 729
    const v0, 0x7f0900d1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    .line 730
    const v0, 0x7f0900d6

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBtn:Landroid/widget/Button;

    .line 731
    const v0, 0x7f0900da

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    .line 732
    const v0, 0x7f0900d7

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    .line 733
    const v0, 0x7f0900d9

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;

    .line 734
    const v0, 0x7f0900d8

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    .line 735
    const v0, 0x7f0900de

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    .line 736
    const v0, 0x7f0900d2

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    .line 737
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setEnabled(Z)V

    .line 739
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 740
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 743
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 744
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 745
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 746
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 748
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 772
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 796
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mRlEmpty:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 800
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->initMusicVolume()V

    .line 808
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 833
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 859
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$14;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 887
    return-void
.end method

.method private progressUpdate()V
    .locals 1

    .prologue
    .line 1431
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1432
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setPlayingTime()V

    .line 1434
    :cond_0
    return-void
.end method

.method private static recycleBitmap(Landroid/widget/ImageView;)V
    .locals 5
    .param p0, "iv"    # Landroid/widget/ImageView;

    .prologue
    .line 702
    if-nez p0, :cond_0

    .line 714
    :goto_0
    return-void

    .line 705
    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 706
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    instance-of v2, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_2

    move-object v2, v1

    .line 707
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 708
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 709
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 710
    :cond_1
    const-string/jumbo v2, "MusicPlayerActivity "

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "recycleBitmap"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    .end local v0    # "b":Landroid/graphics/Bitmap;
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0
.end method

.method private setAutoShrink(Z)V
    .locals 4
    .param p1, "isOn"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1218
    :cond_0
    :goto_0
    return-void

    .line 1201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1203
    if-eqz p1, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    if-nez v0, :cond_3

    .line 1204
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->ismUserPausePriority()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1205
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->multiWindowAccess:Z

    if-nez v0, :cond_2

    .line 1206
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_AUTO_SHRINK - start on playing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1207
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    .line 1208
    const-wide/16 v1, 0x3a98

    .line 1207
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1210
    :cond_2
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_AUTO_SHRINK - start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    .line 1212
    const-wide/16 v1, 0x1388

    .line 1211
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1215
    :cond_3
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_AUTO_SHRINK - removing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1216
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setContentDescription()V
    .locals 2

    .prologue
    .line 890
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBtn:Landroid/widget/Button;

    if-nez v0, :cond_1

    .line 891
    :cond_0
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "Button is null. return..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    :goto_0
    return-void

    .line 895
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 896
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    .line 897
    const v1, 0x7f0a0376

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 902
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 903
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    .line 904
    const v1, 0x7f0a0377

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 899
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    .line 900
    const v1, 0x7f0a0375

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 906
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    .line 907
    const v1, 0x7f0a0378

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setCtrlsEnabled(Z)V
    .locals 3
    .param p1, "isEnabled"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1243
    if-eqz p1, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 1245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 1246
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 1247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 1248
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 1249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setClickable(Z)V

    .line 1260
    :goto_0
    return-void

    .line 1252
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 1253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 1254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 1255
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 1256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 1258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setClickable(Z)V

    goto :goto_0
.end method

.method private setFFREThreadWithStop()V
    .locals 3

    .prologue
    .line 1855
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    if-eqz v1, :cond_0

    .line 1857
    :try_start_0
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "FForwardRewindThread stop"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1858
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 1859
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->interrupt()V

    .line 1860
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1865
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    .line 1867
    :cond_0
    return-void

    .line 1861
    :catch_0
    move-exception v0

    .line 1862
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1863
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "FForwardRewindThread join exception"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setMusicInfo()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1013
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "setMusicInfo"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1064
    :cond_0
    :goto_0
    return-void

    .line 1016
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    .line 1017
    .local v0, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    if-nez v0, :cond_2

    .line 1018
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setUnkownInfo()V

    goto :goto_0

    .line 1022
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1023
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 1027
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isShuffled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1028
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setSelected(Z)V

    .line 1032
    :goto_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v1, :cond_5

    .line 1033
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v2

    if-ne v1, v2, :cond_5

    .line 1034
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "setMusicInfo skipped"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1025
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_1

    .line 1030
    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_2

    .line 1039
    :cond_5
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 1041
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "<unknown>"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1042
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongTitle:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "<"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1043
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f0a036d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1042
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1048
    :goto_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getArtist()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "<unknown>"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1049
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mArtistName:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "<"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1050
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f0a036e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1049
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1056
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->drawAlbumBitmap()V

    .line 1062
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1063
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 1045
    :cond_6
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 1052
    :cond_7
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mArtistName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method private setNextPrevButtonProcess(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1617
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setCtrlsEnabled(Z)V

    .line 1620
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setAllMusicShuffleList()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1621
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setUnkownInfo()V

    .line 1643
    :cond_0
    :goto_0
    return-void

    .line 1625
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1626
    :cond_2
    if-eqz p1, :cond_3

    .line 1627
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayerNoPlay()V

    .line 1631
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMusicPlayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1632
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->progressUpdate()V

    goto :goto_0

    .line 1629
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayerNoPlay()V

    goto :goto_1

    .line 1635
    :cond_4
    if-eqz p1, :cond_5

    .line 1636
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayer()V

    goto :goto_0

    .line 1638
    :cond_5
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayer(Z)V

    goto :goto_0
.end method

.method private setPlayerDimOn(Z)V
    .locals 2
    .param p1, "bOn"    # Z

    .prologue
    .line 1184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1195
    :goto_0
    return-void

    .line 1188
    :cond_0
    if-eqz p1, :cond_1

    .line 1189
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_PLAYER_DIM_ON"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1192
    :cond_1
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_PLAYER_DIM_OFF"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private setPlayingTime()V
    .locals 4

    .prologue
    .line 1564
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlayingTime:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 1565
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1566
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusicDuration()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setMax(I)V

    .line 1568
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getCurrentPosition()I

    move-result v1

    .line 1569
    .local v1, "currenttime":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v2, :cond_0

    .line 1570
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getDuration()I

    move-result v2

    if-gt v1, v2, :cond_1

    .line 1571
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setProgress(I)V

    .line 1572
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->changeToMinute(I)Ljava/lang/String;

    move-result-object v0

    .line 1573
    .local v0, "ct":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlayingTime:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1579
    .end local v0    # "ct":Ljava/lang/String;
    .end local v1    # "currenttime":I
    :cond_0
    :goto_0
    return-void

    .line 1575
    .restart local v1    # "currenttime":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlayingTime:Landroid/widget/TextView;

    const-string/jumbo v3, "00:00"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setProgressStopWithStarted(Z)V
    .locals 4
    .param p1, "value"    # Z

    .prologue
    .line 1901
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    if-eqz v1, :cond_0

    .line 1903
    :try_start_0
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "ProgressStop"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->isRunning:Z

    .line 1905
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->interrupt()V

    .line 1906
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1911
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    .line 1914
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getCurrentPosition()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setTouchPlayingTime(I)V

    .line 1915
    if-eqz p1, :cond_1

    .line 1916
    const-string/jumbo v1, "MusicPlayerActivity "

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "ProgressStart Value : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1917
    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    .line 1918
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->isRunning:Z

    .line 1919
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->start()V

    .line 1921
    :cond_1
    return-void

    .line 1907
    :catch_0
    move-exception v0

    .line 1908
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1909
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "Progress Thread join exception"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setSoundView(Z)V
    .locals 3
    .param p1, "isShow"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1232
    if-eqz p1, :cond_0

    .line 1233
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLlSoundControl:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1234
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mRlEmpty:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1240
    :goto_0
    return-void

    .line 1236
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLlSoundControl:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1237
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mRlEmpty:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setSoundViewAutoDisappear(Z)V
    .locals 4
    .param p1, "isOn"    # Z

    .prologue
    const/4 v3, 0x1

    .line 1221
    if-eqz p1, :cond_0

    .line 1222
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_AUTO_DISAPPEAR_SOUND - start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    .line 1224
    const-wide/16 v1, 0xfa0

    .line 1223
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1229
    :goto_0
    return-void

    .line 1226
    :cond_0
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MSG_AUTO_DISAPPEAR_SOUND - removing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setTouchPlayingTime(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 1601
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlayingTime:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1602
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1603
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusicDuration()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setMax(I)V

    .line 1604
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setProgress(I)V

    .line 1605
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->changeToMinute(I)Ljava/lang/String;

    move-result-object v0

    .line 1606
    .local v0, "ct":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlayingTime:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1608
    .end local v0    # "ct":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setUnkownInfo()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 959
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 960
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setAllMusicShuffleList()Z

    move-result v1

    if-nez v1, :cond_0

    .line 961
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mUnknownValue:Z

    if-nez v1, :cond_0

    .line 962
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mUnknownValue:Z

    .line 963
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 969
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    .line 970
    const v3, 0x7f0a036d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 969
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 971
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mArtistName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    .line 972
    const v3, 0x7f0a036e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 971
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 973
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlayingTime:Landroid/widget/TextView;

    const-string/jumbo v2, "00:00"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 974
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTotalTime:Landroid/widget/TextView;

    const-string/jumbo v2, "00:00"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 975
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDurationBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setProgress(I)V

    .line 977
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 979
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->drawUnknownBitmap()V

    .line 980
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V

    .line 983
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 984
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 985
    const v2, 0x7f0a0117

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 986
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 987
    new-instance v2, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$15;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$15;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 986
    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1009
    .end local v0    # "systemTurnText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setVolumeUI()V
    .locals 5

    .prologue
    .line 916
    const/4 v1, 0x0

    .line 917
    .local v1, "volume":I
    const/4 v0, 0x0

    .line 918
    .local v0, "maxVol":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 919
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMusicVolume()I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVolume:I

    .line 920
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMaxMusicVolume()I

    move-result v0

    .line 922
    :cond_0
    const-string/jumbo v2, "JINSEIL"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "JINSEIL "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTvVolumeInfo:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 924
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setMax(I)V

    .line 925
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBar:Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/music/CustomSeekBar;->setProgress(I)V

    .line 926
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTvVolumeInfo:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 928
    :cond_1
    if-nez v1, :cond_2

    .line 929
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBtn:Landroid/widget/Button;

    const v3, 0x7f0203b4

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 938
    :goto_0
    return-void

    .line 932
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVolumeBtn:Landroid/widget/Button;

    .line 933
    const v3, 0x7f0203f4

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method protected CarAppFinishFunc()V
    .locals 2

    .prologue
    .line 429
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    const-string/jumbo v0, "MusicPlayerActivity"

    .line 431
    const-string/jumbo v1, "CarAppFinishFunc() - isFinishing() == true"

    .line 430
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :goto_0
    return-void

    .line 435
    :cond_0
    const-string/jumbo v0, "MusicPlayerActivity"

    const-string/jumbo v1, "CarAppFinishFunc()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->CarAppFinishFunc()V

    goto :goto_0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 1472
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V

    .line 1473
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V

    .line 1475
    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$22(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 1476
    const v0, 0x7f040025

    const v1, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->overridePendingTransition(II)V

    .line 1477
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 682
    const-string/jumbo v0, "MusicPlayerActivity "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " onBackPressed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 687
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 688
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->showVoiceLayout()V

    .line 689
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    .line 690
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 699
    :goto_0
    return-void

    .line 695
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 696
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->finish()V

    .line 698
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1671
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 1672
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1774
    :goto_0
    :pswitch_0
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 1776
    :goto_1
    return-void

    .line 1674
    :pswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isShuffled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1675
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setShuffle(Z)V

    .line 1676
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 1677
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    .line 1678
    const v1, 0x7f0a0378

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1680
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setShuffle(Z)V

    .line 1681
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 1682
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;

    .line 1683
    const v1, 0x7f0a0377

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1687
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->progressUpdate()V

    .line 1688
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z

    .line 1690
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1692
    const v0, 0x7f0a036b

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1691
    invoke-static {p0, v0, v2}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1693
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1697
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1698
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 1699
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "MUSICService Touch User Setting Pause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1700
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    .line 1701
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 1702
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    .line 1703
    const v1, 0x7f0a0376

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1705
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V

    goto/16 :goto_0

    .line 1707
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1708
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "cancel dialog by touch play button"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1709
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 1710
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 1711
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 1713
    :cond_3
    const-string/jumbo v0, "MusicPlayerActivity "

    .line 1714
    const-string/jumbo v1, "MusicService setMusicPlayerVolume by MusicPlayerActivity User Touched"

    .line 1713
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1715
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 1716
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->playPlayer()V

    .line 1717
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 1718
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPlaypauseBtn:Landroid/widget/Button;

    .line 1719
    const v1, 0x7f0a0375

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1725
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V

    goto/16 :goto_0

    .line 1729
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLlSoundControl:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 1730
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    .line 1731
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    .line 1732
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "JINSEIL volume_btn_player volume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1733
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V

    goto/16 :goto_0

    .line 1735
    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    .line 1736
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    goto/16 :goto_0

    .line 1740
    :pswitch_4
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    .line 1741
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    goto/16 :goto_0

    .line 1745
    :pswitch_5
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    if-eqz v0, :cond_5

    .line 1746
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V

    .line 1747
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    .line 1748
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    goto/16 :goto_1

    .line 1751
    :cond_5
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    .line 1752
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    .line 1753
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z

    .line 1754
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setNextPrevButtonProcess(Z)V

    .line 1755
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V

    goto/16 :goto_0

    .line 1760
    :pswitch_6
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    if-eqz v0, :cond_6

    .line 1761
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V

    .line 1762
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    .line 1763
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    goto/16 :goto_1

    .line 1766
    :cond_6
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    .line 1767
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    .line 1768
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z

    .line 1769
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setNextPrevButtonProcess(Z)V

    .line 1770
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V

    goto/16 :goto_0

    .line 1672
    :pswitch_data_0
    .packed-switch 0x7f0900d6
        :pswitch_3
        :pswitch_6
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 570
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 572
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isTTSState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVoiceLayoutState:Z

    .line 576
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initLayout()V

    .line 577
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initSearchTextField()V

    .line 578
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->checkSearchLayoutState()V

    .line 580
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVoiceLayoutState:Z

    if-eqz v0, :cond_1

    .line 581
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setTTSState()V

    .line 582
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mVoiceLayoutState:Z

    .line 584
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 175
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "onCreate"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "onCreate"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 179
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->isAppInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 180
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "Appication start to terminate. Finish Activity!!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->finish()V

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 189
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 190
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCallMultiwindow()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->multiWindowAccess:Z

    .line 193
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSearchResultFlag(Z)V

    .line 194
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 195
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    .line 196
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "@@@ drivelink framework is not initialized()"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    .line 200
    :cond_3
    const v1, 0x7f03001e

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setContentView(I)V

    .line 201
    const v1, 0x7f0a0253

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setTitle(I)V

    .line 205
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    .line 207
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v1

    .line 208
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 207
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->addDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 212
    new-instance v1, Lcom/sec/android/automotive/drivelink/music/CollectColor;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/music/CollectColor;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

    .line 214
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initView()V

    .line 216
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "JINSEIL onCreate volume"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V

    .line 222
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-nez v1, :cond_8

    .line 223
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "MusicPlayerActivity "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, " MUSIC service Null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    .line 233
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgHandler:Landroid/os/Handler;

    .line 242
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 273
    new-instance v2, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setOnVoiceMusicPlayerActionBarListener(Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    .line 298
    invoke-virtual {v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->isMicDisplayed()Z

    move-result v2

    .line 297
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setMicDisplayed(Z)V

    .line 299
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 300
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 303
    :cond_4
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V

    .line 306
    :try_start_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 307
    new-instance v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    .line 308
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mProgThread:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$ProgressThread;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 315
    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initSearchTextField()V

    .line 317
    if-eqz p1, :cond_6

    .line 318
    const-string/jumbo v1, "SIP_SEARCH_MODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    .line 319
    const-string/jumbo v1, "SIP_SEARCH_TEXT"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchedText:Ljava/lang/String;

    .line 322
    :cond_6
    const/high16 v1, 0x7f040000

    const v2, 0x7f040025

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->overridePendingTransition(II)V

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->moveoutOverlayService()V

    .line 326
    if-eqz p1, :cond_7

    .line 327
    const-string/jumbo v1, "IS_ERROR_STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 328
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    .line 329
    const-string/jumbo v2, "ERROR_STRING"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 328
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->displayError(Ljava/lang/CharSequence;)V

    .line 332
    :cond_7
    if-eqz p1, :cond_0

    .line 333
    const-string/jumbo v1, "IS_TTS_STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setTTSState()V

    goto/16 :goto_0

    .line 227
    :cond_8
    :try_start_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->registerListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 310
    :catch_1
    move-exception v0

    .line 311
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v1, "MusicPlayerActivity "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, " MUSIC service Null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 446
    const-string/jumbo v0, "MusicPlayerActivity "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->stopTTSBarAnimation()V

    .line 452
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V

    .line 453
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V

    .line 455
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->item_played:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 456
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mUnknownValue:Z

    .line 458
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 459
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 460
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->recycleBitmap(Landroid/widget/ImageView;)V

    .line 465
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 466
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 467
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    .line 469
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->unregisterListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V

    .line 470
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 471
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 472
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->removeDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 477
    :cond_2
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 478
    return-void

    .line 474
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v2, 0xea63

    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 1925
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V

    .line 1927
    packed-switch p1, :pswitch_data_0

    .line 1953
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1929
    :pswitch_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    .line 1931
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    .line 1932
    const v2, 0xea60

    .line 1931
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    .line 1935
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "JINSEIL volume_up volume"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1936
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V

    .line 1937
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    .line 1938
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    goto :goto_0

    .line 1941
    :pswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    .line 1943
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "JINSEIL volume_down volume"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1944
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    .line 1945
    const v2, 0xea61

    .line 1944
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    .line 1948
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V

    .line 1949
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V

    .line 1950
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    goto :goto_0

    .line 1927
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 1958
    sparse-switch p1, :sswitch_data_0

    .line 1969
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1960
    :sswitch_0
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    goto :goto_0

    .line 1963
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V

    goto :goto_0

    .line 1966
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->onBackPressed()V

    goto :goto_0

    .line 1958
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1647
    const-string/jumbo v2, "MusicPlayerActivity "

    const-string/jumbo v3, "Carmode_Music_onLongClick "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1649
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 1666
    :cond_1
    :goto_0
    return v0

    .line 1652
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1653
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setFFREInit()V

    .line 1655
    :cond_3
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z

    .line 1656
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevBtn:Landroid/widget/Button;

    if-ne p1, v2, :cond_4

    .line 1657
    new-instance v2, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-direct {v2, v0}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;-><init>(Z)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    .line 1661
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    if-eqz v2, :cond_1

    .line 1662
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    iput-boolean v1, v2, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 1663
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->start()V

    goto :goto_0

    .line 1659
    :cond_4
    new-instance v2, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-direct {v2, v1}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;-><init>(Z)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 441
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 442
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 498
    const-string/jumbo v0, "MusicPlayerActivity "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bmActive:Z

    .line 503
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchedText:Ljava/lang/String;

    .line 506
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPause()V

    .line 507
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 511
    const-string/jumbo v1, "MusicPlayerActivity "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, " onResume"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "inResume"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 515
    invoke-interface {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 516
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "@@@ drivelink framework is not initialized()"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V

    .line 524
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setMusicInfo()V

    .line 529
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bmActive:Z

    .line 530
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setContentDescription()V

    .line 531
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 534
    const-string/jumbo v1, "DM_MUSIC_PLAYER"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 535
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-nez v0, :cond_1

    .line 542
    :goto_0
    return-void

    .line 539
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 541
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->checkSearchLayoutState()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 2182
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2186
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2189
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchedText:Ljava/lang/String;

    .line 2190
    const-string/jumbo v0, "SIP_SEARCH_MODE"

    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2191
    const-string/jumbo v0, "SIP_SEARCH_TEXT"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchedText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2199
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isErrorState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2200
    const-string/jumbo v0, "IS_ERROR_STATE"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2201
    const-string/jumbo v0, "ERROR_STRING"

    .line 2202
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->getErrorCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 2201
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2205
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->isTTSState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206
    const-string/jumbo v0, "IS_TTS_STATE"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2211
    :cond_1
    return-void
.end method

.method protected reloadLayout()Z
    .locals 1

    .prologue
    .line 588
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initLayout()V

    .line 589
    const/4 v0, 0x0

    return v0
.end method

.method public setBtnEnabled(Z)V
    .locals 2
    .param p1, "onOff"    # Z

    .prologue
    .line 1973
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicDim:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1995
    :goto_0
    return-void

    .line 1977
    :cond_0
    if-eqz p1, :cond_1

    .line 1978
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicDim:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1986
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicDim:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1987
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicDim:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setDayMode()V
    .locals 5

    .prologue
    const v4, 0x7f0202f9

    const v3, 0x7f02003e

    .line 1504
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setDayMode()V

    .line 1505
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isNight:Z

    .line 1506
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setDayMode()V

    .line 1507
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1508
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mDefaultBitmap:Z

    if-eqz v0, :cond_1

    .line 1509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackground:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1510
    const v2, 0x7f080078

    .line 1509
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 1515
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1518
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVoiceBackground:Landroid/widget/ImageView;

    const v1, 0x7f02003f

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->drawBigImage(Landroid/widget/ImageView;I)V

    .line 1519
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->drawBigImage(Landroid/widget/ImageView;I)V

    .line 1522
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 1523
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1537
    :goto_1
    return-void

    .line 1512
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackground:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

    .line 1513
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getImage(Landroid/graphics/Bitmap;)I

    move-result v1

    .line 1512
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 1526
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVoiceBackground:Landroid/widget/ImageView;

    .line 1527
    const v1, 0x7f0202fa

    .line 1526
    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->drawBigImage(Landroid/widget/ImageView;I)V

    .line 1528
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->drawBigImage(Landroid/widget/ImageView;I)V

    .line 1531
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 1532
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    .line 1481
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setNightMode()V

    .line 1482
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->isNight:Z

    .line 1483
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->setNightMode()V

    .line 1484
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1485
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackground:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getImage(Landroid/graphics/Bitmap;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 1487
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1488
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVoiceBackground:Landroid/widget/ImageView;

    .line 1489
    const v1, 0x7f02003f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1490
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 1491
    const v1, 0x7f020041

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1500
    :goto_0
    return-void

    .line 1493
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicVoiceBackground:Landroid/widget/ImageView;

    .line 1494
    const v1, 0x7f0202fa

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1495
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 1496
    const v1, 0x7f0202fd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method
