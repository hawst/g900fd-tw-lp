.class public Lcom/sec/android/automotive/drivelink/location/LBHMapViewActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;
.source "LBHMapViewActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 49
    const v0, 0x7f030010

    return v0
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v2, 0x7f090008

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LBHMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 21
    .local v0, "map":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 22
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 25
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 29
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onResume()V

    .line 30
    const v2, 0x7f090008

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LBHMapViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 31
    .local v0, "map":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 32
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 33
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 34
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseMapActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 44
    return-void
.end method
