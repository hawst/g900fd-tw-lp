.class public Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBluetoothState;
.super Ljava/lang/Object;
.source "ConnectivityManagerBluetoothState.java"


# static fields
.field public static final CONNECTION_COMPLETE:I = 0x6c

.field public static final CONNECTION_ESTABLISHMENT:I = 0x6a

.field public static final CONNECTION_TERMINATING:I = 0x6b

.field public static final INITIAL_STATE_OFF:I = 0x65

.field public static final INITIAL_STATE_ON:I = 0x66

.field public static final PAIRING_DEVICES:I = 0x69

.field public static final PASSKEY_VERIFICATION:I = 0x68

.field public static final PERIPHERAL_SEARCH:I = 0x67


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
