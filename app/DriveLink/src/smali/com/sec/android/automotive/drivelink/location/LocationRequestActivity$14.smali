.class Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;
.super Ljava/lang/Object;
.source "LocationRequestActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->updateContacts()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 725
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 707
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->isLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Z)V

    .line 709
    if-nez p1, :cond_1

    .line 710
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 711
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    .line 712
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getInitialPosition()I

    move-result v1

    .line 711
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 720
    :cond_0
    :goto_0
    return-void

    .line 715
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 716
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    .line 717
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 715
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 700
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 701
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 700
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 702
    return-void
.end method
