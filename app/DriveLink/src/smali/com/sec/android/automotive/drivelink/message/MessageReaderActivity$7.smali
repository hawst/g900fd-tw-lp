.class Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$7;
.super Ljava/lang/Object;
.source "MessageReaderActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 209
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 218
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 219
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 220
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->replayReader()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$15(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    .line 222
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/DLApplication;->DL_DEMO:Z

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->finish()V

    .line 225
    :cond_0
    return-void
.end method
