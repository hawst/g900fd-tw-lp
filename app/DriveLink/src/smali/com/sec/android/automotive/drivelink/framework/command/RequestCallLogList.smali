.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestCallLogList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItemCount:I

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "itemCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mContext:Landroid/content/Context;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mList:Ljava/util/ArrayList;

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mItemCount:I

    .line 20
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mContext:Landroid/content/Context;

    .line 21
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mItemCount:I

    .line 22
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mContext:Landroid/content/Context;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mList:Ljava/util/ArrayList;

    .line 47
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mContext:Landroid/content/Context;

    .line 27
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->rescanContactDB(Landroid/content/Context;)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mItemCount:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getCallLogList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mList:Ljava/util/ArrayList;

    .line 32
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkCallLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;

    move-result-object v0

    .line 38
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;
    if-eqz v0, :cond_0

    .line 39
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestCallLogList;->mList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;->onResponseRequestCallLogList(Ljava/util/ArrayList;)V

    .line 41
    :cond_0
    return-void
.end method
