.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getOnEditorActionListener()Landroid/widget/TextView$OnEditorActionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;)Lcom/sec/android/automotive/drivelink/location/LocationActivity;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    return-object v0
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f0a01fb

    const/16 v8, 0x8

    const-wide/16 v6, 0x258

    const/4 v3, 0x0

    .line 347
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const-string/jumbo v5, "input_method"

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 349
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;)V

    .line 363
    .local v0, "h":Landroid/os/Handler;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setBtnSearchInvisible()V

    .line 364
    const/4 v4, 0x3

    if-ne p2, v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->IsOffline()Z
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$7(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 366
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getTextFromEditTextSearch()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 369
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    .line 370
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 368
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 372
    const-string/jumbo v5, "PREF_SETTINGS_MY_NAVIGATION"

    .line 371
    invoke-virtual {v4, v5, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 375
    .local v2, "mapID":I
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->isProviderEnabled()Z
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$8(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 376
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getEnableGPSPDialog()V

    .line 449
    .end local v2    # "mapID":I
    :goto_0
    return v3

    .line 380
    .restart local v2    # "mapID":I
    :cond_0
    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v2, v4, :cond_3

    .line 381
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->hasGoogleAccount(Landroid/content/Context;)Z
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$9(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 383
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    .line 382
    invoke-virtual {v1, v4, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 384
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 386
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->hideLocationLayout()V

    .line 387
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showVoiceLayout()V

    .line 390
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v4

    .line 391
    invoke-virtual {v4, v6, v7}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 393
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/widget/FrameLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 394
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    iput-boolean v3, v4, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 395
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    iput-object v10, v4, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 397
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 399
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    .line 398
    invoke-virtual {v1, v4, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 404
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 405
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 406
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 407
    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 405
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 409
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    invoke-virtual {v4, v9}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showToast(I)V

    .line 410
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v4

    .line 411
    invoke-virtual {v4, v6, v7}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto :goto_0

    .line 417
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    .line 416
    invoke-virtual {v1, v4, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 418
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 424
    .end local v2    # "mapID":I
    :cond_4
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 428
    :cond_5
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->hideLocationLayout()V

    .line 431
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v4

    .line 432
    invoke-virtual {v4, v6, v7}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 434
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/widget/FrameLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 435
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    iput-boolean v3, v4, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 436
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    iput-object v10, v4, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 438
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 440
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    .line 439
    invoke-virtual {v1, v4, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 444
    :cond_6
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 445
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v4

    .line 446
    invoke-virtual {v4}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 447
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showVoiceLayout()V

    goto/16 :goto_0
.end method
