.class Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->disconnectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field stateA2DP:I

.field stateHFP:I

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    .line 1157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->stateA2DP:I

    .line 1159
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->stateHFP:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1164
    const/4 v1, 0x0

    .line 1165
    .local v1, "internalTimer":I
    :goto_0
    const/16 v2, 0x1388

    if-le v1, v2, :cond_1

    .line 1194
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    monitor-enter v3

    .line 1195
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 1194
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1197
    return-void

    .line 1166
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v2, :cond_2

    .line 1167
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 1168
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$19(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    .line 1167
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->stateA2DP:I

    .line 1170
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->stateA2DP:I

    if-eqz v2, :cond_0

    .line 1175
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v2, :cond_3

    .line 1176
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 1177
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$19(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    .line 1176
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->stateHFP:I

    .line 1179
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;->stateHFP:I

    if-eqz v2, :cond_0

    .line 1185
    :cond_3
    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1186
    add-int/lit8 v1, v1, 0x64

    goto :goto_0

    .line 1187
    :catch_0
    move-exception v0

    .line 1188
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1189
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "InterruptedException"

    invoke-static {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1194
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method
