.class public Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;
.super Ljava/lang/Object;
.source "OnDriveLinkConnectivityBaseListener.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;

    .line 10
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 9
    sput-object v0, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;->TAG:Ljava/lang/String;

    .line 10
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyBluetoothDeviceConnected()V
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onNotifyBluetoothDeviceConnected"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    return-void
.end method

.method public onNotifyBluetoothDeviceDisconnected()V
    .locals 2

    .prologue
    .line 105
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onNotifyBluetoothDeviceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void
.end method

.method public onNotifyBluetoothDevicePaired()V
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onNotifyBluetoothDevicePaired"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    return-void
.end method

.method public onNotifyBluetoothDeviceUnpaired()V
    .locals 2

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/listener/OnDriveLinkConnectivityBaseListener;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onNotifyBluetoothDeviceUnpaired"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void
.end method

.method public onResponseRequestBluetoothConnectDevice(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 70
    return-void
.end method

.method public onResponseRequestBluetoothConnectionState(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 82
    return-void
.end method

.method public onResponseRequestBluetoothDisconnectDevice(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 76
    return-void
.end method

.method public onResponseRequestBluetoothMakeDeviceDiscoverable()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public onResponseRequestBluetoothPairDevice(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 58
    return-void
.end method

.method public onResponseRequestBluetoothPairingNSSPAnswer(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 46
    return-void
.end method

.method public onResponseRequestBluetoothPairingSSPAnswer(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 52
    return-void
.end method

.method public onResponseRequestBluetoothSearchDiscoverableDevices()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public onResponseRequestBluetoothSearchPairedDevices()V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method public onResponseRequestBluetoothStartAdapter(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 16
    return-void
.end method

.method public onResponseRequestBluetoothStopAdapter()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public onResponseRequestBluetoothUnpairDevice(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 64
    return-void
.end method
