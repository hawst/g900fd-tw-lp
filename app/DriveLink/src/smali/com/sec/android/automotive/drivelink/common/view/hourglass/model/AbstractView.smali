.class public abstract Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;
.super Landroid/view/View;
.source "AbstractView.java"


# instance fields
.field private final STROKE_WIDTH:F

.field private mContentHeight:F

.field private mContentWidth:F

.field private mPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mScaleMatrix:Landroid/graphics/Matrix;

.field protected mStrokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentWidth"    # F
    .param p3, "contentHeight"    # F

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 15
    const v0, 0x4019999a    # 2.4f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->STROKE_WIDTH:F

    .line 27
    invoke-direct {p0, p2, p3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->init(FF)V

    .line 28
    return-void
.end method

.method private init(FF)V
    .locals 1
    .param p1, "contentWidth"    # F
    .param p2, "contentHeight"    # F

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mContentWidth:F

    .line 37
    iput p2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mContentHeight:F

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->createPathInfos()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    .line 40
    return-void
.end method

.method private setProperty(II)V
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 69
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mScaleMatrix:Landroid/graphics/Matrix;

    if-eqz v5, :cond_1

    .line 88
    :cond_0
    return-void

    .line 73
    :cond_1
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mScaleMatrix:Landroid/graphics/Matrix;

    .line 74
    int-to-float v5, p1

    iget v6, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mContentWidth:F

    div-float v3, v5, v6

    .line 75
    .local v3, "sx":F
    int-to-float v5, p2

    iget v6, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mContentHeight:F

    div-float v4, v5, v6

    .line 77
    .local v4, "sy":F
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mScaleMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 79
    const v5, 0x4019999a    # 2.4f

    mul-float/2addr v5, v3

    iput v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mStrokeWidth:F

    .line 81
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 82
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 83
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 84
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    .line 85
    .local v2, "info":Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    iget-object v5, v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PATH:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mScaleMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected calculate(FFF)F
    .locals 2
    .param p1, "from"    # F
    .param p2, "to"    # F
    .param p3, "ratio"    # F

    .prologue
    .line 57
    sub-float v0, p2, p1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p3

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    return v0
.end method

.method protected abstract createPathInfos()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract modifyPathInfo(Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;F)V
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->getHeight()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->setProperty(II)V

    .line 94
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 95
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 96
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 101
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 97
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    .line 98
    .local v2, "info":Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;
    iget-object v3, v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PATH:Landroid/graphics/Path;

    iget-object v4, v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;->PAINT:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public render(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 43
    invoke-virtual {p0, p1, p1, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->render(FFF)V

    .line 44
    return-void
.end method

.method public render(FFF)V
    .locals 3
    .param p1, "ratio"    # F
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    .line 48
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 49
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->invalidate()V

    .line 54
    return-void

    .line 50
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mPaths:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;

    invoke-virtual {p0, v2, p1}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->modifyPathInfo(Lcom/sec/android/automotive/drivelink/common/view/hourglass/type/PathInfo;F)V

    .line 49
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected transform(Landroid/graphics/Path;)V
    .locals 1
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mScaleMatrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/hourglass/model/AbstractView;->mScaleMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 65
    :cond_0
    return-void
.end method
