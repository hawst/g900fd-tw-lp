.class public final enum Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
.super Ljava/lang/Enum;
.source "NumberPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "PhoneNumberDisplayType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

.field public static final enum FAX:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

.field public static final enum HOME:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

.field public static final enum MOBILE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

.field public static final enum OFFICE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

.field public static final enum PHONE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;


# instance fields
.field private callDrawable:I

.field private phoneDrawableLandscape:I

.field private phoneDrawableNight:I

.field private phoneDrawablePortrait:I

.field private stringResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 294
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    const-string/jumbo v1, "HOME"

    const/4 v2, 0x0

    const v3, 0x7f0a035c

    .line 295
    const v4, 0x7f020227

    .line 296
    const v5, 0x7f020227

    .line 297
    const v6, 0x7f02022a

    .line 298
    const v7, 0x7f02012e

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;-><init>(Ljava/lang/String;IIIIII)V

    .line 294
    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->HOME:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    .line 298
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    const-string/jumbo v1, "MOBILE"

    const/4 v2, 0x1

    .line 299
    const v3, 0x7f0a035d

    .line 300
    const v4, 0x7f02022b

    .line 301
    const v5, 0x7f02022b

    .line 302
    const v6, 0x7f02022c

    .line 303
    const v7, 0x7f020130

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;-><init>(Ljava/lang/String;IIIIII)V

    .line 298
    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->MOBILE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    .line 303
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    const-string/jumbo v1, "OFFICE"

    const/4 v2, 0x2

    .line 304
    const v3, 0x7f0a035e

    .line 305
    const v4, 0x7f02022d

    .line 306
    const v5, 0x7f02022d

    .line 307
    const v6, 0x7f020230

    .line 308
    const v7, 0x7f02012f

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;-><init>(Ljava/lang/String;IIIIII)V

    .line 303
    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->OFFICE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    .line 308
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    const-string/jumbo v1, "FAX"

    const/4 v2, 0x3

    .line 309
    const v3, 0x7f0a035f

    .line 310
    const v4, 0x7f020225

    .line 311
    const v5, 0x7f020225

    .line 312
    const v6, 0x7f020226

    .line 313
    const v7, 0x7f020130

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;-><init>(Ljava/lang/String;IIIIII)V

    .line 308
    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->FAX:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    .line 313
    new-instance v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    const-string/jumbo v1, "PHONE"

    const/4 v2, 0x4

    .line 314
    const v3, 0x7f0a0360

    .line 315
    const v4, 0x7f020231

    .line 316
    const v5, 0x7f020231

    .line 317
    const v6, 0x7f020232

    .line 318
    const v7, 0x7f020130

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;-><init>(Ljava/lang/String;IIIIII)V

    .line 313
    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->PHONE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    .line 293
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->HOME:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->MOBILE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->OFFICE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->FAX:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->PHONE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p3, "stringResource"    # I
    .param p4, "phoneDrawableLandscape"    # I
    .param p5, "phoneDrawablePortrait"    # I
    .param p6, "phoneDrawableNight"    # I
    .param p7, "callDrawable"    # I

    .prologue
    .line 332
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 335
    iput p3, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->stringResource:I

    .line 336
    iput p4, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->phoneDrawableLandscape:I

    .line 337
    iput p5, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->phoneDrawablePortrait:I

    .line 338
    iput p6, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->phoneDrawableNight:I

    .line 339
    iput p7, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->callDrawable:I

    .line 340
    return-void
.end method

.method public static getPhoneNumberDisplayType(I)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
    .locals 1
    .param p0, "phoneType"    # I

    .prologue
    .line 416
    packed-switch p0, :pswitch_data_0

    .line 433
    :pswitch_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->PHONE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    :goto_0
    return-object v0

    .line 418
    :pswitch_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->HOME:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    goto :goto_0

    .line 420
    :pswitch_2
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->OFFICE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    goto :goto_0

    .line 422
    :pswitch_3
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->MOBILE:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    goto :goto_0

    .line 426
    :pswitch_4
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->FAX:Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    goto :goto_0

    .line 416
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCallDrawable()I
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->callDrawable:I

    return v0
.end method

.method public getPhoneDrawableLandscape()I
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->phoneDrawableLandscape:I

    return v0
.end method

.method public getPhoneDrawableNight()I
    .locals 1

    .prologue
    .line 388
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->phoneDrawableNight:I

    return v0
.end method

.method public getPhoneDrawablePortrait()I
    .locals 1

    .prologue
    .line 384
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->phoneDrawablePortrait:I

    return v0
.end method

.method public getStringResource()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/NumberPageAdapter$PhoneNumberDisplayType;->stringResource:I

    return v0
.end method
