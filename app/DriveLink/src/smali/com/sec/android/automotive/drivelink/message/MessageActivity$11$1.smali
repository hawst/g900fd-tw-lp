.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    .line 606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 664
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-void
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/16 v9, 0x28

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 611
    const-string/jumbo v3, "i"

    .line 612
    const-string/jumbo v4, "onResponseRequestSearchedContactList"

    .line 613
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "search : searched! "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 614
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 613
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 611
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$39(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$53(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 616
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 617
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v3

    const/16 v4, 0x43

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    .line 658
    :goto_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$53(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 659
    return-void

    .line 635
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 637
    .local v2, "size":I
    if-le v2, v9, :cond_1

    .line 638
    const/16 v2, 0x28

    .line 640
    invoke-virtual {p1, v7, v9}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 642
    .local v1, "dl":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-static {v1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 648
    .end local v1    # "dl":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :goto_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$19(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/List;)V

    .line 650
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v3

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v3

    .line 651
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 653
    const v5, 0x7f0a0244

    .line 652
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 654
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v6

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v6

    .line 655
    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    aput-object v6, v5, v8

    .line 651
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 650
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 656
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$11;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v3

    const/16 v4, 0x42

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    goto :goto_0

    .line 645
    .end local v0    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    invoke-static {p1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 644
    .restart local v0    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    goto :goto_1
.end method
