.class public Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
.super Ljava/lang/Object;
.source "DLRecommendedContactItem.java"


# instance fields
.field private mLogHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;",
            ">;"
        }
    .end annotation
.end field

.field private phoneNumber:Ljava/lang/String;

.field private weight:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->weight:I

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->mLogHashMap:Ljava/util/HashMap;

    .line 12
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->phoneNumber:Ljava/lang/String;

    .line 13
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->weight:I

    .line 15
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->mLogHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 16
    return-void
.end method


# virtual methods
.method public addWeight(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->weight:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->weight:I

    .line 28
    return-void
.end method

.method public addWeight(Ljava/lang/String;I)V
    .locals 3
    .param p1, "categoryName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 31
    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->weight:I

    add-int/2addr v2, p2

    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->weight:I

    .line 33
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->mLogHashMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;

    .line 34
    .local v1, "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;
    if-nez v1, :cond_0

    .line 35
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;

    .end local v1    # "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;
    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;-><init>(Ljava/lang/String;I)V

    .line 38
    .restart local v1    # "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->getValue()I

    move-result v0

    .line 39
    .local v0, "_value":I
    add-int/2addr v0, p2

    .line 40
    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->setValue(I)V

    .line 42
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->mLogHashMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getWeight()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->weight:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 47
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 48
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x1

    .line 50
    .local v0, "isFirstTime":Z
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->mLogHashMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 60
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 50
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 51
    .local v2, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->mLogHashMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;

    .line 52
    .local v1, "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;
    if-nez v0, :cond_1

    .line 53
    const-string/jumbo v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->getCategoryName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->getValue()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 57
    const/4 v0, 0x0

    goto :goto_0
.end method
