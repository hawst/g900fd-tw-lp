.class Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$11;
.super Ljava/lang/Object;
.source "MessageComposerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getComposerTextClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 993
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 994
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v1

    .line 995
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->isComposerInputMethodShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 998
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1000
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 1001
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 1002
    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 1003
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->showInputMethod()V

    .line 1016
    :cond_0
    :goto_0
    return-void

    .line 1006
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 1008
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 1009
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1011
    const v2, 0x7f0a0199

    .line 1010
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1012
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method
