.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$10;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 753
    const-string/jumbo v2, "JINSEIL"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "JINSEIL = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 756
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 757
    .local v0, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 759
    .local v1, "y":F
    cmpg-float v2, v0, v5

    if-lez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-gez v2, :cond_0

    cmpg-float v2, v1, v5

    if-lez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_2

    .line 761
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->longPressed:Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$36(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 762
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {v2, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->onLongClick(Landroid/view/View;)Z

    .line 764
    :cond_1
    const/4 v2, 0x1

    .line 767
    .end local v0    # "x":F
    .end local v1    # "y":F
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
