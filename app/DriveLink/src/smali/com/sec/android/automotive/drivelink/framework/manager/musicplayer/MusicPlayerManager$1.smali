.class Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$1;
.super Ljava/lang/Object;
.source "MusicPlayerManager.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->initialize(Landroid/content/Context;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlayerStatus:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;->STOPPED:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$MusicPlayerStatus;

    if-ne v0, v1, :cond_0

    .line 110
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->handlePlayOnCompletion()V

    goto :goto_0
.end method
