.class Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;
.super Ljava/lang/Object;
.source "UpdateProgressActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseDownloadProgress(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V
    .locals 7
    .param p1, "progress"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;

    .prologue
    const/high16 v6, 0x100000

    .line 183
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;->getDownloadRate()J

    move-result-wide v3

    long-to-int v0, v3

    .line 184
    .local v0, "downloadRate":I
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;->getDownloadSize()J

    move-result-wide v3

    long-to-int v1, v3

    .line 185
    .local v1, "remain":I
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;->getTotalSize()J

    move-result-wide v3

    long-to-int v2, v3

    .line 186
    .local v2, "total":I
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$2(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Download rate:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", Remain:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 187
    const-string/jumbo v5, ", Total:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 186
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    div-int/2addr v1, v6

    .line 190
    div-int/2addr v2, v6

    .line 192
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setRemain(II)V
    invoke-static {v3, v1, v2}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$5(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;II)V

    .line 193
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->progressBar:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$0(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 194
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setProgressData(I)V
    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$1(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;I)V

    .line 196
    return-void
.end method

.method public onResponseInstallStarted()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$2(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onResponseInstallStarted"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->txtStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$6(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0a0497

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 205
    return-void
.end method

.method public onResponseRequestCheckUpdateVersion(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 148
    return-void
.end method

.method public onResponseRequestUpdateApplicationFail(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .prologue
    const/4 v3, 0x0

    .line 170
    sput-boolean v3, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    .line 171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$2(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "bRequest : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$2(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResponseRequestUpdateApplicationFail : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$3(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0498

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 176
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$4(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x64

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 178
    return-void
.end method

.method public onResponseRequestUpdateApplicationSuccess()V
    .locals 3

    .prologue
    const/16 v1, 0x64

    .line 154
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->progressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$0(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->setProgressData(I)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$1(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;I)V

    .line 157
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    .line 159
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$2(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "bRequest : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->bRequest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->access$2(Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onResponseRequestUpdateApplicationSuccess"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;->finish()V

    .line 164
    return-void
.end method
