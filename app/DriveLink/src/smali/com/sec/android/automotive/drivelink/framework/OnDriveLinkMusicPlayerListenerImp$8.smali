.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;
.super Ljava/lang/Object;
.source "OnDriveLinkMusicPlayerListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->onMusicPlayerEnvInfoChanged(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

.field private final synthetic val$_musicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;->val$_musicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->mOnDriveLinkMusicPlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicPlayerListenerImp$8;->val$_musicPlayerEnvInfo:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayerEnvInfoChanged(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V

    .line 164
    :cond_0
    return-void
.end method
