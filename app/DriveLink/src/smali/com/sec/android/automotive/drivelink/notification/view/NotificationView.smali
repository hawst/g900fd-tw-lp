.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.super Landroid/widget/LinearLayout;
.source "NotificationView.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;
.implements Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;
.implements Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;
.implements Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationView]"


# instance fields
.field private dismissRequested:Z

.field protected isMultiMode:Z

.field private mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

.field private notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

.field private useDefaultFlow:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->useDefaultFlow:Z

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->isMultiMode:Z

    .line 63
    invoke-direct {p0, p1, p4, p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->initView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 64
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->useDefaultFlow:Z

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->isMultiMode:Z

    .line 57
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->initView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 58
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->useDefaultFlow:Z

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->isMultiMode:Z

    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->initView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 52
    return-void
.end method

.method private addQuote(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 263
    if-nez p1, :cond_0

    .line 264
    const-string/jumbo v0, "\'\'"

    .line 267
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private initView(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "bMultiMode"    # Z

    .prologue
    .line 69
    if-eqz p2, :cond_0

    .line 70
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->setNotiFlowListener(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowListener;)V

    .line 72
    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->isMultiMode:Z

    .line 73
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotificationReadout()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setReadoutFlag(Landroid/content/Context;Z)Z

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    const-string/jumbo v0, "[NotificationView]"

    const-string/jumbo v1, "Noti Item is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private removeQuote(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 271
    if-nez p1, :cond_0

    .line 272
    const-string/jumbo v0, ""

    .line 275
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "^\"|\"$"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public cancelNotiFlow()V
    .locals 2

    .prologue
    .line 218
    const-string/jumbo v0, "[NotificationView]"

    const-string/jumbo v1, "Noti. Flow will cancel."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cancelNotiFlow()V

    .line 223
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    if-nez v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->cancelNotiFlow()V

    .line 111
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;->onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    .line 114
    :cond_0
    return-void
.end method

.method public dismiss(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 1
    .param p1, "cmd"    # Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    if-nez v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->cancelNotiFlow()V

    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-interface {v0, p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;->onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    .line 137
    :cond_0
    return-void
.end method

.method public dismissByAutoTimeout()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;->onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    .line 129
    :cond_0
    return-void
.end method

.method public dismissDirect(Z)V
    .locals 1
    .param p1, "gotoMulti"    # Z

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    if-nez v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->cancelNotiFlow()V

    .line 119
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-interface {v0, p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;->onRemoveDirect(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismissRequested:Z

    .line 122
    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 335
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->isMultiMode:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->needProcessBackKey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    const-string/jumbo v0, "[NotificationView]"

    .line 337
    const-string/jumbo v1, "dispatchKeyEvent: This Noti Item is in multi mode. process back key event."

    .line 336
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 339
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismiss()V

    .line 343
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    .line 143
    :cond_0
    const-string/jumbo v0, "[NotificationView]"

    const-string/jumbo v1, "noti item is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->IDLE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    goto :goto_0
.end method

.method public getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    return-object v0
.end method

.method protected getTTSText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 149
    const-string/jumbo v0, "[NotificationView]"

    .line 150
    const-string/jumbo v1, "WARN!! This method must be overridded for default timeout scenario"

    .line 149
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public hasNotiItem()Z
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNotiItemInfo()Z
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isAppeared()Z
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->isAppeared()Z

    move-result v0

    .line 168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected needProcessBackKey()Z
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x1

    return v0
.end method

.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 443
    return-void
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 473
    return-void
.end method

.method public onFlowCommandCustom(Ljava/lang/String;)Z
    .locals 1
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 460
    const/4 v0, 0x0

    return v0
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 407
    return-void
.end method

.method public onFlowCommandIgnore(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 413
    return-void
.end method

.method public onFlowCommandLookup(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 419
    return-void
.end method

.method public onFlowCommandNext(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 455
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 395
    return-void
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 449
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 437
    return-void
.end method

.method public onFlowCommandReset(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 425
    return-void
.end method

.method public onFlowCommandReturnMain()V
    .locals 0

    .prologue
    .line 467
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 431
    return-void
.end method

.method public onFlowCommandSend(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 401
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 389
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 353
    const-string/jumbo v0, "[NotificationView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "MicState:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 358
    :cond_0
    return-void
.end method

.method public onNotiCommandFinished(I)V
    .locals 1
    .param p1, "cmd"    # I

    .prologue
    .line 366
    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismiss()V

    .line 369
    :cond_0
    return-void
.end method

.method public onNotificationAppeared()V
    .locals 3

    .prologue
    .line 309
    const-string/jumbo v0, "[NotificationView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "NotiView  appeared."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->isAppeared()Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setAppeared(Z)V

    .line 313
    :cond_0
    return-void
.end method

.method public onNotificationDisappeared()V
    .locals 3

    .prologue
    .line 323
    const-string/jumbo v0, "[NotificationView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "NotiView  disappeared."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    return-void
.end method

.method public onNotificationRotating()V
    .locals 3

    .prologue
    .line 329
    const-string/jumbo v0, "[NotificationView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "NotiView  rotating."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    return-void
.end method

.method public onNotificationWillAppear()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 285
    const-string/jumbo v0, "[NotificationView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "NotiView will appear: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_1

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->restartNotiFlow()V

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->isAppeared()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->useDefaultFlow:Z

    if-eqz v0, :cond_2

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->startDefaultNotiTimeoutFlow()V

    .line 292
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setAppeared(Z)V

    goto :goto_0

    .line 294
    :cond_2
    const-string/jumbo v0, "[NotificationView]"

    const-string/jumbo v1, "don\'t use default Noti. Flow."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setAppeared(Z)V

    goto :goto_0
.end method

.method public onNotificationWillDisappear()V
    .locals 3

    .prologue
    .line 317
    const-string/jumbo v0, "[NotificationView]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "NotiView  will disappear."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    return-void
.end method

.method public pauseNotiFlow()V
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->pauseNotiFlow()V

    .line 229
    :cond_0
    return-void
.end method

.method public restartNotiFlow()V
    .locals 2

    .prologue
    .line 211
    const-string/jumbo v0, "[NotificationView]"

    const-string/jumbo v1, "Noti. Flow will restart."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->restartNotiFlow()V

    .line 215
    :cond_0
    return-void
.end method

.method public resumeNotiFlow()V
    .locals 2

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->PAUSE:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->resumeNotiFlow()V

    .line 235
    :cond_0
    return-void
.end method

.method protected setAppeared(Z)V
    .locals 1
    .param p1, "appeared"    # Z

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->setAppeared(Z)V

    .line 162
    :cond_0
    return-void
.end method

.method public varargs setButtonQuoted(Z[Landroid/widget/Button;)V
    .locals 6
    .param p1, "enable"    # Z
    .param p2, "buttons"    # [Landroid/widget/Button;

    .prologue
    .line 238
    if-eqz p2, :cond_0

    array-length v3, p2

    if-gtz v3, :cond_1

    .line 260
    :cond_0
    return-void

    .line 241
    :cond_1
    array-length v4, p2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, p2, v3

    .line 242
    .local v0, "button":Landroid/widget/Button;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    if-nez v5, :cond_3

    .line 241
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 245
    :cond_3
    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 247
    .local v2, "text":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 251
    const/4 v1, 0x0

    .line 252
    .local v1, "qtext":Ljava/lang/String;
    if-eqz p1, :cond_4

    .line 253
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->addQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 258
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 255
    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->removeQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public setNotiItem(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->notiItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 81
    return-void
.end method

.method public setOnNotificationRequestListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    .line 106
    return-void
.end method

.method public setReadoutFlag(Landroid/content/Context;Z)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flag"    # Z

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 377
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->setNotificationReadout(Z)V

    .line 379
    :cond_0
    return p2
.end method

.method protected setUseAutoTimeout(Z)V
    .locals 0
    .param p1, "useTimeout"    # Z

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->useDefaultFlow:Z

    .line 156
    return-void
.end method

.method public setVoiceFlowId(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 205
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->mNotificationRequestListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;->onSetVoiceFlowIdRequest(Ljava/lang/String;)V

    .line 208
    :cond_0
    return-void
.end method

.method protected startDefaultNotiTimeoutFlow()V
    .locals 2

    .prologue
    .line 174
    const-string/jumbo v0, "[NotificationView]"

    const-string/jumbo v1, "startDefaultNotiTimeoutFlow"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addAlert()V

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addAlert()V

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 197
    :cond_0
    return-void
.end method
