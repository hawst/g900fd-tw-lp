.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestDLServiceFinalize.java"


# instance fields
.field mContext:Landroid/content/Context;

.field mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

.field mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceInterface"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;
    .param p3, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 9
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mContext:Landroid/content/Context;

    .line 10
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 11
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .line 18
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mContext:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 20
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .line 21
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    .line 44
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->terminate(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->setInitializedFlag(Z)V

    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mOnDriveLinkEventListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkEventListener;->onResponseFinalize()V

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestDLServiceFinalize;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->stopThreadPool()V

    .line 37
    return-void
.end method
