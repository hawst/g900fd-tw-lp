.class public Lcom/sec/android/automotive/drivelink/common/view/easing/BounceEaseInOut;
.super Ljava/lang/Object;
.source "BounceEaseInOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BounceEaseInOut;->s:F

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BounceEaseInOut;->s:F

    .line 15
    return-void
.end method

.method private easeIn(FFFF)F
    .locals 2
    .param p1, "t"    # F
    .param p2, "b"    # F
    .param p3, "c"    # F
    .param p4, "d"    # F

    .prologue
    .line 30
    sub-float v0, p4, p1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/sec/android/automotive/drivelink/common/view/easing/BounceEaseInOut;->easeOut(FFFF)F

    move-result v0

    sub-float v0, p3, v0

    add-float/2addr v0, p2

    return v0
.end method

.method private easeOut(FFFF)F
    .locals 5
    .param p1, "t"    # F
    .param p2, "b"    # F
    .param p3, "c"    # F
    .param p4, "d"    # F

    .prologue
    const/high16 v4, 0x40f20000    # 7.5625f

    .line 18
    div-float/2addr p1, p4

    const v0, 0x3eba2e8c

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 19
    mul-float v0, v4, p1

    mul-float/2addr v0, p1

    mul-float/2addr v0, p3

    add-float/2addr v0, p2

    .line 25
    :goto_0
    return v0

    .line 20
    :cond_0
    const v0, 0x3f3a2e8c

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 21
    const v0, 0x3f0ba2e9

    sub-float/2addr p1, v0

    mul-float v0, v4, p1

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f400000    # 0.75f

    add-float/2addr v0, v1

    mul-float/2addr v0, p3

    add-float/2addr v0, p2

    goto :goto_0

    .line 22
    :cond_1
    float-to-double v0, p1

    const-wide v2, 0x3fed1745d1745d17L    # 0.9090909090909091

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 23
    const v0, 0x3f51745d

    sub-float/2addr p1, v0

    mul-float v0, v4, p1

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f700000    # 0.9375f

    add-float/2addr v0, v1

    mul-float/2addr v0, p3

    add-float/2addr v0, p2

    goto :goto_0

    .line 25
    :cond_2
    const v0, 0x3f745d17

    sub-float/2addr p1, v0

    mul-float v0, v4, p1

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f7c0000    # 0.984375f

    add-float/2addr v0, v1

    mul-float/2addr v0, p3

    add-float/2addr v0, p2

    goto :goto_0
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 6
    .param p1, "input"    # F

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 35
    cmpg-float v1, p1, v4

    if-gez v1, :cond_0

    .line 36
    mul-float v1, p1, v5

    invoke-direct {p0, v1, v3, v2, v2}, Lcom/sec/android/automotive/drivelink/common/view/easing/BounceEaseInOut;->easeIn(FFFF)F

    move-result v1

    mul-float/2addr v1, v4

    add-float v0, v1, v3

    .line 41
    .local v0, "r":F
    :goto_0
    return v0

    .line 38
    .end local v0    # "r":F
    :cond_0
    mul-float v1, p1, v5

    sub-float/2addr v1, v2

    invoke-direct {p0, v1, v3, v2, v2}, Lcom/sec/android/automotive/drivelink/common/view/easing/BounceEaseInOut;->easeOut(FFFF)F

    move-result v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v4

    add-float v0, v1, v3

    .restart local v0    # "r":F
    goto :goto_0
.end method
