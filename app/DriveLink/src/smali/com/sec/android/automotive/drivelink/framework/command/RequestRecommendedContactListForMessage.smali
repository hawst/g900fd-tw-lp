.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestRecommendedContactListForMessage.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItemCount:I

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mUseFavoriteContactList:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "itemCount"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mList:Ljava/util/ArrayList;

    .line 14
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mItemCount:I

    .line 15
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mUseFavoriteContactList:Z

    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    .line 22
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mItemCount:I

    .line 23
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mUseFavoriteContactList:Z

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;IZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "itemCount"    # I
    .param p4, "useFavoriteContactList"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mList:Ljava/util/ArrayList;

    .line 14
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mItemCount:I

    .line 15
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mUseFavoriteContactList:Z

    .line 31
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    .line 32
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mItemCount:I

    .line 33
    iput-boolean p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mUseFavoriteContactList:Z

    .line 34
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mList:Ljava/util/ArrayList;

    .line 66
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    .line 39
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->rescanContactDB(Landroid/content/Context;)V

    .line 42
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mUseFavoriteContactList:Z

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getRecommandationManagerForMessage()Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mItemCount:I

    .line 44
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getRecommendedList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mList:Ljava/util/ArrayList;

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mContext:Landroid/content/Context;

    .line 47
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getFavoriteContactList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkRecommendedContactForMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;

    move-result-object v0

    .line 57
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;
    if-eqz v0, :cond_0

    .line 58
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestRecommendedContactListForMessage;->mList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;->onResponseRequestRecommendedContactList(Ljava/util/ArrayList;)V

    .line 60
    :cond_0
    return-void
.end method
