.class public Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "MirrorLinkManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_ML_BOOT_COMPLETION:Ljava/lang/String; = "samsung.intent.action.MIRRORING_STARTED"

.field private static final ACTION_ML_SHUTDOWN_MSG:Ljava/lang/String; = "samsung.intent.action.MIRRORLINK_SHUTDOWN"

.field private static final MIRRORLINK_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.tmserver.service.TmsService"

.field private static final SHUTDOWN_ML_SERVICE:Ljava/lang/String; = "samsung.intent.action.SHUTDOWN_MIRRORLINK"

.field private static final START_ML_SERVICE:Ljava/lang/String; = "com.sec.android.tmserver.service.TMS_SERVICE_ACTION"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;

.field private mbReceiverRegistered:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 28
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;

    .line 29
    return-void
.end method

.method private isReceiverRegistered()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mbReceiverRegistered:Z

    return v0
.end method

.method private registerMLReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 69
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "samsung.intent.action.MIRRORING_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 71
    const-string/jumbo v1, "samsung.intent.action.MIRRORLINK_SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 75
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->setReceiverRegisteredFlag(Z)V

    .line 76
    return-void
.end method

.method private setReceiverRegisteredFlag(Z)V
    .locals 0
    .param p1, "bFlag"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mbReceiverRegistered:Z

    .line 109
    return-void
.end method

.method private unregisterMLReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager$MirrorLinkReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->setReceiverRegisteredFlag(Z)V

    .line 82
    return-void
.end method


# virtual methods
.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 88
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mContext:Landroid/content/Context;

    .line 89
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->registerMLReceiver(Landroid/content/Context;)V

    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public isMirrorLinkRunning()Z
    .locals 6

    .prologue
    .line 47
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mContext:Landroid/content/Context;

    .line 48
    const-string/jumbo v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 47
    check-cast v0, Landroid/app/ActivityManager;

    .line 50
    .local v0, "manager":Landroid/app/ActivityManager;
    if-eqz v0, :cond_1

    .line 52
    const/16 v3, 0x64

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v1

    .line 53
    .local v1, "runningServiceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v1, :cond_1

    .line 54
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 64
    .end local v1    # "runningServiceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_1
    const/4 v3, 0x0

    :goto_0
    return v3

    .line 54
    .restart local v1    # "runningServiceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 55
    .local v2, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    if-eqz v2, :cond_0

    .line 56
    const-string/jumbo v4, "com.sec.android.tmserver.service.TmsService"

    iget-object v5, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    .line 57
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    .line 56
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 57
    if-eqz v4, :cond_0

    .line 58
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public startMirrorLink(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 33
    .local v0, "startIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.tmserver.service.TMS_SERVICE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 37
    return-void
.end method

.method public stopMirrorLink(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "samsung.intent.action.SHUTDOWN_MIRRORLINK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 43
    .local v0, "shutdownIntent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 44
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->isReceiverRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;->unregisterMLReceiver(Landroid/content/Context;)V

    .line 104
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 105
    return-void
.end method
