.class public final enum Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
.super Ljava/lang/Enum;
.source "OnNotificationChangedListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppearState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

.field public static final enum DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

.field public static final enum ROTATING:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

.field public static final enum WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

.field public static final enum WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    const-string/jumbo v1, "WILL_APPEAR"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    const-string/jumbo v1, "APPEARED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    const-string/jumbo v1, "WILL_DISAPPEAR"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    const-string/jumbo v1, "DISAPPEARED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    const-string/jumbo v1, "ROTATING"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ROTATING:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    .line 4
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ROTATING:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
