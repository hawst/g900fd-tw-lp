.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    .line 802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 868
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-void
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/16 v10, 0x28

    const/4 v11, 0x0

    .line 807
    const-string/jumbo v7, "PhoneActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "search : searched! "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 808
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 807
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 812
    .local v5, "startTime":J
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_0

    .line 813
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v7

    const/16 v8, 0x43

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 858
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 860
    .local v2, "endTime":J
    const-string/jumbo v7, "PhoneActivity"

    .line 861
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "searched onResponseRequestSearchedContactList: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 862
    sub-long v9, v2, v5

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 861
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 860
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    return-void

    .line 830
    .end local v2    # "endTime":J
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 832
    .local v4, "size":I
    if-le v4, v10, :cond_1

    .line 833
    const/16 v4, 0x28

    .line 835
    invoke-virtual {p1, v11, v10}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 837
    .local v1, "dl":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-static {v1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 844
    .end local v1    # "dl":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :goto_1
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v7

    invoke-static {v7, v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$30(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/util/List;)V

    .line 846
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v8

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    .line 847
    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    .line 846
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$21(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 848
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v8

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    .line 849
    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    .line 848
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$22(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 850
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v7

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v7

    .line 851
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 853
    const v9, 0x7f0a0244

    .line 852
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    .line 854
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v11

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v11

    .line 855
    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    aput-object v11, v9, v10

    .line 851
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 850
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 856
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11$1;->this$1:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$11;)Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    move-result-object v7

    const/16 v8, 0x42

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    goto/16 :goto_0

    .line 840
    .end local v0    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    invoke-static {p1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 839
    .restart local v0    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    goto/16 :goto_1
.end method
