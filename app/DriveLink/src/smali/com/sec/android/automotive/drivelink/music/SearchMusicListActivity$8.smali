.class Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;
.super Ljava/lang/Object;
.source "SearchMusicListActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->initSearchResultList(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 452
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 438
    if-nez p1, :cond_0

    .line 439
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$12(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 440
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$11(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    move-result-object v1

    .line 441
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->getInitialPosition()I

    move-result v1

    .line 440
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$10(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 447
    :goto_0
    return-void

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$10(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$11(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 444
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$10(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 431
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mMusicMainPageAdapter:Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$11(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 430
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 433
    return-void
.end method
