.class Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;
.super Ljava/lang/Object;
.source "SettingsRegisterCarOptionsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->showRenameDialog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

.field private final synthetic val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 270
    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->refreshDeviceNameTextView()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 273
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/widget/EditText;

    move-result-object v1

    .line 274
    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    .line 272
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 275
    return-void
.end method
