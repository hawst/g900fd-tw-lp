.class public Lcom/sec/android/automotive/drivelink/location/map/MapFactory;
.super Ljava/lang/Object;
.source "MapFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newLocationMap()Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
    .locals 4

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 26
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 28
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    const/4 v3, 0x0

    .line 27
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 29
    .local v0, "mapID":I
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-eq v1, v0, :cond_0

    .line 30
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-ne v1, v0, :cond_1

    .line 31
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;-><init>()V

    .line 33
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;-><init>()V

    goto :goto_0
.end method

.method public static newLocationMap(Landroid/support/v4/app/Fragment;)Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
    .locals 4
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 59
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 58
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 60
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    const/4 v3, 0x0

    .line 59
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 61
    .local v0, "mapID":I
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-eq v1, v0, :cond_0

    .line 62
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-ne v1, v0, :cond_1

    .line 63
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 65
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;-><init>(Landroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public static newLocationMap(Landroid/support/v4/app/Fragment;Z)Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
    .locals 4
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p1, "isEnableMyLocationButton"    # Z

    .prologue
    .line 78
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 77
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 79
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    const/4 v3, 0x0

    .line 78
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 80
    .local v0, "mapID":I
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-eq v1, v0, :cond_0

    .line 81
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-ne v1, v0, :cond_1

    .line 82
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 84
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;-><init>(Landroid/support/v4/app/Fragment;Z)V

    goto :goto_0
.end method

.method public static newLocationMap(Z)Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
    .locals 4
    .param p0, "isEnableMyLocationButton"    # Z

    .prologue
    .line 43
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 42
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 44
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    const/4 v3, 0x0

    .line 43
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 45
    .local v0, "mapID":I
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-eq v1, v0, :cond_0

    .line 46
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-ne v1, v0, :cond_1

    .line 47
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;-><init>()V

    .line 49
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/GoogleMapAdapter;-><init>(Z)V

    goto :goto_0
.end method

.method public static newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    .locals 4

    .prologue
    .line 92
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 91
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 93
    const-string/jumbo v2, "PREF_SETTINGS_MY_NAVIGATION"

    const/4 v3, 0x0

    .line 92
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 94
    .local v0, "mapID":I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->createInstance(I)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v1

    return-object v1
.end method
