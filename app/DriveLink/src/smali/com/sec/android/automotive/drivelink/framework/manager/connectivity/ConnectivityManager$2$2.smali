.class Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->onServiceDisconnected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

.field private final synthetic val$objPollA2DP:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->val$objPollA2DP:Ljava/lang/Object;

    .line 552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 557
    const/4 v1, 0x0

    .line 558
    .local v1, "internalTimer":I
    :goto_0
    const/16 v2, 0x1388

    if-le v1, v2, :cond_1

    .line 575
    :cond_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->val$objPollA2DP:Ljava/lang/Object;

    monitor-enter v3

    .line 576
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->val$objPollA2DP:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 575
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$6(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 579
    return-void

    .line 559
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v2, :cond_2

    .line 560
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 561
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v4

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$19(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    .line 560
    iput v3, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->result:I

    .line 563
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;->this$1:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->result:I

    if-eqz v2, :cond_0

    .line 568
    :cond_2
    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 569
    add-int/lit8 v1, v1, 0x64

    goto :goto_0

    .line 570
    :catch_0
    move-exception v0

    .line 571
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "connectBluetoothDevice"

    invoke-static {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 575
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method
