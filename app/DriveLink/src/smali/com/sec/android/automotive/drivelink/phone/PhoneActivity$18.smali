.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 2092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 2097
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2121
    :goto_0
    :sswitch_0
    return-void

    .line 2099
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$33(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2100
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$31(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 2099
    sub-int/2addr v1, v2

    .line 2100
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 2099
    add-int v0, v1, v2

    .line 2101
    .local v0, "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$33(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 2111
    .end local v0    # "currentItem":I
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$36(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2112
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$34(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 2111
    sub-int/2addr v1, v2

    .line 2112
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 2111
    add-int v0, v1, v2

    .line 2113
    .restart local v0    # "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$36(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 2116
    .end local v0    # "currentItem":I
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$41(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2117
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$37(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 2116
    sub-int/2addr v1, v2

    .line 2117
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 2116
    add-int v0, v1, v2

    .line 2118
    .restart local v0    # "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mNumberViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$41(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 2097
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x20 -> :sswitch_0
        0x40 -> :sswitch_2
        0x41 -> :sswitch_2
        0x42 -> :sswitch_2
        0x80 -> :sswitch_3
    .end sparse-switch
.end method
