.class Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;
.super Ljava/lang/Object;
.source "SettingsAddContactsActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getSettingsSipStateListener()Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$EditTextForSettingsSIP$SettingsSIPState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$EditTextForSettingsSIP$SettingsSIPState()[I
    .locals 3

    .prologue
    .line 368
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$EditTextForSettingsSIP$SettingsSIPState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->values()[Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$EditTextForSettingsSIP$SettingsSIPState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnStateChanged(Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;)V
    .locals 2
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$23(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;)V

    .line 372
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$EditTextForSettingsSIP$SettingsSIPState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 385
    :goto_0
    :pswitch_0
    return-void

    .line 376
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0

    .line 379
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
