.class Lcom/sec/android/automotive/drivelink/common/DLServiceManager$3;
.super Ljava/lang/Object;
.source "DLServiceManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLMessageSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$3;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestRecommendedContactList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 420
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$3;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$10(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$3;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$11(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 422
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;

    move-result-object v0

    .line 423
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SUGGESTION_UPDATED:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .line 422
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater;->onChangeStatus(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V

    .line 424
    return-void
.end method
