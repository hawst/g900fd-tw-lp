.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 280
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 281
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 283
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 284
    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 286
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 287
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    iput-boolean v4, v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 288
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 290
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 291
    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 294
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Z)V

    .line 299
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Z)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->finish()V

    goto :goto_0
.end method
