.class Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "ArrivedParticipantsExpandableAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private childArea:Landroid/view/View;

.field private imgUser:Landroid/widget/ImageView;

.field private ivListStatus:Landroid/widget/ImageView;

.field private parentArea:Landroid/view/View;

.field private tvArrived:Landroid/widget/TextView;

.field private tvLetterUser:Landroid/widget/TextView;

.field private tvName:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/view/View;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->childArea:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvArrived:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/view/View;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->parentArea:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->ivListStatus:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public getChildArea()Landroid/view/View;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->childArea:Landroid/view/View;

    return-object v0
.end method

.method public getImgUser()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getIvListStatus()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->ivListStatus:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getParentArea()Landroid/view/View;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->parentArea:Landroid/view/View;

    return-object v0
.end method

.method public getTvArrived()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvArrived:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTvLetterUser()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTvName()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    return-object v0
.end method

.method public setChildArea(Landroid/view/View;)V
    .locals 0
    .param p1, "childArea"    # Landroid/view/View;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->childArea:Landroid/view/View;

    .line 235
    return-void
.end method

.method public setImgUser(Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "imgUser"    # Landroid/widget/ImageView;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;

    .line 211
    return-void
.end method

.method public setIvListStatus(Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "ivListStatus"    # Landroid/widget/ImageView;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->ivListStatus:Landroid/widget/ImageView;

    .line 187
    return-void
.end method

.method public setParentArea(Landroid/view/View;)V
    .locals 0
    .param p1, "parentArea"    # Landroid/view/View;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->parentArea:Landroid/view/View;

    .line 227
    return-void
.end method

.method public setTvArrived(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "tvArrived"    # Landroid/widget/TextView;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvArrived:Landroid/widget/TextView;

    .line 219
    return-void
.end method

.method public setTvLetterUser(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "tvLetterUser"    # Landroid/widget/TextView;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;

    .line 195
    return-void
.end method

.method public setTvName(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "tvName"    # Landroid/widget/TextView;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ArrivedParticipantsExpandableAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 203
    return-void
.end method
