.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;
.source "DLFolderImpl.java"


# instance fields
.field private mBucketId:I

.field private mFolderPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 1
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "bucketId"    # I
    .param p3, "folderPath"    # Ljava/lang/String;
    .param p4, "songCount"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->mFolderPath:Ljava/lang/String;

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->mBucketId:I

    .line 11
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->setName(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->setSongCount(I)V

    .line 14
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->mBucketId:I

    .line 15
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->mFolderPath:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->mBucketId:I

    return v0
.end method

.method public getFolderPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->mFolderPath:Ljava/lang/String;

    return-object v0
.end method
