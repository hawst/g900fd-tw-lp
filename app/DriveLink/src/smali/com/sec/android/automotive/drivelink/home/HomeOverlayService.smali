.class public Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;
.super Landroid/app/Service;
.source "HomeOverlayService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;,
        Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OrientationBroadcastReceiver;,
        Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    }
.end annotation


# static fields
.field public static final ACTION_DISMISS_VIEW:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_DISMISS_VIEW"

.field public static final ACTION_EXTRA_DISMISS_VIEW_TYPE:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_EXTRA_DISMISS_VIEW_TYPE"

.field public static final ACTION_MOVE_OUTOF_MULTI:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_MOVE_OUTOF_MULTI"

.field public static final ACTION_PAUSE:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_PAUSE"

.field public static final ACTION_REMOVE_VIEW:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_REMOVE_VIEW"

.field public static final ACTION_RESUME:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_RESUME"

.field private static final TAG:Ljava/lang/String;

.field protected static context:Landroid/content/Context;

.field private static flowIdInterface:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

.field private static mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

.field private static mWindowMgr:Landroid/view/WindowManager;

.field private static nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

.field private static params:Landroid/view/WindowManager$LayoutParams;

.field private static serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;


# instance fields
.field private isCarAppFinishing:Z

.field private mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

.field mFlowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

.field private mLcdOffReceiver:Landroid/content/BroadcastReceiver;

.field private mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

.field protected mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

.field private mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

.field private mOrientationReceiver:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OrientationBroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    .line 55
    sput-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    .line 57
    sput-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->flowIdInterface:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    .line 58
    sput-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 59
    sput-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .line 60
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Stoped:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 64
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->isCarAppFinishing:Z

    .line 66
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$1;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    .line 397
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$2;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .line 412
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    .line 603
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$4;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mFlowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    .line 37
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1()Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)Z
    .locals 1

    .prologue
    .line 289
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->checkNotification(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)V
    .locals 0

    .prologue
    .line 373
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->dismissNotification(Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 0

    .prologue
    .line 343
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    return-void
.end method

.method static synthetic access$6()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;)V
    .locals 0

    .prologue
    .line 58
    sput-object p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 0

    .prologue
    .line 59
    sput-object p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->isCarAppFinishing:Z

    return-void
.end method

.method private addNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 512
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 517
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 518
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Noti. Voice Listener Add"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addOverlayNotiView()V
    .locals 3

    .prologue
    .line 201
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "addOverlayNotiView:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-nez v0, :cond_0

    .line 212
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_1

    .line 207
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->addNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 210
    :cond_1
    const-string/jumbo v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 209
    check-cast v0, Landroid/view/WindowManager;

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    .line 211
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->getOverlayLayoutParams(Landroid/view/WindowManager;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private checkNotification(Z)Z
    .locals 6
    .param p1, "bAnimation"    # Z

    .prologue
    const/4 v2, 0x0

    .line 290
    const/4 v0, 0x0

    .line 292
    .local v0, "bNotified":Z
    sget-object v3, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sget-object v4, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Resumed:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    if-eq v3, v4, :cond_0

    .line 293
    sget-object v3, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "SERVICE STATE:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 294
    const-string/jumbo v5, "! SKIP TO CHECK NOTI!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 293
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :goto_0
    return v2

    .line 298
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_1

    .line 299
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayNotiView(Z)V

    .line 302
    :cond_1
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    .line 303
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getTopNotificationView()Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v2

    .line 302
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 305
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "checkNotification NotiView:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_4

    .line 308
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiReqListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setOnNotificationRequestListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;)V

    .line 309
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 310
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 313
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 314
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->isAppeared()Z

    move-result v2

    if-nez v2, :cond_3

    .line 315
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v2

    const/16 v3, 0xf

    if-eq v2, v3, :cond_3

    .line 316
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 317
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "MUSIC is Playing : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 318
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 317
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    sput-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 321
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onPlaybackRequest PLAYBACK_REQUEST_PAUSE"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 323
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->addOverlayNotiView()V

    .line 331
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 332
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v2

    .line 333
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V

    .line 334
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 335
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->resumeNotiFlow()V

    .line 337
    const/4 v0, 0x1

    :cond_4
    move v2, v0

    .line 340
    goto/16 :goto_0

    .line 324
    :catch_0
    move-exception v1

    .line 325
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private dismissNotification(Z)V
    .locals 4
    .param p1, "hasCommand"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 374
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_1

    .line 375
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "dismissNotification"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 377
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 377
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->sendResetFlowId()V

    .line 380
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 381
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayNotiView(Z)V

    .line 382
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    if-eqz v0, :cond_0

    .line 383
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 384
    invoke-interface {v0, v2}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V

    .line 385
    sput-object v3, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    .line 388
    :cond_0
    if-eqz p1, :cond_2

    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    if-eqz v0, :cond_2

    .line 389
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/INotiCommand;->action(Landroid/content/Context;)V

    .line 390
    sput-object v3, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .line 395
    :cond_1
    :goto_0
    return-void

    .line 392
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->checkNotification(Z)Z

    goto :goto_0
.end method

.method private static getOverlayLayoutParams(Landroid/view/WindowManager;)Landroid/view/WindowManager$LayoutParams;
    .locals 4
    .param p0, "windowManager"    # Landroid/view/WindowManager;

    .prologue
    const/4 v3, -0x2

    .line 249
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 250
    .local v0, "outSize":Landroid/graphics/Point;
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 252
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    .line 254
    const/4 v2, -0x3

    .line 252
    invoke-direct {v1, v3, v3, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    sput-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->params:Landroid/view/WindowManager$LayoutParams;

    .line 266
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->params:Landroid/view/WindowManager$LayoutParams;

    const v2, 0x40320

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 270
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x7d3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 281
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x32

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 283
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->params:Landroid/view/WindowManager$LayoutParams;

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 284
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->params:Landroid/view/WindowManager$LayoutParams;

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 286
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->params:Landroid/view/WindowManager$LayoutParams;

    return-object v1
.end method

.method public static getState()Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    return-object v0
.end method

.method private handleCommand(Landroid/content/Intent;II)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 149
    if-nez p1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 158
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "handleCommand:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_PAUSE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 160
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Paused:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sput-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 161
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayNotiView(Z)V

    goto :goto_0

    .line 162
    :cond_2
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_RESUME"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 163
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Resumed:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sput-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 165
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    .line 166
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->registerOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 171
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setMultiMode(Z)V

    .line 172
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPendingReq()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 173
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->resetPendingReq()Z

    .line 176
    :cond_3
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->checkNotification(Z)Z

    goto :goto_0

    .line 177
    :cond_4
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DISMISS_VIEW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 179
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_EXTRA_DISMISS_VIEW_TYPE"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 181
    .local v1, "type":I
    if-nez v1, :cond_5

    .line 182
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->dismissNotification(Z)V

    goto :goto_0

    .line 184
    :cond_5
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 185
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 186
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->dismissNotification(Z)V

    goto/16 :goto_0

    .line 189
    .end local v1    # "type":I
    :cond_6
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_MOVE_OUTOF_MULTI"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 190
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    .line 192
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .line 191
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->unregisterOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 193
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setMultiMode(Z)V

    goto/16 :goto_0

    .line 194
    :cond_7
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_REMOVE_VIEW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayNotiView(Z)V

    goto/16 :goto_0
.end method

.method public static isRunning()Z
    .locals 2

    .prologue
    .line 144
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Stoped:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 5
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;
    .param p2, "notiView"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 347
    if-nez p2, :cond_1

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    move-object v0, p2

    .line 353
    .local v0, "changeListener":Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;
    :try_start_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_APPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_2

    .line 354
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillAppear()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 365
    :catch_0
    move-exception v1

    .line 366
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "This NotificationView("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 367
    const-string/jumbo v4, ") does not support Change Listener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 366
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0

    .line 355
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->APPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_3

    .line 356
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationAppeared()V

    goto :goto_0

    .line 357
    :cond_3
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_4

    .line 358
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationWillDisappear()V

    goto :goto_0

    .line 359
    :cond_4
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_5

    .line 360
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationDisappeared()V

    goto :goto_0

    .line 361
    :cond_5
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ROTATING:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    if-ne p1, v2, :cond_0

    .line 362
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener;->onNotificationRotating()V
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private registerCarModeChangeToOffReceiver()V
    .locals 3

    .prologue
    .line 543
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$5;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    .line 554
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    .line 555
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.carmodeoff.finish"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 554
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 556
    return-void
.end method

.method private removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 523
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 529
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Noti. Voice Listener Remove"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private removeOverlayNotiView(Z)V
    .locals 3
    .param p1, "bRemoveVoiceUpdater"    # Z

    .prologue
    .line 215
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "removeOverlayNotiView:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_1

    .line 217
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 218
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->ROTATING:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 219
    if-eqz p1, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_0

    .line 223
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v0

    .line 222
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 226
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .line 228
    :cond_1
    return-void
.end method

.method private removeOverlayView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 4
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 231
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "removeOverlayView:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 234
    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mWindowMgr:Landroid/view/WindowManager;

    invoke-interface {v1, p1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->hasNotiItem()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiType()I

    move-result v1

    const/16 v2, 0xe

    if-eq v1, v2, :cond_0

    .line 241
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonVoiceUiUpdater;

    move-result-object v1

    .line 240
    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeNotiVoiceUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 244
    :cond_0
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static setFlowIdInterface(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;)V
    .locals 3
    .param p0, "flowIf"    # Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    .prologue
    .line 595
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setFlowIdInterface:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    sput-object p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->flowIdInterface:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    .line 597
    return-void
.end method

.method private unRegisterCarModeChangeToOffReceiver()V
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mCarModeOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 562
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 91
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Home Overlay Service Created"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 93
    new-instance v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OrientationBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OrientationBroadcastReceiver;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mOrientationReceiver:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OrientationBroadcastReceiver;

    .line 94
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->registerCarModeChangeToOffReceiver()V

    .line 95
    new-instance v0, Landroid/content/IntentFilter;

    .line 96
    const-string/jumbo v1, "android.intent.action.CONFIGURATION_CHANGED"

    .line 95
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 97
    .local v0, "orientationIF":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mOrientationReceiver:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OrientationBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 98
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    .line 99
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->registerOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 100
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setMultiMode(Z)V

    .line 101
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Started:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sput-object v1, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 102
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    .line 103
    const-string/jumbo v3, "android.intent.action.SCREEN_OFF"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 104
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;->Stoped:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->serviceState:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OverlayServiceState;

    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mOrientationReceiver:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$OrientationBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 111
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayNotiView(Z)V

    .line 112
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->isCarAppFinishing:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotificationListener:Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;

    .line 114
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->unregisterOnNotificationChangeListener(Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;)V

    .line 116
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->setMultiMode(Z)V

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->unRegisterCarModeChangeToOffReceiver()V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeAllNotiVoiceUpdater()V

    .line 120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 122
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Home Overlay Service Destroyed"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 124
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 128
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Home Overlay Service onStartCommand"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    if-nez p1, :cond_0

    .line 130
    const/4 v0, 0x0

    .line 136
    :goto_0
    return v0

    .line 133
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->handleCommand(Landroid/content/Intent;II)V

    .line 136
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public removeAllNotiVoiceUpdater()V
    .locals 2

    .prologue
    .line 534
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    if-nez v0, :cond_0

    .line 540
    :goto_0
    return-void

    .line 538
    :cond_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeNotiAllVoiceUiUpdater()V

    .line 539
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Remove All Voice Listener"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method sendResetFlowId()V
    .locals 2

    .prologue
    .line 501
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sendResetFlowId"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->flowIdInterface:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    if-eqz v0, :cond_0

    .line 503
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->flowIdInterface:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;->resetFlowId()V

    .line 504
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setFlowCommandListener(Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;)V

    .line 509
    :goto_0
    return-void

    .line 506
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "flowIdInterface is null!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method sendSetFlowId(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 493
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "sendSetFlowId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->flowIdInterface:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    if-eqz v0, :cond_0

    .line 495
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->flowIdInterface:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$FlowIDInterface;->setFlowId(Ljava/lang/String;)V

    .line 496
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mFlowCmdListener:Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;->setFlowCommandListener(Lcom/sec/android/automotive/drivelink/common/base/FlowCommandListener;)V

    .line 498
    :cond_0
    return-void
.end method
