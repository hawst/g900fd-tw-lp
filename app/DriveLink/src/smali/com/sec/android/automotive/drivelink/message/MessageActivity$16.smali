.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getInboxItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1797
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->hideInputMethod()V
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$61(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 1798
    const/4 v0, 0x0

    .line 1800
    .local v0, "currentItem":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    .line 1801
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v3

    .line 1802
    mul-int/lit8 v3, v3, 0x4

    .line 1801
    add-int/2addr v2, v3

    add-int/lit8 v0, v2, -0x1

    .line 1808
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$54(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .line 1810
    .local v1, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->getMessageHolder()Lcom/sec/android/automotive/drivelink/message/MessageHolder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/message/MessageHolder;->setInbox(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)V

    .line 1812
    const-string/jumbo v2, "DM_SMS_READBACK"

    .line 1813
    const/4 v3, 0x0

    .line 1812
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1815
    sget-boolean v2, Lcom/sec/android/automotive/drivelink/DLApplication;->DL_DEMO:Z

    if-eqz v2, :cond_1

    .line 1816
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->finish()V

    .line 1818
    :cond_1
    return-void

    .line 1803
    .end local v1    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    .line 1804
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$16;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$59(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v3

    .line 1805
    mul-int/lit8 v3, v3, 0x4

    .line 1804
    add-int/2addr v2, v3

    add-int/lit8 v0, v2, -0x1

    goto :goto_0
.end method
