.class public Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;
.super Landroid/widget/RelativeLayout;
.source "VoiceIndicatorMultiActionBarLayout.java"

# interfaces
.implements Lcom/nuance/drivelink/DLUiUpdater;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static final TAG:Ljava/lang/String; = "[VoiceIndicatorMultiActionBarLayout]"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mIsErrorState:Z

.field private mIsMicDisplayed:Z

.field private mIsStartQEnded:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mMicBtn:Landroid/widget/ImageView;

.field private mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

.field private mMicLayout:Landroid/widget/RelativeLayout;

.field private mMicProcessBg:Landroid/widget/ImageView;

.field private mMicStartQBg:Landroid/widget/ImageView;

.field private mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

.field private mStartQAni:Landroid/view/animation/Animation;

.field private mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 53
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    .line 54
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->init(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    .line 61
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->init(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    .line 68
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->init(Landroid/content/Context;)V

    .line 69
    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;Z)V
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsStartQEnded:Z

    return-void
.end method

.method private setAnimation()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    .line 214
    const v1, 0x7f04001e

    .line 213
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    .line 215
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 234
    return-void
.end method

.method private setIDLEMode()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 280
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "setIDLEMode"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsErrorState:Z

    .line 282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 285
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f08000a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsStartQEnded:Z

    .line 291
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 296
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020330

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020334

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 303
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 305
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020382

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 143
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "dispatchTouchEvent:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public displayError(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 106
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "displayError"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsErrorState:Z

    .line 108
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 120
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 130
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "displaySystemTurn"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->setIDLEMode()V

    .line 132
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 125
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "displayUserTurn"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 137
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "displayWidgetContent"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 271
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    const-string/jumbo v1, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v2, "init"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 78
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0300d5

    .line 77
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 79
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 80
    const v2, 0x7f09032b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 79
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    .line 81
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f090092

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    .line 82
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09032f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    .line 83
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f09032c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    .line 84
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 85
    const v2, 0x7f09032d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 84
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    .line 86
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 87
    const v2, 0x7f09032e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 86
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    .line 89
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 90
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v2, v3, v4}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V

    .line 89
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void
.end method

.method public onClickable(Z)V
    .locals 3
    .param p1, "isClickable"    # Z

    .prologue
    .line 248
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "!!!onClickable: set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", But ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 250
    return-void
.end method

.method public onDisplayMic(Z)V
    .locals 5
    .param p1, "isMicDisplayed"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 316
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsMicDisplayed:Z

    .line 317
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Mic onDisplayMic: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    .line 323
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsErrorState:Z

    .line 324
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsStartQEnded:Z

    .line 325
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 326
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 328
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020330

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020334

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020382

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public onPhraseSpotterStateChanged(Z)V
    .locals 3
    .param p1, "isSpotting"    # Z

    .prologue
    .line 309
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - PhraseSpotting : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 310
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 309
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    return-void
.end method

.method public setCurrentMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 101
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setCurrentMode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void
.end method

.method public setMicClickable(Z)V
    .locals 3
    .param p1, "clickable"    # Z

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 95
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Mic setClickable:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 98
    :cond_0
    return-void
.end method

.method protected setMicDisplayed(Z)V
    .locals 0
    .param p1, "isMicDisplayed"    # Z

    .prologue
    .line 347
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsMicDisplayed:Z

    .line 348
    return-void
.end method

.method public setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    .line 244
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsStartQEnded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->update(I)V

    .line 261
    :cond_0
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 149
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    if-nez v0, :cond_4

    .line 153
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 210
    :goto_0
    return-void

    .line 155
    :pswitch_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v0

    .line 157
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_OFF:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 156
    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;->OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    .line 159
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->setIDLEMode()V

    goto :goto_0

    .line 163
    :pswitch_1
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsErrorState:Z

    .line 164
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 165
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v0

    .line 166
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_ON:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 165
    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;->OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    .line 168
    :cond_1
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "LISTENING"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->stop()V

    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020333

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02032f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 186
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->setAnimation()V

    .line 187
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mStartQAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f020381

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    const v1, 0x7f02037d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 190
    :pswitch_2
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "THINKING"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsErrorState:Z

    .line 192
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 193
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getListener()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;

    move-result-object v0

    .line 194
    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_OFF:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 193
    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;->OnState(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;)V

    .line 196
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mIsStartQEnded:Z

    .line 198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 199
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicProcessBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mTimer:Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/hourglass/HourGlassView;->play(I)V

    goto/16 :goto_0

    .line 208
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mOnMicStateChangeListener:Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    goto/16 :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
