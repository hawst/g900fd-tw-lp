.class public Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "LocationPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20

.field private static isNightMode:Z


# instance fields
.field private final df:Ljava/text/DateFormat;

.field private final idsToSave:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmapList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private final mContext:Landroid/content/Context;

.field private mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mInitialPosition:I

.field private mLocationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPageCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->isNightMode:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const/4 v4, 0x0

    .line 52
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 44
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 45
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    .line 46
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->df:Ljava/text/DateFormat;

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->idsToSave:Ljava/util/ArrayList;

    .line 55
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    .line 57
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->updatePageCount()V

    .line 59
    const-string/jumbo v1, "TESTE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, ">> size "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 60
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    .line 63
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->df:Ljava/text/DateFormat;

    const-string/jumbo v2, "GMT+0"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 67
    return-void

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getLocationBitmap(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)Landroid/graphics/Bitmap;
    .locals 23
    .param p1, "locationItem"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .prologue
    .line 357
    const/4 v6, 0x0

    .line 359
    .local v6, "maskedBitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 360
    .local v7, "mode":I
    sget-boolean v20, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->isNightMode:Z

    if-eqz v20, :cond_0

    .line 361
    const/4 v7, 0x1

    .line 363
    :cond_0
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v3, v0, [I

    fill-array-data v3, :array_0

    .line 365
    .local v3, "homeBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v10, v0, [I

    fill-array-data v10, :array_1

    .line 367
    .local v10, "officeBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v4, v0, [I

    fill-array-data v4, :array_2

    .line 369
    .local v4, "homeNewBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v11, v0, [I

    fill-array-data v11, :array_3

    .line 371
    .local v11, "officeNewBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v13, v0, [I

    fill-array-data v13, :array_4

    .line 373
    .local v13, "placeBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v15, v0, [I

    fill-array-data v15, :array_5

    .line 375
    .local v15, "placeBMPdumap":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v16, v0

    fill-array-data v16, :array_6

    .line 378
    .local v16, "placeBMPollehnav":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v17, v0

    fill-array-data v17, :array_7

    .line 380
    .local v17, "placeBMPtmap":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v14, v0, [I

    fill-array-data v14, :array_8

    .line 383
    .local v14, "placeBMPUPlusmap":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v12, v0, [I

    fill-array-data v12, :array_9

    .line 385
    .local v12, "parkingBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v2, v0, [I

    fill-array-data v2, :array_a

    .line 387
    .local v2, "gasBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v8, v0, [I

    fill-array-data v8, :array_b

    .line 389
    .local v8, "myplaceBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v9, v0, [I

    fill-array-data v9, :array_c

    .line 391
    .local v9, "myplaceNewBMP":[I
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v19, v0

    fill-array-data v19, :array_d

    .line 397
    .local v19, "scheduleBMP":[I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    .line 396
    invoke-static/range {v20 .. v20}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v20

    .line 398
    const-string/jumbo v21, "PREF_SETTINGS_MY_NAVIGATION"

    const/16 v22, 0x0

    .line 397
    invoke-virtual/range {v20 .. v22}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v18

    .line 400
    .local v18, "resMapSetup":I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    .line 402
    .local v5, "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v20

    sget-object v21, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v19, v7

    .line 403
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 480
    :cond_1
    :goto_0
    if-nez v6, :cond_2

    .line 482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f02023d

    .line 481
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 485
    :cond_2
    return-object v6

    .line 405
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v20

    sget-object v21, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_14

    .line 407
    const-string/jumbo v20, "location_home"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 408
    if-eqz v5, :cond_4

    .line 409
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidAddres()Z

    move-result v20

    if-nez v20, :cond_5

    .line 410
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v20

    if-nez v20, :cond_5

    .line 412
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v4, v7

    .line 411
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 413
    goto :goto_0

    .line 415
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v3, v7

    .line 414
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 418
    goto :goto_0

    :cond_6
    const-string/jumbo v20, "location_office"

    .line 419
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 421
    if-eqz v5, :cond_7

    .line 422
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidAddres()Z

    move-result v20

    if-nez v20, :cond_8

    .line 423
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v20

    if-nez v20, :cond_8

    .line 425
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v11, v7

    .line 424
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 426
    goto/16 :goto_0

    .line 428
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v10, v7

    .line 427
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 431
    goto/16 :goto_0

    :cond_9
    const-string/jumbo v20, "poi_parking"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v12, v7

    .line 432
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 435
    goto/16 :goto_0

    :cond_a
    const-string/jumbo v20, "poi_gas"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v2, v7

    .line 436
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 439
    goto/16 :goto_0

    :cond_b
    const-string/jumbo v20, "google_map"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 440
    sget v20, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v15, v7

    .line 441
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 443
    goto/16 :goto_0

    :cond_c
    sget v20, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_TMAP:I

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_d

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v17, v7

    .line 444
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 446
    goto/16 :goto_0

    :cond_d
    sget v20, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_UNAVI:I

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v14, v7

    .line 447
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 449
    goto/16 :goto_0

    :cond_e
    sget v20, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_OLLEH_NAVI:I

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 451
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v16, v7

    .line 450
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 452
    goto/16 :goto_0

    .line 454
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v13, v7

    .line 453
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 457
    goto/16 :goto_0

    :cond_10
    const-string/jumbo v20, "location_myplace"

    .line 458
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v21

    .line 457
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    .line 458
    if-eqz v20, :cond_13

    .line 459
    if-eqz v5, :cond_11

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v20

    if-nez v20, :cond_12

    .line 461
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v9, v7

    .line 460
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 462
    goto/16 :goto_0

    .line 464
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v8, v7

    .line 463
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 466
    goto/16 :goto_0

    .line 468
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    aget v21, v8, v7

    .line 467
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 470
    goto/16 :goto_0

    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_15

    .line 471
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_15

    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mDriveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-object/from16 v20, v0

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    .line 474
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v22

    .line 473
    invoke-interface/range {v20 .. v22}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 475
    goto/16 :goto_0

    :cond_15
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v20

    sget-object v21, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 478
    const v21, 0x7f020242

    .line 476
    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    goto/16 :goto_0

    .line 363
    nop

    :array_0
    .array-data 4
        0x7f020227
        0x7f02022a
    .end array-data

    .line 365
    :array_1
    .array-data 4
        0x7f02022d
        0x7f020230
    .end array-data

    .line 367
    :array_2
    .array-data 4
        0x7f020228
        0x7f020229
    .end array-data

    .line 369
    :array_3
    .array-data 4
        0x7f02022e
        0x7f02022f
    .end array-data

    .line 371
    :array_4
    .array-data 4
        0x7f020217
        0x7f02021a
    .end array-data

    .line 373
    :array_5
    .array-data 4
        0x7f020218
        0x7f020219
    .end array-data

    .line 375
    :array_6
    .array-data 4
        0x7f02021b
        0x7f02021c
    .end array-data

    .line 378
    :array_7
    .array-data 4
        0x7f02021d
        0x7f02021e
    .end array-data

    .line 380
    :array_8
    .array-data 4
        0x7f02021f
        0x7f020220
    .end array-data

    .line 383
    :array_9
    .array-data 4
        0x7f020215
        0x7f020216
    .end array-data

    .line 385
    :array_a
    .array-data 4
        0x7f02020d
        0x7f02020e
    .end array-data

    .line 387
    :array_b
    .array-data 4
        0x7f020211
        0x7f020214
    .end array-data

    .line 389
    :array_c
    .array-data 4
        0x7f020212
        0x7f020213
    .end array-data

    .line 391
    :array_d
    .array-data 4
        0x7f02020f
        0x7f020210
    .end array-data
.end method

.method private isGroup(ILcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;)Z
    .locals 1
    .param p1, "index"    # I
    .param p2, "holder"    # Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    if-eqz v0, :cond_0

    .line 353
    if-eqz p2, :cond_0

    .line 351
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAdress(ILcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "holder"    # Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;

    .prologue
    .line 307
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 309
    .local v0, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 310
    const-string/jumbo v1, ""

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 311
    iget-object v1, p2, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    .line 312
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v2

    .line 311
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v1, p2, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    .line 315
    const v2, 0x7f0a02d3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private setColor(Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;Landroid/widget/RelativeLayout;)V
    .locals 4
    .param p1, "holder"    # Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;
    .param p2, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    const v3, 0x7f080047

    const v2, 0x7f080046

    .line 252
    if-nez p1, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->isNightMode:Z

    if-eqz v0, :cond_5

    .line 257
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 262
    :cond_2
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 263
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 267
    :cond_3
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 268
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 269
    const v1, 0x7f02020a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 272
    :cond_4
    if-eqz p2, :cond_0

    .line 273
    const v0, 0x7f08002e

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 276
    :cond_5
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 277
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 281
    :cond_6
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 282
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 286
    :cond_7
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 287
    iget-object v0, p1, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 288
    const v1, 0x7f020209

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 291
    :cond_8
    if-eqz p2, :cond_0

    .line 292
    const v0, 0x7f08002a

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public static setDayMode()V
    .locals 1

    .prologue
    .line 543
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->isNightMode:Z

    .line 544
    return-void
.end method

.method private setGroupName(ILcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "holder"    # Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;

    .prologue
    .line 320
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 321
    .local v0, "group":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 322
    const-string/jumbo v1, ""

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    iget-object v1, p2, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getDestination()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    :goto_0
    return-void

    .line 325
    :cond_0
    iget-object v1, p2, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const v2, 0x7f0a0320

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public static setNightMode()V
    .locals 1

    .prologue
    .line 539
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->isNightMode:Z

    .line 540
    return-void
.end method

.method private updatePageCount()V
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getPageCount()I

    move-result v0

    .line 71
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 73
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 72
    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mInitialPosition:I

    .line 74
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mPageCount:I

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mInitialPosition:I

    .line 77
    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mPageCount:I

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 490
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 491
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 495
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mPageCount:I

    return v0
.end method

.method public getIdsToSave()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->idsToSave:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIndexBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 512
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 513
    :cond_0
    const/4 v0, 0x0

    .line 514
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 523
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 83
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 499
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 500
    const/4 v0, 0x0

    .line 508
    :cond_0
    :goto_0
    return v0

    .line 502
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 503
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 504
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 505
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 13
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 89
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 91
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    const v9, 0x7f030095

    const/4 v10, 0x0

    .line 90
    invoke-virtual {v4, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 93
    .local v8, "v":Landroid/view/View;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v5, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v9, 0x7f090207

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    const v9, 0x7f090208

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    const v9, 0x7f090209

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    const v9, 0x7f09020a

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getPageCount()I

    move-result v9

    rem-int v9, p2, v9

    mul-int/lit8 v2, v9, 0x4

    .line 103
    .local v2, "index":I
    const/4 v1, 0x1

    .line 104
    .local v1, "i":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 246
    invoke-virtual {v8, p2}, Landroid/view/View;->setId(I)V

    .line 247
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v8}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 248
    return-object v8

    .line 104
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 105
    .local v3, "layout":Landroid/widget/RelativeLayout;
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v0, p0, v9}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;)V

    .line 107
    .local v0, "holder":Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v2, :cond_f

    .line 110
    const v9, 0x7f0901f7

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 109
    iput-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 112
    const v9, 0x7f0901f8

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 111
    iput-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 114
    const v9, 0x7f090296

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 113
    iput-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivNewItem:Landroid/widget/ImageView;

    .line 116
    const v9, 0x7f090297

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 115
    iput-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    .line 118
    const v9, 0x7f090295

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 117
    iput-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactMaskImage:Landroid/widget/ImageView;

    .line 119
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    invoke-direct {p0, v0, v3}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->setColor(Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;Landroid/widget/RelativeLayout;)V

    .line 123
    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 124
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 127
    .local v6, "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    if-eqz v6, :cond_0

    .line 130
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getLocationBitmap(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 131
    .local v7, "overlayedBitmap":Landroid/graphics/Bitmap;
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    invoke-virtual {v9, v2, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    invoke-virtual {v9, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 134
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivContactImage:Landroid/widget/ImageView;

    .line 135
    const v11, 0x7f02023c

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 139
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_c

    .line 140
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v11, ""

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 141
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    const-string/jumbo v9, "location_home"

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 144
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 145
    const v12, 0x7f0a02f0

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 144
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :cond_2
    :goto_1
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    move-result-object v9

    sget-object v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne v9, v11, :cond_d

    .line 189
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-result-object v9

    sget-object v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    if-ne v9, v11, :cond_d

    .line 190
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 191
    const v12, 0x7f0a0300

    .line 190
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :cond_3
    :goto_2
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivNewItem:Landroid/widget/ImageView;

    const/4 v11, 0x4

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 219
    invoke-direct {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->isGroup(ILcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 220
    invoke-direct {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->setAdress(ILcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;)V

    .line 242
    .end local v6    # "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    .end local v7    # "overlayedBitmap":Landroid/graphics/Bitmap;
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 243
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 151
    .restart local v6    # "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    .restart local v7    # "overlayedBitmap":Landroid/graphics/Bitmap;
    :cond_4
    const-string/jumbo v9, "location_office"

    .line 152
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    .line 151
    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 152
    if-eqz v9, :cond_5

    .line 154
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 155
    const v12, 0x7f0a02f6

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 154
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 157
    :cond_5
    const-string/jumbo v9, "poi_gas"

    .line 158
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 159
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 160
    const v12, 0x7f0a0306

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 159
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 161
    :cond_6
    const-string/jumbo v9, "Group"

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 163
    invoke-direct {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->setGroupName(ILcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;)V

    goto/16 :goto_1

    .line 164
    :cond_7
    const-string/jumbo v9, "poi_parking"

    .line 165
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    .line 164
    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 165
    if-eqz v9, :cond_8

    .line 166
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 167
    const v12, 0x7f0a0303

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 166
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 168
    :cond_8
    const-string/jumbo v9, "google_map"

    .line 169
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    .line 168
    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 169
    if-eqz v9, :cond_9

    .line 170
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 171
    const v12, 0x7f0a0664

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 170
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 172
    :cond_9
    const-string/jumbo v9, "location_myplace"

    .line 173
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v11

    .line 172
    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 173
    if-eqz v9, :cond_2

    .line 174
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 175
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v9

    .line 176
    const-string/jumbo v11, ""

    invoke-virtual {v9, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 177
    :cond_a
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 178
    const v12, 0x7f0a02f2

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 177
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 180
    :cond_b
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v11

    .line 181
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v11

    .line 180
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 185
    :cond_c
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 192
    :cond_d
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationDescription()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 193
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 194
    const-string/jumbo v11, ""

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 195
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->tvAddress:Landroid/widget/TextView;

    .line 196
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationDescription()Ljava/lang/String;

    move-result-object v11

    .line 195
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 222
    :cond_e
    iget-object v9, v0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter$ViewHolder;->ivNewItem:Landroid/widget/ImageView;

    const/4 v11, 0x4

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 239
    .end local v6    # "locationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    .end local v7    # "overlayedBitmap":Landroid/graphics/Bitmap;
    :cond_f
    const/4 v9, 0x4

    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 519
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setLocationList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    if-nez p1, :cond_0

    .line 339
    :goto_0
    return-void

    .line 333
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mLocationList:Ljava/util/ArrayList;

    .line 334
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    .line 335
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 338
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->updatePageCount()V

    goto :goto_0

    .line 336
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->mBitmapList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
