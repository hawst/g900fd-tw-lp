.class public Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "MessageContactTypePageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;,
        Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x4

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20

.field public static final TYPE_CAR:I = 0x9

.field public static final TYPE_COMPANY_MAIN:I = 0xa

.field public static final TYPE_FAX_HOME:I = 0x5

.field public static final TYPE_FAX_WORK:I = 0x4

.field public static final TYPE_HOME:I = 0x1

.field public static final TYPE_MAIN:I = 0xc

.field public static final TYPE_MOBILE:I = 0x2

.field public static final TYPE_OTHER:I = 0x7

.field public static final TYPE_OTHER_FAX:I = 0xd

.field public static final TYPE_WORK:I = 0x3

.field public static final TYPE_WORK_MOBILE:I = 0x11


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContactTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mInitialPosition:I

.field private mPageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContext:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContactTypeList:Ljava/util/ArrayList;

    .line 49
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v0

    .line 51
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 53
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 52
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mInitialPosition:I

    .line 54
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mPageCount:I

    .line 59
    :goto_0
    return-void

    .line 56
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mInitialPosition:I

    .line 57
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mPageCount:I

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 173
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 174
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 168
    const/4 v0, -0x2

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 182
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContactTypeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x4

    .line 183
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContactTypeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x4

    .line 184
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 185
    add-int/lit8 v0, v0, 0x1

    .line 188
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 12
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 77
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 78
    .local v5, "layoutInflater":Landroid/view/LayoutInflater;
    const v9, 0x7f03006c

    .line 79
    const/4 v10, 0x0

    .line 78
    invoke-virtual {v5, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 81
    .local v8, "v":Landroid/view/View;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v6, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v9, 0x7f090220

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 82
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    const v9, 0x7f090221

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 84
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    const v9, 0x7f090222

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 86
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    const v9, 0x7f090223

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 88
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v9

    rem-int v9, p2, v9

    mul-int/lit8 v3, v9, 0x4

    .line 95
    .local v3, "index":I
    const/4 v7, 0x0

    .line 97
    .local v7, "phoneNumberDisplayType":Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;
    const/4 v2, 0x1

    .line 98
    .local v2, "i":I
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_0

    .line 152
    invoke-virtual {v8, p2}, Landroid/view/View;->setId(I)V

    .line 153
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v8}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 155
    return-object v8

    .line 98
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 99
    .local v4, "layout":Landroid/widget/RelativeLayout;
    new-instance v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v1, v9}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;)V

    .line 102
    .local v1, "holder":Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;
    const v9, 0x7f09021c

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 101
    iput-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    .line 104
    const v9, 0x7f09021d

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 103
    iput-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->ivPhoneNumberMaskImage:Landroid/widget/ImageView;

    .line 106
    const v9, 0x7f09021e

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 105
    iput-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    .line 108
    const v9, 0x7f09021f

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 107
    iput-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    .line 110
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContactTypeList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v3, :cond_2

    .line 111
    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 112
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContactTypeList:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 117
    .local v0, "contactData":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v9

    invoke-static {v9}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->getPhoneNumberDisplayType(I)Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;

    move-result-object v7

    .line 119
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v9, v11, :cond_1

    .line 120
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    .line 122
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->getPhoneDrawableLandscape()I

    move-result v11

    .line 121
    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 129
    :goto_1
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    .line 130
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->getStringResource()I

    move-result v11

    .line 129
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(I)V

    .line 131
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->ivPhoneNumberMaskImage:Landroid/widget/ImageView;

    .line 134
    const v11, 0x7f02020b

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    .end local v0    # "contactData":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 149
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 124
    .restart local v0    # "contactData":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    :cond_1
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    .line 126
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$PhoneNumberDisplayTypeForMsg;->getPhoneDrawablePortrait()I

    move-result v11

    .line 125
    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 137
    .end local v0    # "contactData":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    :cond_2
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->ivPhoneNumberImage:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->ivPhoneNumberMaskImage:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->tvPhoneNumber:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter$ViewHolder;->tvPhoneNumberName:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 193
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setContactTypeList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mContactTypeList:Ljava/util/ArrayList;

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->getPageCount()I

    move-result v0

    .line 65
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 67
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 66
    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mInitialPosition:I

    .line 68
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mPageCount:I

    .line 73
    :goto_0
    return-void

    .line 70
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mInitialPosition:I

    .line 71
    iput v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageContactTypePageAdapter;->mPageCount:I

    goto :goto_0
.end method
