.class Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;
.super Landroid/widget/ArrayAdapter;
.source "HomeListViewDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->createListViewDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "$anonymous0"    # Landroid/content/Context;
    .param p3, "$anonymous1"    # I

    .prologue
    .line 1
    .local p4, "$anonymous2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    .line 49
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v3, 0x7f09022b

    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->access$0(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 55
    const v1, 0x7f03006e

    const/4 v2, 0x0

    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 57
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 56
    check-cast v0, Landroid/widget/TextView;

    .line 58
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mStrListItem:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->access$1(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mIsDriving:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->access$2(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 62
    :cond_0
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 61
    check-cast v0, Landroid/widget/TextView;

    .line 63
    const-string/jumbo v1, "#44f5f5f5"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 66
    :cond_1
    return-object p2
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    .line 71
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$1;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->mIsDriving:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->access$2(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 73
    :cond_0
    const/4 v0, 0x0

    .line 77
    :cond_1
    return v0
.end method
