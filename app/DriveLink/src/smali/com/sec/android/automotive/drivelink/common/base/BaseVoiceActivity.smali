.class public Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "BaseVoiceActivity.java"


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field protected mIsSkipVoiceResume:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string/jumbo v0, "BaseVoiceActivity"

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsSkipVoiceResume:Z

    .line 19
    return-void
.end method


# virtual methods
.method public getDLGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isFlowManagerIntent(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 175
    if-eqz p1, :cond_0

    .line 176
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 177
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 178
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

    .line 177
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 178
    if-eqz v1, :cond_0

    .line 179
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 180
    const-string/jumbo v2, "EXTRA_FROM_FLOW_MGR"

    .line 179
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 180
    if-eqz v1, :cond_0

    .line 181
    const/4 v0, 0x1

    .line 184
    :cond_0
    return v0
.end method

.method protected isNotificationTopActivity()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 194
    const-string/jumbo v2, "activity"

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 195
    .local v0, "am":Landroid/app/ActivityManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 196
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 197
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v2, :cond_1

    :cond_0
    move v2, v4

    .line 209
    :goto_0
    return v2

    .line 201
    :cond_1
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 202
    .local v1, "topActivity":Landroid/content/ComponentName;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "TopActivity : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 203
    const-string/jumbo v6, ", ClassName: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 202
    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 206
    const-string/jumbo v5, "com.sec.android.automotive.drivelink.notification.NotificationActivity"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 207
    goto :goto_0

    :cond_2
    move v2, v4

    .line 209
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 97
    return-void
.end method

.method protected onFlowListSelectedFailed()V
    .locals 2

    .prologue
    .line 189
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onFlowListSelectedFialed:"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->startDmFlow()V

    .line 191
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 69
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onNewIntent:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mFlowID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setIntent(Landroid/content/Intent;)V

    .line 74
    const-string/jumbo v0, "EXTRA_FLOW_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 77
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 81
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 83
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPause:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsRotation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->isNotificationTopActivity()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsRotation:Z

    if-nez v0, :cond_0

    .line 86
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->pause(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)V

    .line 90
    :goto_0
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsRotation:Z

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResume:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsRotation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsSkipVoiceResume:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->getKeyguardManager()Landroid/app/KeyguardManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResume Canceled by keyguard : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 39
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 54
    return-void

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->requstAudioFocus()V

    .line 44
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsSkipVoiceResume:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsRotation:Z

    if-eqz v0, :cond_2

    .line 45
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume: Skip voiceResume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setFlowID()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mIsRotation:Z

    goto :goto_0

    .line 49
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onStartResume()V

    goto :goto_0
.end method

.method protected onStartResume()V
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onStartResume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->resume(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)Z

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setFlowID()V

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->startDmFlow()V

    .line 62
    return-void
.end method

.method protected setFlowID()V
    .locals 5

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 102
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 109
    const-string/jumbo v3, "EXTRA_FLOW_ID"

    .line 108
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "flowID":Ljava/lang/String;
    const-string/jumbo v2, "VOICE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "FlowId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const-string/jumbo v2, "DM_LOCATION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 113
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 114
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 113
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 115
    const-string/jumbo v2, "CM03"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 118
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->setActivityFlowID(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected startDmFlow()V
    .locals 4

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 125
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[startDmFlow]"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->isFlowManagerIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mFlowID:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 144
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mFlowID:Ljava/lang/String;

    .line 145
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->mFlowID:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v2

    .line 144
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 148
    :cond_2
    const-string/jumbo v1, "DLPhraseSpotter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[start] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " - startDmFlow()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 151
    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto :goto_0
.end method
