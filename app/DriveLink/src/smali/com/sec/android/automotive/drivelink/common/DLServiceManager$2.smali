.class Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;
.super Ljava/lang/Object;
.source "DLServiceManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLLogListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkCallLogListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestCallLogList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 305
    .local p1, "calllogList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$7(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$8(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$9(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$2;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$9(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;->onUpdateLogList(Ljava/util/ArrayList;)V

    .line 311
    :cond_0
    return-void
.end method
