.class public Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;
.source "IndicatorFragmentViewForMulti.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I


# instance fields
.field private TAG:Ljava/lang/String;

.field btnListener:Landroid/view/View$OnClickListener;

.field mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field mIvAlbum:Landroid/widget/ImageView;

.field mIvEqualizer:Landroid/widget/ImageView;

.field mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;-><init>()V

    .line 42
    const-string/jumbo v0, "[IndicatorFragmentViewForMulti]"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->TAG:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mContext:Landroid/content/Context;

    .line 45
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvEqualizer:Landroid/widget/ImageView;

    .line 46
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    .line 189
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$1;-><init>(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->btnListener:Landroid/view/View$OnClickListener;

    .line 203
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;-><init>(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 263
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$3;-><init>(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mHandler:Landroid/os/Handler;

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->refreshViews()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;Lcom/nuance/sample/MicState;)V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->clickMic(Lcom/nuance/sample/MicState;)V

    return-void
.end method

.method private clickMic(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "micState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 276
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 279
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick() : PhraseSpoteer is listening"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 282
    const-string/jumbo v2, "[stop] : OnClickMicListenerImpl - onClick()"

    .line 281
    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->stopPhraseSpotting()V

    .line 284
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 285
    invoke-virtual {v1, v4}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->setStartRecoOnSpotterStop(Z)V

    goto :goto_0

    .line 288
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v0

    .line 289
    .local v0, "success":Z
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onClick() mMicState IDLE, startUserFlow returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 289
    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    if-nez v0, :cond_0

    .line 293
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->TAG:Ljava/lang/String;

    .line 294
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onClick() mMicState IDLE, startUserFlow error, returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 295
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 294
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 293
    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 300
    .end local v0    # "success":Z
    :pswitch_1
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 301
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/nuance/drivelink/DLAppUiUpdater;->setCancelByTouch(Z)V

    .line 305
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 306
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick() mMicState LISTENING, so called endpointReco"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 309
    :pswitch_2
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 313
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 314
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick() mMicState THINKING, so called cancelTurn"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private refreshViews()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/home/Indicator_UI_Func;->setAlbum(Landroid/widget/ImageView;Z)V

    .line 156
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvEqualizer:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/home/Indicator_UI_Func;->setEqualizer(Landroid/widget/ImageView;Z)V

    .line 157
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->setAlbumDraw(Z)V

    .line 168
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/home/Indicator_UI_Func;->setAlbum(Landroid/widget/ImageView;Z)V

    .line 160
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvEqualizer:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/home/Indicator_UI_Func;->setEqualizer(Landroid/widget/ImageView;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 165
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/home/Indicator_UI_Func;->setAlbum(Landroid/widget/ImageView;Z)V

    .line 166
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvEqualizer:Landroid/widget/ImageView;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/home/Indicator_UI_Func;->setEqualizer(Landroid/widget/ImageView;Z)V

    goto :goto_0
.end method

.method private setAlbumDraw(Z)V
    .locals 5
    .param p1, "isPlaying"    # Z

    .prologue
    .line 171
    if-eqz p1, :cond_0

    .line 173
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v2

    .line 174
    .local v2, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v3

    .line 175
    invoke-virtual {v3, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 177
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 178
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 187
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :cond_0
    :goto_0
    return-void

    .line 180
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    const v4, 0x7f020322

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 181
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 183
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :catch_0
    move-exception v1

    .line 184
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$4;-><init>(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    .line 128
    const-wide/16 v2, 0xc8

    .line 117
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 129
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mContext:Landroid/content/Context;

    .line 56
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const v2, 0x7f030097

    .line 71
    const/4 v3, 0x0

    .line 70
    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 73
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f09029a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    .line 72
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    .line 74
    const v2, 0x7f09029b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    .line 75
    const v2, 0x7f09029c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvEqualizer:Landroid/widget/ImageView;

    .line 77
    const v2, 0x7f090299

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 78
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->btnListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvAlbum:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->btnListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mIvEqualizer:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 82
    const v4, 0x7f04001f

    .line 81
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 84
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 87
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 88
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->registerListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->setTopAreaHandler(Landroid/os/Handler;)V

    .line 93
    return-object v1

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onDestroy()V

    .line 63
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 99
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 100
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->unregisterListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :cond_0
    :goto_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 107
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->getInstance()Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener;->setTopAreaHandler(Landroid/os/Handler;)V

    .line 109
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onDestroyView()V

    .line 110
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onPause()V

    .line 143
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onResume()V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->refreshViews()V

    .line 136
    return-void
.end method

.method public setMicClickable(Z)V
    .locals 1
    .param p1, "clickable"    # Z

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->setMicClickable(Z)V

    .line 149
    :cond_0
    return-void
.end method
