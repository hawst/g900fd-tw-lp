.class public Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
.source "TMapNavigationAdapter.java"


# static fields
.field private static naviPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;-><init>()V

    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setNeedOrigin(Z)V

    .line 28
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 29
    const-string/jumbo v1, "com.skt.skaf.l001mtm091"

    .line 28
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    const-string/jumbo v0, "com.skt.skaf.l001mtm091"

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    .line 33
    :goto_0
    return-void

    .line 32
    :cond_0
    const-string/jumbo v0, "com.skt.skaf.l001mtm092"

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    goto :goto_0
.end method

.method private setNaviPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 248
    sput-object p1, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    .line 249
    return-void
.end method


# virtual methods
.method public exitNavigation(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.TMAP4_END"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 259
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 260
    return-void
.end method

.method public getNaviPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "address"    # Ljava/lang/String;

    .prologue
    .line 40
    const-string/jumbo v1, ""

    .line 41
    .local v1, "tMapUrl":Ljava/lang/String;
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 42
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "tmap://search?name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 64
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v0, "navigationIntent":Landroid/content/Intent;
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    .line 66
    const-string/jumbo v3, "com.skt.tmap.activity.TmapIntroActivity"

    .line 65
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setNaviPackageName(Ljava/lang/String;)V

    .line 70
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 71
    const/high16 v2, 0x200000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 72
    const v2, 0x8000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 73
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 74
    return-object v0

    .line 61
    .end local v0    # "navigationIntent":Landroid/content/Intent;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "tmap://route?goalx="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "&goaly="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 62
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 61
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getNavigationIntentOption(DDLjava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "address"    # Ljava/lang/String;

    .prologue
    .line 80
    const-string/jumbo v1, ""

    .line 81
    .local v1, "tMapUrl":Ljava/lang/String;
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 82
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "tmap://search?name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 105
    .local v0, "navigationIntent":Landroid/content/Intent;
    const-string/jumbo v2, ""

    .line 106
    const-string/jumbo v3, "com.skt.tmap.activity.TmapIntroActivity"

    .line 105
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setNaviPackageName(Ljava/lang/String;)V

    .line 109
    const/high16 v2, 0x10200000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 111
    const-string/jumbo v2, "LatLongTMAP"

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 113
    return-object v0

    .line 101
    .end local v0    # "navigationIntent":Landroid/content/Intent;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "tmap://route?goalx="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "&goaly="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 102
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 101
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public startMap(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 220
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMap(Landroid/content/Context;)V

    .line 221
    const-string/jumbo v2, "TMapNavigationAdapter"

    const-string/jumbo v3, "startMap"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const v2, 0x7f0a061c

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "mTts":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .line 226
    sget-object v2, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    const-string/jumbo v3, "com.skt.tmap.activity.TmapIntroActivity"

    .line 225
    invoke-direct {v1, p1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    .local v1, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 232
    return-void
.end method

.method public startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pacel"    # Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .prologue
    .line 117
    const/4 v12, 0x1

    .line 118
    .local v12, "ret":Z
    const-string/jumbo v0, "TMapNavigationAdapter"

    const-string/jumbo v1, "startMultiNavi"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget v0, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    if-nez v0, :cond_1

    .line 121
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 122
    .local v10, "navigationIntent":Landroid/content/Intent;
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    .line 123
    const-string/jumbo v1, "com.skt.tmap.activity.TmapIntroActivity"

    .line 122
    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setNaviPackageName(Ljava/lang/String;)V

    .line 127
    const/high16 v0, 0x10000000

    invoke-virtual {v10, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 129
    const/high16 v0, 0x200000

    invoke-virtual {v10, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 130
    const v0, 0x8000

    invoke-virtual {v10, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 132
    :try_start_0
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v10

    .line 135
    invoke-virtual {p1, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :goto_0
    return v12

    .line 137
    :catch_0
    move-exception v6

    .line 138
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 140
    :try_start_1
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    .line 141
    .local v11, "navigationOptionIntent":Landroid/content/Intent;
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    .line 142
    const-string/jumbo v1, "com.skt.tmap.activity.TmapIntroActivity"

    .line 141
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->naviPackageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setNaviPackageName(Ljava/lang/String;)V

    .line 148
    const/high16 v0, 0x10000000

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 150
    const/high16 v0, 0x200000

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 152
    const v0, 0x8000

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0, v11}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v11

    .line 156
    invoke-virtual {p1, v11}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 157
    .end local v11    # "navigationOptionIntent":Landroid/content/Intent;
    :catch_1
    move-exception v7

    .line 158
    .local v7, "eopt":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 161
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 160
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 164
    const-string/jumbo v1, "PREF_SETTINGS_MY_NAVIGATION"

    .line 165
    const/4 v2, 0x0

    .line 163
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v9

    .line 166
    .local v9, "mapID":I
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v8

    .line 167
    .local v8, "goToMarket":Landroid/content/Intent;
    if-eqz v8, :cond_0

    .line 168
    invoke-virtual {p1, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 170
    :cond_0
    const-string/jumbo v0, "Error T Map Navi"

    .line 171
    const-string/jumbo v1, "Error on starting T Map Navigation Intent"

    .line 170
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 176
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v7    # "eopt":Ljava/lang/Exception;
    .end local v8    # "goToMarket":Landroid/content/Intent;
    .end local v9    # "mapID":I
    .end local v10    # "navigationIntent":Landroid/content/Intent;
    :cond_1
    iget-wide v1, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 177
    iget-wide v3, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    iget-object v5, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    move-object v0, p0

    .line 176
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->getNavigationIntent(DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 180
    .restart local v10    # "navigationIntent":Landroid/content/Intent;
    :try_start_2
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v10

    .line 183
    invoke-virtual {p1, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 185
    :catch_2
    move-exception v6

    .line 186
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 190
    :try_start_3
    iget-wide v1, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    iget-wide v3, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    iget-object v5, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    move-object v0, p0

    .line 189
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->getNavigationIntentOption(DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    .line 191
    .restart local v11    # "navigationOptionIntent":Landroid/content/Intent;
    invoke-virtual {p0, v11}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v11

    .line 194
    invoke-virtual {p1, v11}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 195
    .end local v11    # "navigationOptionIntent":Landroid/content/Intent;
    :catch_3
    move-exception v7

    .line 198
    .restart local v7    # "eopt":Ljava/lang/Exception;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 197
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 201
    const-string/jumbo v1, "PREF_SETTINGS_MY_NAVIGATION"

    .line 202
    const/4 v2, 0x0

    .line 200
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v9

    .line 203
    .restart local v9    # "mapID":I
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v8

    .line 204
    .restart local v8    # "goToMarket":Landroid/content/Intent;
    if-eqz v8, :cond_2

    .line 205
    invoke-virtual {p1, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 207
    :cond_2
    const-string/jumbo v0, "Error T Map Navi"

    .line 208
    const-string/jumbo v1, "Error on starting T Map Navigation Intent"

    .line 207
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public startNavigation(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    .line 237
    const-string/jumbo v1, "TMapNavigationAdapter"

    const-string/jumbo v2, "startNavigation"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;DDLjava/lang/String;)V

    .line 240
    .local v0, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p6, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 245
    return-void
.end method
