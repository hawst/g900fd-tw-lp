.class Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$3;
.super Ljava/lang/Object;
.source "TaskManagerReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->onAppPaused()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 204
    const-string/jumbo v0, "TaskManagerReceiver"

    const-string/jumbo v1, "[onAppPaused] run"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestStopDrivingStatusMonitoring(Landroid/content/Context;)V

    .line 209
    const-string/jumbo v0, "TaskManagerReceiver"

    const-string/jumbo v1, "stop driving manager paused complete - background"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    const-string/jumbo v0, "TaskManagerReceiver"

    const-string/jumbo v1, "[onAppPaused] send cancelTurnHandler"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->cancelTurnHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$3(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 216
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$3;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startWakeupBargeIn(Landroid/content/Context;)V

    .line 218
    return-void
.end method
