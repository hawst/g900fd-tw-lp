.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;
.source "DLPlaylist.java"


# static fields
.field public static final FAVORITE:Ljava/lang/String; = "Favorite"

.field public static final MOST_PLAYED:Ljava/lang/String; = "Most Played"

.field public static final RECENTLY_ADDED:Ljava/lang/String; = "Recently Added"

.field public static final RECENTLY_PLAYED:Ljava/lang/String; = "Recently Played"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicContainer;-><init>()V

    return-void
.end method
