.class public Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
.super Landroid/widget/LinearLayout;
.source "DotIndicatorLayout.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentIndex:I

.field private mDotCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mContext:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mCurrentIndex:I

    return v0
.end method

.method public setDotIndicator(IILandroid/view/View$OnClickListener;)V
    .locals 6
    .param p1, "dotCount"    # I
    .param p2, "index"    # I
    .param p3, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x7

    const/16 v3, 0xa

    .line 29
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mDotCount:I

    .line 32
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mDotCount:I

    if-lt v1, v2, :cond_0

    .line 48
    invoke-virtual {p0, p2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setSelected(Z)V

    .line 49
    iput p2, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mCurrentIndex:I

    .line 50
    return-void

    .line 33
    :cond_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 34
    .local v0, "dotBtn":Landroid/widget/ImageButton;
    const v2, 0x7f02015c

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 35
    const v2, 0x106000d

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 36
    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mDotCount:I

    if-le v2, v5, :cond_1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mDotCount:I

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_1

    .line 37
    const/16 v2, 0xf

    invoke-virtual {v0, v4, v3, v2, v3}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 42
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setId(I)V

    .line 43
    invoke-virtual {v0, p3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->addView(Landroid/view/View;)V

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    :cond_1
    invoke-virtual {v0, v4, v3, v4, v3}, Landroid/widget/ImageButton;->setPadding(IIII)V

    goto :goto_1
.end method

.method public setIndex(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mCurrentIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 55
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->mCurrentIndex:I

    .line 56
    return-void
.end method
