.class Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$9;
.super Ljava/lang/Object;
.source "LocationRequestActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getUpdateSuggestionContactListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    .line 416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateSuggestionList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 420
    .local p1, "contactlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$19()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onUpdateSuggestionList"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$4(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Ljava/util/ArrayList;)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->updateContacts()V

    .line 423
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->setSuggetionListFlowParam()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$20(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    .line 424
    return-void
.end method
