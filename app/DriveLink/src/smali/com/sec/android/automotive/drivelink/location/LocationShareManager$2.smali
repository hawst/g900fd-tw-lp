.class Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;
.super Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
.source "LocationShareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->acceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->val$url:Ljava/lang/String;

    .line 175
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;-><init>()V

    return-void
.end method


# virtual methods
.method protected onResponseRequestGeoLocation(Landroid/location/Location;Z)V
    .locals 7
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 180
    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    .line 181
    :cond_0
    const-string/jumbo v0, "LocationManager"

    const-string/jumbo v1, "Fail to get my current location!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 185
    const v1, 0x7f0a0391

    .line 184
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationFailedRequestSent(I)V

    .line 195
    :goto_0
    return-void

    .line 189
    :cond_1
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 190
    .local v4, "dlLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLatitude(D)V

    .line 191
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLongitude(D)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareManager;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->mDLinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareManager;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationShareManager;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareManager$2;->val$url:Ljava/lang/String;

    .line 194
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    const-wide/16 v5, 0x0

    .line 193
    invoke-interface/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestAcceptShareMyLocation(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V

    goto :goto_0
.end method
