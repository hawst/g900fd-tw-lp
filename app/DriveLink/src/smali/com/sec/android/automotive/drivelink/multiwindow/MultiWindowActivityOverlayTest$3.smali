.class Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;
.super Ljava/lang/Object;
.source "MultiWindowActivityOverlayTest.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    .line 844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private removeNotiView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 3
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 846
    if-nez p1, :cond_1

    .line 867
    :cond_0
    :goto_0
    return-void

    .line 849
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$4(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 850
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->dismissNotification()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$5(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    goto :goto_0

    .line 852
    :cond_2
    const-string/jumbo v0, "[MultiWindowActivity]"

    .line 853
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "WARNING!! This view isn\'t showing. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 852
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$6(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 855
    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$0()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 856
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 855
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 857
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->resetNotiFlowId()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$7(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V

    .line 858
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$6(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 859
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->removeOverlayNotiView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$8(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 860
    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$9()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 861
    # getter for: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$9()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    move-result-object v0

    .line 862
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V

    .line 863
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$10(Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;)V

    goto :goto_0
.end method


# virtual methods
.method public onRemoveDirect(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "gotoMulti"    # Z

    .prologue
    .line 894
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "onRemoveDirect"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->removeNotiView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 896
    return-void
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 872
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Remove Notification Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->removeNotiView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 874
    return-void
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "command"    # Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .prologue
    .line 879
    const-string/jumbo v0, "[MultiWindowActivity]"

    const-string/jumbo v1, "Remove Notification Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->removeNotiView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 882
    return-void
.end method

.method public onSetVoiceFlowIdRequest(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 887
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "flowId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is requested!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$3;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    # invokes: Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->setNotiFlowId(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->access$11(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;Ljava/lang/String;)V

    .line 889
    return-void
.end method
