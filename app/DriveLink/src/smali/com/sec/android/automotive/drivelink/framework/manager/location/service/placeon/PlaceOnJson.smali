.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/PlaceOnJson;
.super Ljava/lang/Object;
.source "PlaceOnJson.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 5
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 42
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 43
    .local v0, "builder":Lcom/google/gson/GsonBuilder;
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithoutExposeAnnotation()Lcom/google/gson/GsonBuilder;

    .line 44
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    .line 46
    .local v1, "gson":Lcom/google/gson/Gson;
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    .line 47
    const-class v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .line 46
    invoke-virtual {v1, v3, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .line 49
    .local v2, "profile":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    return-object v2
.end method

.method public static toJson(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Lorg/json/JSONObject;
    .locals 7
    .param p0, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 22
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 23
    .local v0, "builder":Lcom/google/gson/GsonBuilder;
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithoutExposeAnnotation()Lcom/google/gson/GsonBuilder;

    .line 24
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    .line 26
    .local v2, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v2, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 28
    .local v4, "json":Ljava/lang/String;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 29
    .local v3, "jObj":Lorg/json/JSONObject;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v5

    if-lez v5, :cond_0

    .line 30
    const-string/jumbo v5, "userId"

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;->getUserId()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v3    # "jObj":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v3

    .line 34
    :catch_0
    move-exception v1

    .line 36
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 38
    const/4 v3, 0x0

    goto :goto_0
.end method
