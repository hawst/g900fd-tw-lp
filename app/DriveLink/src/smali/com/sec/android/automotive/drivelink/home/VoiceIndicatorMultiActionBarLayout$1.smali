.class Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout$1;
.super Ljava/lang/Object;
.source "VoiceIndicatorMultiActionBarLayout.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->setAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 227
    const-string/jumbo v0, "[VoiceIndicatorMultiActionBarLayout]"

    const-string/jumbo v1, "mStartQAni End"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicEq:Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;)Lcom/sec/android/automotive/drivelink/common/view/ListeningView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->mMicStartQBg:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->access$2(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;->access$3(Lcom/sec/android/automotive/drivelink/home/VoiceIndicatorMultiActionBarLayout;Z)V

    .line 232
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 223
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 219
    return-void
.end method
