.class Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->initializeProfileProxyHFP()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field result:I

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    .line 918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 5
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 923
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "[BT CHECK]onServiceConnected: HFP"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 925
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    iput-object p2, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 927
    const/4 v0, 0x0

    .line 928
    .local v0, "btDeviceHFP":Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 929
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 931
    .local v1, "btDevicesHFP":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 932
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 941
    if-eqz v0, :cond_0

    .line 942
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$20(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 943
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$19(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->saveBTConnection(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Ljava/lang/String;)V

    .line 946
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 933
    .restart local v2    # "i":I
    :cond_1
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "btDeviceHFP":Landroid/bluetooth/BluetoothDevice;
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 934
    .restart local v0    # "btDeviceHFP":Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iget-object v3, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 935
    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    .line 934
    iput v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->result:I

    .line 936
    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->result:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 937
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$20(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 932
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    const/4 v2, 0x0

    .line 950
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "[BT CHECK]onServiceDisconnected: HFP"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 952
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$20(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 953
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 954
    return-void
.end method
