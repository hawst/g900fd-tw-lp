.class Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;
.super Ljava/lang/Object;
.source "SettingsRejectMessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 191
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 192
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->btnRejectMessageCancel:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 193
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 196
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRejectMessageActivity;->etInput:Landroid/widget/EditText;

    .line 197
    const/4 v2, 0x1

    .line 196
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 198
    return-void
.end method
