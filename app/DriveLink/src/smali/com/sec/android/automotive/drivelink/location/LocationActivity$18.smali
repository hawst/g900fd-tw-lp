.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->showRecommendLocationsList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field isLoaded:Z

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 1333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->isLoaded:Z

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1359
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Z)V

    .line 1360
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationActivity;Z)V

    .line 1361
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 1344
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->isLoaded:Z

    if-eqz v0, :cond_0

    .line 1345
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->isLoaded:Z

    .line 1346
    if-nez p1, :cond_1

    .line 1347
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$16(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    move-result-object v1

    .line 1348
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getInitialPosition()I

    move-result v1

    .line 1347
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1349
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1355
    :cond_0
    :goto_0
    return-void

    .line 1351
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1352
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$16(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1351
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1338
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 1339
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$18;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationPageAdapter:Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$16(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 1338
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 1340
    return-void
.end method
