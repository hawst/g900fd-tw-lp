.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestUpdateApplication.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsCheckVersion:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isCheckVersion"    # Z
    .param p3, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 13
    invoke-direct {p0, p3}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->mContext:Landroid/content/Context;

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->mIsCheckVersion:Z

    .line 15
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->mContext:Landroid/content/Context;

    .line 16
    iput-boolean p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->mIsCheckVersion:Z

    .line 17
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getUpdateManager()Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->mContext:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateApplication;->mIsCheckVersion:Z

    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->updateApplication(Landroid/content/Context;Z)V

    .line 23
    return-void
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method
