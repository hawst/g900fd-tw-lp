.class Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "JoinedParticipantAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private ivUser:Landroid/widget/ImageView;

.field private tvName:Landroid/widget/TextView;

.field private tvUserLetter:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->ivUser:Landroid/widget/ImageView;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvUserLetter:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->ivUser:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvUserLetter:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/adapter/JoinedParticipantAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    return-object v0
.end method
