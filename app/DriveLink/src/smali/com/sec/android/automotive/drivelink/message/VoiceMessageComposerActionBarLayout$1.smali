.class Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;
.super Landroid/os/Handler;
.source "VoiceMessageComposerActionBarLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    .line 623
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mIsMicDisplayed:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$0(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->access$2(Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 632
    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 633
    return-void
.end method
