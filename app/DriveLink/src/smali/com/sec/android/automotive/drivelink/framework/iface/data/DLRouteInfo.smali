.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteInfo;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLRouteInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x7435742b9434195dL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getArrivalTime()J
.end method

.method public abstract getIncidents()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPolyline()Ljava/lang/String;
.end method

.method public abstract getRoutePoints()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTotalDistance()D
.end method

.method public abstract getTravelTime()I
.end method

.method public abstract getTravelTimeInfo()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLRouteTravelTimeInfo;",
            ">;"
        }
    .end annotation
.end method
