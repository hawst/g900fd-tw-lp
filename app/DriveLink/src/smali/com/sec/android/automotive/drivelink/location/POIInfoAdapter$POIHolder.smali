.class final Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;
.super Ljava/lang/Object;
.source "POIInfoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "POIHolder"
.end annotation


# instance fields
.field private address:Landroid/widget/TextView;

.field private distance:Landroid/widget/TextView;

.field private info1:Landroid/widget/TextView;

.field private info2:Landroid/widget/TextView;

.field private separator1:Landroid/view/View;

.field private separator2:Landroid/view/View;

.field private title:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;-><init>()V

    return-void
.end method


# virtual methods
.method public getAddress()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->address:Landroid/widget/TextView;

    return-object v0
.end method

.method public getDistance()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->distance:Landroid/widget/TextView;

    return-object v0
.end method

.method public getInfo1()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->info1:Landroid/widget/TextView;

    return-object v0
.end method

.method public getInfo2()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->info2:Landroid/widget/TextView;

    return-object v0
.end method

.method public getSeparator1()Landroid/view/View;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->separator1:Landroid/view/View;

    return-object v0
.end method

.method public getSeparator2()Landroid/view/View;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->separator2:Landroid/view/View;

    return-object v0
.end method

.method public getTitle()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->title:Landroid/widget/TextView;

    return-object v0
.end method

.method public setAddress(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "address"    # Landroid/widget/TextView;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->address:Landroid/widget/TextView;

    .line 190
    return-void
.end method

.method public setDistance(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "distance"    # Landroid/widget/TextView;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->distance:Landroid/widget/TextView;

    .line 214
    return-void
.end method

.method public setInfo1(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "info1"    # Landroid/widget/TextView;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->info1:Landroid/widget/TextView;

    .line 198
    return-void
.end method

.method public setInfo2(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "info2"    # Landroid/widget/TextView;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->info2:Landroid/widget/TextView;

    .line 206
    return-void
.end method

.method public setSeparator1(Landroid/view/View;)V
    .locals 0
    .param p1, "separator1"    # Landroid/view/View;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->separator1:Landroid/view/View;

    .line 222
    return-void
.end method

.method public setSeparator2(Landroid/view/View;)V
    .locals 0
    .param p1, "separator2"    # Landroid/view/View;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->separator2:Landroid/view/View;

    .line 230
    return-void
.end method

.method public setTitle(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "title"    # Landroid/widget/TextView;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/POIInfoAdapter$POIHolder;->title:Landroid/widget/TextView;

    .line 182
    return-void
.end method
