.class Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;
.super Ljava/lang/Object;
.source "CarModeCheckReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 95
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 96
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 98
    const v3, 0x7f0a026a

    .line 97
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 99
    const/16 v3, 0xbb8

    .line 94
    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 102
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 103
    const-string/jumbo v2, "car_mode_on"

    .line 101
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    .line 103
    const/4 v2, 0x1

    .line 101
    if-ne v1, v2, :cond_0

    .line 104
    const-string/jumbo v1, "[CarModeCheckReceiver]"

    const-string/jumbo v2, "car_mode_on is 1, reset system settings, unblock system key"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 107
    const-string/jumbo v2, "car_mode_blocking_system_key"

    const/4 v3, 0x0

    .line 105
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 109
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 110
    const-string/jumbo v2, "car_mode_on"

    const/4 v3, 0x0

    .line 108
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 112
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 113
    const-string/jumbo v2, "vibrate_when_ringing"

    .line 114
    sget v3, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->VIBRATE_WHEN_RINGING:I

    .line 111
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
