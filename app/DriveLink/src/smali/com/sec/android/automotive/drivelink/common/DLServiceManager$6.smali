.class Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;
.super Ljava/lang/Object;
.source "DLServiceManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestAlbumList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 876
    .local p1, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$46(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 877
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$47(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 879
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByAlbum:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$34(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_1

    .line 880
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$48(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V

    .line 885
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mAlbumList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$49(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 888
    return-void

    .line 881
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByAlbum:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$34(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 882
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByAlbum:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$34(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    goto :goto_0

    .line 885
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 886
    .local v0, "album":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateMusicListByAlbum(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V

    goto :goto_1
.end method

.method public onResponseRequestArtistList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 860
    .local p1, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$42(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 861
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$43(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 863
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByArtist:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$32(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_1

    .line 864
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$44(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V

    .line 869
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mArtistList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$45(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 872
    return-void

    .line 865
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByArtist:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$32(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 866
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByArtist:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$32(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    goto :goto_0

    .line 869
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 870
    .local v0, "artist":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateMusicListByArtist(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V

    goto :goto_1
.end method

.method public onResponseRequestFolderList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 843
    .local p1, "folderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;>;"
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$38(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 844
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$39(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 846
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByFolder:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$30(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_1

    .line 847
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$40(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V

    .line 852
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mFolderList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$41(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 855
    return-void

    .line 848
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByFolder:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$30(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 849
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByFolder:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$30(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    goto :goto_0

    .line 852
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    .line 853
    .local v0, "folder":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateMusicListByFolder(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V

    goto :goto_1
.end method

.method public onResponseRequestMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 916
    return-void
.end method

.method public onResponseRequestMusicList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 836
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$36(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 837
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$37(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 838
    return-void
.end method

.method public onResponseRequestMusicListByAlbum(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 2
    .param p2, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ")V"
        }
    .end annotation

    .prologue
    .line 830
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByAlbum:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$34(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 831
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$35(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 832
    return-void
.end method

.method public onResponseRequestMusicListByArtist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 2
    .param p2, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 823
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByArtist:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$32(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$33(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 825
    return-void
.end method

.method public onResponseRequestMusicListByFolder(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
    .locals 2
    .param p2, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 816
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByFolder:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$30(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$31(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 818
    return-void
.end method

.method public onResponseRequestMusicListByMusic(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ")V"
        }
    .end annotation

    .prologue
    .line 811
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByPlaylist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
    .locals 2
    .param p2, "playlist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 802
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByPlay:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$26(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$29(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 804
    return-void
.end method

.method public onResponseRequestMusicListFromSearchResult(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
    .locals 0
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 903
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestPlaylistList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 785
    .local p1, "playlistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;>;"
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$24(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 786
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$25(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 788
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByPlay:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$26(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_1

    .line 789
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$27(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/Map;)V

    .line 794
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPlayList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$28(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 797
    return-void

    .line 790
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByPlay:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$26(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 791
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mMusicListByPlay:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$26(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    goto :goto_0

    .line 794
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;

    .line 795
    .local v0, "playlist":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$6;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateMusicListByPlay(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V

    goto :goto_1
.end method

.method public onResponseRequestSearchAllMusic(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 910
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestSearchMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 0
    .param p1, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .prologue
    .line 895
    return-void
.end method
