.class Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->DialogExit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->finish()V

    .line 83
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 84
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->terminate(Landroid/content/Context;)V

    .line 86
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->setWhenOnDestoryed()V

    .line 87
    return-void
.end method
