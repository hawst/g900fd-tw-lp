.class public Lcom/sec/android/automotive/drivelink/common/view/easing/BounceEaseOut;
.super Ljava/lang/Object;
.source "BounceEaseOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 7
    .param p1, "input"    # F

    .prologue
    const/high16 v6, 0x40f20000    # 7.5625f

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 16
    div-float/2addr p1, v4

    const v0, 0x3eba2e8c

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 17
    mul-float v0, v6, p1

    mul-float/2addr v0, p1

    mul-float/2addr v0, v4

    add-float/2addr v0, v5

    .line 23
    :goto_0
    return v0

    .line 18
    :cond_0
    float-to-double v0, p1

    const-wide v2, 0x3fe745d1745d1746L    # 0.7272727272727273

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 19
    const v0, 0x3f0ba2e9

    sub-float/2addr p1, v0

    mul-float v0, v6, p1

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f400000    # 0.75f

    add-float/2addr v0, v1

    mul-float/2addr v0, v4

    add-float/2addr v0, v5

    goto :goto_0

    .line 20
    :cond_1
    float-to-double v0, p1

    const-wide v2, 0x3fed1745d1745d17L    # 0.9090909090909091

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 21
    const v0, 0x3f51745d

    sub-float/2addr p1, v0

    mul-float v0, v6, p1

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f700000    # 0.9375f

    add-float/2addr v0, v1

    mul-float/2addr v0, v4

    add-float/2addr v0, v5

    goto :goto_0

    .line 23
    :cond_2
    const v0, 0x3f745d17

    sub-float/2addr p1, v0

    mul-float v0, v6, p1

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f7c0000    # 0.984375f

    add-float/2addr v0, v1

    mul-float/2addr v0, v4

    add-float/2addr v0, v5

    goto :goto_0
.end method
