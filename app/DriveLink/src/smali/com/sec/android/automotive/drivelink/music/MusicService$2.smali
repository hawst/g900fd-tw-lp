.class Lcom/sec/android/automotive/drivelink/music/MusicService$2;
.super Ljava/lang/Object;
.source "MusicService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 1207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMusicRemotePlayerErrorStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1431
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1432
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "onMusicRemotePlayerErrorStop is playing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1433
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    .line 1434
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 1435
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    .line 1436
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1442
    :cond_0
    :goto_0
    return-void

    .line 1438
    :cond_1
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "onMusicRemotePlayerErrorStop AudioFocus Loss state"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    goto :goto_0
.end method

.method public onMusicRemotePlayerFForward(Z)V
    .locals 5
    .param p1, "flag"    # Z

    .prologue
    const/4 v4, 0x1

    .line 1390
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerFForward"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1392
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1412
    :goto_0
    return-void

    .line 1397
    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1398
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setFFREInit()V

    .line 1399
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-direct {v1, v4}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;-><init>(Z)V

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$17(Lcom/sec/android/automotive/drivelink/music/MusicService;Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;)V

    .line 1400
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    iput-boolean v4, v0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 1401
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->start()V

    .line 1406
    :cond_1
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicSetValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1407
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTSeek()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$11(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1411
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0

    .line 1402
    :cond_3
    if-nez p1, :cond_1

    .line 1403
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->setFFREThreadWithStop()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$18(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1404
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSetValue(Z)V

    goto :goto_1
.end method

.method public onMusicRemotePlayerNext()V
    .locals 4

    .prologue
    .line 1368
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerNext"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1370
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    :goto_0
    return-void

    .line 1377
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1378
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayerNoPlay()V

    .line 1384
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0

    .line 1381
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 1382
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayer()V

    goto :goto_1
.end method

.method public onMusicRemotePlayerPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1319
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerPaused"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1321
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1331
    :goto_0
    return-void

    .line 1324
    :cond_0
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICService RemoteControl User Setting Pause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1325
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "onMusicRemotePlayerPause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    .line 1327
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, v1, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 1328
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    .line 1330
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0
.end method

.method public onMusicRemotePlayerPlay()V
    .locals 5

    .prologue
    .line 1231
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerStrartPause"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1233
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    :goto_0
    return-void

    .line 1237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$13(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1238
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerPlay input by BT"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mNotBTControlPriority : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$13(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1243
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTCallingState()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$14(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0

    .line 1247
    :cond_2
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MusicService setMusicPlayerVolume by RemoteControl onMusicRemotePlayerPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 1249
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTPlay()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$15(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1250
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->playPlayer()V

    .line 1251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0
.end method

.method public onMusicRemotePlayerPlayPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1337
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerPlayPause"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1339
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    :goto_0
    return-void

    .line 1343
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$13(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1344
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerPlay input by BT"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1345
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mNotBTControlPriority : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mNotBTControlPriority:Z
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$13(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1349
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1350
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICService RemoteControl User Setting Pause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1351
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "onMusicRemotePlayerPlayPause"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    .line 1353
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, v1, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 1354
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;->pausePlayer(I)V

    .line 1361
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0

    .line 1356
    :cond_2
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MusicService setMusicPlayerVolume by RemoteControl onMusicRemotePlayerPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 1358
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTPlay()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$15(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1359
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->playPlayer()V

    goto :goto_1
.end method

.method public onMusicRemotePlayerPrevious()V
    .locals 4

    .prologue
    .line 1296
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerPrevious"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1298
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1313
    :goto_0
    return-void

    .line 1305
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1306
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayerNoPlay()V

    .line 1312
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0

    .line 1309
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 1310
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayer(Z)V

    goto :goto_1
.end method

.method public onMusicRemotePlayerRewind(Z)V
    .locals 5
    .param p1, "flag"    # Z

    .prologue
    const/4 v4, 0x1

    .line 1270
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerRewind"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1272
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    :goto_0
    return-void

    .line 1276
    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1277
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setFFREInit()V

    .line 1278
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;-><init>(Z)V

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$17(Lcom/sec/android/automotive/drivelink/music/MusicService;Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;)V

    .line 1279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    iput-boolean v4, v0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 1280
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->start()V

    .line 1285
    :cond_1
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicSetValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTSeek()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$11(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1289
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0

    .line 1281
    :cond_3
    if-nez p1, :cond_1

    .line 1282
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->setFFREThreadWithStop()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$18(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1283
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSetValue(Z)V

    goto :goto_1
.end method

.method public onMusicRemotePlayerShuffle(Z)V
    .locals 4
    .param p1, "bShuffle"    # Z

    .prologue
    .line 1418
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerShuffle"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1420
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    :goto_0
    return-void

    .line 1424
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyShuffle(Z)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$19(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0
.end method

.method public onMusicRemotePlayerStop()V
    .locals 4

    .prologue
    .line 1258
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePlayerStopped"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1260
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    :goto_0
    return-void

    .line 1264
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->stopPlayer()V

    .line 1265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0
.end method

.method public onMusicRemotePositionUpdate(I)V
    .locals 4
    .param p1, "newPositionMs"    # I

    .prologue
    .line 1213
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "onMusicRemotePositionUpdate"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1215
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    const-string/jumbo v3, "MUSIC.getService() == null"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    :goto_0
    return-void

    .line 1219
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    .line 1223
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTSeek()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$11(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    .line 1224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyBTClicked()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicService;)V

    goto :goto_0
.end method
