.class Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;
.super Ljava/lang/Object;
.source "SettingsRegisterCarOptionsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 136
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v2

    .line 137
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getPairedBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 138
    .local v0, "pairedDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_1

    .line 139
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->removeBond()Z

    move-result v1

    .line 140
    .local v1, "result":Z
    if-eqz v1, :cond_0

    .line 141
    const-string/jumbo v2, "[SettingsRegisterCarOptionsActivity]"

    .line 142
    const-string/jumbo v3, "remove bond success."

    .line 141
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 146
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v3

    .line 144
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->removeRegisteredCarsPreference(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 148
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->finish()V

    .line 165
    .end local v1    # "result":Z
    :goto_0
    return-void

    .line 150
    .restart local v1    # "result":Z
    :cond_0
    const-string/jumbo v2, "[SettingsRegisterCarOptionsActivity]"

    .line 151
    const-string/jumbo v3, "remove bond failed."

    .line 150
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 154
    .end local v1    # "result":Z
    :cond_1
    const-string/jumbo v2, "[SettingsRegisterCarOptionsActivity]"

    .line 155
    const-string/jumbo v3, "not paired"

    .line 154
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 161
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    move-result-object v3

    .line 159
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->removeRegisteredCarsPreference(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->finish()V

    goto :goto_0
.end method
