.class Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;
.super Ljava/lang/Object;
.source "SplashScreenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$3(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$5(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$4(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$5(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$4(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$6(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$6(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mScaleExAnime:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$7(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mNextBtnView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$1(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    .line 212
    return-void
.end method
