.class Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;
.super Landroid/os/Handler;
.source "PrepareDialActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    .line 472
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v8, 0x7f0d0021

    const v7, 0x7f02001c

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 476
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 478
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$0(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$1(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;I)V

    .line 479
    const-string/jumbo v2, "PrepareDial"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "bargein state : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mState:I
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 482
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$3(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v2

    if-lez v2, :cond_1

    .line 483
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$3(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;I)V

    .line 485
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f040011

    .line 484
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 487
    .local v1, "aniRing":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f040010

    .line 486
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 489
    .local v0, "aniNum":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$3(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 507
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 512
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mProgress:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$6(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 513
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 514
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesStart:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$7(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 515
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mQuotesEnd:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$8(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 516
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCallText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$9(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 518
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mProgress:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$6(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 519
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 520
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->timer:Landroid/os/Handler;

    const-wide/16 v3, 0x535

    invoke-virtual {v2, v5, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 579
    .end local v0    # "aniNum":Landroid/view/animation/Animation;
    .end local v1    # "aniRing":Landroid/view/animation/Animation;
    :cond_0
    :goto_1
    return-void

    .line 491
    .restart local v0    # "aniNum":Landroid/view/animation/Animation;
    .restart local v1    # "aniRing":Landroid/view/animation/Animation;
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 495
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 496
    const v4, 0x7f02001b

    .line 495
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 499
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 500
    const v4, 0x7f02001a

    .line 499
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 503
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->count:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 504
    const v4, 0x7f020019

    .line 503
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 521
    .end local v0    # "aniNum":Landroid/view/animation/Animation;
    .end local v1    # "aniRing":Landroid/view/animation/Animation;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->remainedTime:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$3(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)I

    move-result v2

    if-nez v2, :cond_0

    .line 523
    const-string/jumbo v2, "MULTI_WINDOW_ENABLE"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 524
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->isMultiWindow()Z
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$10(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 528
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mThumbImg:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$11(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 531
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 532
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    .line 531
    if-ne v2, v3, :cond_4

    .line 535
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 536
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 537
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$12(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    .line 539
    const v4, 0x7f0d0023

    .line 534
    invoke-static {v2, v3, v8, v4}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiModeSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;II)Z

    .line 552
    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCountDownLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$13(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 553
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mCancelCall:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 554
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mOutgoingLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$15(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/RelativeLayout;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 555
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mOutgoingLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$15(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 574
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$16(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mPhoneNumber:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$17(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->makeCall(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 543
    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 544
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 545
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;->access$12(Lcom/sec/android/automotive/drivelink/phone/PrepareDialActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    .line 547
    const v4, 0x7f0d0022

    .line 542
    invoke-static {v2, v3, v8, v4}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowUtils;->setMultiModeSize(Landroid/content/Context;Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;II)Z

    goto :goto_2

    .line 489
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
