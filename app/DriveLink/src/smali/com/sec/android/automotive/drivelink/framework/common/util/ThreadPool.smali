.class public Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;
.super Ljava/lang/Object;
.source "ThreadPool.java"


# static fields
.field private static final MAX_COMMAND:I = 0x64


# instance fields
.field private bStopped:Z

.field private count:I

.field private head:I

.field private requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

.field private tail:I

.field private threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;


# direct methods
.method public constructor <init>(I)V
    .locals 6
    .param p1, "threads"    # I

    .prologue
    const/4 v5, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/16 v1, 0x64

    new-array v1, v1, [Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    .line 17
    iput v5, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->head:I

    .line 18
    iput v5, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->tail:I

    .line 19
    iput v5, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->count:I

    .line 21
    new-array v1, p1, [Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    .line 22
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 26
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->bStopped:Z

    .line 27
    return-void

    .line 23
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Worker-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;-><init>(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;)V

    aput-object v2, v1, v0

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized putRequest(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;)V
    .locals 2
    .param p1, "request"    # Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 85
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 72
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 74
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->bStopped:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    .line 70
    :cond_2
    :goto_1
    :try_start_2
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->count:I

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->tail:I

    aput-object p1, v0, v1

    .line 82
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->tail:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->tail:I

    .line 83
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->count:I

    .line 84
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 77
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public startWorkers()V
    .locals 2

    .prologue
    .line 30
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 33
    return-void

    .line 31
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->start()V

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized stopWorkers()V
    .locals 2

    .prologue
    .line 36
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->bStopped:Z

    .line 38
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 42
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    monitor-exit p0

    return-void

    .line 39
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->stopThread()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized takeRequest()Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;
    .locals 3

    .prologue
    .line 88
    monitor-enter p0

    :cond_0
    :goto_0
    :try_start_0
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->count:I

    if-lez v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->head:I

    aget-object v0, v1, v2

    .line 99
    .local v0, "request":Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->head:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    array-length v2, v2

    rem-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->head:I

    .line 100
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->count:I

    .line 101
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    .end local v0    # "request":Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;
    :goto_1
    monitor-exit p0

    return-object v0

    .line 90
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 92
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->bStopped:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    .line 93
    const/4 v0, 0x0

    goto :goto_1

    .line 88
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 95
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public waitWorkersStopped()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 55
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 58
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    .line 60
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    array-length v2, v2

    if-lt v1, v2, :cond_2

    .line 63
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    .line 64
    return-void

    .line 48
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 56
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->threadpool:[Lcom/sec/android/automotive/drivelink/framework/common/util/WorkerThread;

    aput-object v3, v2, v1

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 61
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/common/util/ThreadPool;->requestQueue:[Lcom/sec/android/automotive/drivelink/framework/common/pattern/Command;

    aput-object v3, v2, v1

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
