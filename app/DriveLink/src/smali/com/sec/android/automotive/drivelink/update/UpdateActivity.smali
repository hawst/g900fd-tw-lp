.class public Lcom/sec/android/automotive/drivelink/update/UpdateActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "UpdateActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "[UpdateActivity]"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f09018d

    if-ne v1, v2, :cond_1

    .line 55
    const-string/jumbo v1, "[UpdateActivity]"

    const-string/jumbo v2, "Update now Selected!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    new-instance v0, Landroid/content/Intent;

    .line 57
    const-class v1, Lcom/sec/android/automotive/drivelink/update/UpdateProgressActivity;

    .line 56
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateActivity;->startActivity(Landroid/content/Intent;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/update/UpdateActivity;->finish()V

    .line 65
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f090012

    if-ne v1, v2, :cond_0

    .line 61
    const-string/jumbo v1, "[UpdateActivity]"

    const-string/jumbo v2, "Update later Selected!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/update/UpdateActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f03003c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateActivity;->setContentView(I)V

    .line 43
    const v0, 0x7f09018d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v0, 0x7f090012

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/update/UpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 47
    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method
