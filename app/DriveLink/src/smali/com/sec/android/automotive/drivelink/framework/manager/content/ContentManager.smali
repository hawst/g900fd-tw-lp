.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "ContentManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager$DateComparator;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ContentManager"


# instance fields
.field private mCalllogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

.field private mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

.field private mContentHandlerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mDLCallMessageLogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;

.field private mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

.field private mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

.field private mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

.field private mScheduleDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mCalllogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    .line 51
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    .line 52
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mScheduleDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;

    .line 53
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    .line 54
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mDLCallMessageLogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->createContentHandlers()V

    .line 58
    return-void
.end method


# virtual methods
.method public addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "DLLog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mDLCallMessageLogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;->addDLCallMessageLogData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)V

    .line 378
    return-void
.end method

.method public addFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->addFavoriteContact(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Z

    move-result v0

    return v0
.end method

.method public changeMessageStatusToRead(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    .line 285
    check-cast p2, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .line 284
    .end local p2    # "message":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->changeMessageStatusToRead(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;)Z

    move-result v0

    return v0
.end method

.method public clearNewMissedCallFromCallLog(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mCalllogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;->clearNewMissedCallFromCallLog(Landroid/content/Context;)V

    .line 386
    return-void
.end method

.method public createContentHandlers()V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mCalllogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    .line 67
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mScheduleDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    .line 69
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    .line 71
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mDLCallMessageLogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public fillMusicListByMusicId(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p2, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->fillMusicListByMusicId(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 227
    return-void
.end method

.method public getAlbumList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getAlbumList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getArtistList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getArtistList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "calllog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCallLogList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v6, "resultlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mCalllogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

    .line 133
    const/4 v9, 0x0

    .line 132
    invoke-virtual {v8, p1, v9, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;->getAllCalllogList(Landroid/content/Context;II)Ljava/util/ArrayList;

    move-result-object v0

    .line 138
    .local v0, "_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 139
    .local v2, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v3, v8, :cond_0

    .line 160
    :goto_1
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 161
    .local v4, "it":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 169
    new-instance v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager$DateComparator;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager$DateComparator;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager$DateComparator;)V

    invoke-static {v6, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 171
    return-object v6

    .line 140
    .end local v4    # "it":Ljava/util/Iterator;
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 142
    .local v1, "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 143
    .local v7, "target":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    if-eqz v7, :cond_2

    .line 139
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v8

    if-lt v8, p2, :cond_1

    goto :goto_1

    .line 162
    .end local v1    # "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    .end local v7    # "target":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    .restart local v4    # "it":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 163
    .local v5, "pairs":Ljava/util/Map$Entry;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public getCalllogList(Landroid/content/Context;II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hour"    # I
    .param p3, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mCalllogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;->getAllCalllogList(Landroid/content/Context;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    return-object v0
.end method

.method public getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactImageFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getContactList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 98
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-object v0
.end method

.method public getDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mDLCallMessageLogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/dlcallmessagelog/DLCallMessageLogDataHandler;->getDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneNum"    # Ljava/lang/String;

    .prologue
    .line 175
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getContactDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 181
    .end local p2    # "phoneNum":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "phoneNum":Ljava/lang/String;
    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public getFavoriteContactList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getFavoriteContactList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getInboxList(Landroid/content/Context;IZI)Ljava/util/ArrayList;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .param p3, "isGetAllMessage"    # Z
    .param p4, "msgLimitCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZI)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 252
    .local v1, "startTime":J
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    invoke-virtual {v3, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getInboxList(Landroid/content/Context;IZI)Ljava/util/ArrayList;

    move-result-object v0

    .line 254
    .local v0, "inboxList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    const-string/jumbo v3, "ContentManager"

    .line 255
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " end getInboxList() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 255
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 254
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 259
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v3, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getInboxListWithContactData(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 261
    const-string/jumbo v3, "ContentManager"

    .line 262
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " end getInboxListWithContactData() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 262
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 261
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    return-object v0
.end method

.method public getIncommingMessageList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    .param p3, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    .line 271
    check-cast p2, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;

    .line 270
    .end local p2    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getIncommingMessageList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMegetUnreadMessageCountByInbox(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    .line 280
    check-cast p2, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;

    .line 279
    .end local p2    # "inbox":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getUnreadMessageCountByInbox(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;)I

    move-result v0

    return v0
.end method

.method public getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMusicCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getMusicFolderList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicFolderList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMusicListFromSearchList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicListFromSearchList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMusicListWithIdOnly(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicListWithIdOnly(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMusicSearchResult(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicSearchResult(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public getNewMissedCallCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mCalllogDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;->getNewMissedCallCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    long-to-int v1, p2

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getPhoneNumberList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumberList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getPhoneNumberList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getPlaylistList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getPlaylistList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRecommendedContactList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSchedule(Landroid/content/Context;I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventId"    # I

    .prologue
    .line 294
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mScheduleDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;

    .line 295
    int-to-long v2, p2

    .line 294
    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;->getSchedule(Landroid/content/Context;J)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    move-result-object v0

    .line 309
    .local v0, "resultSchedule":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    return-object v0
.end method

.method public getScheduleEventsWithLocation(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getScheduleEventsWithLocation(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mScheduleDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/ScheduleDataHandler;->getScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getSearchedContactList(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->getSearchedContactList(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 123
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-object v0
.end method

.method public getUnreadMessageCount(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getUnreadMessageCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getUnreadMessageCountBySync(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMessageDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getUreadMessageCountBySync(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 78
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 82
    const/4 v1, 0x1

    return v1

    .line 78
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;

    .line 79
    .local v0, "contentBaseHandler":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;->initialize(Landroid/content/Context;)Z

    goto :goto_0
.end method

.method public isLocationGroupShareChangedDestination(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    .line 367
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->isLocationGroupShareChangedDestination(Ljava/lang/String;)Z

    move-result v0

    .line 366
    return v0
.end method

.method public isLocationGroupSharedNotification(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->isLocationGroupSharedNotification(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isLocationRequestNotification(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->isLocationRequestNotification(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isLocationSharedNotification(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->isLocationSharedNotification(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isLocationStopGroupSharing(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)Z
    .locals 2
    .param p1, "msgBody"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .prologue
    .line 408
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    .line 409
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 408
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->isLocationStopGroupSharing(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeFavoriteContact(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->deleteFavoriteContact(ILcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 422
    return-void
.end method

.method public rescanContactDB(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->rescanContactDB(Landroid/content/Context;)V

    .line 128
    return-void
.end method

.method public rescanContactInfo(Landroid/content/Context;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->refreshContactInfo(Landroid/content/Context;J)V

    .line 103
    return-void
.end method

.method public rescanContactList(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p2, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContactDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;->rescanContactList(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 108
    return-void
.end method

.method public setUserProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V
    .locals 1
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mLocationDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->setUserProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V

    .line 400
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mContentHandlerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 91
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 92
    return-void

    .line 87
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;

    .line 88
    .local v0, "contentBaseHandler":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;->terminate(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->mMusicDataHandler:Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    move-result v0

    return v0
.end method
