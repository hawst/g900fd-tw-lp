.class Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;
.super Ljava/lang/Object;
.source "DrawerMenuLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    .line 361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 383
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$17(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 374
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mActionBar:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$18(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 375
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mMoreBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$19(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mMoreBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$19(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # invokes: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->runSelectedMenuItem()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$20(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    .line 379
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 369
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 365
    return-void
.end method
