.class public Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;
.super Landroid/widget/RelativeLayout;
.source "DrawerMenuLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;
    }
.end annotation


# static fields
.field public static final DRAWER_MENU_TYPE_INRIX:I = 0x12

.field public static final DRAWER_MENU_TYPE_INRIX_OBD:I = 0x14

.field public static final DRAWER_MENU_TYPE_NORMAL:I = 0x10

.field public static final DRAWER_MENU_TYPE_OBD:I = 0x11

.field public static final HOME_TO_OFFICE:Ljava/lang/String; = "home_to_office"

.field private static final ITEM_TYPE_COMMUTE_FORECASTS:I = 0x101

.field private static final ITEM_TYPE_CONSUMABLES:I = 0x224

.field private static final ITEM_TYPE_DIAGNOSTIC:I = 0x222

.field private static final ITEM_TYPE_DRIVE_BEHAVIOR:I = 0x221

.field private static final ITEM_TYPE_FIND_MY_CAR:I = 0x202

.field private static final ITEM_TYPE_HOME:I = 0x201

.field private static final ITEM_TYPE_HOME_TO_OFFICE:I = 0x212

.field private static final ITEM_TYPE_INRIX:I = 0x210

.field private static final ITEM_TYPE_MANAGEMENT:I = 0x102

.field private static final ITEM_TYPE_MENU_ITEM:I = 0x200

.field private static final ITEM_TYPE_OBD:I = 0x220

.field private static final ITEM_TYPE_OFFICE_TO_HOME:I = 0x211

.field private static final ITEM_TYPE_SUB_TITLE:I = 0x100

.field private static final ITEM_TYPE_UNKNOWN:I = -0x1

.field public static final MAX_RECOMMENDATIONS:I = 0x64

.field public static final OFFICE_TO_HOME:Ljava/lang/String; = "office_to_home"


# instance fields
.field private driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mActionBar:Landroid/view/View;

.field private mCloseAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mContext:Landroid/content/Context;

.field private mDrawerMenuBgFrame:Landroid/widget/FrameLayout;

.field private mDrawerMenuBtn:Landroid/widget/ImageView;

.field private mDrawerMenuLayout:Landroid/widget/LinearLayout;

.field private mDrawerMenuList:Landroid/widget/ListView;

.field private mIsDrawerMenuOpen:Z

.field private mIsDriving:Z

.field private mIsScrolling:Z

.field private mItem:Ljava/lang/String;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private final mLocationListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

.field private mMoreBtn:Landroid/widget/ImageButton;

.field private mNoHome:Z

.field private mNoInrix:Z

.field private mNoOBD:Z

.field private mNoOffice:Z

.field private mOwnerClassName:Ljava/lang/String;

.field private mPressedMotionEvent:Landroid/view/MotionEvent;

.field private mPressedView:Landroid/view/View;

.field private mSelectedMenuItemType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 854
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLocationListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 100
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    .line 101
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->init()V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 854
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLocationListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 106
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    .line 107
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->init()V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 854
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$1;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLocationListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 112
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    .line 113
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->init()V

    .line 114
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Z)V
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoHome:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Z)V
    .locals 0

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOffice:Z

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsScrolling:Z

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedMotionEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Z)V
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsScrolling:Z

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mActionBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mMoreBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V
    .locals 0

    .prologue
    .line 499
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->runSelectedMenuItem()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoHome:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOffice:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedView:Landroid/view/View;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mPressedMotionEvent:Landroid/view/MotionEvent;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDrawerMenuOpen:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;I)V
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mSelectedMenuItemType:I

    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 143
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 145
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030043

    .line 144
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 146
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 147
    const v2, 0x7f0901a3

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 146
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;

    .line 148
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLayout:Landroid/widget/RelativeLayout;

    .line 149
    const v2, 0x7f0901a4

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 148
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    .line 150
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDrawerMenuOpen:Z

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->refreshDrawableState()V

    .line 154
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$3;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$4;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 247
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$5;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 345
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$6;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$6;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 361
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$7;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mCloseAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    .line 386
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$8;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 414
    return-void
.end method

.method private runSelectedMenuItem()V
    .locals 3

    .prologue
    .line 500
    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mSelectedMenuItemType:I

    sparse-switch v1, :sswitch_data_0

    .line 567
    :cond_0
    :goto_0
    :sswitch_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mSelectedMenuItemType:I

    .line 568
    return-void

    .line 502
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mOwnerClassName:Ljava/lang/String;

    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 503
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 502
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 503
    if-nez v1, :cond_0

    .line 504
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 505
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 507
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 500
    nop

    :sswitch_data_0
    .sparse-switch
        0x201 -> :sswitch_1
        0x211 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public closeDrawerMenu()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x10e0000

    .line 474
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDrawerMenuOpen:Z

    .line 476
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;

    .line 477
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 478
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 480
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 479
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 482
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 483
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    .line 484
    invoke-virtual {v0}, Landroid/widget/ListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 485
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 487
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 486
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 489
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 490
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;

    .line 491
    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 492
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 493
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 496
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mCloseAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 497
    return-void
.end method

.method public getSelectedItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mItem:Ljava/lang/String;

    return-object v0
.end method

.method public isDrawerMenuOpen()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDrawerMenuOpen:Z

    return v0
.end method

.method public openDrawerMenu()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/high16 v3, 0x10e0000

    .line 441
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDrawerMenuOpen:Z

    if-eqz v0, :cond_1

    .line 442
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBgFrame:Landroid/widget/FrameLayout;

    .line 443
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 444
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 445
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 449
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    .line 450
    invoke-virtual {v0}, Landroid/widget/ListView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 451
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 452
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 455
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 456
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;

    .line 457
    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 458
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    const/high16 v2, -0x3f600000    # -5.0f

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->dipToPixels(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    .line 459
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 462
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->stopDmFlow()V

    .line 465
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDrawerMenuOpen:Z

    .line 466
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mActionBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mMoreBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mMoreBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public refreshDrawableState()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 876
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 877
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLocationListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 879
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 878
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 880
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    .line 881
    const/16 v5, 0x64

    .line 880
    invoke-interface {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestRecommendedLocationList(Landroid/content/Context;I)V

    .line 890
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 891
    const-string/jumbo v4, "PREF_SETTINGS_OBD_DEVICE_NAME"

    .line 892
    const-string/jumbo v5, ""

    .line 891
    invoke-virtual {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 892
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    move v1, v2

    .line 889
    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOBD:Z

    .line 893
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isUScsc()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoInrix:Z

    .line 895
    const/16 v0, 0x10

    .line 896
    .local v0, "currentType":I
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOBD:Z

    if-eqz v1, :cond_3

    .line 897
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoInrix:Z

    if-eqz v1, :cond_2

    .line 898
    const/16 v0, 0x10

    .line 910
    :goto_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    .line 911
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3, v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;Landroid/content/Context;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 912
    return-void

    .end local v0    # "currentType":I
    :cond_0
    move v1, v3

    .line 892
    goto :goto_0

    :cond_1
    move v2, v3

    .line 893
    goto :goto_1

    .line 900
    .restart local v0    # "currentType":I
    :cond_2
    const/16 v0, 0x12

    .line 902
    goto :goto_2

    .line 903
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoInrix:Z

    if-eqz v1, :cond_4

    .line 904
    const/16 v0, 0x11

    .line 905
    goto :goto_2

    .line 906
    :cond_4
    const/16 v0, 0x14

    goto :goto_2
.end method

.method public setActionBar(Ljava/lang/String;Landroid/view/View;)V
    .locals 2
    .param p1, "ownerClassName"    # Ljava/lang/String;
    .param p2, "ab"    # Landroid/view/View;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mOwnerClassName:Ljava/lang/String;

    .line 118
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mActionBar:Landroid/view/View;

    .line 119
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mActionBar:Landroid/view/View;

    const v1, 0x7f09032a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mMoreBtn:Landroid/widget/ImageButton;

    .line 120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mActionBar:Landroid/view/View;

    .line 121
    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 120
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuLayout:Landroid/widget/LinearLayout;

    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mActionBar:Landroid/view/View;

    .line 123
    const v1, 0x7f090016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 122
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuBtn:Landroid/widget/ImageView;

    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$2;-><init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method

.method public setDriving()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z

    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;

    .line 431
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->notifyDataSetChanged()V

    .line 432
    return-void
.end method

.method public setSelectedItem()V
    .locals 2

    .prologue
    .line 417
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mSelectedMenuItemType:I

    const/16 v1, 0x211

    if-ne v0, v1, :cond_1

    .line 418
    const-string/jumbo v0, "office_to_home"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mItem:Ljava/lang/String;

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mSelectedMenuItemType:I

    const/16 v1, 0x212

    if-ne v0, v1, :cond_0

    .line 420
    const-string/jumbo v0, "home_to_office"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mItem:Ljava/lang/String;

    goto :goto_0
.end method

.method public setStopped()V
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDriving:Z

    .line 436
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;

    .line 437
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->notifyDataSetChanged()V

    .line 438
    return-void
.end method

.method public updateMenuList()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 915
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 916
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mLocationListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 918
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 917
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 919
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    .line 920
    const/16 v5, 0x64

    .line 919
    invoke-interface {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestRecommendedLocationList(Landroid/content/Context;I)V

    .line 929
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 930
    const-string/jumbo v4, "PREF_SETTINGS_OBD_DEVICE_NAME"

    .line 931
    const-string/jumbo v5, ""

    .line 930
    invoke-virtual {v1, v4, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 931
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    move v1, v2

    .line 928
    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOBD:Z

    .line 932
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isUScsc()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoInrix:Z

    .line 934
    const/16 v0, 0x10

    .line 935
    .local v0, "currentType":I
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoOBD:Z

    if-eqz v1, :cond_3

    .line 936
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoInrix:Z

    if-eqz v1, :cond_2

    .line 937
    const/16 v0, 0x10

    .line 949
    :goto_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;

    .line 950
    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$DrawerMenuAdapter;->updateList(I)V

    .line 951
    return-void

    .end local v0    # "currentType":I
    :cond_0
    move v1, v3

    .line 931
    goto :goto_0

    :cond_1
    move v2, v3

    .line 932
    goto :goto_1

    .line 939
    .restart local v0    # "currentType":I
    :cond_2
    const/16 v0, 0x12

    .line 941
    goto :goto_2

    .line 942
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mNoInrix:Z

    if-eqz v1, :cond_4

    .line 943
    const/16 v0, 0x11

    .line 944
    goto :goto_2

    .line 945
    :cond_4
    const/16 v0, 0x14

    goto :goto_2
.end method
