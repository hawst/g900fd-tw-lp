.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 2138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 2142
    const-string/jumbo v1, "MusicPlayerActivity "

    const-string/jumbo v2, "search : clean"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2143
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 2144
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2145
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2146
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v2, "mSearchClearBtn.setOnClickListener setText"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2147
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2149
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 2150
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$19;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2152
    return-void
.end method
