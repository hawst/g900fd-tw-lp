.class Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$15;
.super Ljava/lang/Object;
.source "LocationShareActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    .line 568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 572
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$14(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 573
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 572
    sub-int/2addr v1, v2

    .line 573
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 572
    add-int v0, v1, v2

    .line 574
    .local v0, "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$15;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$14(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 575
    return-void
.end method
