.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLInbox.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDLContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
.end method

.method public abstract getDLMessagList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLastReceivedDate()J
.end method

.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getUnreadMsgCount()I
.end method
