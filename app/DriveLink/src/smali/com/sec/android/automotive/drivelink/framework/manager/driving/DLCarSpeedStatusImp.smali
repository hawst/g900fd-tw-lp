.class public Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;
.source "DLCarSpeedStatusImp.java"


# instance fields
.field private mActualSpeed:F

.field private mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

.field private mLocation:Landroid/location/Location;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;FLandroid/location/Location;)V
    .locals 0
    .param p1, "carSpeedStatus"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    .param p2, "actualSpeed"    # F
    .param p3, "location"    # Landroid/location/Location;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;->mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    .line 17
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;->mLocation:Landroid/location/Location;

    .line 18
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;->mActualSpeed:F

    .line 19
    return-void
.end method


# virtual methods
.method public getActualSpeed()F
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;->mActualSpeed:F

    return v0
.end method

.method public getCarSpeedStatus()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;->mCarSpeedStatus:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCarSpeedStatus$CarSpeedStatus;

    return-object v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DLCarSpeedStatusImp;->mLocation:Landroid/location/Location;

    return-object v0
.end method
