.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestGroupSharedUpdateInfo.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

.field private mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p4, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mContext:Landroid/content/Context;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mResult:Z

    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mContext:Landroid/content/Context;

    .line 22
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 23
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 24
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mContext:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 46
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestGroupSharedUpdateInfo(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    .line 28
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mResult:Z

    .line 30
    return-void
.end method

.method protected response()V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 36
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 37
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mResult:Z

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestGroupSharedUpdateInfo;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestGroupSharedUpdateInfo(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V

    .line 39
    :cond_0
    return-void
.end method
