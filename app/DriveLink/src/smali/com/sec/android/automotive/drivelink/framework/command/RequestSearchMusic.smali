.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestSearchMusic.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

.field private mSearchKeyword:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "searchKeyword"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mContext:Landroid/content/Context;

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mSearchKeyword:Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mContext:Landroid/content/Context;

    .line 21
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mSearchKeyword:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mContext:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mSearchKeyword:Ljava/lang/String;

    .line 46
    return-void
.end method

.method protected process()V
    .locals 3

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 27
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 28
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mContext:Landroid/content/Context;

    .line 29
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mSearchKeyword:Ljava/lang/String;

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicSearchResult(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .line 30
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v0

    .line 36
    .local v0, "musicListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    if-eqz v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestSearchMusic;->mMusicSearchResult:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestSearchMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V

    .line 39
    :cond_0
    return-void
.end method
