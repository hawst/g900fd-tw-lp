.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
.source "DLLocationEvent.java"


# instance fields
.field private mSchedule:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->mSchedule:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .line 12
    return-void
.end method


# virtual methods
.method public getEventTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->getLocationTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlaceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->getLocationDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSchedule()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->mSchedule:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    return-object v0
.end method

.method public setEventTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "eventTitle"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->setLocationTitle(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public setPlaceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "placeName"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->setLocationDescription(Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public setSchedule(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;)V
    .locals 0
    .param p1, "schedule"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->mSchedule:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .line 37
    return-void
.end method
