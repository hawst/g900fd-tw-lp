.class Lcom/sec/android/automotive/drivelink/home/HomeActivity$6;
.super Landroid/content/BroadcastReceiver;
.source "HomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setLanguageChangedReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 382
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 388
    .line 389
    :try_start_0
    sget-object v2, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

    .line 390
    const/4 v3, 0x0

    .line 387
    invoke-static {p1, v2, v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->initSettings(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 391
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->getSVoiceLocale()Ljava/util/Locale;

    move-result-object v1

    .line 392
    .local v1, "locale":Ljava/util/Locale;
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "updateLocale : setLanguageChangedReceiver : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 393
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 392
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->setVoiceLocale(Ljava/util/Locale;)V

    .line 396
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->initPhraseSpotter()V

    .line 398
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getCarModeState()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 399
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->access$1()Ljava/lang/String;

    move-result-object v2

    .line 400
    const-string/jumbo v3, "updateLocale : setLanguageChangedReceiver : App is foreground"

    .line 399
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    .line 402
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->updateLocale(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    .end local v1    # "locale":Ljava/util/Locale;
    :cond_0
    :goto_0
    return-void

    .line 405
    :catch_0
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
