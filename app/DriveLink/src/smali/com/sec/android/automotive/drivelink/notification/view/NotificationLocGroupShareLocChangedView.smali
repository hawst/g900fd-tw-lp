.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocGroupShareLocChangedView.java"


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mImageView:Landroid/widget/ImageView;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mImageView:Landroid/widget/ImageView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mViewLocal:Landroid/view/View;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mImageView:Landroid/widget/ImageView;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mViewLocal:Landroid/view/View;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mContext:Landroid/content/Context;

    .line 69
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->initIndicatorView(Landroid/content/Context;)V

    .line 70
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v5, 0x7f02023b

    .line 79
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mContext:Landroid/content/Context;

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 82
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 81
    check-cast v1, Landroid/view/LayoutInflater;

    .line 84
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_1

    .line 86
    const v3, 0x7f0300c8

    .line 85
    invoke-virtual {v1, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mViewLocal:Landroid/view/View;

    .line 87
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902e6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mImageView:Landroid/widget/ImageView;

    .line 90
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 91
    const v4, 0x7f020238

    .line 89
    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 93
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 92
    invoke-static {v3, v0, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 95
    .local v2, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 96
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 97
    const v4, 0x7f02023d

    .line 96
    invoke-static {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 100
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 101
    const v4, 0x7f02023c

    .line 100
    invoke-static {v3, v2, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 103
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 106
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "maskedBitmap":Landroid/graphics/Bitmap;
    :cond_1
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocGroupShareLocChangedView;)V

    .line 112
    const-wide/16 v5, 0x7d0

    .line 106
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 114
    return-void
.end method
