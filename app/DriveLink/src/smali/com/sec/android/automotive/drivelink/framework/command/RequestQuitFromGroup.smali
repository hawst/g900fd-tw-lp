.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestQuitFromGroup.java"


# instance fields
.field private mGroupId:I

.field private mResult:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;I)V
    .locals 1
    .param p1, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p2, "groupId"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->mGroupId:I

    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->mResult:Z

    .line 13
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->mGroupId:I

    .line 14
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 19
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->mGroupId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->quitFromGroup(I)Z

    move-result v0

    .line 18
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->mResult:Z

    .line 20
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 26
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 27
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestQuitFromGroup;->mResult:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestQuitFromGroup(Z)V

    .line 29
    :cond_0
    return-void
.end method
