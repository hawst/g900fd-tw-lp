.class public Lcom/sec/android/automotive/drivelink/util/DLConnection;
.super Ljava/lang/Object;
.source "DLConnection.java"


# static fields
.field private static mInstance:Lcom/sec/android/automotive/drivelink/util/DLConnection;


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mInstance:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 16
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 18
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 17
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 19
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/util/DLConnection;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mInstance:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/sec/android/automotive/drivelink/util/DLConnection;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/util/DLConnection;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mInstance:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    .line 24
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mInstance:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    return-object v0
.end method

.method public static hasInternet()Z
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->getInstance()Lcom/sec/android/automotive/drivelink/util/DLConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->isNetworkUsable()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public isNetworkUsable()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v2, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 39
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/util/DLConnection;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 40
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 41
    .local v0, "activeNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public registerConnectionChangedReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 29
    new-instance v0, Landroid/content/IntentFilter;

    .line 30
    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    .line 29
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 31
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 32
    return-void
.end method
