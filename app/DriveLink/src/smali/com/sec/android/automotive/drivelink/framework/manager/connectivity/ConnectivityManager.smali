.class public Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "ConnectivityManager.java"


# static fields
.field private static final REQUEST_DISCOVERABLE_BT:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private BT_FORCE_RECOVERY:Ljava/lang/String;

.field private isA2DPServiceAvailable:Z

.field private isHFPServiceAvailable:Z

.field public mBTProfileA2DP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field public mBTProfileHFP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothAddress:Ljava/lang/String;

.field private mBluetoothAddressForPairing:Ljava/lang/String;

.field private mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

.field private mConnectionAttemptsA2DP:I

.field private mConnectionAttemptsHFP:I

.field private mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mCurrentBluetoothState:I

.field private mDLBTHandler:Landroid/os/Handler;

.field public mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

.field private mErrorNumber:I

.field private mIsBluetoothCommunicating:Z

.field private mIsBluetoothConnected:Z

.field private mIsConnectedA2DP:Z

.field private mIsConnectedHFP:Z

.field private mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mOwnConnectionPattern:Z

.field public mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

.field public mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

.field final obj:Ljava/lang/Object;

.field final objPollDisconnect:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 29
    const-string/jumbo v0, "android.bluetooth.device.action.BT_FORCE_RECOVERY"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->BT_FORCE_RECOVERY:Ljava/lang/String;

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mOwnConnectionPattern:Z

    .line 50
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    .line 51
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    .line 1496
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTHandler:Landroid/os/Handler;

    .line 65
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 66
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;-><init>(Landroid/os/Handler;)V

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 67
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mCurrentBluetoothState:I

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V
    .locals 0

    .prologue
    .line 1429
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->recoverBluetoothOperation(I)V

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mErrorNumber:I

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;I)V
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mCurrentBluetoothState:I

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAddressForPairing:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mOwnConnectionPattern:Z

    return v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;I)Z
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->checkContinueConnection(Landroid/bluetooth/BluetoothDevice;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V
    .locals 0

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mOwnConnectionPattern:Z

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1301
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->saveBTConnection(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1355
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothDeviceByName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    return v0
.end method

.method private checkBluetoothProfilesSupported(Landroid/bluetooth/BluetoothDevice;)V
    .locals 7
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1322
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    .line 1323
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    .line 1325
    if-nez p1, :cond_1

    .line 1353
    :cond_0
    return-void

    .line 1329
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v0

    .line 1331
    .local v0, "listUUIDs":[Landroid/os/ParcelUuid;
    if-eqz v0, :cond_0

    .line 1332
    array-length v3, v0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1333
    .local v1, "uuid":Landroid/os/ParcelUuid;
    invoke-virtual {v1}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    .line 1334
    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1336
    sget-object v5, Landroid/bluetooth/BluetoothUuid;->AdvAudioDist:Landroid/os/ParcelUuid;

    invoke-virtual {v5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1335
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 1336
    if-eqz v4, :cond_3

    .line 1337
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    .line 1332
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1339
    :cond_3
    invoke-virtual {v1}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    .line 1340
    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1342
    sget-object v5, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    invoke-virtual {v5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1341
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 1342
    if-nez v4, :cond_4

    .line 1343
    invoke-virtual {v1}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    .line 1344
    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1346
    sget-object v5, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    invoke-virtual {v5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v5

    .line 1347
    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1345
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 1347
    if-eqz v4, :cond_2

    .line 1348
    :cond_4
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    goto :goto_1
.end method

.method private checkContinueConnection(Landroid/bluetooth/BluetoothDevice;I)Z
    .locals 6
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "connectionType"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 374
    const/4 v0, 0x0

    .line 375
    .local v0, "connectionStateA2DP":I
    const/4 v1, 0x0

    .line 376
    .local v1, "connectionStateHFP":I
    const/4 v2, 0x0

    .line 378
    .local v2, "result":Z
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 379
    const/4 v3, 0x0

    .line 415
    :goto_0
    return v3

    .line 382
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 411
    const/4 v2, 0x0

    :cond_1
    :goto_1
    move v3, v2

    .line 415
    goto :goto_0

    .line 384
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v3, :cond_1

    .line 385
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 386
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    .line 387
    if-ne v0, v4, :cond_2

    .line 388
    const/4 v2, 0x0

    .line 389
    goto :goto_1

    :cond_2
    if-ne v0, v5, :cond_3

    .line 390
    const/4 v2, 0x0

    .line 391
    goto :goto_1

    .line 392
    :cond_3
    const/4 v2, 0x1

    .line 396
    goto :goto_1

    .line 398
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_1

    .line 399
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 400
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    .line 401
    if-ne v1, v4, :cond_4

    .line 402
    const/4 v2, 0x0

    .line 403
    goto :goto_1

    :cond_4
    if-ne v1, v5, :cond_5

    .line 404
    const/4 v2, 0x0

    .line 405
    goto :goto_1

    .line 406
    :cond_5
    const/4 v2, 0x1

    .line 409
    goto :goto_1

    .line 382
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getBluetoothAdapter()Z
    .locals 1

    .prologue
    .line 178
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 180
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 181
    const/4 v0, 0x1

    .line 183
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBluetoothDeviceByName(Ljava/lang/String;)V
    .locals 4
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 1356
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1357
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->searchPairedDevices()Ljava/util/Set;

    move-result-object v1

    .line 1359
    .local v1, "listBluetoothDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 1360
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1376
    .end local v1    # "listBluetoothDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_1
    :goto_0
    return-void

    .line 1360
    .restart local v1    # "listBluetoothDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1362
    .local v0, "btDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1364
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1365
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->checkBluetoothProfilesSupported(Landroid/bluetooth/BluetoothDevice;)V

    .line 1366
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    if-nez v3, :cond_3

    .line 1367
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    if-eqz v3, :cond_0

    .line 1368
    :cond_3
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0
.end method

.method private initializeProfileProxyA2DP()V
    .locals 4

    .prologue
    .line 866
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[BT CHECK]initializeProfileProxyA2DP start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$4;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$4;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileA2DP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 908
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    .line 909
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 910
    const-string/jumbo v1, "[BT CHECK]initializeProfileProxyA2DP mProfileProxyA2DP is null."

    .line 909
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileA2DP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 912
    const/4 v3, 0x2

    .line 911
    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 914
    :cond_0
    return-void
.end method

.method private initializeProfileProxyHFP()V
    .locals 4

    .prologue
    .line 917
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[BT CHECK]initializeProfileProxyHFP start."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$5;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileHFP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 958
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-nez v0, :cond_0

    .line 959
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 960
    const-string/jumbo v1, "[BT CHECK]initializeProfileProxyHFP mProfileProxyHFP is null."

    .line 959
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileHFP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 962
    const/4 v3, 0x1

    .line 961
    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 964
    :cond_0
    return-void
.end method

.method private isBluetoothDeviceConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 7
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1409
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1424
    :cond_0
    :goto_0
    return v2

    .line 1412
    :cond_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1413
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v0

    .line 1414
    .local v0, "connectionStateA2DP":I
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1415
    invoke-virtual {v4, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v1

    .line 1417
    .local v1, "connectionStateHFP":I
    if-eqz v0, :cond_0

    .line 1418
    if-eqz v1, :cond_0

    .line 1420
    if-eq v0, v6, :cond_0

    .line 1421
    if-eq v1, v6, :cond_0

    move v2, v3

    .line 1424
    goto :goto_0
.end method

.method private isBluetoothDevicePaired(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1384
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1385
    const/4 v2, 0x0

    .line 1398
    :cond_0
    return v2

    .line 1387
    :cond_1
    const/4 v2, 0x0

    .line 1389
    .local v2, "result":Z
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->searchPairedDevices()Ljava/util/Set;

    move-result-object v1

    .line 1390
    .local v1, "listBluetoothDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v1, :cond_0

    .line 1391
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1392
    .local v0, "btDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v4

    const/16 v5, 0xc

    if-ne v4, v5, :cond_2

    .line 1393
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private pairBTDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 312
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v0

    return v0
.end method

.method private recoverBluetoothOperation(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 1430
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1431
    packed-switch p1, :pswitch_data_0

    .line 1494
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1435
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->startBluetoothAdapter(Landroid/content/Context;)Z

    goto :goto_0

    .line 1438
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1441
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->makeBluetoothDeviceDiscoverable(Landroid/content/Context;)Z

    .line 1442
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->searchDiscoverableDevices()Z

    goto :goto_0

    .line 1445
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1448
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->makeBluetoothDeviceDiscoverable(Landroid/content/Context;)Z

    .line 1449
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->searchDiscoverableDevices()Z

    goto :goto_0

    .line 1452
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1455
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->makeBluetoothDeviceDiscoverable(Landroid/content/Context;)Z

    .line 1456
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->searchDiscoverableDevices()Z

    goto :goto_0

    .line 1459
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1462
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    .line 1463
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isBluetoothDevicePaired(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1464
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    if-nez v1, :cond_0

    .line 1466
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    .line 1467
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    .line 1466
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->connectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1468
    :catch_0
    move-exception v0

    .line 1469
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1477
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1480
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    .line 1481
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    if-eqz v1, :cond_0

    .line 1482
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    .line 1483
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    .line 1482
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->disconnectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    goto :goto_0

    .line 1431
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method private saveBTConnection(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 1302
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    .line 1303
    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;Ljava/lang/String;I)V

    .line 1302
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .line 1304
    return-void
.end method

.method private unpairBTDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 369
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->removeBond()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public connectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v11, 0x4

    .line 420
    move-object v0, p2

    .line 422
    .local v0, "btDevice":Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_1

    .line 842
    :cond_0
    :goto_0
    return v5

    .line 425
    :cond_1
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "connectBluetoothDevice "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 431
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    if-eqz v7, :cond_2

    .line 432
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    if-eqz v7, :cond_2

    .line 435
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    .line 434
    invoke-virtual {p0, p1, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->disconnectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v7

    .line 435
    if-nez v7, :cond_2

    .line 436
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "could not disconnect current device"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_2
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    if-eq v7, v11, :cond_3

    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    if-ne v7, v11, :cond_4

    .line 441
    :cond_3
    iput v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    .line 442
    iput v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    .line 445
    :cond_4
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    .line 446
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->saveBTConnection(Ljava/lang/String;)V

    .line 448
    new-instance v7, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;

    invoke-direct {v7, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileA2DP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 600
    new-instance v7, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;

    invoke-direct {v7, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V

    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileHFP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 768
    const/4 v3, 0x0

    .line 769
    .local v3, "hasA2DP":Z
    const/4 v4, 0x0

    .line 770
    .local v4, "hasHFP":Z
    const/4 v1, 0x0

    .line 772
    .local v1, "connection_counter":I
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->checkBluetoothProfilesSupported(Landroid/bluetooth/BluetoothDevice;)V

    .line 774
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    if-eqz v7, :cond_5

    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    if-nez v7, :cond_5

    .line 775
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    .line 776
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileA2DP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 775
    invoke-virtual {v7, v8, v9, v12}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v3

    .line 777
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    .line 780
    :cond_5
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    monitor-enter v7

    .line 781
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->notifyAll()V

    .line 780
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 784
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    if-eqz v7, :cond_6

    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    if-nez v7, :cond_6

    .line 785
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileHFP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-virtual {v7, v8, v9, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v4

    .line 787
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    .line 790
    :cond_6
    if-nez v3, :cond_7

    if-eqz v4, :cond_11

    .line 791
    :cond_7
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    .line 793
    :cond_8
    :goto_1
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    if-eq v7, v11, :cond_9

    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    if-ne v7, v11, :cond_b

    .line 832
    :cond_9
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    if-nez v7, :cond_a

    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    if-eqz v7, :cond_10

    :cond_a
    move v5, v6

    .line 833
    goto/16 :goto_0

    .line 780
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 795
    :cond_b
    :try_start_2
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    monitor-enter v8
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 796
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    const-wide/16 v9, 0x2710

    invoke-virtual {v7, v9, v10}, Ljava/lang/Object;->wait(J)V

    .line 795
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 801
    :goto_2
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    if-eqz v7, :cond_c

    .line 802
    iput v11, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    .line 805
    :cond_c
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    if-eqz v7, :cond_d

    .line 806
    iput v11, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    .line 809
    :cond_d
    add-int/lit8 v1, v1, 0x1

    .line 810
    if-ne v1, v11, :cond_e

    .line 811
    iput v11, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    .line 812
    iput v11, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    .line 815
    :cond_e
    if-eqz v3, :cond_f

    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    if-nez v7, :cond_f

    .line 816
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    if-eqz v7, :cond_f

    .line 817
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    if-ge v7, v11, :cond_f

    .line 818
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileA2DP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-virtual {v7, v8, v9, v12}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 820
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsA2DP:I

    .line 823
    :cond_f
    if-eqz v4, :cond_8

    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    if-nez v7, :cond_8

    .line 824
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    if-eqz v7, :cond_8

    .line 825
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    if-ge v7, v11, :cond_8

    .line 826
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBTProfileHFP:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-virtual {v7, v8, v9, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 828
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectionAttemptsHFP:I

    goto :goto_1

    .line 795
    :catchall_1
    move-exception v7

    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v7
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 798
    :catch_0
    move-exception v2

    .line 799
    .local v2, "e":Ljava/lang/InterruptedException;
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "connectBluetoothDevice"

    invoke-static {v7, v8, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 835
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_10
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    .line 836
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    goto/16 :goto_0

    .line 840
    :cond_11
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    .line 841
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    goto/16 :goto_0
.end method

.method public disconnectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v10, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1036
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "disconnectBluetoothDevice "

    invoke-direct {v9, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1037
    if-nez p2, :cond_0

    const-string/jumbo v5, ""

    :goto_0
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1036
    invoke-static {v8, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1039
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "!mBluetoothAdapter.isEnabled()"

    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 1223
    :goto_1
    return v5

    .line 1037
    :cond_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 1043
    :cond_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 1045
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    monitor-enter v8

    .line 1046
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 1045
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1049
    if-nez p2, :cond_2

    .line 1050
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "bluetoothDevice == null"

    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 1051
    goto :goto_1

    .line 1045
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 1053
    :cond_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    if-eqz v5, :cond_8

    .line 1054
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 1055
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    .line 1054
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1055
    if-nez v5, :cond_3

    .line 1056
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 1057
    const-string/jumbo v8, "not same as mBluetoothDeviceConnected"

    .line 1056
    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    :cond_3
    :goto_2
    const/4 v2, 0x0

    .line 1068
    .local v2, "isBluetoothA2DPDisconnected":Z
    const/4 v3, 0x0

    .line 1070
    .local v3, "isBluetoothHFPDisconnected":Z
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-nez v5, :cond_4

    .line 1071
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK]A2DP Proxy is null so call init method."

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->initializeProfileProxyA2DP()V

    .line 1080
    const/4 v0, 0x0

    .line 1081
    .local v0, "count":I
    :goto_3
    if-lt v0, v10, :cond_9

    .line 1099
    .end local v0    # "count":I
    :cond_4
    :goto_4
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v5, :cond_b

    .line 1100
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK]A2DP Proxy is not null."

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 1102
    invoke-virtual {v5, p2}, Landroid/bluetooth/BluetoothA2dp;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    .line 1103
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    .line 1105
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    if-nez v5, :cond_5

    .line 1106
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    .line 1107
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    .line 1115
    :cond_5
    :goto_5
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-nez v5, :cond_6

    .line 1116
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK]HFP Proxy is null so call init method."

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->initializeProfileProxyHFP()V

    .line 1124
    const/4 v0, 0x0

    .line 1125
    .restart local v0    # "count":I
    :goto_6
    if-lt v0, v10, :cond_c

    .line 1143
    .end local v0    # "count":I
    :cond_6
    :goto_7
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v5, :cond_e

    .line 1144
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK]HFP Proxy is not null."

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 1146
    invoke-virtual {v5, p2}, Landroid/bluetooth/BluetoothHeadset;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    .line 1147
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    .line 1149
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    .line 1150
    iput-boolean v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    .line 1157
    :goto_8
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$6;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1199
    .local v4, "mThread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 1201
    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    monitor-enter v8

    .line 1203
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    const-wide/16 v9, 0x1388

    invoke-virtual {v5, v9, v10}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1201
    :goto_9
    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1210
    if-nez v2, :cond_7

    .line 1211
    if-eqz v3, :cond_f

    :cond_7
    move v5, v7

    .line 1212
    goto/16 :goto_1

    .line 1061
    .end local v2    # "isBluetoothA2DPDisconnected":Z
    .end local v3    # "isBluetoothHFPDisconnected":Z
    .end local v4    # "mThread":Ljava/lang/Thread;
    :cond_8
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 1062
    const-string/jumbo v8, "mBluetoothDeviceConnected is null"

    .line 1061
    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1082
    .restart local v0    # "count":I
    .restart local v2    # "isBluetoothA2DPDisconnected":Z
    .restart local v3    # "isBluetoothHFPDisconnected":Z
    :cond_9
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v5, :cond_a

    .line 1083
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK] mProfileProxyA2DP is connected"

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 1087
    :cond_a
    :try_start_4
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 1088
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[BT CHECK] mProfileProxyA2DP is not connected. wait:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1089
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1088
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1087
    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    const-wide/16 v8, 0x64

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 1095
    :goto_a
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    .line 1091
    :catch_0
    move-exception v1

    .line 1093
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_a

    .line 1112
    .end local v0    # "count":I
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_b
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK]A2DP Proxy is null !! do nothing."

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 1126
    .restart local v0    # "count":I
    :cond_c
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v5, :cond_d

    .line 1127
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK] mProfileProxyHFP is connected"

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 1131
    :cond_d
    :try_start_5
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 1132
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "[BT CHECK] mProfileProxyHFP is not connected. wait:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1132
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1131
    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1134
    const-wide/16 v8, 0x64

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1

    .line 1139
    :goto_b
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_6

    .line 1135
    :catch_1
    move-exception v1

    .line 1137
    .restart local v1    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_b

    .line 1154
    .end local v0    # "count":I
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_e
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "[BT CHECK]HFP Proxy is null."

    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 1204
    .restart local v4    # "mThread":Ljava/lang/Thread;
    :catch_2
    move-exception v1

    .line 1205
    .restart local v1    # "e":Ljava/lang/InterruptedException;
    :try_start_6
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "InterruptedException"

    invoke-static {v5, v9, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1206
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_9

    .line 1201
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v5

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v5

    .line 1214
    :cond_f
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    if-eqz v5, :cond_11

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    if-eqz v5, :cond_11

    .line 1215
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 1216
    const-string/jumbo v8, "isA2DPServiceAvailable == true && mIsConnectedA2DP == true"

    .line 1215
    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedA2DP:Z

    :cond_10
    :goto_c
    move v5, v6

    .line 1223
    goto/16 :goto_1

    .line 1218
    :cond_11
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    if-eqz v5, :cond_10

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    if-eqz v5, :cond_10

    .line 1219
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;

    .line 1220
    const-string/jumbo v8, "isHFPServiceAvailable == true && mIsConnectedHFP == true"

    .line 1219
    invoke-static {v5, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsConnectedHFP:Z

    goto :goto_c
.end method

.method public getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;
    .locals 1

    .prologue
    .line 1228
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothCommunicatingState(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1282
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    if-eqz v0, :cond_0

    .line 1283
    const/4 v0, 0x1

    .line 1285
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBluetoothConnectedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1257
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "btDeviceMacAddress"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1262
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    if-eqz v1, :cond_0

    .line 1264
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_1

    .line 1265
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAddress:Ljava/lang/String;

    .line 1270
    :goto_0
    if-eqz p2, :cond_0

    const-string/jumbo v1, ""

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1271
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAddress:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1272
    const/4 v0, 0x1

    .line 1277
    :cond_0
    return v0

    .line 1267
    :cond_1
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAddress:Ljava/lang/String;

    goto :goto_0
.end method

.method public getBluetoothConnectionState(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1248
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    if-eqz v0, :cond_0

    .line 1249
    const/4 v0, 0x1

    .line 1251
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBluetoothDeviceConnectedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1404
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isBluetoothDeviceConnected(Landroid/bluetooth/BluetoothDevice;)Z

    .line 1405
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothConnectedState(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothDeviceDetailsByName(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 4
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1307
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    move-object v1, v2

    .line 1317
    :cond_0
    :goto_0
    return-object v1

    .line 1311
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAddressForPairing:Ljava/lang/String;

    .line 1312
    .local v0, "address":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1313
    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 1314
    .local v1, "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, v2

    .line 1317
    goto :goto_0
.end method

.method public getBluetoothDevicePairedState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1380
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isBluetoothDevicePaired(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public getBluetoothDevicesDiscovered(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 249
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-object v0

    .line 252
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getBluetoothProfileConnectionState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 995
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1029
    :cond_0
    :goto_0
    return v2

    .line 999
    :cond_1
    const/4 v0, 0x0

    .line 1000
    .local v0, "isA2DPConnected":I
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->checkBluetoothProfilesSupported(Landroid/bluetooth/BluetoothDevice;)V

    .line 1002
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    if-eqz v3, :cond_5

    .line 1003
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 1004
    invoke-virtual {v3, p2}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    .line 1013
    :cond_2
    :goto_1
    const/4 v1, 0x0

    .line 1014
    .local v1, "isHFPConnected":I
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    if-eqz v3, :cond_6

    .line 1015
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 1016
    invoke-virtual {v3, p2}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    .line 1025
    :cond_3
    :goto_2
    if-eq v0, v4, :cond_4

    .line 1026
    if-ne v1, v4, :cond_0

    .line 1027
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 1006
    .end local v1    # "isHFPConnected":I
    :cond_5
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isA2DPServiceAvailable:Z

    if-eqz v3, :cond_2

    .line 1007
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->initializeProfileProxyA2DP()V

    .line 1008
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 1009
    invoke-virtual {v3, p2}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    .line 1008
    goto :goto_1

    .line 1018
    .restart local v1    # "isHFPConnected":I
    :cond_6
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->isHFPServiceAvailable:Z

    if-eqz v3, :cond_3

    .line 1019
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->initializeProfileProxyHFP()V

    .line 1020
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 1021
    invoke-virtual {v3, p2}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    .line 1020
    goto :goto_2
.end method

.method public getBluetoothProfileConnectionState(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "btDeviceAddress"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 968
    const/4 v0, 0x0

    .line 970
    .local v0, "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 989
    :cond_0
    :goto_0
    return v3

    .line 974
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->searchPairedDevices()Ljava/util/Set;

    move-result-object v2

    .line 975
    .local v2, "btDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v2, :cond_0

    .line 979
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 985
    if-eqz v0, :cond_0

    .line 989
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothProfileConnectionState(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    goto :goto_0

    .line 979
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 980
    .local v1, "btDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 981
    move-object v0, v1

    goto :goto_1
.end method

.method public getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 848
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    if-nez v3, :cond_0

    move-object v0, v2

    .line 862
    :goto_0
    return-object v0

    .line 852
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->searchPairedDevices()Ljava/util/Set;

    move-result-object v1

    .line 853
    .local v1, "btDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v1, :cond_2

    .line 854
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    move-object v0, v2

    .line 862
    goto :goto_0

    .line 854
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 855
    .local v0, "btDevice":Landroid/bluetooth/BluetoothDevice;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothDeviceConnected:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 856
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 855
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 856
    if-eqz v4, :cond_1

    goto :goto_0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1232
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothAdapter()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1238
    :cond_0
    :goto_0
    return-object v0

    .line 1235
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    if-eqz v1, :cond_0

    .line 1238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPassKey()I
    .locals 1

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->getPassKey()I

    move-result v0

    return v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 70
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 72
    if-eqz p1, :cond_2

    .line 73
    new-instance v1, Landroid/content/IntentFilter;

    .line 74
    const-string/jumbo v6, "android.bluetooth.adapter.action.STATE_CHANGED"

    .line 73
    invoke-direct {v1, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 75
    .local v1, "filterActionStateChanged":Landroid/content/IntentFilter;
    new-instance v0, Landroid/content/IntentFilter;

    .line 76
    const-string/jumbo v6, "android.bluetooth.device.action.PAIRING_REQUEST"

    .line 75
    invoke-direct {v0, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 77
    .local v0, "filterActionPairingRequest":Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    invoke-virtual {p1, v6, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 79
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    invoke-virtual {p1, v6, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 81
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 82
    new-instance v7, Landroid/content/IntentFilter;

    const-string/jumbo v8, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 83
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 84
    new-instance v7, Landroid/content/IntentFilter;

    const-string/jumbo v8, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 85
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 86
    new-instance v7, Landroid/content/IntentFilter;

    const-string/jumbo v8, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 88
    new-instance v7, Landroid/content/IntentFilter;

    const-string/jumbo v8, "android.bluetooth.device.action.FOUND"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 89
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 90
    new-instance v7, Landroid/content/IntentFilter;

    const-string/jumbo v8, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 91
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 92
    new-instance v7, Landroid/content/IntentFilter;

    .line 93
    const-string/jumbo v8, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    .line 92
    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 94
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 95
    new-instance v7, Landroid/content/IntentFilter;

    .line 96
    const-string/jumbo v8, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    .line 95
    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 97
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 98
    new-instance v7, Landroid/content/IntentFilter;

    .line 99
    const-string/jumbo v8, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    .line 98
    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 100
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    .line 101
    new-instance v7, Landroid/content/IntentFilter;

    iget-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->BT_FORCE_RECOVERY:Ljava/lang/String;

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 103
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    .line 104
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;->setContext(Landroid/content/Context;)V

    .line 106
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    .line 107
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 109
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 110
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 111
    invoke-virtual {v6, v9}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v2

    .line 113
    .local v2, "isA2DPConnected":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v6, :cond_4

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 114
    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v3

    .line 117
    .local v3, "isHFPConnected":I
    :goto_1
    if-eq v2, v9, :cond_0

    .line 118
    if-ne v3, v9, :cond_1

    .line 119
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z

    .line 120
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothCommunicating:Z

    .line 122
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->initializeProfileProxyA2DP()V

    .line 123
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->initializeProfileProxyHFP()V

    :cond_1
    move v4, v5

    .line 136
    .end local v0    # "filterActionPairingRequest":Landroid/content/IntentFilter;
    .end local v1    # "filterActionStateChanged":Landroid/content/IntentFilter;
    .end local v2    # "isA2DPConnected":I
    .end local v3    # "isHFPConnected":I
    :cond_2
    return v4

    .restart local v0    # "filterActionPairingRequest":Landroid/content/IntentFilter;
    .restart local v1    # "filterActionStateChanged":Landroid/content/IntentFilter;
    :cond_3
    move v2, v4

    .line 112
    goto :goto_0

    .restart local v2    # "isA2DPConnected":I
    :cond_4
    move v3, v4

    .line 115
    goto :goto_1
.end method

.method public makeBluetoothDeviceDiscoverable(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 204
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 219
    :goto_0
    return v1

    .line 207
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v3

    const/16 v4, 0x17

    if-ne v3, v4, :cond_1

    move v1, v2

    .line 209
    goto :goto_0

    .line 211
    :cond_1
    new-instance v0, Landroid/content/Intent;

    .line 212
    const-string/jumbo v3, "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"

    .line 211
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 214
    .local v0, "enableIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 216
    const-string/jumbo v3, "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"

    .line 215
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 217
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v1, v2

    .line 219
    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 226
    .line 235
    return-void
.end method

.method public pairBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x8

    .line 291
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 307
    :goto_0
    return v1

    .line 296
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v3

    const/16 v4, 0xc

    .line 295
    if-eq v3, v4, :cond_1

    .line 297
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->pairBTDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v2

    .line 298
    .local v0, "willPairBluetoothDevice":Z
    :goto_1
    if-eqz v0, :cond_2

    move v1, v2

    .line 299
    goto :goto_0

    .line 295
    .end local v0    # "willPairBluetoothDevice":Z
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 301
    .restart local v0    # "willPairBluetoothDevice":Z
    :cond_2
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mErrorNumber:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 303
    .end local v0    # "willPairBluetoothDevice":Z
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public pairBluetoothDeviceByNSSP(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;[B)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "PIN"    # [B

    .prologue
    const/4 v0, 0x0

    .line 336
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 339
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 340
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->stopDiscoveringDevices(Landroid/content/Context;)Z

    .line 342
    :cond_2
    invoke-virtual {p2, p3}, Landroid/bluetooth/BluetoothDevice;->setPin([B)Z

    .line 343
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->pairBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    if-nez v1, :cond_0

    .line 344
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public pairBluetoothDeviceBySSP(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Z)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "confirm"    # Z

    .prologue
    const/4 v0, 0x0

    .line 317
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 328
    :cond_0
    :goto_0
    return v0

    .line 320
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 321
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->stopDiscoveringDevices(Landroid/content/Context;)Z

    .line 324
    :cond_2
    invoke-virtual {p2, p3}, Landroid/bluetooth/BluetoothDevice;->setPairingConfirmation(Z)Z

    .line 325
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->pairBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    if-nez v1, :cond_0

    .line 326
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public searchDiscoverableDevices()Z
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    const/4 v0, 0x0

    .line 243
    :goto_0
    return v0

    .line 241
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mListOfBluetoothDevicesDiscovered:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    move-result v0

    goto :goto_0
.end method

.method public searchPairedDevices()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 277
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    .line 279
    .local v0, "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_0

    .line 280
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 285
    .end local v0    # "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendBTPairingRequestNotification(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;)V
    .locals 4
    .param p1, "btInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .prologue
    .line 1290
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .line 1291
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->getBTPairingType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    .line 1292
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;->getPassKey()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo$BT_PAIRING_TYPE;Ljava/lang/String;I)V

    .line 1290
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mDLBTPairingInfoImpl:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/DLBTPairingInfoImpl;

    .line 1294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1295
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1296
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyBTConnectionRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLBTParingInfo;)V

    .line 1299
    :cond_0
    return-void
.end method

.method public startBluetoothAdapter(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothAdapter()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    const/4 v0, 0x0

    .line 191
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    goto :goto_0
.end method

.method public stopBluetoothAdapter(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->getBluetoothAdapter()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    const/4 v0, 0x0

    .line 199
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v0

    goto :goto_0
.end method

.method public stopDiscoveringDevices(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    const/4 v0, 0x0

    .line 268
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    move-result v0

    goto :goto_0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    monitor-enter v2

    .line 148
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 147
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    monitor-enter v2

    .line 152
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->objPollDisconnect:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 151
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 155
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 156
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v1, :cond_0

    .line 157
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v2, 0x2

    .line 158
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 157
    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v1, :cond_1

    .line 161
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v2, 0x1

    .line 162
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyHFP:Landroid/bluetooth/BluetoothHeadset;

    .line 161
    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 169
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mConnectivityManagerBroadcastReceiver:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManagerBroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    .line 174
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 175
    return-void

    .line 147
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 151
    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public unpairBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    :try_start_0
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->unpairBTDevice(Landroid/bluetooth/BluetoothDevice;)Z

    .line 359
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mIsBluetoothConnected:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :goto_0
    return v0

    .line 360
    :catch_0
    move-exception v0

    .line 364
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
