.class public Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;
.super Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;
.source "OnDriveLinkSamsungAccountListenerImp.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;


# instance fields
.field private mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    return-object v0
.end method


# virtual methods
.method public onReceiveSamsungAccountAccessToken(ZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "isSuccess"    # Z
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 93
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v2, :cond_0

    .line 110
    :goto_0
    return-void

    .line 97
    :cond_0
    move v0, p1

    .line 98
    .local v0, "_isSuccess":Z
    move-object v1, p2

    .line 100
    .local v1, "_resultData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$5;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onReceiveSamsungAccountAuthCode(ZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "isSuccess"    # Z
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 159
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v2, :cond_0

    .line 176
    :goto_0
    return-void

    .line 163
    :cond_0
    move v0, p1

    .line 164
    .local v0, "_isSuccess":Z
    move-object v1, p2

    .line 166
    .local v1, "_resultData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$8;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onReceiveSamsungAccountChecklistValidation(ZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "isSuccess"    # Z
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 115
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v2, :cond_0

    .line 132
    :goto_0
    return-void

    .line 119
    :cond_0
    move v0, p1

    .line 120
    .local v0, "_isSuccess":Z
    move-object v1, p2

    .line 122
    .local v1, "_resultData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$6;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$6;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onReceiveSamsungAccountDisclaimerAgreement(ZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "isSuccess"    # Z
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 137
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v2, :cond_0

    .line 154
    :goto_0
    return-void

    .line 141
    :cond_0
    move v0, p1

    .line 142
    .local v0, "_isSuccess":Z
    move-object v1, p2

    .line 144
    .local v1, "_resultData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$7;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$7;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onReceiveSamsungAccountSCloudAccessToken(ZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "isSuccess"    # Z
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 181
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v2, :cond_0

    .line 198
    :goto_0
    return-void

    .line 185
    :cond_0
    move v0, p1

    .line 186
    .local v0, "_isSuccess":Z
    move-object v1, p2

    .line 188
    .local v1, "_resultData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$9;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$9;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onReceiveSamsungAccountUserName(ZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "isSuccess"    # Z
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 203
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v2, :cond_0

    .line 220
    :goto_0
    return-void

    .line 207
    :cond_0
    move v0, p1

    .line 208
    .local v0, "_isSuccess":Z
    move-object v1, p2

    .line 210
    .local v1, "_resultData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$10;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$10;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;ZLandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onResponseSamsungAccountConnectService(Z)V
    .locals 3
    .param p1, "isSuccess"    # Z

    .prologue
    .line 20
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v1, :cond_0

    .line 35
    :goto_0
    return-void

    .line 24
    :cond_0
    move v0, p1

    .line 26
    .local v0, "_isSuccess":Z
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onResponseSamsungAccountDisconnectService()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onResponseSamsungAccountRegisterCallback(Z)V
    .locals 3
    .param p1, "isSuccess"    # Z

    .prologue
    .line 56
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v1, :cond_0

    .line 71
    :goto_0
    return-void

    .line 60
    :cond_0
    move v0, p1

    .line 62
    .local v0, "_isSuccess":Z
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$3;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onResponseSamsungAccountUnregisterCallback()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->getListenerHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp$4;-><init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;

    .prologue
    .line 15
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    .end local p1    # "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkSamsungAccountListenerImp;->mOnDriveLinkSamsungAccountListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkSamsungAccountListener;

    .line 16
    return-void
.end method
