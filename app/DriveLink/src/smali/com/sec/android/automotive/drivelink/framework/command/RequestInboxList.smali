.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestInboxList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInboxItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation
.end field

.field private mIsGetAllMessage:Z

.field private mItemCount:I

.field private mMsgLimitCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IZILcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .param p3, "isGetAllMessage"    # Z
    .param p4, "msgLimitCount"    # I
    .param p5, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p5}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mContext:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mInboxItemList:Ljava/util/ArrayList;

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mIsGetAllMessage:Z

    .line 24
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mContext:Landroid/content/Context;

    .line 25
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mItemCount:I

    .line 26
    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mIsGetAllMessage:Z

    .line 27
    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mMsgLimitCount:I

    .line 28
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mInboxItemList:Ljava/util/ArrayList;

    .line 51
    return-void
.end method

.method protected process()V
    .locals 5

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 34
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mItemCount:I

    .line 35
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mIsGetAllMessage:Z

    iget v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mMsgLimitCount:I

    .line 34
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getInboxList(Landroid/content/Context;IZI)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mInboxItemList:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMessageListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;

    move-result-object v0

    .line 42
    .local v0, "messageListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestInboxList;->mInboxItemList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;->onResponseRequestInboxList(Ljava/util/ArrayList;)V

    .line 45
    :cond_0
    return-void
.end method
