.class public final enum Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;
.super Ljava/lang/Enum;
.source "DrivingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CarMotionStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CAR_MOTION_IDLE:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

.field public static final enum CAR_MOTION_MOVING:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 80
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    const-string/jumbo v1, "CAR_MOTION_IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_IDLE:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    const-string/jumbo v1, "CAR_MOTION_MOVING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_MOVING:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    .line 79
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_IDLE:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->CAR_MOTION_MOVING:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager$CarMotionStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
