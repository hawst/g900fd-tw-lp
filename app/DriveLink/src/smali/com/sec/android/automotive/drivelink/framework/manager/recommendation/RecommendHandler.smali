.class public Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;
.super Ljava/lang/Object;
.source "RecommendHandler.java"


# static fields
.field private static final DEFAULT_PROJECTION:[Ljava/lang/String;

.field private static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "display_name COLLATE LOCALIZED ASC  LIMIT 12"

.field private static final DEFAULT_URI_CONTACT:Landroid/net/Uri;

.field private static final HAS_PHONE_NUMBER:Ljava/lang/String; = "has_phone_number > 0"

.field private static final IS_FAVORITE:Ljava/lang/String; = "starred = 1"

.field private static final SORT_BY_AZ:Ljava/lang/String; = "display_name COLLATE LOCALIZED ASC "

.field private static final SORT_BY_DATE_DESC:Ljava/lang/String; = "date DESC "

.field private static final SORT_BY_LIMIT:Ljava/lang/String; = " LIMIT 12"

.field private static final VISIBLE_GROUP_1:Ljava/lang/String; = "in_visible_group = \'1\'"

.field private static final mIsSamsungMobile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->DEFAULT_PROJECTION:[Ljava/lang/String;

    .line 43
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->DEFAULT_URI_CONTACT:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contactId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 195
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v10, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 199
    const-string/jumbo v3, "contact_id = ?"

    .line 200
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    .line 197
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 202
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 221
    :goto_0
    return-object v10

    .line 205
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 208
    const-string/jumbo v1, "data2"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 210
    .local v7, "colType":I
    const-string/jumbo v1, "data1"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 212
    .local v6, "colPhoneNumber":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 214
    .local v11, "type":I
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 215
    .local v9, "phoneNumber":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    invoke-direct {v1, v11, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    .end local v6    # "colPhoneNumber":I
    .end local v7    # "colType":I
    .end local v9    # "phoneNumber":Ljava/lang/String;
    .end local v11    # "type":I
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public getFavoriteContactList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v12, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 113
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v3, "in_visible_group = \'1\' AND (has_phone_number > 0) AND (starred = 1)"

    .line 115
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 117
    .local v4, "selectionArgs":[Ljava/lang/String;
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->DEFAULT_URI_CONTACT:Landroid/net/Uri;

    .line 118
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->DEFAULT_PROJECTION:[Ljava/lang/String;

    .line 119
    const-string/jumbo v5, "display_name COLLATE LOCALIZED ASC  LIMIT 12"

    .line 117
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 121
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 145
    :goto_0
    return-object v12

    .line 124
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 126
    const-string/jumbo v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 128
    .local v6, "colId":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 131
    :cond_1
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 132
    .local v9, "id":J
    invoke-direct {p0, p1, v9, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v11

    .line 135
    .local v11, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v8, v1, :cond_3

    .line 139
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 143
    .end local v6    # "colId":I
    .end local v8    # "i":I
    .end local v9    # "id":J
    .end local v11    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 136
    .restart local v6    # "colId":I
    .restart local v8    # "i":I
    .restart local v9    # "id":J
    .restart local v11    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_3
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public getFrequentContactList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v12, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 153
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v3, "in_visible_group = \'1\' AND (has_phone_number > 0) "

    .line 156
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 157
    .local v4, "selectionArgs":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "times_contacted DESC LIMIT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 157
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 161
    .local v5, "sortOrder":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 160
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 164
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 188
    :goto_0
    return-object v12

    .line 167
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 169
    const-string/jumbo v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 171
    .local v6, "colId":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 174
    :cond_1
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 175
    .local v9, "id":J
    invoke-direct {p0, p1, v9, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v11

    .line 178
    .local v11, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v8, v1, :cond_3

    .line 182
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    .end local v6    # "colId":I
    .end local v8    # "i":I
    .end local v9    # "id":J
    .end local v11    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 179
    .restart local v6    # "colId":I
    .restart local v8    # "i":I
    .restart local v9    # "id":J
    .restart local v11    # "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    :cond_3
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public getMissedCallList(Landroid/content/Context;J)Ljava/util/ArrayList;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v9, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 49
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .line 53
    .local v1, "uri":Landroid/net/Uri;
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 60
    const/4 v3, 0x0

    .line 61
    .local v3, "selection":Ljava/lang/String;
    const-wide/16 v10, 0x0

    cmp-long v2, p2, v10

    if-ltz v2, :cond_4

    .line 62
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "date > "

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v10, ") AND ( "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 63
    const-string/jumbo v10, "type"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v10, " = "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 62
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 69
    :goto_0
    const-string/jumbo v2, "RecommendHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "selection = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const/4 v4, 0x0

    .line 72
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v5, "date DESC "

    .line 76
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v2, 0x3

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string/jumbo v11, "number"

    aput-object v11, v2, v10

    const/4 v10, 0x1

    const-string/jumbo v11, "date"

    aput-object v11, v2, v10

    const/4 v10, 0x2

    const-string/jumbo v11, "type"

    aput-object v11, v2, v10

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 79
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 81
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    :cond_0
    const-string/jumbo v2, "number"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 85
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 90
    .local v8, "number":Ljava/lang/String;
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    .end local v8    # "number":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 97
    :cond_2
    if-eqz v6, :cond_3

    .line 98
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    :goto_1
    return-object v9

    .line 65
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    :cond_4
    const-string/jumbo v3, "type = \'3\'"

    goto :goto_0

    .line 101
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    .restart local v5    # "sortOrder":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 103
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
