.class public Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
.source "BaiduNavigationAdapter.java"


# static fields
.field private static final OPEN_MAP:Ljava/lang/String; = "intent://map/line?coordtype=&zoom=&region=&name=&src=samsung|drivelink#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end"

.field private static final OPEN_NAVIGATION:Ljava/lang/String; = "intent://map/direction?origin=%f,%f&destination=%f,%f&mode=driving&referer=samsung|drivelink#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end"

.field private static final PLACE_SEARCH_INTENT:Ljava/lang/String; = "intent://map/direction?origin=%f,%f&destination=%s&mode=driving&referer=samsung|drivelink#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end"

.field private static final TAG:Ljava/lang/String; = "BaiduNavigationAdapter"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;-><init>()V

    .line 36
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->setNeedOrigin(Z)V

    .line 37
    return-void
.end method

.method private getTTSText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    .line 229
    const-string/jumbo v1, "BaiduNavigationAdapter"

    const-string/jumbo v2, "getTTSText"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    move-object v0, p2

    .line 232
    .local v0, "tts":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 234
    const v2, 0x7f0a04e9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 233
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 237
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getNaviPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    const-string/jumbo v0, "com.baidu.BaiduMap"

    return-object v0
.end method

.method public getNavigationIntent(DD)Landroid/content/Intent;
    .locals 7
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 43
    const/4 v1, 0x0

    .line 45
    .local v1, "navigationIntent":Landroid/content/Intent;
    :try_start_0
    const-string/jumbo v2, "intent://map/direction?origin=%f,%f&destination=%f,%f&mode=driving&referer=samsung|drivelink#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v5

    iget-wide v5, v5, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v5

    iget-wide v5, v5, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    .line 45
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 47
    const/4 v3, 0x1

    .line 45
    invoke-static {v2, v3}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 48
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_0
    return-object v1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/net/URISyntaxException;
    const-string/jumbo v2, "Baidu Navi"

    const-string/jumbo v3, "URI Syntax Error"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_0
.end method

.method public startMap(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->startMap(Landroid/content/Context;)V

    .line 61
    const-string/jumbo v2, "BaiduNavigationAdapter"

    const-string/jumbo v3, "startMap"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const v2, 0x7f0a061c

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "mTts":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v1, p1, v2, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .local v1, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    .line 75
    return-void
.end method

.method public startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pacel"    # Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v11, 0x0

    .line 78
    const-string/jumbo v6, "BaiduNavigationAdapter"

    const-string/jumbo v7, "startMultiNavi"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v4, 0x1

    .line 81
    .local v4, "ret":Z
    if-eqz p2, :cond_2

    .line 82
    iget v6, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    if-nez v6, :cond_2

    .line 83
    const-string/jumbo v6, "BaiduNavigationAdapter"

    const-string/jumbo v7, "startMultiNavi startMap"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v3, 0x0

    .line 86
    .local v3, "navigationIntent":Landroid/content/Intent;
    :try_start_0
    const-string/jumbo v6, "intent://map/line?coordtype=&zoom=&region=&name=&src=samsung|drivelink#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end"

    .line 87
    const/4 v7, 0x1

    .line 86
    invoke-static {v6, v7}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    .line 89
    const/high16 v6, 0x14000000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 92
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v3

    .line 95
    if-eqz v3, :cond_0

    .line 96
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    move v5, v4

    .line 196
    .end local v4    # "ret":Z
    .local v5, "ret":I
    :goto_1
    return v5

    .line 99
    .end local v5    # "ret":I
    .restart local v4    # "ret":Z
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/net/URISyntaxException;
    const-string/jumbo v6, "BaiduNavigationAdapter"

    const-string/jumbo v7, "URISyntaxException"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_0

    .line 102
    .end local v0    # "e":Ljava/net/URISyntaxException;
    :catch_1
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "BaiduNavigationAdapter"

    const-string/jumbo v7, "Exception"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 106
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v6

    .line 105
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v6

    .line 107
    const-string/jumbo v7, "PREF_SETTINGS_MY_NAVIGATION"

    .line 106
    invoke-virtual {v6, v7, v11}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 108
    .local v2, "mapID":I
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v1

    .line 109
    .local v1, "goToMarket":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 110
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 112
    :cond_1
    const-string/jumbo v6, "Error Baidu Navi"

    .line 113
    const-string/jumbo v7, "Error on starting Baidu Navigation Intent"

    .line 112
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "goToMarket":Landroid/content/Intent;
    .end local v2    # "mapID":I
    .end local v3    # "navigationIntent":Landroid/content/Intent;
    :cond_2
    const/4 v3, 0x0

    .line 129
    .restart local v3    # "navigationIntent":Landroid/content/Intent;
    if-nez p2, :cond_3

    .line 130
    const/4 v4, 0x0

    move v5, v4

    .line 131
    .restart local v5    # "ret":I
    goto :goto_1

    .line 134
    .end local v5    # "ret":I
    :cond_3
    :try_start_1
    iget-wide v6, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_4

    iget-wide v6, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_4

    .line 150
    const-string/jumbo v6, "intent://map/direction?origin=%f,%f&destination=%s&mode=driving&referer=samsung|drivelink#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v9

    iget-wide v9, v9, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v9

    iget-wide v9, v9, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    aput-object v9, v7, v8

    .line 149
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 152
    const/4 v7, 0x1

    .line 149
    invoke-static {v6, v7}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    .line 154
    const/high16 v6, 0x14000000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 168
    :goto_2
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v3

    .line 171
    if-eqz v3, :cond_0

    .line 172
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_0

    .line 175
    :catch_2
    move-exception v0

    .line 176
    .local v0, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto/16 :goto_0

    .line 157
    .end local v0    # "e":Ljava/net/URISyntaxException;
    :cond_4
    :try_start_2
    sget-wide v6, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLat:D

    .line 158
    sget-wide v8, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLng:D

    .line 157
    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->setOrigin(DD)V

    .line 159
    const-string/jumbo v6, "BaiduNavigationAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "multi origin lat: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 160
    sget-wide v8, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLat:D

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", lng: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 161
    sget-wide v8, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mOriginLng:D

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 159
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const-string/jumbo v6, "BaiduNavigationAdapter"

    const-string/jumbo v7, "startBaidu"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-wide v6, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLat:D

    .line 165
    iget-wide v8, p2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mLng:D

    .line 164
    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getNavigationIntent(DD)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v3

    goto :goto_2

    .line 177
    :catch_3
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 179
    const/4 v4, 0x0

    .line 182
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v6

    .line 183
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 181
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v6

    .line 185
    const-string/jumbo v7, "PREF_SETTINGS_MY_NAVIGATION"

    .line 184
    invoke-virtual {v6, v7, v11}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v2

    .line 187
    .restart local v2    # "mapID":I
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getInstallNavigationApp(I)Landroid/content/Intent;

    move-result-object v1

    .line 188
    .restart local v1    # "goToMarket":Landroid/content/Intent;
    if-eqz v1, :cond_5

    .line 189
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 191
    :cond_5
    const-string/jumbo v6, "Error Baidu Navi"

    .line 192
    const-string/jumbo v7, "Error on starting Baidu Navigation Intent"

    .line 191
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public startNavigation(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    .line 204
    const-string/jumbo v1, "BaiduNavigationAdapter"

    const-string/jumbo v2, "startNavigation"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    if-nez v1, :cond_1

    .line 206
    const-string/jumbo v1, "BaiduNavigationAdapter"

    const-string/jumbo v2, "Origin is null."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    const-string/jumbo v1, "BaiduNavigationAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "origin lat: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", lng: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 210
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-direct {p0, p1, p6}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getTTSText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 215
    .local v7, "ttsText":Ljava/lang/String;
    const-string/jumbo v1, "MULTI_WINDOW_ENABLE"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;-><init>(Landroid/content/Context;DDLjava/lang/String;)V

    .line 219
    .local v0, "multiData":Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iget-wide v1, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->getOrigin()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->setOrigin(DD)V

    .line 220
    iput-object p6, v0, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->mTts:Ljava/lang/String;

    .line 221
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v7, v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;->startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method
