.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getMessageSipStateListener()Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$OnMessageSipStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$message$EditTextForMessageSIP$MessageSIPState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$message$EditTextForMessageSIP$MessageSIPState()[I
    .locals 3

    .prologue
    .line 434
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$EditTextForMessageSIP$MessageSIPState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->values()[Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$EditTextForMessageSIP$MessageSIPState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnStateChanged(Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;)V
    .locals 5
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 438
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$50(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;)V

    .line 440
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$message$EditTextForMessageSIP$MessageSIPState()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/message/EditTextForMessageSIP$MessageSIPState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 481
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 449
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$41(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 451
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 453
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    .line 455
    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->startPhraseSpotting()V

    .line 457
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$41(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 458
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$24(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->showVoiceLayout()V

    .line 462
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v2

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    .line 464
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$44(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 465
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$45(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/lang/String;)V

    .line 466
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$46(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 468
    const-string/jumbo v1, "MessageActivity"

    .line 469
    const-string/jumbo v2, "getMessageSipStateListener - setAutoShrink : true"

    .line 468
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    goto :goto_0

    .line 440
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
