.class Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;
.super Ljava/lang/Object;
.source "IndicatorFragmentViewForMulti.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/music/MusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;->this$0:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnBTBtnCallingState()V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public OnBTBtnClicked()V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method public OnBTBtnPlay()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;->this$0:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    # invokes: Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->refreshViews()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->access$1(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    .line 233
    return-void
.end method

.method public OnBTBtnSeek()V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public OnError(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 218
    return-void
.end method

.method public OnFinished()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;->this$0:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    # invokes: Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->refreshViews()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->access$1(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    .line 213
    return-void
.end method

.method public OnSeekComplete()V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public OnSetVolumeControl(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 253
    return-void
.end method

.method public OnShuffle(Z)V
    .locals 1
    .param p1, "bShuffle"    # Z

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;->this$0:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    # invokes: Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->refreshViews()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->access$1(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    .line 243
    return-void
.end method

.method public OnVolumeControlCommand(I)V
    .locals 0
    .param p1, "command"    # I

    .prologue
    .line 259
    return-void
.end method

.method public onMusicChanged(Z)V
    .locals 1
    .param p1, "isPlayed"    # Z

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti$2;->this$0:Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;

    # invokes: Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->refreshViews()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;->access$1(Lcom/sec/android/automotive/drivelink/home/IndicatorFragmentViewForMulti;)V

    .line 208
    return-void
.end method
