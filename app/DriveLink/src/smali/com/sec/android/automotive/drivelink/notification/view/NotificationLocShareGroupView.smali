.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocShareGroupView.java"


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mImageView:Landroid/widget/ImageView;

.field private mItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mImageView:Landroid/widget/ImageView;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mContext:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mViewLocal:Landroid/view/View;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mImageView:Landroid/widget/ImageView;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mContext:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mViewLocal:Landroid/view/View;

    .line 72
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->initIndicatorView(Landroid/content/Context;)V

    .line 74
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v8, 0x7f02023b

    const/4 v7, 0x0

    .line 83
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mContext:Landroid/content/Context;

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 86
    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 85
    check-cast v1, Landroid/view/LayoutInflater;

    .line 88
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mViewLocal:Landroid/view/View;

    if-nez v5, :cond_1

    .line 91
    const v5, 0x7f0300cb

    .line 90
    invoke-virtual {v1, v5, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mViewLocal:Landroid/view/View;

    .line 93
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mViewLocal:Landroid/view/View;

    const v6, 0x7f0902e6

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mImageView:Landroid/widget/ImageView;

    .line 95
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mViewLocal:Landroid/view/View;

    .line 96
    const v6, 0x7f0902e7

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 95
    check-cast v4, Landroid/widget/TextView;

    .line 97
    .local v4, "tvMessage":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 98
    const v6, 0x7f0a0395

    .line 97
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "msg":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    .line 100
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_0
    aput-object v5, v6, v7

    .line 99
    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 103
    const v6, 0x7f020238

    .line 101
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 104
    invoke-static {v5, v0, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 107
    .local v2, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 108
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 109
    const v6, 0x7f02023d

    .line 108
    invoke-static {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 112
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 113
    const v6, 0x7f02023c

    .line 112
    invoke-static {v5, v2, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 115
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 118
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "maskedBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "tvMessage":Landroid/widget/TextView;
    :cond_1
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView$1;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;)V

    .line 124
    const-wide/16 v7, 0x7d0

    .line 118
    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 126
    return-void

    .line 100
    .restart local v3    # "msg":Ljava/lang/String;
    .restart local v4    # "tvMessage":Landroid/widget/TextView;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareGroupView;->mItem:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v5

    goto :goto_0
.end method
