.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
.source "MessageDataHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$android$database$CursorJoiner$Result:[I


# instance fields
.field private AudioCodec:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private CalendarCodec:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ContactCodec:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ImageCodec:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private TAG:Ljava/lang/String;

.field private TextCodec:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private VideoCodec:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic $SWITCH_TABLE$android$database$CursorJoiner$Result()[I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->$SWITCH_TABLE$android$database$CursorJoiner$Result:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Landroid/database/CursorJoiner$Result;->values()[Landroid/database/CursorJoiner$Result;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Landroid/database/CursorJoiner$Result;->BOTH:Landroid/database/CursorJoiner$Result;

    invoke-virtual {v1}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Landroid/database/CursorJoiner$Result;->LEFT:Landroid/database/CursorJoiner$Result;

    invoke-virtual {v1}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Landroid/database/CursorJoiner$Result;->RIGHT:Landroid/database/CursorJoiner$Result;

    invoke-virtual {v1}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->$SWITCH_TABLE$android$database$CursorJoiner$Result:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;-><init>()V

    .line 26
    const-string/jumbo v0, "MessageDataHandler"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    .line 30
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->AudioType:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->AudioCodec:Ljava/util/ArrayList;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    .line 32
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->videoType:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->VideoCodec:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    .line 34
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->imageType:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->ImageCodec:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    .line 36
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->textType:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TextCodec:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    .line 38
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->calendarType:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->CalendarCodec:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    .line 40
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageConstant;->contactType:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->ContactCodec:Ljava/util/ArrayList;

    .line 60
    return-void
.end method

.method private getAddrMap(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadIds"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 404
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getRecipientIds(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 406
    .local v8, "recipients":Ljava/lang/String;
    if-nez v8, :cond_0

    .line 441
    :goto_0
    return-object v4

    .line 409
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 411
    .local v9, "startTime":J
    const-string/jumbo v0, "content://mms-sms/canonical-addresses"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 413
    .local v1, "ADDRESS_URI":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v5, "_id"

    aput-object v5, v2, v0

    const/4 v0, 0x1

    .line 414
    const-string/jumbo v5, "address"

    aput-object v5, v2, v0

    .line 415
    .local v2, "projection3":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "_id IN ("

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 417
    .local v3, "selection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 418
    .local v7, "addrcursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 421
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 423
    .local v6, "addrMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 424
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 427
    :cond_1
    const-string/jumbo v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 429
    const-string/jumbo v4, "address"

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 428
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 426
    invoke-virtual {v6, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 433
    :cond_2
    if-eqz v7, :cond_3

    .line 434
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 437
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 438
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, " getInboxList address cursor : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 439
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v9

    invoke-virtual {v4, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 438
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 437
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    .line 441
    goto/16 :goto_0
.end method

.method private getAttachmentsInfo(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611
    .local p2, "mmsids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 613
    .local v9, "attachments":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;>;"
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 615
    .local v16, "msgIds":Ljava/lang/String;
    if-nez v16, :cond_1

    .line 616
    const/4 v9, 0x0

    .line 680
    .end local v9    # "attachments":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;>;"
    :cond_0
    :goto_0
    return-object v9

    .line 618
    .restart local v9    # "attachments":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;>;"
    :cond_1
    const/4 v13, 0x0

    .local v13, "index":I
    :goto_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v13, v3, :cond_5

    .line 622
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mid IN ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 624
    .local v6, "selection":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 625
    const-string/jumbo v4, "content://mms/part"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 624
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 627
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 630
    :cond_2
    const-string/jumbo v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 629
    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 631
    .local v17, "partId":Ljava/lang/String;
    const-string/jumbo v3, "mid"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 633
    .local v15, "msgId":I
    const-string/jumbo v3, "ct"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 635
    .local v18, "type":Ljava/lang/String;
    const/4 v10, 0x0

    .line 636
    .local v10, "body":Ljava/lang/String;
    const/4 v14, 0x0

    .line 638
    .local v14, "isText":Z
    const-string/jumbo v3, "text/plain"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 640
    const-string/jumbo v3, "_data"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 639
    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 642
    .local v12, "data":Ljava/lang/String;
    if-eqz v12, :cond_6

    .line 643
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getMmsTextUsingStringBuilder(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 649
    :goto_2
    if-eqz v10, :cond_3

    .line 650
    const/4 v14, 0x1

    .line 652
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-virtual {v3, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->setText(Ljava/lang/String;)V

    .line 653
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-virtual {v3, v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->setIsText(Z)V

    .line 673
    .end local v12    # "data":Ljava/lang/String;
    :cond_3
    :goto_3
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 676
    .end local v10    # "body":Ljava/lang/String;
    .end local v14    # "isText":Z
    .end local v15    # "msgId":I
    .end local v17    # "partId":Ljava/lang/String;
    .end local v18    # "type":Ljava/lang/String;
    :cond_4
    if-eqz v11, :cond_0

    .line 677
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 619
    .end local v6    # "selection":Ljava/lang/String;
    .end local v11    # "cursor":Landroid/database/Cursor;
    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;-><init>()V

    invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 646
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local v10    # "body":Ljava/lang/String;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "data":Ljava/lang/String;
    .restart local v14    # "isText":Z
    .restart local v15    # "msgId":I
    .restart local v17    # "partId":Ljava/lang/String;
    .restart local v18    # "type":Ljava/lang/String;
    :cond_6
    const-string/jumbo v3, "text"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 645
    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_2

    .line 655
    .end local v12    # "data":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->ImageCodec:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 656
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 657
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->getImageCount()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 656
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->setImageCount(I)V

    goto :goto_3

    .line 658
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->ContactCodec:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 659
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 660
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->getContactCount()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 659
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->setContactCount(I)V

    goto :goto_3

    .line 661
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->AudioCodec:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 662
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 663
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->getAudioCount()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 662
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->setAudioCount(I)V

    goto/16 :goto_3

    .line 664
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->VideoCodec:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 665
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 666
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->getVideoCount()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 665
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->setVideoCount(I)V

    goto/16 :goto_3

    .line 669
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->CalendarCodec:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 670
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 671
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->getCalendarCount()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 670
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;->setCalendarCount(I)V

    goto/16 :goto_3
.end method

.method private getInboxListCursor(Landroid/content/Context;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadIds"    # Ljava/lang/String;
    .param p3, "itemCount"    # I

    .prologue
    .line 447
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 449
    .local v7, "startTime":J
    const-string/jumbo v0, "content://mms-sms/conversations?simple=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 451
    .local v1, "MESSAGE_URI":Landroid/net/Uri;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    .line 452
    const-string/jumbo v4, "date"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v4, "unread_count"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v4, "recipient_ids"

    aput-object v4, v2, v0

    .line 453
    .local v2, "projection2":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "_id IN ("

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 454
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, " date DESC"

    .line 456
    .local v5, "sortOrder":Ljava/lang/String;
    if-lez p3, :cond_0

    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " LIMIT "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 460
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 461
    const/4 v4, 0x0

    .line 460
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 463
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 464
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v9, " getInboxList thread cursor : "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 465
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v7

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 464
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 463
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    return-object v6
.end method

.method private getInboxThreadIds(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;
    .locals 27
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 293
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 295
    .local v7, "mLastDateMap":Ljava/util/HashMap;
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getSMSInboxCursor(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v13

    .line 296
    .local v13, "smsCursor":Landroid/database/Cursor;
    if-nez v13, :cond_0

    .line 297
    const/16 v19, 0x0

    .line 366
    :goto_0
    return-object v19

    .line 300
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getMMSInboxCursor(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v8

    .line 301
    .local v8, "mmsCursor":Landroid/database/Cursor;
    if-nez v8, :cond_2

    .line 302
    if-eqz v13, :cond_1

    .line 303
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_1
    const/16 v19, 0x0

    goto :goto_0

    .line 308
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 310
    .local v16, "startTime":J
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v12, v0, [Ljava/lang/String;

    const/16 v21, 0x0

    const-string/jumbo v22, "thread_Id"

    aput-object v22, v12, v21

    const/16 v21, 0x1

    const-string/jumbo v22, "last_date"

    aput-object v22, v12, v21

    .line 312
    .local v12, "projection":[Ljava/lang/String;
    new-instance v3, Landroid/database/CursorJoiner;

    invoke-direct {v3, v13, v12, v8, v12}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 315
    .local v3, "joiner":Landroid/database/CursorJoiner;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 317
    .local v20, "thread_ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v11, -0x1

    .line 318
    .local v11, "msgThreadID":I
    invoke-virtual {v3}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_3

    .line 355
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 356
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 358
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 360
    .local v18, "threadIds":Ljava/lang/String;
    new-instance v19, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 362
    .local v19, "threadIdsNDate":Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    .line 363
    new-instance v22, Ljava/lang/StringBuilder;

    const-string/jumbo v23, " getInboxList mms,sms cursor join : "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 364
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    sub-long v23, v23, v16

    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 363
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 362
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 318
    .end local v18    # "threadIds":Ljava/lang/String;
    .end local v19    # "threadIdsNDate":Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;
    :cond_3
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/CursorJoiner$Result;

    .line 319
    .local v4, "joinerResult":Landroid/database/CursorJoiner$Result;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->$SWITCH_TABLE$android$database$CursorJoiner$Result()[I

    move-result-object v22

    invoke-virtual {v4}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_0

    goto :goto_1

    .line 329
    :pswitch_0
    const-string/jumbo v22, "thread_id"

    move-object/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 328
    move/from16 v0, v22

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 330
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    .line 332
    const-string/jumbo v23, "last_date"

    move-object/from16 v0, v23

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    move/from16 v0, v23

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    const-wide/16 v25, 0x3e8

    mul-long v23, v23, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    .line 331
    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 322
    :pswitch_1
    const-string/jumbo v22, "thread_id"

    move-object/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 321
    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 323
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    .line 325
    const-string/jumbo v23, "last_date"

    move-object/from16 v0, v23

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    move/from16 v0, v23

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    .line 324
    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 337
    :pswitch_2
    const-string/jumbo v22, "thread_id"

    move-object/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 336
    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 338
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    const-string/jumbo v22, "last_date"

    move-object/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 340
    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 343
    .local v14, "smsTime":J
    const-string/jumbo v22, "last_date"

    move-object/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 342
    move/from16 v0, v22

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 343
    const-wide/16 v24, 0x3e8

    .line 342
    mul-long v9, v22, v24

    .line 344
    .local v9, "mmsTime":J
    move-wide v5, v14

    .line 346
    .local v5, "lastTime":J
    cmp-long v22, v14, v9

    if-gez v22, :cond_4

    .line 347
    move-wide v5, v9

    .line 349
    :cond_4
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getMMSInboxCursor(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 261
    if-nez p1, :cond_0

    .line 278
    :goto_0
    return-object v4

    .line 264
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 266
    .local v7, "startTime":J
    const-string/jumbo v0, "content://mms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 267
    .local v1, "MMS_INBOX_URI":Landroid/net/Uri;
    const-string/jumbo v3, "msg_box=1) GROUP BY (thread_Id "

    .line 268
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v9, "thread_Id"

    aput-object v9, v2, v0

    const/4 v0, 0x1

    .line 269
    const-string/jumbo v9, "MAX(date) as last_date"

    aput-object v9, v2, v0

    .line 270
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v5, "thread_Id"

    .line 272
    .local v5, "sortOrder":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 275
    .local v6, "mmsCursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v9, " getInboxList mmsCursor : "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 276
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v7

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 275
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    .line 278
    goto :goto_0
.end method

.method private getMmsTextUsingStringBuilder(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 685
    const/4 v1, 0x0

    .line 686
    .local v1, "inputStream":Ljava/io/InputStream;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 689
    .local v5, "stringBuilder":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 690
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "content://mms/part/"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 689
    invoke-virtual {v6, v7}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 692
    if-eqz v1, :cond_0

    .line 694
    new-instance v2, Ljava/io/InputStreamReader;

    .line 695
    const-string/jumbo v6, "UTF-8"

    .line 694
    invoke-direct {v2, v1, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 696
    .local v2, "isr":Ljava/io/InputStreamReader;
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 697
    .local v3, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 699
    .local v4, "sculpture":Ljava/lang/String;
    :goto_0
    if-nez v4, :cond_2

    .line 711
    .end local v2    # "isr":Ljava/io/InputStreamReader;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "sculpture":Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_1

    .line 714
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 721
    :cond_1
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 700
    .restart local v2    # "isr":Ljava/io/InputStreamReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "sculpture":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    goto :goto_0

    .line 706
    .end local v2    # "isr":Ljava/io/InputStreamReader;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "sculpture":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 707
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 711
    if-eqz v1, :cond_1

    .line 714
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 715
    :catch_1
    move-exception v0

    .line 716
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 710
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 711
    if-eqz v1, :cond_3

    .line 714
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 719
    :cond_3
    :goto_2
    throw v6

    .line 715
    :catch_2
    move-exception v0

    .line 716
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 715
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 716
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private getRecipientIds(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadIds"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 370
    if-nez p2, :cond_0

    .line 399
    :goto_0
    return-object v4

    .line 375
    :cond_0
    const-string/jumbo v0, "content://mms-sms/conversations?simple=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 377
    .local v1, "RECIPIENT_URI":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v5, "recipient_ids"

    aput-object v5, v2, v0

    .line 378
    .local v2, "projection4":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "_id IN ("

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 379
    .local v3, "selection":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 381
    .local v7, "recipientIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 384
    .local v6, "recipientCursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 389
    :cond_1
    const-string/jumbo v0, "recipient_ids"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 388
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 393
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->trimString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 395
    .local v8, "recipients":Ljava/lang/String;
    if-eqz v6, :cond_3

    .line 396
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v4, v8

    .line 399
    goto :goto_0
.end method

.method private getSMSInboxCursor(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 239
    if-nez p1, :cond_0

    .line 257
    :goto_0
    return-object v4

    .line 242
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 244
    .local v7, "startTime":J
    const-string/jumbo v0, "content://sms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 246
    .local v1, "SMS_INBOX_URI":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v9, "thread_Id"

    aput-object v9, v2, v0

    const/4 v0, 0x1

    .line 247
    const-string/jumbo v9, "MAX(date) as last_date"

    aput-object v9, v2, v0

    .line 248
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "type=1) GROUP BY (thread_Id "

    .line 249
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "thread_Id"

    .line 251
    .local v5, "sortOrder":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 254
    .local v6, "smsCursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v9, " getInboxList smsCursor : "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v7

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 254
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v6

    .line 257
    goto :goto_0
.end method

.method private getUnreadMessageCount(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    .line 725
    const/4 v6, 0x0

    .line 727
    .local v6, "count":I
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "COUNT(*) AS unread_count"

    aput-object v1, v2, v0

    .line 728
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "read= 0"

    .line 730
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 733
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 735
    const-string/jumbo v0, "unread_count"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 734
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 738
    :cond_0
    if-eqz v7, :cond_1

    .line 739
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 742
    :cond_1
    return v6
.end method

.method private makeGetIncommingMessagListByThreadID(Landroid/content/Context;II)Landroid/database/Cursor;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadID"    # I
    .param p3, "itemCount"    # I

    .prologue
    .line 473
    const-string/jumbo v0, "content://mms-sms/conversations"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    int-to-long v7, p2

    .line 472
    invoke-static {v0, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 475
    .local v1, "NEW_URI":Landroid/net/Uri;
    const/16 v0, 0xb

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    .line 476
    const-string/jumbo v4, "thread_id"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v4, "date"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v4, "body"

    aput-object v4, v2, v0

    const/4 v0, 0x4

    .line 477
    const-string/jumbo v4, "address"

    aput-object v4, v2, v0

    const/4 v0, 0x5

    const-string/jumbo v4, "m_type"

    aput-object v4, v2, v0

    const/4 v0, 0x6

    const-string/jumbo v4, "read"

    aput-object v4, v2, v0

    const/4 v0, 0x7

    .line 478
    const-string/jumbo v4, "hidden"

    aput-object v4, v2, v0

    const/16 v0, 0x8

    const-string/jumbo v4, "type"

    aput-object v4, v2, v0

    const/16 v0, 0x9

    .line 479
    const-string/jumbo v4, "sub"

    aput-object v4, v2, v0

    const/16 v0, 0xa

    const-string/jumbo v4, "sub_cs"

    aput-object v4, v2, v0

    .line 481
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "thread_id="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 482
    const-string/jumbo v4, " And ( type = 1"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 483
    const-string/jumbo v4, " OR m_type = 132 OR m_type = 130 ) AND hidden=0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 481
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 485
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, " normalized_date DESC"

    .line 487
    .local v5, "sortOrder":Ljava/lang/String;
    if-lez p3, :cond_0

    .line 488
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " LIMIT "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 491
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 492
    const/4 v4, 0x0

    .line 491
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 494
    .local v6, "cursor":Landroid/database/Cursor;
    return-object v6
.end method

.method private retrieveDLMessageListFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 500
    .local v18, "msgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 501
    .local v24, "tempMsgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 502
    .local v19, "msgids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 504
    .local v15, "mmsids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    .line 506
    .local v21, "startTime":J
    if-eqz p2, :cond_1

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 509
    :cond_0
    const-string/jumbo v2, "thread_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 508
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 511
    .local v3, "msgThreadID":I
    const-string/jumbo v2, "_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 510
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 513
    .local v4, "msgId":I
    const-string/jumbo v2, "address"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 512
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 515
    .local v10, "address":Ljava/lang/String;
    const-string/jumbo v2, "body"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 514
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 517
    .local v7, "msgBody":Ljava/lang/String;
    const-string/jumbo v2, "date"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 516
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 519
    .local v5, "receivedDate":J
    const-string/jumbo v2, "m_type"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 518
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 521
    .local v20, "msgtype":I
    const-string/jumbo v2, "read"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 520
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 524
    .local v9, "readStatus":I
    const/16 v23, 0x0

    .line 525
    .local v23, "tempMsgId":Ljava/lang/String;
    if-eqz v20, :cond_3

    .line 526
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 527
    const-wide/16 v25, 0x3e8

    mul-long v5, v5, v25

    .line 542
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v23

    .line 547
    :goto_0
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "message : msgThreadID "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " msgId "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 550
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " receivedDate "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " msgBody "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 551
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " msgtype "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " readStatus "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 552
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " address "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 549
    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .line 556
    if-nez v20, :cond_4

    const/4 v8, 0x0

    .line 558
    :goto_1
    const/4 v11, 0x0

    invoke-direct/range {v2 .. v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;-><init>(IIJLjava/lang/String;IILjava/lang/String;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;)V

    .line 554
    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 562
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    .line 565
    .end local v3    # "msgThreadID":I
    .end local v4    # "msgId":I
    .end local v5    # "receivedDate":J
    .end local v7    # "msgBody":Ljava/lang/String;
    .end local v9    # "readStatus":I
    .end local v10    # "address":Ljava/lang/String;
    .end local v20    # "msgtype":I
    .end local v23    # "tempMsgId":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 566
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v11, " getIncommingList conversion cursor : "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 567
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    sub-long v25, v25, v21

    move-wide/from16 v0, v25

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 566
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 565
    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 570
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    .line 572
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getAttachmentsInfo(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v12

    .line 575
    .local v12, "attachmentList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;>;"
    const/4 v13, 0x0

    .local v13, "index":I
    :goto_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v13, v2, :cond_5

    .line 582
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 583
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v11, " getIncommingList get attachmentList : "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 584
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    sub-long v25, v25, v21

    move-wide/from16 v0, v25

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 583
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 582
    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    .end local v12    # "attachmentList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;>;"
    .end local v13    # "index":I
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    .line 589
    const/16 v17, 0x0

    .local v17, "msgIndex":I
    :goto_3
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v17

    if-lt v0, v2, :cond_6

    .line 602
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 603
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v11, " getIncommingList merge with attachment  : "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 604
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    sub-long v25, v25, v21

    move-wide/from16 v0, v25

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 603
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 602
    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    return-object v18

    .line 544
    .end local v17    # "msgIndex":I
    .restart local v3    # "msgThreadID":I
    .restart local v4    # "msgId":I
    .restart local v5    # "receivedDate":J
    .restart local v7    # "msgBody":Ljava/lang/String;
    .restart local v9    # "readStatus":I
    .restart local v10    # "address":Ljava/lang/String;
    .restart local v20    # "msgtype":I
    .restart local v23    # "tempMsgId":Ljava/lang/String;
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "s"

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    goto/16 :goto_0

    .line 557
    :cond_4
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 576
    .end local v3    # "msgThreadID":I
    .end local v4    # "msgId":I
    .end local v5    # "receivedDate":J
    .end local v7    # "msgBody":Ljava/lang/String;
    .end local v9    # "readStatus":I
    .end local v10    # "address":Ljava/lang/String;
    .end local v20    # "msgtype":I
    .end local v23    # "tempMsgId":Ljava/lang/String;
    .restart local v12    # "attachmentList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;>;"
    .restart local v13    # "index":I
    :cond_5
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 577
    .restart local v4    # "msgId":I
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v14

    .line 578
    .local v14, "indexid":I
    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .line 579
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v12, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;

    .line 578
    invoke-virtual {v2, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->setAttachmentInfo(Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;)V

    .line 575
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 590
    .end local v4    # "msgId":I
    .end local v12    # "attachmentList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageAttachmentImpl;>;"
    .end local v13    # "index":I
    .end local v14    # "indexid":I
    .restart local v17    # "msgIndex":I
    :cond_6
    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .line 591
    .local v16, "msg":Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 594
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "message : msgThreadID "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgThreadID()I

    move-result v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " msgId "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 595
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgID()I

    move-result v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " receivedDate "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 596
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getReceivedTime()J

    move-result-wide v25

    move-wide/from16 v0, v25

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " msgBody "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 597
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgBody()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " msgtype "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgType()I

    move-result v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 598
    const-string/jumbo v11, " readStatus "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getReadStatus()I

    move-result v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 599
    const-string/jumbo v11, " address "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 594
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 593
    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3
.end method

.method private trimString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "srcString"    # Ljava/lang/String;

    .prologue
    .line 282
    if-nez p1, :cond_0

    .line 283
    const/4 v0, 0x0

    .line 288
    :goto_0
    return-object v0

    .line 285
    :cond_0
    const-string/jumbo v0, "["

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 286
    const-string/jumbo v0, "]"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    .line 288
    goto :goto_0
.end method


# virtual methods
.method public changeMessageStatusToRead(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    .prologue
    const/4 v6, 0x1

    .line 197
    const/4 v0, 0x0

    .line 199
    .local v0, "baseUri":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgType()I

    move-result v3

    if-nez v3, :cond_0

    .line 200
    const-string/jumbo v0, "content://sms"

    .line 205
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgID()I

    move-result v2

    .line 207
    .local v2, "msgId":I
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Before changeMessageStatusToRead message id"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " PhoneNumber : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 210
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ReadStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 211
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getReadStatus()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " msgBody: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 212
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgBody()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 208
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 207
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    new-instance v1, Landroid/content/Intent;

    .line 217
    const-string/jumbo v3, "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

    .line 216
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 219
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "NOTIFICATION_PACKAGE_NAME"

    .line 220
    const-string/jumbo v4, "com.android.mms"

    .line 219
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    const-string/jumbo v3, "NOTIFICATION_ITEM_ID"

    new-array v4, v6, [I

    const/4 v5, 0x0

    aput v2, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 223
    const-string/jumbo v3, "NOTIFICATION_ITEM_URI"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 228
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    .line 229
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "After changeMessageStatusToRead message id"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 230
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " PhoneNumber : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 231
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ReadStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 232
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getReadStatus()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " msgBody: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 233
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;->getMsgBody()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 229
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 228
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    return v6

    .line 202
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "msgId":I
    :cond_0
    const-string/jumbo v0, "content://mms"

    goto/16 :goto_0
.end method

.method public getInboxList(Landroid/content/Context;IZI)Ljava/util/ArrayList;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .param p3, "isGetAllMessage"    # Z
    .param p4, "msgLimitCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZI)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 65
    const/4 v13, 0x0

    .line 133
    :goto_0
    return-object v13

    .line 68
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getInboxThreadIds(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;

    move-result-object v18

    .line 70
    .local v18, "threadIdsnDate":Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->getThreadIds()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 71
    const/4 v13, 0x0

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->getThreadIds()Ljava/lang/String;

    move-result-object v3

    .line 74
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getAddrMap(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v11

    .line 77
    .local v11, "addrMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v13, "inboxItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;>;"
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->getThreadIds()Ljava/lang/String;

    move-result-object v3

    .line 79
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getInboxListCursor(Landroid/content/Context;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v12

    .line 82
    .local v12, "inboxCursor":Landroid/database/Cursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 84
    .local v16, "startTime":J
    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 87
    :cond_2
    const-string/jumbo v3, "_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 86
    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 88
    .local v6, "id":I
    const-wide/16 v7, 0x0

    .line 89
    .local v7, "date":J
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->getmLastDateMap()Ljava/util/HashMap;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 90
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler$InboxThreadIDsNDate;->getmLastDateMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 97
    :goto_1
    const-string/jumbo v3, "unread_count"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 96
    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 99
    .local v5, "unreadCount":I
    const-string/jumbo v3, "recipient_ids"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 98
    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 101
    .local v15, "recipient_ids":I
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 103
    .local v9, "address":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>()V

    .line 105
    .local v4, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz p3, :cond_6

    .line 106
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->makeGetIncommingMessagListByThreadID(Landroid/content/Context;II)Landroid/database/Cursor;

    move-result-object v14

    .line 108
    .local v14, "msglistCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->retrieveDLMessageListFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v10

    .line 111
    .local v10, "msgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;

    .line 112
    invoke-direct/range {v3 .. v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;IIJLjava/lang/String;Ljava/util/ArrayList;)V

    .line 111
    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    .end local v10    # "msgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    .end local v14    # "msglistCursor":Landroid/database/Cursor;
    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 126
    .end local v4    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v5    # "unreadCount":I
    .end local v6    # "id":I
    .end local v7    # "date":J
    .end local v9    # "address":Ljava/lang/String;
    .end local v15    # "recipient_ids":I
    :cond_3
    if-eqz v12, :cond_4

    .line 127
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 130
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, " thread, addr result: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v16

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 130
    move-object/from16 v0, v19

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 93
    .restart local v6    # "id":I
    .restart local v7    # "date":J
    :cond_5
    const-string/jumbo v3, "date"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 92
    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    goto :goto_1

    .line 114
    .restart local v4    # "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .restart local v5    # "unreadCount":I
    .restart local v9    # "address":Ljava/lang/String;
    .restart local v15    # "recipient_ids":I
    :cond_6
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;

    .line 115
    invoke-direct/range {v3 .. v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;IIJLjava/lang/String;)V

    .line 114
    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public getIncommingMessageList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;I)Ljava/util/ArrayList;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;
    .param p3, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->getMsgThreadID()I

    move-result v2

    .line 141
    .local v2, "threadID":I
    invoke-direct {p0, p1, v2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->makeGetIncommingMessagListByThreadID(Landroid/content/Context;II)Landroid/database/Cursor;

    move-result-object v0

    .line 143
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->retrieveDLMessageListFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    .line 146
    .local v1, "messageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;>;"
    return-object v1
.end method

.method public getUnreadMessageCount(Landroid/content/Context;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 176
    .line 177
    const-string/jumbo v2, "content://sms/inbox"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 176
    invoke-direct {p0, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getUnreadMessageCount(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v1

    .line 179
    .local v1, "smsUnreadCount":I
    const-string/jumbo v2, "content://mms"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 178
    invoke-direct {p0, p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getUnreadMessageCount(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    .line 181
    .local v0, "mmsUnreadCount":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getUnreadMessageCount :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 182
    add-int v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 181
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    add-int v2, v1, v0

    return v2
.end method

.method public getUnreadMessageCountByInbox(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;)I
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inbox"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;

    .prologue
    const/4 v2, 0x0

    .line 150
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLInboxImpl;->getMsgThreadID()I

    move-result v7

    .line 151
    .local v7, "threadID":I
    const/4 v8, 0x0

    .line 153
    .local v8, "unreadCount":I
    const-string/jumbo v0, "content://mms-sms/conversations?simple=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 155
    .local v1, "MESSAGE_URI":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "_id="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 157
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 160
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const-string/jumbo v0, "unread_count"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 161
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 165
    :cond_0
    if-eqz v6, :cond_1

    .line 166
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getUnreadMessageCountByThread : threadID "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 170
    const-string/jumbo v4, " unread count "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return v8
.end method

.method public getUreadMessageCountBySync(Landroid/content/Context;)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->getUnreadMessageCount(Landroid/content/Context;)I

    move-result v0

    .line 190
    .local v0, "unreadCount":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getUreadMessageCountBySync :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    return v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/MessageDataHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "terminate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    return-void
.end method
