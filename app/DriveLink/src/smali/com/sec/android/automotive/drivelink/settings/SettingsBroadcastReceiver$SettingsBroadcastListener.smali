.class public interface abstract Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;
.super Ljava/lang/Object;
.source "SettingsBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SettingsBroadcastListener"
.end annotation


# virtual methods
.method public abstract onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V
.end method

.method public abstract onConnect(Landroid/bluetooth/BluetoothDevice;)V
.end method

.method public abstract onDisconnect(Landroid/bluetooth/BluetoothDevice;)V
.end method
