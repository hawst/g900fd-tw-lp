.class Lcom/sec/android/automotive/drivelink/music/MusicService$6;
.super Ljava/lang/Object;
.source "MusicService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicService;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$6;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 1677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestAlbumList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1717
    .local p1, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    return-void
.end method

.method public onResponseRequestArtistList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1723
    .local p1, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    return-void
.end method

.method public onResponseRequestFolderList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1729
    .local p1, "folderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;>;"
    return-void
.end method

.method public onResponseRequestMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 1759
    return-void
.end method

.method public onResponseRequestMusicList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1681
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$6;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$25(Lcom/sec/android/automotive/drivelink/music/MusicService;Ljava/util/ArrayList;)V

    .line 1682
    return-void
.end method

.method public onResponseRequestMusicListByAlbum(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 0
    .param p2, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1694
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByArtist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 0
    .param p2, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1700
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByFolder(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
    .locals 0
    .param p2, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1706
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByMusic(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1688
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByPlaylist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
    .locals 0
    .param p2, "playlist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1712
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListFromSearchResult(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
    .locals 0
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1748
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestPlaylistList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1735
    .local p1, "playlistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;>;"
    return-void
.end method

.method public onResponseRequestSearchAllMusic(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1754
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestSearchMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 0
    .param p1, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .prologue
    .line 1741
    return-void
.end method
