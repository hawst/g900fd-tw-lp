.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocShareChangedDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static final TAG:Ljava/lang/String; = "[NotificationLocShareChangedDialogView]"

.field private static mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mChangedDestinationView:Landroid/widget/TextView;

.field private mContactBitMap:Landroid/widget/ImageView;

.field protected mContext:Landroid/content/Context;

.field private mMsgChangedDestination:Ljava/lang/String;

.field private mRouteButton:Landroid/widget/Button;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 37
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    .line 38
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mChangedDestinationView:Landroid/widget/TextView;

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 37
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    .line 38
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mChangedDestinationView:Landroid/widget/TextView;

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 37
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    .line 38
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mChangedDestinationView:Landroid/widget/TextView;

    .line 39
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    .line 40
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v6, 0x7f02023b

    .line 64
    if-nez p1, :cond_0

    .line 125
    :goto_0
    return-void

    .line 67
    :cond_0
    const-string/jumbo v3, "[NotificationLocShareChangedDialogView]"

    const-string/jumbo v4, "Init notification"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 71
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 70
    check-cast v1, Landroid/view/LayoutInflater;

    .line 73
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_3

    .line 75
    const v3, 0x7f0300b6

    .line 74
    invoke-virtual {v1, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    .line 78
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    .line 79
    const v4, 0x7f0902ee

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 78
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mChangedDestinationView:Landroid/widget/TextView;

    .line 80
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 81
    const v5, 0x7f0a0339

    .line 80
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82
    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 80
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    .line 83
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    if-eqz v3, :cond_1

    .line 84
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    .line 86
    :cond_1
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mChangedDestinationView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    .line 89
    const v4, 0x7f0902e0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 88
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContactBitMap:Landroid/widget/ImageView;

    .line 91
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    .line 92
    const v4, 0x7f0902e2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 91
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    .line 93
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902e3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    .line 95
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 107
    const v4, 0x7f020236

    .line 105
    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 109
    .local v0, "groupBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 108
    invoke-static {v3, v0, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 111
    .local v2, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 112
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 113
    const v4, 0x7f02023d

    .line 112
    invoke-static {v3, v4, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 116
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 117
    const v4, 0x7f02023c

    .line 116
    invoke-static {v3, v2, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 119
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContactBitMap:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 121
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->readNotificationTTS()V

    .line 124
    .end local v0    # "groupBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "maskedBitmap":Landroid/graphics/Bitmap;
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->setUseAutoTimeout(Z)V

    goto/16 :goto_0
.end method

.method private makeRoute()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 188
    sget-object v2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    if-nez v2, :cond_0

    .line 190
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 192
    const v4, 0x7f0a0330

    .line 191
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 189
    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    .line 193
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 215
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    .line 198
    const-string/jumbo v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 197
    check-cast v1, Landroid/location/LocationManager;

    .line 199
    .local v1, "manager":Landroid/location/LocationManager;
    const-string/jumbo v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 201
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 203
    const v4, 0x7f0a032d

    .line 202
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 200
    invoke-static {v2, v3, v5}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    .line 204
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 208
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 209
    .local v0, "intentMsg":Landroid/content/Intent;
    const-string/jumbo v2, "userAddress"

    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const-string/jumbo v2, "userLatitude"

    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 211
    const-string/jumbo v2, "userLongitude"

    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 212
    const-string/jumbo v2, "userIndex"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 214
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private readNotificationTTS()V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public static setContent(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 129
    sput-object p2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 130
    return-void
.end method


# virtual methods
.method protected getTTSText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mMsgChangedDestination:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 134
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 135
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 146
    :goto_0
    return-void

    .line 137
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->dismiss()V

    goto :goto_0

    .line 140
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->dismiss()V

    .line 141
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->makeRoute()V

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x7f0902e2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 235
    return-void
.end method

.method public onFlowCommandExcute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 220
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 240
    return-void
.end method

.method public onFlowCommandRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 230
    return-void
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 0
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 225
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 165
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 176
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 177
    return-void

    .line 167
    :pswitch_0
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 170
    :pswitch_1
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 173
    :pswitch_2
    new-array v0, v4, [Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mRouteButton:Landroid/widget/Button;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->mCancelButton:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->setButtonQuoted(Z[Landroid/widget/Button;)V

    goto :goto_0

    .line 165
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotiCommandFinished(I)V
    .locals 1
    .param p1, "cmd"    # I

    .prologue
    .line 181
    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->dismiss()V

    .line 183
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocShareChangedDialogView;->makeRoute()V

    .line 185
    :cond_0
    return-void
.end method
