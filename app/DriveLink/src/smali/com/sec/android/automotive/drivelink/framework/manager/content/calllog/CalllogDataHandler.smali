.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
.source "CalllogDataHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;-><init>()V

    .line 23
    return-void
.end method

.method private getCalllogList(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v14, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 156
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v2, "content://logs/historys"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 159
    .local v1, "uri":Landroid/net/Uri;
    const/4 v2, 0x7

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string/jumbo v16, "type"

    aput-object v16, v2, v15

    const/4 v15, 0x1

    const-string/jumbo v16, "name"

    aput-object v16, v2, v15

    const/4 v15, 0x2

    const-string/jumbo v16, "number"

    aput-object v16, v2, v15

    const/4 v15, 0x3

    const-string/jumbo v16, "duration"

    aput-object v16, v2, v15

    const/4 v15, 0x4

    const-string/jumbo v16, "date"

    aput-object v16, v2, v15

    const/4 v15, 0x5

    const-string/jumbo v16, "logtype"

    aput-object v16, v2, v15

    const/4 v15, 0x6

    const-string/jumbo v16, "date"

    aput-object v16, v2, v15

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 165
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_2

    .line 166
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 170
    :cond_0
    const-string/jumbo v2, "type"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 169
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 171
    .local v11, "callType":I
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 172
    .local v3, "type":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
    packed-switch v11, :pswitch_data_0

    .line 203
    :goto_0
    const-string/jumbo v2, "name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 202
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 205
    .local v5, "name":Ljava/lang/String;
    const-string/jumbo v2, "number"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 204
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 207
    .local v6, "phoneNumber":Ljava/lang/String;
    const-string/jumbo v2, "duration"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 206
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 209
    .local v7, "duration":Ljava/lang/String;
    const-string/jumbo v2, "date"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 208
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 212
    .local v8, "date":J
    const-string/jumbo v2, "logtype"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 211
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 214
    .local v10, "_logType":I
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 215
    .local v4, "logType":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    sparse-switch v10, :sswitch_data_0

    .line 246
    :goto_1
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;

    .line 247
    invoke-direct/range {v2 .. v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 246
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 252
    .end local v3    # "type":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
    .end local v4    # "logType":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "phoneNumber":Ljava/lang/String;
    .end local v7    # "duration":Ljava/lang/String;
    .end local v8    # "date":J
    .end local v10    # "_logType":I
    .end local v11    # "callType":I
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 260
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_2
    return-object v14

    .line 175
    .restart local v3    # "type":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
    .restart local v11    # "callType":I
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->INCOMING_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 176
    goto :goto_0

    .line 179
    :pswitch_1
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->OUTGOING_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 180
    goto :goto_0

    .line 183
    :pswitch_2
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->MISSED_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 184
    goto :goto_0

    .line 187
    :pswitch_3
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->VOICEMAIL_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 188
    goto :goto_0

    .line 191
    :pswitch_4
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->REJECTED_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 192
    goto :goto_0

    .line 195
    :pswitch_5
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->AUTOREJECTED_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    goto :goto_0

    .line 218
    .restart local v4    # "logType":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v6    # "phoneNumber":Ljava/lang/String;
    .restart local v7    # "duration":Ljava/lang/String;
    .restart local v8    # "date":J
    .restart local v10    # "_logType":I
    :sswitch_0
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_CALL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 219
    goto :goto_1

    .line 222
    :sswitch_1
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 223
    goto :goto_1

    .line 226
    :sswitch_2
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 227
    goto :goto_1

    .line 230
    :sswitch_3
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_EMAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .line 231
    goto :goto_1

    .line 234
    :sswitch_4
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_VIDEO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 255
    .end local v3    # "type":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
    .end local v4    # "logType":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "phoneNumber":Ljava/lang/String;
    .end local v7    # "duration":Ljava/lang/String;
    .end local v8    # "date":J
    .end local v10    # "_logType":I
    .end local v11    # "callType":I
    .end local v12    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v13

    .line 257
    .local v13, "ex":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 215
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public clearNewMissedCallFromCallLog(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 123
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;->sendIntentToClearNewMissedCallNotification(Landroid/content/Context;)V

    .line 126
    const-string/jumbo v3, "content://logs/historys"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 127
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 128
    .local v1, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "new"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 129
    const-string/jumbo v3, "is_read"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .local v2, "where":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "new"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string/jumbo v3, " = 1 AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string/jumbo v3, "type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    const-string/jumbo v3, " = ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 136
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 135
    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 137
    return-void
.end method

.method public getAllCalllogList(Landroid/content/Context;II)Ljava/util/ArrayList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hour"    # I
    .param p3, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "selection":Ljava/lang/String;
    const/4 v1, 0x0

    .line 144
    .local v1, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v2, "date DESC "

    .line 146
    .local v2, "sortOrder":Ljava/lang/String;
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/CalllogDataHandler;->getCalllogList(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    return-object v3
.end method

.method public getNewMissedCallCount(Landroid/content/Context;)I
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v6, 0x0

    .line 37
    .local v6, "count":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 39
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v2, "content://logs/historys"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 40
    .local v1, "uri":Landroid/net/Uri;
    const-string/jumbo v3, "type=3 AND new=1"

    .line 42
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 43
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 46
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v2, 0x4

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string/jumbo v10, "type"

    aput-object v10, v2, v9

    const/4 v9, 0x1

    const-string/jumbo v10, "new"

    aput-object v10, v2, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "_id"

    aput-object v10, v2, v9

    const/4 v9, 0x3

    const-string/jumbo v10, "date"

    aput-object v10, v2, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 49
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 50
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 52
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 55
    :cond_0
    const-string/jumbo v2, "_id"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 54
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    .line 58
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_0
    return v6

    .line 63
    :catch_0
    move-exception v8

    .line 65
    .local v8, "ex":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public sendIntentToClearNewMissedCallNotification(Landroid/content/Context;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v8, "callIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 76
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v3, "content://logs/historys"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 77
    .local v2, "uri":Landroid/net/Uri;
    const-string/jumbo v4, "type=3 AND new=1"

    .line 79
    .local v4, "selection":Ljava/lang/String;
    const/4 v5, 0x0

    .line 80
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 83
    .local v6, "sortOrder":Ljava/lang/String;
    const/4 v3, 0x4

    :try_start_0
    new-array v3, v3, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string/jumbo v15, "type"

    aput-object v15, v3, v14

    const/4 v14, 0x1

    const-string/jumbo v15, "new"

    aput-object v15, v3, v14

    const/4 v14, 0x2

    const-string/jumbo v15, "_id"

    aput-object v15, v3, v14

    const/4 v14, 0x3

    const-string/jumbo v15, "date"

    aput-object v15, v3, v14

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 86
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_2

    .line 88
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    :cond_0
    const-string/jumbo v3, "_id"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 90
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 95
    .local v7, "callId":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 99
    .end local v7    # "callId":I
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v9, v3, [I

    .line 106
    .local v9, "callIds":[I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v12, v3, :cond_3

    .line 110
    new-instance v13, Landroid/content/Intent;

    .line 111
    const-string/jumbo v3, "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

    .line 110
    invoke-direct {v13, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .local v13, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "NOTIFICATION_PACKAGE_NAME"

    const-string/jumbo v14, "com.android.phone"

    invoke-virtual {v13, v3, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string/jumbo v3, "NOTIFICATION_ITEM_ID"

    invoke-virtual {v13, v3, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 116
    const-string/jumbo v3, "NOTIFICATION_ITEM_URI"

    sget-object v14, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v13, v3, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 118
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 120
    return-void

    .line 101
    .end local v9    # "callIds":[I
    .end local v12    # "i":I
    .end local v13    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v11

    .line 103
    .local v11, "ex":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 107
    .end local v11    # "ex":Ljava/lang/Exception;
    .restart local v9    # "callIds":[I
    .restart local v12    # "i":I
    :cond_3
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v9, v12

    .line 106
    add-int/lit8 v12, v12, 0x1

    goto :goto_1
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    return-void
.end method
