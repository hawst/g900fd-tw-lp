.class public Lcom/sec/android/automotive/drivelink/location/map/util/places/PlaceService;
.super Ljava/lang/Object;
.source "PlaceService.java"


# static fields
.field private static final OPTION_KEY:Ljava/lang/String; = "&key=AIzaSyCdnE4MNl6HbMEitvtPBascKE3U3DfggGY"

.field private static final OPTION_LANGUAGE:Ljava/lang/String; = "&language=en"

.field private static final OPTION_LOCATION:Ljava/lang/String; = "&location="

.field private static final OPTION_NAME:Ljava/lang/String; = "&name="

.field private static final OPTION_RANKBY:Ljava/lang/String; = "&rankby=distance"

.field private static final OPTION_SENSOR:Ljava/lang/String; = "&sensor=false"

.field private static final PLACES_API_BASE_URL:Ljava/lang/String; = "https://maps.googleapis.com/maps/api/place"

.field private static final REQUEST:Ljava/lang/String; = "/nearbysearch"

.field private static final RESPONSE_OUT_PUT:Ljava/lang/String; = "/json?"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method private getJSON(Ljava/net/URL;)Lorg/json/JSONObject;
    .locals 11
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 128
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .local v3, "content":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 130
    .local v2, "conn":Ljava/net/HttpURLConnection;
    const/4 v6, 0x0

    .line 133
    .local v6, "json":Lorg/json/JSONObject;
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 136
    const/16 v9, 0x400

    new-array v1, v9, [C

    .line 137
    .local v1, "buff":[C
    new-instance v5, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    .line 138
    const-string/jumbo v10, "UTF-8"

    .line 137
    invoke-direct {v5, v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 140
    .local v5, "in":Ljava/io/InputStreamReader;
    invoke-virtual {v5, v1}, Ljava/io/InputStreamReader;->read([C)I

    move-result v8

    .line 141
    .local v8, "numOfCharsRead":I
    :goto_0
    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    .line 146
    new-instance v7, Lorg/json/JSONObject;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    .end local v6    # "json":Lorg/json/JSONObject;
    .local v7, "json":Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 153
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    move-object v6, v7

    .line 157
    .end local v1    # "buff":[C
    .end local v5    # "in":Ljava/io/InputStreamReader;
    .end local v7    # "json":Lorg/json/JSONObject;
    .end local v8    # "numOfCharsRead":I
    .restart local v6    # "json":Lorg/json/JSONObject;
    :cond_1
    :goto_1
    return-object v6

    .line 142
    .restart local v1    # "buff":[C
    .restart local v5    # "in":Ljava/io/InputStreamReader;
    .restart local v8    # "numOfCharsRead":I
    :cond_2
    const/4 v9, 0x0

    :try_start_1
    invoke-virtual {v3, v1, v9, v8}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v5, v1}, Ljava/io/InputStreamReader;->read([C)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    goto :goto_0

    .line 147
    .end local v1    # "buff":[C
    .end local v5    # "in":Ljava/io/InputStreamReader;
    .end local v8    # "numOfCharsRead":I
    :catch_0
    move-exception v4

    .line 148
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 152
    if-eqz v2, :cond_1

    .line 153
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 149
    .end local v4    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 150
    .local v4, "e":Lorg/json/JSONException;
    :try_start_3
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 152
    if-eqz v2, :cond_1

    .line 153
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 151
    .end local v4    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v9

    .line 152
    if-eqz v2, :cond_3

    .line 153
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 155
    :cond_3
    throw v9
.end method

.method private makeUrl(Ljava/lang/String;DD)Ljava/net/URL;
    .locals 5
    .param p1, "strToSearch"    # Ljava/lang/String;
    .param p2, "curLat"    # D
    .param p4, "curLng"    # D

    .prologue
    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .local v1, "uri":Ljava/lang/StringBuilder;
    const-string/jumbo v4, "https://maps.googleapis.com/maps/api/place"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string/jumbo v4, "/nearbysearch"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const-string/jumbo v4, "/json?"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    const-string/jumbo v4, "&rankby=distance"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string/jumbo v4, "&language=en"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string/jumbo v4, "&sensor=false"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string/jumbo v4, "&key=AIzaSyCdnE4MNl6HbMEitvtPBascKE3U3DfggGY"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string/jumbo v4, "&name="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v4, "&location="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 117
    const/4 v2, 0x0

    .line 119
    .local v2, "url":Ljava/net/URL;
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "url":Ljava/net/URL;
    .local v3, "url":Ljava/net/URL;
    move-object v2, v3

    .line 124
    .end local v3    # "url":Ljava/net/URL;
    .restart local v2    # "url":Ljava/net/URL;
    :goto_0
    return-object v2

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public searchPlace(Ljava/lang/String;DD)Ljava/util/ArrayList;
    .locals 14
    .param p1, "strToSearch"    # Ljava/lang/String;
    .param p2, "curLat"    # D
    .param p4, "curLng"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DD)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    const/4 v6, 0x0

    .line 52
    .local v6, "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/location/map/util/places/PlaceService;->makeUrl(Ljava/lang/String;DD)Ljava/net/URL;

    move-result-object v11

    .line 53
    .local v11, "url":Ljava/net/URL;
    if-nez v11, :cond_0

    move-object v7, v6

    .line 96
    .end local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .local v7, "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :goto_0
    return-object v7

    .line 57
    .end local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_0
    invoke-direct {p0, v11}, Lcom/sec/android/automotive/drivelink/location/map/util/places/PlaceService;->getJSON(Ljava/net/URL;)Lorg/json/JSONObject;

    move-result-object v3

    .line 58
    .local v3, "jsonObj":Lorg/json/JSONObject;
    if-nez v3, :cond_1

    move-object v7, v6

    .line 59
    .end local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_0

    .line 62
    .end local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_1
    :try_start_0
    const-string/jumbo v12, "status"

    invoke-virtual {v3, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 63
    .local v10, "status":Ljava/lang/String;
    const-string/jumbo v12, "REQUEST_DENIED"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 64
    const-string/jumbo v12, "INVALID_REQUEST"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 65
    const-string/jumbo v12, "OVER_QUERY_LIMIT"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 66
    :cond_2
    new-instance v12, Ljava/net/ConnectException;

    invoke-direct {v12, v10}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_1

    .line 90
    .end local v10    # "status":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .end local v0    # "e":Lorg/json/JSONException;
    :goto_2
    move-object v7, v6

    .line 96
    .end local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_0

    .line 69
    .end local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v10    # "status":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string/jumbo v12, "results"

    invoke-virtual {v3, v12}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 70
    .local v9, "results":Lorg/json/JSONArray;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/ConnectException; {:try_start_1 .. :try_end_1} :catch_1

    .line 75
    .end local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    :try_start_2
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-lt v2, v12, :cond_4

    move-object v6, v7

    .line 90
    .end local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_2

    .line 76
    .end local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :cond_4
    invoke-virtual {v9, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 77
    .local v8, "result":Lorg/json/JSONObject;
    const-string/jumbo v12, "geometry"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 78
    .local v1, "geometry":Lorg/json/JSONObject;
    const-string/jumbo v12, "location"

    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    .line 80
    .local v4, "location":Lorg/json/JSONObject;
    new-instance v5, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;

    invoke-direct {v5}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;-><init>()V

    .line 81
    .local v5, "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    const-string/jumbo v12, "vicinity"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setAddress(Ljava/lang/String;)V

    .line 82
    const-string/jumbo v12, "icon"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setIcon(Ljava/lang/String;)V

    .line 83
    const-string/jumbo v12, "name"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setName(Ljava/lang/String;)V

    .line 84
    const-string/jumbo v12, "id"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setId(Ljava/lang/String;)V

    .line 85
    const-string/jumbo v12, "lat"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-virtual {v5, v12, v13}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setLat(D)V

    .line 86
    const-string/jumbo v12, "lng"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-virtual {v5, v12, v13}, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->setLng(D)V

    .line 88
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/ConnectException; {:try_start_2 .. :try_end_2} :catch_2

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 92
    .end local v1    # "geometry":Lorg/json/JSONObject;
    .end local v2    # "i":I
    .end local v4    # "location":Lorg/json/JSONObject;
    .end local v5    # "place":Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .end local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .end local v8    # "result":Lorg/json/JSONObject;
    .end local v9    # "results":Lorg/json/JSONArray;
    .end local v10    # "status":Ljava/lang/String;
    .restart local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :catch_1
    move-exception v0

    .line 93
    .local v0, "e":Ljava/net/ConnectException;
    :goto_4
    invoke-virtual {v0}, Ljava/net/ConnectException;->printStackTrace()V

    goto :goto_2

    .line 92
    .end local v0    # "e":Ljava/net/ConnectException;
    .end local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v2    # "i":I
    .restart local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v9    # "results":Lorg/json/JSONArray;
    .restart local v10    # "status":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object v6, v7

    .end local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_4

    .line 90
    .end local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    :catch_3
    move-exception v0

    move-object v6, v7

    .end local v7    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    .restart local v6    # "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    goto :goto_1
.end method
