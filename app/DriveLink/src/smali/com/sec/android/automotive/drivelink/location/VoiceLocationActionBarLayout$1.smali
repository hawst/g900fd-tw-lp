.class Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;
.super Landroid/os/Handler;
.source "VoiceLocationActionBarLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 1154
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mIsMicDisplayed:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$0(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mVoiceLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$2(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$3(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1165
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1166
    return-void

    .line 1163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->updateMicLayouts()V

    goto :goto_0
.end method
