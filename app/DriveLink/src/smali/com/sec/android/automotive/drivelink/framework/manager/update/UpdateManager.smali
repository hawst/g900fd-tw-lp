.class public Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "UpdateManager.java"


# static fields
.field private static final RESULT_DOWNLOAD_PROGRESS:I = 0x3

.field private static final RESULT_UPDATE_FAIL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "UpdateManager"


# instance fields
.field private isCancelCheckUpdateCall:Z

.field private mApplicationMgr:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

.field private mCheckInputStream:Ljava/io/InputStream;

.field private mCheckhttpclient:Lorg/apache/http/client/HttpClient;

.field private mContext:Landroid/content/Context;

.field private mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

.field private mIsCancel:Z

.field private mResultHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mIsCancel:Z

    .line 43
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    .line 48
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 49
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 50
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 53
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mIsCancel:Z

    .line 55
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mApplicationMgr:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mApplicationMgr:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->setOnInstalledPackaged(Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mApplicationMgr:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->setOnCallPackageInstall(Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;)V

    .line 92
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mResultHandler:Landroid/os/Handler;

    .line 109
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V
    .locals 0

    .prologue
    .line 900
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendUpdateSuccessResult()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V
    .locals 0

    .prologue
    .line 884
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendUpdateFailResult(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V
    .locals 0

    .prologue
    .line 908
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendInstallStart()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->printtestcode()V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V
    .locals 0

    .prologue
    .line 892
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->sendDownloadProgress(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V

    return-void
.end method

.method private clearFileOutpStream(Ljava/io/FileOutputStream;)V
    .locals 1
    .param p1, "Fout"    # Ljava/io/FileOutputStream;

    .prologue
    .line 461
    if-eqz p1, :cond_0

    .line 463
    :try_start_0
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 464
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private installApplication(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    .line 278
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 280
    .local v1, "folder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 283
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 282
    invoke-direct {p0, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 320
    :goto_0
    return-void

    .line 287
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_INSTALL_RESULT:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V

    .line 291
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mApplicationMgr:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->initalize(Landroid/content/Context;)V

    .line 292
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mApplicationMgr:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->installPackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 296
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 297
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 295
    invoke-direct {p0, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 298
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 299
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 301
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 302
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 300
    invoke-direct {p0, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 303
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 306
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 307
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 305
    invoke-direct {p0, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 308
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 309
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 311
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 312
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 310
    invoke-direct {p0, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 313
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 314
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 316
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->INSTALL_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 317
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 315
    invoke-direct {p0, v4, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private isDownloable(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;)Z
    .locals 2
    .param p1, "downloadInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;

    .prologue
    .line 441
    if-eqz p1, :cond_0

    .line 442
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->getAppId()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->getResultCode()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->getDownloadURL()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 445
    const/4 v0, 0x1

    .line 448
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPossibleSendResult()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 876
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    if-nez v1, :cond_1

    .line 881
    :cond_0
    :goto_0
    return v0

    .line 878
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkUpdateListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 881
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private makeNSendMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "msyType"    # I
    .param p2, "arg"    # Ljava/lang/Object;

    .prologue
    .line 452
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mResultHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 454
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 455
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 457
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mResultHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 458
    return-void
.end method

.method private printtestcode()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 142
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    if-nez v6, :cond_0

    .line 143
    const-string/jumbo v6, "UpdateManager"

    const-string/jumbo v7, "setPreferenceBoolean mContext = null"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    .line 148
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->PREFERENCE_NAME:Ljava/lang/String;

    .line 147
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 150
    .local v5, "pref":Landroid/content/SharedPreferences;
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_UPDATE_VERSION:Ljava/lang/String;

    .line 149
    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 151
    .local v3, "mIsUpdateVersion":Z
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_UPDATE_VERSION:Ljava/lang/String;

    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 154
    .local v4, "mUpdateVersion":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_DOWNLOAD_VERSION:Ljava/lang/String;

    .line 153
    invoke-interface {v5, v6, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "mDownloadVersion":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    .line 155
    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 159
    .local v2, "mIsCompletedDownload":Z
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_INSTALL_RESULT:Ljava/lang/String;

    .line 158
    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 162
    .local v1, "mInstallResult":Z
    const-string/jumbo v6, "UpdateManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, " mIsUpdateVersion : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 163
    const-string/jumbo v8, " mUpdateVersion : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 164
    const-string/jumbo v8, " mDownloadVersion : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 165
    const-string/jumbo v8, " mIsCompletedDownload : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 166
    const-string/jumbo v8, " mInstallResult : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 162
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private requestCheckUpdateVersion(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "checkURLstr"    # Ljava/lang/String;

    .prologue
    .line 723
    const/4 v8, 0x0

    .line 726
    .local v8, "isUpdateVersion":Z
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v5

    .line 727
    .local v5, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v11

    .line 734
    .local v11, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v3, Ljava/net/URI;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 749
    .local v3, "Uri":Ljava/net/URI;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 750
    const-string/jumbo v18, "UpdateManager"

    .line 751
    const-string/jumbo v19, "requestCheckUpdateVersion : isCancelCheckUpdateCall true "

    .line 750
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 864
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v18, "UpdateManager"

    .line 869
    const-string/jumbo v19, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 872
    .end local v3    # "Uri":Ljava/net/URI;
    .end local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "isUpdateVersion":Z
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .local v9, "isUpdateVersion":I
    :goto_0
    return v9

    .line 757
    .end local v9    # "isUpdateVersion":I
    .restart local v3    # "Uri":Ljava/net/URI;
    .restart local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "isUpdateVersion":Z
    .restart local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_0
    :try_start_1
    new-instance v18, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct/range {v18 .. v18}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v10

    .line 761
    .local v10, "params":Lorg/apache/http/params/HttpParams;
    const/16 v18, 0x1388

    .line 760
    move/from16 v0, v18

    invoke-static {v10, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 763
    const/16 v18, 0x3a98

    .line 762
    move/from16 v0, v18

    invoke-static {v10, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 765
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v6, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 767
    .local v6, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 768
    .local v13, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 769
    const-string/jumbo v18, "UpdateManager"

    .line 770
    const-string/jumbo v19, "requestCheckUpdateVersion : httpClient&InputStream created "

    .line 769
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 773
    const-string/jumbo v18, "UpdateManager"

    .line 774
    const-string/jumbo v19, "requestCheckUpdateVersion httpClient&InputStream is not null"

    .line 773
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v11, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 778
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v12

    .line 780
    .local v12, "parserEvent":I
    const-string/jumbo v7, ""

    .local v7, "id":Ljava/lang/String;
    const-string/jumbo v14, ""

    .local v14, "result":Ljava/lang/String;
    const-string/jumbo v17, ""

    .line 782
    .local v17, "version":Ljava/lang/String;
    const-string/jumbo v18, "UpdateManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "requestCheckUpdateVersion cancelFlag :"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 783
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 782
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    :goto_1
    const/16 v18, 0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_1

    .line 786
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    move/from16 v18, v0
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v18, :cond_2

    .line 864
    .end local v7    # "id":Ljava/lang/String;
    .end local v12    # "parserEvent":I
    .end local v14    # "result":Ljava/lang/String;
    .end local v17    # "version":Ljava/lang/String;
    :cond_1
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v18, "UpdateManager"

    .line 869
    const-string/jumbo v19, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .end local v3    # "Uri":Ljava/net/URI;
    .end local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "params":Lorg/apache/http/params/HttpParams;
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    :goto_2
    move v9, v8

    .line 872
    .restart local v9    # "isUpdateVersion":I
    goto/16 :goto_0

    .line 787
    .end local v9    # "isUpdateVersion":I
    .restart local v3    # "Uri":Ljava/net/URI;
    .restart local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v7    # "id":Ljava/lang/String;
    .restart local v10    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v12    # "parserEvent":I
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "result":Ljava/lang/String;
    .restart local v17    # "version":Ljava/lang/String;
    :cond_2
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v12, v0, :cond_5

    .line 788
    :try_start_2
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    .line 790
    .local v15, "tag":Ljava/lang/String;
    const-string/jumbo v18, "appId"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 791
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v16

    .line 792
    .local v16, "type":I
    const/16 v18, 0x4

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 793
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    .line 794
    const-string/jumbo v18, "UpdateManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "requestCheckUpdateVersion appId : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 795
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 794
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    .end local v16    # "type":I
    :cond_3
    const-string/jumbo v18, "resultCode"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 800
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v16

    .line 801
    .restart local v16    # "type":I
    const/16 v18, 0x4

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 802
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v14

    .line 803
    const-string/jumbo v18, "UpdateManager"

    .line 804
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "requestCheckUpdateVersion result : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 805
    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 804
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 803
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    .end local v16    # "type":I
    :cond_4
    const-string/jumbo v18, "versionName"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 810
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v16

    .line 811
    .restart local v16    # "type":I
    const/16 v18, 0x4

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 812
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v17

    .line 818
    .end local v15    # "tag":Ljava/lang/String;
    .end local v16    # "type":I
    :cond_5
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ne v12, v0, :cond_7

    .line 820
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    .line 822
    .restart local v15    # "tag":Ljava/lang/String;
    const-string/jumbo v18, "versionName"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 824
    sget-object v18, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_UPDATE_VERSION:Ljava/lang/String;

    .line 823
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceString(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    const-string/jumbo v17, ""

    .line 829
    :cond_6
    const-string/jumbo v18, "resultCode"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 830
    const-string/jumbo v18, "2"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 831
    const/4 v8, 0x1

    .line 833
    sget-object v18, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_UPDATE_VERSION:Ljava/lang/String;

    .line 834
    const/16 v19, 0x1

    .line 832
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V

    .line 839
    :goto_3
    const-string/jumbo v7, ""

    .line 840
    const-string/jumbo v14, ""

    .line 844
    .end local v15    # "tag":Ljava/lang/String;
    :cond_7
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v12

    goto/16 :goto_1

    .line 836
    .restart local v15    # "tag":Ljava/lang/String;
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setInitializePrefernce()V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 848
    .end local v3    # "Uri":Ljava/net/URI;
    .end local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v6    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v7    # "id":Ljava/lang/String;
    .end local v10    # "params":Lorg/apache/http/params/HttpParams;
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v12    # "parserEvent":I
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "result":Ljava/lang/String;
    .end local v15    # "tag":Ljava/lang/String;
    .end local v17    # "version":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 849
    .local v4, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    const-string/jumbo v18, "UpdateManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "CheckUpdate_XmlPullParserException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 864
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v18, "UpdateManager"

    .line 869
    const-string/jumbo v19, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 851
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v4

    .line 852
    .local v4, "e":Ljava/net/SocketException;
    :try_start_4
    const-string/jumbo v18, "UpdateManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "CheckUpdate_SocketException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    invoke-virtual {v4}, Ljava/net/SocketException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 864
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v18, "UpdateManager"

    .line 869
    const-string/jumbo v19, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 854
    .end local v4    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v4

    .line 855
    .local v4, "e":Ljava/net/UnknownHostException;
    :try_start_5
    const-string/jumbo v18, "UpdateManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "CheckUpdate_UnknownHostException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    invoke-virtual {v4}, Ljava/net/UnknownHostException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 864
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v18, "UpdateManager"

    .line 869
    const-string/jumbo v19, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 857
    .end local v4    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v4

    .line 858
    .local v4, "e":Ljava/io/IOException;
    :try_start_6
    const-string/jumbo v18, "UpdateManager"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "CheckUpdate_IOException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 864
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v18, "UpdateManager"

    .line 869
    const-string/jumbo v19, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 860
    .end local v4    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v4

    .line 862
    .local v4, "e":Ljava/net/URISyntaxException;
    :try_start_7
    invoke-virtual {v4}, Ljava/net/URISyntaxException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 864
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v18, "UpdateManager"

    .line 869
    const-string/jumbo v19, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 863
    .end local v4    # "e":Ljava/net/URISyntaxException;
    :catchall_0
    move-exception v18

    .line 864
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 865
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 866
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 868
    const-string/jumbo v19, "UpdateManager"

    .line 869
    const-string/jumbo v20, "requestCheckUpdateVersion :  mCheckhttpclient, mCheckInputStream, isCancelCheckUpdateCall initialized"

    .line 868
    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    throw v18
.end method

.method private requestDownload(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;)Z
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;

    .prologue
    .line 608
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isDownloable(Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 609
    const/4 v7, 0x2

    .line 610
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 611
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 609
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 612
    const/4 v7, 0x0

    .line 717
    :goto_0
    return v7

    .line 615
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->getIsNoProxy()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 616
    const-string/jumbo v7, "UpdateManager"

    const-string/jumbo v8, " requestDownload : NO PROXY"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->requestDownloadNoProxy(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;)Z

    move-result v7

    goto :goto_0

    .line 620
    :cond_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->getFile()Ljava/io/File;

    move-result-object v19

    .line 622
    .local v19, "file":Ljava/io/File;
    new-instance v21, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct/range {v21 .. v21}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 623
    .local v21, "httpclient":Lorg/apache/http/client/HttpClient;
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v23

    .line 626
    .local v23, "params":Lorg/apache/http/params/HttpParams;
    const/16 v7, 0x2710

    .line 625
    move-object/from16 v0, v23

    invoke-static {v0, v7}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 628
    const/16 v7, 0x7530

    .line 627
    move-object/from16 v0, v23

    invoke-static {v0, v7}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 630
    new-instance v20, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct/range {v20 .. v20}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 632
    .local v20, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 633
    const-string/jumbo v7, "Range"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "bytes="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-virtual {v0, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :cond_2
    const-wide/16 v3, 0x0

    .line 637
    .local v3, "sizeTotal":J
    const/16 v17, 0x0

    .line 639
    .local v17, "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    const/4 v14, 0x0

    .line 642
    .local v14, "Fout":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v25

    .line 644
    .local v25, "sizeDownload":J
    new-instance v7, Ljava/net/URI;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->getDownloadURL()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 646
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v24

    .line 647
    .local v24, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v15

    .line 649
    .local v15, "InStream":Ljava/io/InputStream;
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v7

    add-long v3, v7, v25

    .line 651
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v7

    .line 652
    const v8, 0x8001

    .line 651
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v14

    .line 654
    const/16 v7, 0x400

    new-array v0, v7, [B

    move-object/from16 v16, v0

    .line 655
    .local v16, "buffer":[B
    const/16 v22, 0x0

    .line 657
    .local v22, "len1":I
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V

    .line 659
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;

    .line 660
    const-wide/16 v5, 0x0

    .line 661
    const-wide/16 v7, 0x0

    .line 659
    invoke-direct/range {v2 .. v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;-><init>(JJJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 662
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .local v2, "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    const/4 v7, 0x3

    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-wide/from16 v5, v25

    .end local v25    # "sizeDownload":J
    .local v5, "sizeDownload":J
    move-object/from16 v17, v2

    .line 664
    .end local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :goto_1
    :try_start_2
    invoke-virtual/range {v15 .. v16}, Ljava/io/InputStream;->read([B)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v22

    if-gtz v22, :cond_3

    .line 705
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->clearFileOutpStream(Ljava/io/FileOutputStream;)V

    .line 707
    const-string/jumbo v7, "UpdateManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Download complete."

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    invoke-virtual {v9}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V

    .line 711
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;

    .line 712
    const-wide/16 v12, 0x64

    move-object v7, v2

    move-wide v8, v3

    move-wide v10, v3

    .line 711
    invoke-direct/range {v7 .. v13}, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;-><init>(JJJ)V

    .line 713
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    const/4 v7, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 717
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 665
    .end local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :cond_3
    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mIsCancel:Z

    if-eqz v7, :cond_4

    .line 666
    const-string/jumbo v7, "UpdateManager"

    const-string/jumbo v8, "Cancel Download"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V

    .line 670
    const/4 v7, 0x2

    .line 671
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_CANCEL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 672
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 669
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 673
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 676
    :cond_4
    const/4 v7, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v14, v0, v7, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 678
    move/from16 v0, v22

    int-to-long v7, v0

    add-long/2addr v5, v7

    .line 679
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;

    .line 680
    const-wide/16 v7, 0x64

    mul-long/2addr v7, v5

    div-long/2addr v7, v3

    long-to-int v7, v7

    int-to-long v7, v7

    .line 679
    invoke-direct/range {v2 .. v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;-><init>(JJJ)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 683
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    const/4 v7, 0x3

    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-object/from16 v17, v2

    .end local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    goto/16 :goto_1

    .line 687
    .end local v5    # "sizeDownload":J
    .end local v15    # "InStream":Ljava/io/InputStream;
    .end local v16    # "buffer":[B
    .end local v22    # "len1":I
    .end local v24    # "response":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v18

    move-object/from16 v2, v17

    .line 688
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .local v18, "e":Ljava/io/IOException;
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->clearFileOutpStream(Ljava/io/FileOutputStream;)V

    .line 689
    const-string/jumbo v7, "UpdateManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Download fail IOException - "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 691
    const/4 v7, 0x2

    .line 692
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 693
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 691
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 694
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 695
    .end local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .end local v18    # "e":Ljava/io/IOException;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :catch_1
    move-exception v18

    move-object/from16 v2, v17

    .line 696
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v2    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .local v18, "e":Ljava/lang/Exception;
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->clearFileOutpStream(Ljava/io/FileOutputStream;)V

    .line 697
    const-string/jumbo v7, "UpdateManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Download fail Exception - "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    .line 699
    const/4 v7, 0x2

    .line 700
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 701
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 699
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 702
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 695
    .end local v18    # "e":Ljava/lang/Exception;
    .restart local v15    # "InStream":Ljava/io/InputStream;
    .restart local v16    # "buffer":[B
    .restart local v22    # "len1":I
    .restart local v24    # "response":Lorg/apache/http/HttpResponse;
    :catch_2
    move-exception v18

    goto :goto_3

    .line 687
    :catch_3
    move-exception v18

    goto :goto_2
.end method

.method private requestDownloadInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadURLStr"    # Ljava/lang/String;

    .prologue
    .line 325
    const/4 v5, 0x0

    .line 328
    .local v5, "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v8

    .line 329
    .local v8, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v8}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v13

    .line 331
    .local v13, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v4, Ljava/net/URI;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 332
    .local v4, "Uri":Ljava/net/URI;
    new-instance v10, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v10}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 334
    .local v10, "httpclient":Lorg/apache/http/client/HttpClient;
    invoke-interface {v10}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v12

    .line 336
    .local v12, "params":Lorg/apache/http/params/HttpParams;
    const/16 v19, 0x2710

    .line 335
    move/from16 v0, v19

    invoke-static {v12, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 338
    const/16 v19, 0x7530

    .line 337
    move/from16 v0, v19

    invoke-static {v12, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 340
    new-instance v9, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v9, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 343
    .local v9, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    invoke-interface {v10, v9}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v16

    .line 344
    .local v16, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 355
    .local v3, "InStream":Ljava/io/InputStream;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v13, v3, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 357
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v14

    .line 360
    .local v14, "parserEvent":I
    const-string/jumbo v11, ""

    .local v11, "id":Ljava/lang/String;
    const-string/jumbo v15, ""

    .line 361
    .local v15, "res":Ljava/lang/String;
    const-string/jumbo v2, ""
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_4

    .local v2, "DownloadURI":Ljava/lang/String;
    move-object v6, v5

    .line 363
    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .local v6, "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :goto_0
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v14, v0, :cond_0

    move-object v5, v6

    .line 437
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "InStream":Ljava/io/InputStream;
    .end local v4    # "Uri":Ljava/net/URI;
    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .end local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v11    # "id":Ljava/lang/String;
    .end local v12    # "params":Lorg/apache/http/params/HttpParams;
    .end local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v14    # "parserEvent":I
    .end local v15    # "res":Ljava/lang/String;
    .end local v16    # "response":Lorg/apache/http/HttpResponse;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :goto_1
    return-object v5

    .line 364
    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v2    # "DownloadURI":Ljava/lang/String;
    .restart local v3    # "InStream":Ljava/io/InputStream;
    .restart local v4    # "Uri":Ljava/net/URI;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v9    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v11    # "id":Ljava/lang/String;
    .restart local v12    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v14    # "parserEvent":I
    .restart local v15    # "res":Ljava/lang/String;
    .restart local v16    # "response":Lorg/apache/http/HttpResponse;
    :cond_0
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ne v14, v0, :cond_5

    .line 365
    :try_start_1
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v17

    .line 366
    .local v17, "tag":Ljava/lang/String;
    const-string/jumbo v19, "appId"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 367
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v18

    .line 368
    .local v18, "type":I
    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 369
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v11

    .line 370
    const-string/jumbo v19, "UpdateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "appId : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    .end local v18    # "type":I
    :cond_1
    const-string/jumbo v19, "resultCode"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 374
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v18

    .line 375
    .restart local v18    # "type":I
    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 376
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v15

    .line 377
    const-string/jumbo v19, "UpdateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "StubDownload result : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    .end local v18    # "type":I
    :cond_2
    const-string/jumbo v19, "downloadURI"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 381
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v18

    .line 382
    .restart local v18    # "type":I
    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 383
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->trimCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 384
    const-string/jumbo v19, "UpdateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "downloadURI : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    .end local v18    # "type":I
    :cond_3
    const-string/jumbo v19, "versionName"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 388
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v18

    .line 389
    .restart local v18    # "type":I
    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 390
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v15

    .line 392
    :cond_4
    if-eqz v15, :cond_5

    .line 394
    sget-object v19, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_DOWNLOAD_VERSION:Ljava/lang/String;

    .line 393
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    .end local v17    # "tag":Ljava/lang/String;
    .end local v18    # "type":I
    :cond_5
    const/16 v19, 0x3

    move/from16 v0, v19

    if-ne v14, v0, :cond_7

    .line 400
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v17

    .line 401
    .restart local v17    # "tag":Ljava/lang/String;
    const-string/jumbo v19, "downloadURI"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 402
    const-string/jumbo v19, "46003"

    .line 403
    const-string/jumbo v20, "gsm.operator.numeric"

    const-string/jumbo v21, ""

    .line 402
    invoke-static/range {v20 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    .line 403
    if-eqz v19, :cond_6

    .line 404
    new-instance v5, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;

    .line 405
    const/16 v19, 0x1

    .line 404
    move/from16 v0, v19

    invoke-direct {v5, v2, v15, v11, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_5

    .line 411
    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :goto_2
    :try_start_2
    const-string/jumbo v11, ""

    .line 412
    const-string/jumbo v15, ""

    .line 416
    .end local v17    # "tag":Ljava/lang/String;
    :goto_3
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_4

    move-result v14

    move-object v6, v5

    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto/16 :goto_0

    .line 407
    .restart local v17    # "tag":Ljava/lang/String;
    :cond_6
    :try_start_3
    new-instance v5, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;

    .line 408
    const/16 v19, 0x0

    .line 407
    move/from16 v0, v19

    invoke-direct {v5, v2, v15, v11, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_5

    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto :goto_2

    .line 419
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "InStream":Ljava/io/InputStream;
    .end local v4    # "Uri":Ljava/net/URI;
    .end local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v11    # "id":Ljava/lang/String;
    .end local v12    # "params":Lorg/apache/http/params/HttpParams;
    .end local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v14    # "parserEvent":I
    .end local v15    # "res":Ljava/lang/String;
    .end local v16    # "response":Lorg/apache/http/HttpResponse;
    .end local v17    # "tag":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 420
    .local v7, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_4
    const-string/jumbo v19, "UpdateManager"

    .line 421
    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "CheckDownload_XmlPullParserException - "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 420
    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto/16 :goto_1

    .line 423
    .end local v7    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v7

    .line 424
    .local v7, "e":Ljava/net/SocketException;
    :goto_5
    const-string/jumbo v19, "UpdateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "CheckDownload_SocketException - "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    invoke-virtual {v7}, Ljava/net/SocketException;->printStackTrace()V

    goto/16 :goto_1

    .line 426
    .end local v7    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v7

    .line 427
    .local v7, "e":Ljava/net/UnknownHostException;
    :goto_6
    const-string/jumbo v19, "UpdateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "CheckDownload_UnknownHostException - "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    invoke-virtual {v7}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto/16 :goto_1

    .line 429
    .end local v7    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v7

    .line 430
    .local v7, "e":Ljava/io/IOException;
    :goto_7
    const-string/jumbo v19, "UpdateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "CheckDownload_IOException - "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 432
    .end local v7    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v7

    .line 433
    .local v7, "e":Ljava/net/URISyntaxException;
    :goto_8
    const-string/jumbo v19, "UpdateManager"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string/jumbo v21, "CheckDownload_URISyntaxException - "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    invoke-virtual {v7}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto/16 :goto_1

    .line 432
    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .end local v7    # "e":Ljava/net/URISyntaxException;
    .restart local v2    # "DownloadURI":Ljava/lang/String;
    .restart local v3    # "InStream":Ljava/io/InputStream;
    .restart local v4    # "Uri":Ljava/net/URI;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v8    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v9    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v11    # "id":Ljava/lang/String;
    .restart local v12    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v14    # "parserEvent":I
    .restart local v15    # "res":Ljava/lang/String;
    .restart local v16    # "response":Lorg/apache/http/HttpResponse;
    :catch_5
    move-exception v7

    move-object v5, v6

    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto :goto_8

    .line 429
    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :catch_6
    move-exception v7

    move-object v5, v6

    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto :goto_7

    .line 426
    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :catch_7
    move-exception v7

    move-object v5, v6

    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto :goto_6

    .line 423
    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :catch_8
    move-exception v7

    move-object v5, v6

    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto/16 :goto_5

    .line 419
    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :catch_9
    move-exception v7

    move-object v5, v6

    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto/16 :goto_4

    .end local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    :cond_7
    move-object v5, v6

    .end local v6    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    .restart local v5    # "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    goto/16 :goto_3
.end method

.method private requestDownloadNoProxy(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;)Z
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;

    .prologue
    .line 473
    const-string/jumbo v6, "UpdateManager"

    const-string/jumbo v7, "downloadApkNoProxy"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->getFile()Ljava/io/File;

    move-result-object v19

    .line 477
    .local v19, "file":Ljava/io/File;
    const/4 v14, 0x0

    .line 478
    .local v14, "InStream":Ljava/io/InputStream;
    const/4 v13, 0x0

    .line 479
    .local v13, "Fout":Ljava/io/FileOutputStream;
    new-instance v21, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct/range {v21 .. v21}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 481
    .local v21, "httpclient":Lorg/apache/http/client/HttpClient;
    const/16 v26, 0x78

    .line 482
    .local v26, "timeout":I
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v20

    .line 483
    .local v20, "httpParams":Lorg/apache/http/params/HttpParams;
    const v6, 0x1d4c0

    move-object/from16 v0, v20

    invoke-static {v0, v6}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 484
    const v6, 0x1d4c0

    move-object/from16 v0, v20

    invoke-static {v0, v6}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 486
    const-wide/16 v2, 0x0

    .line 487
    .local v2, "sizeTotal":J
    const/16 v17, 0x0

    .line 489
    .local v17, "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 490
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z

    .line 491
    const-string/jumbo v6, "UpdateManager"

    const-string/jumbo v7, "file exists. so delete."

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    :cond_0
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v24

    .line 497
    .local v24, "sizeDownload":J
    new-instance v27, Ljava/net/URL;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;->getDownloadURL()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-direct {v0, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 500
    .local v27, "url":Ljava/net/URL;
    sget-object v6, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v16

    .line 499
    check-cast v16, Ljava/net/HttpURLConnection;

    .line 501
    .local v16, "conn":Ljava/net/HttpURLConnection;
    const-string/jumbo v6, "UpdateManager"

    const-string/jumbo v7, "HttpURLConnection() : no proxy set"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    if-nez v16, :cond_3

    .line 504
    const-string/jumbo v6, "UpdateManager"

    const-string/jumbo v7, "conn is null!"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    const/4 v6, 0x2

    .line 506
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 507
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 505
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 579
    if-eqz v13, :cond_1

    .line 580
    :try_start_1
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 586
    :cond_1
    :goto_0
    if-eqz v14, :cond_2

    .line 587
    :try_start_2
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 508
    :cond_2
    :goto_1
    const/4 v6, 0x0

    move-object/from16 v1, v17

    .line 603
    .end local v16    # "conn":Ljava/net/HttpURLConnection;
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .end local v24    # "sizeDownload":J
    .end local v27    # "url":Ljava/net/URL;
    .local v1, "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :goto_2
    return v6

    .line 581
    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v16    # "conn":Ljava/net/HttpURLConnection;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v24    # "sizeDownload":J
    .restart local v27    # "url":Ljava/net/URL;
    :catch_0
    move-exception v18

    .line 583
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 588
    .end local v18    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v18

    .line 590
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 510
    .end local v18    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_3
    const-string/jumbo v6, "UpdateManager"

    const-string/jumbo v7, "conn is connected!"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    const v6, 0x1d4c0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 512
    const v6, 0x1d4c0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 513
    const-string/jumbo v6, "GET"

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 514
    const/4 v6, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 516
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v23

    .line 517
    .local v23, "response":I
    const-string/jumbo v6, "UpdateManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "response : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    .line 520
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v6

    int-to-long v6, v6

    add-long v2, v6, v24

    .line 523
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v6

    .line 524
    const v7, 0x8001

    .line 523
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v13

    .line 526
    const/16 v6, 0x400

    new-array v15, v6, [B

    .line 527
    .local v15, "buffer":[B
    const/16 v22, 0x0

    .line 529
    .local v22, "len1":I
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V

    .line 531
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;

    .line 532
    const-wide/16 v4, 0x0

    .line 533
    const-wide/16 v6, 0x0

    .line 531
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;-><init>(JJJ)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 534
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    const/4 v6, 0x3

    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_f
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_e
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-wide/from16 v4, v24

    .end local v24    # "sizeDownload":J
    .local v4, "sizeDownload":J
    move-object/from16 v17, v1

    .line 536
    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :goto_3
    :try_start_5
    invoke-virtual {v14, v15}, Ljava/io/InputStream;->read([B)I

    move-result v22

    if-gtz v22, :cond_7

    .line 556
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    .line 558
    if-eqz v16, :cond_4

    .line 559
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 579
    :cond_4
    if-eqz v13, :cond_5

    .line 580
    :try_start_6
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_c

    .line 586
    :cond_5
    :goto_4
    if-eqz v14, :cond_6

    .line 587
    :try_start_7
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_d

    .line 595
    :cond_6
    :goto_5
    const-string/jumbo v6, "UpdateManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Download complete."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v8}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    sget-object v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->setPreferenceBoolean(Ljava/lang/String;Z)V

    .line 599
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;

    .line 600
    const-wide/16 v11, 0x64

    move-object v6, v1

    move-wide v7, v2

    move-wide v9, v2

    .line 599
    invoke-direct/range {v6 .. v12}, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;-><init>(JJJ)V

    .line 601
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    const/4 v6, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 603
    const/4 v6, 0x1

    goto/16 :goto_2

    .line 537
    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :cond_7
    :try_start_8
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mIsCancel:Z

    if-eqz v6, :cond_a

    .line 538
    const-string/jumbo v6, "UpdateManager"

    const-string/jumbo v7, "mIsCancel = true, Cancel Download"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    .line 540
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 579
    if-eqz v13, :cond_8

    .line 580
    :try_start_9
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 586
    :cond_8
    :goto_6
    if-eqz v14, :cond_9

    .line 587
    :try_start_a
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 542
    :cond_9
    :goto_7
    const/4 v6, 0x0

    move-object/from16 v1, v17

    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    goto/16 :goto_2

    .line 581
    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :catch_2
    move-exception v18

    .line 583
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 588
    .end local v18    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v18

    .line 590
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 545
    .end local v18    # "e":Ljava/io/IOException;
    :cond_a
    const/4 v6, 0x0

    :try_start_b
    move/from16 v0, v22

    invoke-virtual {v13, v15, v6, v0}, Ljava/io/FileOutputStream;->write([BII)V

    .line 547
    move/from16 v0, v22

    int-to-long v6, v0

    add-long/2addr v4, v6

    .line 548
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;

    .line 549
    const-wide/16 v6, 0x64

    mul-long/2addr v6, v4

    div-long/2addr v6, v2

    long-to-int v6, v6

    int-to-long v6, v6

    .line 548
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;-><init>(JJJ)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 552
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    const/4 v6, 0x3

    :try_start_c
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_f
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_e
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-object/from16 v17, v1

    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    goto/16 :goto_3

    .line 561
    .end local v4    # "sizeDownload":J
    .end local v15    # "buffer":[B
    .end local v16    # "conn":Ljava/net/HttpURLConnection;
    .end local v22    # "len1":I
    .end local v23    # "response":I
    .end local v27    # "url":Ljava/net/URL;
    :catch_4
    move-exception v18

    move-object/from16 v1, v17

    .line 562
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v18    # "e":Ljava/io/IOException;
    :goto_8
    :try_start_d
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->clearFileOutpStream(Ljava/io/FileOutputStream;)V

    .line 563
    const-string/jumbo v6, "UpdateManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Download fail IOException - "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 565
    const/4 v6, 0x2

    .line 566
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 567
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 565
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 579
    if-eqz v13, :cond_b

    .line 580
    :try_start_e
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    .line 586
    :cond_b
    :goto_9
    if-eqz v14, :cond_c

    .line 587
    :try_start_f
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    .line 568
    :cond_c
    :goto_a
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 581
    :catch_5
    move-exception v18

    .line 583
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 588
    :catch_6
    move-exception v18

    .line 590
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 569
    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .end local v18    # "e":Ljava/io/IOException;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :catch_7
    move-exception v18

    move-object/from16 v1, v17

    .line 570
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .local v18, "e":Ljava/lang/Exception;
    :goto_b
    :try_start_10
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->clearFileOutpStream(Ljava/io/FileOutputStream;)V

    .line 571
    const-string/jumbo v6, "UpdateManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Download fail Exception - "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    .line 573
    const/4 v6, 0x2

    .line 574
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->DOWNLOAD_FAIL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 575
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 573
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 579
    if-eqz v13, :cond_d

    .line 580
    :try_start_11
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_8

    .line 586
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_d
    :goto_c
    if-eqz v14, :cond_e

    .line 587
    :try_start_12
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_9

    .line 576
    :cond_e
    :goto_d
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 581
    .restart local v18    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v18

    .line 583
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 588
    .end local v18    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v18

    .line 590
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 577
    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .end local v18    # "e":Ljava/io/IOException;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :catchall_0
    move-exception v6

    move-object/from16 v1, v17

    .line 579
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :goto_e
    if-eqz v13, :cond_f

    .line 580
    :try_start_13
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_a

    .line 586
    :cond_f
    :goto_f
    if-eqz v14, :cond_10

    .line 587
    :try_start_14
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_b

    .line 593
    :cond_10
    :goto_10
    throw v6

    .line 581
    :catch_a
    move-exception v18

    .line 583
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 588
    .end local v18    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v18

    .line 590
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_10

    .line 581
    .end local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .end local v18    # "e":Ljava/io/IOException;
    .restart local v4    # "sizeDownload":J
    .restart local v15    # "buffer":[B
    .restart local v16    # "conn":Ljava/net/HttpURLConnection;
    .restart local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .restart local v22    # "len1":I
    .restart local v23    # "response":I
    .restart local v27    # "url":Ljava/net/URL;
    :catch_c
    move-exception v18

    .line 583
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 588
    .end local v18    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v18

    .line 590
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 577
    .end local v4    # "sizeDownload":J
    .end local v15    # "buffer":[B
    .end local v16    # "conn":Ljava/net/HttpURLConnection;
    .end local v17    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    .end local v18    # "e":Ljava/io/IOException;
    .end local v22    # "len1":I
    .end local v23    # "response":I
    .end local v27    # "url":Ljava/net/URL;
    .restart local v1    # "downloadData":Lcom/sec/android/automotive/drivelink/framework/manager/update/DownloadDataImpl;
    :catchall_1
    move-exception v6

    goto :goto_e

    .line 569
    .restart local v15    # "buffer":[B
    .restart local v16    # "conn":Ljava/net/HttpURLConnection;
    .restart local v22    # "len1":I
    .restart local v23    # "response":I
    .restart local v27    # "url":Ljava/net/URL;
    :catch_e
    move-exception v18

    goto :goto_b

    .line 561
    :catch_f
    move-exception v18

    goto/16 :goto_8
.end method

.method private sendDownloadProgress(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V
    .locals 1
    .param p1, "progress"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;

    .prologue
    .line 893
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isPossibleSendResult()Z

    move-result v0

    if-nez v0, :cond_0

    .line 898
    :goto_0
    return-void

    .line 896
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkUpdateListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    move-result-object v0

    .line 897
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;->onResponseDownloadProgress(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V

    goto :goto_0
.end method

.method private sendInstallStart()V
    .locals 2

    .prologue
    .line 909
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isPossibleSendResult()Z

    move-result v0

    if-nez v0, :cond_0

    .line 913
    :goto_0
    return-void

    .line 911
    :cond_0
    const-string/jumbo v0, "UpdateManager"

    const-string/jumbo v1, "sendInstallStart "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkUpdateListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;->onResponseInstallStarted()V

    goto :goto_0
.end method

.method private sendUpdateFailResult(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .prologue
    .line 885
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isPossibleSendResult()Z

    move-result v0

    if-nez v0, :cond_0

    .line 890
    :goto_0
    return-void

    .line 887
    :cond_0
    const-string/jumbo v0, "UpdateManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "sendDownloadFailResult "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkUpdateListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    move-result-object v0

    .line 889
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;->onResponseRequestUpdateApplicationFail(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V

    goto :goto_0
.end method

.method private sendUpdateSuccessResult()V
    .locals 2

    .prologue
    .line 901
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isPossibleSendResult()Z

    move-result v0

    if-nez v0, :cond_0

    .line 906
    :goto_0
    return-void

    .line 903
    :cond_0
    const-string/jumbo v0, "UpdateManager"

    const-string/jumbo v1, "sendDownloadSuccessResult "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkUpdateListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    move-result-object v0

    .line 905
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;->onResponseRequestUpdateApplicationSuccess()V

    goto :goto_0
.end method

.method private setInitializePrefernce()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 170
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 171
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "setPreferenceBoolean mContext = null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :goto_0
    return-void

    .line 175
    :cond_0
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "setInitializePrefernce"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    .line 178
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->PREFERENCE_NAME:Ljava/lang/String;

    .line 177
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 179
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 180
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_UPDATE_VERSION:Ljava/lang/String;

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 181
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_UPDATE_VERSION:Ljava/lang/String;

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 182
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_DOWNLOAD_VERSION:Ljava/lang/String;

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 183
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 184
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_INSTALL_RESULT:Ljava/lang/String;

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 185
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private setPreferenceBoolean(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 112
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 113
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "setPreferenceBoolean mContext = null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "setPreferenceBoolean"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    .line 120
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->PREFERENCE_NAME:Ljava/lang/String;

    const/4 v4, 0x0

    .line 119
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 121
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 122
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 123
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private setPreferenceString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 128
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "setPreferenceBoolean mContext = null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :goto_0
    return-void

    .line 132
    :cond_0
    const-string/jumbo v2, "UpdateManager"

    const-string/jumbo v3, "setPreferenceString"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    .line 135
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->PREFERENCE_NAME:Ljava/lang/String;

    const/4 v4, 0x0

    .line 134
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 136
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 137
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 138
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private trimCDATA(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "szSrc"    # Ljava/lang/String;

    .prologue
    .line 916
    const-string/jumbo v0, "<![CDATA["

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 917
    const-string/jumbo v0, "]]>"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 919
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancelUpdateApplication(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 259
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mIsCancel:Z

    .line 260
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->cancelUpdateVersionCheck(Landroid/content/Context;)V

    .line 261
    return-void
.end method

.method public cancelUpdateVersionCheck(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 264
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 267
    :cond_0
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckInputStream:Ljava/io/InputStream;

    .line 268
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mCheckhttpclient:Lorg/apache/http/client/HttpClient;

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCancelCheckUpdateCall:Z

    .line 272
    const-string/jumbo v0, "UpdateManager"

    .line 273
    const-string/jumbo v1, "cancelUpdateVersionCheck : connection close, httpClient&InputStream&flag initialize "

    .line 272
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    return-void
.end method

.method public isCheckUpdateVersion(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    .line 191
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->CHECK:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    .line 190
    invoke-static {p1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->makeURL(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;)Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "checkURLStr":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 194
    const/4 v1, 0x0

    .line 202
    :goto_0
    return v1

    .line 197
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->requestCheckUpdateVersion(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 200
    .local v1, "isUpdateVersion":Z
    const-string/jumbo v2, "UpdateManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "isCheckUpdateVersion "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateApplication(Landroid/content/Context;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isCheckVersion"    # Z

    .prologue
    const/4 v5, 0x2

    .line 206
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mContext:Landroid/content/Context;

    .line 208
    if-eqz p2, :cond_2

    .line 209
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->isCheckUpdateVersion(Landroid/content/Context;)Z

    move-result v3

    .line 211
    .local v3, "isUpdateVersion":Z
    if-nez v3, :cond_1

    .line 214
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_VERSION:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 215
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 212
    invoke-direct {p0, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    .line 256
    .end local v3    # "isUpdateVersion":Z
    :cond_0
    :goto_0
    return-void

    .line 219
    .restart local v3    # "isUpdateVersion":Z
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->printtestcode()V

    .line 223
    .end local v3    # "isUpdateVersion":Z
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mIsCancel:Z

    .line 226
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;->DONWLOAD:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;

    .line 225
    invoke-static {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant;->makeURL(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateConstant$URL_TYPE;)Ljava/lang/String;

    move-result-object v1

    .line 228
    .local v1, "downloadURLStr":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 230
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->WRONG_URL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 231
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 229
    invoke-direct {p0, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 235
    :cond_3
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->requestDownloadInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;

    move-result-object v0

    .line 237
    .local v0, "downloadInfo":Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;
    if-nez v0, :cond_4

    .line 239
    sget-object v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->NO_DOWNLOAD_INFO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;

    .line 240
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 238
    invoke-direct {p0, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->makeNSendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 244
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->printtestcode()V

    .line 246
    new-instance v4, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    const-string/jumbo v5, ".trancsli"

    invoke-direct {v4, p1, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    .line 247
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->mFileUtil:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateFileUtil;->prepare()Z

    .line 249
    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->requestDownload(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateData;)Z

    move-result v2

    .line 251
    .local v2, "isDownloadComplete":Z
    if-eqz v2, :cond_0

    .line 252
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->printtestcode()V

    .line 253
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;->installApplication(Landroid/content/Context;)V

    goto :goto_0
.end method
