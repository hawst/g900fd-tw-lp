.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationShareIgnoreInvitationDialogView.java"


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mViewLocal:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->mViewLocal:Landroid/view/View;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->mViewLocal:Landroid/view/View;

    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method private getOptionBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 94
    .local v0, "maskedBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 95
    const v2, 0x7f020204

    .line 94
    invoke-static {v1, p2, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 98
    const v2, 0x7f020205

    .line 97
    invoke-static {v1, v0, v2}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 64
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 63
    check-cast v0, Landroid/view/LayoutInflater;

    .line 66
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_0

    .line 69
    const v3, 0x7f0300ad

    .line 68
    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 67
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->mViewLocal:Landroid/view/View;

    .line 71
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902e6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->mImageView:Landroid/widget/ImageView;

    .line 73
    const v3, 0x7f020206

    .line 72
    invoke-direct {p0, p1, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->getOptionBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 74
    .local v1, "overlayedBitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 76
    const v4, 0x7f0a061f

    .line 75
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "prompt":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 80
    .end local v1    # "overlayedBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "prompt":Ljava/lang/String;
    :cond_0
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationShareIgnoreInvitationDialogView;)V

    .line 86
    const-wide/16 v5, 0x1388

    .line 80
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 88
    return-void
.end method
