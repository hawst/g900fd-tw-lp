.class public Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;
.super Ljava/lang/Object;
.source "NotificationHomeListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;
    }
.end annotation


# static fields
.field private static volatile _instance:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;


# instance fields
.field mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;
    .locals 2

    .prologue
    .line 14
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    .line 14
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    return-object v0

    .line 14
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getListener()Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;
    .locals 2

    .prologue
    .line 26
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    monitor-enter v1

    .line 27
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->_instance:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public registerListener(Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;

    .line 38
    return-void
.end method

.method public unregisterListener()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener;->mListener:Lcom/sec/android/automotive/drivelink/notification/NotificationHomeListener$OnHomeNotiListener;

    .line 42
    return-void
.end method
