.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationCommonBaseView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[NotificationCommonBaseView]"


# instance fields
.field mButton1:Landroid/widget/Button;

.field mButton2:Landroid/widget/Button;

.field mButton3:Landroid/widget/Button;

.field mContext:Landroid/content/Context;

.field mIvTitleIcon:Landroid/widget/ImageView;

.field mLayoutBase:Landroid/widget/LinearLayout;

.field mLayoutButton:Landroid/widget/LinearLayout;

.field mLayoutContent:Landroid/widget/LinearLayout;

.field mLayoutTitle:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mContext:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutBase:Landroid/widget/LinearLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    .line 52
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->initNotification(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mContext:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutBase:Landroid/widget/LinearLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->initNotification(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mContext:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutBase:Landroid/widget/LinearLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->initNotification(Landroid/content/Context;)V

    .line 41
    return-void
.end method


# virtual methods
.method public addButton(ILandroid/view/View$OnClickListener;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
    .locals 4
    .param p1, "resouceId"    # I
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "invisibleButton":Landroid/widget/Button;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 132
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 133
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 134
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 138
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 142
    :cond_1
    return-object p0

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 128
    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    goto :goto_0
.end method

.method public addButton(ILandroid/view/View$OnClickListener;Z)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
    .locals 4
    .param p1, "resouceId"    # I
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p3, "clickable"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "invisibleButton":Landroid/widget/Button;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 151
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 158
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 159
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 160
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 162
    invoke-virtual {v0, p3}, Landroid/widget/Button;->setClickable(Z)V

    .line 164
    if-nez p3, :cond_1

    .line 165
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 169
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 173
    :cond_2
    return-object p0

    .line 152
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_4

    .line 153
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 154
    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    goto :goto_0
.end method

.method public addButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 179
    if-nez p1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-object p0

    .line 183
    :cond_1
    const/4 v0, 0x0

    .line 184
    .local v0, "invisibleButton":Landroid/widget/Button;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 192
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 194
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 197
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 198
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 186
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_4

    .line 187
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 188
    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 189
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    goto :goto_1
.end method

.method protected addButton(Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 208
    if-nez p1, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-object p0

    .line 212
    :cond_1
    const/4 v0, 0x0

    .line 213
    .local v0, "invisibleButton":Landroid/widget/Button;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 214
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 221
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 223
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    invoke-virtual {v0, p3}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 225
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 228
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 215
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_4

    .line 216
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 217
    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 218
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    goto :goto_1
.end method

.method protected getBaseTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 237
    const-string/jumbo v0, ""

    .line 238
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutBase:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutBase:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "tag":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 241
    .restart local v0    # "tag":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method protected initNotification(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    const-string/jumbo v3, "[NotificationCommonBaseView]"

    const-string/jumbo v4, "Context is null!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :goto_0
    return-void

    .line 60
    :cond_0
    const-string/jumbo v3, "[NotificationCommonBaseView]"

    const-string/jumbo v4, "initNotification"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mContext:Landroid/content/Context;

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 65
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 64
    check-cast v0, Landroid/view/LayoutInflater;

    .line 67
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 68
    .local v1, "layout":I
    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->isMultiMode:Z

    if-eqz v3, :cond_1

    .line 69
    const v1, 0x7f03009e

    .line 74
    :goto_1
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 76
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0902a8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutBase:Landroid/widget/LinearLayout;

    .line 77
    const v3, 0x7f0902a9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    .line 78
    const v3, 0x7f0902aa

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    .line 79
    const v3, 0x7f0902ab

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutContent:Landroid/widget/LinearLayout;

    .line 80
    const v3, 0x7f0902ac

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutButton:Landroid/widget/LinearLayout;

    .line 81
    const v3, 0x7f0902ad

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton1:Landroid/widget/Button;

    .line 82
    const v3, 0x7f0902ae

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton2:Landroid/widget/Button;

    .line 83
    const v3, 0x7f0902af

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mButton3:Landroid/widget/Button;

    goto :goto_0

    .line 71
    .end local v2    # "view":Landroid/view/View;
    :cond_1
    const v1, 0x7f03009d

    goto :goto_1
.end method

.method protected setIcon(I)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
    .locals 3
    .param p1, "resourceId"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 96
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    :cond_1
    return-object p0
.end method

.method protected setIcon(Landroid/graphics/drawable/Drawable;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;
    .locals 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 103
    if-nez p1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object p0

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 110
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mIvTitleIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationCommonBaseView;->mLayoutTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
