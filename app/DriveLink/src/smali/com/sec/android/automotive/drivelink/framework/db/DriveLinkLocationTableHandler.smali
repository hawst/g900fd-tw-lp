.class public Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;
.super Ljava/lang/Object;
.source "DriveLinkLocationTableHandler.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkBaseTableHandler;


# static fields
.field public static final HOME_ID:Ljava/lang/String; = "MYPLACE.HOME"

.field public static final OFFICE_ID:Ljava/lang/String; = "MYPLACE.OFFICE"


# instance fields
.field private mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

.field private mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

.field private mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

.field private mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

.field private mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

.field private mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

.field private mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

.field private mPlaceOnProfileTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

.field private mRecentlySearchedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnProfileTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

    .line 30
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    .line 31
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    .line 32
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    .line 33
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    .line 34
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    .line 35
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mRecentlySearchedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;

    .line 36
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    .line 37
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    .line 40
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnProfileTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

    .line 41
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;-><init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    .line 42
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    .line 43
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;-><init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    .line 44
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;-><init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    .line 45
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    .line 46
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mRecentlySearchedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;

    .line 47
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    .line 48
    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;-><init>(Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;)V

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    .line 49
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    .line 50
    return-void
.end method

.method private createLocationTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->createLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->createLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnProfileTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;->createPlaceOnProfileTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->createPlaceOnGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->createPlaceOnParticipantsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->createParticipantTrackingTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->createMyPlacesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;->createNavigationAvailableTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 118
    return-void
.end method

.method private dropLocationTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->dropLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->dropLocationsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->dropPlaceOnGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->dropPlaceOnParticipantsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->dropParticipantTrackingTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->dropMyPlacesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;->dropNavigationAvailableTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 130
    return-void
.end method


# virtual methods
.method public addFriendsToGroupShared(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 197
    .local p2, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    .line 198
    invoke-virtual {p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getGroupId()I

    move-result v1

    .line 197
    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->saveParticipants(Landroid/database/sqlite/SQLiteDatabase;ILjava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public changeGroupDestination(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .param p3, "destination"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->changeGroupDestination(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    return v0
.end method

.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->createLocationTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 95
    return-void
.end method

.method public deleteAllGroupShared(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->deleteAllGroups(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    return v0
.end method

.method public dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->dropLocationTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 100
    return-void
.end method

.method public getAllParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;->getAllParticipantTracking(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public getGroubById(Landroid/database/sqlite/SQLiteDatabase;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->getGroupById(Landroid/database/sqlite/SQLiteDatabase;I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v0

    return-object v0
.end method

.method public getLocation(Landroid/database/sqlite/SQLiteDatabase;DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "lat"    # D
    .param p4, "lng"    # D

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->getLocation(Landroid/database/sqlite/SQLiteDatabase;DD)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v0

    return-object v0
.end method

.method public getLocationFriendSharedTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    return-object v0
.end method

.method public getLocationGroup(Landroid/database/sqlite/SQLiteDatabase;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->getLocationGroup(Landroid/database/sqlite/SQLiteDatabase;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-result-object v0

    return-object v0
.end method

.method public getLocationTrackingTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    return-object v0
.end method

.method public getLocationsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    return-object v0
.end method

.method public getMyPlacesTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    return-object v0
.end method

.method public getNavigationAvailableTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    return-object v0
.end method

.method public getParticipantTrackingTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mParticipantTrackingTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantTrackingTableHandler;

    return-object v0
.end method

.method public getPlaceOnGroupTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    return-object v0
.end method

.method public getPlaceOnParticipantsTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    return-object v0
.end method

.method public getPlaceOnProfileTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnProfileTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

    return-object v0
.end method

.method public getRecentlySearchedTableHandler()Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mRecentlySearchedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;

    return-object v0
.end method

.method public getUserProfileByPhoneNumber(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnProfileTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;->getPlaceOnProfileByPhoneNumber(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    move-result-object v0

    return-object v0
.end method

.method public insertNavigationPackage(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "mCountry"    # Ljava/lang/String;
    .param p3, "mSaleCode"    # Ljava/lang/String;
    .param p4, "mNavigation"    # Ljava/lang/String;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;->insertNavigationPackage(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public loadLocationFriendPendingList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->loadLocationFriendPendingList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public loadLocationFriendSharedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->loadLocationFriendSharedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public loadLocationGroupList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromExpiretime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->loadLocationGroupList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public loadLocationMyPlaceList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->loadLocationMyPlaceList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public loadLocationSearchedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "maxCount"    # I
    .param p3, "fromTimeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mRecentlySearchedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/framework/db/location/RecentlyLocationSearchedTableHandler;->loadLocationSearchedList(Landroid/database/sqlite/SQLiteDatabase;IJ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public loadNavigationPackage(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "mCountry"    # Ljava/lang/String;
    .param p3, "mSaleCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mNavigationAvailableTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/location/NavigationAvailableTableHandler;->loadNavigationPackage(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public saveGroupParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "groupId"    # I
    .param p3, "participant"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnParticipantsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/db/location/ParticipantsTableHandler;->saveParticipant(Landroid/database/sqlite/SQLiteDatabase;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    return v0
.end method

.method public saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationsTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationsTableHandler;->saveLocation(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z

    move-result v0

    return v0
.end method

.method public saveLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mLocationFriendSharedTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/LocationFriendSharedTableHandler;->saveLocationFriendShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)Z

    move-result v0

    return v0
.end method

.method public saveLocationGroupShared(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnGroupTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/GroupTableHandler;->saveGroup(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    return v0
.end method

.method public saveMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "place"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mMyPlacesTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/MyPlacesTableHandler;->saveMyPlace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;)Z

    move-result v0

    return v0
.end method

.method public saveUserProfile(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/db/DriveLinkLocationTableHandler;->mPlaceOnProfileTableHandler:Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/db/location/ProfileTableHandler;->saveUserProfile(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)Z

    move-result v0

    return v0
.end method
