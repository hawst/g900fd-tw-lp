.class public Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;
.super Lcom/amap/api/maps/SupportMapFragment;
.source "AutoNaviMapFragment.java"


# instance fields
.field private mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/amap/api/maps/SupportMapFragment;-><init>()V

    .line 12
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->setAdapter(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;)V

    .line 13
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 17
    invoke-super {p0, p1}, Lcom/amap/api/maps/SupportMapFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 18
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->setRetainInstance(Z)V

    .line 20
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->setFragmentLoaded(Z)V

    .line 21
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->setMap(Lcom/amap/api/maps/AMap;)V

    .line 23
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->setOnMarkerDragListener(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;->getMapCompletedListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;->onMapCreated()V

    .line 29
    return-void
.end method

.method protected setAdapter(Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapFragment;->mAdapter:Lcom/sec/android/automotive/drivelink/location/map/view/adapters/AutoNaviMapAdapter;

    .line 33
    return-void
.end method
