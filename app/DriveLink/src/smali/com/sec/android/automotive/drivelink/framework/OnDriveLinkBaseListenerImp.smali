.class public abstract Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;
.super Ljava/lang/Object;
.source "OnDriveLinkBaseListenerImp.java"


# static fields
.field protected static mListenerHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;->mListenerHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getListenerHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkBaseListenerImp;->mListenerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public abstract setDriveLinkListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;)V
.end method
