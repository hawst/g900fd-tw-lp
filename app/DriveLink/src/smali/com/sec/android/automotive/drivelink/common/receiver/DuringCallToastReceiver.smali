.class public Lcom/sec/android/automotive/drivelink/common/receiver/DuringCallToastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DuringCallToastReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isCheckDrivingMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;

    .line 22
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 21
    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/DuringCallToastReceiver;->TAG:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/DuringCallToastReceiver;->isCheckDrivingMode:I

    .line 20
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 27
    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 29
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.block_launch_car_mode_in_call"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "car_mode_on"

    .line 31
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/DuringCallToastReceiver;->isCheckDrivingMode:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/DuringCallToastReceiver;->isCheckDrivingMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 42
    const v2, 0x7f0a026b

    .line 41
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    const/16 v2, 0xbb8

    .line 39
    invoke-static {p1, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 44
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/receiver/DuringCallToastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "make blocking call Toast"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
