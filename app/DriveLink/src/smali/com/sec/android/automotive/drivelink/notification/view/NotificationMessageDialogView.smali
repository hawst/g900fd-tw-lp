.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationMessageDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;,
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$iface$data$DLMSGInfo$MSG_TYPE:[I = null

.field public static final SMS_MODE_NOTI:I = 0x0

.field public static final SMS_MODE_READ:I = 0x1

.field static TAG:Ljava/lang/String; = null

.field static final TIME_OUT:I = 0x8

.field public static final TTS_MODE_NONE:I = 0x0

.field public static final TTS_MODE_OFF:I = 0x2

.field public static final TTS_MODE_ON:I = 0x1

.field private static isDebug:Z


# instance fields
.field TTS_SPEED_LATE:F

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBtIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mBtMMSCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mBtMMSIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field private mBtRead:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

.field protected mContext:Landroid/content/Context;

.field private mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

.field private mIsWiredHeadset:Z

.field private mMMS:Landroid/widget/ImageView;

.field private mMessage:Ljava/lang/String;

.field private mMode:I

.field private mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

.field private mName:Ljava/lang/String;

.field private mNameView:Landroid/widget/TextView;

.field private mNumber:Ljava/lang/String;

.field private mNumberView:Landroid/widget/TextView;

.field protected mReadDialogView:Landroid/view/View;

.field private mSMS:Landroid/widget/ImageView;

.field private mTTSMode:I

.field mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field mTTShandler:Landroid/os/Handler;

.field private mTime:J

.field protected mViewLocal:Landroid/view/View;

.field private mmsbtn:Landroid/widget/LinearLayout;

.field private smsbtn:Landroid/widget/LinearLayout;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$iface$data$DLMSGInfo$MSG_TYPE()[I
    .locals 3

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$iface$data$DLMSGInfo$MSG_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->values()[Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$iface$data$DLMSGInfo$MSG_TYPE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string/jumbo v0, "NotificationMessageDialogView"

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TAG:Ljava/lang/String;

    .line 904
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TTS_SPEED_LATE:F

    .line 62
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    .line 63
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 65
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    .line 66
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    .line 79
    iput v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I

    .line 81
    iput v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    .line 90
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mIsWiredHeadset:Z

    .line 729
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTShandler:Landroid/os/Handler;

    .line 766
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->initMesageView(Landroid/content/Context;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TTS_SPEED_LATE:F

    .line 62
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    .line 63
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 65
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    .line 66
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    .line 79
    iput v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I

    .line 81
    iput v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    .line 90
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mIsWiredHeadset:Z

    .line 729
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTShandler:Landroid/os/Handler;

    .line 766
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 103
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->initMesageView(Landroid/content/Context;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 108
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TTS_SPEED_LATE:F

    .line 62
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    .line 63
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 65
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    .line 66
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    .line 79
    iput v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I

    .line 81
    iput v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    .line 90
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mIsWiredHeadset:Z

    .line 729
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTShandler:Landroid/os/Handler;

    .line 766
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 110
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->initMesageView(Landroid/content/Context;)V

    .line 111
    return-void
.end method

.method private static Debug(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 907
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isDebug:Z

    if-eqz v0, :cond_0

    .line 908
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;I)V
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I

    return v0
.end method

.method static synthetic access$2(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 906
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    return-object v0
.end method

.method private getNotiMessageString(I)Ljava/lang/String;
    .locals 7
    .param p1, "mode"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 841
    const/4 v1, 0x0

    .line 843
    .local v1, "strRtn":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$framework$iface$data$DLMSGInfo$MSG_TYPE()[I

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 879
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "This is Unknow message !!! mMsgType = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 899
    :cond_0
    :goto_0
    return-object v1

    .line 845
    :pswitch_0
    if-nez p1, :cond_4

    .line 846
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 847
    .local v0, "strName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 849
    :cond_1
    const-string/jumbo v2, ""

    const-string/jumbo v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 854
    :cond_2
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "mName = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "strName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 856
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a012e

    new-array v4, v4, [Ljava/lang/Object;

    .line 857
    aput-object v0, v4, v6

    .line 856
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 858
    goto :goto_0

    .line 851
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 852
    const-string/jumbo v2, ""

    const-string/jumbo v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 858
    .end local v0    # "strName":Ljava/lang/String;
    :cond_4
    if-ne p1, v4, :cond_0

    .line 859
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a01cf

    new-array v4, v4, [Ljava/lang/Object;

    .line 860
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMessage:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 859
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 862
    goto :goto_0

    .line 864
    :pswitch_1
    if-nez p1, :cond_8

    .line 865
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 866
    .restart local v0    # "strName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_7

    .line 867
    :cond_5
    const-string/jumbo v2, ""

    const-string/jumbo v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 871
    :cond_6
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "mName = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "strName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 872
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a01af

    new-array v4, v4, [Ljava/lang/Object;

    .line 873
    aput-object v0, v4, v6

    .line 872
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 874
    goto/16 :goto_0

    .line 868
    :cond_7
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 869
    const-string/jumbo v2, ""

    const-string/jumbo v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 874
    .end local v0    # "strName":Ljava/lang/String;
    :cond_8
    if-ne p1, v4, :cond_0

    .line 875
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a01b3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 877
    goto/16 :goto_0

    .line 843
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initMesageView(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v4, 0x7f0902e5

    .line 146
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getCurrentItem()V

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 150
    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 149
    check-cast v1, Landroid/view/LayoutInflater;

    .line 151
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    if-nez v2, :cond_0

    .line 153
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isMultiMode"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 155
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    if-eqz v2, :cond_2

    .line 157
    const v2, 0x7f0300bc

    .line 156
    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 158
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 159
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;

    .line 158
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    .line 168
    :goto_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v3, 0x7f0902f0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mSMS:Landroid/widget/ImageView;

    .line 169
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v3, 0x7f0902f1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMMS:Landroid/widget/ImageView;

    .line 170
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v3, 0x7f0902de

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    .line 171
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v3, 0x7f0902f2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    .line 173
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 174
    const v3, 0x7f0902f4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 173
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtRead:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 175
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 176
    const v3, 0x7f0902e2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 175
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 177
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 178
    const v3, 0x7f0902f6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 177
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtMMSIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 179
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 180
    const v3, 0x7f0902f7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 179
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtMMSCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    .line 182
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtRead:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtMMSIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtMMSCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v2, p0}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v3, 0x7f0902f3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->smsbtn:Landroid/widget/LinearLayout;

    .line 190
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v3, 0x7f0902f5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mmsbtn:Landroid/widget/LinearLayout;

    .line 192
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setUIMsgInfo()V

    .line 195
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    .line 196
    const-string/jumbo v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 195
    check-cast v0, Landroid/media/AudioManager;

    .line 199
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mIsWiredHeadset:Z

    .line 203
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMessage:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 204
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMessage:Ljava/lang/String;

    .line 205
    :cond_1
    invoke-static {p1}, Lcom/vlingo/core/facade/CoreManager;->ttsEngine(Landroid/content/Context;)Lcom/vlingo/core/facade/tts/ITTSEngine;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TTS_SPEED_LATE:F

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/tts/ITTSEngine;->setTtsSpeechRate(F)V

    .line 207
    iget v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setCurrentMode(I)I

    .line 209
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setUseAutoTimeout(Z)V

    .line 212
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 216
    return-void

    .line 162
    :cond_2
    const v2, 0x7f0300bb

    .line 161
    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 163
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 164
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;

    .line 163
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    goto/16 :goto_0
.end method

.method private setCurrentMode(I)I
    .locals 12
    .param p1, "mode"    # I

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 531
    const/4 v7, 0x0

    .line 532
    .local v7, "ret":I
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 533
    const v1, 0x7f0902df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 532
    check-cast v9, Landroid/widget/RelativeLayout;

    .line 534
    .local v9, "rlRecv":Landroid/widget/RelativeLayout;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    .line 535
    const v1, 0x7f0902e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 534
    check-cast v8, Landroid/widget/RelativeLayout;

    .line 536
    .local v8, "rlRead":Landroid/widget/RelativeLayout;
    iput p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    .line 537
    packed-switch p1, :pswitch_data_0

    .line 559
    const-string/jumbo v0, "setCurrentMode default"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 563
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setCurrentItem(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;)V

    .line 565
    return v7

    .line 539
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v1, 0x7f0902de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    .line 540
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mViewLocal:Landroid/view/View;

    const v1, 0x7f0902f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    .line 541
    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 542
    invoke-virtual {v8, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 545
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setMessageReadStatusToRead(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 546
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;

    .line 548
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    .line 549
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 547
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;->initMember(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    :goto_1
    invoke-virtual {v9, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 556
    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;

    .line 552
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 553
    iget-wide v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTime:J

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMessage:Ljava/lang/String;

    .line 551
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->initMember(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;Landroid/graphics/Bitmap;Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_1

    .line 537
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setMessageReadStatusToRead(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V
    .locals 2
    .param p1, "msg"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .prologue
    .line 511
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "setMessageReadStatusToRead"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    if-eqz p1, :cond_0

    .line 514
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setMessageStateToRead(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 516
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 524
    :goto_0
    return-void

    .line 522
    :cond_0
    const-string/jumbo v0, "setMessageReadStatusToRead Msg == null"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startMessageNotiFlow(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x1

    .line 573
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 621
    :goto_0
    return-void

    .line 577
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "startMessageNotiFlow:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 579
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 581
    :pswitch_0
    const-string/jumbo v0, "DM_SMS_READBACK_NOTI"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setVoiceFlowId(Ljava/lang/String;)V

    .line 582
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 583
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mIsWiredHeadset:Z

    if-eqz v0, :cond_1

    .line 584
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 586
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiMessageString(I)Ljava/lang/String;

    move-result-object v1

    .line 587
    const-string/jumbo v2, "DM_SMS_READBACK_NOTI"

    .line 586
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 589
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiMessageString(I)Ljava/lang/String;

    move-result-object v1

    .line 590
    const-string/jumbo v2, "DM_SMS_READBACK_NOTI"

    .line 589
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    goto :goto_0

    .line 596
    :pswitch_1
    const-string/jumbo v0, "DM_SMS_READBACK_NOTI"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setVoiceFlowId(Ljava/lang/String;)V

    .line 597
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 599
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    if-ne v0, v1, :cond_2

    .line 600
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiMessageString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 601
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 602
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 603
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a01b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 604
    const-string/jumbo v2, "DM_SMS_READBACK_NOTI"

    .line 602
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 611
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 612
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a01b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 613
    const-string/jumbo v2, "DM_SMS_READBACK_NOTI"

    .line 611
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 616
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    goto/16 :goto_0

    .line 606
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiMessageString(I)Ljava/lang/String;

    move-result-object v1

    .line 607
    const-string/jumbo v2, "DM_SMS_READBACK_NOTI"

    .line 606
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 579
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method enableQuotesButton(Z)V
    .locals 1
    .param p1, "isVisable"    # Z

    .prologue
    .line 820
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtRead:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 821
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 822
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtMMSIgnore:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 823
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBtMMSCall:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setMarkShow(Z)V

    .line 825
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;

    .line 827
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;->setEnableQuotesbutton(Z)V

    .line 833
    :goto_0
    return-void

    .line 829
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;

    .line 830
    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->setEnableQuotesbutton(Z)V

    goto :goto_0
.end method

.method getCurrentItem()V
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getCurrentMode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getMsgType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMessage:Ljava/lang/String;

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTime:J

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBitmap:Landroid/graphics/Bitmap;

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    .line 137
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->getDlMessage()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    move-result-object v0

    .line 136
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "mName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " mNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 142
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 281
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 282
    .local v1, "id":I
    const-string/jumbo v3, "id"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 284
    sparse-switch v1, :sswitch_data_0

    .line 350
    :goto_0
    return-void

    .line 287
    :sswitch_0
    invoke-direct {p0, v6}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setCurrentMode(I)I

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 289
    iget v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->startMessageNotiFlow(I)V

    goto :goto_0

    .line 293
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->dismiss()V

    goto :goto_0

    .line 298
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->dismiss()V

    goto :goto_0

    .line 303
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->sendCall()V

    goto :goto_0

    .line 308
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->sendCall()V

    goto :goto_0

    .line 313
    :sswitch_5
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 314
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "Sound Mute true"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->sendReply()V

    goto :goto_0

    .line 320
    :sswitch_6
    iput v6, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    .line 322
    sget-boolean v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isDebug:Z

    if-eqz v3, :cond_0

    .line 323
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v2

    .line 324
    .local v2, "state":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 325
    .local v0, "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "TTs state = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " isIdel ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 326
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getNotiItem().getFlowState()e = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 326
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 331
    .end local v0    # "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .end local v2    # "state":Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;
    :cond_0
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "TTS MODE:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    iget v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSMode:I

    if-ne v3, v6, :cond_1

    .line 333
    sget-object v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "TTS MODE ON!!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 336
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    goto/16 :goto_0

    .line 340
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->smsReadTTS()V

    goto/16 :goto_0

    .line 284
    :sswitch_data_0
    .sparse-switch
        0x7f0902e2 -> :sswitch_1
        0x7f0902f4 -> :sswitch_0
        0x7f0902f6 -> :sswitch_2
        0x7f0902f7 -> :sswitch_3
        0x7f0902fa -> :sswitch_6
        0x7f0902fb -> :sswitch_5
        0x7f0902fc -> :sswitch_4
    .end sparse-switch
.end method

.method public onFlowCommandCall(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onSendFlowCommandCall mMode = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 369
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 370
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->sendCall()V

    .line 378
    :goto_0
    return-void

    .line 375
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    goto :goto_0
.end method

.method public onFlowCommandCancel(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onSendFlowCommandCancel mMode = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 402
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->dismiss()V

    .line 404
    return-void
.end method

.method public onFlowCommandNo(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onFlowCommandNo mMode = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 418
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->onFlowCommandCancel(Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method public onFlowCommandRead(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onSendFlowCommandRead mMode = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 383
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    if-nez v0, :cond_0

    .line 384
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setCurrentMode(I)I

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 386
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->startMessageNotiFlow(I)V

    .line 392
    :cond_0
    return-void
.end method

.method public onFlowCommandReply(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onSendFlowCommandReply mMode ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 355
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->sendReply()V

    .line 363
    :goto_0
    return-void

    .line 359
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    goto :goto_0
.end method

.method public onFlowCommandYes(Ljava/lang/String;)V
    .locals 2
    .param p1, "flowID"    # Ljava/lang/String;

    .prologue
    .line 408
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onFlowCommandYes mMode = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->cancelNotiFlow()V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->dismiss()V

    .line 413
    :cond_0
    return-void
.end method

.method public onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v2, 0x0

    .line 424
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 440
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onMicStateChanged(Landroid/view/View;Lcom/nuance/sample/MicState;)V

    .line 441
    return-void

    .line 426
    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->enableQuotesButton(Z)V

    goto :goto_0

    .line 430
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->enableQuotesButton(Z)V

    goto :goto_0

    .line 435
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->enableQuotesButton(Z)V

    goto :goto_0

    .line 424
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotiCommandFinished(I)V
    .locals 0
    .param p1, "cmd"    # I

    .prologue
    .line 264
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotiCommandFinished(I)V

    .line 265
    return-void
.end method

.method public onNotificationDisappeared()V
    .locals 4

    .prologue
    .line 222
    const-string/jumbo v1, "onNotificationDisappeared"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 225
    const-string/jumbo v1, "DM_SMS_READBACK_NOTI"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->removeFlowList(Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 228
    const-string/jumbo v2, "PREF_SETTINGS_READOUT_SPEED"

    const/4 v3, 0x2

    .line 227
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 229
    .local v0, "speed":I
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->setTtsSpeed(I)V

    .line 231
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationDisappeared()V

    .line 232
    return-void
.end method

.method public onNotificationWillAppear()V
    .locals 2

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_1

    .line 245
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->restartNotiFlow()V

    .line 251
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationWillAppear()V

    .line 252
    return-void

    .line 246
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isAppeared()Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->startMessageNotiFlow(I)V

    .line 248
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->setAppeared(Z)V

    goto :goto_0
.end method

.method sendCall()V
    .locals 3

    .prologue
    .line 650
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 652
    .local v1, "displayName":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    .line 653
    .local v0, "cmdCall":Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandCall;->setDisplayName(Ljava/lang/String;)V

    .line 654
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setSoundMuteTure()V

    .line 655
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->dismiss(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 657
    return-void
.end method

.method sendReply()V
    .locals 2

    .prologue
    .line 664
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V

    .line 665
    .local v0, "cmdReply":Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$NotiCommandReply;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setSoundMuteTure()V

    .line 666
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->dismiss(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 668
    return-void
.end method

.method setCurrentItem(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;

    .prologue
    .line 114
    if-eqz p1, :cond_0

    .line 116
    iget v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setCurrentMode(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setMsgType(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setMessage(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setName(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setNumber(Ljava/lang/String;)V

    .line 121
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTime:J

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setTime(J)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mDlMessage:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageItem;->setDlMessage(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 125
    :cond_0
    return-void
.end method

.method public setReadoutFlag(Landroid/content/Context;Z)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flag"    # Z

    .prologue
    .line 275
    const/4 p2, 0x1

    .line 276
    invoke-super {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->setReadoutFlag(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public setUIMsgInfo()V
    .locals 6

    .prologue
    const v5, 0x7f0a03a4

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 445
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumber:Ljava/lang/String;

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "strNumber":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 457
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 466
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 467
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 474
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMsgType:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo$MSG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 475
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mSMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 476
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 478
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->smsbtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 479
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mmsbtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 481
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    if-eqz v1, :cond_3

    .line 482
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;

    .line 483
    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;->setTTSVisable(I)V

    .line 504
    :goto_2
    return-void

    .line 459
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 460
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 462
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 470
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mNumberView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 485
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;

    .line 486
    invoke-virtual {v1, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->setTTSVisable(I)V

    goto :goto_2

    .line 489
    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mSMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 490
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMMS:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 492
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->smsbtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 493
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mmsbtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 495
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->isMultiMode:Z

    if-eqz v1, :cond_5

    .line 496
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;

    .line 497
    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadView;->setTTSVisable(I)V

    goto :goto_2

    .line 499
    :cond_5
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mReadDialogView:Landroid/view/View;

    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;

    .line 500
    invoke-virtual {v1, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageReadFullView;->setTTSVisable(I)V

    goto :goto_2
.end method

.method smsReadTTS()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 628
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 630
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiMessageString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 631
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 632
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 633
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a01b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 634
    const-string/jumbo v2, "DM_SMS_READBACK_NOTI"

    .line 632
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 636
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    .line 637
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a01b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 638
    const-string/jumbo v2, "DM_SMS_READBACK_NOTI"

    .line 636
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTSWithFlowId(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 641
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 642
    return-void
.end method
