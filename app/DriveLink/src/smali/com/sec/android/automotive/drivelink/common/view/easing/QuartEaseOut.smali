.class public Lcom/sec/android/automotive/drivelink/common/view/easing/QuartEaseOut;
.super Ljava/lang/Object;
.source "QuartEaseOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/QuartEaseOut;->s:F

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/QuartEaseOut;->s:F

    .line 15
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3
    .param p1, "input"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 18
    const/high16 v0, -0x40800000    # -1.0f

    div-float v1, p1, v2

    sub-float p1, v1, v2

    mul-float v1, p1, p1

    mul-float/2addr v1, p1

    mul-float/2addr v1, p1

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    const/4 v1, 0x0

    add-float/2addr v0, v1

    return v0
.end method
