.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestBluetoothMakeDeviceDiscoverable.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    .line 15
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 17
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;->mContext:Landroid/content/Context;

    .line 18
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    move-result-object v0

    .line 25
    .local v0, "connectivityManager":Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->makeBluetoothDeviceDiscoverable(Landroid/content/Context;)Z

    .line 26
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestBluetoothMakeDeviceDiscoverable;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkConnectivityListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;

    move-result-object v0

    .line 33
    .local v0, "connectivityListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;
    if-eqz v0, :cond_0

    .line 35
    invoke-interface {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;->onResponseRequestBluetoothMakeDeviceDiscoverable()V

    .line 37
    :cond_0
    return-void
.end method
