.class Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;
.super Ljava/lang/Object;
.source "VoiceLocationActionBarLayout.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 256
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 252
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    const/4 v2, 0x0

    .line 238
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnSearchClear:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$5(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnSearchClear:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$5(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTvSearchLocation:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$6(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnSearchClear:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$5(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->btnSearchClear:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$5(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$3;->this$0:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->mTvSearchLocation:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->access$6(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
