.class public abstract Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;
.source "RecommendationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager$WeightComparator;
    }
.end annotation


# static fields
.field protected static final CHECK_CALL_LOG_DURATION_HOUR:I = 0x41a0

.field protected static final CHECK_FREQUENT_ITEM_COUNT:I = 0x1e

.field protected static final CHECK_LOCATION_DESTINATION_ITEM_COUNT:I = 0x5

.field protected static final CHECK_LOCATION_SHARING_ITEM_COUNT:I = 0xa

.field protected static final CHECK_MISSED_CALL_DURATION_HOUR:I = 0x960

.field protected static final MAX_NUMBER_OF_FRIENDS_SHARED_ITEMS:I = 0x3

.field protected static final MAX_NUMBER_OF_FRIENDS_SHARED_ITEMS_PENDING:I = 0x1

.field protected static final MAX_NUMBER_OF_MY_PLACES_ITEMS:I = 0xc

.field protected static final MAX_NUMBER_OF_SCHEDULE_ITEMS:I = 0x2

.field protected static final TYPE_CALL:I = 0x3e8

.field protected static final TYPE_MESSAGE:I = 0x3e9


# instance fields
.field private mLogTag:Ljava/lang/String;

.field private mRecommendHandler:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

.field private mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendHandler:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 57
    const-string/jumbo v0, "Recommendation"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mLogTag:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private getMaxIndexToAdd(Ljava/util/ArrayList;I)I
    .locals 1
    .param p2, "mLimit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 512
    .local p1, "tmpList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 515
    .local v0, "mSize":I
    if-le v0, p2, :cond_0

    .line 516
    move v0, p2

    .line 519
    :cond_0
    return v0
.end method

.method private isNewContact(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;Ljava/util/ArrayList;)Z
    .locals 7
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 484
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v1, 0x0

    .line 486
    .local v1, "result":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 495
    :goto_1
    return v1

    .line 487
    :cond_0
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 489
    .local v2, "tempContact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    .line 490
    const/4 v1, 0x1

    .line 491
    goto :goto_1

    .line 486
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected addWeightToPhoneNumber(Ljava/lang/String;I)V
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "weight"    # I

    .prologue
    .line 94
    const-string/jumbo v2, "-"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "_phoneNumber":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 96
    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    .line 97
    .local v1, "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    if-nez v1, :cond_0

    .line 98
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    .end local v1    # "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    invoke-direct {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;-><init>(Ljava/lang/String;)V

    .line 101
    .restart local v1    # "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    :cond_0
    invoke-virtual {v1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->addWeight(I)V

    .line 102
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    return-void
.end method

.method protected addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "categoryName"    # Ljava/lang/String;
    .param p3, "weight"    # I

    .prologue
    .line 107
    const-string/jumbo v2, "-"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "_phoneNumber":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 109
    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    .line 110
    .local v1, "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    if-nez v1, :cond_0

    .line 111
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    .end local v1    # "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    invoke-direct {v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;-><init>(Ljava/lang/String;)V

    .line 114
    .restart local v1    # "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    :cond_0
    invoke-virtual {v1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->addWeight(Ljava/lang/String;I)V

    .line 115
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    return-void
.end method

.method protected checkCallLogList(Landroid/content/Context;III)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callOrMessage"    # I
    .param p3, "hour"    # I
    .param p4, "weight"    # I

    .prologue
    const/16 v6, 0x3e8

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v4

    invoke-virtual {v4, p1, p3, v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getCalllogList(Landroid/content/Context;II)Ljava/util/ArrayList;

    move-result-object v1

    .line 247
    .local v1, "callLogList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    if-nez v1, :cond_1

    .line 279
    :cond_0
    return-void

    .line 251
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 252
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 253
    .local v0, "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    const/4 v3, 0x0

    .line 254
    .local v3, "isProperData":Z
    if-ne p2, v6, :cond_5

    .line 255
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getLogType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    move-result-object v4

    .line 256
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_CALL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 257
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getLogType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    move-result-object v4

    .line 258
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_VIDEO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 259
    :cond_2
    const/4 v3, 0x1

    .line 274
    :cond_3
    :goto_1
    if-eqz v3, :cond_4

    .line 275
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "CallLog"

    invoke-virtual {p0, v4, v5, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    .line 251
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 262
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getLogType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    move-result-object v4

    .line 263
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 264
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getLogType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    move-result-object v4

    .line 265
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 267
    :cond_6
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    move-result-object v4

    .line 268
    sget-object v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->OUTGOING_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 267
    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 268
    if-eqz v4, :cond_3

    .line 269
    const/4 v3, 0x1

    goto :goto_1
.end method

.method protected checkDLCallLogList(Landroid/content/Context;II)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "logType"    # I
    .param p3, "weight"    # I

    .prologue
    .line 282
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 283
    .local v2, "fromTime":Ljava/util/Calendar;
    const/4 v5, 0x2

    const/4 v6, -0x1

    invoke-virtual {v2, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 284
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-direct {v1, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 286
    .local v1, "fromDate":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    invoke-virtual {v5, p1, v1, p2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDLCallMessageLogList(Landroid/content/Context;Ljava/util/Date;I)Ljava/util/ArrayList;

    move-result-object v4

    .line 289
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    if-nez v4, :cond_1

    .line 299
    :cond_0
    return-void

    .line 293
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 294
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 296
    .local v0, "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "DLCallLog"

    invoke-virtual {p0, v5, v6, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    .line 293
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method protected checkFavoriteContactist(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "weight"    # I

    .prologue
    .line 135
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendHandler:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

    .line 136
    invoke-virtual {v2, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->getFavoriteContactList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 138
    .local v0, "favoriteContactist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 148
    :cond_0
    return-void

    .line 142
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 145
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "Favorite"

    invoke-virtual {p0, v2, v3, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    .line 142
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected checkFrequentContactist(Landroid/content/Context;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .param p3, "weight"    # I

    .prologue
    .line 152
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendHandler:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

    .line 153
    invoke-virtual {v2, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->getFrequentContactList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 155
    .local v0, "frequentContactist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 163
    :cond_0
    return-void

    .line 159
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 160
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "Frequent"

    invoke-virtual {p0, v2, v3, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected checkLocationSharingList(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "weight"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 303
    .line 304
    const/16 v3, 0xa

    .line 303
    invoke-virtual {p0, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getRecommendedLocationSharedList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v2

    .line 306
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    if-nez v2, :cond_1

    .line 316
    :cond_0
    return-void

    .line 310
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 311
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 313
    .local v1, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "LocationShare"

    invoke-virtual {p0, v3, v4, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    .line 310
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected checkMissedCallList(Landroid/content/Context;JI)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "from"    # J
    .param p4, "weight"    # I

    .prologue
    .line 122
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendHandler:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

    invoke-virtual {v2, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;->getMissedCallList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v1

    .line 125
    .local v1, "missedCallList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 132
    :cond_0
    return-void

    .line 129
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 130
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "MissedCall"

    invoke-virtual {p0, v2, v3, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected checkPOIList(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "weight"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 319
    const/4 v3, 0x5

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getRecommendedLocationDestinationList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 321
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    if-nez v2, :cond_1

    .line 330
    :cond_0
    return-void

    .line 325
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 326
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 328
    .local v1, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "POI"

    invoke-virtual {p0, v3, v4, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected checkScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;
    .param p4, "weight"    # I

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v9

    invoke-virtual {v9, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getScheduleList(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;

    move-result-object v8

    .line 169
    .local v8, "scheduleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;>;"
    if-nez v8, :cond_1

    .line 240
    :cond_0
    return-void

    .line 173
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v2, v9, :cond_0

    .line 175
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;

    .line 176
    invoke-virtual {v9}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;->getAttendeesContactList()Ljava/util/ArrayList;

    move-result-object v1

    .line 178
    .local v1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    if-nez v1, :cond_3

    .line 173
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 182
    :cond_3
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_2

    .line 183
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 184
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v0, :cond_5

    .line 182
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 188
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v9

    .line 195
    invoke-virtual {v9, p1, v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 196
    .local v7, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    if-eqz v7, :cond_4

    .line 200
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_4

    .line 201
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 202
    .local v6, "phoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    if-nez v6, :cond_6

    .line 200
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 206
    :cond_6
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    .line 207
    const-string/jumbo v10, "Schedule"

    .line 206
    invoke-virtual {p0, v9, v10, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->addWeightToPhoneNumber(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3
.end method

.method protected declared-synchronized getContactListFromHashMap(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 337
    .local v16, "startTime":J
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 338
    .local v6, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v15, "sortedRecList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v18

    if-nez v18, :cond_0

    .line 344
    :try_start_1
    new-instance v18, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager$WeightComparator;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager$WeightComparator;-><init>()V

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359
    :goto_1
    :try_start_2
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 378
    .local v5, "contactCheckingList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 379
    .local v9, "endTime":J
    const-string/jumbo v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "[snowdeer] Rec : getContactListFromHashMap - core : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 380
    sub-long v20, v9, v16

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 379
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 386
    const/4 v7, 0x0

    .line 387
    .local v7, "count":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-lt v11, v0, :cond_1

    .line 421
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 422
    const-string/jumbo v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "[snowdeer] Rec : getContactListFromHashMap - duplicated : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 423
    sub-long v20, v9, v16

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 422
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 473
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->rescanContactList(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 475
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 476
    const-string/jumbo v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "[snowdeer] Rec : getContactListFromHashMap - rescan[New] : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 477
    sub-long v20, v9, v16

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 476
    invoke-static/range {v18 .. v19}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 480
    monitor-exit p0

    return-object v6

    .line 339
    .end local v5    # "contactCheckingList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .end local v7    # "count":I
    .end local v9    # "endTime":J
    .end local v11    # "i":I
    :cond_0
    :try_start_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 340
    .local v13, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 335
    .end local v6    # "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .end local v13    # "key":Ljava/lang/String;
    .end local v15    # "sortedRecList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;>;"
    .end local v16    # "startTime":J
    :catchall_0
    move-exception v18

    monitor-exit p0

    throw v18

    .line 345
    .restart local v6    # "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .restart local v15    # "sortedRecList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;>;"
    .restart local v16    # "startTime":J
    :catch_0
    move-exception v8

    .line 346
    .local v8, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 388
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v5    # "contactCheckingList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    .restart local v7    # "count":I
    .restart local v9    # "endTime":J
    .restart local v11    # "i":I
    :cond_1
    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;

    .line 390
    .local v12, "item":Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;
    if-nez v12, :cond_3

    .line 387
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 394
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v18

    .line 395
    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v4

    .line 394
    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 397
    .local v4, "contact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    if-eqz v4, :cond_2

    .line 401
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    .line 400
    move/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 402
    .local v3, "_contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-nez v3, :cond_2

    .line 404
    invoke-virtual {v4, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->cloneWithoutPhoneNumber(Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;)Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    move-result-object v14

    .line 405
    .local v14, "newContact":Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;
    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedContactItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->addPhoneNumber(Ljava/lang/String;)V

    .line 407
    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;->getContactId()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v5, v0, v14}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 411
    add-int/lit8 v7, v7, 0x1

    .line 412
    move/from16 v0, p2

    if-lt v7, v0, :cond_2

    goto/16 :goto_3
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mLogTag:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getRecommendedList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end method

.method public getRecommendedLocationDestinationList(I)Ljava/util/ArrayList;
    .locals 3
    .param p1, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 642
    const-wide/16 v1, -0x1

    .line 641
    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->loadLocationDestinationList(IJ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRecommendedLocationList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 528
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 565
    .local v0, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v2

    .line 566
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    rsub-int/lit8 v3, v3, 0xc

    .line 565
    invoke-virtual {v2, p1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->loadLocationMyPlaceList(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v1

    .line 568
    .local v1, "tmpList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 570
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 573
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, p2, :cond_1

    .line 620
    :cond_1
    return-object v0
.end method

.method public getRecommendedLocationSharedList(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 628
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 629
    .local v0, "fromTime":Ljava/util/Calendar;
    const/16 v1, 0xa

    const/16 v2, -0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 631
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v1

    .line 632
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 631
    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->loadLocationFriendSharedList(Landroid/content/Context;IJ)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->initialize(Landroid/content/Context;)Z

    .line 75
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendHandler:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

    .line 76
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method protected setLogTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mLogTag:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 83
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendHandler:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendHandler;

    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 88
    :cond_0
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;->mRecommendationHashmap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 90
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/BaseManager;->terminate(Landroid/content/Context;)V

    .line 91
    return-void
.end method
