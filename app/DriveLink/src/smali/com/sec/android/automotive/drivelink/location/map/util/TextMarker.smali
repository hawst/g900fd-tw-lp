.class public Lcom/sec/android/automotive/drivelink/location/map/util/TextMarker;
.super Ljava/lang/Object;
.source "TextMarker.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTextMarkClusterToBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "marker"    # Landroid/graphics/Bitmap;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 55
    const/4 v7, 0x5

    .line 56
    .local v7, "thresholdX":I
    const/16 v8, 0xe

    .line 57
    .local v8, "thresholdY":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    .line 58
    .local v6, "mconfig":Landroid/graphics/Bitmap$Config;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 59
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 58
    invoke-static {v11, v12, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 60
    .local v3, "mNewBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 61
    .local v2, "mCanvas":Landroid/graphics/Canvas;
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v2, p0, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 63
    new-instance v4, Landroid/graphics/Paint;

    const/4 v11, 0x1

    invoke-direct {v4, v11}, Landroid/graphics/Paint;-><init>(I)V

    .line 64
    .local v4, "mPaintText":Landroid/graphics/Paint;
    const/high16 v11, -0x1000000

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    const/high16 v11, 0x42200000    # 40.0f

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 67
    sget-object v11, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 68
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float v9, v11, v12

    .line 69
    .local v9, "width":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float v0, v11, v12

    .line 70
    .local v0, "height":F
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 72
    .local v5, "mRectText":Landroid/graphics/Rect;
    const/4 v11, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v4, p1, v11, v12, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 73
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-float v10, v11

    .line 74
    .local v10, "widthAlfa":F
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-float v1, v11

    .line 75
    .local v1, "heightAlfa":F
    const/high16 v11, 0x40a00000    # 5.0f

    sub-float v11, v10, v11

    sub-float v11, v9, v11

    .line 76
    const/high16 v12, 0x41600000    # 14.0f

    add-float/2addr v12, v1

    sub-float v12, v0, v12

    .line 75
    invoke-virtual {v2, p1, v11, v12, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 78
    return-object v3
.end method

.method public static addTextToBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "marker"    # Landroid/graphics/Bitmap;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 19
    const/4 v0, -0x1

    const/16 v1, 0x1e

    invoke-static {p0, p1, v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/util/TextMarker;->addTextToBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static addTextToBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 15
    .param p0, "marker"    # Landroid/graphics/Bitmap;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "color"    # I
    .param p3, "textSize"    # I

    .prologue
    .line 24
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v7

    .line 25
    .local v7, "mconfig":Landroid/graphics/Bitmap$Config;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    .line 26
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    .line 25
    invoke-static {v12, v13, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 27
    .local v4, "mNewBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 28
    .local v3, "mCanvas":Landroid/graphics/Canvas;
    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v3, p0, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 30
    const/4 v8, 0x2

    .line 31
    .local v8, "thresholdX":I
    const/4 v9, 0x2

    .line 33
    .local v9, "thresholdY":I
    new-instance v5, Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-direct {v5, v12}, Landroid/graphics/Paint;-><init>(I)V

    .line 34
    .local v5, "mPaintText":Landroid/graphics/Paint;
    move/from16 v0, p2

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    move/from16 v0, p3

    int-to-float v12, v0

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 36
    sget-object v12, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 37
    sget-object v12, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v5, v12}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 38
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    const/high16 v13, 0x40000000    # 2.0f

    div-float v10, v12, v13

    .line 39
    .local v10, "width":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    int-to-float v12, v12

    const/high16 v13, 0x40000000    # 2.0f

    div-float v1, v12, v13

    .line 41
    .local v1, "height":F
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 42
    .local v6, "mRectText":Landroid/graphics/Rect;
    const/4 v12, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v12, v13, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 44
    invoke-virtual {v6}, Landroid/graphics/Rect;->centerX()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    int-to-float v11, v12

    .line 45
    .local v11, "widthAlfa":F
    invoke-virtual {v6}, Landroid/graphics/Rect;->centerY()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    int-to-float v2, v12

    .line 47
    .local v2, "heightAlfa":F
    const/high16 v12, 0x40000000    # 2.0f

    sub-float v12, v11, v12

    sub-float v12, v10, v12

    .line 48
    const/high16 v13, 0x40000000    # 2.0f

    add-float/2addr v13, v2

    sub-float v13, v1, v13

    .line 47
    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v12, v13, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 50
    return-object v4
.end method
