.class Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;
.super Ljava/lang/Object;
.source "LocationShareConfirmActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->handleMicStateChanged(Lcom/nuance/sample/MicState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->cancelTTS:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->access$2(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 297
    const v1, 0x7f0a0600

    .line 296
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 295
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setVoiceLayoutOnClickListener(Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;->access$3(Lcom/sec/android/automotive/drivelink/location/LocationShareConfirmActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->performCallForClick()V

    .line 301
    :cond_0
    return-void
.end method
