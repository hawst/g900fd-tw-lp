.class public Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;
.super Ljava/lang/Thread;
.source "MusicPlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FileChecker"
.end annotation


# instance fields
.field private isRun:Z

.field musicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field playMusicIdx:I

.field prevMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

.field private prevMusicComp:Z

.field setMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)V
    .locals 1

    .prologue
    .line 958
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 959
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlayIndex()I

    move-result v0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    .line 960
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 961
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->getMusicPlaylist()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->musicList:Ljava/util/ArrayList;

    .line 962
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->setMusicList:Ljava/util/ArrayList;

    .line 963
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->isRun:Z

    .line 964
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    return-void
.end method

.method private setAllMusicList()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1036
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->setMusicList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1037
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setMusicList : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->setMusicList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->setMusicList:Ljava/util/ArrayList;

    .line 1039
    iget v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    .line 1038
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    .line 1055
    :goto_0
    return-void

    .line 1041
    :cond_0
    iget v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1042
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayerPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1043
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->stopPlayer()V

    .line 1044
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getDriveLinkMusicPlayerListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;

    move-result-object v1

    .line 1045
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$4(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Landroid/media/MediaPlayer;

    move-result-object v2

    .line 1046
    const v3, 0x15f90

    .line 1045
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;->onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V

    goto :goto_0

    .line 1049
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 1050
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1051
    .local v0, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "AllMusicList : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;->setMusicPlaylist(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 968
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->isRun:Z

    if-eqz v5, :cond_0

    .line 969
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 970
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->musicList:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 971
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->musicList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 972
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->musicList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v3, v5, :cond_1

    .line 1024
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->setAllMusicList()V

    .line 1025
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->isRun:Z

    .line 1029
    .end local v3    # "i":I
    :cond_0
    return-void

    .line 973
    .restart local v3    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->musicList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 974
    .local v4, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "get Music ID : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 975
    const-string/jumbo v7, " music list count  : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 974
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    .line 977
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "getData is null"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v5

    .line 979
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$3(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z

    .line 982
    :cond_2
    if-eqz v4, :cond_5

    .line 983
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "music is not null"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 985
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    if-nez v5, :cond_3

    .line 986
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusic:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v5

    .line 987
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v6

    .line 986
    if-ne v5, v6, :cond_3

    .line 988
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    .line 989
    :cond_3
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "prevMusicComp : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 990
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 989
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    const/4 v1, 0x0

    .line 993
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    .line 994
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v5

    .line 993
    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 995
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->setMusicList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 996
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    if-eqz v5, :cond_4

    .line 997
    iput v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    .line 998
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    .line 999
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "playMusicIdx : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1000
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 999
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    :cond_4
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Added Music data : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1003
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getData()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1002
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 972
    .end local v2    # "fis":Ljava/io/FileInputStream;
    :cond_5
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1004
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 1005
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    if-eqz v5, :cond_6

    .line 1006
    iput v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    .line 1007
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    .line 1008
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Exception : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1009
    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1008
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1011
    :cond_6
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Music data is deleted"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1015
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fis":Ljava/io/FileInputStream;
    :cond_7
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    if-eqz v5, :cond_5

    .line 1016
    iput v9, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    .line 1017
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->prevMusicComp:Z

    .line 1018
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->access$1()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "is null : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->playMusicIdx:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1004
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public setExit()V
    .locals 1

    .prologue
    .line 1032
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager$FileChecker;->isRun:Z

    .line 1033
    return-void
.end method
