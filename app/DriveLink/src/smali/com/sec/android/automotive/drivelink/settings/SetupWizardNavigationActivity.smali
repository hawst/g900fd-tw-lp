.class public Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SetupWizardNavigationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardNavigationActivity$MapType:[I


# instance fields
.field baidu_navi:Landroid/widget/TextView;

.field cmcc_navi:Landroid/widget/TextView;

.field daum_maps:Landroid/widget/TextView;

.field google_navi:Landroid/widget/TextView;

.field olleh_navi:Landroid/widget/TextView;

.field radio_baidu_navi:Landroid/widget/RadioButton;

.field radio_cmcc_navi:Landroid/widget/RadioButton;

.field radio_google_navi:Landroid/widget/RadioButton;

.field radio_olleh_navi:Landroid/widget/RadioButton;

.field radio_tmap_navi:Landroid/widget/RadioButton;

.field radio_uplus_navi:Landroid/widget/RadioButton;

.field rl_baidu_navi:Landroid/widget/RelativeLayout;

.field rl_cmcc_navi:Landroid/widget/RelativeLayout;

.field rl_google_navi:Landroid/widget/RelativeLayout;

.field rl_olleh_navi:Landroid/widget/RelativeLayout;

.field rl_tmap_navi:Landroid/widget/RelativeLayout;

.field rl_uplus_navi:Landroid/widget/RelativeLayout;

.field tmap_navi:Landroid/widget/TextView;

.field uplus_navi:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardNavigationActivity$MapType()[I
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardNavigationActivity$MapType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->values()[Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->BAIDU:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->CMCC_NAVI:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->DAUM:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->GOOGLE:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->OLLEH:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->TMAP:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->UPLUS:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardNavigationActivity$MapType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;I)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->saveNaviPrefSettings(I)V

    return-void
.end method

.method private checkInstalledApps()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 293
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getUserCountryForNavi()Ljava/lang/String;

    move-result-object v5

    .line 294
    .local v5, "mUserCountry":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getSalesCodePropertyForNavi()Ljava/lang/String;

    move-result-object v4

    .line 297
    .local v4, "mSalesProperty":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 299
    .local v0, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-interface {v0, v5, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestMapAvailableByCountryAndSaleCode(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 302
    .local v2, "mListNavitationAvailable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_1

    .line 333
    :cond_0
    return-void

    .line 305
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 306
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 308
    .local v3, "mPackage":Ljava/lang/String;
    const-string/jumbo v6, "com.google.android.apps.maps"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 309
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_google_navi:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 312
    :cond_2
    const-string/jumbo v6, "com.mnsoft.lgunavi"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 313
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_uplus_navi:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 316
    :cond_3
    const-string/jumbo v6, "com.skt.skaf.l001mtm091"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 317
    const-string/jumbo v6, "com.skt.skaf.l001mtm092"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 318
    :cond_4
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_tmap_navi:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 321
    :cond_5
    const-string/jumbo v6, "kt.navi"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 322
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_olleh_navi:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 325
    :cond_6
    const-string/jumbo v6, "com.baidu.BaiduMap"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 326
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_baidu_navi:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 329
    :cond_7
    const-string/jumbo v6, "com.autonavi.cmccmap"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 330
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_cmcc_navi:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 305
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V
    .locals 4
    .param p1, "map"    # Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 232
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$settings$SetupWizardNavigationActivity$MapType()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 290
    :goto_0
    return-void

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 242
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 250
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 258
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 266
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 274
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 282
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private saveNaviPrefSettings(I)V
    .locals 4
    .param p1, "mapName"    # I

    .prologue
    .line 337
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->getPackageNameChosen(I)Ljava/lang/String;

    move-result-object v1

    .line 340
    .local v1, "navigationPackageName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 341
    .local v2, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION"

    invoke-virtual {v2, v3, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;I)V

    .line 344
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION_PACKAGE"

    .line 343
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    new-instance v0, Landroid/content/Intent;

    .line 348
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.NAVIGATION_PACKAGE_NAME"

    .line 347
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 349
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "com.android.phone"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const-string/jumbo v3, "INTENT_KEY_NAVIGATION_PACKAGE_NAME"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 354
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, -0x1

    .line 60
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->init(Landroid/content/Context;)V

    .line 64
    const v2, 0x7f030035

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->setContentView(I)V

    .line 66
    const v2, 0x7f09016f

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->google_navi:Landroid/widget/TextView;

    .line 67
    const v2, 0x7f090172

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->uplus_navi:Landroid/widget/TextView;

    .line 68
    const v2, 0x7f090175

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->tmap_navi:Landroid/widget/TextView;

    .line 69
    const v2, 0x7f090178

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->baidu_navi:Landroid/widget/TextView;

    .line 70
    const v2, 0x7f09017b

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->olleh_navi:Landroid/widget/TextView;

    .line 71
    const v2, 0x7f09017e

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->cmcc_navi:Landroid/widget/TextView;

    .line 73
    const v2, 0x7f090170

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    .line 74
    const v2, 0x7f090173

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    .line 75
    const v2, 0x7f090176

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    .line 76
    const v2, 0x7f090179

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    .line 77
    const v2, 0x7f09017c

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    .line 78
    const v2, 0x7f09017f

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    .line 80
    const v2, 0x7f09016e

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_google_navi:Landroid/widget/RelativeLayout;

    .line 81
    const v2, 0x7f090171

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_uplus_navi:Landroid/widget/RelativeLayout;

    .line 82
    const v2, 0x7f090174

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_tmap_navi:Landroid/widget/RelativeLayout;

    .line 83
    const v2, 0x7f090177

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_baidu_navi:Landroid/widget/RelativeLayout;

    .line 84
    const v2, 0x7f09017a

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_olleh_navi:Landroid/widget/RelativeLayout;

    .line 85
    const v2, 0x7f09017d

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->rl_cmcc_navi:Landroid/widget/RelativeLayout;

    .line 87
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->google_navi:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_google_navi:Landroid/widget/RadioButton;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->uplus_navi:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_uplus_navi:Landroid/widget/RadioButton;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->tmap_navi:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$5;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_tmap_navi:Landroid/widget/RadioButton;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$6;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->olleh_navi:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$7;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_olleh_navi:Landroid/widget/RadioButton;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->baidu_navi:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$9;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_baidu_navi:Landroid/widget/RadioButton;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$10;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->cmcc_navi:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$11;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->radio_cmcc_navi:Landroid/widget/RadioButton;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$12;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    const v2, 0x7f09016c

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 196
    .local v0, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$13;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$13;-><init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkInstalledApps()V

    .line 206
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 205
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 207
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION"

    .line 206
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v1

    .line 209
    .local v1, "mapID":I
    if-eq v1, v4, :cond_5

    .line 210
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v1, v2, :cond_0

    .line 211
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->GOOGLE:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V

    .line 213
    :cond_0
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_UNAVI:I

    if-ne v1, v2, :cond_1

    .line 214
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->UPLUS:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V

    .line 216
    :cond_1
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_TMAP:I

    if-ne v1, v2, :cond_2

    .line 217
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->TMAP:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V

    .line 219
    :cond_2
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_OLLEH_NAVI:I

    if-ne v1, v2, :cond_3

    .line 220
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->OLLEH:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V

    .line 222
    :cond_3
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-ne v1, v2, :cond_4

    .line 223
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->BAIDU:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V

    .line 225
    :cond_4
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-ne v1, v2, :cond_5

    .line 226
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;->CMCC_NAVI:Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;

    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity;->checkRadioButton(Lcom/sec/android/automotive/drivelink/settings/SetupWizardNavigationActivity$MapType;)V

    .line 229
    :cond_5
    return-void
.end method
