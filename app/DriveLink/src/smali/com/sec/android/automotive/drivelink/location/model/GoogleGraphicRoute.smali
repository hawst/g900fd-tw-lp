.class public Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;
.super Ljava/lang/Object;
.source "GoogleGraphicRoute.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/model/IGraphicRoute;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x50fcebba1856ebb7L


# instance fields
.field private final points:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/model/LatLng;",
            ">;"
        }
    .end annotation
.end field

.field private final polyline:Lcom/google/android/gms/maps/model/Polyline;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/maps/model/Polyline;)V
    .locals 1
    .param p1, "polyline"    # Lcom/google/android/gms/maps/model/Polyline;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->polyline:Lcom/google/android/gms/maps/model/Polyline;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->points:Ljava/util/List;

    .line 34
    return-void
.end method

.method private updateRoute()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->polyline:Lcom/google/android/gms/maps/model/Polyline;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->points:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Polyline;->setPoints(Ljava/util/List;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->polyline:Lcom/google/android/gms/maps/model/Polyline;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Polyline;->setVisible(Z)V

    .line 67
    return-void
.end method


# virtual methods
.method public addPoint(DD)V
    .locals 7
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 38
    const-wide v5, 0x3f079dd66a2f73b6L    # 4.5045E-5

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->addPoint(DDD)V

    .line 39
    return-void
.end method

.method public addPoint(DDD)V
    .locals 16
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "threshold"    # D

    .prologue
    .line 44
    new-instance v12, Lcom/google/android/gms/maps/model/LatLng;

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    invoke-direct {v12, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 46
    .local v12, "currPoint":Lcom/google/android/gms/maps/model/LatLng;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->points:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 47
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->points:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->points:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/gms/maps/model/LatLng;

    .line 49
    .local v15, "lastPoint":Lcom/google/android/gms/maps/model/LatLng;
    iget-wide v4, v15, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    .line 50
    iget-wide v6, v15, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iget-wide v8, v12, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    .line 51
    iget-wide v10, v12, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    .line 49
    invoke-static/range {v4 .. v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getDistance(DDDD)F

    move-result v4

    float-to-double v13, v4

    .line 55
    .local v13, "distance":D
    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpg-double v4, v4, p5

    if-gez v4, :cond_0

    .line 62
    .end local v13    # "distance":D
    .end local v15    # "lastPoint":Lcom/google/android/gms/maps/model/LatLng;
    :goto_0
    return-void

    .line 60
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->points:Ljava/util/List;

    invoke-interface {v4, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/location/model/GoogleGraphicRoute;->updateRoute()V

    goto :goto_0
.end method
