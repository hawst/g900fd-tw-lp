.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 723
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->hideSearchBar()Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$11(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Z

    .line 725
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    add-int/lit8 v0, v1, -0x1

    .line 728
    .local v0, "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->isClickable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 736
    :cond_0
    :goto_0
    return-void

    .line 731
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mLocationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setClickable(Z)V

    .line 733
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->selectItem(I)Z
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$14(Lcom/sec/android/automotive/drivelink/location/LocationActivity;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 734
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->enableViewPager()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$15(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V

    goto :goto_0
.end method
