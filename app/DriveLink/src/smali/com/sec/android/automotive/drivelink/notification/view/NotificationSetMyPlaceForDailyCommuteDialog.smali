.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;
.super Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;
.source "NotificationSetMyPlaceForDailyCommuteDialog.java"


# instance fields
.field private mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

.field private mText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "mText"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->mText:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0a0254

    .line 26
    const v3, 0x7f0a0255

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;-><init>(Landroid/content/Context;II)V

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    .line 28
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a033e

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setTitle(I)V

    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationSetMyPlaceForDailyCommuteDialog;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
