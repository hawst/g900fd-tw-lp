.class public Lcom/sec/android/automotive/drivelink/location/places/impl/TMapPlaceService;
.super Ljava/lang/Object;
.source "TMapPlaceService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/places/IPlaceService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public autocomplete(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "place"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public details(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
    .locals 1
    .param p1, "reference"    # Ljava/lang/String;

    .prologue
    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method public nearbySearch(Ljava/lang/String;DD)Ljava/util/List;
    .locals 1
    .param p1, "place"    # Ljava/lang/String;
    .param p2, "latitude"    # D
    .param p4, "longitude"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DD)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method
