.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkConnectivityListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkConnectivityListener"
.end annotation


# virtual methods
.method public abstract onNotifyBluetoothDeviceConnected()V
.end method

.method public abstract onNotifyBluetoothDeviceDisconnected()V
.end method

.method public abstract onNotifyBluetoothDevicePaired()V
.end method

.method public abstract onNotifyBluetoothDeviceUnpaired()V
.end method

.method public abstract onResponseRequestBluetoothConnectDevice(Z)V
.end method

.method public abstract onResponseRequestBluetoothConnectionState(Z)V
.end method

.method public abstract onResponseRequestBluetoothDisconnectDevice(Z)V
.end method

.method public abstract onResponseRequestBluetoothMakeDeviceDiscoverable()V
.end method

.method public abstract onResponseRequestBluetoothPairDevice(I)V
.end method

.method public abstract onResponseRequestBluetoothPairingNSSPAnswer(Z)V
.end method

.method public abstract onResponseRequestBluetoothPairingSSPAnswer(Z)V
.end method

.method public abstract onResponseRequestBluetoothSearchDiscoverableDevices()V
.end method

.method public abstract onResponseRequestBluetoothSearchPairedDevices()V
.end method

.method public abstract onResponseRequestBluetoothStartAdapter(Z)V
.end method

.method public abstract onResponseRequestBluetoothStopAdapter()V
.end method

.method public abstract onResponseRequestBluetoothUnpairDevice(Z)V
.end method
