.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;
.source "DLCallInfoImp.java"


# instance fields
.field private mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

.field private mName:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;-><init>()V

    .line 6
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 7
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 11
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "callState"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "phoneName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;-><init>()V

    .line 6
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 7
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 17
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 18
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "phoneName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo;-><init>()V

    .line 6
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 7
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getCallState()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public setCallState(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;)V
    .locals 0
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mCallState:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallInfo$CALL_STATE;

    .line 44
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mName:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLCallInfoImp;->mPhoneNumber:Ljava/lang/String;

    .line 52
    return-void
.end method
