.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationType;
.super Ljava/lang/Object;
.source "NotificationType.java"


# static fields
.field public static final ACTION_AUDIO_BECOMING_NOISY:I = 0xd

.field public static final ACTION_MEDIA_BAD_REMOVAL:I = 0x11

.field public static final ACTION_MEDIA_EJECT:I = 0x10

.field public static final ACTION_MEDIA_MOUNTED:I = 0xe

.field public static final ACTION_MEDIA_UNMOUNTED:I = 0xf

.field public static final ACTION_PRIVATE_MODE_OFF:I = 0x13

.field public static final ACTION_PRIVATE_MODE_OFF_INTENT:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

.field public static final ACTION_PRIVATE_MODE_ON:I = 0x12

.field public static final ACTION_PRIVATE_MODE_ON_INTENT:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_ON"

.field public static final NOTI_ALARM:I = 0x7

.field public static final NOTI_BATTERY_LOW:I = 0x6

.field public static final NOTI_CALL_IDLE:I = 0x0

.field public static final NOTI_CALL_OFFHOOK:I = 0x2

.field public static final NOTI_CALL_RINGING:I = 0x1

.field public static final NOTI_DAILY_COMMUTE:I = 0xb

.field public static final NOTI_LOCATION:I = 0x5

.field public static final NOTI_LOCATION_MSG:I = 0xa

.field public static final NOTI_MSG_MMS:I = 0x4

.field public static final NOTI_MSG_SMS:I = 0x3

.field public static final NOTI_OUTGOING_CALL:I = 0x8

.field public static final NOTI_SCHEDULE:I = 0x9

.field public static final NOTI_SMART_ALLERT:I = 0xc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
