.class Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;
.super Landroid/os/Handler;
.source "VoiceHomeActionBarLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    .line 561
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 565
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v1

    sget-object v2, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->access$0(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 566
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 567
    const-string/jumbo v1, "voicehome"

    const-string/jumbo v2, "------------> set visibility visible 1"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mStandByLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->access$2(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 569
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mNoNetworkLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->access$3(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/LinearLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 571
    const-string/jumbo v1, "JINSEIL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "JINSEIL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v3

    sget-object v4, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mIsMicDisplayed:Z
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->access$0(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 572
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout$1;->this$0:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->mLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->access$1(Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 571
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 575
    return-void
.end method
