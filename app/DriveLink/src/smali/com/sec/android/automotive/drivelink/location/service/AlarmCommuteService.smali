.class public Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;
.super Ljava/lang/Object;
.source "AlarmCommuteService.java"


# static fields
.field public static ID_INTENT_END:I

.field public static ID_INTENT_HOME:I

.field public static ID_INTENT_OFFICE:I

.field public static ID_INTENT_START:I


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x64

    sput v0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_HOME:I

    .line 28
    const/16 v0, 0xc8

    sput v0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_OFFICE:I

    .line 30
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_START:I

    .line 31
    const/16 v0, 0x14

    sput v0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_END:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->context:Landroid/content/Context;

    .line 35
    return-void
.end method

.method private cancelAlarm(I)V
    .locals 0
    .param p1, "idIntent"    # I

    .prologue
    .line 75
    return-void
.end method

.method private createAlarmRoute()V
    .locals 9

    .prologue
    const v8, 0x7f0a0432

    const v7, 0x7f0a0431

    .line 83
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 87
    .local v0, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v5, "PREF_SETTINGS_COMMUTE_NOTIFICATION_FROMHOME"

    .line 88
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 89
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 86
    invoke-virtual {v0, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "textFromHome":Ljava/lang/String;
    const-string/jumbo v5, "PREF_SETTINGS_COMMUTE_NOTIFICATION_TOHOME"

    .line 94
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 95
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 92
    invoke-virtual {v0, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 100
    .local v3, "textToHome":Ljava/lang/String;
    const-string/jumbo v5, "PREF_SETTINGS_COMMUTE_NOTIFICATION_FROMWORK"

    .line 101
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 102
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 99
    invoke-virtual {v0, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 106
    .local v2, "textFromWork":Ljava/lang/String;
    const-string/jumbo v5, "PREF_SETTINGS_COMMUTE_NOTIFICATION_TOWORK"

    .line 107
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 108
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 105
    invoke-virtual {v0, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 112
    .local v4, "textToWork":Ljava/lang/String;
    const-string/jumbo v5, "MYPLACE.HOME"

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 113
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_HOME:I

    .line 112
    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->createAlarm(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 116
    const-string/jumbo v5, "MYPLACE.OFFICE"

    .line 117
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 118
    sget v8, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_OFFICE:I

    .line 116
    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->createAlarm(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 119
    return-void
.end method


# virtual methods
.method public cancelAllAlarm()V
    .locals 2

    .prologue
    .line 47
    const-string/jumbo v0, "DEBUG"

    const-string/jumbo v1, "Cancelling ALL commute service alarms."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    sget v0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_HOME:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->cancelAlarm(I)V

    .line 49
    sget v0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->ID_INTENT_OFFICE:I

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->cancelAlarm(I)V

    .line 50
    return-void
.end method

.method public createAlarm(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "myPlace"    # Ljava/lang/String;
    .param p2, "patternTimeStart"    # Ljava/lang/String;
    .param p3, "patternTimeEnd"    # Ljava/lang/String;
    .param p4, "idIntent"    # I

    .prologue
    .line 175
    return-void
.end method

.method public enableAlarmForSelectedDays()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->createAlarmRoute()V

    .line 79
    return-void
.end method

.method public isAlarmDailyCommuteEnable()Z
    .locals 3

    .prologue
    .line 39
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/service/AlarmCommuteService;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 42
    .local v0, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v1, "PREF_SETTINGS_COMMUTE_NOTIFICATION"

    .line 43
    const/4 v2, 0x0

    .line 41
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v1

    .line 40
    return v1
.end method
