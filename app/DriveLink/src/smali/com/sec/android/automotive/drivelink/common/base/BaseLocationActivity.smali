.class public abstract Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;
.source "BaseLocationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity$NetworkConnectionChangedReceiver;
    }
.end annotation


# static fields
.field public static final MY_PLACE_SETTINGS_CODE:I = 0xddd5

.field protected static TAG:Ljava/lang/String;

.field private static mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;


# instance fields
.field private mGelocation:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

.field private mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

.field private mNetConnection:Lcom/sec/android/automotive/drivelink/util/DLConnection;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "BaseLocationActivity"

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->TAG:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 41
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mGelocation:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mNetConnection:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    .line 47
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->getInstance()Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mGelocation:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    .line 48
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->getInstance()Lcom/sec/android/automotive/drivelink/util/DLConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mNetConnection:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;)Lcom/sec/android/automotive/drivelink/util/DLConnection;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mNetConnection:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    return-object v0
.end method

.method private addQuote(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 257
    if-nez p1, :cond_0

    .line 258
    const-string/jumbo v0, "\'\'"

    .line 261
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    if-eqz v0, :cond_0

    .line 147
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    .line 151
    :goto_0
    return-object v0

    .line 149
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    .line 151
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mRequestGeoLocation:Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    goto :goto_0
.end method

.method private initEnableGps()V
    .locals 4

    .prologue
    .line 288
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    .line 289
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "dialogGps"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 291
    .local v1, "fragCheck":Landroid/support/v4/app/Fragment;
    if-eqz v1, :cond_0

    .line 298
    :goto_0
    return-void

    .line 294
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;-><init>()V

    .line 296
    .local v0, "dialog":Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    .line 297
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "dialogGps"

    .line 296
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isNetworkAvailable()Z
    .locals 3

    .prologue
    .line 196
    const-string/jumbo v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 198
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 199
    .local v0, "activeNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private removeQuote(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 265
    if-nez p1, :cond_0

    .line 266
    const-string/jumbo v0, ""

    .line 269
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "^\"|\"$"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected IsOffline()Z
    .locals 3

    .prologue
    const v2, 0x7f0a0322

    const/4 v0, 0x1

    .line 179
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->isNetworkAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->showToast(I)V

    .line 188
    :goto_0
    return v0

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mNetConnection:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->isNetworkUsable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 185
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->showToast(I)V

    goto :goto_0

    .line 188
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finishIsOffline()Z
    .locals 3

    .prologue
    const v2, 0x7f0a0322

    const/4 v0, 0x1

    .line 165
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->isNetworkAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 166
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->showToast(I)V

    .line 175
    :goto_0
    return v0

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mNetConnection:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->isNetworkUsable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 171
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->showToast(I)V

    goto :goto_0

    .line 175
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEnableGPSPDialog()V
    .locals 0

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->initEnableGps()V

    .line 284
    return-void
.end method

.method protected getGeolocation()Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mGelocation:Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    return-object v0
.end method

.method protected abstract getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
.end method

.method protected getNetwork()Lcom/sec/android/automotive/drivelink/util/DLConnection;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mNetConnection:Lcom/sec/android/automotive/drivelink/util/DLConnection;

    return-object v0
.end method

.method protected hasSimCard(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 192
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isProviderEnabled()Z
    .locals 2

    .prologue
    .line 130
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 60
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 63
    if-nez p1, :cond_0

    .line 64
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->stop()V

    .line 66
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;)V

    .line 66
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->setResponseListener(Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation$OnResponseRequestGeolocation;)V

    .line 73
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 276
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onDestroy()V

    .line 278
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 97
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onPause()V

    .line 98
    return-void
.end method

.method protected onResponseRequestGeolocation(Landroid/location/Location;Z)V
    .locals 0
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "timeout"    # Z

    .prologue
    .line 143
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onResume()V

    .line 86
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 80
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method protected requestGeolocation()Z
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected requestGeolocation(J)Z
    .locals 2
    .param p1, "timeout"    # J

    .prologue
    .line 116
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getRequestLocation()Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/geolocation/RequestGeoLocation;->execute(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method protected varargs setButtonQuoted(Z[Landroid/widget/Button;)V
    .locals 9
    .param p1, "enable"    # Z
    .param p2, "buttons"    # [Landroid/widget/Button;

    .prologue
    const v8, 0x7f0c00bc

    const/4 v4, 0x0

    .line 224
    if-eqz p2, :cond_0

    array-length v3, p2

    if-gtz v3, :cond_1

    .line 254
    :cond_0
    return-void

    .line 227
    :cond_1
    array-length v5, p2

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, p2, v3

    .line 228
    .local v0, "button":Landroid/widget/Button;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    if-nez v6, :cond_3

    .line 227
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 231
    :cond_3
    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 233
    .local v2, "text":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 237
    const/4 v1, 0x0

    .line 238
    .local v1, "qtext":Ljava/lang/CharSequence;
    if-eqz p1, :cond_4

    .line 239
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->addQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v0, v6, v8}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 242
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 243
    const/4 v7, 0x3

    .line 242
    invoke-static {v6, v2, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    .line 252
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 245
    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->removeQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 246
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v0, v6, v8}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 248
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 249
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 248
    invoke-static {v6, v7, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v1

    goto :goto_2
.end method

.method public showToast(I)V
    .locals 1
    .param p1, "textID"    # I

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 304
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mToast:Landroid/widget/Toast;

    .line 305
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 307
    :cond_1
    return-void
.end method

.method protected startDmFlow()V
    .locals 1

    .prologue
    .line 90
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsThereDialogActive:Z

    if-nez v0, :cond_0

    .line 91
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->startDmFlow()V

    .line 93
    :cond_0
    return-void
.end method
