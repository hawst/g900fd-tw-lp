.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$3;
.super Landroid/os/Handler;
.source "NotificationItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 892
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 896
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 898
    iget v0, p1, Landroid/os/Message;->arg1:I

    # getter for: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->ACTION_TIMEOUT:I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$0()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 899
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "Timeout Handler:timeout end. next cmd."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$3;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->doCmd()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->access$1(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 902
    :cond_0
    return-void
.end method
