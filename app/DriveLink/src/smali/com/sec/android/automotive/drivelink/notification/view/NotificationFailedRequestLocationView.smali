.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationFailedRequestLocationView.java"


# static fields
.field protected static mContext:Landroid/content/Context;


# instance fields
.field private mViewLocal:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mViewLocal:Landroid/view/View;

    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->initIndicatorView(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mViewLocal:Landroid/view/View;

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->initIndicatorView(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mViewLocal:Landroid/view/View;

    .line 24
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->initIndicatorView(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    if-nez p1, :cond_0

    .line 76
    :goto_0
    return-void

    .line 45
    :cond_0
    sput-object p1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mContext:Landroid/content/Context;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 47
    const-string/jumbo v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 46
    check-cast v0, Landroid/view/LayoutInflater;

    .line 49
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mViewLocal:Landroid/view/View;

    if-nez v4, :cond_1

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->getNotiInfo()Ljava/lang/Object;

    move-result-object v1

    .line 50
    check-cast v1, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;

    .line 53
    .local v1, "info":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    const v4, 0x7f0300a5

    .line 52
    invoke-virtual {v0, v4, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mViewLocal:Landroid/view/View;

    .line 54
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mViewLocal:Landroid/view/View;

    .line 55
    const v5, 0x7f0902cd

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 54
    check-cast v2, Landroid/widget/ImageView;

    .line 56
    .local v2, "ivShared":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;->mViewLocal:Landroid/view/View;

    .line 57
    const v5, 0x7f0902ce

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 56
    check-cast v3, Landroid/widget/TextView;

    .line 59
    .local v3, "tvInfo":Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getMessageId()I

    move-result v4

    const v5, 0x7f0a0391

    if-ne v4, v5, :cond_2

    .line 60
    const v4, 0x7f020238

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 65
    :goto_1
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;->getMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 69
    .end local v1    # "info":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    .end local v2    # "ivShared":Landroid/widget/ImageView;
    .end local v3    # "tvInfo":Landroid/widget/TextView;
    :cond_1
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView$1;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationFailedRequestLocationView;)V

    .line 75
    const-wide/16 v6, 0xbb8

    .line 69
    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 62
    .restart local v1    # "info":Lcom/sec/android/automotive/drivelink/notification/model/NotiGeneralMessageInfo;
    .restart local v2    # "ivShared":Landroid/widget/ImageView;
    .restart local v3    # "tvInfo":Landroid/widget/TextView;
    :cond_2
    const v4, 0x7f020237

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method
