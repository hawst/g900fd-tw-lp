.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationSearched;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
.source "DLLocationSearched.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->FRIENDS_SHARED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V

    .line 9
    return-void
.end method


# virtual methods
.method public getSearchedAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationSearched;->getLocationDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setSearchedAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationSearched;->setLocationDescription(Ljava/lang/String;)V

    .line 17
    return-void
.end method
