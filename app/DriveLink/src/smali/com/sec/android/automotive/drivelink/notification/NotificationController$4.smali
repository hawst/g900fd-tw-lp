.class Lcom/sec/android/automotive/drivelink/notification/NotificationController$4;
.super Ljava/lang/Object;
.source "NotificationController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 255
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Disappear Animation End."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->removeNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Z)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->bEndAnimationStarted:Z

    .line 258
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Animation End..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 265
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$4;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->bEndAnimationStarted:Z

    .line 271
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Animation Start..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    return-void
.end method
