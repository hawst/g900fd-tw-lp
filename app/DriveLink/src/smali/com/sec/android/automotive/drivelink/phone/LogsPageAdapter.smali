.class public Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "LogsPageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final ITEM_NUM_PER_PAGE:I = 0x6

.field private static final MAX_PAGE_TO_LOOP:I = 0x4e20


# instance fields
.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mInitialPosition:I

.field private mLogsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;"
        }
    .end annotation
.end field

.field private mPageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mLogsList:Ljava/util/ArrayList;

    .line 43
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->getPageCount()I

    move-result v0

    .line 46
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 48
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 47
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mInitialPosition:I

    .line 49
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mPageCount:I

    .line 54
    :goto_0
    return-void

    .line 51
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mInitialPosition:I

    .line 52
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mPageCount:I

    goto :goto_0
.end method

.method private getDate(J)Ljava/lang/String;
    .locals 7
    .param p1, "date"    # J

    .prologue
    const/4 v6, 0x6

    .line 188
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 189
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 190
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 192
    .local v4, "today":I
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 193
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 195
    .local v1, "day":I
    sub-int v2, v4, v1

    .line 196
    .local v2, "diffDay":I
    if-nez v2, :cond_0

    .line 197
    const-string/jumbo v5, "hh:mm AA"

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v5, v6}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 204
    .local v3, "ret":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 198
    .end local v3    # "ret":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    .line 199
    const-string/jumbo v3, "Yesterday"

    .line 200
    .restart local v3    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 201
    .end local v3    # "ret":Ljava/lang/String;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, " days ago"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method private getIconRes(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;)I
    .locals 2
    .param p1, "callLog"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .prologue
    const v0, 0x7f02025a

    .line 208
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->OUTGOING_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    if-ne p1, v1, :cond_1

    .line 209
    const v0, 0x7f02025d

    .line 221
    :cond_0
    :goto_0
    return v0

    .line 210
    :cond_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->INCOMING_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    if-ne p1, v1, :cond_2

    .line 211
    const v0, 0x7f02025b

    goto :goto_0

    .line 212
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->MISSED_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    if-ne p1, v1, :cond_3

    .line 213
    const v0, 0x7f02025c

    goto :goto_0

    .line 214
    :cond_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->VOICEMAIL_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    if-ne p1, v1, :cond_4

    .line 215
    const v0, 0x7f02025f

    goto :goto_0

    .line 216
    :cond_4
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->REJECTED_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    if-ne p1, v1, :cond_5

    .line 217
    const v0, 0x7f02025e

    goto :goto_0

    .line 218
    :cond_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->AUTOREJECTED_TYPE:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    if-ne p1, v1, :cond_0

    goto :goto_0
.end method

.method private getIconTypeRes(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;)I
    .locals 2
    .param p1, "log"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .prologue
    const v0, 0x7f020260

    .line 226
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_CALL:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    if-ne p1, v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v0

    .line 228
    :cond_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_SMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    if-ne p1, v1, :cond_2

    .line 229
    const v0, 0x7f020262

    goto :goto_0

    .line 230
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_MMS:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    if-ne p1, v1, :cond_3

    .line 231
    const v0, 0x7f020261

    goto :goto_0

    .line 232
    :cond_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->LOG_TYPE_VIDEO:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    if-ne p1, v1, :cond_0

    .line 233
    const v0, 0x7f020263

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 156
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 157
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mPageCount:I

    return v0
.end method

.method public getInitialPosition()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mInitialPosition:I

    return v0
.end method

.method public getPageCount()I
    .locals 3

    .prologue
    .line 165
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mLogsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int/lit8 v0, v2, 0x6

    .line 166
    .local v0, "page":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mLogsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    rem-int/lit8 v1, v2, 0x6

    .line 167
    .local v1, "remainder":I
    if-lez v1, :cond_0

    .line 168
    add-int/lit8 v0, v0, 0x1

    .line 171
    :cond_0
    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 15
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 72
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 73
    .local v8, "layoutInflater":Landroid/view/LayoutInflater;
    const v11, 0x7f03006a

    const/4 v12, 0x0

    invoke-virtual {v8, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 75
    .local v10, "v":Landroid/view/View;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v9, "listItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/RelativeLayout;>;"
    const v11, 0x7f090216

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    const v11, 0x7f090217

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    const v11, 0x7f090218

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    const v11, 0x7f090219

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    const v11, 0x7f09021a

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    const v11, 0x7f09021b

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v3

    .line 86
    .local v3, "driveLinkServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->getPageCount()I

    move-result v11

    rem-int v11, p2, v11

    mul-int/lit8 v6, v11, 0x6

    .line 89
    .local v6, "index":I
    const/4 v1, 0x0

    .line 92
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    const/4 v5, 0x1

    .line 93
    .local v5, "i":I
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_0

    .line 149
    move/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/view/View;->setId(I)V

    .line 150
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 151
    return-object v10

    .line 93
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 94
    .local v7, "layout":Landroid/widget/RelativeLayout;
    new-instance v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;

    const/4 v11, 0x0

    invoke-direct {v4, v11}, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;)V

    .line 96
    .local v4, "holder":Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;
    const v11, 0x7f090210

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 95
    iput-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsImage:Landroid/widget/ImageView;

    .line 98
    const v11, 0x7f090211

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 97
    iput-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    .line 99
    const v11, 0x7f090212

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    .line 100
    const v11, 0x7f090213

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->tvTime:Landroid/widget/TextView;

    .line 102
    const v11, 0x7f090214

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 101
    iput-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivCallTypeIcon:Landroid/widget/ImageView;

    .line 104
    const v11, 0x7f090215

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 103
    iput-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsTypeIcon:Landroid/widget/ImageView;

    .line 106
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mLogsList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-le v11, v6, :cond_4

    .line 107
    invoke-virtual {v7, v5}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 108
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mLogsList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 111
    .local v2, "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mContext:Landroid/content/Context;

    invoke-interface {v3, v11, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getCallLogImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 114
    if-nez v1, :cond_2

    .line 115
    iget-object v11, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    const/4 v13, 0x2

    if-ne v11, v13, :cond_1

    .line 116
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsImage:Landroid/widget/ImageView;

    .line 117
    const v13, 0x7f02024e

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    :goto_1
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getName()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 127
    const-string/jumbo v11, ""

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 128
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    :goto_2
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->tvTime:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getDate()J

    move-result-wide v13

    invoke-direct {p0, v13, v14}, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivCallTypeIcon:Landroid/widget/ImageView;

    .line 133
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    move-result-object v13

    .line 132
    invoke-direct {p0, v13}, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->getIconRes(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;)I

    move-result v13

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 134
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsTypeIcon:Landroid/widget/ImageView;

    .line 135
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getLogType()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    move-result-object v13

    .line 134
    invoke-direct {p0, v13}, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->getIconTypeRes(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;)I

    move-result v13

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 145
    .end local v2    # "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    :goto_3
    add-int/lit8 v6, v6, 0x1

    .line 146
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 119
    .restart local v2    # "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    :cond_1
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsImage:Landroid/widget/ImageView;

    .line 120
    const v13, 0x7f02023d

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 123
    :cond_2
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsImage:Landroid/widget/ImageView;

    invoke-virtual {v11, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 130
    :cond_3
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;->getPhoneNumber()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 137
    .end local v2    # "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    :cond_4
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsImage:Landroid/widget/ImageView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsMaskImage:Landroid/widget/ImageView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->tvName:Landroid/widget/TextView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->tvTime:Landroid/widget/TextView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivCallTypeIcon:Landroid/widget/ImageView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 142
    iget-object v11, v4, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter$ViewHolder;->ivLogsTypeIcon:Landroid/widget/ImageView;

    const/16 v13, 0x8

    invoke-virtual {v11, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 176
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setLogsList(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "logsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mLogsList:Ljava/util/ArrayList;

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->getPageCount()I

    move-result v0

    .line 60
    .local v0, "pageCount":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 62
    const/16 v1, 0x2710

    rem-int/2addr v1, v0

    rsub-int v1, v1, 0x2710

    .line 61
    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mInitialPosition:I

    .line 63
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mPageCount:I

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mInitialPosition:I

    .line 66
    iput v0, p0, Lcom/sec/android/automotive/drivelink/phone/LogsPageAdapter;->mPageCount:I

    goto :goto_0
.end method
