.class public Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.source "LocationRequestActivity.java"


# static fields
.field public static final DLCONTACT:Ljava/lang/String; = "dlcontact"

.field public static final FLAG_FROM_REQUEST:Ljava/lang/String; = "fromRequest"

.field public static final LOGS_MODE:I = 0x20

.field public static final MAX_LOGS:I = 0xf

.field public static final MAX_SUGGESTIONS:I = 0xc

.field public static final NUMBER_MODE:I = 0x80

.field public static final PHONE_MODE:Ljava/lang/String; = "PHONE_MODE"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phoneNumber"

.field public static final SEARCH_MODE:I = 0x40

.field public static final SIP_SEARCH_MODE:Ljava/lang/String; = "SIP_SEARCH_MODE"

.field public static final SIP_SEARCH_TEXT:Ljava/lang/String; = "SIP_SEARCH_TEXT"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final SUGGESTIONS_MODE:I = 0x10

.field private static TAG:Ljava/lang/String;


# instance fields
.field private driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private isLoaded:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

.field private mAllListLayout:Landroid/widget/LinearLayout;

.field private mBackLayout:Landroid/widget/RelativeLayout;

.field private mBtnBack:Landroid/widget/ImageButton;

.field private mContactList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyListLayout:Landroid/view/View;

.field private mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

.field private mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

.field private mPageNavigationLayout:Landroid/widget/RelativeLayout;

.field private mPhoneNumber:Ljava/lang/String;

.field private mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

.field private mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

.field private mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

.field private mSIPState:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

.field protected mSearchMode:Z

.field mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

.field protected mSearchedText:Ljava/lang/String;

.field private mTextfieldLayout:Landroid/widget/RelativeLayout;

.field private requestSearchedContactList:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-string/jumbo v0, "LocationRequestActivity"

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->TAG:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;-><init>()V

    .line 81
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    .line 89
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->isLoaded:Z

    .line 112
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->requestSearchedContactList:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    .line 62
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 535
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->isLoaded:Z

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Z)V
    .locals 0

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->isLoaded:Z

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;I)V
    .locals 0

    .prologue
    .line 480
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->startConfirm(I)V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->requestSearchedContactList:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mTextfieldLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$19()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V
    .locals 0

    .prologue
    .line 451
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->setSuggetionListFlowParam()V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->finish()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 437
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .prologue
    .line 275
    const v0, 0x7f09009e

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    .line 276
    const v0, 0x7f09008a

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    .line 277
    const v0, 0x7f09009f

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    .line 279
    const v0, 0x7f09009c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mAllListLayout:Landroid/widget/LinearLayout;

    .line 281
    const v0, 0x7f09009d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 283
    const v0, 0x7f09009b

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v1, 0x7f090331

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBtnBack:Landroid/widget/ImageButton;

    .line 285
    return-void
.end method

.method private configureVoiceActionBar()V
    .locals 2

    .prologue
    const v1, 0x7f0a020f

    .line 288
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayText(I)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSayListeningText(I)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const v1, 0x7f0a0210

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setTitleBar(I)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setBtnSearchVisible()V

    .line 293
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 294
    const v1, 0x7f0a0211

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setVoiceLayoutOnClickListener(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setContactsHint()V

    .line 297
    return-void
.end method

.method private getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 536
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    return-object v0
.end method

.method private getListItemOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 438
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    return-object v0
.end method

.method private getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;
    .locals 1

    .prologue
    .line 362
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    return-object v0
.end method

.method private getMicStateChangeListener()Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;
    .locals 1

    .prologue
    .line 549
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$13;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$13;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    return-object v0
.end method

.method private getUpdateLogListListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;
    .locals 1

    .prologue
    .line 429
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    return-object v0
.end method

.method private getUpdateSuggestionContactListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;
    .locals 1

    .prologue
    .line 416
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    return-object v0
.end method

.method private initSearchTextField()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 246
    const v1, 0x7f090333

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 245
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mTextfieldLayout:Landroid/widget/RelativeLayout;

    .line 247
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 248
    const v1, 0x7f09034a

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 247
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 252
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnSearchBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 263
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setOnLocationSipStateListener(Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBtnBack:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    return-void
.end method

.method private loadAllContatcts()V
    .locals 0

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->updateContacts()V

    .line 402
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->setSuggetionListFlowParam()V

    .line 403
    return-void
.end method

.method private setInitUI(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v13, 0x0

    .line 738
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 745
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "EXTRA_FROM_FLOW_MGR"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 756
    const/4 v11, 0x0

    .line 757
    .local v11, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "EXTRA_FLOW_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 758
    .local v10, "flowID":Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v11

    .line 759
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "flow ID :: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 790
    .local v9, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const-string/jumbo v1, "DM_LOCATION_CONTACT_REQUEST"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 791
    iget-object v8, v11, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 792
    .local v8, "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v8, :cond_2

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 793
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Problem with contacts return on voice search"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 796
    :cond_3
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 813
    .end local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_4
    const-string/jumbo v1, "DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 821
    iget-object v8, v11, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 822
    .restart local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v8, :cond_5

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 823
    :cond_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Problem with contacts return on voice search"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 796
    :cond_6
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 798
    .local v7, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 797
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 799
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 800
    iget-wide v2, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 799
    invoke-virtual {v1, p0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v6

    .line 802
    .local v6, "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 803
    iget-wide v1, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    iget-object v3, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 804
    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v5

    .line 802
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 806
    .local v0, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 807
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->startConfirm(I)V

    goto :goto_1

    .line 827
    .end local v0    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v6    # "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v7    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_7
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_a

    .line 840
    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    .line 841
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->updateContacts()V

    .line 844
    .end local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_8
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 845
    :cond_9
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1, v13}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setResutlText(I)V

    .line 846
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    invoke-virtual {v1, v13}, Landroid/view/View;->setVisibility(I)V

    .line 847
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 848
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 849
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 850
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->invalidate()V

    goto/16 :goto_0

    .line 827
    .restart local v8    # "contactChoiceList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_a
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 829
    .restart local v7    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 828
    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 830
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 831
    iget-wide v2, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 830
    invoke-virtual {v1, p0, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v6

    .line 833
    .restart local v6    # "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    .line 834
    iget-wide v1, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    iget-object v3, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 835
    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v5

    .line 833
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 837
    .restart local v0    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private setSuggetionListFlowParam()V
    .locals 5

    .prologue
    .line 452
    const/4 v1, 0x0

    .line 454
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v2, "DM_SMS_CONTACT"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 456
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 467
    :cond_0
    return-void

    .line 460
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 461
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 463
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 464
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private setupListeners()V
    .locals 2

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getUpdateLogListListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .line 301
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getUpdateSuggestionContactListener()Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 303
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 304
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mOnUpdateLogListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;

    .line 303
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateLogListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateLogListListener;)V

    .line 305
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    .line 305
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setOnUpdateSugessionListListener(Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnBackBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBtnBack:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setISearchListener(Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout$ISearchListener;)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    new-instance v1, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 355
    const v1, 0x7f09034a

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 354
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    .line 358
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getLocationSipStateListener()Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->setOnLocationSipStateListener(Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$OnLocationSipStateListener;)V

    .line 359
    return-void
.end method

.method private startConfirm(I)V
    .locals 9
    .param p1, "currentItem"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 481
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->checkSimState()I

    move-result v3

    .line 483
    .local v3, "state":I
    const/4 v5, 0x5

    if-eq v3, v5, :cond_1

    .line 484
    invoke-static {p0, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->showToast(Landroid/app/Activity;I)V

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v5

    if-nez v5, :cond_2

    .line 491
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 492
    const v6, 0x7f0a0321

    .line 491
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 489
    invoke-static {p0, v5, v8}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v5

    .line 493
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 497
    :cond_2
    if-gez p1, :cond_4

    .line 498
    const/4 p1, 0x0

    .line 502
    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 504
    .local v4, "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v4, :cond_0

    .line 507
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 508
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_5

    .line 509
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPhoneNumber:Ljava/lang/String;

    .line 513
    :goto_2
    const-string/jumbo v5, "VAC_CLIENT_DM"

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 514
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 516
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {v4}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 517
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPhoneNumber:Ljava/lang/String;

    iput-object v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPhoneNum:Ljava/lang/String;

    .line 519
    const-string/jumbo v5, "DM_LOCATION_REQ_CONFIRM"

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 518
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->getPrompt(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 520
    .local v2, "prompt":Ljava/lang/String;
    new-array v6, v8, [Ljava/lang/Object;

    if-nez v4, :cond_6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 521
    :goto_3
    aput-object v5, v6, v7

    .line 520
    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 524
    const-string/jumbo v5, "DM_LOCATION_REQ_CONFIRM"

    .line 523
    invoke-static {v5, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 499
    .end local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v2    # "prompt":Ljava/lang/String;
    .end local v4    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt p1, v5, :cond_3

    .line 500
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 p1, v5, -0x1

    goto :goto_1

    .line 511
    .restart local v4    # "user":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_5
    const-string/jumbo v5, ""

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPhoneNumber:Ljava/lang/String;

    goto :goto_2

    .line 521
    .restart local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v2    # "prompt":Ljava/lang/String;
    :cond_6
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 526
    .end local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v2    # "prompt":Ljava/lang/String;
    :cond_7
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 527
    const-class v6, Lcom/sec/android/automotive/drivelink/location/LocationRequestConfirmActivity;

    .line 526
    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 528
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v5, "dlcontact"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 529
    const-string/jumbo v5, "phoneNumber"

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 530
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 619
    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->finish()V
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->access$21(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    .line 620
    const v0, 0x7f040025

    const v1, 0x7f040001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->overridePendingTransition(II)V

    .line 624
    return-void
.end method

.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 597
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 667
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onBackPressed()V

    .line 669
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 201
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 202
    const v2, 0x7f030013

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->setContentView(I)V

    .line 204
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->bindViews()V

    .line 205
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->setupListeners()V

    .line 206
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->configureVoiceActionBar()V

    .line 208
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->initSearchTextField()V

    .line 210
    if-nez p1, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchMode:Z

    .line 212
    if-nez p1, :cond_1

    move-object v2, v4

    :goto_1
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mSearchedText:Ljava/lang/String;

    .line 215
    if-eqz p1, :cond_2

    .line 216
    const-string/jumbo v2, "search_mode"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 217
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 218
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->requestSearchedContactList:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;

    invoke-interface {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 219
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->driveLinkServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 220
    const-string/jumbo v3, "search_text"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 219
    invoke-interface {v2, p0, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    .line 227
    :goto_2
    :try_start_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :goto_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updatePhoneSuggestionList()V

    .line 241
    const/high16 v2, 0x7f040000

    const v3, 0x7f040025

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->overridePendingTransition(II)V

    .line 242
    return-void

    .line 211
    :cond_0
    const-string/jumbo v2, "SIP_SEARCH_MODE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    .line 213
    :cond_1
    const-string/jumbo v2, "SIP_SEARCH_TEXT"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 222
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->loadAllContatcts()V

    goto :goto_2

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/Exception;
    const v2, 0x7f0a0296

    .line 230
    const/4 v3, 0x1

    .line 229
    invoke-static {p0, v2, v3}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v1

    .line 232
    .local v1, "toast":Landroid/widget/Toast;
    if-eqz v1, :cond_3

    .line 233
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 236
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 574
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 575
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 576
    return-void
.end method

.method protected onFlowListSelected(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "isOrdinal"    # Z

    .prologue
    .line 473
    if-eqz p3, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr p2, v0

    .line 477
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->startConfirm(I)V

    .line 478
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 731
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 732
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 733
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->setInitUI(Landroid/content/Intent;)V

    .line 735
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 580
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onPause()V

    .line 582
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 407
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 408
    const-string/jumbo v0, "search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    .line 410
    const-string/jumbo v1, "search_text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 409
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setSearchText(Ljava/lang/String;)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showSearch()V

    .line 413
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 586
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 587
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->isSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    const-string/jumbo v0, ""

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 589
    const-string/jumbo v0, "search_mode"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 590
    const-string/jumbo v0, "search_text"

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    :cond_0
    return-void
.end method

.method protected setDayMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002a

    .line 883
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setDayMode()V

    .line 885
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 886
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setDayMode(Z)V

    .line 889
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->setDayMode()V

    .line 891
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 892
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 895
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 896
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    .line 897
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 900
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    if-eqz v0, :cond_3

    .line 901
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->notifyDataSetChanged()V

    .line 902
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->updateContacts()V

    .line 904
    :cond_3
    return-void
.end method

.method protected setNightMode()V
    .locals 3

    .prologue
    const v2, 0x7f08002e

    .line 858
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->setNightMode()V

    .line 860
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    if-eqz v0, :cond_0

    .line 861
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->setNightMode(Z)V

    .line 864
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->setNightMode()V

    .line 866
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 867
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 870
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 871
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    .line 872
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 875
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    if-eqz v0, :cond_3

    .line 876
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->notifyDataSetChanged()V

    .line 877
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->updateContacts()V

    .line 879
    :cond_3
    return-void
.end method

.method public updateContacts()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 672
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 673
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 728
    :goto_0
    return-void

    .line 675
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mEmptyListLayout:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 677
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->adjustToMaxSizeSuggestionList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 676
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    .line 678
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    .line 679
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mContactList:Ljava/util/ArrayList;

    .line 680
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getListItemOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;)V

    .line 678
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    .line 681
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 682
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 683
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;-><init>(Landroid/content/Context;)V

    .line 682
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 684
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 685
    const/4 v1, -0x1

    const/4 v2, -0x2

    .line 684
    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 686
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 687
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 688
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 689
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v5}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 690
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setGravity(I)V

    .line 691
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mPageNavigationLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 692
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    .line 693
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v2

    .line 694
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 692
    invoke-virtual {v1, v2, v5, v3}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setDotIndicator(IILandroid/view/View$OnClickListener;)V

    .line 695
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;

    .line 696
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity$14;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationRequestActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto :goto_0
.end method
