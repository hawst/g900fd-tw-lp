.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationMusicView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;,
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;,
        Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;
    }
.end annotation


# static fields
.field private static final DELAY_TIME_FOR_AUTO_SHRINK:J = 0x1388L

.field private static final FFORWARD_REWIND_DELAY_TIME:I = 0x190

.field private static final MSG_MULTI_AUTO_SHRINK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "[NotificationMusicView]"


# instance fields
.field private bitmapAlbum:Landroid/graphics/Bitmap;

.field private isAlbumNull:Z

.field private isNight:Z

.field private item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

.field private longPressed:Z

.field private mArtistName:Landroid/widget/TextView;

.field private mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

.field mContext:Landroid/content/Context;

.field private mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

.field private mHandler:Landroid/os/Handler;

.field mLayoutBase:Landroid/widget/RelativeLayout;

.field mLayoutContents:Landroid/widget/LinearLayout;

.field private mLongClickState:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;

.field private mMusicBackgroundGradient:Landroid/widget/ImageView;

.field mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

.field private mNextBtn:Landroid/widget/Button;

.field private mPlaypauseBtn:Landroid/widget/Button;

.field private mPrevBtn:Landroid/widget/Button;

.field private mShrinkBtn:Landroid/widget/ImageView;

.field private mSongThumbnail:Landroid/widget/ImageView;

.field private mSongTitle:Landroid/widget/TextView;

.field private startTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 109
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 63
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    .line 64
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutContents:Landroid/widget/LinearLayout;

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    .line 80
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isNight:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isAlbumNull:Z

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->startTime:J

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    .line 89
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;->NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLongClickState:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;

    .line 673
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 738
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mHandler:Landroid/os/Handler;

    .line 110
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->initNotification(Landroid/content/Context;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 103
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 63
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    .line 64
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutContents:Landroid/widget/LinearLayout;

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    .line 80
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isNight:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isAlbumNull:Z

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->startTime:J

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    .line 89
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;->NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLongClickState:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;

    .line 673
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 738
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mHandler:Landroid/os/Handler;

    .line 104
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->initNotification(Landroid/content/Context;)V

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 63
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    .line 64
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutContents:Landroid/widget/LinearLayout;

    .line 65
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    .line 80
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isNight:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isAlbumNull:Z

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->startTime:J

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    .line 89
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;->NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLongClickState:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$LongClickState;

    .line 673
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 738
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mHandler:Landroid/os/Handler;

    .line 98
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->initNotification(Landroid/content/Context;)V

    .line 99
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V
    .locals 0

    .prologue
    .line 507
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setMusicInfo()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V
    .locals 0

    .prologue
    .line 752
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isNight:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V
    .locals 0

    .prologue
    .line 834
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setNightMode()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V
    .locals 0

    .prologue
    .line 860
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setDayMode()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;ZLcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;)V
    .locals 0

    .prologue
    .line 765
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setButtonClickable(ZLcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;)V

    return-void
.end method

.method private drawAlbumBitmap(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 577
    const-string/jumbo v1, "[NotificationMusicView]"

    const-string/jumbo v2, "Album info in Mini : start"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 579
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 580
    iput-object v7, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 583
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 585
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 587
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v6, :cond_3

    .line 588
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    const/16 v2, 0x186

    .line 589
    const/16 v3, 0x186

    const/4 v4, 0x1

    .line 588
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 599
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    if-nez v1, :cond_5

    .line 600
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isAlbumNull:Z

    .line 602
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 603
    const v2, 0x7f02027e

    .line 602
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 604
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v6, :cond_4

    .line 605
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    const/16 v2, 0x186

    .line 606
    const/16 v3, 0x186

    const/4 v4, 0x1

    .line 605
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 618
    :goto_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->recycleBitmap(Landroid/widget/ImageView;)V

    .line 619
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 620
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 621
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongThumbnail:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 623
    :cond_2
    return-void

    .line 591
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    const/16 v2, 0xb4

    .line 592
    const/16 v3, 0xb4

    const/4 v4, 0x1

    .line 591
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 608
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    const/16 v2, 0xb4

    .line 609
    const/16 v3, 0xb4

    const/4 v4, 0x1

    .line 608
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 611
    :catch_1
    move-exception v0

    .line 612
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 615
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isAlbumNull:Z

    goto :goto_1
.end method

.method private drawUnknownBitmap()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 626
    const-string/jumbo v1, "[NotificationMusicView]"

    const-string/jumbo v2, "Album info in Mini : start"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 629
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 633
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 634
    const v2, 0x7f02027e

    .line 633
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 636
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    const/16 v2, 0x186

    const/16 v3, 0x186

    .line 637
    const/4 v4, 0x1

    .line 636
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->recycleBitmap(Landroid/widget/ImageView;)V

    .line 647
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 648
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 649
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongThumbnail:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 651
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 652
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 653
    const v3, 0x7f080078

    .line 652
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 654
    return-void

    .line 639
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    const/16 v2, 0xb4

    const/16 v3, 0xb4

    .line 640
    const/4 v4, 0x1

    .line 639
    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 642
    :catch_0
    move-exception v0

    .line 643
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static recycleBitmap(Landroid/widget/ImageView;)V
    .locals 5
    .param p0, "iv"    # Landroid/widget/ImageView;

    .prologue
    .line 657
    if-nez p0, :cond_0

    .line 671
    :goto_0
    return-void

    .line 660
    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 661
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    instance-of v2, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_2

    move-object v2, v1

    .line 662
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 663
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 664
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 665
    const/4 v0, 0x0

    .line 667
    :cond_1
    const-string/jumbo v2, "[NotificationMusicView]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "recycleBitmap"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    .end local v0    # "b":Landroid/graphics/Bitmap;
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0
.end method

.method private setAutoShrink(Z)V
    .locals 5
    .param p1, "isOn"    # Z

    .prologue
    const/4 v4, 0x0

    .line 753
    if-eqz p1, :cond_0

    .line 755
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "MUSIC"

    const-string/jumbo v2, "[NotificationMusicView]"

    const-string/jumbo v3, "MSG_MULTI_AUTO_SHRINK - start"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mHandler:Landroid/os/Handler;

    .line 757
    const-wide/16 v1, 0x1388

    .line 756
    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 763
    :goto_0
    return-void

    .line 759
    :cond_0
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "MUSIC"

    const-string/jumbo v2, "[NotificationMusicView]"

    .line 760
    const-string/jumbo v3, "MSG_MULTI_AUTO_SHRINK - removing"

    .line 759
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setButtonClickable(ZLcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;)V
    .locals 1
    .param p1, "value"    # Z
    .param p2, "state"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    .prologue
    .line 766
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->NEXT_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    if-ne p2, v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    .line 776
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    .line 777
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mShrinkBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 778
    return-void

    .line 768
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->PREV_BTN_CLICKED:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    if-ne p2, v0, :cond_1

    .line 769
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_0

    .line 771
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    .line 772
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    .line 773
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 774
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setButtonPressed(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 781
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setPressed(Z)V

    .line 782
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setPressed(Z)V

    .line 783
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setPressed(Z)V

    .line 784
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mShrinkBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 785
    return-void
.end method

.method private setDayMode()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 861
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isAlbumNull:Z

    if-nez v0, :cond_1

    .line 862
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getImage(Landroid/graphics/Bitmap;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 864
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_0

    .line 865
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 866
    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 884
    :goto_0
    return-void

    .line 868
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 869
    const v1, 0x7f0202f7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 872
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 873
    const v2, 0x7f080078

    .line 872
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 874
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 876
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_2

    .line 877
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 878
    const v1, 0x7f02003b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 880
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 881
    const v1, 0x7f0202f6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private setFFREThreadWithStop()V
    .locals 3

    .prologue
    .line 819
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    if-eqz v1, :cond_0

    .line 821
    :try_start_0
    const-string/jumbo v1, "[NotificationMusicView]"

    const-string/jumbo v2, "FForwardRewindThread stop"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isRunning:Z

    .line 823
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->interrupt()V

    .line 824
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 829
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    .line 831
    :cond_0
    return-void

    .line 825
    :catch_0
    move-exception v0

    .line 826
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 827
    const-string/jumbo v1, "[NotificationMusicView]"

    const-string/jumbo v2, "FForwardRewindThread join exception"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setMusicInfo()V
    .locals 5

    .prologue
    .line 508
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 512
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 513
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v0, :cond_0

    .line 515
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "MUSIC"

    const-string/jumbo v2, "[NotificationMusicView]"

    .line 516
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " setMusicInfo  getTitle: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 515
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 519
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 524
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mArtistName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getArtist()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->item:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->drawAlbumBitmap(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V

    .line 535
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isNight:Z

    if-eqz v0, :cond_3

    .line 536
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setNightMode()V

    .line 540
    :goto_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$6;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$6;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    .line 547
    const-wide/16 v2, 0x1f4

    .line 540
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 521
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_1

    .line 538
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setDayMode()V

    goto :goto_2
.end method

.method private setNextPrevButtonProcess(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    const/4 v1, 0x0

    .line 250
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setAllMusicShuffleList()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setUnkownInfo()V

    .line 270
    :goto_0
    return-void

    .line 257
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setButtonClickable(ZLcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;)V

    .line 258
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isCalling()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_3

    .line 259
    :cond_1
    if-eqz p1, :cond_2

    .line 260
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayerNoPlay()V

    goto :goto_0

    .line 262
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayerNoPlay()V

    goto :goto_0

    .line 265
    :cond_3
    if-eqz p1, :cond_4

    .line 266
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayer()V

    goto :goto_0

    .line 268
    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayer(Z)V

    goto :goto_0
.end method

.method private setNightMode()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 835
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->isAlbumNull:Z

    if-nez v0, :cond_1

    .line 836
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/CollectColor;->getImage(Landroid/graphics/Bitmap;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 838
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_0

    .line 839
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 840
    const v1, 0x7f02003d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 858
    :goto_0
    return-void

    .line 842
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 843
    const v1, 0x7f0202f8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 846
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 847
    const v2, 0x7f080079

    .line 846
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 848
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 850
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_2

    .line 851
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 852
    const v1, 0x7f02003b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 854
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 855
    const v1, 0x7f0202f6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private setUnkownInfo()V
    .locals 3

    .prologue
    .line 887
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 889
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a036d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 890
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mArtistName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a036e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 892
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 894
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->drawUnknownBitmap()V

    .line 895
    return-void
.end method


# virtual methods
.method protected getBaseTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 339
    const-string/jumbo v0, ""

    .line 341
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutContents:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutContents:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "tag":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 344
    .restart local v0    # "tag":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public init(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 411
    const v0, 0x7f0902a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mLayoutBase:Landroid/widget/RelativeLayout;

    .line 413
    const v0, 0x7f0900cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 412
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicBackgroundGradient:Landroid/widget/ImageView;

    .line 414
    const v0, 0x7f0900cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongTitle:Landroid/widget/TextView;

    .line 415
    const v0, 0x7f0900d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mArtistName:Landroid/widget/TextView;

    .line 416
    const v0, 0x7f0900d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mSongThumbnail:Landroid/widget/ImageView;

    .line 417
    const v0, 0x7f0900d8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    .line 418
    const v0, 0x7f0900d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    .line 419
    const v0, 0x7f0900d9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    .line 420
    const v0, 0x7f0902a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mShrinkBtn:Landroid/widget/ImageView;

    .line 422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mShrinkBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 429
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 432
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mNextBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$4;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$5;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 500
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 505
    :goto_0
    return-void

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_0
.end method

.method protected initNotification(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    if-nez p1, :cond_0

    .line 115
    const-string/jumbo v3, "[NotificationMusicView]"

    const-string/jumbo v4, "Context is null!"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :goto_0
    return-void

    .line 118
    :cond_0
    const-string/jumbo v3, "[NotificationMusicView]"

    const-string/jumbo v4, "initNotification"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const-string/jumbo v3, "i"

    const-string/jumbo v4, "MUSIC"

    const-string/jumbo v5, "[NotificationMusicView]"

    const-string/jumbo v6, "initNotification"

    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 124
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 123
    check-cast v0, Landroid/view/LayoutInflater;

    .line 126
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 128
    .local v1, "layout":I
    const v1, 0x7f0300c0

    .line 130
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 132
    .local v2, "view":Landroid/view/View;
    new-instance v3, Lcom/sec/android/automotive/drivelink/music/CollectColor;

    invoke-direct {v3}, Lcom/sec/android/automotive/drivelink/music/CollectColor;-><init>()V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mCollectColor:Lcom/sec/android/automotive/drivelink/music/CollectColor;

    .line 133
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->init(Landroid/view/View;)V

    .line 134
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setMusicInfo()V

    .line 135
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 349
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    .line 350
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 351
    .local v0, "id":I
    sparse-switch v0, :sswitch_data_0

    .line 407
    :goto_0
    return-void

    .line 353
    :sswitch_0
    const-string/jumbo v1, "[NotificationMusicView]"

    const-string/jumbo v2, "shrink_btn_player clicked"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setFFREThreadWithStop()V

    .line 355
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->cancelNotiFlow()V

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->dismiss()V

    goto :goto_0

    .line 360
    :sswitch_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;->NONE:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;

    invoke-direct {p0, v3, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setButtonClickable(ZLcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$ClickState;)V

    .line 361
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1, v4, v4, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 363
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setSelected(Z)V

    .line 364
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    .line 365
    const-string/jumbo v1, "[NotificationMusicView]"

    .line 366
    const-string/jumbo v2, "MUSICService Touch NotiMusicView User Setting Pause"

    .line 365
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    goto :goto_0

    .line 369
    :cond_0
    const-string/jumbo v1, "[NotificationMusicView]"

    .line 370
    const-string/jumbo v2, "MusicService setMusicPlayerVolume by NotiMusicView User Touched"

    .line 369
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 372
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->playPlayer()V

    .line 373
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 374
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    goto :goto_0

    .line 379
    :sswitch_2
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    if-eqz v1, :cond_1

    .line 380
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setFFREThreadWithStop()V

    .line 381
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    goto :goto_0

    .line 385
    :cond_1
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setNextPrevButtonProcess(Z)V

    .line 386
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setFFREThreadWithStop()V

    .line 387
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    goto :goto_0

    .line 392
    :sswitch_3
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    if-eqz v1, :cond_2

    .line 393
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setFFREThreadWithStop()V

    .line 394
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    goto/16 :goto_0

    .line 398
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setNextPrevButtonProcess(Z)V

    .line 399
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setFFREThreadWithStop()V

    .line 400
    invoke-direct {p0, v4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    goto/16 :goto_0

    .line 351
    :sswitch_data_0
    .sparse-switch
        0x7f0900d7 -> :sswitch_3
        0x7f0900d8 -> :sswitch_1
        0x7f0900d9 -> :sswitch_2
        0x7f0902a2 -> :sswitch_0
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 900
    const-string/jumbo v2, "[NotificationMusicView]"

    const-string/jumbo v3, "Carmode_Music_onLongClick "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 919
    :cond_1
    :goto_0
    return v0

    .line 905
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 906
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setFFREInit()V

    .line 908
    :cond_3
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->longPressed:Z

    .line 910
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mPrevBtn:Landroid/widget/Button;

    if-ne p1, v2, :cond_4

    .line 911
    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    .line 915
    :goto_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    if-eqz v2, :cond_1

    .line 916
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    iput-boolean v1, v2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->isRunning:Z

    .line 917
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;->start()V

    goto :goto_0

    .line 913
    :cond_4
    new-instance v2, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mFFREThread:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$FForwardRewindThread;

    goto :goto_1
.end method

.method public onNotificationAppeared()V
    .locals 3

    .prologue
    .line 141
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-nez v1, :cond_0

    .line 150
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->registerListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    .line 149
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationAppeared()V

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "[NotificationMusicView]"

    const-string/jumbo v2, "Music Service is Null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onNotificationDisappeared()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 166
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->setListener(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;)V

    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 170
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->bitmapAlbum:Landroid/graphics/Bitmap;

    .line 173
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationDisappeared()V

    .line 174
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "MUSIC"

    const-string/jumbo v2, "[NotificationMusicView]"

    const-string/jumbo v3, "onNotificationDisappeared"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public onNotificationRotating()V
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setFFREThreadWithStop()V

    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V

    .line 201
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationRotating()V

    .line 202
    return-void
.end method

.method public onNotificationWillDisappear()V
    .locals 3

    .prologue
    .line 155
    :try_start_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 156
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->unregisterListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationWillDisappear()V

    .line 162
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "[NotificationMusicView]"

    const-string/jumbo v2, "Music Service is Null"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resumeNotiFlow()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;

    move-result-object v0

    .line 180
    new-instance v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$3;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    .line 179
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager;->setListener(Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;)V

    .line 194
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->resumeNotiFlow()V

    .line 195
    return-void
.end method
