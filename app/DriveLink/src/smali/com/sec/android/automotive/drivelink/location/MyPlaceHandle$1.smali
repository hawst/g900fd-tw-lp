.class Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;
.super Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
.source "MyPlaceHandle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->getLocationBaseListener()Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    .line 59
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const-string/jumbo v4, "MyPlaceHandle"

    const-string/jumbo v5, "onResponseRequestRecommendedLocationList"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 68
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    .line 69
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    move-result-object v5

    .line 68
    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->removeLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 72
    :cond_0
    if-nez p1, :cond_2

    .line 73
    const-string/jumbo v4, "MyPlaceHandle"

    const-string/jumbo v5, "don\u00b4t have recommended locations."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_1
    :goto_0
    return-void

    .line 77
    :cond_2
    const-string/jumbo v4, "MyPlaceHandle"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "onResponseRequestRecommendedLocationList: size: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 77
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v2, "locationListResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const/4 v1, 0x0

    .line 82
    .local v1, "home":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    const/4 v3, 0x0

    .line 85
    .local v3, "office":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_6

    .line 109
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$1(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 110
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$1(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;->responseHome(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)V

    .line 113
    :cond_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$1(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 114
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$1(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;->responseOffice(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;)V

    .line 117
    :cond_5
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$1(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 118
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->access$1(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;->response(Ljava/util/List;)V

    goto :goto_0

    .line 85
    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 90
    .local v0, "dlLocationItem":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 91
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "location_home"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 93
    const-string/jumbo v5, "MyPlaceHandle"

    .line 94
    const-string/jumbo v6, "onResponseRequestRecommendedLocationList FOUND : location_home"

    .line 93
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    move-object v1, v0

    .line 97
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 98
    :cond_7
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 99
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "location_office"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 101
    const-string/jumbo v5, "MyPlaceHandle"

    .line 102
    const-string/jumbo v6, "onResponseRequestRecommendedLocationList FOUND : location_office"

    .line 101
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    move-object v3, v0

    .line 105
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method
