.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$OnPhoneSipStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getPhoneSipStateListener()Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$OnPhoneSipStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$phone$EditTextForPhoneSIP$PhoneSIPState:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$phone$EditTextForPhoneSIP$PhoneSIPState()[I
    .locals 3

    .prologue
    .line 468
    sget-object v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$phone$EditTextForPhoneSIP$PhoneSIPState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->values()[Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$phone$EditTextForPhoneSIP$PhoneSIPState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnStateChanged(Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;)V
    .locals 5
    .param p1, "state"    # Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 472
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$27(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;)V

    .line 474
    invoke-static {}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$phone$EditTextForPhoneSIP$PhoneSIPState()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/phone/EditTextForPhoneSIP$PhoneSIPState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 535
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 483
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$18(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 484
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 486
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 487
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 488
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    .line 490
    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->startPhraseSpotting()V

    .line 492
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$18(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 493
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$15(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->showVoiceLayout()V

    .line 497
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v2

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 499
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$19(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 500
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$20(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 501
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$21(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 502
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$22(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 504
    const-string/jumbo v1, "PhoneActivity"

    .line 505
    const-string/jumbo v2, "getMessageSipStateListener - setAutoShrink : true"

    .line 504
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    goto/16 :goto_0

    .line 474
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
