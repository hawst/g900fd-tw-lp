.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicPlayerListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkMusicPlayerListener"
.end annotation


# virtual methods
.method public abstract onMusicPlayCompleted(Landroid/media/MediaPlayer;)V
.end method

.method public abstract onMusicPlayError(Landroid/media/MediaPlayer;IILjava/lang/Object;)V
.end method

.method public abstract onMusicPlayPaused(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;I)V
.end method

.method public abstract onMusicPlayResumed(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
.end method

.method public abstract onMusicPlaySeekComplete(Landroid/media/MediaPlayer;)V
.end method

.method public abstract onMusicPlayStarted(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;Z)V
.end method

.method public abstract onMusicPlayStopped(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
.end method

.method public abstract onMusicPlayerEnvInfoChanged(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerEnvInfo;)V
.end method
