.class Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;
.super Landroid/os/Handler;
.source "DisclaimerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 206
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$2(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$3(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;I)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mWarningProgress:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$4(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mWarningProgress:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$4(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$2(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$2(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)I

    move-result v0

    const/16 v1, 0x46

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBlockBackKey:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$5(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->access$6(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Z)V

    .line 218
    :cond_0
    return-void
.end method
