.class Lcom/sec/android/automotive/drivelink/music/MusicService$5;
.super Landroid/os/Handler;
.source "MusicService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 2039
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x3dcccccd    # 0.1f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 2043
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Message : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2044
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mCurrentVolume : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v4, v4, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2045
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mMusicDelaycount : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v4, v4, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2046
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 2093
    :cond_0
    :goto_0
    return-void

    .line 2048
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    sub-float/2addr v1, v6

    iput v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    .line 2049
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_1

    .line 2050
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$23(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2054
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v1, v1, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setVolume(F)V

    goto :goto_0

    .line 2052
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    goto :goto_1

    .line 2057
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 2058
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    .line 2059
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "[CarMode]"

    const-string/jumbo v2, "MUSICService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "FADEUP Delay count : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v4, v4, Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicDelaycount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2060
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$23(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v9, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 2065
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    const v2, 0x3c23d70a    # 0.01f

    add-float/2addr v1, v2

    iput v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    .line 2066
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_3

    .line 2067
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMediaplayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$23(Lcom/sec/android/automotive/drivelink/music/MusicService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v9, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2071
    :goto_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mMusicController:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v1, v1, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->setVolume(F)V

    .line 2072
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 2069
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iput v5, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->mCurrentVolume:F

    goto :goto_2

    .line 2077
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2078
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayer()V

    .line 2081
    :goto_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    .line 2082
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-static {v0, v8}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$24(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    goto/16 :goto_0

    .line 2080
    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayerNoPlay()V

    goto :goto_3

    .line 2085
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2086
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayer(Z)V

    .line 2089
    :goto_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusicDuration()I

    move-result v1

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getSeekFFRETime()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    .line 2090
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$5;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-static {v0, v8}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$24(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    goto/16 :goto_0

    .line 2088
    :cond_5
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayerNoPlay()V

    goto :goto_4

    .line 2046
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x6 -> :sswitch_1
        0xc350 -> :sswitch_2
        0xc351 -> :sswitch_3
    .end sparse-switch
.end method
