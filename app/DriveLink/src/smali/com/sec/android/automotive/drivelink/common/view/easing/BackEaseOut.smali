.class public Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseOut;
.super Ljava/lang/Object;
.source "BackEaseOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseOut;->s:F

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseOut;->s:F

    .line 15
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 5
    .param p1, "input"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 18
    div-float v1, p1, v4

    sub-float p1, v1, v4

    mul-float v1, p1, p1

    iget v2, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseOut;->s:F

    add-float/2addr v2, v4

    mul-float/2addr v2, p1

    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseOut;->s:F

    add-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v1, v4

    mul-float/2addr v1, v4

    const/4 v2, 0x0

    add-float v0, v1, v2

    .line 19
    .local v0, "r":F
    return v0
.end method
