.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->OnInboxSearchChoice(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    .line 2335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2370
    const-string/jumbo v0, "MessageActivity"

    .line 2371
    const-string/jumbo v1, "OnInboxSearchChoice - setAutoShrink : false n true"

    .line 2370
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2372
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2373
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 2374
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 2349
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->isLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2350
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;Z)V

    .line 2351
    if-nez p1, :cond_1

    .line 2352
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 2353
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    .line 2354
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getInitialPosition()I

    move-result v1

    .line 2353
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2355
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2356
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2357
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v0

    .line 2358
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v0

    .line 2357
    if-eqz v0, :cond_0

    .line 2359
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2366
    :cond_0
    :goto_0
    return-void

    .line 2361
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2362
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    .line 2363
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2361
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2339
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2340
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 2341
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3$4;->this$1:Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity$3;)Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    move-result-object v1

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxChoicePageAdapter:Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$34(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;

    move-result-object v1

    .line 2342
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageInboxPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 2340
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 2344
    :cond_0
    return-void
.end method
