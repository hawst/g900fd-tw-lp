.class public Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;
.super Ljava/lang/Object;
.source "DLWakeupManager.java"


# static fields
.field public static final BG_WAKEUP_AP:Ljava/lang/String; = "BG_WAKEUP_AP"

.field public static final BG_WAKEUP_CP:Ljava/lang/String; = "BG_WAKEUP_CP"

.field public static final BG_WAKEUP_NONE:Ljava/lang/String; = "BG_WAKEUP_NONE"

.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

.field private static mWakeupState:Ljava/lang/String;


# instance fields
.field private mAudioFocusLossHandler:Landroid/os/Handler;

.field mAudioMgr:Landroid/media/AudioManager;

.field private mChipWakeupFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    .line 25
    const-string/jumbo v0, "BG_WAKEUP_NONE"

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mWakeupState:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioMgr:Landroid/media/AudioManager;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 160
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$1;-><init>(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mChipWakeupFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 193
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager$2;-><init>(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioFocusLossHandler:Landroid/os/Handler;

    .line 30
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioFocusLossHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startChipWakeupBargeIn(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 220
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;)Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mChipWakeupFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->isEnableWakeup(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startAPWakeupBargeIn(Landroid/content/Context;)V

    return-void
.end method

.method private getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioMgr:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 223
    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 222
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioMgr:Landroid/media/AudioManager;

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioMgr:Landroid/media/AudioManager;

    return-object v0
.end method

.method public static getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    .line 36
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mInstance:Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    return-object v0
.end method

.method private isEnableWakeup(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 61
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 63
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->isCarModePaused(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "isEnableWakeup : false - isCarMode not paused"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :goto_0
    return v0

    .line 68
    :cond_0
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->isCallStateIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "isEnableWakeup : false - Call State in not Idle"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 73
    :cond_1
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInitState()I

    move-result v1

    if-eqz v1, :cond_2

    .line 74
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "isEnableWakeup : false - VAC not initialized"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 78
    :cond_2
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->isSvoiceRunning(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 79
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "isEnableWakeup : false - S Voice is runnig"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 83
    :cond_3
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "isEnableWakeup : true "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setWakeupState(Ljava/lang/String;)V
    .locals 3
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 152
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setWakeupState : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    sput-object p1, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mWakeupState:Ljava/lang/String;

    .line 154
    return-void
.end method

.method private startAPWakeupBargeIn(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 90
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startAPWakeupBargeIn : PhraseSpotter"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string/jumbo v0, "DLPhraseSpotter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[stop] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 93
    const-string/jumbo v2, " - startWakeupBargeIn() : AP wakeup start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 96
    const-string/jumbo v0, "BG_WAKEUP_AP"

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->setWakeupState(Ljava/lang/String;)V

    .line 98
    const-string/jumbo v0, "DLPhraseSpotter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[start] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 99
    const-string/jumbo v2, " - startWakeupBargeIn() : AP wakeup start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 106
    return-void
.end method

.method private startChipWakeupBargeIn(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 111
    const-string/jumbo v0, "TAG"

    const-string/jumbo v1, "reset Chipset"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->stopWakeupBargeIn(Landroid/content/Context;Z)V

    .line 114
    const-string/jumbo v0, "DLPhraseSpotter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[stop] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 115
    const-string/jumbo v2, " - startWakeupBargeIn() : CP wakeup start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 118
    const-string/jumbo v0, "BG_WAKEUP_CP"

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->setWakeupState(Ljava/lang/String;)V

    .line 120
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startChipWakeupBargeIn : voice_wakeup_mic=on"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_wakeup_mic=on"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 124
    const-string/jumbo v0, "CM22"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 126
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mChipWakeupFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 127
    const/16 v2, 0x9

    .line 128
    const/4 v3, 0x3

    .line 126
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 129
    return-void
.end method


# virtual methods
.method public getWakeupState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mWakeupState:Ljava/lang/String;

    return-object v0
.end method

.method public onApWakeupStopped(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 232
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    .line 233
    const-string/jumbo v2, "onApWakeupStopped: send broadCast ACTION_TASK_RESTART_BG_WAKEUP"

    .line 232
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 236
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.automotive.drivelink.ACTION_TASK_RESTART_BG_WAKEUP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string/jumbo v1, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 242
    return-void
.end method

.method public startWakeupBargeIn(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 42
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startWakeupBargeIn "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->isEnableWakeup(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startChipWakeupBargeIn(Landroid/content/Context;)V

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->startAPWakeupBargeIn(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public stopWakeupBargeIn(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isCloseApWakeup"    # Z

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mContext:Landroid/content/Context;

    .line 134
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mChipWakeupFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 135
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->mAudioFocusLossHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 137
    const-string/jumbo v0, "BG_WAKEUP_NONE"

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->setWakeupState(Ljava/lang/String;)V

    .line 139
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopWakeupBargeIn : voice_wakeup_mic=off"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getAudioManager(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 144
    :cond_0
    if-eqz p2, :cond_1

    .line 145
    const-string/jumbo v0, "DLPhraseSpotter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[stop] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - stopWakeupBargeIn"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 149
    :cond_1
    return-void
.end method
