.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationGroup;
.source "DLLocationGroupImp.java"


# instance fields
.field private mGroupID:Ljava/lang/String;

.field private mLocationBuddies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationGroup;-><init>()V

    .line 8
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mLocationBuddies:Ljava/util/ArrayList;

    .line 9
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mGroupID:Ljava/lang/String;

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mLocationBuddies:Ljava/util/ArrayList;

    .line 13
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mGroupID:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public addLocationBuddy(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;)V
    .locals 1
    .param p1, "buddy"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mLocationBuddies:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public getGroupID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mGroupID:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationBuddies()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mLocationBuddies:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setGroupID(Ljava/lang/String;)V
    .locals 0
    .param p1, "groupID"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroupImp;->mGroupID:Ljava/lang/String;

    .line 23
    return-void
.end method
