.class public Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SettingsBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private connected:Z

.field private connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

.field private connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

.field settingsBroadcastListener:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;

    .line 19
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 18
    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;)V
    .locals 2
    .param p1, "settingsBroadcastListener"    # Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->settingsBroadcastListener:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 40
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 41
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 42
    return-void
.end method


# virtual methods
.method protected getConnectedDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method protected getConnectedDevice2()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method protected isConnected()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v5, -0x80000000

    .line 47
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v4, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 53
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 55
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    const-string/jumbo v4, "android.bluetooth.device.extra.BOND_STATE"

    .line 54
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 57
    .local v3, "state":I
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 58
    const-string/jumbo v6, " state is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 57
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->settingsBroadcastListener:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;

    invoke-interface {v4, v1, v3}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;->onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V

    .line 164
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const-string/jumbo v4, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 62
    const-string/jumbo v4, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 65
    .restart local v3    # "state":I
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 86
    :pswitch_0
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 87
    .local v2, "disconnectedDevice":Landroid/bluetooth/BluetoothDevice;
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 88
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "BluetoothHeadset STATE_DISCONNECTED "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 88
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 87
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-eqz v4, :cond_2

    .line 91
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 92
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 91
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 92
    if-eqz v4, :cond_2

    .line 93
    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 95
    :cond_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-nez v4, :cond_3

    .line 96
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-nez v4, :cond_3

    .line 97
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 99
    :cond_3
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-eqz v4, :cond_4

    .line 100
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 101
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 100
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 101
    if-nez v4, :cond_0

    .line 103
    :cond_4
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->settingsBroadcastListener:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;

    .line 104
    invoke-interface {v4, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;->onDisconnect(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 68
    .end local v2    # "disconnectedDevice":Landroid/bluetooth/BluetoothDevice;
    :pswitch_1
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 69
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 70
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "BluetoothHeadset STATE_CONNECTED "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 72
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 73
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-eqz v4, :cond_5

    .line 74
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 75
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 74
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 75
    if-nez v4, :cond_0

    .line 77
    :cond_5
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->settingsBroadcastListener:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;

    invoke-interface {v4, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;->onConnect(Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 81
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :pswitch_2
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 82
    const-string/jumbo v5, "BluetoothHeadset STATE_CONNECTING"

    .line 81
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 108
    :pswitch_3
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 109
    const-string/jumbo v5, "BluetoothHeadset STATE_DISCONNECTING"

    .line 108
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 112
    .end local v3    # "state":I
    :cond_6
    const-string/jumbo v4, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    const-string/jumbo v4, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 116
    .restart local v3    # "state":I
    packed-switch v3, :pswitch_data_1

    goto/16 :goto_0

    .line 137
    :pswitch_4
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 138
    .restart local v2    # "disconnectedDevice":Landroid/bluetooth/BluetoothDevice;
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 139
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "BluetoothA2dp STATE_DISCONNECTED "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 140
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 139
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 138
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-eqz v4, :cond_7

    .line 142
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 143
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 142
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 143
    if-eqz v4, :cond_7

    .line 144
    iput-object v9, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 146
    :cond_7
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-nez v4, :cond_8

    .line 147
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    if-nez v4, :cond_8

    .line 148
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 150
    :cond_8
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-eqz v4, :cond_9

    .line 151
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 152
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 151
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 152
    if-nez v4, :cond_0

    .line 154
    :cond_9
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->settingsBroadcastListener:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;

    .line 155
    invoke-interface {v4, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;->onDisconnect(Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 119
    .end local v2    # "disconnectedDevice":Landroid/bluetooth/BluetoothDevice;
    :pswitch_5
    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 120
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 121
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "BluetoothA2dp STATE_CONNECTED "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 120
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 123
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    .line 124
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    if-eqz v4, :cond_a

    .line 125
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 126
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 125
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 126
    if-nez v4, :cond_0

    .line 128
    :cond_a
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->settingsBroadcastListener:Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;

    invoke-interface {v4, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver$SettingsBroadcastListener;->onConnect(Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 132
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :pswitch_6
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 133
    const-string/jumbo v5, "BluetoothA2dp STATE_CONNECTING"

    .line 132
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 159
    :pswitch_7
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 160
    const-string/jumbo v5, "BluetoothA2dp STATE_DISCONNECTING"

    .line 159
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 116
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public register(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 207
    new-instance v0, Landroid/content/IntentFilter;

    .line 208
    const-string/jumbo v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 209
    new-instance v0, Landroid/content/IntentFilter;

    .line 210
    const-string/jumbo v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 211
    new-instance v0, Landroid/content/IntentFilter;

    .line 212
    const-string/jumbo v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 213
    return-void
.end method

.method public setInitialValues(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;)V
    .locals 6
    .param p1, "driveLinkServiceInterface"    # Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 226
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setInitialValues"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "setInitialValues.isConnected"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 234
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "AutoLaunchReceiver connected"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 237
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDeviceHeadset()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 236
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 239
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDeviceA2DP()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 238
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0

    .line 242
    :cond_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 243
    const-string/jumbo v2, "AutoLaunchReceiver not connected"

    .line 242
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 246
    .local v0, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_2

    .line 247
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getConnectionState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 249
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getBluetoothCommunicatingState(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 250
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 251
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 252
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "ConnectivityManager connected to device "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 253
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    .line 254
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 253
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 252
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 251
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iput-boolean v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 257
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 256
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    .line 259
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 258
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceA2DP:Landroid/bluetooth/BluetoothDevice;

    goto :goto_0

    .line 261
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 262
    const-string/jumbo v2, "BluetoothAdapter is null, definitely not connected!"

    .line 261
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 264
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "ConnectivityManager "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 266
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getBluetoothCommunicatingState(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, ""

    .line 265
    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 267
    const-string/jumbo v3, " connected"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 264
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 263
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 269
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "ConnectivityManager device"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 270
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-nez v1, :cond_4

    const-string/jumbo v1, " null"

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 269
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 268
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connected:Z

    .line 274
    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsBroadcastReceiver;->connectedDeviceHeadset:Landroid/bluetooth/BluetoothDevice;

    goto/16 :goto_0

    .line 267
    :cond_3
    const-string/jumbo v1, " not"

    goto :goto_1

    .line 271
    :cond_4
    invoke-interface {p1, v4}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getDevice(Landroid/content/Context;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 272
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
