.class public final Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;
.super Ljava/lang/Object;
.source "GroupMarker.java"


# static fields
.field private static OFFSET:I = 0x0

.field private static RADIUS:D = 0.0

.field private static final THRESHOLD_DISTANCE_MARKERS:I = 0x28

.field private static pi:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const/high16 v0, 0x10000000

    sput v0, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->OFFSET:I

    .line 14
    const-wide v0, 0x41945f306dc9d495L    # 8.54456594471E7

    sput-wide v0, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->RADIUS:D

    .line 15
    const-wide v0, 0x400927bb2fec56d6L    # 3.1444

    sput-wide v0, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->pi:D

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cluster(Ljava/util/List;F)Ljava/util/ArrayList;
    .locals 17
    .param p1, "zoom"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;F)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "markers":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v10, "clusterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v15, "originalListCopy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 67
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v11, v2, :cond_1

    .line 113
    return-object v10

    .line 62
    .end local v11    # "i":I
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 63
    .local v13, "marker":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    .end local v13    # "marker":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .restart local v11    # "i":I
    :cond_1
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v14, "markerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    add-int/lit8 v12, v11, 0x1

    .local v12, "j":I
    :goto_2
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    if-lt v12, v2, :cond_2

    .line 94
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 95
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;

    .line 97
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v4, v3, 0x1

    .line 98
    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v5

    .line 99
    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v7

    move-object v3, v14

    .line 96
    invoke-direct/range {v1 .. v8}, Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;-><init>(ILjava/util/ArrayList;IDD)V

    .line 100
    .local v1, "cluster":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 102
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 103
    const/4 v11, 0x0

    .line 105
    goto :goto_1

    .line 72
    .end local v1    # "cluster":Lcom/sec/android/automotive/drivelink/location/cluster/Cluster;, "Lcom/sec/android/automotive/drivelink/location/cluster/Cluster<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    :cond_2
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v1

    .line 73
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v3

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 74
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v5

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v7

    .line 75
    move/from16 v0, p1

    float-to-int v9, v0

    .line 72
    invoke-static/range {v1 .. v9}, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->pixelDistance(DDDDI)I

    move-result v16

    .line 77
    .local v16, "pixelDistance":I
    const/16 v2, 0x28

    move/from16 v0, v16

    if-ge v0, v2, :cond_3

    .line 79
    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 83
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 84
    add-int/lit8 v12, v11, 0x1

    .line 85
    goto/16 :goto_2

    .line 86
    :cond_3
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 106
    .end local v16    # "pixelDistance":I
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1
.end method

.method public static clusterByCoordinate(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Ljava/util/List;F)Ljava/util/List;
    .locals 13
    .param p0, "locActual"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "zoom"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;F)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "markers":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v9, "clusterList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    return-object v9

    .line 130
    :cond_1
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 131
    .local v11, "point":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v0

    .line 132
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLatitude()D

    move-result-wide v4

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->getLongitude()D

    move-result-wide v6

    float-to-int v8, p2

    .line 131
    invoke-static/range {v0 .. v8}, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->pixelDistance(DDDDI)I

    move-result v10

    .line 135
    .local v10, "pixelDistance":I
    const/16 v0, 0x28

    if-gt v10, v0, :cond_0

    .line 136
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static latToY(D)D
    .locals 12
    .param p0, "lat"    # D

    .prologue
    const-wide v10, 0x4066800000000000L    # 180.0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 29
    sget v0, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->OFFSET:I

    int-to-double v0, v0

    .line 30
    sget-wide v2, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->RADIUS:D

    .line 31
    sget-wide v4, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->pi:D

    mul-double/2addr v4, p0

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    add-double/2addr v4, v8

    .line 32
    sget-wide v6, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->pi:D

    mul-double/2addr v6, p0

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    sub-double v6, v8, v6

    .line 31
    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    .line 30
    mul-double/2addr v2, v4

    .line 32
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 30
    div-double/2addr v2, v4

    .line 29
    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method private static lonToX(D)D
    .locals 6
    .param p0, "lon"    # D

    .prologue
    .line 25
    sget v0, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->OFFSET:I

    int-to-double v0, v0

    sget-wide v2, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->RADIUS:D

    mul-double/2addr v2, p0

    sget-wide v4, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->pi:D

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method private static pixelDistance(DDDDI)I
    .locals 14
    .param p0, "lat1"    # D
    .param p2, "lon1"    # D
    .param p4, "lat2"    # D
    .param p6, "lon2"    # D
    .param p8, "zoom"    # I

    .prologue
    .line 37
    invoke-static/range {p2 .. p3}, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->lonToX(D)D

    move-result-wide v0

    .line 38
    .local v0, "x1":D
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->latToY(D)D

    move-result-wide v4

    .line 40
    .local v4, "y1":D
    invoke-static/range {p6 .. p7}, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->lonToX(D)D

    move-result-wide v2

    .line 41
    .local v2, "x2":D
    invoke-static/range {p4 .. p5}, Lcom/sec/android/automotive/drivelink/location/cluster/GroupMarker;->latToY(D)D

    move-result-wide v6

    .line 44
    .local v6, "y2":D
    sub-double v8, v0, v2

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    sub-double v10, v4, v6

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v8, v8

    rsub-int/lit8 v9, p8, 0x15

    .line 43
    shr-int/2addr v8, v9

    return v8
.end method
