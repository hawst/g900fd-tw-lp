.class public Lcom/sec/android/automotive/drivelink/location/map/util/GooglePlacesAPI;
.super Ljava/lang/Object;
.source "GooglePlacesAPI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static searchPlace(Ljava/lang/String;DD)Ljava/util/ArrayList;
    .locals 7
    .param p0, "strToSearch"    # Ljava/lang/String;
    .param p1, "curLat"    # D
    .param p3, "curLng"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DD)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/util/places/PlaceService;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/util/places/PlaceService;-><init>()V

    .local v0, "placeService":Lcom/sec/android/automotive/drivelink/location/map/util/places/PlaceService;
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 23
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/location/map/util/places/PlaceService;->searchPlace(Ljava/lang/String;DD)Ljava/util/ArrayList;

    move-result-object v6

    .line 25
    .local v6, "places":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;>;"
    return-object v6
.end method
