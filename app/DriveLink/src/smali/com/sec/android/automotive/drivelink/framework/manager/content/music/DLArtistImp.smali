.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
.source "DLArtistImp.java"


# instance fields
.field private mAlbumCount:I

.field private mArtistId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;II)V
    .locals 1
    .param p1, "artistId"    # I
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "albumCount"    # I
    .param p4, "songCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;-><init>()V

    .line 6
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mArtistId:I

    .line 7
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mAlbumCount:I

    .line 11
    invoke-virtual {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->setName(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0, p4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->setSongCount(I)V

    .line 13
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mArtistId:I

    .line 14
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mAlbumCount:I

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "artist"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;-><init>()V

    .line 6
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mArtistId:I

    .line 7
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mAlbumCount:I

    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->setName(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public getAlbumCount()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mAlbumCount:I

    return v0
.end method

.method public getArtistId()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;->mArtistId:I

    return v0
.end method
