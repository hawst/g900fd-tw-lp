.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
.source "DLLocationGroup.java"


# instance fields
.field mParticipants:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;"
        }
    .end annotation
.end field

.field private myBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->GROUP_SHARING:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V

    .line 11
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->mParticipants:Ljava/util/ArrayList;

    .line 12
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->myBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->mParticipants:Ljava/util/ArrayList;

    .line 17
    return-void
.end method


# virtual methods
.method protected addParticipant(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 1
    .param p1, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->mParticipants:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->getLocationTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGroupStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->getLocationDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMyBuddy(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->myBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    if-nez v1, :cond_0

    .line 62
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->getMyPhoneNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "myPhone":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->getParticipantFromPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->myBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 66
    .end local v0    # "myPhone":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->myBuddy:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    return-object v1
.end method

.method public getParticipantFromPhoneNumber(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    .locals 4
    .param p1, "phone"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 45
    if-eqz p1, :cond_0

    const-string/jumbo v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 53
    :goto_0
    return-object v0

    .line 48
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->mParticipants:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    move-object v0, v1

    .line 53
    goto :goto_0

    .line 48
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 49
    .local v0, "buddy":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationPhoneUtils;->areEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0
.end method

.method public getParticipants()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->mParticipants:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setGroupName(Ljava/lang/String;)V
    .locals 0
    .param p1, "groupName"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->setLocationTitle(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public setGroupStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->setLocationDescription(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public setParticipants(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "participants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationGroup;->mParticipants:Ljava/util/ArrayList;

    .line 38
    return-void
.end method
