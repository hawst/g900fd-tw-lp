.class public interface abstract Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;
.super Ljava/lang/Object;
.source "MessageVoiceInput.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnComposerTextInputListener"
.end annotation


# virtual methods
.method public abstract OnContactChoice(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract OnContactTypeInfo(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
.end method

.method public abstract OnInboxChoice(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract OnInboxSearchChoice(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract OnInputTextUpdated(Ljava/lang/String;)V
.end method
