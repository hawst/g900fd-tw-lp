.class Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$2;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLLightSensorManager$OnLightSensorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setBrightMode()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setDayMode()V

    .line 226
    return-void
.end method

.method public setDarkMode()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setNightMode()V

    .line 221
    return-void
.end method
