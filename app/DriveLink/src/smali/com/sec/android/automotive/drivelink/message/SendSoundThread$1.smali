.class Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;
.super Ljava/lang/Object;
.source "SendSoundThread.java"

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->playSound(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/SendSoundThread;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 1
    .param p1, "arg0"    # Landroid/media/SoundPool;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 45
    if-nez p3, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    iput-object p1, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->soundPool:Landroid/media/SoundPool;

    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    iput p2, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->sampleId:I

    .line 48
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    iput p3, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->status:I

    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$1;->this$0:Lcom/sec/android/automotive/drivelink/message/SendSoundThread;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/message/SendSoundThread;->soundThread:Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/SendSoundThread$NotiSoundThread;->start()V

    .line 51
    :cond_0
    return-void
.end method
