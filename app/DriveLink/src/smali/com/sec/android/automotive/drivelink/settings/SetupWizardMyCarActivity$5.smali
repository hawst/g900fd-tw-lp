.class Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;
.super Ljava/lang/Object;
.source "SetupWizardMyCarActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 230
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    invoke-static {v0, v6}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;Z)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarDefault:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$3(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarUnRegisteredNotConnected:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$4(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvMyCarMsg3:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$5(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 234
    const v2, 0x7f0a041b

    new-array v3, v6, [Ljava/lang/Object;

    .line 235
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getPhoneName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 233
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->tvCantFind:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$6(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 237
    const v2, 0x7f0a041c

    new-array v3, v6, [Ljava/lang/Object;

    .line 238
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->getPhoneName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 236
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$7(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-string/jumbo v0, "BaseActivity"

    .line 241
    const-string/jumbo v1, "initLayout UNREGISTERED_NOT_CONNECTED isFirstAccess"

    .line 240
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarNotConnectedSkipMsg:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 248
    :goto_0
    return-void

    .line 244
    :cond_0
    const-string/jumbo v0, "BaseActivity"

    .line 245
    const-string/jumbo v1, "initLayout UNREGISTERED_NOT_CONNECTED !isFirstAccess"

    .line 244
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->layoutMyCarNotConnectedSkipMsg:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;->access$8(Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
