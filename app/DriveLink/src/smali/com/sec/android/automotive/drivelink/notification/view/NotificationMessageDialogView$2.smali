.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;
.super Ljava/lang/Object;
.source "NotificationMessageDialogView.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    .line 766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 3
    .param p1, "ar"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "rc"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 770
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestCancelled: audioFocusType = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioFocusType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 771
    const-string/jumbo v2, "audioStream = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "flags = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 770
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$2(Ljava/lang/String;)V

    .line 772
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestCancelled: ReasonCanceled = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$2(Ljava/lang/String;)V

    .line 773
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 774
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 775
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 776
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTShandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 779
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "ar"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 784
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestDidPlay: audioFocusType = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioFocusType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 785
    const-string/jumbo v2, "audioStream = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "flags = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 784
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$2(Ljava/lang/String;)V

    .line 787
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 788
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 789
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 790
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTShandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 793
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 2
    .param p1, "ar"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "ri"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 798
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onRequestIgnored: audioFocusType = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioFocusType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 799
    const-string/jumbo v1, "audioStream = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "flags = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 798
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$2(Ljava/lang/String;)V

    .line 800
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onRequestIgnored: ReasonIgnored = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$2(Ljava/lang/String;)V

    .line 802
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 4
    .param p1, "ar"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v3, 0x1

    .line 807
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestWillPlay: audioFocusType = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioFocusType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 808
    const-string/jumbo v2, "audioStream = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "flags = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->flags:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 807
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->Debug(Ljava/lang/String;)V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$2(Ljava/lang/String;)V

    .line 809
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 810
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 811
    .local v0, "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 812
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMessageDialogView;->mTTShandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 814
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
