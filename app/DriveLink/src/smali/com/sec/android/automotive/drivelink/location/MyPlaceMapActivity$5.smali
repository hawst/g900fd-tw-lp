.class Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;
.super Ljava/lang/Object;
.source "MyPlaceMapActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

.field private final synthetic val$ivClearText:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;->val$ivClearText:Landroid/widget/ImageView;

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 107
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 103
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v2, 0x0

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;->val$ivClearText:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/AutoCompleteTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 98
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;->val$ivClearText:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->txtSearch:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceMapActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    .line 96
    const v1, 0x7f0201f8

    .line 95
    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/AutoCompleteTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0
.end method
