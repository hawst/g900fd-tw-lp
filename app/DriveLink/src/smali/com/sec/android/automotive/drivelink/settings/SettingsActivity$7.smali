.class Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$7;
.super Ljava/lang/Object;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 234
    const-string/jumbo v1, "[SettingsActivity]"

    const-string/jumbo v2, "layoutServerDataReset OnClick"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    .line 236
    const-class v2, Lcom/sec/android/automotive/drivelink/settings/SettingsServerDataResetActivity;

    .line 235
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 237
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 238
    return-void
.end method
