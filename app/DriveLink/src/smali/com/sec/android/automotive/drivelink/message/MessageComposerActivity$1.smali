.class Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;
.super Ljava/lang/Object;
.source "MessageComposerActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    .line 649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 688
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 693
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v5, 0x4

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 653
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 656
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 657
    const v2, 0x7f0a0249

    .line 656
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setTTSText(Ljava/lang/String;)V

    .line 658
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setOnSendBtnEnabled(Z)V

    .line 659
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getComposerSIPState()Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    if-ne v0, v1, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V
    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;I)V

    .line 684
    :goto_0
    return-void

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->isNewintent:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 666
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 667
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;I)V

    goto :goto_0

    .line 669
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$5(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;Z)V

    goto :goto_0

    .line 673
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 674
    const v2, 0x7f0a0245

    .line 673
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setTTSText(Ljava/lang/String;)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageComposerActionBarLayout;->setOnSendBtnEnabled(Z)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mMessageComposerTextView:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView;->getComposerSIPState()Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/message/MessageComposerTextView$MessageComposerSIPState;

    if-ne v0, v1, :cond_3

    .line 677
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V
    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;I)V

    goto :goto_0

    .line 680
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->mStartCancelBtn:Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;)Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/common/view/QuotationMarksButton;->setVisibility(I)V

    .line 681
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity$1;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->setStateCompleteLayout(I)V
    invoke-static {v0, v5}, Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;->access$3(Lcom/sec/android/automotive/drivelink/message/MessageComposerActivity;I)V

    goto :goto_0
.end method
