.class Lcom/sec/android/automotive/drivelink/music/MusicService$3;
.super Landroid/telephony/PhoneStateListener;
.source "MusicService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$3;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 1494
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 1497
    invoke-super {p0, p1, p2}, Landroid/telephony/PhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V

    .line 1499
    packed-switch p1, :pswitch_data_0

    .line 1527
    :cond_0
    :goto_0
    return-void

    .line 1501
    :pswitch_0
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICService Call State Idle"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1502
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$3;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mCallingState:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$20(Lcom/sec/android/automotive/drivelink/music/MusicService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1504
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$3;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$21(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1507
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$3;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 1508
    const/4 v1, 0x3

    .line 1507
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->onPlaybackRequest(I)V

    .line 1510
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICServiceCall State Idle --- Music resume!!!!!!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1515
    :pswitch_1
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICService Call State OffHook"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1516
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$3;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$21(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    goto :goto_0

    .line 1521
    :pswitch_2
    const-string/jumbo v0, "[CarMode]"

    const-string/jumbo v1, "MUSICService Call State Ringing"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1522
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$3;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$21(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    goto :goto_0

    .line 1499
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
