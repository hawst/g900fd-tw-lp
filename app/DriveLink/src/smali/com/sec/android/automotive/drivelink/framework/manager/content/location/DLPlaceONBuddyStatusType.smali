.class public final enum Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
.super Ljava/lang/Enum;
.source "DLPlaceONBuddyStatusType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

.field public static final enum NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

.field public static final enum REJECTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

.field public static final enum WITHDRAWAL:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    const-string/jumbo v1, "NOT_RESPONSED"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    const-string/jumbo v1, "ACCEPTED"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    const-string/jumbo v1, "REJECTED"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->REJECTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    const-string/jumbo v1, "WITHDRAWAL"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->WITHDRAWAL:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->REJECTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->WITHDRAWAL:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 8
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->value:I

    .line 9
    return-void
.end method

.method public static getTypeFromInt(I)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->value:I

    if-ne v0, p0, :cond_0

    .line 17
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 22
    :goto_0
    return-object v0

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->REJECTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->value:I

    if-ne v0, p0, :cond_1

    .line 19
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->REJECTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    goto :goto_0

    .line 20
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->WITHDRAWAL:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    iget v0, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->value:I

    if-ne v0, p0, :cond_2

    .line 21
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->WITHDRAWAL:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    goto :goto_0

    .line 22
    :cond_2
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->NOT_RESPONSED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 12
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->value:I

    return v0
.end method
