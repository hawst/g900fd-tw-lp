.class Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;
.super Ljava/lang/Object;
.source "LocationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    .line 565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 570
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->hideLocationLayout()V

    .line 571
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$6(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/VoiceLocationActionBarLayout;->showVoiceLayout()V

    .line 572
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 573
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 574
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 575
    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 576
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 577
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    iput-boolean v4, v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchMode:Z

    .line 578
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchedText:Ljava/lang/String;

    .line 579
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationActivity$9;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationActivity;->mSearchText:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/LocationActivity;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 580
    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 585
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method
