.class public Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;
.super Ljava/lang/Object;
.source "MyPlaceHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    }
.end annotation


# static fields
.field private static final HOME:Ljava/lang/String; = "location_home"

.field private static final OFFICE:Ljava/lang/String; = "location_office"

.field private static final TAG:Ljava/lang/String; = "MyPlaceHandle"


# instance fields
.field private dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

.field private mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    .line 33
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    return-object v0
.end method

.method private getLocationBaseListener()Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$1;-><init>(Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;)V

    return-object v0
.end method


# virtual methods
.method public getHomeOffice()V
    .locals 3

    .prologue
    .line 37
    const-string/jumbo v0, "MyPlaceHandle"

    const-string/jumbo v1, "getHomeOffice"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mOnResponseMyPlace:Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle$OnResponseMyPlace;

    if-nez v0, :cond_0

    .line 40
    const-string/jumbo v0, "MyPlaceHandle"

    const-string/jumbo v1, "mOnResponseMyPlace is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 46
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    if-nez v0, :cond_1

    .line 47
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->getLocationBaseListener()Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 50
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->mLocationBaseListener:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    .line 50
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/MyPlaceHandle;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 54
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 55
    const/16 v2, 0xc

    .line 53
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestRecommendedLocationList(Landroid/content/Context;I)V

    goto :goto_0
.end method
