.class public Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "BaseFirstAccessActivity.java"


# instance fields
.field private exitDialogOpen:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected isExitDialogOpen()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->exitDialogOpen:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 50
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 51
    instance-of v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/TutorialGuideActivity01;

    if-eqz v1, :cond_1

    .line 52
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 53
    const-class v2, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    .line 52
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->startActivity(Landroid/content/Intent;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->finish()V

    .line 93
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    instance-of v1, p0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;

    if-eqz v1, :cond_3

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 60
    const-string/jumbo v2, "NAME_BT_STATE_UNREGISTERED"

    .line 59
    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    .line 60
    if-eqz v1, :cond_2

    .line 61
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    goto :goto_0

    .line 63
    :cond_2
    new-instance v0, Landroid/content/Intent;

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 65
    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 63
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    .line 66
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    const-string/jumbo v1, "BACK"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->finish()V

    goto :goto_0

    .line 72
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    instance-of v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    if-eqz v1, :cond_4

    .line 73
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    new-instance v0, Landroid/content/Intent;

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 76
    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    .line 74
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    .line 77
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->startActivity(Landroid/content/Intent;)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->finish()V

    goto :goto_0

    .line 82
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    instance-of v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    if-eqz v1, :cond_0

    .line 83
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 84
    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    .line 83
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->startActivity(Landroid/content/Intent;)V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->finish()V

    goto :goto_0

    .line 91
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 39
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->initDialogExit(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;Landroid/os/Bundle;)V

    .line 40
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 45
    invoke-static {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->onSaveInstanceStateIsExitDialogOpen(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;Landroid/os/Bundle;)V

    .line 46
    return-void
.end method

.method protected setExitDialogOpen(Z)V
    .locals 0
    .param p1, "exitDialogOpen"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->exitDialogOpen:Z

    .line 120
    return-void
.end method
