.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
.source "MusicDataHandler.java"


# static fields
.field private static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field private static final DATE_ADDED:Ljava/lang/String; = "date_added"

.field private static final FIXED_PLAYLIST_ID:I = -0x1

.field private static final FOLDER_NAME:Ljava/lang/String; = "bucket_display_name"

.field private static final GENRE_NAME:Ljava/lang/String; = "genre_name"

.field private static final IS_FAVORITE:Ljava/lang/String; = "is_favorite"

.field private static final MOST_PLAYED:Ljava/lang/String; = "most_played"

.field private static final MUSICPLUS_AUTHORITY:Ljava/lang/String; = "com.samsung.musicplus"

.field private static final RECENTLY_ADDED:Ljava/lang/String; = "recently_added_remove_flag"

.field private static final RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"

.field private static final SELECTION_VALID_MUSIC:Ljava/lang/String; = "title != \'\' AND is_music = 1"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mMusicAlbumUri:Landroid/net/Uri;

.field private mMusicArtistUri:Landroid/net/Uri;

.field private mMusicDataUri:Landroid/net/Uri;

.field private mMusicFolderUri:Landroid/net/Uri;

.field private mMusicPlaylistUri:Landroid/net/Uri;

.field private mMusicplusProvider:Landroid/content/ContentProviderClient;

.field private mRemoveBlankString:Ljava/lang/String;

.field private mbUserMusicProvider:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->TAG:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicPlaylistUri:Landroid/net/Uri;

    .line 46
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicArtistUri:Landroid/net/Uri;

    .line 47
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicFolderUri:Landroid/net/Uri;

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicAlbumUri:Landroid/net/Uri;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mbUserMusicProvider:Z

    .line 52
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mRemoveBlankString:Ljava/lang/String;

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mbUserMusicProvider:Z

    .line 56
    return-void
.end method

.method private fillMusicFromCursor(Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "musicImp"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 924
    .line 925
    const-string/jumbo v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 924
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setId(I)V

    .line 927
    const-string/jumbo v1, "title"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 926
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setTitle(Ljava/lang/String;)V

    .line 929
    const-string/jumbo v1, "_display_name"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 928
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setDisplayName(Ljava/lang/String;)V

    .line 931
    const-string/jumbo v1, "_data"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 930
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setData(Ljava/lang/String;)V

    .line 933
    const-string/jumbo v1, "artist"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 932
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setArtist(Ljava/lang/String;)V

    .line 935
    const-string/jumbo v1, "album"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 934
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setAlbum(Ljava/lang/String;)V

    .line 937
    const-string/jumbo v1, "album_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 936
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setAlbumId(I)V

    .line 939
    const-string/jumbo v1, "duration"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 938
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setDuration(I)V

    .line 943
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 944
    .local v0, "musicURI":Landroid/net/Uri;
    invoke-virtual {p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setUri(Landroid/net/Uri;)V

    .line 947
    const-string/jumbo v1, "genre_name"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 946
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setGenreName(Ljava/lang/String;)V

    .line 948
    return-void
.end method

.method private getAlbumArt(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "bResized"    # Z

    .prologue
    .line 876
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 878
    .local v2, "res":Landroid/content/ContentResolver;
    const/4 v0, 0x0

    .line 880
    .local v0, "bitmapAlbumArt":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {v2, p2}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 885
    :goto_0
    if-nez v0, :cond_0

    .line 886
    const/4 v4, 0x0

    .line 901
    :goto_1
    return-object v4

    .line 881
    :catch_0
    move-exception v1

    .line 882
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 889
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    if-eqz p3, :cond_2

    .line 891
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 892
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    const/4 v6, 0x1

    .line 890
    invoke-static {v0, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 893
    .local v3, "resizedAlbumArt":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 894
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 895
    const/4 v0, 0x0

    .line 898
    :cond_1
    move-object v0, v3

    .end local v3    # "resizedAlbumArt":Landroid/graphics/Bitmap;
    :cond_2
    move-object v4, v0

    .line 901
    goto :goto_1
.end method

.method private getFixedPlaylist(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 657
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 658
    .local v1, "musicUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v5, "COUNT(*) AS song_count"

    aput-object v5, v2, v0

    .line 660
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "title != \'\' AND is_music = 1"

    .line 662
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v0, "Favorite"

    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "is_favorite"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, " != 0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 672
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 674
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 676
    .local v7, "songCount":I
    if-eqz v6, :cond_2

    .line 677
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 678
    const-string/jumbo v0, "song_count"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 681
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 684
    :cond_2
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    const/4 v4, -0x1

    invoke-direct {v0, v4, p2, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;-><init>(ILjava/lang/String;I)V

    return-object v0

    .line 664
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "songCount":I
    :cond_3
    const-string/jumbo v0, "Most Played"

    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 665
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "most_played"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, " != 0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 666
    goto :goto_0

    :cond_4
    const-string/jumbo v0, "Recently Played"

    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "recently_played"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, " != 0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 668
    goto :goto_0

    :cond_5
    const-string/jumbo v0, "Recently Added"

    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 669
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "recently_added_remove_flag"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, " != 1"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method private getMusicAlbumId(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)I
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .prologue
    .line 817
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 818
    .local v1, "musicUri":Landroid/net/Uri;
    const/4 v7, -0x1

    .line 821
    .local v7, "albumId":I
    instance-of v0, p2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v0, :cond_1

    move-object v11, p2

    .line 822
    check-cast v11, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 823
    .local v11, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getAlbumId()I

    move-result v7

    .end local v11    # "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    :cond_0
    :goto_0
    move v0, v7

    .line 872
    :goto_1
    return v0

    .line 824
    :cond_1
    instance-of v0, p2, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;

    if-eqz v0, :cond_2

    move-object v6, p2

    .line 825
    check-cast v6, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;

    .line 826
    .local v6, "album":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;
    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;->getAlbumId()I

    move-result v7

    .line 827
    goto :goto_0

    .line 828
    .end local v6    # "album":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;
    :cond_2
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 829
    const-string/jumbo v12, "album_id"

    aput-object v12, v2, v0

    const/4 v0, 0x1

    .line 830
    const-string/jumbo v12, "title"

    aput-object v12, v2, v0

    const/4 v0, 0x2

    .line 831
    const-string/jumbo v12, "artist"

    aput-object v12, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v12, "bucket_id"

    aput-object v12, v2, v0

    .line 832
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 833
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 834
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v5, "title COLLATE LOCALIZED ASC"

    .line 837
    .local v5, "orderBy":Ljava/lang/String;
    instance-of v0, p2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    if-eqz v0, :cond_4

    move-object v8, p2

    .line 838
    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 839
    .local v8, "artist":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    const-string/jumbo v3, "title != \'\' AND is_music = 1 AND artist=?"

    .line 841
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v0

    .line 852
    .end local v8    # "artist":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :goto_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 855
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    .line 856
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 860
    const-string/jumbo v0, "album_id"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 859
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 868
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 842
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_4
    instance-of v0, p2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    if-eqz v0, :cond_5

    move-object v10, p2

    .line 843
    check-cast v10, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;

    .line 844
    .local v10, "folder":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;
    const-string/jumbo v3, "title != \'\' AND is_music = 1 AND bucket_id=?"

    .line 845
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 846
    invoke-virtual {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->getBucketId()I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v0

    .line 847
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_2

    .line 849
    .end local v10    # "folder":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;
    :cond_5
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getPlaylistMusicUri(I)Landroid/net/Uri;
    .locals 4
    .param p1, "playlistId"    # I

    .prologue
    .line 951
    const/4 v0, 0x0

    .line 953
    .local v0, "musicUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    if-eqz v1, :cond_0

    .line 955
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "content://com.samsung.musicplus/audio/playlists/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 956
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/members"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 955
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 962
    :goto_0
    return-object v0

    .line 959
    :cond_0
    const-string/jumbo v1, "external"

    int-to-long v2, p1

    .line 958
    invoke-static {v1, v2, v3}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private getSongCountWithPlaylistId(Landroid/content/Context;I)I
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "playlistId"    # I

    .prologue
    const/4 v4, 0x0

    .line 688
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getPlaylistMusicUri(I)Landroid/net/Uri;

    move-result-object v1

    .line 690
    .local v1, "playlistMemberUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v3, "COUNT(*) as song_count"

    aput-object v3, v2, v0

    .line 691
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 692
    const-string/jumbo v3, "is_music = 1"

    move-object v5, v4

    .line 691
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 695
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 697
    .local v7, "songCount":I
    if-eqz v6, :cond_1

    .line 698
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    const-string/jumbo v0, "song_count"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 701
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 704
    :cond_1
    return v7
.end method

.method private removeDuplacatedMusic(Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;)V
    .locals 10
    .param p1, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;

    .prologue
    .line 611
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->getMusicList()Ljava/util/ArrayList;

    move-result-object v7

    .line 612
    .local v7, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->getAlbumList()Ljava/util/ArrayList;

    move-result-object v0

    .line 613
    .local v0, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->getArtistList()Ljava/util/ArrayList;

    move-result-object v1

    .line 615
    .local v1, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 616
    .local v2, "duplicatedMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    .line 617
    .local v6, "locale":Ljava/util/Locale;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v3, v8, :cond_0

    .line 640
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 641
    return-void

    .line 618
    :cond_0
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v4, v8, :cond_1

    .line 625
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v5, v8, :cond_3

    .line 617
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 619
    .end local v5    # "k":I
    :cond_1
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getAlbum()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 620
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 621
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 627
    .restart local v5    # "k":I
    :cond_3
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 628
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getArtist()Ljava/lang/String;

    move-result-object v8

    .line 629
    invoke-virtual {v8, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 630
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 631
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 632
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->increaseReplicatedCount()V

    .line 625
    :cond_4
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 634
    :cond_5
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method private retrieveDLMusicListFromCursor(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 905
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 907
    .local v0, "audioList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 909
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;-><init>()V

    .line 911
    .local v1, "musicImp":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    invoke-direct {p0, v1, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->fillMusicFromCursor(Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;Landroid/database/Cursor;)V

    .line 913
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 914
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 916
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 919
    .end local v1    # "musicImp":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    :cond_1
    return-object v0
.end method

.method private searchAlbum(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 523
    .local v4, "searchedAlbumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getAlbumList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 525
    .local v1, "allAlbumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 526
    .local v3, "locale":Ljava/util/Locale;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_0

    .line 536
    return-object v4

    .line 527
    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 528
    .local v0, "album":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " "

    const-string/jumbo v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mRemoveBlankString:Ljava/lang/String;

    .line 530
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mRemoveBlankString:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 531
    invoke-virtual {p2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 530
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 531
    if-eqz v5, :cond_1

    .line 532
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private searchArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 504
    .local v4, "searchedArtistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getArtistList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 506
    .local v0, "allArtistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 507
    .local v3, "locale":Ljava/util/Locale;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_0

    .line 517
    return-object v4

    .line 508
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 509
    .local v1, "artist":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " "

    const-string/jumbo v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mRemoveBlankString:Ljava/lang/String;

    .line 511
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mRemoveBlankString:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 512
    invoke-virtual {p2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 511
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 512
    if-eqz v5, :cond_1

    .line 513
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private searchMusic(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 540
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 542
    .local v4, "searchedMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 544
    .local v0, "allMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 545
    .local v2, "locale":Ljava/util/Locale;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v1, v5, :cond_0

    .line 555
    return-object v4

    .line 546
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 547
    .local v3, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " "

    const-string/jumbo v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mRemoveBlankString:Ljava/lang/String;

    .line 549
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mRemoveBlankString:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 550
    invoke-virtual {p2, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 549
    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 550
    if-eqz v5, :cond_1

    .line 551
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public fillMusicListByMusicId(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 763
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 764
    .local v1, "musicUri":Landroid/net/Uri;
    const/16 v0, 0x9

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "_id"

    aput-object v0, v2, v9

    .line 765
    const-string/jumbo v0, "title"

    aput-object v0, v2, v10

    const/4 v0, 0x2

    .line 766
    const-string/jumbo v5, "_display_name"

    aput-object v5, v2, v0

    const/4 v0, 0x3

    .line 767
    const-string/jumbo v5, "_data"

    aput-object v5, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v5, "artist"

    aput-object v5, v2, v0

    const/4 v0, 0x5

    .line 768
    const-string/jumbo v5, "album"

    aput-object v5, v2, v0

    const/4 v0, 0x6

    const-string/jumbo v5, "album_id"

    aput-object v5, v2, v0

    const/4 v0, 0x7

    .line 769
    const-string/jumbo v5, "duration"

    aput-object v5, v2, v0

    const/16 v0, 0x8

    const-string/jumbo v5, "genre_name"

    aput-object v5, v2, v0

    .line 771
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "title != \'\' AND is_music = 1 AND _id=?"

    .line 773
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 775
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v7, v0, :cond_0

    .line 793
    return-void

    .line 776
    :cond_0
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    .line 778
    .local v8, "musicImp":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    new-array v4, v10, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    .line 780
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 781
    const/4 v5, 0x0

    .line 780
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 783
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 784
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 786
    :cond_1
    invoke-direct {p0, v8, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->fillMusicFromCursor(Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;Landroid/database/Cursor;)V

    .line 787
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 790
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 775
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public getAlbumList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 267
    .local v7, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    const/4 v2, 0x0

    .line 269
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_3

    .line 270
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 271
    const-string/jumbo v1, "album_group_id AS _id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    .line 272
    const-string/jumbo v1, "album"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    .line 273
    const-string/jumbo v1, "artist"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    .line 274
    const-string/jumbo v1, "count(album_key) AS numsongs"

    aput-object v1, v2, v0

    .line 283
    .restart local v2    # "projection":[Ljava/lang/String;
    :goto_0
    const-string/jumbo v5, "album COLLATE LOCALIZED ASC"

    .line 287
    .local v5, "orderBy":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicAlbumUri:Landroid/net/Uri;

    .line 288
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 287
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 290
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_2

    .line 291
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    :cond_0
    const-string/jumbo v0, "_id"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 293
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 296
    .local v11, "id":I
    const-string/jumbo v0, "album"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 295
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 299
    .local v6, "album":Ljava/lang/String;
    const-string/jumbo v0, "artist"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 298
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 302
    .local v8, "artist":Ljava/lang/String;
    const-string/jumbo v0, "numsongs"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 301
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 304
    .local v12, "songCount":I
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;

    .line 305
    invoke-direct {v0, v11, v6, v8, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    .line 304
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    .end local v6    # "album":Ljava/lang/String;
    .end local v8    # "artist":Ljava/lang/String;
    .end local v11    # "id":I
    .end local v12    # "songCount":I
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    return-object v7

    .line 277
    .end local v5    # "orderBy":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v0, 0x0

    const-string/jumbo v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    .line 278
    const-string/jumbo v1, "album"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    .line 279
    const-string/jumbo v1, "artist"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    .line 280
    const-string/jumbo v1, "numsongs"

    aput-object v1, v2, v0

    .line 277
    .restart local v2    # "projection":[Ljava/lang/String;
    goto :goto_0

    .line 311
    .restart local v5    # "orderBy":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 312
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 645
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicSearchResult(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    move-result-object v0

    .line 648
    .local v0, "musicSearchResult":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    if-nez v0, :cond_0

    .line 649
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 652
    :goto_0
    return-object v1

    .line 653
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getResultItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 652
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicListFromSearchList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method public getArtistList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 319
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 321
    .local v9, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    const/4 v2, 0x0

    .line 323
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_3

    .line 324
    new-array v2, v13, [Ljava/lang/String;

    .line 325
    .end local v2    # "projection":[Ljava/lang/String;
    const-string/jumbo v0, "_id"

    aput-object v0, v2, v1

    .line 326
    const-string/jumbo v0, "artist"

    aput-object v0, v2, v3

    .line 327
    const-string/jumbo v0, "count(distinct album) AS number_of_albums"

    aput-object v0, v2, v4

    .line 329
    const-string/jumbo v0, "count(*) AS number_of_tracks"

    aput-object v0, v2, v12

    .line 337
    .restart local v2    # "projection":[Ljava/lang/String;
    :goto_0
    const-string/jumbo v5, "artist COLLATE LOCALIZED ASC"

    .line 340
    .local v5, "orderBy":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 341
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicArtistUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 340
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 343
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_2

    .line 344
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    :cond_0
    const-string/jumbo v0, "_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 346
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 349
    .local v8, "artistId":I
    const-string/jumbo v0, "artist"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 348
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 352
    .local v7, "artist":Ljava/lang/String;
    const-string/jumbo v0, "number_of_albums"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 351
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 355
    .local v6, "albumCount":I
    const-string/jumbo v0, "number_of_tracks"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 354
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 357
    .local v11, "songCount":I
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;

    .line 358
    invoke-direct {v0, v8, v7, v6, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;-><init>(ILjava/lang/String;II)V

    .line 357
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 362
    .end local v6    # "albumCount":I
    .end local v7    # "artist":Ljava/lang/String;
    .end local v8    # "artistId":I
    .end local v11    # "songCount":I
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 365
    :cond_2
    return-object v9

    .line 331
    .end local v5    # "orderBy":Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_3
    new-array v2, v13, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const-string/jumbo v0, "_id"

    aput-object v0, v2, v1

    .line 332
    const-string/jumbo v0, "artist"

    aput-object v0, v2, v3

    .line 333
    const-string/jumbo v0, "number_of_albums"

    aput-object v0, v2, v4

    .line 334
    const-string/jumbo v0, "number_of_tracks"

    aput-object v0, v2, v12

    .line 331
    .restart local v2    # "projection":[Ljava/lang/String;
    goto :goto_0
.end method

.method public getMusicAlbumArt(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    .prologue
    .line 745
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicAlbumId(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)I

    move-result v1

    .line 747
    .local v1, "albumId":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 748
    const/4 v2, 0x0

    .line 757
    :goto_0
    return-object v2

    .line 752
    :cond_0
    const-string/jumbo v2, "content://media/external/audio/albumart"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    int-to-long v3, v1

    .line 751
    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 754
    .local v0, "albumArtUri":Landroid/net/Uri;
    instance-of v2, p2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v2, :cond_1

    .line 755
    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getAlbumArt(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 757
    :cond_1
    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getAlbumArt(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public getMusicCount(Landroid/content/Context;)I
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 796
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 797
    .local v1, "musicUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v5, "COUNT(*) AS song_count"

    aput-object v5, v2, v0

    .line 799
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "title != \'\' AND is_music = 1"

    .line 801
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 804
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 805
    .local v7, "songCount":I
    if-eqz v6, :cond_1

    .line 806
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 807
    const-string/jumbo v0, "song_count"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 810
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 813
    :cond_1
    return v7
.end method

.method public getMusicFolderList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 369
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v8, "folderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 372
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .line 373
    .local v1, "musicUri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 374
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v5, "bucket_display_name COLLATE LOCALIZED ASC"

    .line 377
    .local v5, "orderBy":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    if-eqz v3, :cond_3

    .line 378
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicFolderUri:Landroid/net/Uri;

    .line 379
    const/4 v3, 0x4

    new-array v2, v3, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 380
    const-string/jumbo v4, "bucket_display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 381
    const-string/jumbo v4, "bucket_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 382
    const-string/jumbo v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "COUNT(_id) AS song_count"

    aput-object v4, v2, v3

    .line 389
    .restart local v2    # "projection":[Ljava/lang/String;
    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 392
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 393
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 395
    :cond_0
    const/4 v3, 0x0

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 396
    .local v9, "folderName":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 397
    .local v6, "bucketId":I
    const/4 v3, 0x2

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 398
    .local v10, "folderPath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 399
    const-string/jumbo v4, "/"

    invoke-virtual {v10, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 398
    invoke-virtual {v10, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 399
    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 400
    const/4 v3, 0x3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 402
    .local v11, "songCount":I
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;

    .line 403
    invoke-direct {v3, v9, v6, v10, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 402
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 407
    .end local v6    # "bucketId":I
    .end local v9    # "folderName":Ljava/lang/String;
    .end local v10    # "folderPath":Ljava/lang/String;
    .end local v11    # "songCount":I
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 410
    :cond_2
    return-object v8

    .line 384
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 385
    const/4 v3, 0x4

    new-array v2, v3, [Ljava/lang/String;

    .end local v2    # "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string/jumbo v4, "bucket_display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "bucket_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 386
    const-string/jumbo v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "COUNT(*) AS song_count"

    aput-object v4, v2, v3

    .line 385
    .restart local v2    # "projection":[Ljava/lang/String;
    goto :goto_0
.end method

.method public getMusicList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    const/16 v1, 0xa

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v15, "_id"

    aput-object v15, v3, v1

    const/4 v1, 0x1

    .line 106
    const-string/jumbo v15, "title"

    aput-object v15, v3, v1

    const/4 v1, 0x2

    .line 107
    const-string/jumbo v15, "_display_name"

    aput-object v15, v3, v1

    const/4 v1, 0x3

    .line 108
    const-string/jumbo v15, "_data"

    aput-object v15, v3, v1

    const/4 v1, 0x4

    const-string/jumbo v15, "artist"

    aput-object v15, v3, v1

    const/4 v1, 0x5

    .line 109
    const-string/jumbo v15, "album"

    aput-object v15, v3, v1

    const/4 v1, 0x6

    const-string/jumbo v15, "album_id"

    aput-object v15, v3, v1

    const/4 v1, 0x7

    .line 110
    const-string/jumbo v15, "duration"

    aput-object v15, v3, v1

    const/16 v1, 0x8

    const-string/jumbo v15, "genre_name"

    aput-object v15, v3, v1

    const/16 v1, 0x9

    const-string/jumbo v15, "bucket_id"

    aput-object v15, v3, v1

    .line 112
    .local v3, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 113
    .local v2, "musicUri":Landroid/net/Uri;
    const-string/jumbo v4, "title != \'\' AND is_music = 1"

    .line 114
    .local v4, "selection":Ljava/lang/String;
    const/4 v5, 0x0

    .line 115
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 118
    .local v6, "orderBy":Ljava/lang/String;
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    if-eqz v1, :cond_1

    move-object/from16 v7, p2

    .line 119
    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;

    .line 121
    .local v7, "album":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "album"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 122
    const-string/jumbo v15, " = ?"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 123
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;->getName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .line 124
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v6, "title COLLATE LOCALIZED ASC"

    .line 211
    .end local v7    # "album":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    :cond_0
    :goto_0
    const/4 v10, 0x0

    .line 213
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 220
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->retrieveDLMusicListFromCursor(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v9

    .line 222
    .local v9, "audioList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-object v9

    .line 125
    .end local v9    # "audioList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_1
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    if-eqz v1, :cond_2

    move-object/from16 v8, p2

    .line 126
    check-cast v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;

    .line 128
    .local v8, "artist":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "artist"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 129
    const-string/jumbo v15, " = ?"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 128
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 130
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;->getName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .line 131
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v6, "artist COLLATE LOCALIZED ASC"

    .line 132
    goto :goto_0

    .end local v8    # "artist":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    :cond_2
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;

    if-eqz v1, :cond_4

    move-object/from16 v12, p2

    .line 133
    check-cast v12, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;

    .line 135
    .local v12, "folder":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    if-eqz v1, :cond_3

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 137
    const-string/jumbo v15, "bucket_id"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, " = ?"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 138
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 139
    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->getBucketId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .line 146
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    :goto_2
    const-string/jumbo v6, "_data COLLATE LOCALIZED ASC"

    .line 147
    goto/16 :goto_0

    .line 141
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "_data"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 142
    const-string/jumbo v15, " like ?"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 141
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 143
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;->getFolderPath()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v16, "%"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    goto :goto_2

    .line 147
    .end local v12    # "folder":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLFolderImpl;
    :cond_4
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    if-eqz v1, :cond_a

    move-object/from16 v14, p2

    .line 148
    check-cast v14, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    .line 150
    .local v14, "playList":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v15, "Favorite"

    invoke-virtual {v1, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 151
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->getId()I

    move-result v1

    const/4 v15, -0x1

    if-ne v1, v15, :cond_5

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "is_favorite"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, " != 0"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 153
    goto/16 :goto_0

    .line 156
    :cond_5
    const/16 v1, 0x9

    new-array v3, v1, [Ljava/lang/String;

    .end local v3    # "projection":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 157
    const-string/jumbo v15, "audio_id"

    aput-object v15, v3, v1

    const/4 v1, 0x1

    .line 158
    const-string/jumbo v15, "title"

    aput-object v15, v3, v1

    const/4 v1, 0x2

    .line 159
    const-string/jumbo v15, "_display_name"

    aput-object v15, v3, v1

    const/4 v1, 0x3

    .line 160
    const-string/jumbo v15, "_data"

    aput-object v15, v3, v1

    const/4 v1, 0x4

    .line 161
    const-string/jumbo v15, "artist"

    aput-object v15, v3, v1

    const/4 v1, 0x5

    .line 162
    const-string/jumbo v15, "album"

    aput-object v15, v3, v1

    const/4 v1, 0x6

    .line 163
    const-string/jumbo v15, "album_id"

    aput-object v15, v3, v1

    const/4 v1, 0x7

    .line 164
    const-string/jumbo v15, "duration"

    aput-object v15, v3, v1

    const/16 v1, 0x8

    const-string/jumbo v15, "genre_name"

    aput-object v15, v3, v1

    .line 166
    .restart local v3    # "projection":[Ljava/lang/String;
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->getId()I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getPlaylistMusicUri(I)Landroid/net/Uri;

    move-result-object v2

    .line 168
    const-string/jumbo v6, "title COLLATE LOCALIZED ASC"

    .line 172
    goto/16 :goto_0

    :cond_6
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v15, "Most Played"

    invoke-virtual {v1, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "most_played"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, " != 0"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 174
    const-string/jumbo v6, "most_played DESC"

    .line 175
    goto/16 :goto_0

    :cond_7
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v15, "Recently Played"

    invoke-virtual {v1, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "recently_played"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, " != 0"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 177
    const-string/jumbo v6, "most_played DESC"

    .line 178
    goto/16 :goto_0

    :cond_8
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v15, "Recently Added"

    invoke-virtual {v1, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_9

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "recently_added_remove_flag"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, " != 1"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 180
    const-string/jumbo v6, "date_added DESC"

    .line 181
    goto/16 :goto_0

    .line 182
    :cond_9
    const/16 v1, 0xd

    new-array v3, v1, [Ljava/lang/String;

    .end local v3    # "projection":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 183
    const-string/jumbo v15, "audio_id"

    aput-object v15, v3, v1

    const/4 v1, 0x1

    .line 184
    const-string/jumbo v15, "title"

    aput-object v15, v3, v1

    const/4 v1, 0x2

    .line 185
    const-string/jumbo v15, "_display_name"

    aput-object v15, v3, v1

    const/4 v1, 0x3

    .line 186
    const-string/jumbo v15, "_data"

    aput-object v15, v3, v1

    const/4 v1, 0x4

    .line 187
    const-string/jumbo v15, "artist"

    aput-object v15, v3, v1

    const/4 v1, 0x5

    .line 188
    const-string/jumbo v15, "album"

    aput-object v15, v3, v1

    const/4 v1, 0x6

    .line 189
    const-string/jumbo v15, "album_id"

    aput-object v15, v3, v1

    const/4 v1, 0x7

    .line 190
    const-string/jumbo v15, "duration"

    aput-object v15, v3, v1

    const/16 v1, 0x8

    const-string/jumbo v15, "genre_name"

    aput-object v15, v3, v1

    const/16 v1, 0x9

    .line 191
    const-string/jumbo v15, "is_favorite"

    aput-object v15, v3, v1

    const/16 v1, 0xa

    const-string/jumbo v15, "recently_played"

    aput-object v15, v3, v1

    const/16 v1, 0xb

    const-string/jumbo v15, "most_played"

    aput-object v15, v3, v1

    const/16 v1, 0xc

    const-string/jumbo v15, "date_added"

    aput-object v15, v3, v1

    .line 193
    .restart local v3    # "projection":[Ljava/lang/String;
    invoke-virtual {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;->getId()I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getPlaylistMusicUri(I)Landroid/net/Uri;

    move-result-object v2

    .line 195
    const-string/jumbo v6, "title COLLATE LOCALIZED ASC"

    .line 198
    goto/16 :goto_0

    .end local v14    # "playList":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;
    :cond_a
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v1, :cond_0

    move-object/from16 v13, p2

    .line 199
    check-cast v13, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .line 201
    .local v13, "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v15, " AND "

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, "upper("

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 202
    const-string/jumbo v15, "title"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, ") like ?"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 203
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    .end local v5    # "selectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "%"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 204
    const-string/jumbo v16, "%"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .line 205
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v6, "title COLLATE LOCALIZED ASC"

    goto/16 :goto_0

    .line 215
    .end local v13    # "music":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v11

    .line 216
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public getMusicListFromSearchList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)Ljava/util/ArrayList;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 560
    sget-object v7, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "getMusicListFromSearchList("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 561
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 560
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 565
    .local v5, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-nez p2, :cond_1

    .line 607
    :cond_0
    :goto_0
    return-object v5

    .line 569
    :cond_1
    invoke-virtual {p2, p3}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->get(I)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    move-result-object v6

    .line 571
    .local v6, "musicObject":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
    if-eqz v6, :cond_0

    .line 575
    instance-of v7, v6, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    if-eqz v7, :cond_7

    .line 576
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getArtistList()Ljava/util/ArrayList;

    move-result-object v1

    .line 578
    .local v1, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v3, v7, :cond_2

    .line 582
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getAlbumList()Ljava/util/ArrayList;

    move-result-object v0

    .line 583
    .local v0, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v3, v7, :cond_3

    .line 587
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getMusicList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 588
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 592
    .local v2, "duplicatedMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v3, v7, :cond_4

    .line 601
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 579
    .end local v0    # "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    .end local v2    # "duplicatedMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :cond_2
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    invoke-virtual {p0, p1, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 578
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 584
    .restart local v0    # "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    :cond_3
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;

    invoke-virtual {p0, p1, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 583
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 593
    .restart local v2    # "duplicatedMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :cond_4
    add-int/lit8 v4, v3, 0x1

    .local v4, "j":I
    :goto_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v4, v7, :cond_5

    .line 592
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 594
    :cond_5
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v8

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v7

    if-ne v8, v7, :cond_6

    .line 595
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 593
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 604
    .end local v0    # "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    .end local v1    # "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    .end local v2    # "duplicatedMusicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    .end local v3    # "i":I
    .end local v4    # "j":I
    :cond_7
    invoke-virtual {p0, p1, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getMusicList(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public getMusicListWithIdOnly(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v10, "_id"

    aput-object v10, v2, v0

    .line 228
    .local v2, "projection":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 229
    .local v1, "musicUri":Landroid/net/Uri;
    const-string/jumbo v3, "title != \'\' AND is_music = 1"

    .line 230
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 231
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 234
    .local v5, "orderBy":Ljava/lang/String;
    const/4 v7, 0x0

    .line 236
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 243
    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v6, "audioList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    if-eqz v7, :cond_2

    .line 247
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    :cond_0
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-direct {v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;-><init>()V

    .line 252
    .local v9, "musicImp":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    const-string/jumbo v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 251
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;->setId(I)V

    .line 254
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    .end local v9    # "musicImp":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 261
    :cond_2
    return-object v6

    .line 238
    .end local v6    # "audioList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :catch_0
    move-exception v8

    .line 239
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getMusicSearchResult(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchKeyword"    # Ljava/lang/String;

    .prologue
    .line 461
    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getMusicSearchResult("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    if-nez p2, :cond_0

    .line 463
    const/4 v0, 0x0

    .line 479
    :goto_0
    return-object v0

    .line 466
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;

    invoke-direct {v0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;-><init>(Ljava/lang/String;)V

    .line 471
    .local v0, "musicSearchResult":Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->searchArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->setArtistList(Ljava/util/ArrayList;)V

    .line 472
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->searchAlbum(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->setAlbumList(Ljava/util/ArrayList;)V

    .line 473
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->searchMusic(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;->setMusicList(Ljava/util/ArrayList;)V

    .line 477
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->removeDuplacatedMusic(Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicSearchResultImp;)V

    goto :goto_0
.end method

.method public getPlaylistList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 416
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 418
    .local v8, "playlistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;>;"
    const-string/jumbo v0, "Favorite"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getFixedPlaylist(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    const-string/jumbo v0, "Most Played"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getFixedPlaylist(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    const-string/jumbo v0, "Recently Played"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getFixedPlaylist(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    const-string/jumbo v0, "Recently Added"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getFixedPlaylist(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "_id"

    aput-object v0, v2, v11

    const/4 v0, 0x1

    .line 425
    const-string/jumbo v1, "name"

    aput-object v1, v2, v0

    .line 427
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 428
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicPlaylistUri:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    .line 427
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 430
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 431
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    :cond_0
    const-string/jumbo v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 433
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 436
    .local v7, "playlistId":I
    const-string/jumbo v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 435
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 437
    .local v9, "playlistName":Ljava/lang/String;
    invoke-direct {p0, p1, v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->getSongCountWithPlaylistId(Landroid/content/Context;I)I

    move-result v10

    .line 440
    .local v10, "songCount":I
    const-string/jumbo v0, "Quick list"

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 443
    const-string/jumbo v9, "Favorite"

    .line 444
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    .line 445
    invoke-direct {v0, v7, v9, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;-><init>(ILjava/lang/String;I)V

    .line 444
    invoke-virtual {v8, v11, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 450
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 453
    .end local v7    # "playlistId":I
    .end local v9    # "playlistName":Ljava/lang/String;
    .end local v10    # "songCount":I
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 456
    :cond_2
    return-object v8

    .line 447
    .restart local v7    # "playlistId":I
    .restart local v9    # "playlistName":Ljava/lang/String;
    .restart local v10    # "songCount":I
    :cond_3
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;

    .line 448
    invoke-direct {v0, v7, v9, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLPlaylistImp;-><init>(ILjava/lang/String;I)V

    .line 447
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mbUserMusicProvider:Z

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 62
    const-string/jumbo v1, "com.samsung.musicplus"

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 61
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicplusProvider:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_1

    .line 67
    const-string/jumbo v0, "content://com.samsung.musicplus/audio/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 66
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 70
    const-string/jumbo v0, "content://com.samsung.musicplus/audio/playlists"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 69
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicPlaylistUri:Landroid/net/Uri;

    .line 74
    const-string/jumbo v0, "content://com.samsung.musicplus/audio/artists"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 73
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicArtistUri:Landroid/net/Uri;

    .line 77
    const-string/jumbo v0, "content://com.samsung.musicplus/audio/albums"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 76
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicAlbumUri:Landroid/net/Uri;

    .line 81
    const-string/jumbo v0, "content://com.samsung.musicplus/audio/music_folders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 80
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicFolderUri:Landroid/net/Uri;

    .line 91
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 84
    :cond_1
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 85
    sget-object v0, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicPlaylistUri:Landroid/net/Uri;

    .line 86
    sget-object v0, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicArtistUri:Landroid/net/Uri;

    .line 87
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicFolderUri:Landroid/net/Uri;

    .line 88
    sget-object v0, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicAlbumUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    return-void
.end method

.method public updatePlayedMusicMetaData(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Z
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 708
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/MusicDataHandler;->mMusicDataUri:Landroid/net/Uri;

    .line 709
    .local v1, "musicUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v9, "most_played"

    aput-object v9, v2, v0

    .line 710
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "title != \'\' AND is_music = 1 AND _id=?"

    .line 712
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v0

    .line 713
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 715
    .local v5, "orderBy":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 718
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 719
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 722
    :cond_0
    const-string/jumbo v0, "most_played"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 721
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 724
    .local v7, "mostPlayed":I
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 725
    .local v8, "newValues":Landroid/content/ContentValues;
    const-string/jumbo v0, "recently_played"

    .line 726
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 725
    invoke-virtual {v8, v0, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 727
    const-string/jumbo v0, "most_played"

    add-int/lit8 v9, v7, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 728
    const-string/jumbo v3, "title != \'\' AND is_music = 1 AND _id=?"

    .line 730
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 731
    invoke-virtual {p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v0

    .line 733
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1, v8, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 735
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 738
    .end local v7    # "mostPlayed":I
    .end local v8    # "newValues":Landroid/content/ContentValues;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 741
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
