.class Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;
.super Ljava/lang/Object;
.source "DLServiceManager.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getOnDLPhoneSuggestionListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkRecommendedContactListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestRecommendedContactList(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    invoke-static {v1, p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$0(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Ljava/util/ArrayList;)V

    .line 277
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$1(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$1(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 286
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$5(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;Z)V

    .line 288
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$6(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 289
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mOnUpdateSuggestionListListener:Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$6(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;

    move-result-object v1

    .line 290
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$2(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;->onUpdateSuggestionList(Ljava/util/ArrayList;)V

    .line 294
    :cond_1
    return-void

    .line 281
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 282
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mPhoneSuggestionThumbnailList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$1(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$3(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v3

    .line 283
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/DLServiceManager$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->access$4(Lcom/sec/android/automotive/drivelink/common/DLServiceManager;)Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 282
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
