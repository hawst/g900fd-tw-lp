.class Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;
.super Ljava/lang/Object;
.source "TaskManagerReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->onAppResumed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 175
    const-string/jumbo v0, "TaskManagerReceiver"

    const-string/jumbo v1, "[onAppResumed] run "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 179
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestStartDrivingStatusMonitoring(Landroid/content/Context;)V

    .line 180
    const-string/jumbo v0, "TaskManagerReceiver"

    .line 181
    const-string/jumbo v1, "start driving manager resumed complete - foreground"

    .line 180
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mPrevState:I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$1()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 184
    const-string/jumbo v0, "TaskManagerReceiver"

    const-string/jumbo v1, "[onAppResumed] - TTS cache purge"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Landroid/content/Context;Z)I

    .line 188
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->stopWakeupBargeIn(Landroid/content/Context;Z)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$2;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;

    # getter for: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$0(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;)Landroid/content/Context;

    move-result-object v0

    # invokes: Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->stopTaskObserverService(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->access$2(Landroid/content/Context;)V

    .line 192
    return-void
.end method
