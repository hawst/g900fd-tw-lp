.class public Lcom/sec/android/automotive/drivelink/common/view/easing/ElasticEaseInOut;
.super Ljava/lang/Object;
.source "ElasticEaseInOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ElasticEaseInOut;->s:F

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ElasticEaseInOut;->s:F

    .line 16
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 11
    .param p1, "input"    # F

    .prologue
    .line 20
    const/4 v1, 0x0

    .line 21
    .local v1, "p":F
    const/4 v0, 0x0

    .line 22
    .local v0, "a":F
    const/4 v3, 0x0

    cmpl-float v3, p1, v3

    if-nez v3, :cond_0

    .line 23
    const/4 v3, 0x0

    .line 42
    :goto_0
    return v3

    .line 25
    :cond_0
    float-to-double v3, p1

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    div-double/2addr v3, v5

    double-to-float p1, v3

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v3, p1, v3

    if-nez v3, :cond_1

    .line 26
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 28
    :cond_1
    const/4 v3, 0x0

    cmpl-float v3, v1, v3

    if-nez v3, :cond_2

    .line 29
    const v1, 0x3ee66667    # 0.45000002f

    .line 31
    :cond_2
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-eqz v3, :cond_3

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_4

    .line 32
    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    .line 33
    const/high16 v3, 0x40800000    # 4.0f

    div-float v2, v1, v3

    .line 37
    .local v2, "s":F
    :goto_1
    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, p1, v3

    if-gez v3, :cond_5

    .line 38
    const-wide/high16 v3, -0x4020000000000000L    # -0.5

    .line 39
    float-to-double v5, v0

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    const/high16 v9, 0x41200000    # 10.0f

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr p1, v10

    mul-float/2addr v9, p1

    float-to-double v9, v9

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    mul-double/2addr v5, v7

    .line 40
    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v7, p1

    sub-float/2addr v7, v2

    const v8, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v7, v8

    div-float/2addr v7, v1

    invoke-static {v7}, Landroid/util/FloatMath;->sin(F)F

    move-result v7

    float-to-double v7, v7

    .line 39
    mul-double/2addr v5, v7

    .line 38
    mul-double/2addr v3, v5

    .line 40
    const-wide/16 v5, 0x0

    .line 38
    add-double/2addr v3, v5

    double-to-float v3, v3

    goto :goto_0

    .line 35
    .end local v2    # "s":F
    :cond_4
    const v3, 0x40490fdb    # (float)Math.PI

    div-float v3, v1, v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    float-to-double v3, v3

    const/high16 v5, 0x3f800000    # 1.0f

    div-float/2addr v5, v0

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->asin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    double-to-float v2, v3

    .restart local v2    # "s":F
    goto :goto_1

    .line 42
    :cond_5
    float-to-double v3, v0

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    const/high16 v7, -0x3ee00000    # -10.0f

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr p1, v8

    mul-float/2addr v7, p1

    float-to-double v7, v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    mul-double/2addr v3, v5

    .line 43
    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v5, p1

    sub-float/2addr v5, v2

    const v6, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    div-float/2addr v5, v1

    invoke-static {v5}, Landroid/util/FloatMath;->sin(F)F

    move-result v5

    float-to-double v5, v5

    .line 42
    mul-double/2addr v3, v5

    .line 43
    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    .line 42
    mul-double/2addr v3, v5

    .line 44
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 42
    add-double/2addr v3, v5

    .line 44
    const-wide/16 v5, 0x0

    .line 42
    add-double/2addr v3, v5

    double-to-float v3, v3

    goto/16 :goto_0
.end method
