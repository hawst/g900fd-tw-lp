.class public Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;
.super Ljava/lang/Object;
.source "CallEndCheckSingleton.java"


# static fields
.field private static mIsCallEnd:Z

.field private static sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    .line 6
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->mIsCallEnd:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static delCallEndCheckSingleton()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    .line 24
    :cond_0
    return-void
.end method

.method public static getCallEndCheckSingleton()Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;
    .locals 2

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    if-nez v0, :cond_1

    .line 10
    const-class v1, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    monitor-enter v1

    .line 11
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    .line 10
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->sCallEndCheckSingleton:Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    return-object v0

    .line 10
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getCallEndFlag()Z
    .locals 1

    .prologue
    .line 35
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->mIsCallEnd:Z

    return v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->mIsCallEnd:Z

    .line 28
    return-void
.end method

.method public setCallEndFlag(Z)V
    .locals 0
    .param p1, "check"    # Z

    .prologue
    .line 31
    sput-boolean p1, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->mIsCallEnd:Z

    .line 32
    return-void
.end method
