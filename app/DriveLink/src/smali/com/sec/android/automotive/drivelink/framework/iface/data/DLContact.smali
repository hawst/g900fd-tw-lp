.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLContact.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x7f83e186b2e35326L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getContactId()J
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getFirstName()Ljava/lang/String;
.end method

.method public abstract getLastName()Ljava/lang/String;
.end method

.method public abstract getPhoneNumberList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPhoneticName(Landroid/content/Context;)Ljava/lang/String;
.end method
