.class public final enum Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;
.super Ljava/lang/Enum;
.source "MultiWindowListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MultiHomeState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

.field public static final enum STATE_CLICK_LOCATION:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

.field public static final enum STATE_CLICK_MESSAGE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

.field public static final enum STATE_CLICK_MUSIC:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

.field public static final enum STATE_CLICK_PHONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

.field public static final enum STATE_MIC_OFF:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

.field public static final enum STATE_MIC_ON:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

.field public static final enum STATE_NONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    const-string/jumbo v1, "STATE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_NONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    const-string/jumbo v1, "STATE_MIC_ON"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_ON:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    const-string/jumbo v1, "STATE_MIC_OFF"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_OFF:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    const-string/jumbo v1, "STATE_CLICK_PHONE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_PHONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    const-string/jumbo v1, "STATE_CLICK_MESSAGE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MESSAGE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    const-string/jumbo v1, "STATE_CLICK_LOCATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_LOCATION:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    const-string/jumbo v1, "STATE_CLICK_MUSIC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MUSIC:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    .line 66
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_NONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_ON:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_MIC_OFF:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_PHONE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MESSAGE:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_LOCATION:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->STATE_CLICK_MUSIC:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowListener$OnMultiHomeListener$MultiHomeState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
