.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;
.super Ljava/lang/Object;
.source "InitialSearcher.java"


# static fields
.field private static final FIRST_CHARACTER:[C

.field private static final HANGUL_BASE_UNIT:C = '\u024c'

.field private static final HANGUL_UNICODE_BEGIN:C = '\uac00'

.field private static final HANGUL_UNICODE_END:C = '\ud7a3'

.field private static mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;

    .line 10
    const/16 v0, 0x13

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->FIRST_CHARACTER:[C

    .line 12
    return-void

    .line 10
    nop

    :array_0
    .array-data 2
        0x3131s
        0x3132s
        0x3134s
        0x3137s
        0x3138s
        0x3139s
        0x3141s
        0x3142s
        0x3143s
        0x3145s
        0x3146s
        0x3147s
        0x3148s
        0x3149s
        0x314as
        0x314bs
        0x314cs
        0x314ds
        0x314es
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method private getFirstCharacter(C)C
    .locals 4
    .param p1, "inputChar"    # C

    .prologue
    .line 34
    const v3, 0xac00

    sub-int v0, p1, v3

    .line 35
    .local v0, "begin":I
    div-int/lit16 v1, v0, 0x24c

    .line 36
    .local v1, "index":I
    sget-object v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->FIRST_CHARACTER:[C

    aget-char v2, v3, v1

    .line 38
    .local v2, "resultChar":C
    return v2
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;

    return-object v0
.end method

.method private isFirstCharacter(C)Z
    .locals 3
    .param p1, "inputChar"    # C

    .prologue
    .line 22
    const/4 v1, 0x0

    .line 23
    .local v1, "result":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->FIRST_CHARACTER:[C

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 30
    :goto_1
    return v1

    .line 24
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->FIRST_CHARACTER:[C

    aget-char v2, v2, v0

    if-ne p1, v2, :cond_1

    .line 25
    const/4 v1, 0x1

    .line 26
    goto :goto_1

    .line 23
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isKoreanCharacter(C)Z
    .locals 2
    .param p1, "inputChar"    # C

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    .local v0, "result":Z
    const v1, 0xac00

    if-gt v1, p1, :cond_0

    .line 44
    const v1, 0xd7a3

    if-gt p1, v1, :cond_0

    .line 45
    const/4 v0, 0x1

    .line 48
    :cond_0
    return v0
.end method


# virtual methods
.method public checkString(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 53
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v8

    .line 57
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    sub-int v4, v9, v10

    .line 58
    .local v4, "differenceOfEOF":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    .line 60
    .local v7, "targetLength":I
    if-ltz v4, :cond_0

    .line 64
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-gt v5, v4, :cond_0

    .line 65
    const/4 v6, 0x0

    .line 66
    .local v6, "offset":I
    :goto_2
    if-lt v6, v7, :cond_3

    .line 93
    :cond_2
    if-ne v6, v7, :cond_5

    .line 94
    const/4 v8, 0x1

    goto :goto_0

    .line 69
    :cond_3
    add-int v9, v5, v6

    .line 68
    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->isKoreanCharacter(C)Z

    move-result v2

    .line 70
    .local v2, "condition_1":Z
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->isFirstCharacter(C)Z

    move-result v3

    .line 72
    .local v3, "condition_2":Z
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 73
    add-int v9, v5, v6

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/InitialSearcher;->getFirstCharacter(C)C

    move-result v0

    .line 74
    .local v0, "char_1":C
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 76
    .local v1, "char_2":C
    if-ne v0, v1, :cond_2

    .line 77
    add-int/lit8 v6, v6, 0x1

    .line 81
    goto :goto_2

    .line 82
    .end local v0    # "char_1":C
    .end local v1    # "char_2":C
    :cond_4
    add-int v9, v6, v5

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 83
    .restart local v0    # "char_1":C
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 85
    .restart local v1    # "char_2":C
    if-ne v0, v1, :cond_2

    .line 86
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 64
    .end local v0    # "char_1":C
    .end local v1    # "char_2":C
    .end local v2    # "condition_1":Z
    .end local v3    # "condition_2":Z
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method
