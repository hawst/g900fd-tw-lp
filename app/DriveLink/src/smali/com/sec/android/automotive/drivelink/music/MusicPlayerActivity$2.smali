.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/music/MusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 1262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;)Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;
    .locals 1

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    return-object v0
.end method


# virtual methods
.method public OnBTBtnCallingState()V
    .locals 3

    .prologue
    .line 1379
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 1380
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const v2, 0x7f0a036b

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1381
    const/4 v2, 0x0

    .line 1379
    invoke-static {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1381
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1382
    return-void
.end method

.method public OnBTBtnClicked()V
    .locals 2

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1394
    return-void
.end method

.method public OnBTBtnPlay()V
    .locals 1

    .prologue
    .line 1387
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 1388
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 1389
    return-void
.end method

.method public OnBTBtnSeek()V
    .locals 1

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setPlayingTime()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$25(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 1408
    return-void
.end method

.method public OnError(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 1336
    const-string/jumbo v2, "MusicPlayerActivity "

    const-string/jumbo v3, "OnError"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1337
    new-instance v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2$1;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;)V

    .line 1346
    .local v0, "showError":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1348
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 1349
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1350
    const v3, 0x7f0a0117

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1351
    .local v1, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    .line 1352
    new-instance v3, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2$2;-><init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;)V

    .line 1351
    invoke-interface {v2, v1, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1375
    return-void
.end method

.method public OnFinished()V
    .locals 2

    .prologue
    .line 1325
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "OnFinished"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setFFREThreadWithStop()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$20(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 1327
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->bmActive:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$21(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1328
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->finish()V

    .line 1332
    :goto_0
    return-void

    .line 1330
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->finish()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$22(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    goto :goto_0
.end method

.method public OnSeekComplete()V
    .locals 2

    .prologue
    .line 1318
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "OnSeekComplete"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->progressUpdate()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$19(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 1321
    return-void
.end method

.method public OnSetVolumeControl(Z)V
    .locals 3
    .param p1, "onoff"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1412
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1413
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1414
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "JINSEIL onSetVolumeControl volume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1415
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$27(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 1416
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1417
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1418
    return-void
.end method

.method public OnShuffle(Z)V
    .locals 3
    .param p1, "bShuffle"    # Z

    .prologue
    .line 1398
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1399
    const-string/jumbo v0, "MusicPlayerActivity "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "shuffle is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1400
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setShuffle(Z)V

    .line 1401
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShuffleBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$24(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/Button;

    move-result-object v1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setSelected(Z)V

    .line 1403
    :cond_0
    return-void

    .line 1401
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public OnVolumeControlCommand(I)V
    .locals 3
    .param p1, "command"    # I

    .prologue
    const/4 v2, 0x1

    .line 1422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1423
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "JINSEIL onVolumeControlCommand volume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1424
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setVolumeUI()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$27(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundView(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$4(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1426
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1427
    return-void
.end method

.method public onMusicChanged(Z)V
    .locals 5
    .param p1, "isPlayed"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1265
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setCtrlsEnabled(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$7(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1266
    const-string/jumbo v0, "MusicPlayerActivity "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onMusicChanged : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1267
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setMusicInfo()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$8(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V

    .line 1268
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1269
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->multiWindowAccess:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$9(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 1271
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$10(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1272
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mShowSIPInMultiWindow:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$11(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1273
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1274
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSearchResultFlag(Z)V

    .line 1277
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1314
    :cond_0
    :goto_0
    return-void

    .line 1280
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$12(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1291
    :goto_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mPrevOrNextBtnClicked:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$10(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1292
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMediaPlayerListCount()I

    move-result v0

    if-eq v0, v3, :cond_2

    .line 1293
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setTouchPlayingTime(I)V
    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$14(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;I)V

    .line 1294
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$13(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1296
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$15(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1297
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isMusicSetValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1298
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1299
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLongClickState:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$17(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;->PREV_BTN_LONG_CLICKED:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

    if-ne v0, v1, :cond_5

    .line 1300
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "FForwardRewindThread false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1301
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-direct {v1, v4}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;-><init>(Z)V

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$18(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;)V

    .line 1302
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    iput-boolean v3, v0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 1303
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->start()V

    goto :goto_0

    .line 1283
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 1284
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$13(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    goto :goto_1

    .line 1287
    :cond_4
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    goto :goto_1

    .line 1304
    :cond_5
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mLongClickState:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$17(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;->NEXT_BTN_LONG_CLICKED:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$LongClickState;

    if-ne v0, v1, :cond_0

    .line 1305
    const-string/jumbo v0, "MusicPlayerActivity "

    const-string/jumbo v1, "FForwardRewindThread true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1306
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    new-instance v1, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    invoke-direct {v1, v3}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;-><init>(Z)V

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$18(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;)V

    .line 1307
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    iput-boolean v3, v0, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->isRunning:Z

    .line 1308
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mFFREThread:Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$16(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/FForwardRewindThread;->start()V

    goto/16 :goto_0
.end method
