.class public Lcom/sec/android/automotive/drivelink/location/POIParkingLotMapActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;
.source "POIParkingLotMapActivity.java"


# static fields
.field public static final POI_TYPE:Ljava/lang/String; = "poi_type"

.field public static final POI_TYPE_GAS:Ljava/lang/String; = "poi_gas"

.field public static final POI_TYPE_PARKING:Ljava/lang/String; = "poi_parking"

.field public static final STATE_LATITUDE:Ljava/lang/String; = "latitude"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method protected getLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/POIParkingLotMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 38
    const-class v2, Lcom/sec/android/automotive/drivelink/location/PointOfInterestMapActivity;

    .line 37
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    .local v0, "parkingIntent":Landroid/content/Intent;
    const-string/jumbo v1, "poi_type"

    .line 40
    const-string/jumbo v2, "poi_parking"

    .line 39
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/location/POIParkingLotMapActivity;->startActivity(Landroid/content/Intent;)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/POIParkingLotMapActivity;->finish()V

    .line 43
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseLocationActivity;->onDestroy()V

    .line 48
    return-void
.end method
