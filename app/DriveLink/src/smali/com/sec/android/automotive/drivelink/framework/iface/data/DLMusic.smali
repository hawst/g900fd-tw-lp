.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLMusic.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAlbum()Ljava/lang/String;
.end method

.method public abstract getAlbumArt()Landroid/graphics/Bitmap;
.end method

.method public abstract getAlbumId()I
.end method

.method public abstract getArtist()Ljava/lang/String;
.end method

.method public abstract getData()Ljava/lang/String;
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getDuration()I
.end method

.method public abstract getGenreName()Ljava/lang/String;
.end method

.method public abstract getId()I
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getUri()Landroid/net/Uri;
.end method

.method public abstract isAlbumArtCached()Z
.end method

.method public abstract resetInfo()V
.end method
