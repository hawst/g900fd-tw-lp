.class public Lcom/sec/android/automotive/drivelink/common/DLConstants;
.super Ljava/lang/Object;
.source "DLConstants.java"


# static fields
.field public static final ACTION_DL_SECURE_LOCK:Ljava/lang/String; = "com.sec.android.automotive.drivelink.ACTION_DL_SECURE_LOCK"

.field public static final ACTION_DL_VOICE_CANCEL:Ljava/lang/String; = "com.sec.android.automotive.drivelink.DL_VOICE_ACTION_CANCEL"

.field public static final ACTION_NAVIGATION_PACKAGE_NAME_TO_CALL:Ljava/lang/String; = "com.sec.android.automotive.drivelink.NAVIGATION_PACKAGE_NAME"

.field public static final CARD_CHANNEL_FOR_LBH:Ljava/lang/String; = "location based home"

.field public static final CARD_TYPE_FOR_LBH_DAILY_COMMUTE:Ljava/lang/String; = "lbh daily commutes"

.field public static final CARD_TYPE_FOR_LBH_GAS_STATION:Ljava/lang/String; = "lbh gas station"

.field public static final CARD_TYPE_FOR_LBH_OBD:Ljava/lang/String; = "lbh obd"

.field public static final CARD_TYPE_FOR_LBH_PARKING_LOT:Ljava/lang/String; = "lbh garage"

.field public static final CARD_TYPE_FOR_TOP_STORY:Ljava/lang/String; = "drive mode"

.field public static final EXTRA_DL_SECURE_LOCK_ON_OFF:Ljava/lang/String; = "EXTRA_DL_SECURE_LOCK_ON_OFF"

.field public static final INTENT_KEY_NAVIGATION_PACKAGE_NAME:Ljava/lang/String; = "INTENT_KEY_NAVIGATION_PACKAGE_NAME"

.field public static final PACKAGE_NAME_CALL:Ljava/lang/String; = "com.android.phone"

.field public static final PACKAGE_NAME_DRIVELINK:Ljava/lang/String; = "com.sec.android.automotive.drivelink"

.field public static final S_VOICE_LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
