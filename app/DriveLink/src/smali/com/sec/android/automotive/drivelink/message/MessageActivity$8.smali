.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 508
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 509
    const-string/jumbo v2, "MessageActivity"

    const-string/jumbo v3, "[Lee] mSearchBtn.setOnClickListene "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$44(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 512
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 513
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    .line 514
    invoke-virtual {v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 516
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$24(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/VoiceMessageActionBarLayout;->hideVoiceLayout()V

    .line 518
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$41(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 519
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 520
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$42(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 521
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 522
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 523
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 524
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$47(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v0, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 527
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$46(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 528
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    const-string/jumbo v3, ""

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$45(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/lang/String;)V

    .line 530
    const-string/jumbo v2, "MessageActivity"

    const-string/jumbo v3, "mSearchBtn - setAutoShrink : true"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setAutoShrink(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$18(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Z)V

    .line 544
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 533
    :cond_0
    const-string/jumbo v2, "MessageActivity"

    .line 534
    const-string/jumbo v3, "[Lee] mSearchBtn.setOnClickListene  } else { :: true "

    .line 533
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 538
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 539
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 541
    const v3, 0x7f0a0199

    .line 540
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 542
    .local v1, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method
