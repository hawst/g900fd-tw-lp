.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;
.super Ljava/lang/Object;
.source "OnDriveLinkNotificationListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->onNotifyLocationGroupShareRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

.field private final synthetic val$_location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private final synthetic val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;->val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;->val$_location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->mOnDriveLinkNotificationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;->val$_msgInfo:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;

    .line 256
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkNotificationListenerImp$12;->val$_location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 255
    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkNotificationListener;->onNotifyLocationGroupShareRequest(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMSGInfo;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 258
    :cond_0
    return-void
.end method
