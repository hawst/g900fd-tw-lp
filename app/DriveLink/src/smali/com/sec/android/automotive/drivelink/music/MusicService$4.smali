.class Lcom/sec/android/automotive/drivelink/music/MusicService$4;
.super Ljava/lang/Object;
.source "MusicService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$MusicRemoteControllerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$4;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    .line 1765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMetadataUpdate(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;J)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "artWork"    # Landroid/graphics/Bitmap;
    .param p4, "duration"    # J

    .prologue
    .line 1777
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$4;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$4;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$22(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->isPlaying()Z

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1778
    return-void
.end method

.method public onPlaybackStateUpdate(IJJ)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "currentPosition"    # J
    .param p4, "duration"    # J

    .prologue
    .line 1772
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$4;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicService$4;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicService;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicService;->mRemoteController:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$22(Lcom/sec/android/automotive/drivelink/music/MusicService;)Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->isPlaying()Z

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicService;->notifyMusicChanged(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->access$2(Lcom/sec/android/automotive/drivelink/music/MusicService;Z)V

    .line 1773
    return-void
.end method

.method public onRegistered()V
    .locals 0

    .prologue
    .line 1768
    return-void
.end method
