.class public Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;
.super Ljava/lang/Object;
.source "DriveLinkServiceMediator.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/common/pattern/Mediator;


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field private mColleagueList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectivityManager:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

.field private mContentManager:Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

.field private mDrivingManager:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

.field private mLocationManager:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

.field private mMessageManager:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

.field private mMirrorLinkManager:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

.field private mMusicPlayerManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

.field private mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

.field private mMusicRemotePlayerManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

.field private mNotificationManager:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

.field private mPushMessageManager:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;

.field private mRecommandationManagerForCall:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

.field private mRecommandationManagerForMessage:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

.field private mRecommendationManagerForLocation:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

.field private mSamsungAccountManager:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

.field private mUpdateManager:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string/jumbo v0, "DriveLinkServiceMediator"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method


# virtual methods
.method public createColleagues()V
    .locals 3

    .prologue
    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    .line 56
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mContentManager:Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mConnectivityManager:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mDrivingManager:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mLocationManager:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMirrorLinkManager:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMusicPlayerManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mNotificationManager:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    .line 71
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForCall;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mRecommandationManagerForCall:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    .line 73
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForMessage;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mRecommandationManagerForMessage:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    .line 75
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForLocation;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManagerForLocation;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mRecommendationManagerForLocation:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mUpdateManager:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    .line 79
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mSamsungAccountManager:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMessageManager:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mPushMessageManager:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    .line 83
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;-><init>()V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMusicRemotePlayerManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 88
    return-void

    .line 85
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;

    .line 86
    .local v0, "colleague":Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;
    invoke-interface {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;->setMediator(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Mediator;)V

    goto :goto_0
.end method

.method public getConnectivityManager()Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mConnectivityManager:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    return-object v0
.end method

.method public getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mContentManager:Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    return-object v0
.end method

.method public getDrivingManager()Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mDrivingManager:Lcom/sec/android/automotive/drivelink/framework/manager/driving/DrivingManager;

    return-object v0
.end method

.method public getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mLocationManager:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    return-object v0
.end method

.method public getMessageManager()Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMessageManager:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    return-object v0
.end method

.method public getMirrorLinkManager()Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMirrorLinkManager:Lcom/sec/android/automotive/drivelink/framework/manager/mirrorlink/MirrorLinkManager;

    return-object v0
.end method

.method public getMusicPlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMusicPlayerManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;

    return-object v0
.end method

.method public getMusicPlaylistManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMusicPlaylistManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicplaylist/MusicPlaylistManager;

    return-object v0
.end method

.method public getMusicRemotePlayerManager()Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mMusicRemotePlayerManager:Lcom/sec/android/automotive/drivelink/framework/manager/musicremotecontrol/MusicRemotePlayerManager;

    return-object v0
.end method

.method public getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mNotificationManager:Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    return-object v0
.end method

.method public getPushMessageManager()Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mPushMessageManager:Lcom/sec/android/automotive/drivelink/framework/manager/pushmessage/PushMessageManager;

    return-object v0
.end method

.method public getRecommandationManagerForCall()Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mRecommandationManagerForCall:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    return-object v0
.end method

.method public getRecommandationManagerForLocation()Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mRecommendationManagerForLocation:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    return-object v0
.end method

.method public getRecommandationManagerForMessage()Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mRecommandationManagerForMessage:Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationManager;

    return-object v0
.end method

.method public getSamsungAccountManager()Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mSamsungAccountManager:Lcom/sec/android/automotive/drivelink/framework/manager/samsungaccount/SamsungAccountManager;

    return-object v0
.end method

.method public getUpdateManager()Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mUpdateManager:Lcom/sec/android/automotive/drivelink/framework/manager/update/UpdateManager;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->createColleagues()V

    .line 94
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    const/4 v1, 0x1

    return v1

    .line 94
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;

    .line 95
    .local v0, "colleague":Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;->initialize(Landroid/content/Context;)Z

    goto :goto_0
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 103
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 108
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 109
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceMediator;->mColleagueList:Ljava/util/ArrayList;

    .line 110
    return-void

    .line 103
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;

    .line 104
    .local v0, "colleague":Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;
    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;->terminate(Landroid/content/Context;)V

    .line 105
    invoke-interface {v0, v3}, Lcom/sec/android/automotive/drivelink/framework/common/pattern/Colleague;->setMediator(Lcom/sec/android/automotive/drivelink/framework/common/pattern/Mediator;)V

    goto :goto_0
.end method
