.class public Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "BasicDialogFragment.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BasicDialogFragment"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 16
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStart()V

    .line 22
    const-string/jumbo v0, "BasicDialogFragment"

    const-string/jumbo v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->onPhraseSpotterStarted()V

    .line 24
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsThereDialogActive:Z

    .line 25
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 26
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 27
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 31
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStop()V

    .line 32
    const-string/jumbo v0, "BasicDialogFragment"

    const-string/jumbo v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/DLApplication;->mIsThereDialogActive:Z

    .line 34
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 35
    return-void
.end method
