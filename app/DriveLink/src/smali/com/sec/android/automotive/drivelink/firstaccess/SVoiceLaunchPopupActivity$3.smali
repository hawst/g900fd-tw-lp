.class Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$3;
.super Ljava/lang/Object;
.source "SVoiceLaunchPopupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x0

    .line 71
    const/4 v1, 0x4

    if-ne p2, v1, :cond_1

    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 75
    const-string/jumbo v2, "car_mode_on"

    .line 74
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    .line 75
    const/4 v2, 0x1

    .line 74
    if-ne v1, v2, :cond_0

    .line 76
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 77
    const-string/jumbo v2, "car_mode_on"

    const/4 v3, 0x0

    .line 76
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity$3;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;->finish()V

    .line 82
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 84
    :cond_1
    return v4

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
