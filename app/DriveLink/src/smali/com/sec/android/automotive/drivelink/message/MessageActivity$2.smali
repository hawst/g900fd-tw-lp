.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MessageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceMessageChangeUpdater$MSGSTATUS:[I


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceMessageChangeUpdater$MSGSTATUS()[I
    .locals 3

    .prologue
    .line 1545
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceMessageChangeUpdater$MSGSTATUS:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->values()[Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_CHANGE_NONE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_RECEIVE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_CHANGE_STATUS_TO_READ:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INBOX_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_INCOMMING_MESSAGE_LIST:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SEND_MESSAGE:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_SUGGESTION_UPDATED:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_COUNT:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->STATUS_REQUEST_UNREAD_MESSAGE_BY_INBOX:Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceMessageChangeUpdater$MSGSTATUS:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 1545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMessageStatusChanged(Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;)V
    .locals 2
    .param p1, "type"    # Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;

    .prologue
    .line 1552
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->$SWITCH_TABLE$com$sec$android$automotive$drivelink$common$DLServiceMessageChangeUpdater$MSGSTATUS()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/common/DLServiceMessageChangeUpdater$MSGSTATUS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1577
    :goto_0
    :pswitch_0
    return-void

    .line 1562
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInboxList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$0(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V

    .line 1563
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    goto :goto_0

    .line 1572
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 1573
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMessageSuggestionList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1572
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$3(Lcom/sec/android/automotive/drivelink/message/MessageActivity;Ljava/util/ArrayList;)V

    .line 1574
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->setCurrentMode(I)I
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$2(Lcom/sec/android/automotive/drivelink/message/MessageActivity;I)I

    goto :goto_0

    .line 1552
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
