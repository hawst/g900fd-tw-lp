.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
.source "DLLocationMyPlace.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->MY_PLACE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V

    .line 10
    return-void
.end method


# virtual methods
.method public getPlaceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getLocationDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlaceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->getLocationTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setPlaceAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setLocationDescription(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public setPlaceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "placeName"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;->setLocationTitle(Ljava/lang/String;)V

    .line 19
    return-void
.end method
