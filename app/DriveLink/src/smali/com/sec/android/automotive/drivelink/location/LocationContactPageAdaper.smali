.class public Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;
.super Landroid/support/v4/view/PagerAdapter;
.source "LocationContactPageAdaper.java"


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;

.field private mBtnNext:Landroid/widget/ImageButton;

.field private mBtnPrev:Landroid/widget/ImageButton;

.field private mContext:Landroid/content/Context;

.field private mDestination:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mFriendList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;"
        }
    .end annotation
.end field

.field private mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

.field private mNextClickListener:Landroid/view/View$OnClickListener;

.field private mPrevClickListener:Landroid/view/View$OnClickListener;

.field private final mViewHeightList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "destination"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    .local p2, "friendList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mViewHeightList:Ljava/util/List;

    .line 55
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    .line 56
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->inflater:Landroid/view/LayoutInflater;

    .line 57
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mDestination:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 58
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mContext:Landroid/content/Context;

    .line 59
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->initializeHeight()V

    .line 60
    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;)V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->prepareCallContact(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;)V

    return-void
.end method

.method private initializeHeight()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mViewHeightList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mViewHeightList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method private prepareCallContact(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;)V
    .locals 10
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 172
    move-object v3, p1

    check-cast v3, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getFriendName()Ljava/lang/String;

    move-result-object v1

    .line 173
    .local v1, "displayName":Ljava/lang/String;
    check-cast p1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .end local p1    # "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    .line 175
    .local v9, "phoneNumber":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const-wide/16 v3, 0x0

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 177
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v2, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v3, v0

    move-object v5, v9

    move v7, v6

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 179
    .local v2, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 181
    new-instance v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v8}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 182
    .local v8, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v0, v8, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 183
    const-string/jumbo v3, "DM_DIAL"

    invoke-static {v3, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 185
    return-void
.end method

.method private setPortraitHeightPage(Landroid/view/View;I)V
    .locals 3
    .param p1, "page"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 234
    if-nez p1, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 239
    const v0, 0x7f090205

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 240
    const v0, 0x7f090206

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 241
    const v0, 0x7f090203

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mViewHeightList:Ljava/util/List;

    .line 244
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 245
    const v2, 0x7f0d0029

    .line 244
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 242
    invoke-interface {v0, p2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mViewHeightList:Ljava/util/List;

    .line 249
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 250
    const v2, 0x7f0d002a

    .line 249
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 247
    invoke-interface {v0, p2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private updateButtonStatus(I)V
    .locals 3
    .param p1, "selected"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 191
    if-gtz p1, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnPrev:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 197
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnNext:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 206
    :goto_1
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnPrev:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    .line 201
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnNext:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnNext:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 127
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 128
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 132
    const/4 v0, -0x2

    return v0
.end method

.method public getPortraitHeightPage(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 216
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mViewHeightList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 218
    const v1, 0x7f0d0029

    .line 217
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 220
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mViewHeightList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 18
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 75
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;

    .line 77
    .local v12, "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030063

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    .line 79
    .local v13, "page":Landroid/view/View;
    const v2, 0x7f090206

    invoke-virtual {v13, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 78
    check-cast v10, Landroid/widget/LinearLayout;

    .line 81
    .local v10, "btCall":Landroid/widget/LinearLayout;
    const v2, 0x7f0901f8

    invoke-virtual {v13, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 80
    check-cast v15, Landroid/widget/TextView;

    .line 82
    .local v15, "tvName":Landroid/widget/TextView;
    const v2, 0x7f0901dd

    invoke-virtual {v13, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 84
    .local v14, "tvDistance":Landroid/widget/TextView;
    const v2, 0x7f090201

    invoke-virtual {v13, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnPrev:Landroid/widget/ImageButton;

    .line 85
    const v2, 0x7f090202

    invoke-virtual {v13, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnNext:Landroid/widget/ImageButton;

    .line 87
    const v2, 0x7f090204

    invoke-virtual {v13, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 88
    .local v16, "tvSpeed":Landroid/widget/TextView;
    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    .line 89
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v4

    .line 90
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mDestination:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mDestination:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v8

    .line 88
    invoke-static/range {v2 .. v9}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->getDistanceInKm(DDDD)Ljava/lang/String;

    move-result-object v11

    .line 92
    .local v11, "distance":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;->getFriendName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "km"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    const-string/jumbo v17, ""

    .line 97
    .local v17, "velocity":Ljava/lang/String;
    if-eqz v17, :cond_1

    const-string/jumbo v2, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 98
    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    if-eqz v2, :cond_0

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;->getMyBuddy(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    move-result-object v2

    .line 108
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 107
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 108
    if-eqz v2, :cond_2

    const/16 v2, 0x8

    .line 107
    :goto_1
    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 109
    :cond_0
    new-instance v2, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v12}, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper$1;-><init>(Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationFriend;)V

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v13, v1}, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->setPortraitHeightPage(Landroid/view/View;I)V

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnPrev:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mPrevClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mBtnNext:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mNextClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->updateButtonStatus(I)V

    .line 122
    return-object v13

    .line 100
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 101
    const v3, 0x7f0a0763

    .line 100
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 108
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public bridge synthetic instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 69
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setFriendList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "friendList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mFriendList:Ljava/util/List;

    .line 137
    return-void
.end method

.method public setGroup(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "mGroup"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 141
    return-void
.end method

.method public setNextButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mNextClickListener:Landroid/view/View$OnClickListener;

    .line 149
    return-void
.end method

.method public setPrevButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationContactPageAdaper;->mPrevClickListener:Landroid/view/View$OnClickListener;

    .line 145
    return-void
.end method
