.class public Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DLMMSBroadcastReceiver.java"


# instance fields
.field private mNotificationHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    .line 24
    return-void
.end method

.method private processMMS(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    const/4 v3, 0x0

    .line 51
    .local v3, "inNumber":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 52
    .local v1, "bundle":Landroid/os/Bundle;
    const-string/jumbo v8, "data"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 53
    .local v0, "buffer":[B
    const/4 v4, 0x0

    .line 55
    .local v4, "incomingNumber":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 56
    :try_start_0
    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "UTF-8"

    invoke-direct {v5, v0, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v4    # "incomingNumber":Ljava/lang/String;
    .local v5, "incomingNumber":Ljava/lang/String;
    move-object v4, v5

    .line 63
    .end local v5    # "incomingNumber":Ljava/lang/String;
    .restart local v4    # "incomingNumber":Ljava/lang/String;
    :cond_0
    :goto_0
    if-nez v4, :cond_2

    .line 85
    :cond_1
    :goto_1
    return-void

    .line 58
    :catch_0
    move-exception v2

    .line 60
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 67
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_2
    const-string/jumbo v8, "/TYPE"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 68
    .local v6, "indx":I
    if-lez v6, :cond_1

    add-int/lit8 v8, v6, -0xf

    if-lez v8, :cond_1

    .line 69
    add-int/lit8 v7, v6, -0xf

    .line 70
    .local v7, "newIndx":I
    invoke-virtual {v4, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 72
    const-string/jumbo v8, "+"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 73
    const-string/jumbo v8, "+"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 77
    :goto_2
    if-lez v6, :cond_1

    .line 78
    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 79
    const-string/jumbo v8, "MMSReceiver.java | onReceive"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "| from : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "|"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 79
    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v8, 0x4

    const/4 v9, 0x0

    new-instance v10, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 82
    invoke-direct {v10, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->sendMessage(IILjava/lang/Object;)V

    goto :goto_1

    .line 75
    :cond_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v6, v8, -0xb

    goto :goto_2
.end method

.method private sendMessage(IILjava/lang/Object;)V
    .locals 2
    .param p1, "notiType"    # I
    .param p2, "arg1"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 88
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 89
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 90
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 91
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 93
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 94
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->mNotificationHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "MY_TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[YANG] onReceive() :: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const-string/jumbo v1, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMMSBroadcastReceiver;->processMMS(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    return-void
.end method
