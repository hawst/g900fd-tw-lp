.class Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;
.super Ljava/lang/Object;
.source "MessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageActivity;->getDotIndicatorOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    .line 2380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 2384
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->hideInputMethod()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$61(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)V

    .line 2386
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$1(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2410
    :goto_0
    return-void

    .line 2388
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$57(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2389
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$55(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 2388
    sub-int/2addr v1, v2

    .line 2389
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 2388
    add-int v0, v1, v2

    .line 2390
    .local v0, "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSuggestionsViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$57(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 2393
    .end local v0    # "currentItem":I
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$60(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2394
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$59(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 2393
    sub-int/2addr v1, v2

    .line 2394
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 2393
    add-int v0, v1, v2

    .line 2395
    .restart local v0    # "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mInboxViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$60(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 2400
    .end local v0    # "currentItem":I
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2401
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->getIndex()I

    move-result v2

    .line 2400
    sub-int/2addr v1, v2

    .line 2401
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 2400
    add-int v0, v1, v2

    .line 2402
    .restart local v0    # "currentItem":I
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageActivity$20;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageActivity;->mSearchViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 2386
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_2
        0x41 -> :sswitch_2
        0x42 -> :sswitch_2
    .end sparse-switch
.end method
