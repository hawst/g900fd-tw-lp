.class public Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SplashScreenActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static mIsLauchSVoice:Z


# instance fields
.field private fadeInAniListener:Landroid/view/animation/Animation$AnimationListener;

.field private fadeOutAniListener:Landroid/view/animation/Animation$AnimationListener;

.field private isVideoPlay:Z

.field private mBackgroundLayout:Landroid/widget/RelativeLayout;

.field private mBottomLayout:Landroid/widget/LinearLayout;

.field private mCenterLayout:Landroid/widget/RelativeLayout;

.field private mContext:Landroid/content/Context;

.field private mFadeInAnime:Landroid/view/animation/Animation;

.field private mFadeOutAnime:Landroid/view/animation/Animation;

.field private mNextBtnView:Landroid/widget/TextView;

.field private mScaleExAnime:Landroid/view/animation/Animation;

.field private mVideoView:Landroid/widget/VideoView;

.field startClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mIsLauchSVoice:Z

    .line 43
    const-class v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    .line 44
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 43
    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 146
    new-instance v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->fadeInAniListener:Landroid/view/animation/Animation$AnimationListener;

    .line 164
    new-instance v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->fadeOutAniListener:Landroid/view/animation/Animation$AnimationListener;

    .line 197
    new-instance v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->startClickListener:Landroid/view/View$OnClickListener;

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/VideoView;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mNextBtnView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;Z)V
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->isVideoPlay:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mScaleExAnime:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private exitCarApp()V
    .locals 5

    .prologue
    .line 216
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "exitCarApp"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 221
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 222
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->terminate(Landroid/content/Context;)V

    .line 223
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->setWhenOnDestoryed()V

    .line 228
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "car_mode_on"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "car_mode_on"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_1
    :goto_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotify:Landroid/app/NotificationManager;

    if-eqz v2, :cond_2

    .line 237
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotify:Landroid/app/NotificationManager;

    const/16 v3, 0x1e3e

    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 240
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 241
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mContext:Landroid/content/Context;

    invoke-interface {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestStopDrivingStatusMonitoring(Landroid/content/Context;)V

    .line 242
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "stop driving manager"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 245
    .local v1, "service":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->stopService(Landroid/content/Intent;)Z

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->finish()V

    .line 248
    return-void

    .line 230
    .end local v1    # "service":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private initPlayingSplashIntro()V
    .locals 3

    .prologue
    .line 87
    const v1, 0x7f090185

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 88
    const v1, 0x7f090182

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    .line 89
    const v1, 0x7f090183

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    .line 90
    const v1, 0x7f090187

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mNextBtnView:Landroid/widget/TextView;

    .line 93
    const v1, 0x7f040028

    .line 92
    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeInAnime:Landroid/view/animation/Animation;

    .line 94
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeInAnime:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->fadeInAniListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 96
    const v1, 0x7f040029

    .line 95
    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;

    .line 97
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeOutAnime:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->fadeOutAniListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 99
    const v1, 0x7f04002a

    .line 98
    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mScaleExAnime:Landroid/view/animation/Animation;

    .line 100
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mNextBtnView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->startClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v1, 0x7f090181

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/VideoView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "android.resource://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 113
    const-string/jumbo v2, "/raw/welcome_vertical_intro"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 116
    .local v0, "video":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 117
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 118
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    .line 140
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeInAnime:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 142
    const v2, 0x7f040027

    .line 141
    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 143
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "finish-init"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 0

    .prologue
    .line 253
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->finish()V

    .line 254
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 344
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->exitCarApp()V

    .line 349
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 339
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 340
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mContext:Landroid/content/Context;

    .line 63
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->setContentView(I)V

    .line 66
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->setRequestedOrientation(I)V

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0a026d

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 69
    const v1, 0x7f0a0257

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 70
    const v1, 0x7f0a026e

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 71
    const v1, 0x7f0a026f

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 73
    sput-boolean v2, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mIsLauchSVoice:Z

    .line 75
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setContext(Landroid/content/Context;)V

    .line 78
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 79
    const-string/jumbo v1, "PREF_USER_TOURINGGUIDE_DONE"

    .line 78
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 81
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->initPlayingSplashIntro()V

    .line 83
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 329
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 330
    return-void
.end method

.method protected onResume()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 258
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 260
    new-instance v0, Landroid/content/Intent;

    .line 261
    const-string/jumbo v4, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    .line 260
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 262
    .local v0, "collapsePanelIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 264
    sget-boolean v4, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mIsLauchSVoice:Z

    if-eqz v4, :cond_3

    .line 265
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    invoke-virtual {v4}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInitState()I

    move-result v3

    .line 266
    .local v3, "vacSTate":I
    if-eq v3, v8, :cond_0

    .line 267
    if-ne v3, v9, :cond_2

    .line 270
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 271
    const-string/jumbo v5, "car_mode_on"

    .line 270
    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    if-ne v4, v8, :cond_1

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 273
    const-string/jumbo v5, "car_mode_on"

    const/4 v6, 0x0

    .line 272
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :cond_1
    :goto_0
    sget-object v4, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "mIsLauchSVoice : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v6, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mIsLauchSVoice:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->finish()V

    .line 279
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 293
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBackgroundLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 294
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 295
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 298
    sget-object v4, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "!!!!!!!!!! isVideoPlay = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->isVideoPlay:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->isVideoPlay:Z

    if-eqz v4, :cond_5

    .line 301
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v4, v10}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 311
    :goto_2
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mNextBtnView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->requestFocus()Z

    .line 314
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 315
    .local v2, "service":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 317
    return-void

    .line 274
    .end local v2    # "service":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 275
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 282
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .end local v3    # "vacSTate":I
    :cond_3
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    invoke-virtual {v4}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInitState()I

    move-result v3

    .line 283
    .restart local v3    # "vacSTate":I
    sget-object v4, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "VAC init state : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    if-ne v3, v8, :cond_4

    .line 286
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceLaunchPopupActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 287
    :cond_4
    if-ne v3, v9, :cond_2

    .line 288
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/automotive/drivelink/firstaccess/SVoiceDisabledExitActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->finish()V

    goto :goto_1

    .line 303
    :cond_5
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->isVideoPlay:Z

    .line 304
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v4}, Landroid/widget/VideoView;->start()V

    .line 305
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mFadeInAnime:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 306
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 307
    const v5, 0x7f040027

    .line 306
    invoke-static {p0, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 308
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v4, v7}, Landroid/widget/VideoView;->setFocusable(Z)V

    .line 309
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v4, v7}, Landroid/widget/VideoView;->setFocusableInTouchMode(Z)V

    goto :goto_2
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 335
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onStop()V

    .line 336
    return-void
.end method
