.class public Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;
.super Landroid/widget/BaseAdapter;
.source "ParticipantNotJoinedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private participantList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->context:Landroid/content/Context;

    .line 42
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 43
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->participantList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->participantList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->participantList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->participantList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 57
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->getItem(I)Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;

    move-result-object v4

    .line 67
    .local v4, "participant":Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
    if-nez p2, :cond_1

    .line 68
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->context:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 69
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030072

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 71
    .local v5, "view":Landroid/view/View;
    new-instance v1, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;

    const/4 v6, 0x0

    invoke-direct {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;-><init>(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)V

    .line 72
    .local v1, "holder":Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;
    const v6, 0x7f0901a9

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->setImgUser(Landroid/widget/ImageView;)V

    .line 73
    const v6, 0x7f090123

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->setTvName(Landroid/widget/TextView;)V

    .line 75
    const v6, 0x7f0901aa

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 74
    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->setTvLetterUser(Landroid/widget/TextView;)V

    .line 77
    const v6, 0x7f0901fa

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 76
    invoke-virtual {v1, v6}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->setTvNotJoined(Landroid/widget/TextView;)V

    .line 79
    invoke-virtual {v5, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 86
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->dlinkService:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->context:Landroid/content/Context;

    .line 87
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDlContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v8

    .line 86
    invoke-interface {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getContactImage(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 90
    .local v2, "image":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    .line 91
    # getter for: Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->access$1(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 92
    # getter for: Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 94
    const v7, 0x7f0201a3

    const v8, 0x7f0201a2

    .line 93
    invoke-static {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 95
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->getImgUser()Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 108
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDlContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 109
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->getTvName()Landroid/widget/TextView;

    move-result-object v6

    .line 110
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getDlContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    .line 109
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :goto_2
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->getTvName()Landroid/widget/TextView;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 116
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->hasJoined()Z

    move-result v6

    if-nez v6, :cond_0

    .line 117
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->getTvNotJoined()Landroid/widget/TextView;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    :cond_0
    new-instance v6, Landroid/widget/AbsListView$LayoutParams;

    .line 121
    const/4 v7, -0x2

    .line 122
    const/4 v8, -0x2

    invoke-direct {v6, v7, v8}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 120
    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 125
    return-object v5

    .line 82
    .end local v1    # "holder":Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;
    .end local v2    # "image":Landroid/graphics/Bitmap;
    .end local v5    # "view":Landroid/view/View;
    :cond_1
    move-object v5, p2

    .line 83
    .restart local v5    # "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;
    goto :goto_0

    .line 96
    .restart local v2    # "image":Landroid/graphics/Bitmap;
    :cond_2
    if-nez v2, :cond_3

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 97
    # getter for: Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->access$1(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    # getter for: Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    # getter for: Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 101
    :cond_3
    # getter for: Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->imgUser:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->access$1(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    # getter for: Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->tvLetterUser:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->access$2(Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 104
    const v7, 0x7f0201a2

    .line 103
    invoke-static {v6, v2, v7}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->getImgUser()Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 112
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter$ViewHolder;->getTvName()Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public updateList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "notJoinedParticipants":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->participantList:Ljava/util/List;

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/ParticipantNotJoinedAdapter;->notifyDataSetChanged()V

    .line 131
    return-void
.end method
