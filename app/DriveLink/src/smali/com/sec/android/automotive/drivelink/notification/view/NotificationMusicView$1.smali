.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;
.super Ljava/lang/Object;
.source "NotificationMusicView.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/music/MusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    .line 673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnBTBtnCallingState()V
    .locals 0

    .prologue
    .line 707
    return-void
.end method

.method public OnBTBtnClicked()V
    .locals 2

    .prologue
    .line 715
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    .line 716
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;Z)V

    .line 717
    return-void
.end method

.method public OnBTBtnPlay()V
    .locals 0

    .prologue
    .line 711
    return-void
.end method

.method public OnBTBtnSeek()V
    .locals 0

    .prologue
    .line 727
    return-void
.end method

.method public OnError(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 702
    return-void
.end method

.method public OnFinished()V
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->cancelNotiFlow()V

    .line 691
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->dismiss()V

    .line 692
    return-void
.end method

.method public OnSeekComplete()V
    .locals 0

    .prologue
    .line 686
    return-void
.end method

.method public OnSetVolumeControl(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 731
    return-void
.end method

.method public OnShuffle(Z)V
    .locals 0
    .param p1, "bShuffle"    # Z

    .prologue
    .line 722
    return-void
.end method

.method public OnVolumeControlCommand(I)V
    .locals 0
    .param p1, "command"    # I

    .prologue
    .line 735
    return-void
.end method

.method public onMusicChanged(Z)V
    .locals 5
    .param p1, "isPlayed"    # Z

    .prologue
    .line 677
    const-string/jumbo v0, "i"

    const-string/jumbo v1, "MUSIC"

    const-string/jumbo v2, "[NotificationMusicView]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " onMusicChanged - isPlayed : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 678
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 677
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView$1;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->setMusicInfo()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;->access$0(Lcom/sec/android/automotive/drivelink/notification/view/NotificationMusicView;)V

    .line 681
    return-void
.end method
