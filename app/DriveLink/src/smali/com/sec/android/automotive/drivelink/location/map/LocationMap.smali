.class public abstract Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
.super Ljava/lang/Object;
.source "LocationMap.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/location/map/view/ILocationMap;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;,
        Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;
    }
.end annotation


# instance fields
.field private mFragmentLoaded:Z

.field private mIsCluster:Z

.field private mIsDraggable:Z

.field private mLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

.field private mMapFragment:Landroid/support/v4/app/Fragment;

.field private final mMarkes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;"
        }
    .end annotation
.end field

.field private mZoomLevel:F

.field private mapCompletedListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;

.field private mapDragDropListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

.field protected zoomCameraCurrent:F


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, v3, v4, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mZoomLevel:F

    .line 30
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mFragmentLoaded:Z

    .line 31
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mIsDraggable:Z

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mIsCluster:Z

    .line 33
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mapCompletedListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;

    .line 34
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mapDragDropListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    .line 37
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMapFragment:Landroid/support/v4/app/Fragment;

    .line 26
    return-void
.end method


# virtual methods
.method public addMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 1
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public clearMarkers()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 151
    return-void
.end method

.method public abstract createUpdateableRoute(I)Lcom/sec/android/automotive/drivelink/location/model/IGraphicRoute;
.end method

.method public abstract drawRoute(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;)V"
        }
    .end annotation
.end method

.method protected fragmentLoaded()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mFragmentLoaded:Z

    return v0
.end method

.method public getDragDropListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mapDragDropListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    return-object v0
.end method

.method public getLocationPoint()Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    return-object v0
.end method

.method public getMapCompletedListener()Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mapCompletedListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;

    return-object v0
.end method

.method public getMapFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMapFragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public getZoomCameraCurrent()F
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->zoomCameraCurrent:F

    return v0
.end method

.method public getZoomLevel()F
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mZoomLevel:F

    return v0
.end method

.method public getmMarkes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isCluster()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mIsCluster:Z

    return v0
.end method

.method public isDraggable()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mIsDraggable:Z

    return v0
.end method

.method public removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 1
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method public abstract setCompassEnabled(Z)V
.end method

.method public setDragAndDropListener(Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mapDragDropListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMarkerDragDropListener;

    .line 142
    return-void
.end method

.method public setFragmentLoaded(Z)V
    .locals 0
    .param p1, "loaded"    # Z

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mFragmentLoaded:Z

    .line 117
    return-void
.end method

.method public setIsDraggable(Z)V
    .locals 0
    .param p1, "isDraggable"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mIsDraggable:Z

    .line 109
    return-void
.end method

.method public setLocationPoint(DD)V
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 45
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 46
    return-void
.end method

.method public setMapCompletedListener(Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;)V
    .locals 0
    .param p1, "onMapCreatedListener"    # Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mapCompletedListener:Lcom/sec/android/automotive/drivelink/location/map/LocationMap$OnMapCreatedListener;

    .line 134
    return-void
.end method

.method protected setMapFragment(Landroid/support/v4/app/Fragment;)V
    .locals 0
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMapFragment:Landroid/support/v4/app/Fragment;

    .line 97
    return-void
.end method

.method public abstract setZoomControlsEnabled(Z)V
.end method

.method public setZoomLevel(DD)V
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 87
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 88
    return-void
.end method

.method public setZoomLevel(F)V
    .locals 0
    .param p1, "level"    # F

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mZoomLevel:F

    .line 79
    return-void
.end method

.method public setZoomLevel(FDDI)V
    .locals 1
    .param p1, "level"    # F
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "userIndex"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mZoomLevel:F

    .line 83
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mLocationPoint:Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 84
    return-void
.end method

.method public updateIconMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    .param p2, "icon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 54
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 55
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 56
    .local v0, "index":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->mMarkes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .line 57
    .local v1, "item":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    if-eqz v1, :cond_0

    .line 58
    invoke-virtual {v1, p2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;->setIcon(Landroid/graphics/Bitmap;)V

    .line 61
    .end local v0    # "index":I
    .end local v1    # "item":Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;
    :cond_0
    return-void
.end method

.method public abstract zoomToArea(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;I)V
.end method
