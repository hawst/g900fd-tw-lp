.class public Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;
.super Ljava/lang/Object;
.source "ParticipantModel.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0xb5de1e982d8dbc3L


# instance fields
.field private distance:D

.field private dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

.field private host:Z

.field private location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private me:Z

.field private name:Ljava/lang/String;

.field private phone:Ljava/lang/String;

.field private status:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;D)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "distance"    # D

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->name:Ljava/lang/String;

    .line 43
    iput-wide p2, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->distance:D

    .line 44
    return-void
.end method


# virtual methods
.method public getDistance()D
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->distance:D

    return-wide v0
.end method

.method public getDlContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    return-object v0
.end method

.method public getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->phone:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->status:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    return-object v0
.end method

.method public hasJoined()Z
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->status:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->ACCEPTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeft()Z
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->status:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->WITHDRAWAL:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHost()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->host:Z

    return v0
.end method

.method public isMe()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->me:Z

    return v0
.end method

.method public rejected()Z
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->status:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;->REJECTED:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0
    .param p1, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 114
    return-void
.end method

.method public setDistance(D)V
    .locals 0
    .param p1, "distance"    # D

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->distance:D

    .line 74
    return-void
.end method

.method public setHost(Z)V
    .locals 0
    .param p1, "host"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->host:Z

    .line 82
    return-void
.end method

.method public setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 0
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->location:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 126
    return-void
.end method

.method public setMe(Z)V
    .locals 0
    .param p1, "me"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->me:Z

    .line 90
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->name:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setPhone(Ljava/lang/String;)V
    .locals 0
    .param p1, "phone"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->phone:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setStatus(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;)V
    .locals 0
    .param p1, "status"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->status:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 98
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/ParticipantModel;->name:Ljava/lang/String;

    return-object v0
.end method
