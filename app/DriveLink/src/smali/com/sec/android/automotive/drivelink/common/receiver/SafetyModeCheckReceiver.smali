.class public Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SafetyModeCheckReceiver.java"


# static fields
.field private static final MODE_DISABLED:I = 0x5

.field private static final MODE_DISABLING:I = 0x4

.field private static final MODE_ENABLED:I = 0x3

.field private static final MODE_ENABLING:I = 0x2

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;

    .line 14
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 13
    sput-object v0, Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;->TAG:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 22
    const-string/jumbo v2, "android.intent.action.EMERGENCY_STATE_CHANGED"

    .line 23
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 23
    if-eqz v2, :cond_1

    .line 24
    const-string/jumbo v2, "reason"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 25
    .local v1, "reason":I
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 26
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Safefy mode enabled"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 29
    const-string/jumbo v3, "car_mode_on"

    .line 28
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    .line 29
    const/4 v3, 0x1

    .line 28
    if-ne v2, v3, :cond_1

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 31
    const-string/jumbo v3, "car_mode_on"

    const/4 v4, 0x0

    .line 30
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v1    # "reason":I
    :cond_1
    :goto_0
    return-void

    .line 32
    .restart local v1    # "reason":I
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
