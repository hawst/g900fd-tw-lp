.class Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;
.super Ljava/lang/Object;
.source "NotificationController.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRemoveDirect(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 3
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "gotoMulti"    # Z

    .prologue
    .line 102
    const-string/jumbo v0, "[NotificationController]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRemoveDirect:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->removeNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Z)V

    .line 104
    if-eqz p2, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$4(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 108
    :cond_0
    return-void
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 73
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Remove Notification Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    .line 75
    return-void
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "command"    # Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .prologue
    .line 80
    const-string/jumbo v0, "[NotificationController]"

    const-string/jumbo v1, "Remove Notification and do command Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    if-eqz p2, :cond_0

    .line 82
    invoke-static {p2}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$1(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->dismissNotification(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$0(Lcom/sec/android/automotive/drivelink/notification/NotificationController;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V

    goto :goto_0
.end method

.method public onSetVoiceFlowIdRequest(Ljava/lang/String;)V
    .locals 1
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 93
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$2(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationController$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationController;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationController;->mNotiCallbackListener:Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationController;->access$2(Lcom/sec/android/automotive/drivelink/notification/NotificationController;)Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/notification/NotificationController$NotificationCallbackListener;->setVoiceFlowId(Ljava/lang/String;)V

    .line 98
    :cond_0
    return-void
.end method
