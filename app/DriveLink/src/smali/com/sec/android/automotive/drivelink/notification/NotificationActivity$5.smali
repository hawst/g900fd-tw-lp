.class Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;
.super Ljava/lang/Object;
.source "NotificationActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    .line 578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 582
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Disappear Animation End."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$8()Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotification(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Z)V

    .line 585
    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->nextCommand:Lcom/sec/android/automotive/drivelink/notification/INotiCommand;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$8()Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$9(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/INotiCommand;->action(Landroid/content/Context;)V

    .line 586
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$1(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 590
    :goto_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    iput-boolean v2, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    .line 591
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$7(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Landroid/view/animation/Animation;)V

    .line 592
    const-string/jumbo v0, "[NotificationActivity]"

    const-string/jumbo v1, "Disappear Animation End..."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    return-void

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->removeNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->access$3(Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 599
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 604
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/notification/NotificationActivity;->bEndAnimationStarted:Z

    .line 605
    return-void
.end method
