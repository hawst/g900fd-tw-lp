.class public Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;
.super Landroid/os/AsyncTask;
.source "FindMyCarMapViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyAddressSearchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private mAddressSearchListener:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;)V
    .locals 0

    .prologue
    .line 571
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->doInBackground([Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Ljava/lang/String;
    .locals 5
    .param p1, "params"    # [Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 585
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 598
    :cond_0
    :goto_0
    return-object v0

    .line 588
    :cond_1
    aget-object v1, p1, v2

    # invokes: Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->isValidLocationPoint(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$0(Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 592
    aget-object v0, p1, v2

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLatitude()D

    move-result-wide v0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->getLongitude()D

    move-result-wide v2

    .line 593
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    .line 591
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getAddressFromLatLng(DDLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->address:Ljava/lang/String;

    .line 595
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->address:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string/jumbo v0, ""

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 596
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a021b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->address:Ljava/lang/String;

    .line 598
    :cond_3
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->address:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .param p1, "pAddress"    # Ljava/lang/String;

    .prologue
    .line 604
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->this$0:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;->access$1(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity;Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->mAddressSearchListener:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->mAddressSearchListener:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->address:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;->onAddressSearchFinish(Ljava/lang/String;)V

    .line 609
    :cond_0
    return-void
.end method

.method public setOnAddressSearchListener(Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;

    .prologue
    .line 579
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$MyAddressSearchTask;->mAddressSearchListener:Lcom/sec/android/automotive/drivelink/location/FindMyCarMapViewActivity$OnAddressSearchListener;

    .line 580
    return-void
.end method
