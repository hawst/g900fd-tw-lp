.class Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$2;
.super Ljava/lang/Object;
.source "DrawerMenuLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->setActionBar(Ljava/lang/String;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$2;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$2;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    # getter for: Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->mIsDrawerMenuOpen:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->access$8(Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$2;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->closeDrawerMenu()V

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout$2;->this$0:Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/DrawerMenuLayout;->openDrawerMenu()V

    goto :goto_0
.end method
