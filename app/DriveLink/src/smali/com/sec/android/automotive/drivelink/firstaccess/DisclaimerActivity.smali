.class public Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;
.super Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;
.source "DisclaimerActivity.java"


# static fields
.field public static final DRIVELINK_APP:I = 0x1e3e

.field private static final TAG:Ljava/lang/String;

.field public static mNotification:Landroid/app/Notification;

.field public static mNotify:Landroid/app/NotificationManager;


# instance fields
.field DuringCallToastHandlerdler:Landroid/os/Handler;

.field private volatile isAlive:Z

.field private mActivity:Landroid/app/Activity;

.field private volatile mBlockBackKey:Z

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mCheckRunProgress:Z

.field private mCheckStartHomeActivity:Z

.field private final mConnectionMusic:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mProgress:I

.field private mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

.field private mSaveProgress:I

.field private mWarningProgress:Landroid/widget/ProgressBar;

.field private runHandler:Landroid/os/Handler;

.field private runProgress:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    const-class v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    .line 72
    sput-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotify:Landroid/app/NotificationManager;

    .line 73
    sput-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotification:Landroid/app/Notification;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;-><init>()V

    .line 69
    iput v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I

    .line 70
    iput v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mSaveProgress:I

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBlockBackKey:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckRunProgress:Z

    .line 82
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckStartHomeActivity:Z

    .line 320
    new-instance v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mConnectionMusic:Landroid/content/ServiceConnection;

    .line 470
    new-instance v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->DuringCallToastHandlerdler:Landroid/os/Handler;

    .line 62
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runProgress:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;I)V
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mWarningProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBlockBackKey:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Z)V
    .locals 0

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBlockBackKey:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckStartHomeActivity:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Z)V
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckStartHomeActivity:Z

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Z)V
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckRunProgress:Z

    return-void
.end method

.method private registerNoti(Landroid/content/Context;)V
    .locals 6
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const v4, 0x7f0a0265

    const/16 v5, 0x1e3e

    const/4 v3, 0x0

    .line 342
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/OngoingNotiActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 347
    .local v0, "mIntent":Landroid/content/Intent;
    const/high16 v2, 0x30000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 351
    const-string/jumbo v2, "notification"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 350
    check-cast v2, Landroid/app/NotificationManager;

    sput-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotify:Landroid/app/NotificationManager;

    .line 353
    invoke-static {p1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 356
    .local v1, "pintent":Landroid/app/PendingIntent;
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 358
    const v3, 0x7f0a0264

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 357
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 360
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 359
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 361
    const v3, 0x7f0201e8

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 363
    new-instance v3, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v3}, Landroid/app/Notification$BigTextStyle;-><init>()V

    .line 364
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 363
    invoke-virtual {v3, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    .line 362
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 365
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 356
    sput-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotification:Landroid/app/Notification;

    .line 368
    sget-object v2, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotify:Landroid/app/NotificationManager;

    sget-object v3, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v5, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 370
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 371
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    .line 373
    sget-object v3, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mNotification:Landroid/app/Notification;

    .line 371
    invoke-virtual {v2, v5, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->startForegroundService(ILandroid/app/Notification;)V

    .line 375
    :cond_0
    return-void
.end method

.method private requestThreadStop()V
    .locals 1

    .prologue
    .line 492
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z

    .line 493
    return-void
.end method

.method private requestWarningFinish()V
    .locals 2

    .prologue
    .line 496
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 497
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 498
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->terminate(Landroid/content/Context;)V

    .line 499
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->setWhenOnDestoryed()V

    .line 501
    :cond_0
    return-void
.end method


# virtual methods
.method public activateBluetooth()V
    .locals 3

    .prologue
    .line 378
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 380
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "BT Disabled!! >> CarMode turn on BT"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    .line 385
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 386
    const-string/jumbo v1, "PREF_USER_BT_PREVIOUS_STATE"

    const/4 v2, 0x1

    .line 385
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 398
    :goto_0
    return-void

    .line 390
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "BT aleady enabled"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public activateGPS()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const-wide v3, 0x401199999999999aL    # 4.4

    .line 401
    const-string/jumbo v1, "location"

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 402
    .local v0, "mLocationManager":Landroid/location/LocationManager;
    if-eqz v0, :cond_2

    .line 404
    const-string/jumbo v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 406
    if-eqz v0, :cond_1

    .line 408
    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 411
    const-string/jumbo v2, "PREF_USER_GPS_PREVIOUS_STATE_IS_POWERSAVING"

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 414
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    int-to-double v1, v1

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_0

    .line 415
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "GPS HIGH ACCURACY on"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 417
    const-string/jumbo v2, "location_mode"

    .line 416
    invoke-static {v1, v2, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 423
    const-string/jumbo v2, "PREF_USER_GPS_PREVIOUS_STATE"

    .line 422
    invoke-virtual {v1, v2, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 426
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    int-to-double v1, v1

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_0

    .line 427
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "GPS SENSORS ONLY on"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 429
    const-string/jumbo v2, "location_mode"

    .line 428
    invoke-static {v1, v2, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 434
    :cond_2
    sget-object v1, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "GPS provider is not enabled."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 518
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mBlockBackKey:Z

    if-eqz v0, :cond_0

    .line 519
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "blocking backkey in 70% of progress"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :goto_0
    return-void

    .line 523
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 524
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->requestWarningFinish()V

    .line 525
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->finish()V

    .line 526
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBackgroundVisibleBehindChanged(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 582
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onBackgroundVisibleBehindChanged(Z)V

    .line 583
    const-string/jumbo v0, "JINSEIL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onBackgroundVisibleBehindChanged! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->initValue(Landroid/content/Context;)V

    .line 89
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/DLApplication;->init()V

    .line 90
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->start()V

    .line 92
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "onCreate"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string/jumbo v5, "JINSEIL"

    const-string/jumbo v6, "onCreate!"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v6, "intent.stop.app-in-app"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 99
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onAppLaunched()V

    .line 103
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 104
    .local v3, "mintent":Landroid/content/Intent;
    new-instance v5, Landroid/content/ComponentName;

    .line 105
    const-string/jumbo v6, "com.sec.android.automotive.drivelinkremote"

    .line 106
    const-string/jumbo v7, "com.sec.android.automotive.drivelinkremote.DeathCheckService"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 107
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 108
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "Start service for remote car mode"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->registerNoti(Landroid/content/Context;)V

    .line 124
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v5

    .line 125
    const-string/jumbo v6, "PREF_CAR_APP_RUNNING"

    .line 124
    invoke-virtual {v5, v6, v8}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 126
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "PREF_CAR_APP_RUNNING is set to true"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setContext(Landroid/content/Context;)V

    .line 129
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v5

    if-nez v5, :cond_0

    .line 130
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 131
    :cond_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInitState()I

    move-result v5

    if-eqz v5, :cond_3

    .line 132
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 133
    .local v2, "intent":Landroid/content/Intent;
    const-class v5, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    invoke-virtual {v2, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 134
    const-string/jumbo v5, "EXTRA_NAME_FIRST_ACCESS"

    invoke-virtual {v2, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 135
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->startActivity(Landroid/content/Intent;)V

    .line 139
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 140
    const-string/jumbo v6, "car_mode_on"

    .line 139
    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 142
    const-string/jumbo v6, "car_mode_on"

    const/4 v7, 0x1

    .line 141
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :cond_2
    :goto_0
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "finish / go to splash"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->finish()V

    .line 150
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v5, :cond_4

    .line 151
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 152
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "Now MultiWindowMode!!! change to normal Window!!"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->normalWindow()V

    .line 156
    :cond_4
    const v5, 0x7f030003

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->setContentView(I)V

    .line 157
    new-instance v5, Ljava/lang/StringBuilder;

    const v6, 0x7f0a0270

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, ". "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 158
    const v6, 0x7f0a0263

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 157
    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 160
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 161
    const/16 v5, 0xe

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->setRequestedOrientation(I)V

    .line 164
    :cond_5
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mActivity:Landroid/app/Activity;

    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    .line 167
    const v5, 0x7f090013

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    .line 166
    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mWarningProgress:Landroid/widget/ProgressBar;

    .line 170
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 171
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mWarningProgress:Landroid/widget/ProgressBar;

    if-eqz v5, :cond_6

    .line 172
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mWarningProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 179
    :cond_6
    :goto_1
    const v5, 0x7f090012

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 180
    new-instance v6, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V

    .line 179
    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    new-instance v4, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V

    .line 221
    .local v4, "progressHandler":Landroid/os/Handler;
    new-instance v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;

    .line 235
    new-instance v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;

    invoke-direct {v5, p0, v4}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;Landroid/os/Handler;)V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runProgress:Ljava/lang/Runnable;

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->activateBluetooth()V

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->activateGPS()V

    .line 262
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->changeSoundMode(Landroid/content/Context;)Z

    .line 263
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->changeNfcMode(Landroid/content/Context;)Z

    .line 266
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 267
    const-string/jumbo v6, "vibrate_when_ringing"

    .line 266
    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    .line 265
    sput v5, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->VIBRATE_WHEN_RINGING:I
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 273
    :goto_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 274
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mContext:Landroid/content/Context;

    invoke-interface {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestStartDrivingStatusMonitoring(Landroid/content/Context;)V

    .line 275
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "start driving manager"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "mCheckRunProgress = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckRunProgress:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 279
    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckRunProgress:Z

    if-nez v5, :cond_7

    .line 280
    new-instance v5, Ljava/lang/Thread;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runProgress:Ljava/lang/Runnable;

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 281
    iput-boolean v8, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mCheckRunProgress:Z

    .line 293
    :cond_7
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v5

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->registerMusicRemoteControl(Landroid/content/Context;)V

    .line 295
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    .line 296
    const-string/jumbo v6, "register MusicRemoteControl - DisclaimerActivity::onCreate()"

    .line 295
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->isStarted()Z

    move-result v5

    if-nez v5, :cond_8

    .line 300
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    .line 301
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mConnectionMusic:Landroid/content/ServiceConnection;

    .line 300
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->StartService(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 304
    :cond_8
    new-instance v5, Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;

    invoke-direct {v5}, Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;-><init>()V

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

    .line 305
    sget-object v5, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "register - mSafetyModeCheckReceiver"

    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

    new-instance v6, Landroid/content/IntentFilter;

    .line 307
    const-string/jumbo v7, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p0, v5, v6}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 312
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "car_mode_on"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    .line 311
    if-nez v5, :cond_9

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 314
    const-string/jumbo v6, "car_mode_on"

    const/4 v7, 0x1

    .line 313
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 318
    :cond_9
    :goto_3
    return-void

    .line 143
    .end local v4    # "progressHandler":Landroid/os/Handler;
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 175
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_a
    const v5, 0x7f090011

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 268
    .restart local v4    # "progressHandler":Landroid/os/Handler;
    :catch_1
    move-exception v1

    .line 270
    .local v1, "e1":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_2

    .line 315
    .end local v1    # "e1":Landroid/provider/Settings$SettingNotFoundException;
    :catch_2
    move-exception v0

    .line 316
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 506
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->requestThreadStop()V

    .line 507
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "onDestroy!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 510
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onDestroy()V

    .line 511
    return-void
.end method

.method public onEnterAnimationComplete()V
    .locals 2

    .prologue
    .line 575
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onEnterAnimationComplete()V

    .line 576
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "onEnterAnimationComplete!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 531
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onPause()V

    .line 532
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->requestThreadStop()V

    .line 534
    iget v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I

    iput v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mSaveProgress:I

    .line 535
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runProgress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 538
    :cond_0
    const-string/jumbo v0, "JINSEIL"

    const-string/jumbo v1, "onPause!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 545
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onResume()V

    .line 547
    new-instance v0, Landroid/content/Intent;

    .line 548
    const-string/jumbo v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    .line 547
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 549
    .local v0, "collapsePanelIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 550
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 551
    .local v1, "service":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 553
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserTouringGuideDone()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 554
    iget v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mSaveProgress:I

    const/16 v3, 0x64

    if-eq v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z

    if-nez v2, :cond_1

    .line 555
    iget v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mSaveProgress:I

    iput v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->mProgress:I

    .line 556
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z

    .line 557
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$7;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;)V

    .line 562
    const-wide/16 v4, 0x190

    .line 557
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 569
    :cond_0
    :goto_0
    const-string/jumbo v2, "JINSEIL"

    const-string/jumbo v3, "onResume!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    return-void

    .line 563
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z

    if-nez v2, :cond_0

    .line 564
    iput-boolean v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->isAlive:Z

    .line 565
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;->runHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
