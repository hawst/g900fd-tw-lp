.class Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$2;
.super Ljava/lang/Object;
.source "SamsungAccount.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->getUserName(Ljava/lang/String;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 473
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->resultData:Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$20()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 481
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$6()Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 482
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "NOTIF doWork"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$6()Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->resultData:Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$20()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;->doNotify(Landroid/os/Bundle;)V

    .line 485
    :cond_1
    return-void

    .line 473
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 474
    .local v0, "key":Ljava/lang/String;
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->resultData:Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$20()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 475
    .local v1, "value":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 476
    const-string/jumbo v3, "[Samsung Account]"

    const-string/jumbo v4, "%s %s (%s)"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    .line 477
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 476
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
