.class Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;
.super Ljava/lang/Object;
.source "SearchMusicListActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 167
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 163
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 151
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchClearBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$7(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchIconText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$8(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchClearBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$7(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchIconText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$8(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
