.class public final enum Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;
.super Ljava/lang/Enum;
.source "EditTextForLocationSIP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LocationSIPState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

.field public static final enum SIP_CLOSE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

.field public static final enum SIP_NONE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

.field public static final enum SIP_SHOW:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    const-string/jumbo v1, "SIP_NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    const-string/jumbo v1, "SIP_SHOW"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    const-string/jumbo v1, "SIP_CLOSE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    .line 11
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_SHOW:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->SIP_CLOSE:Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/location/EditTextForLocationSIP$LocationSIPState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
