.class Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;
.super Landroid/os/Handler;
.source "DLTaskObserverService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    .line 72
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 76
    const-string/jumbo v5, "DLTaskObserverService"

    const-string/jumbo v7, "handleMessage"

    invoke-static {v5, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v3, 0x0

    .line 79
    .local v3, "pkgName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 80
    .local v4, "state":Ljava/lang/String;
    const/4 v2, 0x0

    .line 82
    .local v2, "newState":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 83
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v7

    monitor-enter v7

    .line 84
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mRunningAppMap:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$0(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Ljava/util/HashMap;

    move-result-object v5

    .line 85
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 84
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 83
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :cond_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->mTaskHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$3(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;)Landroid/os/Handler;

    move-result-object v5

    # getter for: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->DELAY_TIME:J
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$4()J

    move-result-wide v7

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 104
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 105
    return-void

    .line 85
    :cond_1
    :try_start_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 86
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    .line 87
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 88
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    # invokes: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->getAppRunningState(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v5, v3}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$1(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 90
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 91
    const-string/jumbo v5, "DLTaskObserverService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "handleMessage - "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 92
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 91
    invoke-static {v5, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v9, p0, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService$1;->this$0:Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;

    const-string/jumbo v5, "STATE_RUNNING"

    if-ne v2, v5, :cond_2

    const/4 v5, 0x1

    :goto_1
    # invokes: Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->sendTaskObserverDone(ILjava/lang/String;)V
    invoke-static {v9, v5, v3}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->access$2(Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;ILjava/lang/String;)V

    goto :goto_0

    .line 83
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_0
    move-exception v5

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    move v5, v6

    .line 94
    goto :goto_1

    .line 96
    :cond_3
    :try_start_2
    const-string/jumbo v5, "DLTaskObserverService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "handleMessage - "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 97
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 96
    invoke-static {v5, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
