.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;
.super Ljava/lang/Object;
.source "SearchAlgorithm.java"


# static fields
.field private static mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;->mInstance:Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;

    return-object v0
.end method

.method public static indexOf([C[C)I
    .locals 6
    .param p0, "haystack"    # [C
    .param p1, "needle"    # [C

    .prologue
    .line 20
    array-length v4, p1

    if-nez v4, :cond_1

    .line 21
    const/4 v1, 0x0

    .line 34
    :cond_0
    :goto_0
    return v1

    .line 23
    :cond_1
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;->makeCharTable([C)[I

    move-result-object v0

    .line 24
    .local v0, "charTable":[I
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;->makeOffsetTable([C)[I

    move-result-object v3

    .line 25
    .local v3, "offsetTable":[I
    array-length v4, p1

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_1
    array-length v4, p0

    if-lt v1, v4, :cond_2

    .line 34
    const/4 v1, -0x1

    goto :goto_0

    .line 26
    :cond_2
    array-length v4, p1

    add-int/lit8 v2, v4, -0x1

    .local v2, "j":I
    :goto_2
    aget-char v4, p1, v2

    aget-char v5, p0, v1

    if-eq v4, v5, :cond_3

    .line 32
    array-length v4, p1

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v2

    aget v4, v3, v4

    aget-char v5, p0, v1

    aget v5, v0, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_1

    .line 27
    :cond_3
    if-eqz v2, :cond_0

    .line 26
    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v2, -0x1

    goto :goto_2
.end method

.method private static isPrefix([CI)Z
    .locals 4
    .param p0, "needle"    # [C
    .param p1, "p"    # I

    .prologue
    .line 75
    move v0, p1

    .local v0, "i":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v2, p0

    if-lt v0, v2, :cond_0

    .line 80
    const/4 v2, 0x1

    :goto_1
    return v2

    .line 76
    :cond_0
    aget-char v2, p0, v0

    aget-char v3, p0, v1

    if-eq v2, v3, :cond_1

    .line 77
    const/4 v2, 0x0

    goto :goto_1

    .line 75
    :cond_1
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static makeCharTable([C)[I
    .locals 5
    .param p0, "needle"    # [C

    .prologue
    .line 41
    const/16 v0, 0x100

    .line 42
    .local v0, "ALPHABET_SIZE":I
    const/16 v3, 0x100

    new-array v2, v3, [I

    .line 43
    .local v2, "table":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-lt v1, v3, :cond_0

    .line 46
    const/4 v1, 0x0

    :goto_1
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    if-lt v1, v3, :cond_1

    .line 49
    return-object v2

    .line 44
    :cond_0
    array-length v3, p0

    aput v3, v2, v1

    .line 43
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_1
    aget-char v3, p0, v1

    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v1

    aput v4, v2, v3

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static makeOffsetTable([C)[I
    .locals 7
    .param p0, "needle"    # [C

    .prologue
    .line 56
    array-length v4, p0

    new-array v3, v4, [I

    .line 57
    .local v3, "table":[I
    array-length v1, p0

    .line 58
    .local v1, "lastPrefixPosition":I
    array-length v4, p0

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 64
    const/4 v0, 0x0

    :goto_1
    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_2

    .line 68
    return-object v3

    .line 59
    :cond_0
    add-int/lit8 v4, v0, 0x1

    invoke-static {p0, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;->isPrefix([CI)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 60
    add-int/lit8 v1, v0, 0x1

    .line 62
    :cond_1
    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    sub-int v5, v1, v0

    array-length v6, p0

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    aput v5, v3, v4

    .line 58
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 65
    :cond_2
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/SearchAlgorithm;->suffixLength([CI)I

    move-result v2

    .line 66
    .local v2, "slen":I
    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    add-int/2addr v4, v2

    aput v4, v3, v2

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static suffixLength([CI)I
    .locals 5
    .param p0, "needle"    # [C
    .param p1, "p"    # I

    .prologue
    .line 87
    const/4 v2, 0x0

    .line 88
    .local v2, "len":I
    move v0, p1

    .local v0, "i":I
    array-length v3, p0

    add-int/lit8 v1, v3, -0x1

    .line 89
    .local v1, "j":I
    :goto_0
    if-ltz v0, :cond_0

    aget-char v3, p0, v0

    aget-char v4, p0, v1

    if-eq v3, v4, :cond_1

    .line 92
    :cond_0
    return v2

    .line 90
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 89
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method
