.class public Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
.super Ljava/lang/Object;
.source "LocationPoint.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x489070e141931126L


# instance fields
.field private mLatitude:D

.field private mLongitude:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    .line 20
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLongitude:D

    .line 23
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 2
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    const-wide/16 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    .line 20
    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLongitude:D

    .line 26
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    .line 27
    iput-wide p3, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLongitude:D

    .line 28
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 48
    if-nez p1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    instance-of v2, p1, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 53
    check-cast v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;

    .line 54
    .local v0, "other":Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    iget-wide v4, v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    .line 55
    iget-wide v2, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLongitude:D

    iget-wide v4, v0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLongitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    .line 54
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLongitude:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    return v0
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "latitude"    # D

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLatitude:D

    .line 36
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "longitude"    # D

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/map/common/LocationPoint;->mLongitude:D

    .line 44
    return-void
.end method
