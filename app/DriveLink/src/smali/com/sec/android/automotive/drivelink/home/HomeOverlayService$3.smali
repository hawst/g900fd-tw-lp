.class Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;
.super Ljava/lang/Object;
.source "HomeOverlayService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationView$OnNotificationRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRemoveDirect(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Z)V
    .locals 3
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "gotoMulti"    # Z

    .prologue
    .line 465
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onRemoveDirect"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    if-nez p1, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 470
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->dismissNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$3(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)V

    goto :goto_0

    .line 472
    :cond_2
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 473
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "WARNING!! This view isn\'t showing. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 472
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$4(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 475
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 476
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 475
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->sendResetFlowId()V

    .line 478
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$4(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 479
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$5(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 480
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$6()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 481
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$6()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    move-result-object v0

    .line 482
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V

    .line 483
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$7(Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;)V

    goto :goto_0
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    .locals 3
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    .prologue
    .line 416
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Remove Notification Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    if-nez p1, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->dismissNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$3(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)V

    goto :goto_0

    .line 423
    :cond_2
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 424
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "WARNING!! This view isn\'t showing. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 423
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->WILL_DISAPPEAR:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$4(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 426
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 427
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v1

    .line 426
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->removeNotification(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->sendResetFlowId()V

    .line 429
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;->DISAPPEARED:Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->notiAlertAppearState(Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$4(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/OnNotificationChangedListener$AppearState;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->removeOverlayView(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V
    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$5(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;)V

    .line 431
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$6()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 432
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mMusicListener:Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$6()Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;

    move-result-object v0

    .line 433
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;->onPlaybackRequest(I)V

    .line 434
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$7(Lcom/sec/android/automotive/drivelink/music/OnMusicPlaybackRequestListener;)V

    goto :goto_0
.end method

.method public onRemoveRequest(Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V
    .locals 2
    .param p1, "view"    # Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
    .param p2, "command"    # Lcom/sec/android/automotive/drivelink/notification/INotiCommand;

    .prologue
    .line 442
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Remove Notification Request Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    if-nez p1, :cond_0

    .line 453
    :goto_0
    return-void

    .line 447
    :cond_0
    if-eqz p2, :cond_1

    .line 448
    invoke-static {p2}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$8(Lcom/sec/android/automotive/drivelink/notification/INotiCommand;)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->dismissNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$3(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)V

    goto :goto_0

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->dismissNotification(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$3(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)V

    goto :goto_0
.end method

.method public onSetVoiceFlowIdRequest(Ljava/lang/String;)V
    .locals 3
    .param p1, "flowId"    # Ljava/lang/String;

    .prologue
    .line 458
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "flowId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is requested!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$3;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->sendSetFlowId(Ljava/lang/String;)V

    .line 460
    return-void
.end method
