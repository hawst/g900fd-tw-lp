.class Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;
.super Ljava/util/HashMap;
.source "FlowSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 7

    .prologue
    const v6, 0x7f0a0069

    const v5, 0x7f0a0026

    const v4, 0x7f0a05eb

    const v3, 0x7f0a0211

    const/4 v2, -0x1

    .line 196
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 198
    const-string/jumbo v0, "DM_MAIN"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    const-string/jumbo v0, "DM_DIAL_CONTACT"

    .line 203
    const v1, 0x7f0a00b9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 202
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    const-string/jumbo v0, "DM_DIAL_CONTACT_SEARCH_LIST"

    .line 205
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 204
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    const-string/jumbo v0, "DM_DIAL_TYPE"

    .line 207
    const v1, 0x7f0a006b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 206
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    const-string/jumbo v0, "DM_DIAL"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    const-string/jumbo v0, "DM_SMS_CONTACT"

    .line 211
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 210
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    const-string/jumbo v0, "DM_SMS_CONTACT_SEARCH_LIST"

    .line 213
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 212
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    const-string/jumbo v0, "DM_SMS_TYPE"

    .line 215
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 214
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    const-string/jumbo v0, "DM_SMS_COMPOSE"

    .line 217
    const v1, 0x7f0a0025

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 216
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    const-string/jumbo v0, "DM_SMS_COMPOSING"

    .line 219
    const v1, 0x7f0a0186

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 218
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    const-string/jumbo v0, "DM_SMS_COMPLETE"

    .line 221
    const v1, 0x7f0a0024

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 220
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    const-string/jumbo v0, "DM_SMS_SEND"

    const v1, 0x7f0a00cd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    const-string/jumbo v0, "DM_SMS_INBOX"

    .line 224
    const v1, 0x7f0a0147

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 223
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    const-string/jumbo v0, "DM_SMS_INBOX_SEARCH_LIST"

    .line 226
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 225
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const-string/jumbo v0, "DM_SMS_READBACK"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    const-string/jumbo v0, "DM_SMS_READBACK_NOTI"

    .line 229
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 228
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    const-string/jumbo v0, "DM_LOCATION"

    .line 232
    const v1, 0x7f0a015f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 231
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE"

    .line 234
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 233
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE_SEARCH_LIST"

    .line 236
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 235
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE_TYPE"

    .line 238
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 237
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST"

    .line 241
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 240
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST"

    .line 243
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 242
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST_TYPE"

    .line 245
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 244
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    const-string/jumbo v0, "DM_LOCATION_SHARE_CONFIRM"

    .line 250
    const v1, 0x7f0a05ed

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 249
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    const-string/jumbo v0, "DM_LOCATION_REQ_CONFIRM"

    .line 252
    const v1, 0x7f0a0213

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 251
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    const-string/jumbo v0, "DM_LOCATION_ADDR_TYPE"

    .line 254
    const v1, 0x7f0a0033

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 253
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    const-string/jumbo v0, "DM_LOCATION_NAV_SEARCH_CONFIRM"

    .line 256
    const v1, 0x7f0a015c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 255
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    const-string/jumbo v0, "DM_MUSIC_PLAYER"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    const-string/jumbo v0, "DM_WHAT_CAN_I_SAY"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1
    return-void
.end method
