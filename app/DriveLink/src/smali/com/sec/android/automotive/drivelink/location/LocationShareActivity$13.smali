.class Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;
.super Ljava/lang/Object;
.source "LocationShareActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->updateContacts()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 453
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 435
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->isLoaded:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$12(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$13(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;Z)V

    .line 437
    if-nez p1, :cond_1

    .line 438
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$14(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    .line 439
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$11(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    .line 440
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getInitialPosition()I

    move-result v1

    .line 439
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 444
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$11(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    .line 445
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 443
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationDotLayout:Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$10(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;

    move-result-object v0

    .line 429
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->mRecommendationPageAdapter:Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;->access$11(Lcom/sec/android/automotive/drivelink/location/LocationShareActivity;)Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/SuggestionsLocationPageAdapter;->getPageCount()I

    move-result v1

    rem-int v1, p1, v1

    .line 428
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/component/DotIndicatorLayout;->setIndex(I)V

    .line 430
    return-void
.end method
