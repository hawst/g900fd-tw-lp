.class public Lcom/sec/android/automotive/drivelink/location/model/LocationShareHeader;
.super Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;
.source "LocationShareHeader.java"


# instance fields
.field private header:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "header"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareHeader;->header:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareHeader;->header:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;->HEADER:Lcom/sec/android/automotive/drivelink/location/model/LocationShareItem$Type;

    return-object v0
.end method
