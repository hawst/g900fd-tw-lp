.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceDataFactory;
.super Ljava/lang/Object;
.source "DriveLinkServiceDataFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createDLAlbum(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    .locals 1
    .param p0, "albumName"    # Ljava/lang/String;

    .prologue
    .line 28
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLAlbumImp;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createDLArtist(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    .locals 1
    .param p0, "artistName"    # Ljava/lang/String;

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLArtistImp;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createDLCallLog(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    .locals 8
    .param p0, "phoneNumber"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "callType"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;
    .param p3, "logType"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    .prologue
    .line 33
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;

    const-string/jumbo v5, ""

    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    move-object v4, p0

    .line 33
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0
.end method

.method public static createDLMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;
    .locals 1
    .param p0, "phoneNumber"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 16
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/message/DLMessageImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createDLMusic(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .locals 1
    .param p0, "musicName"    # Ljava/lang/String;

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/music/DLMusicImp;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
