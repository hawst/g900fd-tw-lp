.class public Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseInOut;
.super Ljava/lang/Object;
.source "BackEaseInOut.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseInOut;->s:F

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseInOut;->s:F

    .line 15
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 7
    .param p1, "input"    # F

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const v5, 0x3fc33333    # 1.525f

    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f000000    # 0.5f

    .line 25
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/BackEaseInOut;->s:F

    .line 26
    .local v0, "s2":F
    div-float/2addr p1, v3

    cmpg-float v1, p1, v2

    if-gez v1, :cond_0

    .line 27
    mul-float v1, p1, p1

    mul-float/2addr v0, v5

    add-float/2addr v2, v0

    mul-float/2addr v2, p1

    sub-float/2addr v2, v0

    mul-float/2addr v1, v2

    mul-float/2addr v1, v3

    add-float/2addr v1, v4

    .line 30
    :goto_0
    return v1

    :cond_0
    sub-float/2addr p1, v6

    mul-float v1, p1, p1

    .line 31
    mul-float/2addr v0, v5

    add-float/2addr v2, v0

    mul-float/2addr v2, p1

    add-float/2addr v2, v0

    mul-float/2addr v1, v2

    add-float/2addr v1, v6

    .line 30
    mul-float/2addr v1, v3

    add-float/2addr v1, v4

    goto :goto_0
.end method
