.class public Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;
.super Ljava/lang/Object;
.source "MessageVoiceInput.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;
    }
.end annotation


# static fields
.field private static mContext:Landroid/content/Context;

.field private static mInstance:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;


# instance fields
.field public mComposeInput:Ljava/lang/String;

.field mComposerTextInputListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

.field mContactSearchListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

.field public mVoiceFieldID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mInstance:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    .line 15
    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mComposeInput:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mVoiceFieldID:Ljava/lang/String;

    .line 23
    sput-object p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mInstance:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    .line 24
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mComposeInput:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mVoiceFieldID:Ljava/lang/String;

    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->setContext(Landroid/content/Context;)V

    .line 28
    sput-object p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mInstance:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;
    .locals 2

    .prologue
    .line 37
    const-class v1, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    monitor-enter v1

    .line 38
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mInstance:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mInstance:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    .line 37
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    sget-object v0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mInstance:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public registerContactSearchListener(Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mContactSearchListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .line 89
    return-void
.end method

.method public registerVoiceInputListener(Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mComposerTextInputListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    .line 84
    return-void
.end method

.method public setContactChoice(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "displayedChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mContactSearchListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mContactSearchListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;->OnContactChoice(Ljava/util/List;)V

    .line 80
    :cond_0
    return-void
.end method

.method public setContactTypeInfo(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/util/Map;)V
    .locals 1
    .param p1, "ContactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p2, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mContactSearchListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mContactSearchListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    invoke-interface {v0, p1}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;->OnContactTypeInfo(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 68
    :cond_0
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    sput-object p1, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method public setMessageComposeInput(Ljava/lang/String;)V
    .locals 2
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mComposeInput:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mComposerTextInputListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mComposerTextInputListener:Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mComposeInput:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput$OnComposerTextInputListener;->OnInputTextUpdated(Ljava/lang/String;)V

    .line 75
    :cond_0
    return-void
.end method

.method public setVoiceFieldIDForMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldID"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageVoiceInput;->mVoiceFieldID:Ljava/lang/String;

    .line 47
    return-void
.end method
