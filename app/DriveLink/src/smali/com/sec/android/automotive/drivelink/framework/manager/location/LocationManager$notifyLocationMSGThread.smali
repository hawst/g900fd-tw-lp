.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;
.super Ljava/lang/Thread;
.source "LocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "notifyLocationMSGThread"
.end annotation


# instance fields
.field private msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;)V
    .locals 1
    .param p2, "msgInfo"    # Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    .line 232
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 233
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 234
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 237
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 238
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 237
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getUrlFromLocationMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, "url":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 244
    const-string/jumbo v2, "LocationManager"

    const-string/jumbo v3, "Missing url on location message!"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)Landroid/content/Context;

    move-result-object v3

    .line 249
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 248
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getContactFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 250
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->setName(Ljava/lang/String;)V

    .line 252
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    .line 253
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->isLocationRequestNotification(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 254
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v2

    .line 255
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 254
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyLocationRequest(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v2

    .line 257
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->isLocationSharedNotification(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->getMediator()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;)Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getNotificationManager()Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;

    move-result-object v2

    .line 259
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager$notifyLocationMSGThread;->msgInfo:Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;

    .line 258
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/notification/NotificationManager;->NotifyLocationShare(Lcom/sec/android/automotive/drivelink/framework/manager/notification/broadcast/DLMSGInfoImp;Ljava/lang/String;)V

    goto :goto_0
.end method
