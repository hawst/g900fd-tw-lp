.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationWaitingRequestDialogView.java"


# static fields
.field private static final TIME_OUT:I = 0x7530

.field private static dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mImageView:Landroid/widget/ImageView;

.field protected mViewLocal:Landroid/view/View;

.field private tvNotification:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mContext:Landroid/content/Context;

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mContext:Landroid/content/Context;

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mContext:Landroid/content/Context;

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 24
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v8, 0x7f02023b

    .line 50
    if-nez p1, :cond_0

    .line 105
    :goto_0
    return-void

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mContext:Landroid/content/Context;

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 57
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 56
    check-cast v0, Landroid/view/LayoutInflater;

    .line 59
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mViewLocal:Landroid/view/View;

    if-nez v3, :cond_2

    .line 61
    const v3, 0x7f0300b7

    .line 60
    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 63
    const v3, 0x7f0902eb

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 64
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 65
    const v4, 0x7f0a03a2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 66
    sget-object v7, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 64
    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mViewLocal:Landroid/view/View;

    const v4, 0x7f0902cd

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 72
    const v4, 0x7f020238

    .line 70
    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 74
    .local v2, "shareBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 73
    invoke-static {v3, v2, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 76
    .local v1, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_1

    .line 77
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 78
    const v4, 0x7f02023d

    .line 77
    invoke-static {v3, v4, v8}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 81
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 82
    const v4, 0x7f02023c

    .line 81
    invoke-static {v3, v1, v4}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 84
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 87
    .end local v1    # "maskedBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "shareBitmap":Landroid/graphics/Bitmap;
    :cond_2
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView$1;

    invoke-direct {v4, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView$1;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;)V

    .line 97
    const-wide/16 v5, 0x7530

    .line 87
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 99
    new-instance v3, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView$2;-><init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public static setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0
    .param p0, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 108
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationWaitingRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 109
    return-void
.end method
