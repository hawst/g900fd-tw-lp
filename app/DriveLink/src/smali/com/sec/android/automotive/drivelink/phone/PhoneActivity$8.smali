.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->initSearchTextField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 561
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "mSearchBtn getDrivingStatus() : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 562
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 561
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 565
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 566
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 565
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 567
    const-string/jumbo v2, "CM06"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 569
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$19(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 571
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 572
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    .line 573
    invoke-virtual {v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 574
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$15(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->hideVoiceLayout()V

    .line 576
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$18(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 577
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 578
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 579
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 580
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->clearFocus()V

    .line 581
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 582
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 583
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 584
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v0, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 586
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$20(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 587
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const-string/jumbo v3, ""

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$21(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 588
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v7}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$22(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 590
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$11(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 592
    const-string/jumbo v2, "PhoneActivity"

    const-string/jumbo v3, "mSearchBtn - setAutoShrink : true"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 604
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 596
    :cond_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 598
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 599
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 601
    const v3, 0x7f0a0199

    .line 600
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 602
    .local v1, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method
