.class Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;
.super Landroid/content/BroadcastReceiver;
.source "MessageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    .line 54
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;->getResultCode()I

    move-result v0

    .line 59
    .local v0, "resultCode":I
    packed-switch v0, :pswitch_data_0

    .line 85
    :goto_0
    :pswitch_0
    return-void

    .line 61
    :pswitch_1
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "onReceive Activity.RESULT_OK"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendRequestResult(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;Z)V

    goto :goto_0

    .line 66
    :pswitch_2
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "onReceive RESULT_ERROR_GENERIC_FAILURE"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendRequestResult(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;Z)V

    goto :goto_0

    .line 71
    :pswitch_3
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "onReceive RESULT_ERROR_NO_SERVICE"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendRequestResult(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;Z)V

    goto :goto_0

    .line 76
    :pswitch_4
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "onReceive RESULT_ERROR_NULL_PDU"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendRequestResult(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;Z)V

    goto :goto_0

    .line 81
    :pswitch_5
    const-string/jumbo v1, "MessageManager"

    const-string/jumbo v2, "onReceive RESULT_ERROR_RADIO_OFF"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager$1;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;

    # invokes: Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->sendRequestResult(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;->access$0(Lcom/sec/android/automotive/drivelink/framework/manager/message/MessageManager;Z)V

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
