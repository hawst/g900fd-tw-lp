.class public Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
.source "DLLocationPlaceOn.java"


# static fields
.field private static final serialVersionUID:J = 0x3d25884b8ff92850L


# instance fields
.field private mDate:Ljava/lang/String;

.field private mLatitude:D

.field private mLocationID:Ljava/lang/String;

.field private mLongitude:D


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "object"    # Lorg/json/JSONObject;

    .prologue
    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    .line 19
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;-><init>()V

    .line 14
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLocationID:Ljava/lang/String;

    .line 15
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLatitude:D

    .line 16
    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLongitude:D

    .line 17
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mDate:Ljava/lang/String;

    .line 21
    :try_start_0
    const-string/jumbo v1, "locationId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLocationID:Ljava/lang/String;

    .line 22
    const-string/jumbo v1, "date"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->setDate(Ljava/lang/String;)V

    .line 23
    const-string/jumbo v1, "latitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLatitude:D

    .line 24
    const-string/jumbo v1, "longitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLongitude:D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    :goto_0
    return-void

    .line 25
    :catch_0
    move-exception v0

    .line 26
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mDate:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLatitude:D

    return-wide v0
.end method

.method public getLocationAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLocationID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLocationID:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLocationType()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    return-object v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLongitude:D

    return-wide v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 77
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public isSamePosition(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)Z
    .locals 1
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public isValidAddres()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public isValidCoords()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "mDate"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mDate:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "lat"    # D

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLatitude:D

    .line 46
    return-void
.end method

.method public setLocationAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 107
    return-void
.end method

.method public setLocationID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLocationID:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setLocationName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 101
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "lng"    # D

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/location/service/placeon/data/DLLocationPlaceOn;->mLongitude:D

    .line 55
    return-void
.end method
