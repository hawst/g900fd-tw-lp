.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/common/DLServiceManager$OnUpdateSuggestionListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 1247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateSuggestionList(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v1, 0x0

    .line 1251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v0, p1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$0(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/util/ArrayList;)V

    .line 1253
    const-string/jumbo v0, "PhoneActivity"

    const-string/jumbo v2, "suggestionListener!"

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1254
    const-string/jumbo v2, "PhoneActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "contact size : "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1255
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1254
    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1256
    const-string/jumbo v0, "PhoneActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "contact size : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1257
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$1(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1256
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1259
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1260
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->showSuggestionList()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$3(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    .line 1263
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v1

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setSuggetionListFlowParam(I)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$5(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 1265
    return-void

    .line 1255
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 1257
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mContactList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$1(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_1
.end method
