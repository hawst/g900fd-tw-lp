.class public Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;
.super Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;
.source "SCOMusicPlayerManager.java"


# static fields
.field private static final ARG_PLAYER_RESUME:I = 0x2

.field private static final ARG_PLAYER_START:I = 0x1

.field private static final MAX_SCO_CHECK_TRY_COUNT:I = 0xf

.field private static final MSG_SCO_OPEN_CHECK_START:I = 0x1

.field private static final SCO_OPEN_CHECK_INTERVAL_MS:I = 0xc8


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mBTSCOCheckHandler:Landroid/os/Handler;

.field private mSCOCheckTryCount:I

.field private mbTryingToPlay:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;-><init>()V

    .line 15
    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mSCOCheckTryCount:I

    .line 17
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mbTryingToPlay:Z

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mAudioManager:Landroid/media/AudioManager;

    .line 22
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mBTSCOCheckHandler:Landroid/os/Handler;

    .line 8
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mSCOCheckTryCount:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mBTSCOCheckHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->startPlayerWithPlayType(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;I)V
    .locals 0

    .prologue
    .line 15
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mSCOCheckTryCount:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;Z)V
    .locals 0

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mbTryingToPlay:Z

    return-void
.end method

.method private startBTSCOCheck(II)V
    .locals 2
    .param p1, "checkType"    # I
    .param p2, "playType"    # I

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 138
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 139
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 140
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 142
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mBTSCOCheckHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 144
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mSCOCheckTryCount:I

    .line 145
    return-void
.end method

.method private startPlayerWithPlayType(I)V
    .locals 1
    .param p1, "playType"    # I

    .prologue
    const/4 v0, 0x1

    .line 55
    if-ne p1, v0, :cond_1

    .line 56
    invoke-super {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 58
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->resumePlayer()V

    goto :goto_0
.end method

.method private stopBTSCOCheck()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mBTSCOCheckHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 150
    iput v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mSCOCheckTryCount:I

    .line 151
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mbTryingToPlay:Z

    .line 152
    return-void
.end method


# virtual methods
.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->initialize(Landroid/content/Context;)Z

    .line 67
    const-string/jumbo v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 66
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mAudioManager:Landroid/media/AudioManager;

    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public isPlayerPlaying()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mbTryingToPlay:Z

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->isPlayerPlaying()Z

    move-result v0

    goto :goto_0
.end method

.method public pausePlayer()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 115
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->pausePlayer(Z)V

    .line 116
    return-void
.end method

.method public playNextMusic(Z)Z
    .locals 1
    .param p1, "bPlayNow"    # Z

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 92
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playNextMusic(Z)Z

    move-result v0

    return v0
.end method

.method public playPrevMusic(Z)Z
    .locals 1
    .param p1, "bPlayNow"    # Z

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->playPrevMusic(Z)Z

    move-result v0

    return v0
.end method

.method public resumePlayer()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 119
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 121
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    const/4 v0, 0x2

    invoke-direct {p0, v1, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->startBTSCOCheck(II)V

    .line 123
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mbTryingToPlay:Z

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->resumePlayer()V

    goto :goto_0
.end method

.method public startPlayer(Z)V
    .locals 2
    .param p1, "bPlayNow"    # Z

    .prologue
    const/4 v1, 0x1

    .line 102
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 104
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-direct {p0, v1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->startBTSCOCheck(II)V

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mbTryingToPlay:Z

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->startPlayer(Z)V

    goto :goto_0
.end method

.method public stopPlayer()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 132
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->stopPlayer()V

    .line 133
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/MusicPlayerManager;->terminate(Landroid/content/Context;)V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->stopBTSCOCheck()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/musicplayer/SCOMusicPlayerManager;->mAudioManager:Landroid/media/AudioManager;

    .line 79
    return-void
.end method
