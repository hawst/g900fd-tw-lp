.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;
.source "LocationDataHandler.java"


# static fields
.field static final ATTENDEES_URI:Ljava/lang/String; = "content://com.android.calendar/attendees"

.field static final CALENDAR_URI:Ljava/lang/String; = "content://com.android.canlendar/calendars"

.field static final EVENT_URI:Ljava/lang/String; = "content://com.android.calendar/events"

.field public static final LOCATION_CONTENT_GROUP:Ljava/lang/String; = "!Join group location sharing."

.field public static final LOCATION_CONTENT_REMOVE_GROUP:Ljava/lang/String; = "!Stop group location sharing."

.field public static final LOCATION_CONTENT_REQUEST:Ljava/lang/String; = "!Let me know your location."

.field public static final LOCATION_CONTENT_SHARE:Ljava/lang/String; = "!I\'m here now. See me here!"

.field public static final LOCATION_GROUP_CHANGED_DESTINATION:Ljava/lang/String; = "!Let\'s meet here."

.field public static final LOCATION_TYPE_REQUEST:I = 0x1

.field public static final LOCATION_TYPE_SHARE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "LocationDataHandler"


# instance fields
.field private mUserProfile:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentBaseHandler;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->mUserProfile:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .line 43
    return-void
.end method

.method public static createGroupDeletedMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 75
    const-string/jumbo v0, "!Stop group location sharing."

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createLocationMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createGroupDestinationChangedMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 71
    const-string/jumbo v0, "!Let\'s meet here."

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createLocationMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createGroupRestartMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createGroupShareMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createGroupShareMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string/jumbo v0, "!Join group location sharing."

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createLocationMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createLocationMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 91
    if-nez p1, :cond_0

    .line 93
    .end local p0    # "msg":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "msg":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static createLocationShareMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string/jumbo v0, "!I\'m here now. See me here!"

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createLocationMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createRequestLocationMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 59
    const-string/jumbo v0, "!Let me know your location."

    invoke-static {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->createLocationMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getEventProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 263
    const/16 v1, 0xb

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 264
    const-string/jumbo v2, "calendar_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 265
    const-string/jumbo v2, "dtstart"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "dtend"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 266
    const-string/jumbo v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 267
    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 268
    const-string/jumbo v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 269
    const-string/jumbo v2, "eventLocation"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 270
    const-string/jumbo v2, "allDay"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "longitude"

    aput-object v2, v0, v1

    .line 271
    .local v0, "eventProjection":[Ljava/lang/String;
    return-object v0
.end method

.method public static getMyPlaceList(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    const/4 v1, 0x0

    .line 298
    .local v1, "provider":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
    const/4 v0, 0x0

    .line 300
    .local v0, "placeList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationMyPlace;>;"
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->isKDevice(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 301
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;

    .end local v1    # "provider":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;-><init>()V

    .line 306
    .restart local v1    # "provider":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
    :goto_0
    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;->getMyPlaces(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 308
    return-object v0

    .line 303
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;

    .end local v1    # "provider":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/GeneralDeviceMyPlaceProvider;-><init>()V

    .restart local v1    # "provider":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
    goto :goto_0
.end method

.method private getScheduleEventFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventCursor"    # Landroid/database/Cursor;

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getScheduleFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;

    move-result-object v1

    .line 175
    .local v1, "schedule":Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;-><init>()V

    .line 177
    .local v0, "locationEvent":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->setLocation(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 178
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->setSchedule(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;)V

    .line 179
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->setEventTitle(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;->setPlaceName(Ljava/lang/String;)V

    .line 182
    return-object v0
.end method

.method private getScheduleFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventCursor"    # Landroid/database/Cursor;

    .prologue
    .line 188
    new-instance v14, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;

    invoke-direct {v14}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;-><init>()V

    .line 189
    .local v14, "schedule":Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;
    new-instance v11, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 193
    .local v11, "location":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;
    const-string/jumbo v16, "_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 192
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 195
    .local v8, "eventId":I
    const-string/jumbo v16, "calendar_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 194
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 197
    .local v2, "calendarId":I
    const-string/jumbo v16, "dtstart"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 196
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 199
    .local v6, "dtstart":J
    const-string/jumbo v16, "dtend"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 198
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 201
    .local v4, "dtend":J
    const-string/jumbo v16, "description"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 200
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 203
    .local v3, "desc":Ljava/lang/String;
    const-string/jumbo v16, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 202
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 205
    .local v15, "title":Ljava/lang/String;
    const-string/jumbo v16, "eventLocation"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 204
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 206
    .local v12, "locationName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 207
    .local v10, "lat":I
    const/4 v13, 0x0

    .line 209
    .local v13, "lon":I
    sget-object v16, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;->SCHEDULE:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationType(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationType;)V

    .line 210
    invoke-virtual {v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationName(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v11, v12}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationAddress(Ljava/lang/String;)V

    .line 213
    const-string/jumbo v16, "latitude"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 214
    .local v9, "i":I
    if-lez v9, :cond_0

    .line 215
    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 218
    :cond_0
    const-string/jumbo v16, "longitude"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 219
    if-lez v9, :cond_1

    .line 220
    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 224
    :cond_1
    if-eq v10, v13, :cond_2

    .line 225
    invoke-static {v13}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->microDegreesToDegrees(I)D

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v11, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 226
    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->microDegreesToDegrees(I)D

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v11, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 229
    :cond_2
    invoke-virtual {v14, v8}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->setEventId(I)V

    .line 230
    invoke-virtual {v14, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->setCalendarId(I)V

    .line 231
    new-instance v16, Ljava/util/Date;

    move-object/from16 v0, v16

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->setStartDate(Ljava/util/Date;)V

    .line 232
    new-instance v16, Ljava/util/Date;

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->setEndDate(Ljava/util/Date;)V

    .line 233
    invoke-virtual {v14, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->setDescription(Ljava/lang/String;)V

    .line 234
    invoke-virtual {v14, v15}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->setTitle(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v14, v11}, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->setLocation(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;)V

    .line 259
    return-object v14
.end method

.method public static getUrlFromLocationMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 80
    if-nez p0, :cond_0

    .line 81
    const/4 v1, 0x0

    .line 87
    :goto_0
    return-object v1

    .line 83
    :cond_0
    const-string/jumbo v1, "http"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 84
    .local v0, "i":I
    if-gez v0, :cond_1

    .line 85
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static isKDevice(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 313
    const/4 v0, 0x1

    return v0
.end method

.method public static microDegreesToDegrees(I)D
    .locals 4
    .param p0, "microDegrees"    # I

    .prologue
    .line 293
    int-to-double v0, p0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private queryScheduleEvents(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Landroid/database/Cursor;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;

    .prologue
    .line 147
    const-string/jumbo v3, ""

    .line 148
    .local v3, "eventSelection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 149
    .local v6, "eventCursor":Landroid/database/Cursor;
    const-string/jumbo v0, "content://com.android.calendar/events"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 151
    .local v1, "event":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getEventProjection()[Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "eventProjection":[Ljava/lang/String;
    const-string/jumbo v3, "eventLocation NOT NULL AND deleted=0 AND eventLocation!=\"\""

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " AND (dtstart <= "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 162
    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, "dtend"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 163
    const-string/jumbo v4, " >= "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 165
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 166
    const/4 v4, 0x0

    .line 167
    const-string/jumbo v5, "dtstart asc"

    .line 165
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 169
    return-object v6
.end method


# virtual methods
.method public getScheduleEventsWithLocation(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "toDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v2, "eventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->queryScheduleEvents(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)Landroid/database/Cursor;

    move-result-object v1

    .line 119
    .local v1, "eventCursor":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 120
    const-string/jumbo v4, "LocationDataHandler"

    const-string/jumbo v5, "Fail to query schedule events."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v1    # "eventCursor":Landroid/database/Cursor;
    :goto_0
    return-object v2

    .line 124
    .restart local v1    # "eventCursor":Landroid/database/Cursor;
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_1

    .line 125
    const-string/jumbo v4, "LocationDataHandler"

    const-string/jumbo v5, "No schedules found."

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 138
    .end local v1    # "eventCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "eventCursor":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->getScheduleEventFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;

    move-result-object v3

    .line 133
    .local v3, "scheduleEvent":Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationEvent;
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 136
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getUserProfile()Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->mUserProfile:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public isLocationGroupShareChangedDestination(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 109
    const-string/jumbo v0, "!Let\'s meet here."

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isLocationGroupSharedNotification(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 105
    const-string/jumbo v0, "!Join group location sharing."

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isLocationRequestNotification(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 101
    const-string/jumbo v0, "!Let me know your location."

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isLocationSharedNotification(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 97
    const-string/jumbo v0, "!I\'m here now. See me here!"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isLocationStopGroupSharing(Ljava/lang/String;)Z
    .locals 1
    .param p1, "msgBody"    # Ljava/lang/String;

    .prologue
    .line 283
    const-string/jumbo v0, "!Stop group location sharing."

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public setUserProfile(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;)V
    .locals 0
    .param p1, "profile"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/LocationDataHandler;->mUserProfile:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONProfile;

    .line 280
    return-void
.end method

.method public terminate(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    return-void
.end method
