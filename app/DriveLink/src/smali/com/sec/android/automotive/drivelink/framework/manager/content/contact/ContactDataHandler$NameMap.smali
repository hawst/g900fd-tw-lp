.class Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;
.super Ljava/lang/Object;
.source "ContactDataHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NameMap"
.end annotation


# instance fields
.field familyName:Ljava/lang/String;

.field givenName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "fName"    # Ljava/lang/String;
    .param p2, "gName"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->familyName:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->givenName:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method getFamilyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->familyName:Ljava/lang/String;

    return-object v0
.end method

.method getGivenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->givenName:Ljava/lang/String;

    return-object v0
.end method

.method setFamilyName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fName"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->familyName:Ljava/lang/String;

    .line 72
    return-void
.end method

.method setGivenName(Ljava/lang/String;)V
    .locals 0
    .param p1, "gName"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/ContactDataHandler$NameMap;->givenName:Ljava/lang/String;

    .line 76
    return-void
.end method
