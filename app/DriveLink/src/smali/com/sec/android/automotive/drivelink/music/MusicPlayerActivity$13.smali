.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setTouchPlayingTime(I)V
    invoke-static {v0, p2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$14(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;I)V

    .line 856
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v1, 0x0

    .line 848
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 849
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$15(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 850
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 837
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 838
    .local v0, "valueToSeek":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 839
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->seekToPlayer(I)V

    .line 840
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 842
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$13;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v2

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setProgressStopWithStarted(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$15(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 844
    :cond_0
    return-void
.end method
