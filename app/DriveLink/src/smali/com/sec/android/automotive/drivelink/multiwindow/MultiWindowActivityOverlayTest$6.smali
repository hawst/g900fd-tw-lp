.class Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$6;
.super Ljava/lang/Object;
.source "MultiWindowActivityOverlayTest.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$6;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    .line 979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 3
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 984
    if-nez p1, :cond_0

    .line 985
    const-string/jumbo v0, "[MultiWindowActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "StateChangeListener: isMultiWindow?"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 986
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Multi Window will be finished."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 985
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest$6;->this$0:Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivityOverlayTest;->finish()V

    .line 990
    :cond_0
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "arg0"    # Landroid/graphics/Rect;

    .prologue
    .line 996
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 1002
    return-void
.end method
