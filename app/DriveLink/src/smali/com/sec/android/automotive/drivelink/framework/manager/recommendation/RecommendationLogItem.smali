.class public Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;
.super Ljava/lang/Object;
.source "RecommendationLogItem.java"


# instance fields
.field private categoryName:Ljava/lang/String;

.field private value:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "categoryName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->categoryName:Ljava/lang/String;

    .line 10
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->value:I

    .line 11
    return-void
.end method


# virtual methods
.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->value:I

    return v0
.end method

.method public setCategoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "categoryName"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->categoryName:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public setValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/RecommendationLogItem;->value:I

    .line 27
    return-void
.end method
