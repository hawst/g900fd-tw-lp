.class Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$1;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->showSetupWizardDialogExit(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$activity:Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$1;->val$activity:Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;

    .line 686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$1;->val$activity:Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->finish()V

    .line 691
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    .line 692
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils$1;->val$activity:Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 691
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->terminate(Landroid/content/Context;)V

    .line 693
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->setWhenOnDestoryed()V

    .line 694
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 695
    return-void
.end method
