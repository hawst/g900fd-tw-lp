.class Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;
.super Ljava/lang/Object;
.source "NotificationItem.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    .line 989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 994
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "onRequestCancelled!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 996
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 997
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "call nextCmdHandler <= onRequestDidPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 998
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1000
    :cond_0
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1005
    const-string/jumbo v0, "[NotificationItem]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "<<<TTS Play End: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1006
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1007
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1008
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "call nextCmdHandler <= onRequestDidPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1011
    :cond_0
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 1016
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "onRequestIgnored!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->mTTSPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1018
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1019
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, "call nextCmdHandler <= onRequestDidPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$6;->this$0:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->nextCmdHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1022
    :cond_0
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1027
    const-string/jumbo v0, "[NotificationItem]"

    const-string/jumbo v1, ">>>TTS Play Start: onRequestWillPlay"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    return-void
.end method
