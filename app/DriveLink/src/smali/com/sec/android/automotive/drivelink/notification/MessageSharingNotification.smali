.class public Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;
.super Ljava/lang/Object;
.source "MessageSharingNotification.java"


# static fields
.field private static volatile _instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;


# instance fields
.field mListener:Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;
    .locals 2

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    if-nez v0, :cond_1

    .line 14
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    .line 14
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    return-object v0

    .line 14
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getListener()Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    if-nez v0, :cond_1

    .line 26
    const-class v1, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    monitor-enter v1

    .line 27
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->_instance:Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->mListener:Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public registerListener(Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->mListener:Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;

    .line 38
    return-void
.end method

.method public unregisterListener()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->mListener:Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;

    .line 42
    return-void
.end method
