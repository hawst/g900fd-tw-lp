.class public Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;
.super Ljava/lang/Object;
.source "DLRecommendedLocationItem.java"


# instance fields
.field private mLocationID:Ljava/lang/String;

.field private mWeight:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "locationID"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mLocationID:Ljava/lang/String;

    .line 5
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mWeight:I

    .line 8
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mLocationID:Ljava/lang/String;

    .line 9
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mWeight:I

    .line 10
    return-void
.end method


# virtual methods
.method public addWeight(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mWeight:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mWeight:I

    .line 22
    return-void
.end method

.method public getLocationID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mLocationID:Ljava/lang/String;

    return-object v0
.end method

.method public getWeight()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/recommendation/DLRecommendedLocationItem;->mWeight:I

    return v0
.end method
