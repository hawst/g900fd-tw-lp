.class public abstract Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
.super Ljava/lang/Object;
.source "NavigationMap.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[NavigationMap]"


# instance fields
.field private address:Ljava/lang/String;

.field private needOrigin:Z

.field private origin:Lcom/google/android/gms/maps/model/LatLng;

.field private point:Lcom/google/android/gms/maps/model/LatLng;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->needOrigin:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->point:Lcom/google/android/gms/maps/model/LatLng;

    .line 47
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->origin:Lcom/google/android/gms/maps/model/LatLng;

    .line 48
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->address:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static createInstance(I)Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    .locals 2
    .param p0, "mapID"    # I

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 69
    .local v0, "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_TMAP:I

    if-ne v1, p0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;

    .end local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/TMapNavigationAdapter;-><init>()V

    .line 82
    .restart local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    :goto_0
    return-object v0

    .line 71
    :cond_0
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-ne v1, p0, :cond_1

    .line 72
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;

    .end local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/BaiduNavigationAdapter;-><init>()V

    .line 73
    .restart local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    goto :goto_0

    :cond_1
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_OLLEH_NAVI:I

    if-ne v1, p0, :cond_2

    .line 74
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;

    .end local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/OllehNavigationAdapter;-><init>()V

    .line 75
    .restart local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    goto :goto_0

    :cond_2
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_UNAVI:I

    if-ne v1, p0, :cond_3

    .line 76
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;

    .end local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/UplusNavigationAdapter;-><init>()V

    .line 77
    .restart local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    goto :goto_0

    :cond_3
    sget v1, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-ne v1, p0, :cond_4

    .line 78
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;

    .end local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/CMCCNavigationAdapter;-><init>()V

    .line 79
    .restart local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    goto :goto_0

    .line 80
    :cond_4
    new-instance v0, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;

    .end local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/location/map/navi/adapters/GoogleMapNavigationAdapter;-><init>()V

    .restart local v0    # "nav":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    goto :goto_0
.end method

.method public static degreesToMicroDegrees(D)I
    .locals 2
    .param p0, "degrees"    # D

    .prologue
    .line 286
    const-wide v0, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, p0

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public exitNavigation(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 290
    const-string/jumbo v0, "[NavigationMap]"

    const-string/jumbo v1, "exitNavigation"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    return-void
.end method

.method public getDestAddress(DD)Ljava/lang/String;
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getDestPoint()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->point:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public getInstallNavigationApp(I)Landroid/content/Intent;
    .locals 6
    .param p1, "mapID"    # I

    .prologue
    .line 155
    const-string/jumbo v3, "market://details?id="

    .line 156
    .local v3, "prefixMarket":Ljava/lang/String;
    const-string/jumbo v0, "ozstore.external.linked"

    .line 157
    .local v0, "UPLUS_INTENT":Ljava/lang/String;
    const-string/jumbo v1, "ozstore://STORE/PID=Q06010475433"

    .line 159
    .local v1, "UPLUS_MARKET":Ljava/lang/String;
    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_TMAP:I

    if-ne v4, p1, :cond_0

    .line 160
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 161
    const-string/jumbo v5, "http://m.tstore.co.kr/userpoc/mp.jsp?pid=0000163382"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 160
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 180
    .local v2, "goToMarket":Landroid/content/Intent;
    :goto_0
    const/high16 v4, 0x10200000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 183
    return-object v2

    .line 162
    .end local v2    # "goToMarket":Landroid/content/Intent;
    :cond_0
    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_BAIDU_NAVI:I

    if-ne v4, p1, :cond_1

    .line 163
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    const-string/jumbo v5, "market://details?id=com.baidu.BaiduMap"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 163
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 165
    .restart local v2    # "goToMarket":Landroid/content/Intent;
    goto :goto_0

    .end local v2    # "goToMarket":Landroid/content/Intent;
    :cond_1
    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_OLLEH_NAVI:I

    if-ne v4, p1, :cond_2

    .line 166
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    const-string/jumbo v5, "market://details?id=kt.navi"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 166
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 168
    .restart local v2    # "goToMarket":Landroid/content/Intent;
    goto :goto_0

    .end local v2    # "goToMarket":Landroid/content/Intent;
    :cond_2
    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_UNAVI:I

    if-ne v4, p1, :cond_3

    .line 169
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "ozstore.external.linked"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 170
    const-string/jumbo v5, "ozstore://STORE/PID=Q06010475433"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 169
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 173
    .restart local v2    # "goToMarket":Landroid/content/Intent;
    goto :goto_0

    .end local v2    # "goToMarket":Landroid/content/Intent;
    :cond_3
    sget v4, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_CMCC_NAVI:I

    if-ne v4, p1, :cond_4

    .line 174
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 175
    const-string/jumbo v5, "market://details?id=com.autonavi.cmccmap"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 174
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 176
    .restart local v2    # "goToMarket":Landroid/content/Intent;
    goto :goto_0

    .line 177
    .end local v2    # "goToMarket":Landroid/content/Intent;
    :cond_4
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    const-string/jumbo v5, "market://details?id=com.google.android.apps.maps"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 177
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .restart local v2    # "goToMarket":Landroid/content/Intent;
    goto :goto_0
.end method

.method public abstract getNaviPackageName()Ljava/lang/String;
.end method

.method public getNavigationIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public getOrigin()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->origin:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 188
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 190
    .local v0, "app_installed":Z
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v2, p2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    .line 192
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOriginNeeded()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->needOrigin:Z

    return v0
.end method

.method public setDestination(DD)V
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 99
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->point:Lcom/google/android/gms/maps/model/LatLng;

    .line 100
    return-void
.end method

.method public setDestination(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->address:Ljava/lang/String;

    .line 109
    return-void
.end method

.method protected setMultiWindowIntentToNavi(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .param p1, "navigationIntent"    # Landroid/content/Intent;

    .prologue
    .line 205
    const-string/jumbo v1, "MULTI_WINDOW_ENABLE"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    const-string/jumbo v1, "[NavigationMap]"

    const-string/jumbo v2, "setMultiWindowIntentToNavi!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    sget v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_B:I

    const/4 v2, 0x0

    .line 207
    invoke-static {p1, v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    move-result-object v0

    .line 215
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    return-object v0

    .line 211
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    move-object v0, p1

    .line 212
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "[NavigationMap]"

    .line 213
    const-string/jumbo v2, "setMultiWindowIntentToNavi -> Multi Window Feature is disabled!!"

    .line 212
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setNeedOrigin(Z)V
    .locals 0
    .param p1, "needOrigin"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->needOrigin:Z

    .line 57
    return-void
.end method

.method public setOrigin(DD)V
    .locals 1
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->origin:Lcom/google/android/gms/maps/model/LatLng;

    .line 147
    return-void
.end method

.method public startMap(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 277
    return-void
.end method

.method public startMultiNavi(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pacel"    # Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    .prologue
    .line 140
    const/4 v0, 0x1

    .line 142
    .local v0, "ret":Z
    return v0
.end method

.method protected startMultiWindowWithIndicator(Landroid/content/Context;ZLjava/lang/String;Landroid/os/Parcelable;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bMulti"    # Z
    .param p3, "tts"    # Ljava/lang/String;
    .param p4, "multiData"    # Landroid/os/Parcelable;

    .prologue
    .line 221
    const-string/jumbo v2, "MULTI_WINDOW_ENABLE"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 222
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 227
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 230
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 231
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_TTS"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    :cond_0
    if-eqz p4, :cond_2

    .line 235
    const-class v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    invoke-virtual {v2, p4}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object v2, p4

    .line 237
    check-cast v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    if-nez v2, :cond_5

    .line 238
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.multiwindow.ACTION_START_MAP"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    :cond_1
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 249
    .local v1, "naviBundle":Landroid/os/Bundle;
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI_DATA"

    .line 248
    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 251
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.multiwindow.ACTION_EXTRA_STARTNAVI"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 255
    .end local v1    # "naviBundle":Landroid/os/Bundle;
    :cond_2
    if-eqz p2, :cond_3

    .line 257
    sget v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_A:I

    const/4 v3, 0x0

    .line 256
    invoke-static {v0, v2, v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    move-result-object v0

    .line 260
    :cond_3
    const-string/jumbo v2, "[NavigationMap]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "startMultiWindowWithIndicator!!, Multi?"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 261
    const-string/jumbo v4, ", tts:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 260
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->isOriginNeeded()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->origin:Lcom/google/android/gms/maps/model/LatLng;

    if-eqz v2, :cond_4

    .line 264
    const-string/jumbo v2, "lat"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->origin:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 265
    const-string/jumbo v2, "lng"

    .line 266
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->origin:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    .line 265
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 269
    :cond_4
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 274
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_5
    move-object v2, p4

    .line 239
    check-cast v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;

    iget v2, v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NaviMultiParcelable;->isStartMap:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 240
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.multiwindow.ACTION_START_NAVI"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 243
    :cond_6
    const-string/jumbo v2, "[NavigationMap]"

    .line 244
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "multiData is not instance of NaviMultiParcelable!! :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 245
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 244
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 243
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 271
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_7
    const-string/jumbo v2, "[NavigationMap]"

    .line 272
    const-string/jumbo v3, "startMultiWindowWithIndicator -> Multi Window Feature is disabled!!"

    .line 271
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startNavigation(Landroid/content/Context;DDLjava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "tts"    # Ljava/lang/String;

    .prologue
    .line 137
    return-void
.end method
