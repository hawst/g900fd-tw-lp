.class public abstract Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPointOfInterest;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;
.source "DLPointOfInterest.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x14c083a165203088L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLObject;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAddress()Ljava/lang/String;
.end method

.method public abstract getDistance()D
.end method

.method public abstract getInfo1()Ljava/lang/String;
.end method

.method public abstract getInfo2()Ljava/lang/String;
.end method

.method public abstract getInfo3()Ljava/lang/String;
.end method

.method public abstract getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPrice()D
.end method
