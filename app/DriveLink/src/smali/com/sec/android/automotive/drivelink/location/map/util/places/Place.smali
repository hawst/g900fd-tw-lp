.class public Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;
.super Ljava/lang/Object;
.source "Place.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x51b22387d533a0c4L


# instance fields
.field private address:Ljava/lang/String;

.field private icon:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private lat:D

.field private lng:D

.field private name:Ljava/lang/String;

.field private reference:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->icon:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLat()D
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->lat:D

    return-wide v0
.end method

.method public getLng()D
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->lng:D

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getReference()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->reference:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->address:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setIcon(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->icon:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->id:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setLat(D)V
    .locals 0
    .param p1, "lat"    # D

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->lat:D

    .line 93
    return-void
.end method

.method public setLng(D)V
    .locals 0
    .param p1, "lng"    # D

    .prologue
    .line 107
    iput-wide p1, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->lng:D

    .line 108
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->name:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setReference(Ljava/lang/String;)V
    .locals 0
    .param p1, "reference"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/map/util/places/Place;->reference:Ljava/lang/String;

    .line 123
    return-void
.end method
