.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestParkingLots.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RequestParkingLots"


# instance fields
.field private mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mOrder:I

.field private mRadius:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p4, "radius"    # I
    .param p5, "order"    # I

    .prologue
    const/4 v1, -0x1

    .line 20
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 14
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->mRadius:I

    .line 15
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->mOrder:I

    .line 22
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 23
    iput p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->mRadius:I

    .line 24
    iput p5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->mOrder:I

    .line 25
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->mDLLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 55
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestParkingLots;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 31
    .local v1, "service":Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    if-nez v1, :cond_1

    .line 32
    const-string/jumbo v2, "RequestParkingLots"

    const-string/jumbo v3, "[stop] DriveLinkServiceProvider is null."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 38
    .local v0, "manager":Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;
    if-nez v0, :cond_0

    .line 39
    const-string/jumbo v2, "RequestParkingLots"

    const-string/jumbo v3, "[stop] RecommendationManager is null."

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected response()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method
