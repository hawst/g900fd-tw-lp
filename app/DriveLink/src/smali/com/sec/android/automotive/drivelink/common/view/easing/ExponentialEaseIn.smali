.class public Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseIn;
.super Ljava/lang/Object;
.source "ExponentialEaseIn.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseIn;->s:F

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/view/easing/ExponentialEaseIn;->s:F

    .line 15
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 9
    .param p1, "input"    # F

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const-wide/16 v0, 0x0

    .line 18
    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-nez v2, :cond_0

    :goto_0
    double-to-float v0, v0

    return v0

    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 19
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const/high16 v6, 0x41200000    # 10.0f

    div-float v7, p1, v8

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x3f50624de0000000L    # 0.0010000000474974513

    sub-double/2addr v0, v2

    goto :goto_0
.end method
