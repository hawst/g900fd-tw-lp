.class public Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;
.super Landroid/app/Service;
.source "QuickMenuAccessService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field sbm:Landroid/app/StatusBarManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    .line 11
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "QuickMenuAccessService Service Created"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->getApplicationContext()Landroid/content/Context;

    const-string/jumbo v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    .line 22
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    const/high16 v1, 0x470000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 26
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "QuickMenuAccessService Service onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    .line 35
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "QuickMenuAccessService sbm not null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    invoke-virtual {v0, v2}, Landroid/app/StatusBarManager;->disable(I)V

    .line 44
    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 45
    return-void

    .line 40
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "QuickMenuAccessService sbm is null"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->getApplicationContext()Landroid/content/Context;

    const-string/jumbo v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/QuickMenuAccessService;->sbm:Landroid/app/StatusBarManager;

    invoke-virtual {v0, v2}, Landroid/app/StatusBarManager;->disable(I)V

    goto :goto_0
.end method
