.class Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;
.super Ljava/lang/Object;
.source "OnDriveLinkMusicRemotePlayerListenerImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->onMusicRemotePlayerShuffle(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;

.field private final synthetic val$_bShuffle:Z


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;

    iput-boolean p2, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;->val$_bShuffle:Z

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;->this$0:Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;

    # getter for: Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->mOnDriveLinkMusicRemotePlayerListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;->access$0(Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp;)Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;

    move-result-object v0

    .line 193
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/OnDriveLinkMusicRemotePlayerListenerImp$10;->val$_bShuffle:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicRemotePlayerListener;->onMusicRemotePlayerShuffle(Z)V

    .line 195
    :cond_0
    return-void
.end method
