.class public Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;
.super Ljava/lang/Object;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;,
        Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;
    }
.end annotation


# static fields
.field public static final INSTALL_FAILED_ALREADY_EXISTS:I = -0x1

.field public static final INSTALL_FAILED_CONFLICTING_PROVIDER:I = -0xd

.field public static final INSTALL_FAILED_CONTAINER_ERROR:I = -0x12

.field public static final INSTALL_FAILED_CPU_ABI_INCOMPATIBLE:I = -0x10

.field public static final INSTALL_FAILED_DEXOPT:I = -0xb

.field public static final INSTALL_FAILED_DUPLICATE_PACKAGE:I = -0x5

.field public static final INSTALL_FAILED_INSUFFICIENT_STORAGE:I = -0x4

.field public static final INSTALL_FAILED_INTERNAL_ERROR:I = -0x6e

.field public static final INSTALL_FAILED_INVALID_APK:I = -0x2

.field public static final INSTALL_FAILED_INVALID_INSTALL_LOCATION:I = -0x13

.field public static final INSTALL_FAILED_INVALID_URI:I = -0x3

.field public static final INSTALL_FAILED_MEDIA_UNAVAILABLE:I = -0x14

.field public static final INSTALL_FAILED_MISSING_FEATURE:I = -0x11

.field public static final INSTALL_FAILED_MISSING_SHARED_LIBRARY:I = -0x9

.field public static final INSTALL_FAILED_NEWER_SDK:I = -0xe

.field public static final INSTALL_FAILED_NO_SHARED_USER:I = -0x6

.field public static final INSTALL_FAILED_OLDER_SDK:I = -0xc

.field public static final INSTALL_FAILED_REPLACE_COULDNT_DELETE:I = -0xa

.field public static final INSTALL_FAILED_SHARED_USER_INCOMPATIBLE:I = -0x8

.field public static final INSTALL_FAILED_TEST_ONLY:I = -0xf

.field public static final INSTALL_FAILED_UPDATE_INCOMPATIBLE:I = -0x7

.field public static final INSTALL_FAILED_VERSION_DOWNGRADE:I = -0x19

.field public static final INSTALL_PARSE_FAILED_BAD_MANIFEST:I = -0x65

.field public static final INSTALL_PARSE_FAILED_BAD_PACKAGE_NAME:I = -0x6a

.field public static final INSTALL_PARSE_FAILED_BAD_SHARED_USER_ID:I = -0x6b

.field public static final INSTALL_PARSE_FAILED_CERTIFICATE_ENCODING:I = -0x69

.field public static final INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES:I = -0x68

.field public static final INSTALL_PARSE_FAILED_MANIFEST_EMPTY:I = -0x6d

.field public static final INSTALL_PARSE_FAILED_MANIFEST_MALFORMED:I = -0x6c

.field public static final INSTALL_PARSE_FAILED_NOT_APK:I = -0x64

.field public static final INSTALL_PARSE_FAILED_NO_CERTIFICATES:I = -0x67

.field public static final INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION:I = -0x66

.field public static final INSTALL_REPLACE_EXISTING:I = 0x2

.field public static final INSTALL_SUCCEEDED:I = 0x1


# instance fields
.field private TAG:Ljava/lang/String;

.field private callHander:Landroid/os/Handler;

.field private deleteHandler:Landroid/os/Handler;

.field private deleteObserver:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;

.field private handler:Landroid/os/Handler;

.field private method:Ljava/lang/reflect/Method;

.field private methodForExistingPackage:Ljava/lang/reflect/Method;

.field private observer:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;

.field private onCallPackageInstall:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;

.field private onInstalledPackaged:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

.field private onPackageDeleted:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;

.field pkgname:Ljava/lang/String;

.field private pm:Landroid/content/pm/PackageManager;

.field returncode:I

.field private uninstallmethod:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string/jumbo v0, "ApplicationManager"

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->TAG:Ljava/lang/String;

    .line 441
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->handler:Landroid/os/Handler;

    .line 449
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteHandler:Landroid/os/Handler;

    .line 460
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->callHander:Landroid/os/Handler;

    .line 477
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string/jumbo v3, "ApplicationManager"

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->TAG:Ljava/lang/String;

    .line 441
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->handler:Landroid/os/Handler;

    .line 449
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$2;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteHandler:Landroid/os/Handler;

    .line 460
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$3;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$3;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->callHander:Landroid/os/Handler;

    .line 510
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->observer:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;

    .line 511
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteObserver:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;

    .line 512
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    .line 514
    const/4 v3, 0x4

    new-array v1, v3, [Ljava/lang/Class;

    const-class v3, Landroid/net/Uri;

    aput-object v3, v1, v4

    .line 515
    const-class v3, Landroid/content/pm/IPackageInstallObserver;

    aput-object v3, v1, v5

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v6

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v7

    .line 516
    .local v1, "types":[Ljava/lang/Class;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "installPackage"

    invoke-virtual {v3, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->method:Ljava/lang/reflect/Method;

    .line 519
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 520
    const-string/jumbo v4, "installExistingPackage"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    .line 519
    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    :goto_0
    const/4 v3, 0x3

    :try_start_1
    new-array v2, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 527
    const-class v4, Landroid/content/pm/IPackageDeleteObserver;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    .line 528
    .local v2, "uninstalltypes":[Ljava/lang/Class;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "deletePackage"

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 534
    .end local v2    # "uninstalltypes":[Ljava/lang/Class;
    :goto_1
    return-void

    .line 521
    :catch_0
    move-exception v0

    .line 522
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iput-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 531
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 532
    .restart local v0    # "e":Ljava/lang/NoSuchMethodException;
    iput-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onPackageDeleted:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onCallPackageInstall:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public initalize(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 481
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->observer:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;

    .line 482
    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;

    invoke-direct {v3, p0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;)V

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteObserver:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;

    .line 483
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    .line 485
    const/4 v3, 0x4

    new-array v1, v3, [Ljava/lang/Class;

    const-class v3, Landroid/net/Uri;

    aput-object v3, v1, v4

    .line 486
    const-class v3, Landroid/content/pm/IPackageInstallObserver;

    aput-object v3, v1, v5

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v6

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v7

    .line 487
    .local v1, "types":[Ljava/lang/Class;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "installPackage"

    invoke-virtual {v3, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->method:Ljava/lang/reflect/Method;

    .line 490
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 491
    const-string/jumbo v4, "installExistingPackage"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    .line 490
    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    :goto_0
    const/4 v3, 0x3

    :try_start_1
    new-array v2, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 498
    const-class v4, Landroid/content/pm/IPackageDeleteObserver;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    .line 499
    .local v2, "uninstalltypes":[Ljava/lang/Class;
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "deletePackage"

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 505
    .end local v2    # "uninstalltypes":[Ljava/lang/Class;
    :goto_1
    return-void

    .line 492
    :catch_0
    move-exception v0

    .line 493
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iput-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 502
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 503
    .restart local v0    # "e":Ljava/lang/NoSuchMethodException;
    iput-object v8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    goto :goto_1
.end method

.method public installExistingPackage(Ljava/lang/String;)V
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 584
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    if-nez v5, :cond_0

    .line 585
    new-instance v5, Ljava/lang/Exception;

    const-string/jumbo v6, "Can not find installExistingPackage method."

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 588
    :cond_0
    const/4 v4, 0x1

    .line 590
    .local v4, "result":Z
    const/4 v1, 0x0

    .line 593
    .local v1, "error":Ljava/lang/Exception;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->TAG:Ljava/lang/String;

    .line 594
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "installPackage method invoke."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 593
    invoke-static {v5, v6}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    .line 596
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    .line 595
    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 598
    .local v3, "output":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->returncode:I

    .line 600
    iget v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->returncode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, v9, :cond_2

    .line 601
    const/4 v4, 0x1

    .line 613
    .end local v3    # "output":Ljava/lang/Integer;
    :goto_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 615
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

    if-eqz v5, :cond_1

    .line 619
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->handler:Landroid/os/Handler;

    invoke-virtual {v5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 620
    .local v2, "msg":Landroid/os/Message;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->handler:Landroid/os/Handler;

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 623
    .end local v2    # "msg":Landroid/os/Message;
    :cond_1
    if-nez v4, :cond_3

    if-eqz v1, :cond_3

    .line 624
    throw v1

    .line 603
    .restart local v3    # "output":Ljava/lang/Integer;
    :cond_2
    const/4 v4, 0x0

    .line 604
    :try_start_1
    new-instance v1, Ljava/lang/Exception;

    .end local v1    # "error":Ljava/lang/Exception;
    const-string/jumbo v5, "Failed to install existing package."

    invoke-direct {v1, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v1    # "error":Ljava/lang/Exception;
    goto :goto_0

    .line 607
    .end local v1    # "error":Ljava/lang/Exception;
    .end local v3    # "output":Ljava/lang/Integer;
    :catch_0
    move-exception v0

    .line 608
    .local v0, "e":Ljava/lang/Exception;
    const/16 v5, -0x6e

    iput v5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->returncode:I

    .line 609
    const/4 v4, 0x0

    .line 610
    move-object v1, v0

    .restart local v1    # "error":Ljava/lang/Exception;
    goto :goto_0

    .line 626
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    return-void
.end method

.method public installPackage(Landroid/net/Uri;)V
    .locals 7
    .param p1, "apkFile"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 574
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "installPackage method invoke."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->method:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->observer:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageInstallObserver;

    aput-object v4, v3, v5

    .line 576
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 575
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 579
    .local v0, "msg":Landroid/os/Message;
    iput v5, v0, Landroid/os/Message;->arg1:I

    .line 580
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->callHander:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 581
    return-void
.end method

.method public installPackage(Ljava/io/File;)V
    .locals 2
    .param p1, "apkFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 563
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 564
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 567
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 569
    .local v0, "packageURI":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->installPackage(Landroid/net/Uri;)V

    .line 570
    return-void
.end method

.method public installPackage(Ljava/lang/String;)V
    .locals 1
    .param p1, "apkFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 558
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->installPackage(Ljava/io/File;)V

    .line 559
    return-void
.end method

.method public setOnCallPackageInstall(Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;)V
    .locals 0
    .param p1, "onCallPackageInstall"    # Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;

    .prologue
    .line 546
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onCallPackageInstall:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnCallPackageInstall;

    .line 547
    return-void
.end method

.method public setOnDeletePackage(Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;)V
    .locals 0
    .param p1, "onPackageDeleted"    # Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;

    .prologue
    .line 541
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onPackageDeleted:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageDeleted;

    .line 542
    return-void
.end method

.method public setOnInstalledPackaged(Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;)V
    .locals 0
    .param p1, "onInstalledPackaged"    # Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

    .prologue
    .line 537
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/automotive/drivelink/framework/manager/update/OnPackageInstalled;

    .line 538
    return-void
.end method

.method public uninstallPackage(Ljava/lang/String;)V
    .locals 6
    .param p1, "packagename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 552
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager;->deleteObserver:Lcom/sec/android/automotive/drivelink/framework/manager/update/ApplicationManager$PackageDeleteObserver;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 553
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 552
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    return-void
.end method
