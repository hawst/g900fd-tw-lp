.class final enum Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;
.super Ljava/lang/Enum;
.source "SetupWizardMyCarActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "CarBTState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

.field public static final enum FIRST_ACCESS_REGISTERED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

.field public static final enum UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

.field public static final enum UNREGISTERED_NOT_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    const-string/jumbo v1, "FIRST_ACCESS_REGISTERED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->FIRST_ACCESS_REGISTERED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    const-string/jumbo v1, "UNREGISTERED_CONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    const-string/jumbo v1, "UNREGISTERED_NOT_CONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_NOT_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    .line 86
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->FIRST_ACCESS_REGISTERED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->UNREGISTERED_NOT_CONNECTED:Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;->ENUM$VALUES:[Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/automotive/drivelink/settings/SetupWizardMyCarActivity$CarBTState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
