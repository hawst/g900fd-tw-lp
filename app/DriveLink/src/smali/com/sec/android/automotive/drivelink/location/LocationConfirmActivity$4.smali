.class Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$4;
.super Ljava/lang/Object;
.source "LocationConfirmActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 164
    const-string/jumbo v1, "VAC_CLIENT_DM"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "DM_LOCATION_NAV_SEARCH_CONFIRM"

    if-ne v1, v2, :cond_0

    .line 166
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 167
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "DM_LOCATION"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 170
    .end local v0    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity$4;->this$0:Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;->finish()V

    .line 171
    return-void
.end method
