.class public Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;
.super Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;
.source "RegulationsActivity.java"


# static fields
.field private static final KEY_IS_CHECKED:Ljava/lang/String; = "KEY_IS_CHECKED"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private cbTermsofServiceAgree1:Landroid/widget/CheckBox;

.field private isChecked:[Z

.field private isGetFile:Z

.field private layoutBasicSettingsNext:Landroid/widget/LinearLayout;

.field private layoutBasicSettingsNextBtn:Landroid/widget/LinearLayout;

.field private layoutTermsofServiceAgree1:Landroid/widget/LinearLayout;

.field private regulation:Ljava/lang/String;

.field private tvTermsofServiceAgree1:Landroid/widget/TextView;

.field private tvTermsofServiceBody1:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->TAG:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 38
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;-><init>()V

    .line 43
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isGetFile:Z

    .line 55
    const/4 v0, 0x5

    new-array v0, v0, [Z

    const/4 v1, 0x2

    aput-boolean v2, v0, v1

    const/4 v1, 0x3

    aput-boolean v2, v0, v1

    const/4 v1, 0x4

    aput-boolean v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z

    .line 38
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;)[Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->toggleEnableNext()V

    return-void
.end method

.method private readRegulationFile()Ljava/lang/String;
    .locals 9

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 268
    const v8, 0x7f060016

    .line 267
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 269
    .local v3, "input":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 272
    .local v4, "result":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v6

    .line 274
    .local v6, "size":I
    if-lez v6, :cond_0

    .line 275
    new-array v0, v6, [B

    .line 276
    .local v0, "data":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    .line 277
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v4    # "result":Ljava/lang/String;
    .local v5, "result":Ljava/lang/String;
    move-object v4, v5

    .line 283
    .end local v0    # "data":[B
    .end local v5    # "result":Ljava/lang/String;
    .restart local v4    # "result":Ljava/lang/String;
    :cond_0
    if-eqz v3, :cond_1

    .line 285
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 286
    const/4 v3, 0x0

    .line 291
    :cond_1
    :goto_0
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isGetFile:Z

    if-nez v7, :cond_2

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->finish()V

    .line 296
    .end local v6    # "size":I
    :cond_2
    :goto_1
    return-object v4

    .line 279
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    :try_start_2
    iput-boolean v7, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isGetFile:Z

    .line 281
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 283
    if-eqz v3, :cond_3

    .line 285
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 286
    const/4 v3, 0x0

    .line 291
    :cond_3
    :goto_2
    iget-boolean v7, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isGetFile:Z

    if-nez v7, :cond_2

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->finish()V

    goto :goto_1

    .line 287
    :catch_1
    move-exception v2

    .line 288
    .local v2, "ex":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 282
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 283
    if-eqz v3, :cond_4

    .line 285
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 286
    const/4 v3, 0x0

    .line 291
    :cond_4
    :goto_3
    iget-boolean v8, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isGetFile:Z

    if-nez v8, :cond_5

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->finish()V

    .line 294
    :cond_5
    throw v7

    .line 287
    :catch_2
    move-exception v2

    .line 288
    .restart local v2    # "ex":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 287
    .end local v2    # "ex":Ljava/lang/Exception;
    .restart local v6    # "size":I
    :catch_3
    move-exception v2

    .line 288
    .restart local v2    # "ex":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private setRegulationText()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->regulation:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 252
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->readRegulationFile()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->regulation:Ljava/lang/String;

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->tvTermsofServiceBody1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->regulation:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    return-void
.end method

.method private setupView(Landroid/widget/TextView;Landroid/widget/CheckBox;Landroid/widget/TextView;Landroid/widget/LinearLayout;I)V
    .locals 5
    .param p1, "tvTermsofServiceBody"    # Landroid/widget/TextView;
    .param p2, "cbTermsofServiceAgree"    # Landroid/widget/CheckBox;
    .param p3, "tvTermsofServiceAgree"    # Landroid/widget/TextView;
    .param p4, "layoutTermsofServiceAgree"    # Landroid/widget/LinearLayout;
    .param p5, "index"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 188
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    invoke-virtual {p2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 190
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 191
    invoke-virtual {p2, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 192
    invoke-virtual {p2, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 193
    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-virtual {p2, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 196
    invoke-virtual {p4, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 198
    const v1, 0x7f0a0278

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 197
    invoke-virtual {p2, v1}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 199
    new-instance v0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;

    invoke-direct {v0, p0, p2, p5}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;Landroid/widget/CheckBox;I)V

    .line 219
    .local v0, "agreeOnClickListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z

    aget-boolean v1, v1, p5

    invoke-virtual {p2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 220
    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    invoke-virtual {p4, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    invoke-virtual {p4, v2}, Landroid/widget/LinearLayout;->setAddStatesFromChildren(Z)V

    goto :goto_0
.end method

.method private toggleEnableNext()V
    .locals 2

    .prologue
    .line 165
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->cbTermsofServiceAgree1:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 166
    .local v0, "enabled":Z
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutBasicSettingsNextBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 167
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 307
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 308
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->setRegulationText()V

    .line 309
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f0a0442

    const v9, 0x7f090005

    const/16 v8, 0x8

    const/4 v5, 0x0

    .line 66
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    sget-object v0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const v0, 0x7f030027

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->setContentView(I)V

    .line 70
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->setContext(Landroid/content/Context;)V

    .line 72
    if-eqz p1, :cond_0

    .line 74
    const-string/jumbo v0, "KEY_IS_CHECKED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-string/jumbo v0, "KEY_IS_CHECKED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    .line 75
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z

    .line 85
    :goto_0
    const v0, 0x7f090114

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->tvTermsofServiceBody1:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f090116

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 86
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->cbTermsofServiceAgree1:Landroid/widget/CheckBox;

    .line 89
    const v0, 0x7f090117

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 88
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->tvTermsofServiceAgree1:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f090115

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 90
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutTermsofServiceAgree1:Landroid/widget/LinearLayout;

    .line 93
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->tvTermsofServiceBody1:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->cbTermsofServiceAgree1:Landroid/widget/CheckBox;

    .line 94
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->tvTermsofServiceAgree1:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutTermsofServiceAgree1:Landroid/widget/LinearLayout;

    move-object v0, p0

    .line 93
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->setupView(Landroid/widget/TextView;Landroid/widget/CheckBox;Landroid/widget/TextView;Landroid/widget/LinearLayout;I)V

    .line 96
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->setRegulationText()V

    .line 98
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    const v0, 0x7f090112

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 100
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 102
    const v2, 0x7f0d0041

    .line 101
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 100
    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 103
    const v0, 0x7f090111

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 120
    :goto_1
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->setTitle(I)V

    .line 123
    const v0, 0x7f090011

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 122
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutBasicSettingsNext:Landroid/widget/LinearLayout;

    .line 125
    const v0, 0x7f090012

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 124
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutBasicSettingsNextBtn:Landroid/widget/LinearLayout;

    .line 126
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutBasicSettingsNextBtn:Landroid/widget/LinearLayout;

    .line 127
    new-instance v1, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutBasicSettingsNext:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 153
    :goto_2
    return-void

    .line 79
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/firstaccess/HomeFirstFlowManager;->getUserRegulationAgreement()Z

    move-result v7

    .line 80
    .local v7, "checked":Z
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z

    aput-boolean v7, v0, v5

    .line 81
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z

    const/4 v1, 0x1

    aput-boolean v7, v0, v1

    goto/16 :goto_0

    .line 107
    .end local v7    # "checked":Z
    :cond_1
    invoke-virtual {p0, v9}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 106
    check-cast v6, Landroid/widget/LinearLayout;

    .line 108
    .local v6, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;)V

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 116
    const-string/jumbo v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 117
    const v1, 0x7f0a03f4

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 150
    .end local v6    # "btnNavBack":Landroid/widget/LinearLayout;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->layoutBasicSettingsNext:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->toggleEnableNext()V

    goto :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 236
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/settings/BaseFirstAccessActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 237
    const-string/jumbo v0, "KEY_IS_CHECKED"

    .line 238
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;->isChecked:[Z

    .line 237
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 239
    return-void
.end method
