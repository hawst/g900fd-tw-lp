.class Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;
.super Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
.source "NotificationPlaceOnDisclaimerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->onAttach(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    .line 83
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseCreateUserProfile(Z)V
    .locals 3
    .param p1, "result"    # Z

    .prologue
    const/4 v2, 0x1

    .line 85
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mLocationListner:Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->access$3(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/location/listener/OnDrivelinkLocationBaseListener;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->removeLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 88
    if-nez p1, :cond_1

    .line 89
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;->onClickListener(Z)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mShared:Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->access$1(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 96
    const-string/jumbo v1, "PREF_ACCEPT_DISCLAIMER_PLACE_ON"

    .line 95
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$2;->this$0:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    # getter for: Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->mOnClickListener:Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->access$2(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;)Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;->onClickListener(Z)V

    goto :goto_0
.end method
