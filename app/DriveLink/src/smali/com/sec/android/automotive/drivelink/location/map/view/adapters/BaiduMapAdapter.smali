.class public Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;
.super Lcom/sec/android/automotive/drivelink/location/map/LocationMap;
.source "BaiduMapAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BaiduMapAdapter"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/location/map/view/adapters/BaiduMapAdapter;->createMapFragment()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 0
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;-><init>()V

    .line 43
    return-void
.end method

.method private createMarkPin(DD)V
    .locals 0
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 257
    return-void
.end method


# virtual methods
.method public clearMarkers()V
    .locals 0

    .prologue
    .line 122
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->clearMarkers()V

    .line 126
    return-void
.end method

.method protected createMapFragment()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public createUpdateableRoute(I)Lcom/sec/android/automotive/drivelink/location/model/IGraphicRoute;
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 230
    const/4 v0, 0x0

    return-object v0
.end method

.method public drawRoute(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;"
    return-void
.end method

.method public removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V
    .locals 0
    .param p1, "marker"    # Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->removeMarkerItem(Lcom/sec/android/automotive/drivelink/location/map/common/LocationMarkerItem;)V

    .line 112
    return-void
.end method

.method public setCompassEnabled(Z)V
    .locals 0
    .param p1, "option"    # Z

    .prologue
    .line 295
    return-void
.end method

.method public setLocationPoint(DD)V
    .locals 0
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 116
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/location/map/LocationMap;->setLocationPoint(DD)V

    .line 118
    return-void
.end method

.method public setZoomControlsEnabled(Z)V
    .locals 0
    .param p1, "option"    # Z

    .prologue
    .line 235
    return-void
.end method

.method public setZoomLevel(F)V
    .locals 0
    .param p1, "level"    # F

    .prologue
    .line 132
    return-void
.end method

.method public setZoomLevel(FDDI)V
    .locals 0
    .param p1, "level"    # F
    .param p2, "lat"    # D
    .param p4, "lng"    # D
    .param p6, "userIndex"    # I

    .prologue
    .line 141
    return-void
.end method

.method public zoomToArea(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;I)V
    .locals 0
    .param p1, "loc1"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p2, "loc2"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p3, "padding"    # I

    .prologue
    .line 225
    return-void
.end method
