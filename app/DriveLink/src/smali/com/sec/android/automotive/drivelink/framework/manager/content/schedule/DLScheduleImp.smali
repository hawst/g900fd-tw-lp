.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;
.source "DLScheduleImp.java"


# instance fields
.field private mAllDay:I

.field private mAttendeeContactId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAttendeesContactList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mCalendarId:I

.field private mEventDescription:Ljava/lang/String;

.field private mEventDuration:I

.field private mEventEndDate:Ljava/util/Date;

.field private mEventId:I

.field private mEventStartDate:Ljava/util/Date;

.field private mEventTitle:Ljava/lang/String;

.field private mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeeContactId:Ljava/util/ArrayList;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeesContactList:Ljava/util/ArrayList;

    .line 23
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 26
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 27
    return-void
.end method

.method public constructor <init>(IILjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;DDI)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "calendarId"    # I
    .param p3, "startDate"    # Ljava/util/Date;
    .param p4, "endDate"    # Ljava/util/Date;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "title"    # Ljava/lang/String;
    .param p7, "location"    # Ljava/lang/String;
    .param p9, "latitude"    # D
    .param p11, "longitude"    # D
    .param p13, "allDay"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;DDI)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p8, "attendeesContactId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLSchedule;-><init>()V

    .line 21
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeeContactId:Ljava/util/ArrayList;

    .line 22
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeesContactList:Ljava/util/ArrayList;

    .line 23
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 33
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventId:I

    .line 34
    iput p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mCalendarId:I

    .line 35
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventStartDate:Ljava/util/Date;

    .line 36
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventEndDate:Ljava/util/Date;

    .line 37
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventDescription:Ljava/lang/String;

    .line 38
    iput-object p6, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventTitle:Ljava/lang/String;

    .line 39
    iput-object p8, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeeContactId:Ljava/util/ArrayList;

    .line 40
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAllDay:I

    .line 42
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 43
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-virtual {v1, p7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationName(Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    invoke-virtual {v1, p7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLocationAddress(Ljava/lang/String;)V

    .line 45
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double v2, p9, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLatitude(D)V

    .line 46
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double v2, p11, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;->setLongitude(D)V

    .line 47
    return-void
.end method


# virtual methods
.method public getAllDay()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAllDay:I

    return v0
.end method

.method public getAttendeesContactList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeesContactList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAttendeesList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeeContactId:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCalendarId()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mCalendarId:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventDuration:I

    return v0
.end method

.method public getEndDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventEndDate:Ljava/util/Date;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventId:I

    return v0
.end method

.method public getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    return-object v0
.end method

.method public getStartDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventStartDate:Ljava/util/Date;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setAttendeesContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "attendeesContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mAttendeesContactList:Ljava/util/ArrayList;

    .line 144
    return-void
.end method

.method public setCalendarId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mCalendarId:I

    .line 67
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "desc"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventDescription:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventDuration:I

    .line 97
    return-void
.end method

.method public setEndDate(Ljava/util/Date;)V
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventEndDate:Ljava/util/Date;

    .line 87
    return-void
.end method

.method public setEventId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventId:I

    .line 57
    return-void
.end method

.method public setLocation(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;)V
    .locals 0
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mLocation:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .line 127
    return-void
.end method

.method public setStartDate(Ljava/util/Date;)V
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventStartDate:Ljava/util/Date;

    .line 77
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/schedule/DLScheduleImp;->mEventTitle:Ljava/lang/String;

    .line 117
    return-void
.end method
