.class public Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;
.super Ljava/lang/Object;
.source "SamsungAccount.java"


# static fields
.field private static final HTTP_CONNECTION_TIMEOUT:I = 0x1388

.field private static final HTTP_SOCKET_TIMEOUT:I = 0x1388

.field private static final INSTANCE:Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;

.field public static final REQUEST_LOAD_SIGN_IN_SCREEN:I = 0x1

.field public static final TAG:Ljava/lang/String; = "[Samsung Account]"

.field private static accessToken:Ljava/lang/String; = null

.field private static birthday:Ljava/lang/String; = null

.field private static cc:Ljava/lang/String; = null

.field public static final clientId:Ljava/lang/String; = "u0zz5r9gn0"

.field public static final clientSecret:Ljava/lang/String; = "53507B3F26D537D106397316DEA2F29C"

.field private static email:Ljava/lang/String; = null

.field private static eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier; = null

.field private static isServiceStarted:Z = false

.field private static mContext:Landroid/content/Context; = null

.field private static mExpiredAccessToken:Ljava/lang/String; = null

.field static mHandler:Landroid/os/Handler; = null

.field private static mISaService:Lcom/msc/sa/aidl/ISAService; = null

.field private static mRegistrationCode:Ljava/lang/String; = null

.field private static mSACallback:Lcom/msc/sa/aidl/ISACallback; = null

.field private static mServiceConnection:Landroid/content/ServiceConnection; = null

.field public static final packageName:Ljava/lang/String; = "com.sec.android.automotive.drivelink"

.field private static requestId:I

.field private static resultData:Landroid/os/Bundle;

.field private static userId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->isServiceStarted:Z

    .line 78
    sput-object v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    .line 80
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->INSTANCE:Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;

    .line 85
    sput-object v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mExpiredAccessToken:Ljava/lang/String;

    .line 86
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->resultData:Landroid/os/Bundle;

    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mHandler:Landroid/os/Handler;

    .line 132
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 266
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    return-void
.end method

.method private static RequestDisconnectService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 321
    const-string/jumbo v0, "[Samsung Account]"

    const-string/jumbo v1, "RequestDisconnectService start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :goto_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;

    .line 328
    const-string/jumbo v0, "[Samsung Account]"

    const-string/jumbo v1, "RequestDisconnectService end"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return-void

    .line 324
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static RequestSamsungAccountAccessToken(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "expiredAccessToken"    # Ljava/lang/String;

    .prologue
    .line 100
    sput-object p1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mExpiredAccessToken:Ljava/lang/String;

    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "toReturn":Z
    sget-boolean v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->isServiceStarted:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v1, :cond_0

    .line 103
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestDisconnectService(Landroid/content/Context;)V

    .line 105
    :cond_0
    const/16 v1, 0xad2

    sput v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->requestId:I

    .line 106
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;

    .line 107
    new-instance v1, Landroid/content/Intent;

    .line 108
    const-string/jumbo v2, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 109
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    .line 107
    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 110
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->isServiceStarted:Z

    .line 111
    return v0
.end method

.method public static RequestSamsungAccountUserName(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "expiredAccessToken"    # Ljava/lang/String;

    .prologue
    .line 116
    sput-object p1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mExpiredAccessToken:Ljava/lang/String;

    .line 117
    const/4 v0, 0x0

    .line 118
    .local v0, "toReturn":Z
    sget-boolean v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->isServiceStarted:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v1, :cond_0

    .line 119
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestDisconnectService(Landroid/content/Context;)V

    .line 122
    :cond_0
    const/16 v1, 0xad7

    sput v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->requestId:I

    .line 123
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;

    .line 124
    new-instance v1, Landroid/content/Intent;

    .line 125
    const-string/jumbo v2, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    .line 124
    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 127
    sput-boolean v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->isServiceStarted:Z

    .line 129
    return v0
.end method

.method public static RequestSignInScreen(Landroid/app/Activity;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 291
    if-eqz p0, :cond_0

    .line 292
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;

    .line 293
    new-instance v0, Landroid/content/Intent;

    .line 294
    const-string/jumbo v1, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    .line 293
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "client_id"

    const-string/jumbo v2, "u0zz5r9gn0"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    const-string/jumbo v1, "client_secret"

    const-string/jumbo v2, "53507B3F26D537D106397316DEA2F29C"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    const-string/jumbo v1, "mypackage"

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 298
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 297
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string/jumbo v1, "OSP_VER"

    const-string/jumbo v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    const-string/jumbo v1, "MODE"

    const-string/jumbo v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 303
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private static RequestUnregisterCallback()Z
    .locals 4

    .prologue
    .line 306
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "RequestUnregisterCallback start"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    const/4 v1, 0x0

    .line 308
    .local v1, "toReturn":Z
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v2, :cond_0

    .line 310
    :try_start_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;

    sget-object v3, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 315
    :cond_0
    :goto_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestDisconnectService(Landroid/content/Context;)V

    .line 316
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "RequestUnregisterCallback end"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    return v1

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private static RequestUserNameAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 270
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "RequestUserNameAccount start"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const/4 v1, 0x0

    .line 274
    .local v1, "toReturn":Z
    :try_start_0
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->getUserName(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 279
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z

    .line 286
    :goto_0
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "RequestUserNameAccount end"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    return v1

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    const/4 v1, 0x0

    .line 279
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z

    goto :goto_0

    .line 278
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 279
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z

    .line 280
    throw v2
.end method

.method static synthetic access$0(Lcom/msc/sa/aidl/ISAService;)V
    .locals 0

    .prologue
    .line 83
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-void
.end method

.method static synthetic access$1(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 70
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->accessToken:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$10()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$11(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 268
    invoke-static {p0, p1, p2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUserNameAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/msc/sa/aidl/ISACallback;)V
    .locals 0

    .prologue
    .line 82
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    return-void
.end method

.method static synthetic access$13()Lcom/msc/sa/aidl/ISACallback;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    return-object v0
.end method

.method static synthetic access$14()Lcom/msc/sa/aidl/ISAService;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-object v0
.end method

.method static synthetic access$15(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mRegistrationCode:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$16()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mExpiredAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$17()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mRegistrationCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$18()I
    .locals 1

    .prologue
    .line 88
    sget v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->requestId:I

    return v0
.end method

.method static synthetic access$19(Z)V
    .locals 0

    .prologue
    .line 76
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->isServiceStarted:Z

    return-void
.end method

.method static synthetic access$2(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->userId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$20()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->resultData:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$3(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->birthday:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    sput-object p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->cc:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6()Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    return-object v0
.end method

.method static synthetic access$7()Z
    .locals 1

    .prologue
    .line 305
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8()Landroid/content/Context;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$9()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;
    .locals 2

    .prologue
    .line 94
    const-string/jumbo v0, "[Samsung Account]"

    const-string/jumbo v1, "SA GetInstance()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->INSTANCE:Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;

    return-object v0
.end method

.method public static getSamsungAccountEmailFromAccounts()Ljava/lang/String;
    .locals 3

    .prologue
    .line 383
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 384
    .local v1, "manager":Landroid/accounts/AccountManager;
    const-string/jumbo v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 385
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 386
    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 388
    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v2, ""

    goto :goto_0
.end method

.method private static getUserName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 19
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 418
    const/4 v10, 0x0

    .line 420
    .local v10, "line":Ljava/lang/String;
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 421
    .local v4, "httpClient":Lorg/apache/http/client/HttpClient;
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v16

    const-string/jumbo v17, "http.protocol.expect-continue"

    .line 422
    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    .line 421
    invoke-interface/range {v16 .. v18}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 423
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v16

    const-string/jumbo v17, "http.connection.timeout"

    .line 424
    const/16 v18, 0x1388

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    .line 423
    invoke-interface/range {v16 .. v18}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 425
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v16

    const-string/jumbo v17, "http.socket.timeout"

    .line 426
    const/16 v18, 0x1388

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    .line 425
    invoke-interface/range {v16 .. v18}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 428
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    .line 429
    .local v15, "url":Ljava/lang/StringBuffer;
    const-string/jumbo v16, "https://api.samsungosp.com/v2/profile/user/user/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 431
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    .line 433
    .local v12, "requestURL":Ljava/lang/String;
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v12}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 434
    .local v5, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    const-string/jumbo v16, "Content-Type"

    const-string/jumbo v17, "text/xml"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const-string/jumbo v16, "authorization"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string/jumbo v18, "Bearer "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :try_start_0
    invoke-interface {v4, v5}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 440
    .local v13, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 441
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    .line 443
    .local v7, "is":Ljava/io/InputStream;
    new-instance v11, Ljava/io/BufferedReader;

    new-instance v16, Ljava/io/InputStreamReader;

    .line 444
    const-string/jumbo v17, "UTF-8"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v7, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 443
    move-object/from16 v0, v16

    invoke-direct {v11, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 445
    .local v11, "reader":Ljava/io/BufferedReader;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 447
    .local v14, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_0

    .line 451
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->onPostExecute(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 452
    .local v6, "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v6, :cond_1

    .line 453
    const/16 v16, 0x0

    .line 494
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v6    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sb":Ljava/lang/StringBuilder;
    :goto_1
    return v16

    .line 448
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v7    # "is":Ljava/io/InputStream;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v17, "\n"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 488
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v2

    .line 489
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 491
    const/16 v16, 0x0

    goto :goto_1

    .line 456
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v6    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v7    # "is":Ljava/io/InputStream;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_1
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    .line 457
    .local v9, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v9, :cond_2

    .line 458
    const/16 v16, 0x0

    goto :goto_1

    .line 461
    :cond_2
    sget-object v16, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->resultData:Landroid/os/Bundle;

    invoke-virtual/range {v16 .. v16}, Landroid/os/Bundle;->clear()V

    .line 463
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_3

    .line 468
    sget-object v16, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mHandler:Landroid/os/Handler;

    new-instance v17, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$2;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$2;-><init>()V

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 494
    const/16 v16, 0x1

    goto :goto_1

    .line 463
    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 464
    .local v8, "key":Ljava/lang/String;
    sget-object v18, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->resultData:Landroid/os/Bundle;

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public static hasSamsungAccount(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 392
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 393
    .local v1, "manager":Landroid/accounts/AccountManager;
    const-string/jumbo v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 394
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 395
    const/4 v2, 0x1

    .line 397
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isHangul(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 583
    const v0, 0xac00

    if-lt p0, v0, :cond_0

    const v0, 0xd7a3

    if-le p0, v0, :cond_1

    .line 584
    :cond_0
    const/4 v0, 0x0

    .line 587
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static onPostExecute(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 17
    .param p0, "result"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    const/4 v11, 0x0

    .line 500
    .local v11, "isFamilyName":Z
    const/4 v12, 0x0

    .line 501
    .local v12, "isGivenName":Z
    const/4 v10, 0x0

    .line 502
    .local v10, "isEmail":Z
    const/4 v9, 0x0

    .line 504
    .local v9, "isCountryCode":Z
    const/4 v5, 0x0

    .line 505
    .local v5, "familyName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 506
    .local v7, "givenName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 507
    .local v6, "fullName":Ljava/lang/String;
    const/4 v14, 0x0

    .line 508
    .local v14, "receiveEmailText":Ljava/lang/String;
    const/4 v1, 0x0

    .line 510
    .local v1, "countryCode":Ljava/lang/String;
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 513
    .local v8, "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 514
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v13

    .line 515
    .local v13, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v15, Ljava/io/StringReader;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v13, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 517
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 518
    .local v3, "eventType":I
    :goto_0
    const/4 v15, 0x1

    if-ne v3, v15, :cond_0

    .line 579
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :goto_1
    return-object v8

    .line 519
    .restart local v3    # "eventType":I
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_0
    packed-switch v3, :pswitch_data_0

    .line 569
    :cond_1
    :goto_2
    :pswitch_0
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 525
    :pswitch_1
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "familyName"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 526
    const/4 v11, 0x1

    .line 528
    :cond_2
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "givenName"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 529
    const/4 v12, 0x1

    .line 531
    :cond_3
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "login_id"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 532
    const/4 v10, 0x1

    .line 534
    :cond_4
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "countryCode"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 535
    const/4 v9, 0x1

    .line 538
    goto :goto_2

    .line 542
    :pswitch_2
    if-eqz v11, :cond_5

    .line 543
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v5

    .line 544
    const/4 v11, 0x0

    .line 545
    goto :goto_2

    :cond_5
    if-eqz v12, :cond_7

    .line 546
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    .line 549
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-static {v15}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->isHangul(C)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 550
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 555
    :goto_3
    const-string/jumbo v15, "userName"

    invoke-virtual {v8, v15, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    const/4 v12, 0x0

    .line 557
    goto :goto_2

    .line 552
    :cond_6
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 557
    :cond_7
    if-eqz v10, :cond_8

    .line 558
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v14

    .line 559
    const-string/jumbo v15, "eMail"

    invoke-virtual {v8, v15, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    const/4 v10, 0x0

    .line 561
    goto/16 :goto_2

    :cond_8
    if-eqz v9, :cond_1

    .line 562
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    .line 563
    const-string/jumbo v15, "countryCode"

    invoke-virtual {v8, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 564
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 571
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v13    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v2

    .line 572
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 573
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 574
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v2

    .line 575
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 576
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 519
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static setDLRegisteredSamsungAccountEmail(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 338
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 339
    const-string/jumbo v1, "PREF_SETTINGS_ACCOUNT_EMAIL"

    .line 338
    invoke-virtual {v0, v1, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    return-void
.end method


# virtual methods
.method public getBirthday()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->birthday:Ljava/lang/String;

    return-object v0
.end method

.method public getCc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->cc:Ljava/lang/String;

    return-object v0
.end method

.method public getDLRegisteredSamsungAccountEmail(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    .line 343
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 344
    .local v3, "manager":Landroid/accounts/AccountManager;
    const-string/jumbo v4, "com.osp.app.signin"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 345
    .local v0, "accountArr":[Landroid/accounts/Account;
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 346
    const-string/jumbo v5, "PREF_SETTINGS_ACCOUNT_EMAIL"

    const-string/jumbo v6, ""

    .line 345
    invoke-virtual {v4, v5, v6}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    .line 347
    const/4 v1, 0x0

    .line 348
    .local v1, "foundEmail":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    .line 354
    if-eqz v1, :cond_2

    .line 355
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    .line 370
    :goto_1
    return-object v4

    .line 349
    :cond_0
    aget-object v4, v0, v2

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v5, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 350
    const/4 v1, 0x1

    .line 348
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 357
    :cond_2
    sget-object v4, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    if-nez v1, :cond_3

    .line 358
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v4

    .line 359
    const-string/jumbo v5, "PREF_SETTINGS_ACCOUNT_EMAIL"

    .line 358
    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->removeKey(Ljava/lang/String;)V

    .line 361
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v4

    .line 363
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 365
    const v6, 0x7f0a0443

    .line 364
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 366
    sget-object v8, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    aput-object v8, v6, v7

    .line 362
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 360
    invoke-static {v4, v5, v9}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v4

    .line 366
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 367
    const-string/jumbo v4, ""

    sput-object v4, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    .line 370
    :cond_3
    const-string/jumbo v4, ""

    goto :goto_1
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 410
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getSamsungAccountEmailFromAccounts(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 374
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 375
    .local v1, "manager":Landroid/accounts/AccountManager;
    const-string/jumbo v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 376
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 377
    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 379
    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v2, ""

    goto :goto_0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public setEventNotifier(Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;)V
    .locals 2
    .param p1, "event"    # Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;

    .prologue
    .line 332
    const-string/jumbo v0, "[Samsung Account]"

    const-string/jumbo v1, "NOTIF setEventNotifier"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    invoke-direct {v0, p1}, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;-><init>(Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier$SADataNotifier;)V

    sput-object v0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    .line 334
    return-void
.end method
