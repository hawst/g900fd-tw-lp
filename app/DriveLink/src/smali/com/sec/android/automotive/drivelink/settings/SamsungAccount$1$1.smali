.class Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1$1;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "SamsungAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1$1;->this$1:Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1;

    .line 138
    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 163
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "onReceiveAccessToken()"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    if-eqz p2, :cond_4

    .line 166
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "onReceiveAccessToken() SUCCESS"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 174
    const-string/jumbo v2, "access_token"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$1(Ljava/lang/String;)V

    .line 175
    const-string/jumbo v2, "user_id"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$2(Ljava/lang/String;)V

    .line 176
    const-string/jumbo v2, "email_id"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$3(Ljava/lang/String;)V

    .line 177
    const-string/jumbo v2, "birthday"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$4(Ljava/lang/String;)V

    .line 178
    const-string/jumbo v2, "cc"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$5(Ljava/lang/String;)V

    .line 179
    const/16 v2, 0xad2

    if-ne p1, v2, :cond_2

    .line 180
    sget-object v2, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1$1$1;

    invoke-direct {v3, p0, p3}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1$1$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SamsungAccount$1$1;Landroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 190
    # invokes: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$7()Z

    .line 209
    :goto_1
    return-void

    .line 167
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 168
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 169
    .local v1, "value":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 170
    const-string/jumbo v3, "[Samsung Account]"

    const-string/jumbo v4, "%s %s (%s)"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    .line 171
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    .line 172
    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 170
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 191
    .end local v0    # "key":Ljava/lang/String;
    .end local v1    # "value":Ljava/lang/Object;
    :cond_2
    const/16 v2, 0xad7

    if-ne p1, v2, :cond_3

    .line 194
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$8()Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->userId:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$9()Ljava/lang/String;

    move-result-object v3

    .line 195
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->accessToken:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$10()Ljava/lang/String;

    move-result-object v4

    .line 194
    # invokes: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUserNameAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$11(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    .line 197
    :cond_3
    const-string/jumbo v2, "[Samsung Account]"

    .line 198
    const-string/jumbo v3, "Unhandled request id in onReceiveAccessToken."

    .line 197
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 201
    :cond_4
    const-string/jumbo v2, "[Samsung Account]"

    const-string/jumbo v3, "onReceiveAccessToken() FAILED"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$6()Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 203
    const-string/jumbo v2, "[Samsung Account]"

    .line 204
    const-string/jumbo v3, "NOTIF doWork (FAILED IN REQUEST ACCESSS TOKEN)"

    .line 203
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    # getter for: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->eventNotifier:Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$6()Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/sec/android/automotive/drivelink/settings/SAEventNotifier;->doNotify(Landroid/os/Bundle;)V

    .line 207
    :cond_5
    # invokes: Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->RequestUnregisterCallback()Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/settings/SamsungAccount;->access$7()Z

    goto :goto_1
.end method

.method public onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "id"    # Z
    .param p3, "isSuccess"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 158
    return-void
.end method

.method public onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "arg2"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 153
    return-void
.end method

.method public onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "arg2"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 148
    return-void
.end method

.method public onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "arg2"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 143
    return-void
.end method
