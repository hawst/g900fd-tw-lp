.class Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$2;
.super Ljava/lang/Object;
.source "HomeOverlayService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/NotificationCenter$OnNotificationChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    .line 397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewNotification(Z)V
    .locals 2
    .param p1, "animation"    # Z

    .prologue
    .line 401
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "New Notification Received!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    if-eqz v0, :cond_0

    .line 403
    # getter for: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "higher priority noti is received."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->mNotiView:Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->pauseNotiFlow()V

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService$2;->this$0:Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->checkNotification(Z)Z
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;->access$2(Lcom/sec/android/automotive/drivelink/home/HomeOverlayService;Z)Z

    .line 408
    return-void
.end method
