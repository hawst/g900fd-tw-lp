.class public Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CarModeCheckReceiver.java"


# static fields
.field private static final BLOCK_CARMODE_WITH_TALKBACK_ON:I = 0x0

.field public static final CALL_STATE_OFFHOOK:I = 0x2

.field private static final CLOSE_CARMODE_WITH_AIRPLANEMODE_ON:I = 0x2

.field private static final CLOSE_CARMODE_WITH_TALKBACK_ON:I = 0x1

.field private static final TAG:Ljava/lang/String; = "[CarModeCheckReceiver]"


# instance fields
.field DuringCallToastHandlerdler:Landroid/os/Handler;

.field TalkBackToastHandlerdler:Landroid/os/Handler;

.field private isCheckDrivingMode:I

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isCheckDrivingMode:I

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->DuringCallToastHandlerdler:Landroid/os/Handler;

    .line 410
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->TalkBackToastHandlerdler:Landroid/os/Handler;

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V
    .locals 0

    .prologue
    .line 564
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->restoreSoundAndBTState()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;Landroid/bluetooth/BluetoothAdapter;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method public static isAirplaneModeEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 536
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_2

    .line 537
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 538
    const-string/jumbo v3, "airplane_mode_on"

    .line 537
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 540
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 537
    goto :goto_0

    .line 540
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 541
    const-string/jumbo v3, "airplane_mode_on"

    .line 540
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static isTalkBackEnabled(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 506
    const/16 v1, 0x3a

    .line 507
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string/jumbo v0, "com.google.android.marvin.talkback"

    .line 508
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v8, 0x3a

    invoke-direct {v6, v8}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 509
    .local v6, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    if-nez p0, :cond_1

    .line 531
    :cond_0
    :goto_0
    return v7

    .line 512
    :cond_1
    const/4 v5, 0x0

    .line 513
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 514
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "enabled_accessibility_services"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 516
    :cond_2
    if-nez v5, :cond_3

    .line 517
    const-string/jumbo v5, ""

    .line 519
    :cond_3
    move-object v2, v6

    .line 520
    .local v2, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 521
    :cond_4
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 522
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 523
    .local v3, "componentNameString":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 524
    .local v4, "enabledService":Landroid/content/ComponentName;
    if-eqz v4, :cond_4

    .line 525
    const-string/jumbo v8, "com.google.android.marvin.talkback"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 526
    const-string/jumbo v8, "com.google.android.marvin.talkback"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 527
    const-string/jumbo v8, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 528
    const/4 v7, 0x1

    goto :goto_0
.end method

.method private restoreSoundAndBTState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 565
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->restoreRingerMode(Landroid/content/Context;)Z

    .line 594
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 595
    const-string/jumbo v1, "PREF_USER_BT_PREVIOUS_STATE"

    .line 594
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v0

    .line 595
    if-eqz v0, :cond_2

    .line 596
    const-string/jumbo v0, "[CarModeCheckReceiver]"

    const-string/jumbo v1, "PREF_USER_BT_PREVIOUS_STATE true"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 598
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    const-string/jumbo v0, "[CarModeCheckReceiver]"

    const-string/jumbo v1, "BT enabled"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/AutoLaunchReceiver;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-nez v0, :cond_1

    .line 601
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 602
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 603
    const-string/jumbo v1, "PREF_USER_BT_PREVIOUS_STATE"

    .line 602
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 605
    const-string/jumbo v0, "[CarModeCheckReceiver]"

    const-string/jumbo v1, "BT off"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    const-string/jumbo v0, "[CarModeCheckReceiver]"

    const-string/jumbo v1, "BT connected"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 611
    :cond_2
    const-string/jumbo v0, "[CarModeCheckReceiver]"

    const-string/jumbo v1, "PREF_USER_BT_PREVIOUS_STATE false"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public disableCarMode(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 547
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "car_mode_on"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 548
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "car_mode_on"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 549
    :cond_0
    const-string/jumbo v2, "[CarModeCheckReceiver]"

    const-string/jumbo v3, "disableCarMode:change to car_mode_off"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 557
    :goto_0
    const-string/jumbo v2, "[CarModeCheckReceiver]"

    const-string/jumbo v3, "disableCarMode:send broad - carmodechanged => 0"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 559
    .local v1, "presenceIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.carmodechanged"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 560
    const-string/jumbo v2, "car_mode_state"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 561
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 562
    return-void

    .line 550
    .end local v1    # "presenceIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 553
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_1
    move-exception v0

    .line 554
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "[CarModeCheckReceiver]"

    const-string/jumbo v3, "Exception Occured::"

    invoke-static {v2, v3, v0}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 57
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 60
    const-string/jumbo v7, "phone"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    .line 59
    invoke-static {v7}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v4

    .line 64
    .local v4, "mTelephonyService":Lcom/android/internal/telephony/ITelephony;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "car_mode_on"

    .line 63
    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isCheckDrivingMode:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 71
    .local v5, "presenceIntent":Landroid/content/Intent;
    const-string/jumbo v7, "com.sec.android.automotive.drivelink.carmodechanged"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string/jumbo v7, "car_mode_state"

    iget v8, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isCheckDrivingMode:I

    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 73
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "onReceive:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", Driving Mode:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isCheckDrivingMode:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-string/jumbo v7, "android.settings.CAR_MODE_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 81
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "onReceive - check Mode:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isCheckDrivingMode:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isCheckDrivingMode:I

    if-ne v7, v11, :cond_0

    .line 85
    if-eqz v4, :cond_1

    .line 88
    :try_start_1
    invoke-interface {v4}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    move-result v7

    if-eqz v7, :cond_1

    .line 89
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    const-string/jumbo v8, "Car Mode Holding by Call"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->DuringCallToastHandlerdler:Landroid/os/Handler;

    new-instance v8, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;

    invoke-direct {v8, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$2;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 408
    :cond_0
    :goto_1
    return-void

    .line 65
    .end local v0    # "action":Ljava/lang/String;
    .end local v5    # "presenceIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 67
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 121
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v5    # "presenceIntent":Landroid/content/Intent;
    :cond_1
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 122
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    .line 123
    const-string/jumbo v8, "Car Mode could not be launched with talkback on"

    .line 122
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->TalkBackToastHandlerdler:Landroid/os/Handler;

    .line 125
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 149
    :catch_1
    move-exception v1

    .line 151
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 126
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isAirplaneModeEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 127
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    const-string/jumbo v8, "Car Mode could not be launched with Airplane Mode on"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->TalkBackToastHandlerdler:Landroid/os/Handler;

    .line 129
    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 130
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v7}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->disableCarMode(Landroid/content/Context;)V

    goto :goto_1

    .line 133
    :cond_3
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    const-string/jumbo v8, "Car Mode Start"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    const-string/jumbo v8, "block system key!"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 137
    const-string/jumbo v8, "car_mode_blocking_system_key"

    const/4 v9, 0x1

    .line 135
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 138
    new-instance v3, Landroid/content/Intent;

    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 139
    const-class v8, Lcom/sec/android/automotive/drivelink/firstaccess/DisclaimerActivity;

    .line 138
    invoke-direct {v3, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .local v3, "mIntent":Landroid/content/Intent;
    const-string/jumbo v7, "android.intent.action.MAIN"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string/jumbo v7, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const/high16 v7, 0x10000000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 145
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/app/IActivityManager;->resumeAppSwitches()V

    .line 147
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 155
    .end local v3    # "mIntent":Landroid/content/Intent;
    :cond_4
    const-string/jumbo v7, "com.sec.android.automotive.drivelink.CAR_MODE_CHANGED"

    .line 156
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 158
    iget v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->isCheckDrivingMode:I

    if-nez v7, :cond_0

    .line 159
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;

    invoke-direct {v8, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$3;-><init>(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 363
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 365
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v7

    .line 366
    const-string/jumbo v8, "PREF_CAR_APP_RUNNING"

    .line 365
    invoke-virtual {v7, v8, v10}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v7

    .line 366
    if-eqz v7, :cond_5

    .line 367
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 369
    .local v2, "finishintent":Landroid/content/Intent;
    const-string/jumbo v7, "com.sec.android.automotive.drivelink.carmodeoff.finish"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    const-string/jumbo v7, "com.sec.android.automotive.drivelink"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 372
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    .line 373
    const-string/jumbo v8, "Send broadcast for finish activity to baseActivity"

    .line 372
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    .end local v2    # "finishintent":Landroid/content/Intent;
    :goto_2
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v7

    .line 379
    const-string/jumbo v8, "PREF_CAR_APP_RUNNING"

    .line 378
    invoke-virtual {v7, v8, v10}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 380
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    const-string/jumbo v8, "PREF_CAR_APP_RUNNING is set to false"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 384
    .local v6, "taskIntent":Landroid/content/Intent;
    const-string/jumbo v7, "com.sec.android.automotive.drivelink.ACTION_TASK_MANAGER"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 386
    const-string/jumbo v7, "EXTRA_KEY_TASK_MANAGER_STATE"

    .line 385
    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 388
    const-string/jumbo v7, "com.sec.android.automotive.drivelink"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 389
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 390
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    const-string/jumbo v8, "Send broadcast for Voice wakeup off to TaskManager"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    const-string/jumbo v8, "unblock system key!!!"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 403
    const-string/jumbo v8, "car_mode_blocking_system_key"

    .line 401
    invoke-static {v7, v8, v10}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 375
    .end local v6    # "taskIntent":Landroid/content/Intent;
    :cond_5
    const-string/jumbo v7, "[CarModeCheckReceiver]"

    .line 376
    const-string/jumbo v8, "NOT Send finish broadcast to baseActivity. it\'s finish by Backkey"

    .line 375
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
