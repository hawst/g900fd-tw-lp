.class public Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;
.source "MusicFragmentViewForMulti.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mArtistName:Landroid/widget/TextView;

.field private mNextBtn:Landroid/widget/Button;

.field private mPlaypauseBtn:Landroid/widget/Button;

.field private mPrevBtn:Landroid/widget/Button;

.field private mRlLayout:Landroid/widget/RelativeLayout;

.field private mShrinkBtn:Landroid/widget/ImageView;

.field private mSongThumbnail:Landroid/widget/ImageView;

.field private mSongTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;-><init>()V

    return-void
.end method

.method private setMusicInfo()V
    .locals 4

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 114
    .local v1, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 115
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getPlayingMusic()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    move-result-object v1

    .line 117
    :cond_0
    if-nez v1, :cond_1

    .line 133
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mSongTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mArtistName:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;->getArtist()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 122
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 123
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mSongThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 128
    :goto_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 129
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_0

    .line 125
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mSongThumbnail:Landroid/widget/ImageView;

    const v3, 0x7f020183

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mSongThumbnail:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 131
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPlaypauseBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_0
.end method


# virtual methods
.method public init(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 90
    const v0, 0x7f0902a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mRlLayout:Landroid/widget/RelativeLayout;

    .line 91
    const v0, 0x7f0900cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mSongTitle:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0900d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mArtistName:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0900d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mSongThumbnail:Landroid/widget/ImageView;

    .line 94
    const v0, 0x7f0900d8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPlaypauseBtn:Landroid/widget/Button;

    .line 95
    const v0, 0x7f0900d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPrevBtn:Landroid/widget/Button;

    .line 96
    const v0, 0x7f0900d9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mNextBtn:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0902a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mShrinkBtn:Landroid/widget/ImageView;

    .line 99
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPrevBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mNextBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mShrinkBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 55
    .local v0, "id":I
    sparse-switch v0, :sswitch_data_0

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 57
    :sswitch_0
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->showMusicFragment(Z)V

    goto :goto_0

    .line 61
    :sswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 62
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 64
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_0

    .line 66
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->playPlayer()V

    .line 67
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mPlaypauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->setMusicInfo()V

    goto :goto_0

    .line 75
    :sswitch_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 76
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->prevPlayer(Z)V

    .line 77
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->setMusicInfo()V

    goto :goto_0

    .line 80
    :sswitch_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 81
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->nextPlayer()V

    .line 82
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->setMusicInfo()V

    goto :goto_0

    .line 55
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0900d7 -> :sswitch_2
        0x7f0900d8 -> :sswitch_1
        0x7f0900d9 -> :sswitch_3
        0x7f0902a2 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    const v1, 0x7f03009a

    .line 41
    const/4 v2, 0x0

    .line 40
    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 42
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->init(Landroid/view/View;)V

    .line 43
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->setMusicInfo()V

    .line 44
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseFragment;->onDestroy()V

    .line 50
    return-void
.end method

.method public showMusicFragment(Z)V
    .locals 2
    .param p1, "isShow"    # Z

    .prologue
    .line 106
    if-eqz p1, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mRlLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 110
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicFragmentViewForMulti;->mRlLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method
