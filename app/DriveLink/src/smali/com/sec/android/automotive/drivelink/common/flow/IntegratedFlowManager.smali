.class public Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;
.super Ljava/lang/Object;
.source "IntegratedFlowManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "IntegratedFlowManager"

.field private static mContext:Landroid/content/Context;

.field private static mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

.field private static mFlowList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;",
            ">;"
        }
    .end annotation
.end field

.field private static mHomeMode:Ljava/lang/String;

.field private static mInDmFlowChanging:Z

.field private static mIsCanceltoMain:Z

.field private static mIsCanceltoMusicPause:Z

.field private static mIsWakeup:Z

.field private static mNewFlow:Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    sput-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    .line 38
    sput-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mNewFlow:Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 40
    sput-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 42
    const-string/jumbo v0, "HOME_MODE_BASIC"

    sput-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mHomeMode:Ljava/lang/String;

    .line 43
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsWakeup:Z

    .line 45
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mInDmFlowChanging:Z

    .line 47
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsCanceltoMain:Z

    .line 48
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsCanceltoMusicPause:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1()V
    .locals 0

    .prologue
    .line 654
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startListening()V

    return-void
.end method

.method private static addFlowList(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    .locals 4
    .param p0, "flowSet"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    const/4 v1, 0x0

    .line 53
    const/4 v0, 0x0

    .line 55
    .local v0, "newFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    if-nez p0, :cond_0

    .line 56
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mNewFlow:Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 61
    :goto_0
    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    const-string/jumbo v3, "DM_MAIN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 73
    .end local v0    # "newFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    :goto_1
    return-object v0

    .line 58
    .restart local v0    # "newFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    :cond_0
    move-object v0, p0

    goto :goto_0

    .line 70
    :cond_1
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    sput-object v1, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mNewFlow:Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    goto :goto_1
.end method

.method private static clearFlowList()V
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 171
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 581
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getCurrentActiviy()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
    .locals 3

    .prologue
    .line 569
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    if-eqz v0, :cond_0

    .line 570
    const-string/jumbo v0, "IntegratedFlowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getCurrentActiviy : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 571
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 570
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    return-object v0
.end method

.method public static getCurrentFlow()Ljava/lang/String;
    .locals 5

    .prologue
    .line 585
    const/4 v1, 0x0

    .line 587
    .local v1, "string":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 589
    .local v0, "i":I
    if-eqz v0, :cond_0

    .line 590
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    iget-object v1, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    .line 595
    :goto_0
    const-string/jumbo v2, "IntegratedFlowManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getCurrentFlow : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    return-object v1

    .line 592
    :cond_0
    const-string/jumbo v1, "DM_MAIN"

    goto :goto_0
.end method

.method public static getFlowID(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 213
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->getFlowIdByFieldId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getFlowList(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    .locals 3
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 174
    const/4 v0, 0x0

    .line 176
    .local v0, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 184
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 177
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    check-cast v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 179
    .restart local v0    # "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v2, v0

    .line 180
    goto :goto_1

    .line 176
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .locals 2
    .param p0, "FlowID"    # Ljava/lang/String;

    .prologue
    .line 550
    const/4 v0, 0x0

    .line 551
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowList(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v1

    .line 553
    .local v1, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    if-eqz v1, :cond_0

    .line 554
    iget-object v0, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmflowParams:Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .line 557
    :cond_0
    return-object v0
.end method

.method public static getHomeMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 679
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mHomeMode:Ljava/lang/String;

    return-object v0
.end method

.method public static getIsCanceledtoMain()Z
    .locals 1

    .prologue
    .line 407
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsCanceltoMain:Z

    return v0
.end method

.method public static getIsCanceledtoPauseMusic()Z
    .locals 1

    .prologue
    .line 417
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsCanceltoMusicPause:Z

    return v0
.end method

.method public static getIsWakeup()Z
    .locals 1

    .prologue
    .line 397
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsWakeup:Z

    return v0
.end method

.method public static getNewFlowSet()Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mNewFlow:Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    return-object v0
.end method

.method public static getPrevFlowNoDuplicated(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "curFlowID"    # Ljava/lang/String;

    .prologue
    .line 600
    const/4 v1, 0x0

    .line 601
    .local v1, "flowID":Ljava/lang/String;
    const/4 v0, 0x0

    .line 602
    .local v0, "curFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    const/4 v2, 0x0

    .line 604
    .local v2, "prevFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowList(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v0

    .line 606
    if-nez v0, :cond_0

    .line 607
    const-string/jumbo v3, "IntegratedFlowManager"

    const-string/jumbo v4, "getPrevFlow : return - curFlow is null"

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v3, 0x0

    .line 621
    :goto_0
    return-object v3

    .line 611
    :cond_0
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPrevFlowsetNoDuplicated(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v2

    .line 613
    if-eqz v2, :cond_1

    .line 614
    iget-object v1, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    .line 619
    :goto_1
    const-string/jumbo v3, "IntegratedFlowManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "getPrevFlow - flowID :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v1

    .line 621
    goto :goto_0

    .line 616
    :cond_1
    const-string/jumbo v1, "DM_MAIN"

    goto :goto_1
.end method

.method private static getPrevFlowSet(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    .locals 5
    .param p0, "flowset"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    const/4 v2, 0x0

    .line 145
    const/4 v1, 0x0

    .line 148
    .local v1, "prevFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    if-nez p0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-object v2

    .line 152
    :cond_1
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    .line 154
    .local v0, "lastIndex":I
    if-lez v0, :cond_0

    .line 155
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "prevFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    check-cast v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 157
    .restart local v1    # "prevFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    if-eqz v1, :cond_2

    .line 158
    const-string/jumbo v2, "IntegratedFlowManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getPrevFlowSet flowID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move-object v2, v1

    .line 163
    goto :goto_0

    .line 160
    :cond_2
    const-string/jumbo v2, "IntegratedFlowManager"

    const-string/jumbo v3, "getPrevFlowSet is null"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static getPrevFlowsetNoDuplicated(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    .locals 4
    .param p0, "curFlowSet"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    .line 625
    const/4 v0, 0x0

    .line 627
    .local v0, "prevFlowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    if-nez p0, :cond_0

    .line 628
    const-string/jumbo v1, "IntegratedFlowManager"

    .line 629
    const-string/jumbo v2, "getPrevFlowsetNoDuplicated : return - curFlowSet is null"

    .line 628
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    const/4 v1, 0x0

    .line 642
    :goto_0
    return-object v1

    .line 633
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPrevFlowSet(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v0

    .line 635
    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isDuplicatedFlowset(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 636
    const-string/jumbo v1, "IntegratedFlowManager"

    .line 637
    const-string/jumbo v2, "getPrevFlowsetNoDuplicated : Duplicated flow - get Prev"

    .line 636
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPrevFlowsetNoDuplicated(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v0

    .line 641
    :cond_1
    const-string/jumbo v1, "IntegratedFlowManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getPrevFlowsetNoDuplicated - return flowID :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 642
    goto :goto_0
.end method

.method public static getPromptString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 209
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-static {p0, v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->getPrompt(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isDmIdle()Z
    .locals 1

    .prologue
    .line 651
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    return v0
.end method

.method private static isDuplicatedFlowset(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Z
    .locals 4
    .param p0, "curflowset"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    .param p1, "prevFlowSet"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    const/4 v0, 0x1

    .line 94
    if-eqz p0, :cond_2

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 95
    if-eqz p1, :cond_2

    iget-object v1, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 97
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    const-string/jumbo v1, "IntegratedFlowManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isDuplicatedFlowset is true - flowID :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :goto_0
    return v0

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isMsgComposing(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    iget-object v1, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isMsgComposing(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    const-string/jumbo v1, "IntegratedFlowManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isDuplicatedFlowset is true - flowID composing :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 106
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 110
    :cond_1
    iget-object v1, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isNotification(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    const-string/jumbo v1, "IntegratedFlowManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isDuplicatedFlowset is true - prev flowID Noti :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 112
    iget-object v3, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 111
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInDmFlowChanging()Z
    .locals 3

    .prologue
    .line 684
    const-string/jumbo v0, "IntegratedFlowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "isInDmFlowChanging : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mInDmFlowChanging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mInDmFlowChanging:Z

    return v0
.end method

.method private static isMsgComposing(Ljava/lang/String;)Z
    .locals 2
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 123
    .local v0, "isComposing":Z
    const-string/jumbo v1, "DM_SMS_COMPOSE"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 124
    const-string/jumbo v1, "DM_SMS_COMPOSING"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 125
    const-string/jumbo v1, "DM_SMS_COMPLETE"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    const-string/jumbo v1, "DM_SMS_SEND"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    :cond_0
    const/4 v0, 0x1

    .line 131
    :cond_1
    return v0
.end method

.method private static isNotification(Ljava/lang/String;)Z
    .locals 2
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 137
    .local v0, "isNoti":Z
    const-string/jumbo v1, "DM_SMS_READBACK_NOTI"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    const/4 v0, 0x1

    .line 141
    :cond_0
    return v0
.end method

.method private static prepareClientDM(Ljava/lang/String;)V
    .locals 1
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 422
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 424
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Request:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    goto :goto_0
.end method

.method public static removeFlowList(Ljava/lang/String;)V
    .locals 5
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 78
    const-string/jumbo v2, "IntegratedFlowManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "removeFlowList flowID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowList(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v0

    .line 81
    .local v0, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPrevFlowSet(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v1

    .line 83
    .local v1, "prevFlowset":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isDuplicatedFlowset(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    const-string/jumbo v2, "IntegratedFlowManager"

    const-string/jumbo v3, "removeFlowList remove duplicated prev flowSet"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static resetFlowParam(Ljava/lang/String;)V
    .locals 4
    .param p0, "flowID"    # Ljava/lang/String;

    .prologue
    .line 188
    const/4 v0, 0x0

    .line 190
    .local v0, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 197
    return-void

    .line 191
    :cond_0
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mFlowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    check-cast v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 193
    .restart local v0    # "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 194
    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmflowParams:Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    .line 190
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static resetMainFlow(Ljava/lang/String;)V
    .locals 2
    .param p0, "homeMode"    # Ljava/lang/String;

    .prologue
    .line 462
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;-><init>()V

    .line 463
    .local v0, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    const-string/jumbo v1, "DM_MAIN"

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    .line 464
    const/16 v1, 0x20

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mFlowDrivenType:I

    .line 466
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setHomeMode(Ljava/lang/String;)V

    .line 468
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->clearFlowList()V

    .line 469
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->addFlowList(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 470
    return-void
.end method

.method private static sendPrepareFlowChange(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V
    .locals 3
    .param p0, "flowSet"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    .line 473
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v0

    .line 475
    .local v0, "curFlowID":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 476
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentActiviy()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 477
    .local v1, "currentActivity":Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
    if-eqz v1, :cond_0

    .line 478
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPrepareFlowChange(Ljava/lang/String;)V

    .line 481
    .end local v1    # "currentActivity":Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
    :cond_0
    return-void
.end method

.method private static setClientDM(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V
    .locals 5
    .param p0, "flowSet"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    .line 486
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFlowMap:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 488
    .local v1, "targetActivity":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v1, :cond_1

    .line 490
    const-string/jumbo v2, "IntegratedFlowManager"

    .line 491
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[setClientDM] activity :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 492
    const-string/jumbo v4, " FlowID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 491
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 490
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    new-instance v0, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 495
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x14000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 497
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 498
    const-string/jumbo v2, "EXTRA_FROM_FLOW_MGR"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 499
    const-string/jumbo v2, "EXTRA_DRIVEN_TYPE"

    .line 500
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mFlowDrivenType:I

    .line 499
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 501
    const-string/jumbo v2, "EXTRA_FLOW_ID"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 505
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    const-string/jumbo v3, "DM_DIAL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 506
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 507
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    .line 509
    const/4 v3, 0x2

    .line 508
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->onPlaybackRequest(I)V

    .line 516
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    const-string/jumbo v2, "IntegratedFlowManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[setClientDM] Canceled - FlowID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static setClientDMForMulti(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V
    .locals 5
    .param p0, "flowSet"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    .line 526
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFlowMap:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 528
    .local v1, "targetActivity":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v1, :cond_0

    .line 530
    const-string/jumbo v2, "IntegratedFlowManager"

    .line 531
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[setClientDM] activity :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 532
    const-string/jumbo v4, " FlowID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 531
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 530
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    new-instance v0, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 535
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10030000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 538
    const-string/jumbo v2, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 539
    const-string/jumbo v2, "EXTRA_FROM_FLOW_MGR"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 540
    const-string/jumbo v2, "EXTRA_DRIVEN_TYPE"

    .line 541
    iget v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mFlowDrivenType:I

    .line 540
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 542
    const-string/jumbo v2, "EXTRA_FLOW_ID"

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 544
    sget-object v2, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 547
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 577
    sput-object p0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    .line 578
    return-void
.end method

.method public static setCurrentActiviy(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;Landroid/content/Context;)V
    .locals 0
    .param p0, "activity"    # Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 561
    sput-object p0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mCurrentActivity:Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .line 562
    if-eqz p0, :cond_0

    .line 563
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setContext(Landroid/content/Context;)V

    .line 566
    :goto_0
    return-void

    .line 565
    :cond_0
    invoke-static {p1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setContext(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static setExitNavi()V
    .locals 2

    .prologue
    .line 711
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v0

    .line 712
    .local v0, "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    sget-object v1, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->exitNavigation(Landroid/content/Context;)V

    .line 713
    return-void
.end method

.method public static setFlowCommand(Ljava/lang/String;)V
    .locals 6
    .param p0, "command"    # Ljava/lang/String;

    .prologue
    .line 434
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmCommandMap:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 435
    .local v2, "targetActivity":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmCommandFlowChanging:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 437
    .local v1, "isDmFlowChange":Z
    const-string/jumbo v3, "IntegratedFlowManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[setFlowCommand] command :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    if-nez v2, :cond_0

    .line 440
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentActiviy()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 443
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 444
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v3, 0x30000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 446
    const-string/jumbo v3, "com.sec.android.automotive.drivelink.ACTION_DL_DM_FLOW_COMMAND"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const-string/jumbo v3, "EXTRA_FLOW_COMMAND"

    invoke-virtual {v0, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    const-string/jumbo v3, "EXTRA_FLOW_COMMAND_FLOWID"

    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    const-class v3, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 451
    const-string/jumbo v3, "com.sec.android.automotive.drivelink"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 456
    :goto_0
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 457
    sget-object v3, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 459
    return-void

    .line 453
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static setHomeMode(Ljava/lang/String;)V
    .locals 3
    .param p0, "mode"    # Ljava/lang/String;

    .prologue
    .line 667
    sput-object p0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mHomeMode:Ljava/lang/String;

    .line 669
    const-string/jumbo v0, "HOME_MODE_BASIC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 670
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFlowMap:Ljava/util/HashMap;

    const-string/jumbo v1, "DM_MAIN"

    .line 671
    const-class v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    .line 670
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 672
    :cond_1
    const-string/jumbo v0, "HOME_MODE_MULTIWINDOW"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    sget-object v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->DmFlowMap:Ljava/util/HashMap;

    const-string/jumbo v1, "DM_MAIN"

    .line 674
    const-class v2, Lcom/sec/android/automotive/drivelink/multiwindow/MultiWindowActivity;

    .line 673
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static setInDmFlowChanging(Z)V
    .locals 3
    .param p0, "inDmFlowChanging"    # Z

    .prologue
    .line 690
    const-string/jumbo v0, "IntegratedFlowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setInDmFlowChanging : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mInDmFlowChanging:Z

    .line 692
    return-void
.end method

.method public static setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V
    .locals 4
    .param p0, "flowID"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .prologue
    .line 217
    const-string/jumbo v1, "IntegratedFlowManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[setIntegratedFlow] FlowID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;-><init>()V

    .line 221
    .local v0, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    if-nez p1, :cond_0

    .line 222
    new-instance p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .end local p1    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-direct {p1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 225
    .restart local p1    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    iput-object p0, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    .line 226
    const/16 v1, 0x20

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mFlowDrivenType:I

    .line 227
    iput-object p1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmflowParams:Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .line 229
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->sendPrepareFlowChange(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V

    .line 231
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setClientDM(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V

    .line 233
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->addFlowList(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 234
    return-void
.end method

.method public static setIntegratedFlowForMulti(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V
    .locals 4
    .param p0, "flowID"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .prologue
    .line 242
    const-string/jumbo v1, "IntegratedFlowManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[setIntegratedFlow] FlowID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;-><init>()V

    .line 246
    .local v0, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    if-nez p1, :cond_0

    .line 247
    new-instance p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .end local p1    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-direct {p1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 250
    .restart local p1    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    iput-object p0, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    .line 251
    const/16 v1, 0x20

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mFlowDrivenType:I

    .line 252
    iput-object p1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmflowParams:Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .line 254
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->sendPrepareFlowChange(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V

    .line 259
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setClientDMForMulti(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V

    .line 262
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->addFlowList(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 263
    return-void
.end method

.method public static setIsCanceledtoMain(Z)V
    .locals 3
    .param p0, "isCanceltoMain"    # Z

    .prologue
    .line 401
    const-string/jumbo v0, "IntegratedFlowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[setIsCanceledtoMain] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsCanceltoMain:Z

    .line 403
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 404
    return-void
.end method

.method public static setIsCanceledtoPauseMusic(Z)V
    .locals 3
    .param p0, "isCanceltoMain"    # Z

    .prologue
    .line 412
    const-string/jumbo v0, "IntegratedFlowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[setIsCanceledtoPauseMusic] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsCanceltoMusicPause:Z

    .line 414
    return-void
.end method

.method public static setIsWakeup(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isWakeup"    # Z

    .prologue
    .line 391
    const-string/jumbo v0, "IntegratedFlowManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[setIsWakeup] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    sput-boolean p1, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsWakeup:Z

    .line 394
    return-void
.end method

.method public static setNewFlowSet(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)V
    .locals 0
    .param p0, "flowSet"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .prologue
    .line 204
    sput-object p0, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mNewFlow:Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 205
    return-void
.end method

.method public static startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V
    .locals 12
    .param p0, "flowID"    # Ljava/lang/String;
    .param p1, "flowParams"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .prologue
    const/4 v11, 0x0

    .line 266
    const-string/jumbo v8, "IntegratedFlowManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "[startIntegratedFlow] flowID : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const/4 v6, 0x0

    .line 269
    .local v6, "prompt":Ljava/lang/String;
    const/4 v1, 0x0

    .line 271
    .local v1, "defaultPrompt":Ljava/lang/String;
    if-eqz p1, :cond_6

    iget-object v8, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    if-eqz v8, :cond_6

    .line 272
    const-string/jumbo v8, "DM_SMS_INBOX"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 273
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPromptString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 281
    :cond_0
    :goto_0
    if-eqz p1, :cond_8

    iget-object v8, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    if-eqz v8, :cond_8

    .line 282
    const-string/jumbo v8, "DM_SMS_INBOX"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 283
    iget-object v6, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    .line 291
    :cond_1
    :goto_1
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->prepareClientDM(Ljava/lang/String;)V

    .line 293
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->getFiledIdByFlowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 295
    .local v2, "fieldID":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowList(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    move-result-object v8

    if-nez v8, :cond_3

    .line 296
    if-nez p1, :cond_2

    .line 297
    new-instance p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .end local p1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-direct {p1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 299
    .restart local p1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_2
    new-instance v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    invoke-direct {v3}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;-><init>()V

    .line 301
    .local v3, "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    iput-object p0, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmFlow:Ljava/lang/String;

    .line 302
    const/16 v8, 0x30

    iput v8, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mFlowDrivenType:I

    .line 303
    iput-object p1, v3, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->mDmflowParams:Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    .line 304
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->addFlowList(Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;)Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;

    .line 307
    .end local v3    # "flowSet":Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;
    :cond_3
    sget-boolean v8, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsWakeup:Z

    if-eqz v8, :cond_4

    .line 308
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v8

    invoke-virtual {v8}, Lcom/nuance/drivelink/DLAppUiUpdater;->getLastMicState()Lcom/nuance/sample/MicState;

    move-result-object v5

    .line 310
    .local v5, "micState":Lcom/nuance/sample/MicState;
    sget-object v8, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    if-eq v5, v8, :cond_9

    .line 311
    const-string/jumbo v8, "IntegratedFlowManager"

    .line 312
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Wakeup DM Cancel, Dialog flow was :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 313
    invoke-virtual {v5}, Lcom/nuance/sample/MicState;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 312
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 311
    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    sget-object v8, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    invoke-static {v8, v11}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsWakeup(Landroid/content/Context;Z)V

    .line 315
    const/4 v6, 0x0

    .line 328
    .end local v5    # "micState":Lcom/nuance/sample/MicState;
    :cond_4
    if-eqz v6, :cond_b

    .line 330
    const-string/jumbo v8, "DM_LOCATION"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 331
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 332
    .local v4, "h":Landroid/os/Handler;
    new-instance v7, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager$1;

    invoke-direct {v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager$1;-><init>()V

    .line 352
    .local v7, "r":Ljava/lang/Runnable;
    const-wide/16 v8, 0xa

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 386
    .end local v4    # "h":Landroid/os/Handler;
    .end local v7    # "r":Ljava/lang/Runnable;
    :goto_2
    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 387
    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoPauseMusic(Z)V

    .line 388
    :goto_3
    return-void

    .line 275
    .end local v2    # "fieldID":Ljava/lang/String;
    :cond_5
    iget-object v1, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 277
    goto/16 :goto_0

    :cond_6
    if-eqz p1, :cond_0

    iget-object v8, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 278
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPromptString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 285
    :cond_7
    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, ". "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 287
    goto/16 :goto_1

    :cond_8
    if-eqz p1, :cond_1

    iget-object v8, p1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    if-nez v8, :cond_1

    .line 288
    move-object v6, v1

    goto/16 :goto_1

    .line 318
    .restart local v2    # "fieldID":Ljava/lang/String;
    .restart local v5    # "micState":Lcom/nuance/sample/MicState;
    :cond_9
    const-string/jumbo v8, "IntegratedFlowManager"

    const-string/jumbo v9, "Wakeup DM Starting, Dialog flow was Idle"

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 321
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v8

    .line 322
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v9

    .line 321
    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 323
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startListening()V

    goto :goto_3

    .line 354
    .end local v5    # "micState":Lcom/nuance/sample/MicState;
    :cond_a
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 355
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/nuance/drivelink/DLAppUiUpdater;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 357
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v8

    .line 358
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v9

    .line 357
    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 359
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startListening()V

    goto :goto_2

    .line 363
    :cond_b
    const-string/jumbo v8, "DM_MAIN"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    sget-boolean v8, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mIsCanceltoMain:Z

    if-eqz v8, :cond_c

    .line 364
    const-string/jumbo v8, "IntegratedFlowManager"

    const-string/jumbo v9, "mIsCanceltoMain is TRUE"

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v8

    .line 368
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v9

    .line 367
    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 370
    sget-object v8, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->mContext:Landroid/content/Context;

    const v9, 0x7f0a01f6

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 371
    .local v0, "cancelTTString":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v8

    invoke-interface {v8, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 372
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->displayError(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 375
    .end local v0    # "cancelTTString":Ljava/lang/String;
    :cond_c
    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 376
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v8

    .line 377
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v9

    .line 376
    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 378
    const-string/jumbo v8, "DLPhraseSpotter"

    const-string/jumbo v9, "[start] : IntegratedFlowManager - startDmFlow()"

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v8

    .line 381
    const-wide/16 v9, 0x258

    invoke-virtual {v8, v9, v10}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto/16 :goto_2
.end method

.method public static startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 695
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 696
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 698
    const-string/jumbo v0, "IntegratedFlowManager"

    const-string/jumbo v1, "Activity.startInternalActivity"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 707
    :goto_0
    invoke-static {}, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->resetRepeated()V

    .line 708
    return-void

    .line 703
    .restart local p0    # "context":Landroid/content/Context;
    :cond_0
    const-string/jumbo v0, "IntegratedFlowManager"

    const-string/jumbo v1, "Context.startInternalActivity"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static startListening()V
    .locals 4

    .prologue
    .line 655
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 656
    const-string/jumbo v2, "[stop] : IntegratedFlowManager - startListening()"

    .line 655
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 658
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v0

    .line 659
    .local v0, "success":Z
    const-string/jumbo v1, "IntegratedFlowManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "startUserFlow returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    if-nez v0, :cond_0

    .line 662
    const-string/jumbo v1, "startListening"

    const-string/jumbo v2, "startUserFlow failed"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    :cond_0
    return-void
.end method

.method public static stopDmFlow()V
    .locals 1

    .prologue
    .line 646
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 647
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 648
    return-void
.end method
