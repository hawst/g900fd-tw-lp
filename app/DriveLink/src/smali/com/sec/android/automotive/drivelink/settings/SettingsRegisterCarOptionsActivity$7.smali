.class Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;
.super Ljava/lang/Object;
.source "SettingsRegisterCarOptionsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->showRenameDialog(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

.field private final synthetic val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 314
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    .line 315
    # invokes: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->refreshDeviceNameTextView()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;->val$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 318
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/widget/EditText;

    move-result-object v1

    .line 319
    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 317
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 320
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;->this$0:Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Z)V

    .line 321
    return-void
.end method
