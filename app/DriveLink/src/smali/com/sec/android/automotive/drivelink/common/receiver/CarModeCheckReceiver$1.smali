.class Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;
.super Landroid/os/Handler;
.source "CarModeCheckReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    .line 410
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0xbb8

    .line 412
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_1

    .line 414
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 415
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 416
    const v3, 0x7f0a04ac

    .line 415
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 413
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 417
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 419
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 420
    const-string/jumbo v2, "car_mode_on"

    .line 419
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 421
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 422
    const-string/jumbo v2, "car_mode_on"

    const/4 v3, 0x0

    .line 421
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 424
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 425
    const-string/jumbo v2, "vibrate_when_ringing"

    .line 426
    sget v3, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->VIBRATE_WHEN_RINGING:I

    .line 424
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 428
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 431
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v5, :cond_2

    .line 433
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 434
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 435
    const v3, 0x7f0a04ad

    .line 434
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 432
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 436
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 437
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 439
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v1, v1, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    .line 440
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver$1;->this$0:Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;

    iget-object v2, v2, Lcom/sec/android/automotive/drivelink/common/receiver/CarModeCheckReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 441
    const v3, 0x7f0a04ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 438
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 441
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
