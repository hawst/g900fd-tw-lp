.class public Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationSendRequestDialogView.java"


# static fields
.field private static dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mImageView:Landroid/widget/ImageView;

.field protected mViewLocal:Landroid/view/View;

.field private tvNotification:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mContext:Landroid/content/Context;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mContext:Landroid/content/Context;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mContext:Landroid/content/Context;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 25
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 27
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->initIndicatorView(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v11, 0x7f02023b

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 50
    if-nez p1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mContext:Landroid/content/Context;

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 56
    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 55
    check-cast v0, Landroid/view/LayoutInflater;

    .line 58
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mViewLocal:Landroid/view/View;

    if-nez v5, :cond_2

    .line 60
    const v5, 0x7f0300b1

    .line 59
    invoke-virtual {v0, v5, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mViewLocal:Landroid/view/View;

    .line 62
    const v5, 0x7f0902eb

    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 63
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->tvNotification:Landroid/widget/TextView;

    .line 64
    const v6, 0x7f0a03a1

    new-array v7, v10, [Ljava/lang/Object;

    .line 65
    sget-object v8, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    .line 63
    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mViewLocal:Landroid/view/View;

    const v6, 0x7f0902cd

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mImageView:Landroid/widget/ImageView;

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 71
    const v6, 0x7f020238

    .line 69
    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 73
    .local v4, "shareBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 72
    invoke-static {v5, v4, v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 75
    .local v1, "maskedBitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_1

    .line 76
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 77
    const v6, 0x7f02023d

    .line 76
    invoke-static {v5, v6, v11}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageMasking(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 80
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 81
    const v6, 0x7f02023c

    .line 80
    invoke-static {v5, v1, v6}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->imageOverlay(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 83
    iget-object v5, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 85
    const v6, 0x7f0a05f1

    .line 84
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "prompt":Ljava/lang/String;
    new-array v6, v10, [Ljava/lang/Object;

    .line 87
    sget-object v5, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    if-eqz v5, :cond_3

    sget-object v5, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    :goto_1
    aput-object v5, v6, v9

    .line 86
    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "promptDefault":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 91
    .end local v1    # "maskedBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "prompt":Ljava/lang/String;
    .end local v3    # "promptDefault":Ljava/lang/String;
    .end local v4    # "shareBitmap":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p0, v10}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->setUseAutoTimeout(Z)V

    goto/16 :goto_0

    .line 87
    .restart local v1    # "maskedBitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "prompt":Ljava/lang/String;
    .restart local v4    # "shareBitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_1
.end method

.method public static setContact(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V
    .locals 0
    .param p0, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    .line 138
    sput-object p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 139
    return-void
.end method


# virtual methods
.method protected getTimeout()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x2

    return v0
.end method

.method public onNotificationDisappeared()V
    .locals 2

    .prologue
    .line 131
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationDisappeared()V

    .line 132
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->dismiss()V

    .line 133
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 134
    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->dlContact:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->onNotifyLocationRequestWaiting(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)V

    .line 135
    return-void
.end method

.method public onNotificationWillAppear()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;->onNotificationWillAppear()V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getFlowState()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;->RESTART:Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem$NotiFlowState;

    if-ne v0, v1, :cond_1

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->restartNotiFlow()V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->isAppeared()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->startDefaultNotiTimeoutFlow()V

    .line 125
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->setAppeared(Z)V

    goto :goto_0
.end method

.method protected startDefaultNotiTimeoutFlow()V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addBegin()V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getTTSText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTTS(Ljava/lang/String;)V

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getTimeout()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addTimeout(Ljava/lang/Integer;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->addFinish()V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationLocationSendRequestDialogView;->getNotiItem()Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;->cmdFlowStart()V

    .line 109
    :cond_1
    return-void
.end method
