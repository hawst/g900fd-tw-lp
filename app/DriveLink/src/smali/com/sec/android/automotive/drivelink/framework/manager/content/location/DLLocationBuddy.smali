.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;
.super Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
.source "DLLocationBuddy.java"


# static fields
.field private static final serialVersionUID:J = 0x7938caf3a52579e4L


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mBirthday:Ljava/lang/String;

.field private mCountryCode:Ljava/lang/String;

.field private mEmail:Ljava/lang/String;

.field private mExpiredTime:Ljava/lang/String;

.field private mGender:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mPushId:Ljava/lang/String;

.field private mSspGuid:Ljava/lang/String;

.field private mUpdateUrl:Ljava/lang/String;

.field private mUserId:I

.field private mUserImgUrl:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUpdateUrl:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserName:Ljava/lang/String;

    .line 11
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserId:I

    .line 12
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mCountryCode:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mGender:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mBirthday:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mEmail:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserImgUrl:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mAccessToken:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mExpiredTime:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mPushId:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mModel:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mSspGuid:Ljava/lang/String;

    .line 25
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUpdateUrl:Ljava/lang/String;

    .line 26
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserName:Ljava/lang/String;

    .line 27
    iput v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserId:I

    .line 28
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mCountryCode:Ljava/lang/String;

    .line 29
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mGender:Ljava/lang/String;

    .line 30
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mBirthday:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mEmail:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getBirthday()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mBirthday:Ljava/lang/String;

    return-object v0
.end method

.method public getContactDiplayName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 37
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 38
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 39
    :cond_0
    const-string/jumbo v1, ""

    .line 42
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiredTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mExpiredTime:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mGender:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 5

    .prologue
    .line 46
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "phoneNumber":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 57
    .end local v2    # "phoneNumber":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 50
    .restart local v2    # "phoneNumber":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->getContact()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    move-result-object v0

    .line 51
    .local v0, "contact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 52
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_2

    .line 53
    :cond_1
    const-string/jumbo v2, ""

    goto :goto_0

    .line 56
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 57
    .local v1, "phone":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getPushId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mPushId:Ljava/lang/String;

    return-object v0
.end method

.method public getSspGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mSspGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUpdateUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserId:I

    return v0
.end method

.method public getUserImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public setAccessToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "mAccessToken"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mAccessToken:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setBirthday(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBirthday"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mBirthday:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setCountryCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCountryCode"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mCountryCode:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEmail"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mEmail:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setExpiredTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "mExpiredTime"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mExpiredTime:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setGender(Ljava/lang/String;)V
    .locals 0
    .param p1, "mGender"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mGender:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "mModel"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mModel:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setPushId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPushId"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mPushId:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public setSspGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSspGuid"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mSspGuid:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public setUpdateUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUpdateUrl"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUpdateUrl:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "mUserId"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserId:I

    .line 82
    return-void
.end method

.method public setUserImgUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserImgUrl"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserImgUrl:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUserName"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationBuddy;->mUserName:Ljava/lang/String;

    .line 74
    return-void
.end method
