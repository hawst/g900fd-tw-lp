.class public interface abstract Lcom/sec/android/automotive/drivelink/music/OnVoiceMusicPlayerActionBarListener;
.super Ljava/lang/Object;
.source "OnVoiceMusicPlayerActionBarListener.java"


# static fields
.field public static final MIC_STATE_IDLE:I = 0x0

.field public static final MIC_STATE_LISTENING:I = 0x1

.field public static final MIC_STATE_THINKING:I = 0x2

.field public static final PHRASE_SPOTTER_STATE:I = 0x1

.field public static final SPOTTER_STATE_NONE_SPOTTING:I = 0x0

.field public static final SPOTTER_STATE_SPOTTING:I = 0x1

.field public static final UPDATE_MIC_STATE:I


# virtual methods
.method public abstract onVoiceMusicPlayerActionBarUpdate(II)V
.end method
