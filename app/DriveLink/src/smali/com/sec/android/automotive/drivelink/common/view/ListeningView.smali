.class public Lcom/sec/android/automotive/drivelink/common/view/ListeningView;
.super Landroid/view/View;
.source "ListeningView.java"


# static fields
.field private static final BASE_DENSITY:F = 3.0f


# instance fields
.field frameMultiplier:I

.field mContext:Landroid/content/Context;

.field mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

.field mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

.field mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

.field mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

.field mCueRect4:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

.field private mDensityRatio:F

.field mHeight:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field mRect0Anchors1:[[F

.field mRect0Anchors2:[[F

.field mRect0Points:[[F

.field mRect1Anchors1:[[F

.field mRect1Anchors2:[[F

.field mRect1Points:[[F

.field mRect2Anchors1:[[F

.field mRect2Anchors2:[[F

.field mRect2Points:[[F

.field mRect3Anchors1:[[F

.field mRect3Anchors2:[[F

.field mRect3Points:[[F

.field mRect4Anchors1:[[F

.field mRect4Anchors2:[[F

.field mRect4Points:[[F

.field mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    iput v3, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    .line 32
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    .line 33
    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 34
    new-array v2, v3, [F

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect4Points:[[F

    .line 35
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v5

    .line 36
    new-array v1, v3, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_a

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_b

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 37
    new-array v2, v3, [F

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect4Anchors1:[[F

    .line 38
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_d

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_e

    aput-object v1, v0, v5

    .line 39
    new-array v1, v3, [F

    fill-array-data v1, :array_f

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_10

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_11

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 40
    new-array v2, v3, [F

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect4Anchors2:[[F

    .line 42
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_13

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_14

    aput-object v1, v0, v5

    .line 43
    new-array v1, v3, [F

    fill-array-data v1, :array_15

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_16

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_17

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 44
    new-array v2, v3, [F

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect3Points:[[F

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_1a

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_1b

    aput-object v1, v0, v5

    .line 46
    new-array v1, v3, [F

    fill-array-data v1, :array_1c

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_1d

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_1e

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 47
    new-array v2, v3, [F

    fill-array-data v2, :array_1f

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect3Anchors1:[[F

    .line 48
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_20

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_21

    aput-object v1, v0, v5

    .line 49
    new-array v1, v3, [F

    fill-array-data v1, :array_22

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_23

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_24

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 50
    new-array v2, v3, [F

    fill-array-data v2, :array_25

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect3Anchors2:[[F

    .line 52
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_26

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_27

    aput-object v1, v0, v5

    .line 53
    new-array v1, v3, [F

    fill-array-data v1, :array_28

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_29

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_2a

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 54
    new-array v2, v3, [F

    fill-array-data v2, :array_2b

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_2c

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect2Points:[[F

    .line 55
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_2d

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_2e

    aput-object v1, v0, v5

    .line 56
    new-array v1, v3, [F

    fill-array-data v1, :array_2f

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_30

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_31

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 57
    new-array v2, v3, [F

    fill-array-data v2, :array_32

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect2Anchors1:[[F

    .line 58
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_33

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_34

    aput-object v1, v0, v5

    .line 59
    new-array v1, v3, [F

    fill-array-data v1, :array_35

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_36

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_37

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 60
    new-array v2, v3, [F

    fill-array-data v2, :array_38

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect2Anchors2:[[F

    .line 62
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_39

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_3a

    aput-object v1, v0, v5

    .line 63
    new-array v1, v3, [F

    fill-array-data v1, :array_3b

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_3c

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_3d

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 64
    new-array v2, v3, [F

    fill-array-data v2, :array_3e

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_3f

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect1Points:[[F

    .line 65
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_40

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_41

    aput-object v1, v0, v5

    .line 66
    new-array v1, v3, [F

    fill-array-data v1, :array_42

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_43

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_44

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 67
    new-array v2, v3, [F

    fill-array-data v2, :array_45

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect1Anchors1:[[F

    .line 68
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_46

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_47

    aput-object v1, v0, v5

    .line 69
    new-array v1, v3, [F

    fill-array-data v1, :array_48

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_49

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_4a

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 70
    new-array v2, v3, [F

    fill-array-data v2, :array_4b

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect1Anchors2:[[F

    .line 72
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_4c

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_4d

    aput-object v1, v0, v5

    .line 73
    new-array v1, v3, [F

    fill-array-data v1, :array_4e

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_4f

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_50

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 74
    new-array v2, v3, [F

    fill-array-data v2, :array_51

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_52

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect0Points:[[F

    .line 75
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_53

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_54

    aput-object v1, v0, v5

    .line 76
    new-array v1, v3, [F

    fill-array-data v1, :array_55

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_56

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_57

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 77
    new-array v2, v3, [F

    fill-array-data v2, :array_58

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect0Anchors1:[[F

    .line 78
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_59

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_5a

    aput-object v1, v0, v5

    .line 79
    new-array v1, v3, [F

    fill-array-data v1, :array_5b

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_5c

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_5d

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 80
    new-array v2, v3, [F

    fill-array-data v2, :array_5e

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect0Anchors2:[[F

    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->init(Landroid/content/Context;)V

    .line 85
    return-void

    .line 32
    :array_0
    .array-data 4
        0x43538000    # 211.5f
        0x42c10000    # 96.5f
    .end array-data

    :array_1
    .array-data 4
        0x43d3cccd    # 423.6f
        0x42c10000    # 96.5f
    .end array-data

    .line 33
    :array_2
    .array-data 4
        0x4401e666    # 519.6f
        0x43408000    # 192.5f
    .end array-data

    :array_3
    .array-data 4
        0x43d3cccd    # 423.6f
        0x43904000    # 288.5f
    .end array-data

    :array_4
    .array-data 4
        0x43538000    # 211.5f
        0x43904000    # 288.5f
    .end array-data

    .line 34
    :array_5
    .array-data 4
        0x42e6cccd    # 115.4f
        0x43408000    # 192.5f
    .end array-data

    :array_6
    .array-data 4
        0x43538000    # 211.5f
        0x42c10000    # 96.5f
    .end array-data

    .line 35
    :array_7
    .array-data 4
        0x43d3cccd    # 423.6f
        0x42c10000    # 96.5f
    .end array-data

    :array_8
    .array-data 4
        0x43ee4ccd    # 476.6f
        0x42c10000    # 96.5f
    .end array-data

    .line 36
    :array_9
    .array-data 4
        0x4401e666    # 519.6f
        0x43758000    # 245.5f
    .end array-data

    :array_a
    .array-data 4
        0x43538000    # 211.5f
        0x43904000    # 288.5f
    .end array-data

    :array_b
    .array-data 4
        0x431e6666    # 158.4f
        0x43904000    # 288.5f
    .end array-data

    .line 37
    :array_c
    .array-data 4
        0x42e6cccd    # 115.4f
        0x430b8000    # 139.5f
    .end array-data

    .line 38
    :array_d
    .array-data 4
        0x43d3cccd    # 423.6f
        0x42c10000    # 96.5f
    .end array-data

    :array_e
    .array-data 4
        0x4401e666    # 519.6f
        0x430b8000    # 139.5f
    .end array-data

    .line 39
    :array_f
    .array-data 4
        0x43ee4ccd    # 476.6f
        0x43904000    # 288.5f
    .end array-data

    :array_10
    .array-data 4
        0x43538000    # 211.5f
        0x43904000    # 288.5f
    .end array-data

    :array_11
    .array-data 4
        0x42e6cccd    # 115.4f
        0x43758000    # 245.5f
    .end array-data

    .line 40
    :array_12
    .array-data 4
        0x431e6666    # 158.4f
        0x42c10000    # 96.5f
    .end array-data

    .line 42
    :array_13
    .array-data 4
        0x43d5c000    # 427.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_14
    .array-data 4
        0x434f8000    # 207.5f
        0x439c0000    # 312.0f
    .end array-data

    .line 43
    :array_15
    .array-data 4
        0x42b00000    # 88.0f
        0x43408000    # 192.5f
    .end array-data

    :array_16
    .array-data 4
        0x434f8000    # 207.5f
        0x42920000    # 73.0f
    .end array-data

    :array_17
    .array-data 4
        0x43d5c000    # 427.5f
        0x42920000    # 73.0f
    .end array-data

    .line 44
    :array_18
    .array-data 4
        0x4408c000    # 547.0f
        0x43408000    # 192.5f
    .end array-data

    :array_19
    .array-data 4
        0x43d5c000    # 427.5f
        0x439c0000    # 312.0f
    .end array-data

    .line 45
    :array_1a
    .array-data 4
        0x434f8000    # 207.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_1b
    .array-data 4
        0x430d8000    # 141.5f
        0x439c0000    # 312.0f
    .end array-data

    .line 46
    :array_1c
    .array-data 4
        0x42b00000    # 88.0f
        0x42fd0000    # 126.5f
    .end array-data

    :array_1d
    .array-data 4
        0x43d5c000    # 427.5f
        0x42920000    # 73.0f
    .end array-data

    :array_1e
    .array-data 4
        0x43f6c000    # 493.5f
        0x42920000    # 73.0f
    .end array-data

    .line 47
    :array_1f
    .array-data 4
        0x4408c000    # 547.0f
        0x4383f333    # 263.9f
    .end array-data

    .line 48
    :array_20
    .array-data 4
        0x434f8000    # 207.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_21
    .array-data 4
        0x42b00000    # 88.0f
        0x4381b333    # 259.4f
    .end array-data

    .line 49
    :array_22
    .array-data 4
        0x430d8000    # 141.5f
        0x42920000    # 73.0f
    .end array-data

    :array_23
    .array-data 4
        0x43d5c000    # 427.5f
        0x42920000    # 73.0f
    .end array-data

    :array_24
    .array-data 4
        0x4408c000    # 547.0f
        0x42fd0000    # 126.5f
    .end array-data

    .line 50
    :array_25
    .array-data 4
        0x43f6c000    # 493.5f
        0x439c0000    # 312.0f
    .end array-data

    .line 52
    :array_26
    .array-data 4
        0x44106000    # 577.5f
        0x43408000    # 192.5f
    .end array-data

    :array_27
    .array-data 4
        0x43dac000    # 437.5f
        0x423e0000    # 47.5f
    .end array-data

    .line 53
    :array_28
    .array-data 4
        0x43458000    # 197.5f
        0x423e0000    # 47.5f
    .end array-data

    :array_29
    .array-data 4
        0x42660000    # 57.5f
        0x43408000    # 192.5f
    .end array-data

    :array_2a
    .array-data 4
        0x43458000    # 197.5f
        0x43a8c000    # 337.5f
    .end array-data

    .line 54
    :array_2b
    .array-data 4
        0x43dac000    # 437.5f
        0x43a8c000    # 337.5f
    .end array-data

    :array_2c
    .array-data 4
        0x44106000    # 577.5f
        0x43408000    # 192.5f
    .end array-data

    .line 55
    :array_2d
    .array-data 4
        0x44106000    # 577.5f
        0x42e66666    # 115.2f
    .end array-data

    :array_2e
    .array-data 4
        0x43458000    # 197.5f
        0x423e0000    # 47.5f
    .end array-data

    .line 56
    :array_2f
    .array-data 4
        0x42f06666    # 120.2f
        0x423e0000    # 47.5f
    .end array-data

    :array_30
    .array-data 4
        0x42660000    # 57.5f
        0x43874000    # 270.5f
    .end array-data

    :array_31
    .array-data 4
        0x43458000    # 197.5f
        0x43a8c000    # 337.5f
    .end array-data

    .line 57
    :array_32
    .array-data 4
        0x44014ccd    # 517.2f
        0x43a8c000    # 337.5f
    .end array-data

    .line 58
    :array_33
    .array-data 4
        0x4400b99a    # 514.9f
        0x423e0000    # 47.5f
    .end array-data

    :array_34
    .array-data 4
        0x43458000    # 197.5f
        0x423e0000    # 47.5f
    .end array-data

    .line 59
    :array_35
    .array-data 4
        0x42660000    # 57.5f
        0x42ed0000    # 118.5f
    .end array-data

    :array_36
    .array-data 4
        0x42f06666    # 120.2f
        0x43a8c000    # 337.5f
    .end array-data

    :array_37
    .array-data 4
        0x43bcc000    # 377.5f
        0x43a8c000    # 337.5f
    .end array-data

    .line 60
    :array_38
    .array-data 4
        0x44106000    # 577.5f
        0x4386c000    # 269.5f
    .end array-data

    .line 62
    :array_39
    .array-data 4
        0x43dc0000    # 440.0f
        0x43b40000    # 360.0f
    .end array-data

    :array_3a
    .array-data 4
        0x43430000    # 195.0f
        0x43b40000    # 360.0f
    .end array-data

    .line 63
    :array_3b
    .array-data 4
        0x41dc0000    # 27.5f
        0x43408000    # 192.5f
    .end array-data

    :array_3c
    .array-data 4
        0x43430000    # 195.0f
        0x41c80000    # 25.0f
    .end array-data

    :array_3d
    .array-data 4
        0x43dc0000    # 440.0f
        0x41c80000    # 25.0f
    .end array-data

    .line 64
    :array_3e
    .array-data 4
        0x4417e000    # 607.5f
        0x43408000    # 192.5f
    .end array-data

    :array_3f
    .array-data 4
        0x43dc0000    # 440.0f
        0x43b40000    # 360.0f
    .end array-data

    .line 65
    :array_40
    .array-data 4
        0x43430000    # 195.0f
        0x43b40000    # 360.0f
    .end array-data

    :array_41
    .array-data 4
        0x42cbcccd    # 101.9f
        0x43b40000    # 360.0f
    .end array-data

    .line 66
    :array_42
    .array-data 4
        0x41dc0000    # 27.5f
        0x42c80000    # 100.0f
    .end array-data

    :array_43
    .array-data 4
        0x43dc0000    # 440.0f
        0x41c80000    # 25.0f
    .end array-data

    :array_44
    .array-data 4
        0x44052000    # 532.5f
        0x41c80000    # 25.0f
    .end array-data

    .line 67
    :array_45
    .array-data 4
        0x4417e000    # 607.5f
        0x43904000    # 288.5f
    .end array-data

    .line 68
    :array_46
    .array-data 4
        0x43430000    # 195.0f
        0x43b40000    # 360.0f
    .end array-data

    :array_47
    .array-data 4
        0x41dc0000    # 27.5f
        0x43940ccd    # 296.1f
    .end array-data

    .line 69
    :array_48
    .array-data 4
        0x42cd0000    # 102.5f
        0x41c80000    # 25.0f
    .end array-data

    :array_49
    .array-data 4
        0x43dc0000    # 440.0f
        0x41c80000    # 25.0f
    .end array-data

    :array_4a
    .array-data 4
        0x4417e000    # 607.5f
        0x42c80000    # 100.0f
    .end array-data

    .line 70
    :array_4b
    .array-data 4
        0x4405999a    # 534.4f
        0x43b40000    # 360.0f
    .end array-data

    .line 72
    :array_4c
    .array-data 4
        0x43df3333    # 446.4f
        0x43c08000    # 385.0f
    .end array-data

    :array_4d
    .array-data 4
        0x433c999a    # 188.6f
        0x43c08000    # 385.0f
    .end array-data

    .line 73
    :array_4e
    .array-data 4
        0x0
        0x43408000    # 192.5f
    .end array-data

    :array_4f
    .array-data 4
        0x433c999a    # 188.6f
        0x0
    .end array-data

    :array_50
    .array-data 4
        0x43df3333    # 446.4f
        0x0
    .end array-data

    .line 74
    :array_51
    .array-data 4
        0x441ec666    # 635.1f
        0x43408000    # 192.5f
    .end array-data

    :array_52
    .array-data 4
        0x43df3333    # 446.4f
        0x43c08000    # 385.0f
    .end array-data

    .line 75
    :array_53
    .array-data 4
        0x433c999a    # 188.6f
        0x43c08000    # 385.0f
    .end array-data

    :array_54
    .array-data 4
        0x42a90000    # 84.5f
        0x43c08000    # 385.0f
    .end array-data

    .line 76
    :array_55
    .array-data 4
        0x0
        0x42b70000    # 91.5f
    .end array-data

    :array_56
    .array-data 4
        0x43df3333    # 446.4f
        0x0
    .end array-data

    :array_57
    .array-data 4
        0x4409a666    # 550.6f
        0x0
    .end array-data

    .line 77
    :array_58
    .array-data 4
        0x441ec666    # 635.1f
        0x43944000    # 296.5f
    .end array-data

    .line 78
    :array_59
    .array-data 4
        0x433c999a    # 188.6f
        0x43c08000    # 385.0f
    .end array-data

    :array_5a
    .array-data 4
        0x0
        0x43974000    # 302.5f
    .end array-data

    .line 79
    :array_5b
    .array-data 4
        0x42a90000    # 84.5f
        0x0
    .end array-data

    :array_5c
    .array-data 4
        0x43df3333    # 446.4f
        0x0
    .end array-data

    :array_5d
    .array-data 4
        0x441ec666    # 635.1f
        0x42b30000    # 89.5f
    .end array-data

    .line 80
    :array_5e
    .array-data 4
        0x4409a666    # 550.6f
        0x43c08000    # 385.0f
    .end array-data
.end method

.method private init(Landroid/content/Context;)V
    .locals 57
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mContext:Landroid/content/Context;

    .line 97
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 103
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPath:Landroid/graphics/Path;

    .line 105
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getRotation()F

    move-result v15

    .line 107
    .local v15, "rot":F
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 108
    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    .line 107
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mDensityRatio:F

    .line 110
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getScaleX()F

    move-result v55

    .line 111
    .local v55, "scaleX":F
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getScaleY()F

    move-result v56

    .line 112
    .local v56, "scaleY":F
    const/4 v2, 0x3

    new-array v8, v2, [F

    fill-array-data v8, :array_0

    .line 113
    .local v8, "xscales":[F
    const/4 v2, 0x3

    new-array v9, v2, [F

    fill-array-data v9, :array_1

    .line 114
    .local v9, "yscales":[F
    const/4 v2, 0x2

    new-array v13, v2, [I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0x10

    aput v3, v13, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v13, v2

    .line 115
    .local v13, "frames":[I
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect0Points:[[F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect0Anchors1:[[F

    .line 116
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect0Anchors2:[[F

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v10, 0xff

    const/16 v11, 0x1a

    .line 117
    const/16 v12, 0x95

    const/16 v14, 0x9d

    .line 116
    invoke-static {v10, v11, v12, v14}, Landroid/graphics/Color;->argb(IIII)I

    move-result v10

    .line 117
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mDensityRatio:F

    move/from16 v16, v0

    .line 118
    mul-float v16, v16, v55

    invoke-direct/range {v2 .. v16}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    .line 115
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    .line 120
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v22, v0

    fill-array-data v22, :array_2

    .line 121
    .local v22, "xscales2":[F
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v23, v0

    fill-array-data v23, :array_3

    .line 122
    .local v23, "yscales2":[F
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v27, v0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xf

    aput v3, v27, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v27, v2

    .line 123
    .local v27, "frames2":[I
    new-instance v16, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect1Points:[[F

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect1Anchors1:[[F

    move-object/from16 v18, v0

    .line 124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect1Anchors2:[[F

    move-object/from16 v19, v0

    const/high16 v20, 0x41dc0000    # 27.5f

    const/high16 v21, 0x41c80000    # 25.0f

    .line 125
    const/16 v2, 0xff

    const/16 v3, 0x30

    const/16 v4, 0x9f

    const/16 v5, 0xa8

    .line 124
    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v24

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v26, v0

    const/16 v28, 0x0

    .line 126
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mDensityRatio:F

    mul-float v30, v2, v55

    move/from16 v29, v15

    invoke-direct/range {v16 .. v30}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    .line 123
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    .line 128
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v34, v0

    fill-array-data v34, :array_4

    .line 129
    .local v34, "xscales3":[F
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v35, v0

    fill-array-data v35, :array_5

    .line 130
    .local v35, "yscales3":[F
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v39, v0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v39, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v39, v2

    .line 131
    .local v39, "frames3":[I
    new-instance v28, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect2Points:[[F

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect2Anchors1:[[F

    move-object/from16 v30, v0

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect2Anchors2:[[F

    move-object/from16 v31, v0

    const/high16 v32, 0x42660000    # 57.5f

    const/high16 v33, 0x423e0000    # 47.5f

    .line 133
    const/16 v2, 0xff

    const/16 v3, 0x5b

    const/16 v4, 0xb2

    const/16 v5, 0xb9

    .line 132
    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v36

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v38, v0

    const/16 v40, 0x0

    .line 134
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mDensityRatio:F

    mul-float v42, v2, v55

    move/from16 v41, v15

    invoke-direct/range {v28 .. v42}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    .line 131
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    .line 136
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v46, v0

    fill-array-data v46, :array_6

    .line 137
    .local v46, "xscales4":[F
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v47, v0

    fill-array-data v47, :array_7

    .line 138
    .local v47, "yscales4":[F
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v51, v0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xd

    aput v3, v51, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xd

    aput v3, v51, v2

    .line 139
    .local v51, "frames4":[I
    new-instance v40, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect3Points:[[F

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect3Anchors1:[[F

    move-object/from16 v42, v0

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mRect3Anchors2:[[F

    move-object/from16 v43, v0

    const/high16 v44, 0x42b00000    # 88.0f

    const/high16 v45, 0x42920000    # 73.0f

    .line 141
    const/16 v2, 0xff

    const/16 v3, 0x79

    const/16 v4, 0xc2

    const/16 v5, 0xc8

    .line 140
    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v48

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v50, v0

    const/16 v52, 0x0

    .line 142
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mDensityRatio:F

    mul-float v54, v2, v55

    move/from16 v53, v15

    invoke-direct/range {v40 .. v54}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    .line 139
    move-object/from16 v0, v40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    .line 144
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 145
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 147
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setDimension()V

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 154
    return-void

    .line 112
    nop

    :array_0
    .array-data 4
        0x3ee66666    # 0.45f
        0x3f800000    # 1.0f
        0x3e9eb852    # 0.31f
    .end array-data

    .line 113
    :array_1
    .array-data 4
        0x3ecccccd    # 0.4f
        0x3f800000    # 1.0f
        0x3e9eb852    # 0.31f
    .end array-data

    .line 120
    :array_2
    .array-data 4
        0x3f11eb85    # 0.57f
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data

    .line 121
    :array_3
    .array-data 4
        0x3f051eb8    # 0.52f
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data

    .line 128
    :array_4
    .array-data 4
        0x3f2b851f    # 0.67f
        0x3f800000    # 1.0f
        0x3ed70a3d    # 0.42f
    .end array-data

    .line 129
    :array_5
    .array-data 4
        0x3f1eb852    # 0.62f
        0x3f800000    # 1.0f
        0x3ed70a3d    # 0.42f
    .end array-data

    .line 136
    :array_6
    .array-data 4
        0x3f400000    # 0.75f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data

    .line 137
    :array_7
    .array-data 4
        0x3f333333    # 0.7f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method private restart()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->restart()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->restart()V

    .line 173
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->restart()V

    .line 174
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->restart()V

    .line 177
    :cond_0
    return-void
.end method

.method private setDimension()V
    .locals 3

    .prologue
    .line 202
    const/4 v1, 0x0

    .local v1, "maxWidth":F
    const/4 v0, 0x0

    .line 204
    .local v0, "maxHeight":F
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_0

    .line 205
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v1

    .line 206
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_1

    .line 207
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v1

    .line 208
    :cond_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    .line 209
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v1

    .line 210
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_3

    .line 211
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getWidth()F

    move-result v1

    .line 214
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_4

    .line 215
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v0

    .line 216
    :cond_4
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_5

    .line 217
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v0

    .line 218
    :cond_5
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_6

    .line 219
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v0

    .line 220
    :cond_6
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_7

    .line 221
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->getHeight()F

    move-result v0

    .line 225
    :cond_7
    float-to-int v2, v1

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mWidth:I

    .line 226
    float-to-int v2, v0

    iput v2, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mHeight:I

    .line 227
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 192
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->invalidate()V

    .line 193
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 197
    iget v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mWidth:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->getScaleX()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 198
    iget v1, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mHeight:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->getScaleY()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 197
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->setMeasuredDimension(II)V

    .line 199
    return-void
.end method

.method public update(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect0:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->update(I)V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect1:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->update(I)V

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect2:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->update(I)V

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    if-eqz v0, :cond_3

    .line 165
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/ListeningView;->mCueRect3:Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/sec/android/automotive/drivelink/common/view/CueRoundRect;->update(I)V

    .line 167
    :cond_3
    return-void
.end method
