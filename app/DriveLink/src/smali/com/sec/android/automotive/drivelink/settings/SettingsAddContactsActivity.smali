.class public Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsAddContactsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;,
        Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextFilterBack;,
        Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$EditTextImeBackListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "[SettingsAddContactsActivity]"


# instance fields
.field private final REQUEST_ALL:I

.field private final REQUEST_SEARCH:I

.field private adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

.field private btnDone:Landroid/widget/Button;

.field private btnSearchContactsCancel:Landroid/widget/ImageButton;

.field dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private etSearchContacts:Landroid/widget/EditText;

.field private filteredItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/settings/SettingsContactsItem;",
            ">;"
        }
    .end annotation
.end field

.field private lvFavorites:Landroid/widget/ListView;

.field private mCommonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mFavoriteContactsFetched:Z

.field private mFavoriteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestType:I

.field private mSIPState:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

.field private mSearchOrAllContactsFetched:Z

.field private mToDelete:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private mToSave:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;"
        }
    .end annotation
.end field

.field private noListLayout:Landroid/widget/LinearLayout;

.field private tvNoListLayout:Landroid/widget/TextView;

.field private tvSearchContacts:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 83
    iput v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->REQUEST_ALL:I

    .line 85
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->REQUEST_SEARCH:I

    .line 88
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteContactsFetched:Z

    .line 90
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSearchOrAllContactsFetched:Z

    .line 94
    sget-object v0, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;->SIP_NONE:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    .line 52
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->filteredItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSearchOrAllContactsFetched:Z

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mCommonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvNoListLayout:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->noListLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->filteredItems:Ljava/util/List;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->adapter:Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$ContactsListAdapter;

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteContactsFetched:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;I)V
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mRequestType:I

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mRequestType:I

    return v0
.end method

.method static synthetic access$23(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSIPState:Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$SettingsSIPState;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnDone:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvSearchContacts:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;Z)V
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteContactsFetched:Z

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSearchOrAllContactsFetched:Z

    return v0
.end method

.method private getSettingsSipStateListener()Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;
    .locals 1

    .prologue
    .line 368
    new-instance v0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 398
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onBackPressed()V

    .line 400
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 437
    const-string/jumbo v0, "[SettingsAddContactsActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onConfigurationChanged().... "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 438
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->getInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 437
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->getInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 446
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 447
    return-void

    .line 443
    :cond_0
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->getInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 444
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f0a0446

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 97
    const-string/jumbo v1, "[SettingsAddContactsActivity]"

    const-string/jumbo v2, "onCreate()...."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    const v1, 0x7f030029

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->setContentView(I)V

    .line 101
    const v1, 0x7f090122

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;

    .line 102
    const v1, 0x7f09009f

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->noListLayout:Landroid/widget/LinearLayout;

    .line 103
    const v1, 0x7f0900ae

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvNoListLayout:Landroid/widget/TextView;

    .line 104
    const v1, 0x7f090120

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    .line 105
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSoundEffectsEnabled(Z)V

    .line 106
    const v1, 0x7f09011e

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnDone:Landroid/widget/Button;

    .line 107
    const v1, 0x7f090121

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;

    .line 108
    const v1, 0x7f09011f

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->tvSearchContacts:Landroid/widget/TextView;

    .line 110
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    const-string/jumbo v2, "disableEmoticonInput=true;"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 112
    const v1, 0x7f09011d

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 114
    .local v0, "btnNavBack":Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 115
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 116
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->getInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 120
    const/4 v2, 0x5

    .line 119
    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 122
    :cond_0
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130
    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 131
    const v2, 0x7f0a03f4

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p0, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->setTitle(I)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->lvFavorites:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 154
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v1

    .line 153
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 156
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToSave:Ljava/util/ArrayList;

    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mToDelete:Ljava/util/ArrayList;

    .line 159
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 160
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkFavoriteContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkFavoriteContactListener;)V

    .line 174
    iput v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mRequestType:I

    .line 175
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteContactsFetched:Z

    .line 176
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSearchOrAllContactsFetched:Z

    .line 177
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 178
    invoke-interface {v1, p0}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestFavoriteContactList(Landroid/content/Context;)V

    .line 179
    iget v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mRequestType:I

    if-nez v1, :cond_1

    .line 180
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 181
    const v2, 0xc350

    .line 180
    invoke-interface {v1, p0, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestContactList(Landroid/content/Context;I)V

    .line 187
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 188
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-interface {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 257
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnDone:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 258
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnDone:Landroid/widget/Button;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 327
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    .line 328
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$8;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 349
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 350
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 351
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->btnSearchContactsCancel:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$9;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    check-cast v1, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;

    .line 364
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->getSettingsSipStateListener()Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP;->setOnSettingsSipStateListener(Lcom/sec/android/automotive/drivelink/settings/EditTextForSettingsSIP$OnSettingsSipStateListener;)V

    .line 365
    return-void

    .line 183
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 184
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    .line 185
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 183
    invoke-interface {v1, p0, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 391
    const-string/jumbo v0, "[SettingsAddContactsActivity]"

    const-string/jumbo v1, "onPause()...."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 394
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 404
    const-string/jumbo v0, "[SettingsAddContactsActivity]"

    const-string/jumbo v1, "onResume()...."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 407
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    const-string/jumbo v0, "[SettingsAddContactsActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DrivingStatus :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 409
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 408
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->SettingsDrivingDialog(Landroid/content/Context;)V

    .line 414
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->isContactsChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->setContactsChanged(Z)V

    .line 417
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mFavoriteContactsFetched:Z

    .line 418
    iput-boolean v3, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mSearchOrAllContactsFetched:Z

    .line 419
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 420
    const/4 v1, 0x1

    .line 419
    invoke-interface {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestFavoriteContactList(Landroid/content/Context;Z)V

    .line 421
    iget v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->mRequestType:I

    if-nez v0, :cond_2

    .line 422
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 423
    const v1, 0xc350

    .line 422
    invoke-interface {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestContactList(Landroid/content/Context;I)V

    .line 428
    :goto_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setPhoneSuggestionListUpdateListener()V

    .line 430
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updatePhoneSuggestionList()V

    .line 432
    :cond_1
    return-void

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->dlServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 426
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsAddContactsActivity;->etSearchContacts:Landroid/widget/EditText;

    .line 427
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 425
    invoke-interface {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
