.class Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;
.super Ljava/lang/Object;
.source "PhoneActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 266
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$18(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 267
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 270
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    .line 271
    const-wide/16 v3, 0x258

    invoke-virtual {v2, v3, v4}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 272
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mTextfieldLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$18(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/FrameLayout;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 273
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchResultText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$2(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$15(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/phone/VoicePhoneActionBarLayout;->showVoiceLayout()V

    .line 280
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mPreviousMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$12(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v1

    .line 281
    .local v1, "temp":I
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$11(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 282
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$13(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 284
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v3

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 288
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$19(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 289
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$20(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 290
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$21(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 291
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    invoke-static {v2, v6}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$22(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$23(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    .line 294
    invoke-virtual {v0, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 298
    :cond_0
    const-string/jumbo v2, "PhoneActivity"

    .line 299
    const-string/jumbo v3, "setOnBackBtnClickListener - setAutoShrink : true"

    .line 298
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 318
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .end local v1    # "temp":I
    :goto_0
    return-void

    .line 307
    :cond_1
    const-string/jumbo v2, "PhoneActivity"

    const-string/jumbo v3, "setOnBackBtnClickListener - setAutoShrink : false"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setAutoShrink(Z)V
    invoke-static {v2, v5}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$24(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;Z)V

    .line 310
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    .line 311
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$11(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 312
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$13(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    .line 313
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->mCurrentMode:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$4(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)I

    move-result v3

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->setCurrentMode(I)V
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$14(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;I)V

    goto :goto_0

    .line 315
    :cond_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity$5;->this$0:Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->restorePrevFlow()V
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;->access$25(Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;)V

    goto :goto_0
.end method
