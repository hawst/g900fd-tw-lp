.class public Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;
.super Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;
.source "KDeviceMyPlaceProvider.java"


# static fields
.field private static MYPLACE_ADDRESS_K:Ljava/lang/String;

.field private static MYPLACE_CATEGORY_K:Ljava/lang/String;

.field private static MYPLACE_ID_K:Ljava/lang/String;

.field private static MYPLACE_LATITUDE_K:Ljava/lang/String;

.field private static MYPLACE_LONGITUDE_K:Ljava/lang/String;

.field private static MYPLACE_PROFILE_NAME_K:Ljava/lang/String;

.field private static MYPLACE_PROVIDER_K:Ljava/lang/String;

.field private static MYPLACE_TYPE_K:Ljava/lang/String;

.field private static final PROJECTION_K:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5
    const-string/jumbo v0, "content://com.samsung.android.internal.intelligence.useranalysis/place"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_PROVIDER_K:Ljava/lang/String;

    .line 7
    const-string/jumbo v0, "_id"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_ID_K:Ljava/lang/String;

    .line 9
    const-string/jumbo v0, "category"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_CATEGORY_K:Ljava/lang/String;

    .line 11
    const-string/jumbo v0, "type"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_TYPE_K:Ljava/lang/String;

    .line 13
    const-string/jumbo v0, "name"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_PROFILE_NAME_K:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, "address"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_ADDRESS_K:Ljava/lang/String;

    .line 17
    const-string/jumbo v0, "latitude"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_LATITUDE_K:Ljava/lang/String;

    .line 19
    const-string/jumbo v0, "longitude"

    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_LONGITUDE_K:Ljava/lang/String;

    .line 21
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_ID_K:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 22
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_TYPE_K:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_CATEGORY_K:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_PROFILE_NAME_K:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 23
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_ADDRESS_K:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_LATITUDE_K:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_LONGITUDE_K:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 21
    sput-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->PROJECTION_K:[Ljava/lang/String;

    .line 23
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/MyPlaceProvider;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLatLngDegree(D)D
    .locals 0
    .param p1, "latLng"    # D

    .prologue
    .line 68
    return-wide p1
.end method

.method protected getNameFieldMyPlaceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_ADDRESS_K:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_ID_K:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_LATITUDE_K:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_LONGITUDE_K:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceProfile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_PROFILE_NAME_K:Ljava/lang/String;

    return-object v0
.end method

.method protected getNameFieldMyPlaceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_TYPE_K:Ljava/lang/String;

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getProviderUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/KDeviceMyPlaceProvider;->MYPLACE_PROVIDER_K:Ljava/lang/String;

    return-object v0
.end method
