.class public Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "SettingsRegisterCarOptionsActivity.java"


# static fields
.field protected static final EXTRA_NAME_DEVICE_ADDRESS:Ljava/lang/String; = "EXTRA_NAME_DEVICE_ADDRESS"

.field protected static final EXTRA_NAME_DEVICE_ID:Ljava/lang/String; = "EXTRA_NAME_DEVICE_ID"

.field protected static final EXTRA_NAME_DEVICE_NAME:Ljava/lang/String; = "EXTRA_NAME_DEVICE_NAME"

.field private static final KEY_CAR_SAVED_NAME:Ljava/lang/String; = "KEY_CAR_SAVED_NAME"

.field private static final KEY_DIALOG_ET_VALUE:Ljava/lang/String; = "KEY_DIALOG_ET_VALUE"

.field private static final KEY_DIALOG_IS_OPEN:Ljava/lang/String; = "KEY_DIALOG_IS_OPEN"

.field private static final KEY_DIALOG_OK_ENABLED:Ljava/lang/String; = "KEY_DIALOG_OK_ENABLED"

.field private static final KEY_DIALOG_SEL_END:Ljava/lang/String; = "KEY_DIALOG_SEL_END"

.field private static final KEY_DIALOG_SEL_START:Ljava/lang/String; = "KEY_DIALOG_SEL_START"

.field private static final MAX_NAME_BYTE_LENGTH:I = 0x20

.field private static final TAG:Ljava/lang/String; = "[SettingsRegisterCarOptionsActivity]"


# instance fields
.field private dialogEditTextValue:Ljava/lang/String;

.field private dialogSelectionEnd:I

.field private dialogSelectionStart:I

.field private etDialogEditText:Landroid/widget/EditText;

.field private isDialogOkEnabled:Z

.field private isDialogOpen:Z

.field private layoutSettingsRemove:Landroid/widget/LinearLayout;

.field private layoutSettingsRename:Landroid/widget/LinearLayout;

.field private mDialog:Landroid/app/AlertDialog;

.field private settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

.field private tvSettingsRenameDeviceName:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 67
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOpen:Z

    .line 70
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOkEnabled:Z

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V
    .locals 0

    .prologue
    .line 396
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->refreshDeviceNameTextView()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOkEnabled:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Z)V
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOpen:Z

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Z)V
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOkEnabled:Z

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->dialogEditTextValue:Ljava/lang/String;

    return-void
.end method

.method private refreshDeviceNameTextView()V
    .locals 2

    .prologue
    .line 397
    .line 398
    const v0, 0x7f090146

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 397
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->tvSettingsRenameDeviceName:Landroid/widget/TextView;

    .line 399
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->tvSettingsRenameDeviceName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .line 400
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v1

    .line 399
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0a03e5

    .line 81
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const v1, 0x7f030030

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->setContentView(I)V

    .line 85
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 88
    const-string/jumbo v3, "EXTRA_NAME_DEVICE_ID"

    .line 89
    const/4 v4, 0x0

    .line 87
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 90
    const-string/jumbo v3, ""

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 93
    const-string/jumbo v5, "EXTRA_NAME_DEVICE_NAME"

    .line 92
    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 96
    const-string/jumbo v6, "EXTRA_NAME_DEVICE_ADDRESS"

    .line 95
    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .line 98
    if-eqz p1, :cond_1

    .line 100
    const-string/jumbo v1, "KEY_DIALOG_ET_VALUE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->dialogEditTextValue:Ljava/lang/String;

    .line 102
    const-string/jumbo v1, "KEY_DIALOG_SEL_START"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 101
    iput v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->dialogSelectionStart:I

    .line 104
    const-string/jumbo v1, "KEY_DIALOG_SEL_END"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 103
    iput v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->dialogSelectionEnd:I

    .line 106
    const-string/jumbo v1, "KEY_DIALOG_OK_ENABLED"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOkEnabled:Z

    .line 108
    const-string/jumbo v1, "KEY_DIALOG_IS_OPEN"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->showRenameDialog(Z)V

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    .line 113
    const-string/jumbo v2, "KEY_CAR_SAVED_NAME"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->setBtName(Ljava/lang/String;)V

    .line 116
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->refreshDeviceNameTextView()V

    .line 119
    const v1, 0x7f090145

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 118
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->layoutSettingsRename:Landroid/widget/LinearLayout;

    .line 120
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->layoutSettingsRename:Landroid/widget/LinearLayout;

    .line 121
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    const v1, 0x7f090147

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 129
    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->layoutSettingsRemove:Landroid/widget/LinearLayout;

    .line 131
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->layoutSettingsRemove:Landroid/widget/LinearLayout;

    .line 132
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 170
    check-cast v0, Landroid/widget/LinearLayout;

    .line 172
    .local v0, "btnNavBack":Landroid/widget/LinearLayout;
    new-instance v1, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    .line 180
    invoke-virtual {p0, v7}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 181
    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 182
    const v2, 0x7f0a03f4

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 184
    invoke-virtual {p0, v7}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->setTitle(I)V

    .line 186
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 191
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 195
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 197
    const-string/jumbo v0, "[SettingsRegisterCarOptionsActivity]"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const-string/jumbo v0, "[SettingsRegisterCarOptionsActivity]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DrivingStatus :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 201
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->SettingsDrivingDialog(Landroid/content/Context;)V

    .line 204
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 363
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 365
    const-string/jumbo v0, "KEY_DIALOG_IS_OPEN"

    .line 366
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOpen:Z

    .line 364
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 368
    const-string/jumbo v2, "KEY_DIALOG_ET_VALUE"

    .line 369
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 370
    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 367
    :goto_0
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string/jumbo v2, "KEY_DIALOG_SEL_START"

    .line 373
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    .line 374
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 371
    :goto_1
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 376
    const-string/jumbo v0, "KEY_DIALOG_SEL_END"

    .line 377
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    .line 378
    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 375
    :cond_0
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 380
    const-string/jumbo v0, "KEY_DIALOG_OK_ENABLED"

    .line 381
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->isDialogOkEnabled:Z

    .line 379
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 384
    const-string/jumbo v0, "KEY_CAR_SAVED_NAME"

    .line 385
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v1

    .line 383
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    return-void

    .line 370
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 374
    goto :goto_1
.end method

.method protected showRenameDialog(Z)V
    .locals 10
    .param p1, "isRotated"    # Z

    .prologue
    const/16 v2, 0x1e

    const/4 v3, 0x4

    .line 216
    .line 217
    const-string/jumbo v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 216
    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    .line 218
    .local v6, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 220
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a0422

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 222
    const-string/jumbo v1, "JINSEIL"

    const-string/jumbo v4, "SettingsRegisterCarOptionAcitivity showRenameDialog!"

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    .line 226
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    const-string/jumbo v4, "disableEmoticonInput=true;"

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 227
    if-nez p1, :cond_0

    .line 228
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->settingsCarBTDevice:Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/settings/SettingsCarBTDevice;->getBtName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    .line 236
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    .line 237
    const/4 v4, 0x1

    new-array v4, v4, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    new-instance v7, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;

    .line 239
    const/16 v8, 0x20

    .line 240
    const v9, 0x7f0a03ec

    invoke-direct {v7, p0, v8, v9}, Lcom/sec/android/automotive/drivelink/settings/ByteInputFilter;-><init>(Landroid/app/Activity;II)V

    aput-object v7, v4, v5

    .line 237
    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 243
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    const/16 v5, 0x16

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;IIII)Landroid/app/AlertDialog$Builder;

    .line 245
    const v1, 0x7f0a043a

    .line 246
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V

    .line 245
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 264
    const v1, 0x7f0a043b

    .line 265
    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V

    .line 264
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 278
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->mDialog:Landroid/app/AlertDialog;

    .line 280
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->mDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 310
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->mDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 324
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 351
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 352
    return-void

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->dialogEditTextValue:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->etDialogEditText:Landroid/widget/EditText;

    iget v4, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->dialogSelectionStart:I

    .line 233
    iget v5, p0, Lcom/sec/android/automotive/drivelink/settings/SettingsRegisterCarOptionsActivity;->dialogSelectionEnd:I

    .line 232
    invoke-virtual {v1, v4, v5}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0
.end method
