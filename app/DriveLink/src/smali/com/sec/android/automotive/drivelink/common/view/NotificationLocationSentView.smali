.class public Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;
.super Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;
.source "NotificationLocationSentView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected mViewLocal:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p5, "isMultiMode"    # Z

    .prologue
    .line 32
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->initIndicatorView(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p4, "isMultiMode"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->initIndicatorView(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;
    .param p3, "isMultiMode"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationView;-><init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/notification/model/NotificationItem;Z)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->initIndicatorView(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method private initIndicatorView(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    if-nez p1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 42
    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 41
    check-cast v0, Landroid/view/LayoutInflater;

    .line 44
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    if-nez v1, :cond_0

    .line 45
    const v1, 0x7f0300b2

    .line 46
    const/4 v2, 0x0

    .line 45
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    .line 47
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->addView(Landroid/view/View;)V

    .line 49
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    const v2, 0x7f0902ec

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 50
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 58
    .local v0, "id":I
    const v1, 0x7f0902ec

    if-eq v0, v1, :cond_0

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->mViewLocal:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/common/view/NotificationLocationSentView;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
