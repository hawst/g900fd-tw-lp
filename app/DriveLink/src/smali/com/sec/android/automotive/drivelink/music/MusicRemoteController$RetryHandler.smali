.class Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;
.super Landroid/os/Handler;
.source "MusicRemoteController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RetryHandler"
.end annotation


# static fields
.field public static final CMD_CHECK_TO_WORKING_CALLBACK:I = 0x2

.field private static final CMD_REQUEST_POOL:I = 0x1

.field public static final CMD_REQUEST_TO_REGISTER:I = 0x0

.field private static final MAX_RETRY:I = 0x3

.field private static final RETRY_INTERVAL:I = 0x3e8


# instance fields
.field private mRetryCount:I

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 329
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    .line 330
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 331
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v5, 0x3e8

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 335
    const-string/jumbo v0, "RemoteMusicController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "handleMessage what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 364
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 365
    return-void

    .line 338
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 339
    invoke-virtual {p0, v3, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 341
    :cond_1
    iput v4, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->mRetryCount:I

    goto :goto_0

    .line 345
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->registerRemoteController()Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$7(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 346
    invoke-virtual {p0, v3}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->removeMessages(I)V

    goto :goto_0

    .line 350
    :cond_2
    iget v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->mRetryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->mRetryCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 351
    invoke-virtual {p0, v3, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 356
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->mFlagForFirstPlayingChecker:Z
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$0(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const-string/jumbo v0, "RemoteMusicController"

    .line 358
    const-string/jumbo v1, "callback is not working unregister remote controller"

    .line 357
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->unregisterRemoteController()V
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$8(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;

    invoke-static {v0, v4}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;->access$1(Lcom/sec/android/automotive/drivelink/music/MusicRemoteController;Z)V

    goto :goto_0

    .line 336
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->removeMessages(I)V

    .line 369
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->removeMessages(I)V

    .line 370
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/music/MusicRemoteController$RetryHandler;->removeMessages(I)V

    .line 371
    return-void
.end method
