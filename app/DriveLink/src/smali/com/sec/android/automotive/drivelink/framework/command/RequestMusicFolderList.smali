.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestMusicFolderList.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mContext:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mList:Ljava/util/ArrayList;

    .line 20
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mList:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method protected process()V
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getContentManager()Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;

    move-result-object v0

    .line 27
    .local v0, "contentManager":Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/ContentManager;->getMusicFolderList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mList:Ljava/util/ArrayList;

    .line 28
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkMusicListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    move-result-object v0

    .line 34
    .local v0, "musicListener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;
    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestMusicFolderList;->mList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;->onResponseRequestFolderList(Ljava/util/ArrayList;)V

    .line 37
    :cond_0
    return-void
.end method
