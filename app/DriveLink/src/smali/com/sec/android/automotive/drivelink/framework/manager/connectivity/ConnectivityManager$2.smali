.class Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->connectBluetoothDevice(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field result:I

.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

.field private final synthetic val$btDevice:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iput-object p2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    .line 448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;)Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    return-object v0
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 453
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 454
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    check-cast p2, Landroid/bluetooth/BluetoothA2dp;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    iput-object p2, v1, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 456
    new-instance v0, Ljava/lang/Thread;

    .line 457
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$1;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->val$btDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$1;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;Landroid/bluetooth/BluetoothDevice;)V

    .line 456
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 545
    .local v0, "mCheckingConnectionThreadA2DP":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 546
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 7
    .param p1, "profile"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 550
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 552
    .local v2, "objPollA2DP":Ljava/lang/Object;
    new-instance v1, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2$2;-><init>(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;Ljava/lang/Object;)V

    invoke-direct {v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 581
    .local v1, "mThread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 583
    monitor-enter v2

    .line 585
    const-wide/16 v3, 0x1388

    :try_start_0
    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 583
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v3, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$5(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 592
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v3, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$1(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 593
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v3, v6}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$20(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 594
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    iput-object v6, v3, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->mProfileProxyA2DP:Landroid/bluetooth/BluetoothA2dp;

    .line 595
    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager$2;->this$0:Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;

    invoke-static {v3, v5}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$6(Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;Z)V

    .line 596
    return-void

    .line 586
    :catch_0
    move-exception v0

    .line 587
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    # getter for: Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/manager/connectivity/ConnectivityManager;->access$0()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "connectBluetoothDevice"

    invoke-static {v3, v4, v0}, Lcom/sec/android/automotive/drivelink/framework/common/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 583
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method
