.class Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;
.super Ljava/lang/Object;
.source "SplashScreenActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v3, 0x8

    .line 179
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$2(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;Z)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mCenterLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$3(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 181
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->access$4(Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 184
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/settings/SettingsUtils;->isFirstAccess(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    .line 187
    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/RegulationsActivity;

    .line 186
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 188
    const-string/jumbo v1, "EXTRA_NAME_FIRST_ACCESS"

    .line 189
    const/4 v2, 0x1

    .line 188
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 190
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 192
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity$2;->this$0:Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/firstaccess/SplashScreenActivity;->finish()V

    .line 194
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 172
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 168
    return-void
.end method
