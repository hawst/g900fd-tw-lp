.class Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    .line 613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 616
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$30(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 617
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 620
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 621
    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 622
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mTextfieldLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$30(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/view/ViewGroup;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 623
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$31(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/VoiceMusicPlayerActionBarLayout;->showVoiceLayout()V

    .line 625
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$32(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 627
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$33(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 628
    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 632
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 640
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 637
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setAutoShrink(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$3(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 638
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->setSoundViewAutoDisappear(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->access$26(Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;Z)V

    .line 639
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity$8;->this$0:Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;->finish()V

    goto :goto_0
.end method
