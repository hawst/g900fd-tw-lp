.class public Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;
.super Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;
.source "GoogleLaunchPopupActivity.java"


# instance fields
.field private mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 20
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0a04a5

    .line 21
    const v3, 0x7f0a02f3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;-><init>(Landroid/content/Context;II)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    .line 23
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a001a

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 24
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    const v1, 0x7f0a04a4

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setMessage(I)V

    .line 26
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    new-instance v1, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog$BasicDialogListener;)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/firstaccess/GoogleLaunchPopupActivity;->mDialog:Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/common/view/dialog/BasicDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
