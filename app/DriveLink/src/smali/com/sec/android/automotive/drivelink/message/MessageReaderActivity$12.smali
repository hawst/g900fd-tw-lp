.class Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;
.super Ljava/lang/Object;
.source "MessageReaderActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->setCurrentPage(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    .line 696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 4
    .param p1, "arg0"    # I

    .prologue
    const/4 v3, 0x0

    .line 700
    if-nez p1, :cond_1

    .line 701
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->pageSet:Z
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$19(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 702
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 704
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 705
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 706
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 708
    const v2, 0x7f0a0184

    .line 707
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 706
    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 710
    const-string/jumbo v1, "DM_SMS_READBACK"

    .line 709
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 711
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    .line 724
    .end local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    .line 726
    :cond_1
    return-void

    .line 712
    :cond_2
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTotalMsgCount:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$21(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 714
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 715
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 716
    .restart local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 718
    const v2, 0x7f0a0185

    .line 717
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 716
    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 720
    const-string/jumbo v1, "DM_SMS_READBACK"

    .line 719
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 721
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-static {v1, v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$10(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    goto :goto_0
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 732
    return-void
.end method

.method public onPageSelected(I)V
    .locals 7
    .param p1, "arg0"    # I

    .prologue
    const/4 v6, 0x1

    .line 736
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 738
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 739
    .local v0, "nDefaultSmsApplication":Ljava/lang/String;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 741
    const-string/jumbo v1, "com.android.mms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 742
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    .line 744
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 743
    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setMessageStateToRead(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;)V

    .line 748
    .end local v0    # "nDefaultSmsApplication":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 749
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$24(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;I)V

    .line 751
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-static {v1, v6}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$22(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Z)V

    .line 752
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 753
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getReceivedTime()J

    move-result-wide v4

    .line 752
    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getDate(J)Ljava/lang/String;
    invoke-static {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$25(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$26(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V

    .line 754
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTime:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$27(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageTime:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$28(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 756
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgType()I

    move-result v1

    if-nez v1, :cond_3

    .line 758
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 759
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getMsgBody()Ljava/lang/String;

    move-result-object v1

    .line 758
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V

    .line 760
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 761
    :cond_1
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 762
    const v3, 0x7f0a03b0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 761
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V

    .line 780
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 781
    const v3, 0x7f0a01eb

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 782
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 780
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mTTSMessageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$4(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 788
    const v3, 0x7f0a0183

    .line 787
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 791
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # invokes: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->startMsgReader()V
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$17(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)V

    .line 792
    return-void

    .line 766
    :cond_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMsgInfo:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$23(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mCurrentPage:I
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$20(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;

    .line 767
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;->getAttachmentInfo()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessageAttachment;->getText()Ljava/lang/String;

    move-result-object v1

    .line 766
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V

    .line 768
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->mMessageBody:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$30(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 769
    :cond_4
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity$12;->this$0:Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 770
    const v3, 0x7f0a018c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 769
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;->access$29(Lcom/sec/android/automotive/drivelink/message/MessageReaderActivity;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
