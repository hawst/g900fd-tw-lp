.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkUpdateListener"
.end annotation


# virtual methods
.method public abstract onResponseDownloadProgress(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLDownloadData;)V
.end method

.method public abstract onResponseInstallStarted()V
.end method

.method public abstract onResponseRequestCheckUpdateVersion(Z)V
.end method

.method public abstract onResponseRequestUpdateApplicationFail(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo$UPDATE_ERROR_TYPE;)V
.end method

.method public abstract onResponseRequestUpdateApplicationSuccess()V
.end method
