.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestUpdateParticipantStatus.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDuration:J

.field private mGroupId:I

.field private mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

.field private mResult:Z

.field private mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "groupId"    # I
    .param p4, "myStatus"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;
    .param p5, "myLocation"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p6, "duration"    # J

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 11
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mContext:Landroid/content/Context;

    .line 12
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mDuration:J

    .line 13
    iput-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mGroupId:I

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mResult:Z

    .line 23
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mContext:Landroid/content/Context;

    .line 24
    iput-object p5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 25
    iput-wide p6, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mDuration:J

    .line 26
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 27
    iput p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mGroupId:I

    .line 28
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mContext:Landroid/content/Context;

    .line 49
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 50
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    .line 51
    return-void
.end method

.method protected process()V
    .locals 7

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mGroupId:I

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mStatus:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mMyLocation:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .line 34
    iget-wide v5, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mDuration:J

    .line 33
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->updateMyStatus(Landroid/content/Context;ILcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddyStatusType;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;J)Z

    move-result v0

    .line 32
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mResult:Z

    .line 35
    return-void
.end method

.method protected response()V
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 41
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 42
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestUpdateParticipantStatus;->mResult:Z

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestUpdateParticipantStatus(Z)V

    .line 44
    :cond_0
    return-void
.end method
