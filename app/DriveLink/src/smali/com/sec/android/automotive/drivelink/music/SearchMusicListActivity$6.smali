.class Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;
.super Ljava/lang/Object;
.source "SearchMusicListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 206
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$5(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 207
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$3(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    .line 210
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 212
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity$6;->this$0:Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;

    # getter for: Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->mSearchText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$6(Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 213
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/music/SearchMusicListActivity;->access$4(Z)V

    .line 214
    return-void
.end method
