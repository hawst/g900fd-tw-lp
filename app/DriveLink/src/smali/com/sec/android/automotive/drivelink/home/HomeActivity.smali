.class public Lcom/sec/android/automotive/drivelink/home/HomeActivity;
.super Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/notification/view/NotificationSamsungAccountRegisterDialog$ClickListener;


# static fields
.field public static final SHOW_DIALOG_SET_MYPLACE:Ljava/lang/String; = "setDialogForMyPlace"

.field public static final STATE_DAILY_COMMUTE:Ljava/lang/String; = "state_daily_commute"

.field private static final TAG:Ljava/lang/String;

.field private static Updating:Z

.field private static hasUpdate:Z

.field private static isFinished:Z

.field public static startTransitionTime:J


# instance fields
.field UpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

.field public endTime:J

.field private isActionBarInitiated:Z

.field private isCarAppFinishing:Z

.field private isForeground:Z

.field public isShowSetMyPlaceDialog:Z

.field private mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

.field private mBackkeyPressed:Z

.field private mBlockingCallReceiver:Landroid/content/BroadcastReceiver;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtnMessage:Landroid/widget/LinearLayout;

.field private mBtnMusic:Landroid/widget/LinearLayout;

.field private mBtnNavigation:Landroid/widget/LinearLayout;

.field private mBtnPhone:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

.field private mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

.field private mHomeLayout:Landroid/widget/RelativeLayout;

.field private mIsNowDriving:Z

.field private mLanguageChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mLcdOffReceiver:Landroid/content/BroadcastReceiver;

.field private mMessageIcon:Landroid/widget/ImageView;

.field private mMessageName:Landroid/widget/TextView;

.field private mMusicIcon:Landroid/widget/ImageView;

.field private mMusicName:Landroid/widget/TextView;

.field private mNavigationIcon:Landroid/widget/ImageView;

.field private mNavigationName:Landroid/widget/TextView;

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPhoneIcon:Landroid/widget/ImageView;

.field private mPhoneName:Landroid/widget/TextView;

.field private mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

.field private mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

.field private promptForMusic:Ljava/lang/String;

.field rr:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    .line 144
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->hasUpdate:Z

    .line 146
    sput-boolean v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->Updating:Z

    .line 153
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isFinished:Z

    .line 189
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;-><init>()V

    .line 107
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    .line 116
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnPhone:Landroid/widget/LinearLayout;

    .line 118
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMessage:Landroid/widget/LinearLayout;

    .line 120
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnNavigation:Landroid/widget/LinearLayout;

    .line 122
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMusic:Landroid/widget/LinearLayout;

    .line 140
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isForeground:Z

    .line 142
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isActionBarInitiated:Z

    .line 158
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isShowSetMyPlaceDialog:Z

    .line 176
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    .line 180
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isCarAppFinishing:Z

    .line 182
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBackkeyPressed:Z

    .line 184
    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z

    .line 186
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->promptForMusic:Ljava/lang/String;

    .line 562
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$1;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 1428
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$2;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->UpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    .line 96
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V
    .locals 0

    .prologue
    .line 1594
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V

    return-void
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1601
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->checkTtsForUnreadMessage()V

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 803
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->showListDialog()V

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/automotive/drivelink/home/HomeActivity;I)V
    .locals 0

    .prologue
    .line 699
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->moreMenuSelected(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->promptForMusic:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->promptForMusic:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z
    .locals 1

    .prologue
    .line 1379
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isUpdate()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isForeground:Z

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1368
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->goUpdateActivity()V

    return-void
.end method

.method static synthetic access$8(Z)V
    .locals 0

    .prologue
    .line 144
    sput-boolean p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->hasUpdate:Z

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/automotive/drivelink/home/HomeActivity;Z)V
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z

    return-void
.end method

.method private cancelCheckUpdate()Z
    .locals 3

    .prologue
    .line 1351
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "CANCEL REQUEST CHECK UPDATE"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 1355
    .local v0, "DLServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1354
    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestCancelUpdateVersionCheck(Landroid/content/Context;)V

    .line 1356
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->UpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V

    .line 1358
    const/4 v1, 0x1

    return v1
.end method

.method private checkTtsForUnreadMessage()V
    .locals 1

    .prologue
    .line 1602
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasPendingReq()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1603
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 1604
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasNotification()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1605
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    move-result-object v0

    .line 1606
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->resetPendingReq()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1607
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->checkUnreadMessage()V

    .line 1612
    :cond_1
    :goto_0
    return-void

    .line 1610
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->checkUnreadMessage()V

    goto :goto_0
.end method

.method private checkUnreadMessage()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1120
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->getCallEndCheckSingleton()Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->getCallEndFlag()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isActionBarInitiated:Z

    if-eqz v2, :cond_2

    .line 1121
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v2

    .line 1122
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->requestUnreadMessageCountBySync()I

    move-result v0

    .line 1123
    .local v0, "msgCount":I
    const-string/jumbo v2, ""

    .line 1124
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " requestUnreadMessageCountBySync end time : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1124
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1123
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    if-eqz v0, :cond_0

    .line 1128
    if-ne v0, v7, :cond_1

    .line 1129
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1130
    const v3, 0x7f0a0181

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 1129
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1136
    .local v1, "voiceGuide":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 1137
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 1139
    .end local v1    # "voiceGuide":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->getCallEndCheckSingleton()Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->setCallEndFlag(Z)V

    .line 1147
    .end local v0    # "msgCount":I
    :goto_1
    return-void

    .line 1132
    .restart local v0    # "msgCount":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1133
    const v3, 0x7f0a0180

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 1132
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "voiceGuide":Ljava/lang/String;
    goto :goto_0

    .line 1142
    .end local v0    # "msgCount":I
    .end local v1    # "voiceGuide":Ljava/lang/String;
    :cond_2
    const-string/jumbo v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "CallEndCheckSingleton :::: calll fail:::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1143
    invoke-static {}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->getCallEndCheckSingleton()Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;

    move-result-object v4

    .line 1144
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/message/CallEndCheckSingleton;->getCallEndFlag()Z

    move-result v4

    .line 1143
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1142
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private checkUpdate()Z
    .locals 3

    .prologue
    .line 1340
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "REQUEST CHECK UPDATE"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 1344
    .local v0, "DLServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1343
    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestCheckUpdateVersion(Landroid/content/Context;)V

    .line 1345
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->UpdateListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkUpdateListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkUpdateListener;)V

    .line 1347
    const/4 v1, 0x1

    return v1
.end method

.method private checkUserMode()Z
    .locals 4

    .prologue
    .line 1362
    const-string/jumbo v1, "user"

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1364
    .local v0, "userMode":Z
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "checkUserMode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    return v0
.end method

.method private convertDpToPixel(FLandroid/content/Context;)F
    .locals 5
    .param p1, "dp"    # F
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 888
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 889
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 890
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    const/high16 v4, 0x43200000    # 160.0f

    div-float/2addr v3, v4

    mul-float v1, p1, v3

    .line 891
    .local v1, "px":F
    return v1
.end method

.method private disclaimer()V
    .locals 0

    .prologue
    .line 1021
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->initPlaceOnDisclaimnerDialog()V

    .line 1022
    return-void
.end method

.method private getStringPreference(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "mStringPreferenceName"    # Ljava/lang/String;

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v1

    .line 419
    .local v1, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    const-string/jumbo v2, ""

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 421
    .local v0, "mPreferenceValue":Ljava/lang/String;
    return-object v0
.end method

.method private goUpdateActivity()V
    .locals 4

    .prologue
    .line 1369
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "GO UPDATE ACTIVITY : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isMultiWindow()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1371
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1372
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/automotive/drivelink/update/UpdateActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1373
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 1374
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->Updating:Z

    .line 1375
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->hasUpdate:Z

    .line 1377
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private handleShareIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1199
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1201
    const-string/jumbo v0, "fromShare"

    .line 1200
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1201
    if-eqz v0, :cond_1

    .line 1202
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->getListener()Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;->onMessageSharingState(I)V

    .line 1205
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1206
    const-string/jumbo v1, "fromShare"

    .line 1205
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1227
    :cond_0
    :goto_0
    return-void

    .line 1209
    :cond_1
    const-string/jumbo v0, "fromRequest"

    .line 1208
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1209
    if-eqz v0, :cond_0

    .line 1211
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/MessageSharingNotification;->getListener()Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/OnMessageSharingNotiListener;->onMessageSharingState(I)V

    .line 1214
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1215
    const-string/jumbo v1, "fromRequest"

    .line 1214
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1216
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1217
    const-string/jumbo v1, "fromRequest"

    .line 1216
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initActionBarIcon()V
    .locals 3

    .prologue
    .line 507
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    if-eqz v1, :cond_0

    .line 508
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "update drving status_initActionBarIcon_dismiss"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->dismiss()V

    .line 511
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    .line 512
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->updateSampleCommandList()V

    .line 514
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    .line 516
    .local v0, "voiceUpdatre":Lcom/nuance/drivelink/DLAppUiUpdater;
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 517
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->isMicDisplayed()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->setMicDisplayed(Z)V

    .line 518
    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 520
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    new-instance v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$7;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->setOnMoreBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    return-void
.end method

.method private initHomeLayout()V
    .locals 2

    .prologue
    .line 463
    const v0, 0x7f090076

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    .line 464
    const v0, 0x7f090075

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mHomeLayout:Landroid/widget/RelativeLayout;

    .line 472
    const v0, 0x7f090078

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnPhone:Landroid/widget/LinearLayout;

    .line 473
    const v0, 0x7f09007b

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMessage:Landroid/widget/LinearLayout;

    .line 474
    const v0, 0x7f09007e

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnNavigation:Landroid/widget/LinearLayout;

    .line 475
    const v0, 0x7f090081

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMusic:Landroid/widget/LinearLayout;

    .line 477
    const v0, 0x7f090079

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneIcon:Landroid/widget/ImageView;

    .line 478
    const v0, 0x7f09007c

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageIcon:Landroid/widget/ImageView;

    .line 479
    const v0, 0x7f09007f

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationIcon:Landroid/widget/ImageView;

    .line 480
    const v0, 0x7f090082

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicIcon:Landroid/widget/ImageView;

    .line 482
    const v0, 0x7f09007a

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneName:Landroid/widget/TextView;

    .line 483
    const v0, 0x7f09007d

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageName:Landroid/widget/TextView;

    .line 484
    const v0, 0x7f090080

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationName:Landroid/widget/TextView;

    .line 485
    const v0, 0x7f090083

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicName:Landroid/widget/TextView;

    .line 487
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnPhone:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 488
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMessage:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnNavigation:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 490
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMusic:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 491
    return-void
.end method

.method private initPlaceOnDisclaimnerDialog()V
    .locals 3

    .prologue
    .line 1006
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;-><init>()V

    .line 1007
    .local v0, "dialog":Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;
    new-instance v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$12;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->setOnClickListener(Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog$ClickListener;)V

    .line 1016
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 1017
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog 3"

    .line 1016
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationPlaceOnDisclaimerDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1018
    return-void
.end method

.method private isUpdate()Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1381
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v8

    .line 1382
    invoke-virtual {v8}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 1383
    sget-object v9, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->PREFERENCE_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 1385
    .local v5, "pref":Landroid/content/SharedPreferences;
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_UPDATE_VERSION:Ljava/lang/String;

    .line 1384
    invoke-interface {v5, v8, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 1386
    .local v3, "mIsUpdateVersion":Z
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_UPDATE_VERSION:Ljava/lang/String;

    invoke-interface {v5, v8, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1389
    .local v4, "mUpdateVersion":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_DOWNLOAD_VERSION:Ljava/lang/String;

    .line 1388
    invoke-interface {v5, v8, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1391
    .local v0, "mDownloadVersion":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_IS_COMPLETED_DOWNLAOD:Ljava/lang/String;

    .line 1390
    invoke-interface {v5, v8, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 1393
    .local v2, "mIsCompletedDownload":Z
    sget-object v8, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLUpdateInfo;->KEY_INSTALL_RESULT:Ljava/lang/String;

    .line 1392
    invoke-interface {v5, v8, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1395
    .local v1, "mInstallResult":Z
    sget-object v8, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "- isUpdate - mIsUpdateVersion :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1396
    const-string/jumbo v10, " mUpdateVersion : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1395
    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1397
    sget-object v8, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "- isUpdate - mDownloadVersion :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1398
    const-string/jumbo v10, " mIsCompletedDownload : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1399
    const-string/jumbo v10, " mInstallResult:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1397
    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1401
    if-nez v3, :cond_0

    .line 1402
    sget-object v7, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "- mIsUpdateVersion false!!"

    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    :goto_0
    return v6

    .line 1406
    :cond_0
    if-nez v2, :cond_1

    .line 1407
    sget-object v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "- mIsCompletedDownload false"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 1408
    goto :goto_0

    .line 1410
    :cond_1
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1411
    if-eqz v1, :cond_2

    .line 1412
    sget-object v7, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    .line 1413
    const-string/jumbo v8, "- mUpdateVersion.equals(mDownloadVersion) && mInstallResult == true"

    .line 1412
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1415
    :cond_2
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1416
    if-nez v1, :cond_3

    .line 1417
    sget-object v7, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    .line 1418
    const-string/jumbo v8, "- mUpdateVersion.equals(mDownloadVersion) && mIsCompletedDownload  && mInstallResult == false ???"

    .line 1417
    invoke-static {v7, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1421
    :cond_3
    sget-object v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "- !mUpdateVersion.equals(mDownloadVersion)"

    invoke-static {v6, v8}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 1422
    goto :goto_0
.end method

.method private moreMenuSelected(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 700
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$8;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;I)V

    .line 800
    const-wide/16 v2, 0x12c

    .line 700
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 801
    return-void
.end method

.method private setBtnClickable(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 1595
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnPhone:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1596
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMessage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1597
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnNavigation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1598
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBtnMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 1599
    return-void
.end method

.method private setDefaultValueSettingsPoi()V
    .locals 3

    .prologue
    const v2, 0x7f0a0450

    .line 435
    .line 436
    const-string/jumbo v0, "PREF_SETTINGS_GAS_STATION_ORDER"

    .line 435
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getStringPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 437
    const-string/jumbo v1, ""

    .line 436
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 437
    if-eqz v0, :cond_0

    .line 439
    const-string/jumbo v0, "PREF_SETTINGS_GAS_STATION_ORDER"

    .line 438
    invoke-direct {p0, v0, v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setStringPreference(Ljava/lang/String;I)V

    .line 444
    :cond_0
    const-string/jumbo v0, "PREF_SETTINGS_GAS_STATION_FUEL_TYPE"

    .line 443
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getStringPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 448
    const-string/jumbo v0, "PREF_SETTINGS_GAS_STATION_FUEL_TYPE"

    .line 449
    const v1, 0x7f0a044b

    .line 447
    invoke-direct {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setStringPreference(Ljava/lang/String;I)V

    .line 453
    :cond_1
    const-string/jumbo v0, "PREF_SETTINGS_PARKING_LOT_ORDER"

    .line 452
    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getStringPreference(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 454
    const-string/jumbo v1, ""

    .line 453
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 454
    if-eqz v0, :cond_2

    .line 457
    const-string/jumbo v0, "PREF_SETTINGS_PARKING_LOT_ORDER"

    .line 456
    invoke-direct {p0, v0, v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setStringPreference(Ljava/lang/String;I)V

    .line 460
    :cond_2
    return-void
.end method

.method private setLanguageChangedReceiver()V
    .locals 3

    .prologue
    .line 382
    new-instance v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$6;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mLanguageChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 411
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mLanguageChangedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    .line 412
    const-string/jumbo v2, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 411
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 413
    return-void
.end method

.method private setStringPreference(Ljava/lang/String;I)V
    .locals 2
    .param p1, "mStringPreferenceName"    # Ljava/lang/String;
    .param p2, "mResourceId"    # I

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 430
    .local v0, "preference":Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-virtual {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    return-void
.end method

.method private showListDialog()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 804
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    if-eqz v4, :cond_1

    .line 805
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->show()V

    .line 885
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 819
    .local v0, "item":[Ljava/lang/String;
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 820
    .local v2, "listItem":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 822
    .local v1, "itemArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    iget-boolean v5, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z

    invoke-direct {v4, p0, v1, v5}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Z)V

    iput-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    .line 824
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    new-instance v5, Lcom/sec/android/automotive/drivelink/home/HomeActivity$9;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$9;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->onOnSetItemClickListener(Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog$ListViewDialogSelectListener;)V

    .line 839
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    new-instance v5, Lcom/sec/android/automotive/drivelink/home/HomeActivity$10;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$10;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 855
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    new-instance v5, Lcom/sec/android/automotive/drivelink/home/HomeActivity$11;

    invoke-direct {v5, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$11;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 866
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x35

    invoke-virtual {v4, v5}, Landroid/view/Window;->setGravity(I)V

    .line 868
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 869
    .local v3, "params":Landroid/view/WindowManager$LayoutParams;
    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 870
    invoke-direct {p0, v6, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->convertDpToPixel(FLandroid/content/Context;)F

    move-result v4

    float-to-int v4, v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 871
    const/high16 v4, 0x42400000    # 48.0f

    invoke-direct {p0, v4, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->convertDpToPixel(FLandroid/content/Context;)F

    move-result v4

    float-to-int v4, v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 873
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 875
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 876
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 875
    invoke-virtual {v4, v5}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 877
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->show()V

    .line 879
    iget-boolean v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mIsNowDriving:Z

    if-eqz v4, :cond_0

    .line 880
    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 881
    sget-object v4, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "update drving status_TTS"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    const v4, 0x7f0a01bf

    invoke-virtual {p0, v4}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected CarAppFinishFunc()V
    .locals 2

    .prologue
    .line 1504
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "CarAppFinishFunc event Home"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1510
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBackkeyPressed:Z

    if-nez v0, :cond_0

    .line 1511
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->exitApplication()V

    .line 1512
    :cond_0
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->CarAppFinishFunc()V

    .line 1513
    return-void
.end method

.method public activateBluetooth()V
    .locals 3

    .prologue
    .line 494
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 495
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    .line 497
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 498
    const-string/jumbo v1, "PREF_USER_BT_PREVIOUS_STATE"

    const/4 v2, 0x1

    .line 497
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->put(Ljava/lang/String;Z)V

    .line 504
    :cond_0
    return-void
.end method

.method public exitApplication()V
    .locals 4

    .prologue
    .line 1271
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V

    .line 1273
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->cancelCheckUpdate()Z

    .line 1275
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->restoreRingerMode(Landroid/content/Context;)Z

    .line 1276
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->restoreNfcMode(Landroid/content/Context;)Z

    .line 1295
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->destroyInstance()V

    .line 1297
    :try_start_0
    const-string/jumbo v0, "e"

    const-string/jumbo v1, "[CarMode]"

    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "exitApplication"

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->terminate(Landroid/content/Context;)V

    .line 1301
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->setWhenOnDestoryed()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1305
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->finish()V

    .line 1307
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->terminate()V

    .line 1309
    return-void

    .line 1302
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 982
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onActivityResult start"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "requestCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " resultCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    packed-switch p1, :pswitch_data_0

    .line 1001
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onActivityResult end"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1003
    return-void

    .line 986
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 988
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    .line 989
    const-string/jumbo v1, "onActivityResult [REQUEST_LOAD_SIGN_IN_SCREEN] Sign in SUCCESS"

    .line 988
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 992
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v0

    .line 994
    const-string/jumbo v1, "PREF_ACCEPT_DISCLAIMER_PLACE_ON"

    .line 995
    const/4 v2, 0x0

    .line 993
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;Z)Z

    move-result v0

    .line 995
    if-nez v0, :cond_0

    .line 996
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->disclaimer()V

    goto :goto_0

    .line 984
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1231
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1235
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->stopDmFlow()V

    .line 1238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBackkeyPressed:Z

    .line 1239
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->exitApplication()V

    .line 1241
    return-void
.end method

.method public onClickListener(Z)V
    .locals 0
    .param p1, "accept"    # Z

    .prologue
    .line 1494
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1026
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1027
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeAllVoiceUiUpdater()V

    .line 1029
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->isErrorState()Z

    move-result v1

    .line 1030
    .local v1, "isErrorState":Z
    const/4 v0, 0x0

    .line 1031
    .local v0, "errorCS":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 1032
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->getErrorCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1035
    :cond_0
    const v2, 0x7f03000f

    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setContentView(I)V

    .line 1036
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->initHomeLayout()V

    .line 1037
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->initActionBarIcon()V

    .line 1039
    if-eqz v1, :cond_1

    .line 1040
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v2, v0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->displayError(Ljava/lang/CharSequence;)V

    .line 1042
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 195
    const-string/jumbo v6, "e"

    const-string/jumbo v7, "[CarMode]"

    sget-object v8, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "onCreate "

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 198
    const v6, 0x7f03000f

    invoke-virtual {p0, v6}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setContentView(I)V

    .line 199
    iput-object p0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mContext:Landroid/content/Context;

    .line 238
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->initHomeLayout()V

    .line 246
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->hasInstance()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 247
    invoke-static {}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->destroyInstance()V

    .line 249
    :cond_0
    invoke-static {p0}, Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/notification/NotificationCenter;

    .line 258
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setDefaultValueSettingsPoi()V

    .line 261
    sget-boolean v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isFinished:Z

    if-eqz v6, :cond_1

    .line 262
    const/4 v6, 0x0

    sput-boolean v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isFinished:Z

    .line 269
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->checkUserMode()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 270
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->checkUpdate()Z

    .line 273
    :cond_1
    new-instance v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$3;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 322
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v6

    .line 323
    iget-object v7, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    .line 322
    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->addDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 325
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->setMessageMessageUpdateListener()V

    .line 326
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->updateInboxList()V

    .line 328
    new-instance v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity$4;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$4;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    .line 338
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    .line 339
    const-string/jumbo v8, "android.intent.action.SCREEN_OFF"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 338
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 341
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setLanguageChangedReceiver()V

    .line 343
    new-instance v6, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;-><init>()V

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBlockingCallReceiver:Landroid/content/BroadcastReceiver;

    .line 344
    sget-object v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "register - BlockingPhoneStateReceiver"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBlockingCallReceiver:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    .line 346
    const-string/jumbo v8, "android.intent.action.PHONE_STATE"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 345
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 348
    new-instance v6, Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;

    invoke-direct {v6}, Lcom/sec/android/automotive/drivelink/common/receiver/SafetyModeCheckReceiver;-><init>()V

    iput-object v6, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

    .line 349
    sget-object v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "register - mSafetyModeCheckReceiver"

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v6, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    .line 351
    const-string/jumbo v8, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 350
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 365
    const/high16 v6, 0x7f040000

    const v7, 0x7f040025

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->overridePendingTransition(II)V

    .line 366
    new-instance v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity$5;

    invoke-direct {v6, p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity$5;-><init>(Lcom/sec/android/automotive/drivelink/home/HomeActivity;)V

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->setTaskManagerListener(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;)V

    .line 374
    sget-wide v4, Lcom/sec/android/automotive/drivelink/DLApplication;->startTime:J

    .line 375
    .local v4, "startTime":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 376
    .local v0, "endTime":J
    sub-long v6, v0, v4

    const-wide/32 v8, 0xf4240

    div-long v2, v6, v8

    .line 378
    .local v2, "measuredTime":J
    sget-object v6, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Launch time: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 972
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 973
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->invalidateOptionsMenu()V

    .line 974
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->stopDmFlow()V

    .line 975
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->showListDialog()V

    .line 976
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 896
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 897
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isFinished:Z

    .line 900
    :cond_0
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->setTaskManagerListener(Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver$OnTaskManagerReceiverListener;)V

    .line 902
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;

    move-result-object v0

    .line 903
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDrivingChangeListener:Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater;->removeDrivingChangeListener(Lcom/sec/android/automotive/drivelink/common/DLServiceDrivingChangeUpdater$DrivingChangeListener;)V

    .line 905
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeAllVoiceUiUpdater()V

    .line 907
    const-string/jumbo v0, "e"

    const-string/jumbo v1, "[CarMode]"

    sget-object v2, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onDestroy "

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->Debug(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerDestroy()V

    .line 911
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 912
    const-string/jumbo v1, "vibrate_when_ringing"

    .line 913
    sget v2, Lcom/sec/android/automotive/drivelink/common/receiver/BlockingPhoneStateReceiver;->VIBRATE_WHEN_RINGING:I

    .line 911
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 915
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mLcdOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 916
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mLanguageChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 917
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Unregister - BlockingPhoneStateReceiver"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mBlockingCallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 919
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mSafetyModeCheckReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 925
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 926
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestStopDrivingStatusMonitoring(Landroid/content/Context;)V

    .line 927
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stop driving manager"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    const v0, 0xddd5

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->finishActivity(I)V

    .line 933
    invoke-static {v4}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 939
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onDestroy()V

    .line 947
    return-void
.end method

.method public onFlowCommandCustom(Ljava/lang/String;)Z
    .locals 4
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 542
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getHomeMode()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "HOME_MODE_MULTIWINDOW"

    if-ne v1, v2, :cond_1

    .line 559
    :cond_0
    :goto_0
    return v3

    .line 546
    :cond_1
    const-string/jumbo v1, "DM_CANCEL"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    sget-object v1, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Command Cancel"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsCanceledtoPauseMusic()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 550
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 551
    const-string/jumbo v1, "DM_MAIN"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 553
    :cond_2
    const v1, 0x7f0a01f7

    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 554
    .local v0, "cancelTts":Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->displayError(Ljava/lang/CharSequence;)V

    .line 555
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFlowCommandReturnMain()V
    .locals 0

    .prologue
    .line 1500
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1166
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 1171
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->handleShareIntent(Landroid/content/Intent;)V

    .line 1173
    if-eqz p1, :cond_0

    .line 1174
    const-string/jumbo v1, "startMusic"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1176
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 1177
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1178
    const v2, 0x7f0a0032

    .line 1177
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 1179
    const-string/jumbo v1, "DM_MUSIC_PLAYER"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1196
    .end local v0    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1046
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onPause()V

    .line 1047
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isForeground:Z

    .line 1049
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->pause(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)V

    .line 1050
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V

    .line 1052
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isActionBarInitiated:Z

    .line 1054
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    if-eqz v0, :cond_0

    .line 1055
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;->dismiss()V

    .line 1056
    iput-object v3, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mDialog:Lcom/sec/android/automotive/drivelink/home/HomeListViewDialog;

    .line 1058
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->startTransitionTime:J

    .line 1059
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1319
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1320
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1063
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onResume()V

    .line 1065
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/DLConnection;->hasInternet()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->setHasInternetLayout(Z)V

    .line 1067
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isForeground:Z

    .line 1077
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->initActionBarIcon()V

    .line 1078
    invoke-direct {p0, v2}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setBtnClickable(Z)V

    .line 1080
    const-string/jumbo v0, "HOME_MODE_BASIC"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->resetMainFlow(Ljava/lang/String;)V

    .line 1081
    const-string/jumbo v0, "DM_MAIN"

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setActivityFlowID(Ljava/lang/String;)V

    .line 1083
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsWakeup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1084
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsCanceledtoMain()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1086
    :cond_0
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Wake up or Canceled to Main"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    const-string/jumbo v0, "DM_MAIN"

    invoke-static {v0, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 1092
    :goto_0
    sget-boolean v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->hasUpdate:Z

    if-eqz v0, :cond_1

    .line 1093
    sget-object v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "There is update.!!"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->goUpdateActivity()V

    .line 1104
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->isActionBarInitiated:Z

    .line 1105
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->checkTtsForUnreadMessage()V

    .line 1110
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onUiShown()V

    .line 1117
    return-void

    .line 1089
    :cond_2
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->resume(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1313
    invoke-super {p0, p1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1315
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 1336
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onStop()V

    .line 1337
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 952
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->onUserLeaveHint()V

    .line 968
    return-void
.end method

.method protected reloadLayout()Z
    .locals 1

    .prologue
    .line 1324
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->removeAllVoiceUiUpdater()V

    .line 1326
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->setContentView(I)V

    .line 1327
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->initHomeLayout()V

    .line 1328
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->initActionBarIcon()V

    .line 1330
    const/4 v0, 0x0

    return v0
.end method

.method protected setDayMode()V
    .locals 4

    .prologue
    const v3, 0x7f08002f

    const v2, 0x7f0201b3

    .line 1564
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setDayMode()V

    .line 1565
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->setDayMode()V

    .line 1566
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mHomeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08000a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1568
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201bb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1569
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1570
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201b7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1571
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1572
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201bd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1573
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1574
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201b9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1575
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1577
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1579
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1581
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1583
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1585
    return-void
.end method

.method protected setNightMode()V
    .locals 4

    .prologue
    const v3, 0x7f080030

    const v2, 0x7f0201b4

    .line 1539
    invoke-super {p0}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->setNightMode()V

    .line 1540
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mActionBar:Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/home/VoiceHomeActionBarLayout;->setNightMode()V

    .line 1541
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mHomeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1543
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1544
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1545
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201b8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1546
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1547
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201be

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1548
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1549
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicIcon:Landroid/widget/ImageView;

    const v1, 0x7f0201ba

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1550
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1552
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mPhoneName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1554
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMessageName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1556
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mNavigationName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1558
    iget-object v0, p0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->mMusicName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1560
    return-void
.end method
