.class public interface abstract Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMessageListener;
.super Ljava/lang/Object;
.source "DriveLinkServiceInterface.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkBaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDriveLinkMessageListener"
.end annotation


# virtual methods
.method public abstract onResponseRequestChangeMessageStatusToRead(Z)V
.end method

.method public abstract onResponseRequestInboxList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLInbox;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestIncommingMessageList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMessage;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onResponseRequestSendMessage(Z)V
.end method

.method public abstract onResponseRequestUnreadMessageCount(I)V
.end method

.method public abstract onResponseRequestUnreadMessageCountByInbox(I)V
.end method
