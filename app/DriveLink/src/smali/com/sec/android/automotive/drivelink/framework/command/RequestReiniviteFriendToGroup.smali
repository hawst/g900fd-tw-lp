.class public Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;
.super Lcom/sec/android/automotive/drivelink/framework/command/Request;
.source "RequestReiniviteFriendToGroup.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFriend:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

.field private mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "driveLinkServiceProvider"    # Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p4, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 19
    invoke-direct {p0, p2}, Lcom/sec/android/automotive/drivelink/framework/command/Request;-><init>(Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;)V

    .line 20
    iput-object p1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mContext:Landroid/content/Context;

    .line 21
    iput-object p4, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mFriend:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 22
    iput-object p3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 23
    return-void
.end method


# virtual methods
.method protected getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected postprocess()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mContext:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mFriend:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 45
    iput-object v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .line 46
    return-void
.end method

.method protected process()V
    .locals 4

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getLocationManager()Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mFriend:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/location/LocationManager;->requestReiniviteFriendToGroup(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)Z

    move-result v0

    .line 27
    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mResult:Z

    .line 29
    return-void
.end method

.method protected response()V
    .locals 4

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getOnDriveLinkLocationListener()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v0

    .line 35
    .local v0, "listener":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    if-eqz v0, :cond_0

    .line 36
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mResult:Z

    .line 37
    iget-object v2, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mGroup:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelink/framework/command/RequestReiniviteFriendToGroup;->mFriend:Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .line 36
    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;->onResponseRequestReinviteFriendToGroup(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V

    .line 39
    :cond_0
    return-void
.end method
