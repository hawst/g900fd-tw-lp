.class Lcom/inrix/sdk/UserManager$EmailOptions;
.super Ljava/lang/Object;
.source "UserManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/UserManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EmailOptions"
.end annotation


# instance fields
.field private email:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/UserManager$EmailOptions;->setEmail(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$EmailOptions;

    .line 42
    return-void
.end method


# virtual methods
.method getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$EmailOptions;->email:Ljava/lang/String;

    return-object v0
.end method

.method public setEmail(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$EmailOptions;
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$EmailOptions;->email:Ljava/lang/String;

    .line 56
    return-object p0
.end method
