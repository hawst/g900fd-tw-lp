.class public Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;
.super Ljava/lang/Object;
.source "LocationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/LocationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteLocationOptions"
.end annotation


# instance fields
.field private locationId:J


# direct methods
.method public constructor <init>(J)V
    .locals 2
    .param p1, "locationId"    # J

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 232
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x410

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 234
    :cond_0
    iput-wide p1, p0, Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;->locationId:J

    .line 235
    return-void
.end method


# virtual methods
.method public getLocationId()J
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p0, Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;->locationId:J

    return-wide v0
.end method
