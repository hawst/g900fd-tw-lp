.class Lcom/inrix/sdk/LocationsManager$3;
.super Ljava/lang/Object;
.source "LocationsManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/LocationsManager;->createLocation(Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/SingleLocationCollection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/LocationsManager;

.field final synthetic val$listener:Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$3;->this$0:Lcom/inrix/sdk/LocationsManager;

    iput-object p2, p0, Lcom/inrix/sdk/LocationsManager$3;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/SingleLocationCollection;)V
    .locals 2
    .param p1, "response"    # Lcom/inrix/sdk/model/SingleLocationCollection;

    .prologue
    .line 304
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$3;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;

    invoke-virtual {p1}, Lcom/inrix/sdk/model/SingleLocationCollection;->getLocation()Lcom/inrix/sdk/model/Location;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;->onResult(Ljava/lang/Object;)V

    .line 305
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 300
    check-cast p1, Lcom/inrix/sdk/model/SingleLocationCollection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/LocationsManager$3;->onResponse(Lcom/inrix/sdk/model/SingleLocationCollection;)V

    return-void
.end method
