.class public Lcom/inrix/sdk/UserManager$ChangePasswordOptions;
.super Ljava/lang/Object;
.source "UserManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/UserManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChangePasswordOptions"
.end annotation


# instance fields
.field private newPassword:Ljava/lang/String;

.field private oldPassword:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "oldPassword"    # Ljava/lang/String;
    .param p2, "newPassword"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->setNewPassword(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$ChangePasswordOptions;

    .line 98
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->setOldPassword(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$ChangePasswordOptions;

    .line 99
    return-void
.end method


# virtual methods
.method getNewPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->newPassword:Ljava/lang/String;

    return-object v0
.end method

.method getOldPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->oldPassword:Ljava/lang/String;

    return-object v0
.end method

.method public setNewPassword(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$ChangePasswordOptions;
    .locals 0
    .param p1, "newPassword"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->newPassword:Ljava/lang/String;

    .line 116
    return-object p0
.end method

.method public setOldPassword(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$ChangePasswordOptions;
    .locals 0
    .param p1, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->oldPassword:Ljava/lang/String;

    .line 107
    return-object p0
.end method
