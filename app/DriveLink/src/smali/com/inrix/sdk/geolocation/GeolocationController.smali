.class public Lcom/inrix/sdk/geolocation/GeolocationController;
.super Ljava/lang/Object;
.source "GeolocationController.java"

# interfaces
.implements Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;
    }
.end annotation


# static fields
.field private static instance:Lcom/inrix/sdk/geolocation/GeolocationController;


# instance fields
.field private fixListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;",
            ">;"
        }
    .end annotation
.end field

.field protected geolocationPriority:Ljava/lang/Integer;

.field private isActive:Z

.field private isCurrentlyDriving:Z

.field private isStoppedExplicitly:Z

.field private lastKnown:Landroid/location/Location;

.field private lastKnownBearing:F

.field private locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    .line 14
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnownBearing:F

    .line 15
    iput-boolean v1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isStoppedExplicitly:Z

    .line 17
    iput-boolean v1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isActive:Z

    .line 18
    iput-boolean v1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isCurrentlyDriving:Z

    .line 20
    iput-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->geolocationPriority:Ljava/lang/Integer;

    .line 27
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->fixListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/inrix/sdk/geolocation/GeolocationController;->instance:Lcom/inrix/sdk/geolocation/GeolocationController;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/inrix/sdk/geolocation/GeolocationController;

    invoke-direct {v0}, Lcom/inrix/sdk/geolocation/GeolocationController;-><init>()V

    sput-object v0, Lcom/inrix/sdk/geolocation/GeolocationController;->instance:Lcom/inrix/sdk/geolocation/GeolocationController;

    .line 34
    :cond_0
    sget-object v0, Lcom/inrix/sdk/geolocation/GeolocationController;->instance:Lcom/inrix/sdk/geolocation/GeolocationController;

    return-object v0
.end method


# virtual methods
.method public addOnFixListener(Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    .prologue
    .line 186
    if-nez p1, :cond_0

    .line 194
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    invoke-interface {p1, v0}, Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;->onLocationAcquired(Landroid/location/Location;)V

    goto :goto_0

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->fixListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getIsStoppedExplicitly()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isStoppedExplicitly:Z

    return v0
.end method

.method public getLastKnownBearing()F
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnownBearing:F

    return v0
.end method

.method public getLastKnownLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    return-object v0
.end method

.method public getLocationSource()Lcom/inrix/sdk/geolocation/IGeolocationSource;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    return-object v0
.end method

.method public getPriority()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->geolocationPriority:Ljava/lang/Integer;

    return-object v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isActive:Z

    return v0
.end method

.method public isDriving()Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isCurrentlyDriving:Z

    return v0
.end method

.method public onGeolocationChange(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 121
    if-nez p1, :cond_0

    .line 139
    :goto_0
    return-void

    .line 125
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    .line 128
    iget-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->hasBearing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 129
    iget-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getBearing()F

    move-result v2

    iput v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnownBearing:F

    .line 132
    :cond_1
    iget-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->fixListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 133
    iget-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->fixListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    .line 134
    .local v1, "listener":Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;
    iget-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    invoke-interface {v1, v2}, Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;->onLocationAcquired(Landroid/location/Location;)V

    goto :goto_1

    .line 136
    .end local v1    # "listener":Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;
    :cond_2
    iget-object v2, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->fixListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 138
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-static {}, Lcom/inrix/sdk/phs/PhsController;->getInstance()Lcom/inrix/sdk/phs/PhsController;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/inrix/sdk/phs/PhsController;->trackLocation(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public removeOnFixListener(Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    .prologue
    .line 202
    if-nez p1, :cond_0

    .line 206
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->fixListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public resetLastKnownLocation()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnown:Landroid/location/Location;

    .line 168
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->lastKnownBearing:F

    .line 169
    return-void
.end method

.method public setDriving(Z)V
    .locals 1
    .param p1, "isDriving"    # Z

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isCurrentlyDriving:Z

    if-ne p1, v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 225
    :cond_0
    iput-boolean p1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isCurrentlyDriving:Z

    .line 226
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isCurrentlyDriving:Z

    if-eqz v0, :cond_1

    .line 227
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/geolocation/GeolocationController;->setPriority(Ljava/lang/Integer;)V

    .line 228
    invoke-virtual {p0}, Lcom/inrix/sdk/geolocation/GeolocationController;->startLocationTracking()V

    goto :goto_0

    .line 230
    :cond_1
    invoke-virtual {p0}, Lcom/inrix/sdk/geolocation/GeolocationController;->stopLocationTracking()V

    goto :goto_0
.end method

.method public setLocationSource(Lcom/inrix/sdk/geolocation/IGeolocationSource;)V
    .locals 1
    .param p1, "locationSource"    # Lcom/inrix/sdk/geolocation/IGeolocationSource;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    invoke-interface {v0}, Lcom/inrix/sdk/geolocation/IGeolocationSource;->deactivate()V

    .line 49
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    .line 50
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->geolocationPriority:Ljava/lang/Integer;

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/geolocation/GeolocationController;->setPriority(Ljava/lang/Integer;)V

    .line 51
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isActive:Z

    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/inrix/sdk/geolocation/GeolocationController;->startLocationTracking()V

    .line 54
    :cond_1
    return-void
.end method

.method public setPriority(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "priority"    # Ljava/lang/Integer;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->geolocationPriority:Ljava/lang/Integer;

    .line 67
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    instance-of v0, v0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    check-cast v0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;

    iget-object v1, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->geolocationPriority:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->setPriority(Ljava/lang/Integer;)V

    .line 72
    :cond_0
    return-void
.end method

.method public declared-synchronized startLocationTracking()V
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isActive:Z

    .line 100
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    invoke-interface {v0, p0}, Lcom/inrix/sdk/geolocation/IGeolocationSource;->activate(Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;)V

    .line 103
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isStoppedExplicitly:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopLocationTracking()V
    .locals 1

    .prologue
    .line 112
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isActive:Z

    .line 113
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->locationSource:Lcom/inrix/sdk/geolocation/IGeolocationSource;

    invoke-interface {v0}, Lcom/inrix/sdk/geolocation/IGeolocationSource;->deactivate()V

    .line 116
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/inrix/sdk/geolocation/GeolocationController;->isStoppedExplicitly:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
