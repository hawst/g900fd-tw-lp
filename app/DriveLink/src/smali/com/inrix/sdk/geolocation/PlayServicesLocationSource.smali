.class public Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;
.super Ljava/lang/Object;
.source "PlayServicesLocationSource.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/gms/location/LocationListener;
.implements Lcom/inrix/sdk/geolocation/IGeolocationSource;


# static fields
.field private static final DEFAULT_PRIORITY:I = 0x64

.field private static final FASTEST_INTERVAL:J = 0x3e8L

.field private static final FASTEST_INTERVAL_IN_SECONDS:I = 0x1

.field private static final MILLISECONDS_PER_SECOND:I = 0x3e8

.field private static final UPDATE_INTERVAL:J = 0x2710L

.field public static final UPDATE_INTERVAL_IN_SECONDS:I = 0xa


# instance fields
.field private callback:Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;

.field private context:Landroid/content/Context;

.field protected isActive:Z

.field private locationClient:Lcom/google/android/gms/location/LocationClient;

.field protected locationRequest:Lcom/google/android/gms/location/LocationRequest;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isActive:Z

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->context:Landroid/content/Context;

    .line 42
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->create()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    .line 43
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->setFastestInterval(J)Lcom/google/android/gms/location/LocationRequest;

    .line 44
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {p0}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->getDefaultPriority()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->setPriority(I)Lcom/google/android/gms/location/LocationRequest;

    .line 45
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->setInterval(J)Lcom/google/android/gms/location/LocationRequest;

    .line 46
    new-instance v0, Lcom/google/android/gms/location/LocationClient;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/location/LocationClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iput-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    .line 47
    return-void
.end method

.method private isPlayServicesAvailable()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized activate(Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    const-string/jumbo v0, "Google Play Services are not available on this device! Unable to start geolocation tracking!"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 56
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->callback:Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;

    .line 57
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isActive:Z

    if-nez v0, :cond_0

    .line 60
    invoke-direct {p0}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isPlayServicesAvailable()Z

    move-result v0

    if-nez v0, :cond_2

    .line 61
    const-string/jumbo v0, "Location services not available"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 64
    :cond_2
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isActive:Z

    .line 65
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationClient;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    const-string/jumbo v0, "Activate location source"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationClient;->connect()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized deactivate()V
    .locals 1

    .prologue
    .line 106
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->callback:Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;

    .line 107
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 118
    :goto_0
    monitor-exit p0

    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isActive:Z

    .line 111
    const-string/jumbo v0, "Deactivate location source"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    const-string/jumbo v0, "Remove location updates"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/location/LocationClient;->removeLocationUpdates(Lcom/google/android/gms/location/LocationListener;)V

    .line 116
    :cond_1
    const-string/jumbo v0, "Send request to disconnect"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationClient;->disconnect()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDefaultPriority()I
    .locals 1

    .prologue
    .line 101
    const/16 v0, 0x64

    return v0
.end method

.method public getLocationClient()Lcom/google/android/gms/location/LocationClient;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    return-object v0
.end method

.method public declared-synchronized onConnected(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "connectionHint"    # Landroid/os/Bundle;

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "LocationSource connected"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 133
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 143
    :goto_0
    monitor-exit p0

    return-void

    .line 136
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    if-eqz v0, :cond_1

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Request location updates with priority: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->getPriority()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n\tUpdate interval: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->getInterval()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    iget-object v1, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/location/LocationClient;->requestLocationUpdates(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/LocationListener;)V

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationClient:Lcom/google/android/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationClient;->getLastLocation()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->onLocationChanged(Landroid/location/Location;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LocationClient connection failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 147
    const-string/jumbo v0, "LocationSource disconnected"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 152
    if-nez p1, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Location update: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->callback:Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->callback:Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;->onGeolocationChange(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public resetPriority()V
    .locals 1

    .prologue
    .line 97
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->setPriority(Ljava/lang/Integer;)V

    .line 98
    return-void
.end method

.method public declared-synchronized setPriority(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "priority"    # Ljava/lang/Integer;

    .prologue
    .line 78
    monitor-enter p0

    if-nez p1, :cond_0

    .line 79
    :try_start_0
    invoke-virtual {p0}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->getDefaultPriority()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 82
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/location/LocationRequest;->getPriority()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v0, v1, :cond_2

    .line 91
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 85
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Set prio: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->locationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->setPriority(I)Lcom/google/android/gms/location/LocationRequest;

    .line 87
    iget-boolean v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->isActive:Z

    if-eqz v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->deactivate()V

    .line 89
    iget-object v0, p0, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->callback:Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;->activate(Lcom/inrix/sdk/geolocation/IOnGeolocationChangeListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
