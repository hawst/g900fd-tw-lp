.class public Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;
.super Lcom/inrix/sdk/ParkingManager$ParkingOptions;
.source "ParkingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/ParkingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParkingInBoxOptions"
.end annotation


# instance fields
.field private corner1:Lcom/inrix/sdk/model/GeoPoint;

.field private corner2:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 0
    .param p1, "corner1"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "corner2"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/inrix/sdk/ParkingManager$ParkingOptions;-><init>()V

    .line 293
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->setCorner1(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;

    .line 294
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->setCorner2(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;

    .line 295
    return-void
.end method


# virtual methods
.method getCorner1()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->corner1:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method getCorner2()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->corner2:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public setCorner1(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;
    .locals 2
    .param p1, "corner"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 307
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 310
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->corner1:Lcom/inrix/sdk/model/GeoPoint;

    .line 311
    return-object p0
.end method

.method public setCorner2(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;
    .locals 2
    .param p1, "corner"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 323
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 326
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->corner2:Lcom/inrix/sdk/model/GeoPoint;

    .line 327
    return-object p0
.end method
