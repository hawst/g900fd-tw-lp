.class Lcom/inrix/sdk/RouteManager$2;
.super Ljava/lang/Object;
.source "RouteManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/RouteManager;->requestTravelTimes(Lcom/inrix/sdk/RouteManager$TravelTimeOptions;Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/RouteManager;

.field final synthetic val$listener:Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/RouteManager;Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$2;->this$0:Lcom/inrix/sdk/RouteManager;

    iput-object p2, p0, Lcom/inrix/sdk/RouteManager$2;->val$listener:Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 431
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$2;->val$listener:Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 432
    return-void
.end method
