.class public Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;
.super Lcom/inrix/sdk/ParkingManager$ParkingOptions;
.source "ParkingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/ParkingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParkingInfoOptions"
.end annotation


# instance fields
.field private ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 348
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/inrix/sdk/ParkingManager$ParkingOptions;-><init>()V

    .line 349
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->setIds(Ljava/util/List;)Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;

    .line 350
    return-void
.end method


# virtual methods
.method getIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->ids:Ljava/util/List;

    return-object v0
.end method

.method public setIds(Ljava/util/List;)Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;"
        }
    .end annotation

    .prologue
    .line 362
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3f0

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 365
    :cond_1
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->ids:Ljava/util/List;

    .line 366
    return-object p0
.end method
