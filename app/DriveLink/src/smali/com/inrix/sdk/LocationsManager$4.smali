.class Lcom/inrix/sdk/LocationsManager$4;
.super Ljava/lang/Object;
.source "LocationsManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/LocationsManager;->createLocation(Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/LocationsManager;

.field final synthetic val$listener:Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$4;->this$0:Lcom/inrix/sdk/LocationsManager;

    iput-object p2, p0, Lcom/inrix/sdk/LocationsManager$4;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$4;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 311
    return-void
.end method
