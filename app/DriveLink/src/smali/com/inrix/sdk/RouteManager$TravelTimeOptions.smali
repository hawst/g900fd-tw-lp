.class public Lcom/inrix/sdk/RouteManager$TravelTimeOptions;
.super Ljava/lang/Object;
.source "RouteManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/RouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TravelTimeOptions"
.end annotation


# instance fields
.field private arrivalTime:Ljava/util/Date;

.field private departureTime:Ljava/util/Date;

.field private route:Lcom/inrix/sdk/model/Route;

.field private travelTimeCount:I

.field private travelTimeInterval:I


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/Route;II)V
    .locals 1
    .param p1, "route"    # Lcom/inrix/sdk/model/Route;
    .param p2, "travelTimeCount"    # I
    .param p3, "travelTimeInterval"    # I

    .prologue
    const/4 v0, 0x0

    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 505
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->route:Lcom/inrix/sdk/model/Route;

    .line 506
    iput p2, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeCount:I

    .line 507
    iput p3, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeInterval:I

    .line 508
    iput-object v0, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->departureTime:Ljava/util/Date;

    .line 509
    iput-object v0, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->arrivalTime:Ljava/util/Date;

    .line 510
    return-void
.end method


# virtual methods
.method public getArrivalTimeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->arrivalTime:Ljava/util/Date;

    invoke-static {v0}, Lcom/inrix/sdk/utils/InrixDateUtils;->getStringFromDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDepartureTimeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->departureTime:Ljava/util/Date;

    invoke-static {v0}, Lcom/inrix/sdk/utils/InrixDateUtils;->getStringFromDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRoute()Lcom/inrix/sdk/model/Route;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->route:Lcom/inrix/sdk/model/Route;

    return-object v0
.end method

.method public getTravelTimeCount()I
    .locals 1

    .prologue
    .line 584
    iget v0, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeCount:I

    return v0
.end method

.method public getTravelTimeInterval()I
    .locals 1

    .prologue
    .line 602
    iget v0, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeInterval:I

    return v0
.end method

.method public isRouteValid()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 652
    iget-object v1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->route:Lcom/inrix/sdk/model/Route;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->route:Lcom/inrix/sdk/model/Route;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/Route;->getPoints()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->route:Lcom/inrix/sdk/model/Route;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/Route;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 656
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTravelTimeCountValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 622
    iget v1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeCount:I

    if-lt v1, v0, :cond_0

    iget v1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeCount:I

    const/16 v2, 0x60

    if-le v1, v2, :cond_1

    .line 624
    :cond_0
    const/4 v0, 0x0

    .line 626
    :cond_1
    return v0
.end method

.method public isTravelTimeIntervalValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 636
    iget v1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeInterval:I

    if-lt v1, v0, :cond_0

    iget v1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeInterval:I

    const/16 v2, 0x5a0

    if-le v1, v2, :cond_1

    .line 638
    :cond_0
    const/4 v0, 0x0

    .line 640
    :cond_1
    return v0
.end method

.method public setArrivalTime(Ljava/util/Date;)V
    .locals 0
    .param p1, "arrivalTime"    # Ljava/util/Date;

    .prologue
    .line 575
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->arrivalTime:Ljava/util/Date;

    .line 576
    return-void
.end method

.method public setDepartureTime(Ljava/util/Date;)V
    .locals 0
    .param p1, "departureTime"    # Ljava/util/Date;

    .prologue
    .line 551
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->departureTime:Ljava/util/Date;

    .line 552
    return-void
.end method

.method public setRoute(Lcom/inrix/sdk/model/Route;)V
    .locals 0
    .param p1, "route"    # Lcom/inrix/sdk/model/Route;

    .prologue
    .line 528
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->route:Lcom/inrix/sdk/model/Route;

    .line 529
    return-void
.end method

.method public setTravelTimeCount(I)V
    .locals 0
    .param p1, "travelTimeCount"    # I

    .prologue
    .line 593
    iput p1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeCount:I

    .line 594
    return-void
.end method

.method public setTravelTimeInterval(I)V
    .locals 0
    .param p1, "travelTimeInterval"    # I

    .prologue
    .line 612
    iput p1, p0, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->travelTimeInterval:I

    .line 613
    return-void
.end method
