.class public Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;
.super Lcom/inrix/sdk/GasStationManager$GasStationsOptions;
.source "GasStationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/GasStationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GasStationsBoxOptions"
.end annotation


# instance fields
.field private boxEnd:Lcom/inrix/sdk/model/GeoPoint;

.field private boxStart:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 0
    .param p1, "boxStart"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "boxEnd"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 633
    invoke-direct {p0}, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;-><init>()V

    .line 634
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxStart:Lcom/inrix/sdk/model/GeoPoint;

    .line 635
    iput-object p2, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxEnd:Lcom/inrix/sdk/model/GeoPoint;

    .line 636
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;II)V
    .locals 0
    .param p1, "boxStart"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "boxEnd"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p3, "outputFields"    # I
    .param p4, "productTypes"    # I

    .prologue
    .line 647
    invoke-direct {p0, p3, p4}, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;-><init>(II)V

    .line 648
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxStart:Lcom/inrix/sdk/model/GeoPoint;

    .line 649
    iput-object p2, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxEnd:Lcom/inrix/sdk/model/GeoPoint;

    .line 650
    return-void
.end method


# virtual methods
.method public getBoxEnd()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxEnd:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public getBoxStart()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxStart:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public isEndValid()Z
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxEnd:Lcom/inrix/sdk/model/GeoPoint;

    invoke-static {v0}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    return v0
.end method

.method public isStartValid()Z
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxStart:Lcom/inrix/sdk/model/GeoPoint;

    invoke-static {v0}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    return v0
.end method

.method public setBoxEnd(Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 0
    .param p1, "boxEnd"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 688
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxEnd:Lcom/inrix/sdk/model/GeoPoint;

    .line 689
    return-void
.end method

.method public setBoxStart(Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 0
    .param p1, "boxStart"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 664
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->boxStart:Lcom/inrix/sdk/model/GeoPoint;

    .line 665
    return-void
.end method
