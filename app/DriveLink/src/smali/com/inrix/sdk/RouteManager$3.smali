.class Lcom/inrix/sdk/RouteManager$3;
.super Ljava/lang/Object;
.source "RouteManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/RouteManager;->requestRoutes(Lcom/inrix/sdk/RouteManager$RouteOptions;Lcom/inrix/sdk/RouteManager$IRouteResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/RoutesCollection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/RouteManager;

.field final synthetic val$listener:Lcom/inrix/sdk/RouteManager$IRouteResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/RouteManager;Lcom/inrix/sdk/RouteManager$IRouteResponseListener;)V
    .locals 0

    .prologue
    .line 699
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$3;->this$0:Lcom/inrix/sdk/RouteManager;

    iput-object p2, p0, Lcom/inrix/sdk/RouteManager$3;->val$listener:Lcom/inrix/sdk/RouteManager$IRouteResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/RoutesCollection;)V
    .locals 1
    .param p1, "response"    # Lcom/inrix/sdk/model/RoutesCollection;

    .prologue
    .line 702
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$3;->val$listener:Lcom/inrix/sdk/RouteManager$IRouteResponseListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/RouteManager$IRouteResponseListener;->onResult(Ljava/lang/Object;)V

    .line 703
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 699
    check-cast p1, Lcom/inrix/sdk/model/RoutesCollection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/RouteManager$3;->onResponse(Lcom/inrix/sdk/model/RoutesCollection;)V

    return-void
.end method
