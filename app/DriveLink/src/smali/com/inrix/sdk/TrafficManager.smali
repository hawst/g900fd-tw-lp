.class public final Lcom/inrix/sdk/TrafficManager;
.super Ljava/lang/Object;
.source "TrafficManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;,
        Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method


# virtual methods
.method public final getTrafficQuality(Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 7
    .param p1, "options"    # Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;

    .prologue
    .line 62
    if-nez p2, :cond_0

    .line 63
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 66
    :cond_0
    if-nez p1, :cond_1

    .line 67
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 70
    :cond_1
    new-instance v0, Lcom/inrix/sdk/network/request/TrafficQualityRequest;

    invoke-virtual {p1}, Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;->getLocation()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;->getLocation()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v3

    new-instance v5, Lcom/inrix/sdk/TrafficManager$1;

    invoke-direct {v5, p0, p2}, Lcom/inrix/sdk/TrafficManager$1;-><init>(Lcom/inrix/sdk/TrafficManager;Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;)V

    new-instance v6, Lcom/inrix/sdk/TrafficManager$2;

    invoke-direct {v6, p0, p2}, Lcom/inrix/sdk/TrafficManager$2;-><init>(Lcom/inrix/sdk/TrafficManager;Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;)V

    invoke-direct/range {v0 .. v6}, Lcom/inrix/sdk/network/request/TrafficQualityRequest;-><init>(DDLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 86
    .local v0, "request":Lcom/inrix/sdk/network/request/TrafficQualityRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 87
    return-object v0
.end method
