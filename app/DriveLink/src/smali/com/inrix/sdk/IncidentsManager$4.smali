.class Lcom/inrix/sdk/IncidentsManager$4;
.super Ljava/lang/Object;
.source "IncidentsManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/IncidentsManager;->getIncidentsInRadius(Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/IncidentsManager;

.field final synthetic val$listener:Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/IncidentsManager;Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/inrix/sdk/IncidentsManager$4;->this$0:Lcom/inrix/sdk/IncidentsManager;

    iput-object p2, p0, Lcom/inrix/sdk/IncidentsManager$4;->val$listener:Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 544
    iget-object v0, p0, Lcom/inrix/sdk/IncidentsManager$4;->val$listener:Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 545
    return-void
.end method
