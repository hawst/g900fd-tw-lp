.class public Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;
.super Lcom/inrix/sdk/IncidentsManager$IncidentOptions;
.source "IncidentsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/IncidentsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IncidentBoxOptions"
.end annotation


# instance fields
.field private corner1:Lcom/inrix/sdk/model/GeoPoint;

.field private corner2:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 0
    .param p1, "corner1"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "corner2"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 295
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;-><init>()V

    .line 296
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->setCorner1(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;

    .line 297
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->setCorner2(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;

    .line 298
    return-void
.end method


# virtual methods
.method getCorner1()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->corner1:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method getCorner2()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->corner2:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public setCorner1(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;
    .locals 2
    .param p1, "corner1"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 313
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 316
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->corner1:Lcom/inrix/sdk/model/GeoPoint;

    .line 317
    return-object p0
.end method

.method public setCorner2(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;
    .locals 2
    .param p1, "corner2"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 332
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 335
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->corner2:Lcom/inrix/sdk/model/GeoPoint;

    .line 336
    return-object p0
.end method
