.class Lcom/inrix/sdk/LocationsManager$5;
.super Ljava/lang/Object;
.source "LocationsManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/LocationsManager;->deleteLocation(Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/SingleLocationCollection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/LocationsManager;

.field final synthetic val$listener:Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$5;->this$0:Lcom/inrix/sdk/LocationsManager;

    iput-object p2, p0, Lcom/inrix/sdk/LocationsManager$5;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/SingleLocationCollection;)V
    .locals 3
    .param p1, "response"    # Lcom/inrix/sdk/model/SingleLocationCollection;

    .prologue
    .line 341
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$5;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;

    invoke-virtual {p1}, Lcom/inrix/sdk/model/SingleLocationCollection;->getLocation()Lcom/inrix/sdk/model/Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/model/Location;->getLocationId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;->onResult(Ljava/lang/Object;)V

    .line 343
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 337
    check-cast p1, Lcom/inrix/sdk/model/SingleLocationCollection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/LocationsManager$5;->onResponse(Lcom/inrix/sdk/model/SingleLocationCollection;)V

    return-void
.end method
