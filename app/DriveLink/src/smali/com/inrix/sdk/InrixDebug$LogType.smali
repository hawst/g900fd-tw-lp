.class final enum Lcom/inrix/sdk/InrixDebug$LogType;
.super Ljava/lang/Enum;
.source "InrixDebug.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/InrixDebug;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LogType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/InrixDebug$LogType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/InrixDebug$LogType;

.field public static final enum Debug:Lcom/inrix/sdk/InrixDebug$LogType;

.field public static final enum Error:Lcom/inrix/sdk/InrixDebug$LogType;

.field public static final enum Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

.field public static final enum Warning:Lcom/inrix/sdk/InrixDebug$LogType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/inrix/sdk/InrixDebug$LogType;

    const-string/jumbo v1, "Error"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/InrixDebug$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    new-instance v0, Lcom/inrix/sdk/InrixDebug$LogType;

    const-string/jumbo v1, "Warning"

    invoke-direct {v0, v1, v3}, Lcom/inrix/sdk/InrixDebug$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Warning:Lcom/inrix/sdk/InrixDebug$LogType;

    new-instance v0, Lcom/inrix/sdk/InrixDebug$LogType;

    const-string/jumbo v1, "Debug"

    invoke-direct {v0, v1, v4}, Lcom/inrix/sdk/InrixDebug$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Debug:Lcom/inrix/sdk/InrixDebug$LogType;

    new-instance v0, Lcom/inrix/sdk/InrixDebug$LogType;

    const-string/jumbo v1, "Verbose"

    invoke-direct {v0, v1, v5}, Lcom/inrix/sdk/InrixDebug$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/inrix/sdk/InrixDebug$LogType;

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Warning:Lcom/inrix/sdk/InrixDebug$LogType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Debug:Lcom/inrix/sdk/InrixDebug$LogType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->$VALUES:[Lcom/inrix/sdk/InrixDebug$LogType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(I)Lcom/inrix/sdk/InrixDebug$LogType;
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 29
    packed-switch p0, :pswitch_data_0

    .line 39
    :pswitch_0
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    :goto_0
    return-object v0

    .line 31
    :pswitch_1
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    goto :goto_0

    .line 33
    :pswitch_2
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Warning:Lcom/inrix/sdk/InrixDebug$LogType;

    goto :goto_0

    .line 35
    :pswitch_3
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Debug:Lcom/inrix/sdk/InrixDebug$LogType;

    goto :goto_0

    .line 37
    :pswitch_4
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/InrixDebug$LogType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/InrixDebug$LogType;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/InrixDebug$LogType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->$VALUES:[Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-virtual {v0}, [Lcom/inrix/sdk/InrixDebug$LogType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/InrixDebug$LogType;

    return-object v0
.end method
