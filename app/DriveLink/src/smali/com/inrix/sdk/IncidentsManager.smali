.class public final Lcom/inrix/sdk/IncidentsManager;
.super Ljava/lang/Object;
.source "IncidentsManager.java"

# interfaces
.implements Lcom/inrix/sdk/IRefreshableActions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/IncidentsManager$5;,
        Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;,
        Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;,
        Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;,
        Lcom/inrix/sdk/IncidentsManager$IncidentOptions;,
        Lcom/inrix/sdk/IncidentsManager$ACTIONS;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/IRefreshableActions",
        "<",
        "Lcom/inrix/sdk/IncidentsManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final GET_INCIDENTS_INTERVAL:I = 0xb4

.field public static final INCIDENT_OUTPUT_FIELDS_ALL:I = 0xffff

.field public static final INCIDENT_OUTPUT_FIELDS_AREA:I = 0x400

.field public static final INCIDENT_OUTPUT_FIELDS_DELAY_IMPACT:I = 0x200

.field public static final INCIDENT_OUTPUT_FIELDS_ENDTIME:I = 0x100

.field public static final INCIDENT_OUTPUT_FIELDS_EVENT_CODE:I = 0x10

.field public static final INCIDENT_OUTPUT_FIELDS_ID:I = 0x1

.field public static final INCIDENT_OUTPUT_FIELDS_IMPACTING:I = 0x40

.field public static final INCIDENT_OUTPUT_FIELDS_LATLONG:I = 0x20

.field public static final INCIDENT_OUTPUT_FIELDS_RDS:I = 0x800

.field public static final INCIDENT_OUTPUT_FIELDS_SEVERITY:I = 0x8

.field public static final INCIDENT_OUTPUT_FIELDS_STARTTIME:I = 0x80

.field public static final INCIDENT_OUTPUT_FIELDS_TYPE:I = 0x4

.field public static final INCIDENT_OUTPUT_FIELDS_VERSION:I = 0x2

.field public static final INCIDENT_RESULT_TYPE_ALL:I = 0xffff

.field public static final INCIDENT_RESULT_TYPE_CONSTRUCTION:I = 0x2

.field public static final INCIDENT_RESULT_TYPE_EVENTS:I = 0x4

.field public static final INCIDENT_RESULT_TYPE_FLOW:I = 0x8

.field public static final INCIDENT_RESULT_TYPE_INCIDENTS:I = 0x1

.field public static final INCIDENT_RESULT_TYPE_POLICE:I = 0x10

.field public static final INCIDENT_RESULT_TYPE_WEATHER:I = 0x20

.field public static final INCIDENT_SOURCE_ALL:I = 0xff

.field public static final INCIDENT_SOURCE_COMMUNITY:I = 0x2

.field public static final INCIDENT_SOURCE_INRIXONLY:I = 0x1

.field public static final INCIDENT_TYPE_CONSTRUCTION:I = 0x1

.field public static final INCIDENT_TYPE_EVENT:I = 0x2

.field public static final INCIDENT_TYPE_FLOW:I = 0x3

.field public static final INCIDENT_TYPE_HAZARD:I = 0x8

.field public static final INCIDENT_TYPE_INCIDENT:I = 0x4

.field public static final INCIDENT_TYPE_POLICE:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    return-void
.end method

.method private final getIncidentOutputFieldsString(I)Ljava/lang/String;
    .locals 3
    .param p1, "fieldsFlag"    # I

    .prologue
    const v2, 0xffff

    .line 559
    and-int v1, p1, v2

    if-ne v1, v2, :cond_0

    .line 560
    const-string/jumbo v1, "all"

    .line 608
    :goto_0
    return-object v1

    .line 563
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 564
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    and-int/lit8 v1, p1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 565
    const-string/jumbo v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 568
    :cond_1
    and-int/lit8 v1, p1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 569
    const-string/jumbo v1, "version"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    :cond_2
    and-int/lit8 v1, p1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 573
    const-string/jumbo v1, "type"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 576
    :cond_3
    and-int/lit8 v1, p1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 577
    const-string/jumbo v1, "severity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 580
    :cond_4
    and-int/lit8 v1, p1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 581
    const-string/jumbo v1, "eventcode"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584
    :cond_5
    and-int/lit8 v1, p1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 585
    const-string/jumbo v1, "latlong"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    :cond_6
    and-int/lit8 v1, p1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 589
    const-string/jumbo v1, "impacting"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 592
    :cond_7
    and-int/lit16 v1, p1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 593
    const-string/jumbo v1, "starttime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 596
    :cond_8
    and-int/lit16 v1, p1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 597
    const-string/jumbo v1, "endtime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    :cond_9
    and-int/lit16 v1, p1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 601
    const-string/jumbo v1, "delayimpact"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 604
    :cond_a
    and-int/lit16 v1, p1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 605
    const-string/jumbo v1, "area"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 608
    :cond_b
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_c
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private final getIncidentResultTypeAsString(I)Ljava/lang/String;
    .locals 3
    .param p1, "typeFlags"    # I

    .prologue
    const v2, 0xffff

    .line 619
    and-int v1, p1, v2

    if-ne v1, v2, :cond_0

    .line 620
    const-string/jumbo v1, "all"

    .line 648
    :goto_0
    return-object v1

    .line 623
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 624
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    and-int/lit8 v1, p1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 625
    const-string/jumbo v1, "incidents"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 628
    :cond_1
    and-int/lit8 v1, p1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 629
    const-string/jumbo v1, "construction"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 632
    :cond_2
    and-int/lit8 v1, p1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 633
    const-string/jumbo v1, "events"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636
    :cond_3
    and-int/lit8 v1, p1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 637
    const-string/jumbo v1, "police"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 640
    :cond_4
    and-int/lit8 v1, p1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 641
    const-string/jumbo v1, "flow"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    :cond_5
    and-int/lit8 v1, p1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 645
    const-string/jumbo v1, "weather"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 648
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x0

    goto :goto_0

    :cond_7
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private final getIncidentSourceAsString(I)Ljava/lang/String;
    .locals 3
    .param p1, "sourceFlags"    # I

    .prologue
    .line 659
    and-int/lit16 v1, p1, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_0

    .line 660
    const-string/jumbo v1, "All"

    .line 672
    :goto_0
    return-object v1

    .line 663
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 664
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    and-int/lit8 v1, p1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 665
    const-string/jumbo v1, "inrixonly"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 668
    :cond_1
    and-int/lit8 v1, p1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 669
    const-string/jumbo v1, "community"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 672
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final getIncidentsInBox(Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 9
    .param p1, "listener"    # Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;
    .param p2, "requestParams"    # Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 442
    if-nez p2, :cond_0

    .line 443
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 445
    :cond_0
    if-nez p1, :cond_1

    .line 446
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 449
    :cond_1
    const/4 v6, 0x0

    .line 450
    .local v6, "severityValue":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->getSeverity()[Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 451
    const-string/jumbo v1, ","

    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->getSeverity()[Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 454
    :cond_2
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->getIncidentType()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/inrix/sdk/IncidentsManager;->getIncidentResultTypeAsString(I)Ljava/lang/String;

    move-result-object v3

    .line 457
    .local v3, "incidentsTypeValue":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->getIncidentSource()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/inrix/sdk/IncidentsManager;->getIncidentSourceAsString(I)Ljava/lang/String;

    move-result-object v4

    .line 460
    .local v4, "incidentsSourceValue":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->getIncidentOutputFields()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/inrix/sdk/IncidentsManager;->getIncidentOutputFieldsString(I)Ljava/lang/String;

    move-result-object v5

    .line 464
    .local v5, "incidentOutputFieldsValue":Ljava/lang/String;
    new-instance v0, Lcom/inrix/sdk/network/request/IncidentBoxRequest;

    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->getCorner1()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;->getCorner2()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v2

    new-instance v7, Lcom/inrix/sdk/IncidentsManager$1;

    invoke-direct {v7, p0, p1}, Lcom/inrix/sdk/IncidentsManager$1;-><init>(Lcom/inrix/sdk/IncidentsManager;Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;)V

    new-instance v8, Lcom/inrix/sdk/IncidentsManager$2;

    invoke-direct {v8, p0, p1}, Lcom/inrix/sdk/IncidentsManager$2;-><init>(Lcom/inrix/sdk/IncidentsManager;Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;)V

    invoke-direct/range {v0 .. v8}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;-><init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 485
    .local v0, "boxRequest":Lcom/inrix/sdk/network/request/IncidentBoxRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 486
    return-object v0
.end method

.method public final getIncidentsInRadius(Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 11
    .param p1, "listener"    # Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;
    .param p2, "requestParams"    # Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 503
    if-nez p2, :cond_0

    .line 504
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 506
    :cond_0
    if-nez p1, :cond_1

    .line 507
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 510
    :cond_1
    const/4 v8, 0x0

    .line 511
    .local v8, "severityValue":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getSeverity()[Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 512
    const-string/jumbo v1, ","

    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getSeverity()[Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 515
    :cond_2
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getIncidentType()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/inrix/sdk/IncidentsManager;->getIncidentResultTypeAsString(I)Ljava/lang/String;

    move-result-object v4

    .line 518
    .local v4, "incidentsTypeValue":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getIncidentSource()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/inrix/sdk/IncidentsManager;->getIncidentSourceAsString(I)Ljava/lang/String;

    move-result-object v5

    .line 521
    .local v5, "incidentsSourceValue":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getIncidentOutputFields()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/inrix/sdk/IncidentsManager;->getIncidentOutputFieldsString(I)Ljava/lang/String;

    move-result-object v6

    .line 525
    .local v6, "incidentOutputFieldsValue":Ljava/lang/String;
    new-instance v0, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;

    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getCenter()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getRadius()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v7

    invoke-virtual {v7}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->ordinal()I

    move-result v7

    new-instance v9, Lcom/inrix/sdk/IncidentsManager$3;

    invoke-direct {v9, p0, p1}, Lcom/inrix/sdk/IncidentsManager$3;-><init>(Lcom/inrix/sdk/IncidentsManager;Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;)V

    new-instance v10, Lcom/inrix/sdk/IncidentsManager$4;

    invoke-direct {v10, p0, p1}, Lcom/inrix/sdk/IncidentsManager$4;-><init>(Lcom/inrix/sdk/IncidentsManager;Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;)V

    invoke-direct/range {v0 .. v10}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;-><init>(Lcom/inrix/sdk/model/GeoPoint;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 547
    .local v0, "radiusRequest":Lcom/inrix/sdk/network/request/IncidentRadiusRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 548
    return-object v0
.end method

.method public getRefreshInterval(Lcom/inrix/sdk/IncidentsManager$ACTIONS;)I
    .locals 3
    .param p1, "action"    # Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    .prologue
    .line 173
    if-nez p1, :cond_0

    .line 174
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x412

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 176
    :cond_0
    sget-object v1, Lcom/inrix/sdk/IncidentsManager$5;->$SwitchMap$com$inrix$sdk$IncidentsManager$ACTIONS:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/IncidentsManager$ACTIONS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    .line 178
    const/16 v0, 0xb4

    .line 182
    .local v0, "interval":I
    return v0
.end method

.method public bridge synthetic getRefreshInterval(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/IncidentsManager;->getRefreshInterval(Lcom/inrix/sdk/IncidentsManager$ACTIONS;)I

    move-result v0

    return v0
.end method
