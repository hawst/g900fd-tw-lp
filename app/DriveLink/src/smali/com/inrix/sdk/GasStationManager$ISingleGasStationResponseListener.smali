.class public interface abstract Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;
.super Ljava/lang/Object;
.source "GasStationManager.java"

# interfaces
.implements Lcom/inrix/sdk/IDataResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/GasStationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ISingleGasStationResponseListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/IDataResponseListener",
        "<",
        "Lcom/inrix/sdk/model/SingleGasStationResult;",
        ">;"
    }
.end annotation
