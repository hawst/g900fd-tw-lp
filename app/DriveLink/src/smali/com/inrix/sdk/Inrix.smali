.class public final Lcom/inrix/sdk/Inrix;
.super Ljava/lang/Object;
.source "Inrix.java"


# static fields
.field public static final NO_VALUE:I = -0x80000000

.field private static activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

.field private static config:Lcom/inrix/sdk/InrixConfig;

.field private static instance:Lcom/inrix/sdk/Inrix;

.field private static isInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/inrix/sdk/Inrix;

    invoke-direct {v0}, Lcom/inrix/sdk/Inrix;-><init>()V

    sput-object v0, Lcom/inrix/sdk/Inrix;->instance:Lcom/inrix/sdk/Inrix;

    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/inrix/sdk/Inrix;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/inrix/sdk/Inrix;->instance:Lcom/inrix/sdk/Inrix;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 14
    sget-boolean v0, Lcom/inrix/sdk/Inrix;->isInitialized:Z

    return v0
.end method

.method public static getInstance()Lcom/inrix/sdk/Inrix;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/inrix/sdk/Inrix;->instance:Lcom/inrix/sdk/Inrix;

    return-object v0
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    const-string/jumbo v0, "1.0.02042014.0300.3935b77aae6f9c014f5d78d63292ffeb38c84e5d"

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    sget-boolean v0, Lcom/inrix/sdk/Inrix;->isInitialized:Z

    if-eqz v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-static {p0}, Lcom/inrix/sdk/InrixConfig;->loadDefaultOptions(Landroid/content/Context;)Lcom/inrix/sdk/InrixConfig;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/inrix/sdk/Inrix;->initialize(Landroid/content/Context;Lcom/inrix/sdk/InrixConfig;Z)V

    goto :goto_0
.end method

.method public static initialize(Landroid/content/Context;Lcom/inrix/sdk/InrixConfig;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "configuration"    # Lcom/inrix/sdk/InrixConfig;

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/inrix/sdk/Inrix;->initialize(Landroid/content/Context;Lcom/inrix/sdk/InrixConfig;Z)V

    .line 72
    return-void
.end method

.method private static initialize(Landroid/content/Context;Lcom/inrix/sdk/InrixConfig;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "configuration"    # Lcom/inrix/sdk/InrixConfig;
    .param p2, "autoEnableActivityRecognition"    # Z

    .prologue
    .line 87
    sget-object v1, Lcom/inrix/sdk/Inrix;->instance:Lcom/inrix/sdk/Inrix;

    monitor-enter v1

    .line 88
    :try_start_0
    sget-boolean v0, Lcom/inrix/sdk/Inrix;->isInitialized:Z

    if-eqz v0, :cond_0

    .line 89
    invoke-static {}, Lcom/inrix/sdk/Inrix;->shutdown()V

    .line 92
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->load(Landroid/content/Context;)V

    .line 93
    sput-object p1, Lcom/inrix/sdk/Inrix;->config:Lcom/inrix/sdk/InrixConfig;

    .line 94
    invoke-static {}, Lcom/inrix/sdk/Inrix;->validate()V

    .line 95
    sget-object v0, Lcom/inrix/sdk/Inrix;->config:Lcom/inrix/sdk/InrixConfig;

    invoke-virtual {v0}, Lcom/inrix/sdk/InrixConfig;->getLogLevel()I

    move-result v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->setLogLevel(I)V

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Inrix version: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/inrix/sdk/Inrix;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogV(Ljava/lang/String;)V

    .line 97
    const-string/jumbo v0, "Inrix revision: 3935b77aae6f9c014f5d78d63292ffeb38c84e5d"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogV(Ljava/lang/String;)V

    .line 98
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getHttpStack()Lcom/android/volley/toolbox/HttpStack;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/inrix/sdk/network/request/RequestExecutor;->init(Landroid/content/Context;Lcom/android/volley/toolbox/HttpStack;)V

    .line 101
    const/4 v0, 0x1

    sput-boolean v0, Lcom/inrix/sdk/Inrix;->isInitialized:Z

    .line 102
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/inrix/sdk/Inrix$1;

    invoke-direct {v1, p0, p2}, Lcom/inrix/sdk/Inrix$1;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 125
    return-void

    .line 102
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static initialize(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "strConfigFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-static {p0, p1}, Lcom/inrix/sdk/InrixConfig;->loadOptions(Landroid/content/Context;Ljava/lang/String;)Lcom/inrix/sdk/InrixConfig;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/inrix/sdk/Inrix;->initialize(Landroid/content/Context;Lcom/inrix/sdk/InrixConfig;)V

    .line 137
    return-void
.end method

.method private static initialize(Landroid/content/Context;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enableGeolocation"    # Z

    .prologue
    .line 56
    invoke-static {p0}, Lcom/inrix/sdk/InrixConfig;->loadDefaultOptions(Landroid/content/Context;)Lcom/inrix/sdk/InrixConfig;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/inrix/sdk/Inrix;->initialize(Landroid/content/Context;Lcom/inrix/sdk/InrixConfig;Z)V

    .line 59
    return-void
.end method

.method public static setDriving(Z)V
    .locals 1
    .param p0, "isDriving"    # Z

    .prologue
    .line 193
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/inrix/sdk/geolocation/GeolocationController;->setDriving(Z)V

    .line 194
    return-void
.end method

.method public static shutdown()V
    .locals 2

    .prologue
    .line 155
    sget-object v1, Lcom/inrix/sdk/Inrix;->instance:Lcom/inrix/sdk/Inrix;

    monitor-enter v1

    .line 156
    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/inrix/sdk/Inrix;->isInitialized:Z

    .line 157
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->shutdown()V

    .line 158
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->clear()V

    .line 159
    sget-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    invoke-virtual {v0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->stopActivityRecognition()V

    .line 161
    const/4 v0, 0x0

    sput-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    .line 163
    :cond_0
    invoke-static {}, Lcom/inrix/sdk/phs/PhsController;->getInstance()Lcom/inrix/sdk/phs/PhsController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/phs/PhsController;->stopTracking()V

    .line 164
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/geolocation/GeolocationController;->stopLocationTracking()V

    .line 165
    const/4 v0, 0x0

    sput-boolean v0, Lcom/inrix/sdk/Inrix;->isInitialized:Z

    .line 166
    monitor-exit v1

    .line 167
    return-void

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static startActivityRecognition(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 205
    sget-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    invoke-direct {v0, p0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    .line 208
    :cond_0
    sget-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    invoke-virtual {v0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->startActivityRecognition()V

    .line 209
    return-void
.end method

.method public static stopActivityRecognition()V
    .locals 1

    .prologue
    .line 215
    sget-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/inrix/sdk/Inrix;->activityRecognition:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;

    invoke-virtual {v0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->stopActivityRecognition()V

    .line 218
    :cond_0
    return-void
.end method

.method private static validate()V
    .locals 2

    .prologue
    .line 233
    sget-object v0, Lcom/inrix/sdk/Inrix;->config:Lcom/inrix/sdk/InrixConfig;

    if-nez v0, :cond_0

    .line 234
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x415

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 237
    :cond_0
    sget-object v0, Lcom/inrix/sdk/Inrix;->config:Lcom/inrix/sdk/InrixConfig;

    invoke-virtual {v0}, Lcom/inrix/sdk/InrixConfig;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x416

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 241
    :cond_1
    return-void
.end method


# virtual methods
.method public getConfiguration()Lcom/inrix/sdk/InrixConfig;
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lcom/inrix/sdk/Inrix;->config:Lcom/inrix/sdk/InrixConfig;

    return-object v0
.end method

.method setLocationSource(Lcom/inrix/sdk/geolocation/IGeolocationSource;)V
    .locals 1
    .param p1, "locationSource"    # Lcom/inrix/sdk/geolocation/IGeolocationSource;

    .prologue
    .line 184
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/inrix/sdk/geolocation/GeolocationController;->setLocationSource(Lcom/inrix/sdk/geolocation/IGeolocationSource;)V

    .line 185
    return-void
.end method
