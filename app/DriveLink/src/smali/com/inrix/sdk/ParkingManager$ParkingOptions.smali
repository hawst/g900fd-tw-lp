.class public abstract Lcom/inrix/sdk/ParkingManager$ParkingOptions;
.super Ljava/lang/Object;
.source "ParkingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/ParkingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ParkingOptions"
.end annotation


# instance fields
.field private arrivalDate:Ljava/util/Date;

.field private outputFields:I

.field private sortBy:Lcom/inrix/sdk/ParkingManager$SORT_BY;

.field private units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->arrivalDate:Ljava/util/Date;

    .line 91
    const/4 v0, 0x1

    iput v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->outputFields:I

    .line 92
    sget-object v0, Lcom/inrix/sdk/ParkingManager$SORT_BY;->NONE:Lcom/inrix/sdk/ParkingManager$SORT_BY;

    iput-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->sortBy:Lcom/inrix/sdk/ParkingManager$SORT_BY;

    .line 93
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getSettingUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 94
    return-void
.end method


# virtual methods
.method final getArrivalDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->arrivalDate:Ljava/util/Date;

    return-object v0
.end method

.method final getOutputFields()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->outputFields:I

    return v0
.end method

.method final getSortBy()Lcom/inrix/sdk/ParkingManager$SORT_BY;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->sortBy:Lcom/inrix/sdk/ParkingManager$SORT_BY;

    return-object v0
.end method

.method final getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    return-object v0
.end method

.method public final setArrivalDate(Ljava/util/Date;)Lcom/inrix/sdk/ParkingManager$ParkingOptions;
    .locals 0
    .param p1, "arrivalDate"    # Ljava/util/Date;

    .prologue
    .line 106
    # invokes: Lcom/inrix/sdk/ParkingManager;->validateArrivalDate(Ljava/util/Date;)V
    invoke-static {p1}, Lcom/inrix/sdk/ParkingManager;->access$000(Ljava/util/Date;)V

    .line 107
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->arrivalDate:Ljava/util/Date;

    .line 108
    return-object p0
.end method

.method public final setOutputFields(I)Lcom/inrix/sdk/ParkingManager$ParkingOptions;
    .locals 0
    .param p1, "fields"    # I

    .prologue
    .line 147
    # invokes: Lcom/inrix/sdk/ParkingManager;->validateOutputFields(I)V
    invoke-static {p1}, Lcom/inrix/sdk/ParkingManager;->access$100(I)V

    .line 148
    iput p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->outputFields:I

    .line 149
    return-object p0
.end method

.method public final setSortBy(Lcom/inrix/sdk/ParkingManager$SORT_BY;)Lcom/inrix/sdk/ParkingManager$ParkingOptions;
    .locals 0
    .param p1, "sortBy"    # Lcom/inrix/sdk/ParkingManager$SORT_BY;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->sortBy:Lcom/inrix/sdk/ParkingManager$SORT_BY;

    .line 173
    return-object p0
.end method

.method public final setUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)Lcom/inrix/sdk/ParkingManager$ParkingOptions;
    .locals 2
    .param p1, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .prologue
    .line 197
    if-nez p1, :cond_0

    .line 198
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 200
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 201
    return-object p0
.end method
