.class Lcom/inrix/sdk/RouteManager$1;
.super Ljava/lang/Object;
.source "RouteManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/RouteManager;->requestTravelTimes(Lcom/inrix/sdk/RouteManager$TravelTimeOptions;Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/TravelTimeResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/RouteManager;

.field final synthetic val$listener:Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/RouteManager;Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$1;->this$0:Lcom/inrix/sdk/RouteManager;

    iput-object p2, p0, Lcom/inrix/sdk/RouteManager$1;->val$listener:Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/TravelTimeResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/inrix/sdk/model/TravelTimeResponse;

    .prologue
    .line 415
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/inrix/sdk/model/TravelTimeResponse;->getTripInformation()Lcom/inrix/sdk/model/TripInformation;

    move-result-object v0

    if-nez v0, :cond_1

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$1;->val$listener:Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    new-instance v2, Lcom/android/volley/ServerError;

    invoke-direct {v2}, Lcom/android/volley/ServerError;-><init>()V

    invoke-direct {v1, v2}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 425
    :goto_0
    return-void

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$1;->val$listener:Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;->onResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 412
    check-cast p1, Lcom/inrix/sdk/model/TravelTimeResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/RouteManager$1;->onResponse(Lcom/inrix/sdk/model/TravelTimeResponse;)V

    return-void
.end method
