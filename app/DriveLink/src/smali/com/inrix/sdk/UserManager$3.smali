.class Lcom/inrix/sdk/UserManager$3;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/UserManager;->resetPassword(Lcom/inrix/sdk/UserManager$ResetPasswordOptions;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/UserManager;

.field final synthetic val$listener:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$3;->this$0:Lcom/inrix/sdk/UserManager;

    iput-object p2, p0, Lcom/inrix/sdk/UserManager$3;->val$listener:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/parser/xml/XMLEntityBase;)V
    .locals 2
    .param p1, "data"    # Lcom/inrix/sdk/parser/xml/XMLEntityBase;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$3;->val$listener:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;->onResult(Ljava/lang/Object;)V

    .line 282
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 277
    check-cast p1, Lcom/inrix/sdk/parser/xml/XMLEntityBase;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/UserManager$3;->onResponse(Lcom/inrix/sdk/parser/xml/XMLEntityBase;)V

    return-void
.end method
