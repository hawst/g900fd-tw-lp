.class Lcom/inrix/sdk/TrafficManager$2;
.super Ljava/lang/Object;
.source "TrafficManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/TrafficManager;->getTrafficQuality(Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/TrafficManager;

.field final synthetic val$listener:Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/TrafficManager;Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/inrix/sdk/TrafficManager$2;->this$0:Lcom/inrix/sdk/TrafficManager;

    iput-object p2, p0, Lcom/inrix/sdk/TrafficManager$2;->val$listener:Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/inrix/sdk/TrafficManager$2;->val$listener:Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 83
    return-void
.end method
