.class public final enum Lcom/inrix/sdk/utils/UserPreferences$UNIT;
.super Ljava/lang/Enum;
.source "UserPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/utils/UserPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UNIT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/utils/UserPreferences$UNIT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/utils/UserPreferences$UNIT;

.field public static final enum METERS:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

.field public static final enum MILES:Lcom/inrix/sdk/utils/UserPreferences$UNIT;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    const-string/jumbo v1, "MILES"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->MILES:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    new-instance v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    const-string/jumbo v1, "METERS"

    invoke-direct {v0, v1, v3}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->METERS:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    sget-object v1, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->MILES:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->METERS:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    aput-object v1, v0, v3

    sput-object v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->$VALUES:[Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->$VALUES:[Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    invoke-virtual {v0}, [Lcom/inrix/sdk/utils/UserPreferences$UNIT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    return-object v0
.end method
