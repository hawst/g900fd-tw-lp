.class public Lcom/inrix/sdk/utils/AddressLocator;
.super Ljava/lang/Object;
.source "AddressLocator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/utils/AddressLocator$1;,
        Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;,
        Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;,
        Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;,
        Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;,
        Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;
    }
.end annotation


# static fields
.field private static final SERVICE_NOT_AVAILABLE_EXCEPTION_MSG:Ljava/lang/String; = "Service not Available"


# instance fields
.field protected addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

.field private geocoder:Landroid/location/Geocoder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/inrix/sdk/utils/AddressLocator;->geocoder:Landroid/location/Geocoder;

    .line 41
    iput-object p2, p0, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    .line 42
    return-void
.end method

.method static synthetic access$100(Lcom/inrix/sdk/utils/AddressLocator;Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/utils/AddressLocator;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/utils/AddressLocator;->getLocationAddressFromString(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/inrix/sdk/utils/AddressLocator;Landroid/location/Location;)Landroid/location/Address;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/utils/AddressLocator;
    .param p1, "x1"    # Landroid/location/Location;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/inrix/sdk/utils/AddressLocator;->getAddressFromLocation(Landroid/location/Location;)Landroid/location/Address;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/inrix/sdk/utils/AddressLocator;DD)Landroid/location/Address;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/utils/AddressLocator;
    .param p1, "x1"    # D
    .param p3, "x2"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/inrix/sdk/utils/AddressLocator;->getAddressFromLocation(DD)Landroid/location/Address;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/inrix/sdk/utils/AddressLocator;Landroid/location/Address;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/utils/AddressLocator;
    .param p1, "x1"    # Landroid/location/Address;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/inrix/sdk/utils/AddressLocator;->fixAddressThoroughfare(Landroid/location/Address;)V

    return-void
.end method

.method private fixAddressThoroughfare(Landroid/location/Address;)V
    .locals 4
    .param p1, "address"    # Landroid/location/Address;

    .prologue
    const/4 v3, 0x0

    .line 196
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 197
    invoke-virtual {p1, v3}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "line0":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 203
    const-string/jumbo v1, ""

    invoke-virtual {p1, v3, v1}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 205
    .end local v0    # "line0":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private getAddressFromLocation(DD)Landroid/location/Address;
    .locals 10
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 282
    const/4 v7, 0x0

    .line 284
    .local v7, "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :try_start_0
    iget-object v0, p0, Lcom/inrix/sdk/utils/AddressLocator;->geocoder:Landroid/location/Geocoder;

    const/4 v5, 0x1

    move-wide v1, p1

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 285
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 286
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 287
    .local v6, "address":Landroid/location/Address;
    if-eqz v6, :cond_0

    .line 291
    invoke-virtual {v6, p1, p2}, Landroid/location/Address;->setLatitude(D)V

    .line 292
    invoke-virtual {v6, p3, p4}, Landroid/location/Address;->setLongitude(D)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 311
    .end local v6    # "address":Landroid/location/Address;
    :cond_0
    :goto_0
    return-object v6

    .line 296
    :catch_0
    move-exception v8

    .line 297
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Service not Available"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    const-string/jumbo v0, "Something wrong with geo locator service!"

    invoke-static {v0, v8}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 301
    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;

    invoke-direct {v0, p0, v9}, Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;-><init>(Lcom/inrix/sdk/utils/AddressLocator;Lcom/inrix/sdk/utils/AddressLocator$1;)V

    throw v0

    .line 303
    :cond_1
    const-string/jumbo v0, "Cannot resolve address. GPS or network is down"

    invoke-static {v0, v8}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 306
    throw v8

    .line 308
    .end local v8    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v8

    .line 309
    .local v8, "e":Ljava/lang/Exception;
    invoke-static {v8}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/Throwable;)V

    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2
    move-object v6, v9

    .line 311
    goto :goto_0
.end method

.method private getAddressFromLocation(Landroid/location/Location;)Landroid/location/Address;
    .locals 5
    .param p1, "location"    # Landroid/location/Location;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    .line 263
    .local v0, "latitude":D
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    .line 264
    .local v2, "longitude":D
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/inrix/sdk/utils/AddressLocator;->getAddressFromLocation(DD)Landroid/location/Address;

    move-result-object v4

    return-object v4
.end method

.method private getLocationAddressFromString(Ljava/lang/String;I)Ljava/util/List;
    .locals 5
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "maxResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v0, "addressList":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :try_start_0
    iget-object v3, p0, Lcom/inrix/sdk/utils/AddressLocator;->geocoder:Landroid/location/Geocoder;

    invoke-virtual {v3, p1, p2}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 246
    :goto_0
    return-object v0

    .line 226
    :catch_0
    move-exception v2

    .line 227
    .local v2, "ioex":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Service not Available"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 230
    const-string/jumbo v3, "Something wrong with geo locator service!"

    invoke-static {v3, v2}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 232
    new-instance v3, Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;-><init>(Lcom/inrix/sdk/utils/AddressLocator;Lcom/inrix/sdk/utils/AddressLocator$1;)V

    throw v3

    .line 234
    :cond_0
    const-string/jumbo v3, "Reverse-Geocoding IO Failed"

    invoke-static {v3, v2}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 240
    throw v2

    .line 242
    .end local v2    # "ioex":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 243
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "Reverse-Geocoding Failed"

    invoke-static {v3, v1}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public getAddress(FF)V
    .locals 4
    .param p1, "latitude"    # F
    .param p2, "longitude"    # F

    .prologue
    .line 102
    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;-><init>(Lcom/inrix/sdk/utils/AddressLocator;Lcom/inrix/sdk/utils/AddressLocator$1;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 104
    return-void
.end method

.method public getAddress(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 83
    if-nez p1, :cond_1

    .line 84
    iget-object v0, p0, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    invoke-interface {v0}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onNoAddressFound()V

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;-><init>(Lcom/inrix/sdk/utils/AddressLocator;Lcom/inrix/sdk/utils/AddressLocator$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public getLocation(Ljava/lang/String;)V
    .locals 1
    .param p1, "addressStr"    # Ljava/lang/String;

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/inrix/sdk/utils/AddressLocator;->getLocations(Ljava/lang/String;I)V

    .line 53
    return-void
.end method

.method public getLocations(Ljava/lang/String;I)V
    .locals 4
    .param p1, "addressStr"    # Ljava/lang/String;
    .param p2, "maxResults"    # I

    .prologue
    .line 65
    if-nez p1, :cond_1

    .line 66
    iget-object v0, p0, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    invoke-interface {v0}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onNoAddressFound()V

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;-><init>(Lcom/inrix/sdk/utils/AddressLocator;Lcom/inrix/sdk/utils/AddressLocator$1;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
