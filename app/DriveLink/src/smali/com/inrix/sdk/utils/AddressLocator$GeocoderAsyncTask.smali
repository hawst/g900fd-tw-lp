.class Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;
.super Landroid/os/AsyncTask;
.source "AddressLocator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/utils/AddressLocator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GeocoderAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/utils/AddressLocator;


# direct methods
.method private constructor <init>(Lcom/inrix/sdk/utils/AddressLocator;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/inrix/sdk/utils/AddressLocator;Lcom/inrix/sdk/utils/AddressLocator$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/inrix/sdk/utils/AddressLocator;
    .param p2, "x1"    # Lcom/inrix/sdk/utils/AddressLocator$1;

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;-><init>(Lcom/inrix/sdk/utils/AddressLocator;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;
    .locals 11
    .param p1, "objects"    # [Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 110
    if-eqz p1, :cond_1

    aget-object v2, p1, v5

    .line 111
    .local v2, "obj":Ljava/lang/Object;
    :goto_0
    const/4 v3, 0x0

    .line 112
    .local v3, "result":Landroid/location/Address;
    new-instance v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;

    invoke-direct {v4}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;-><init>()V

    .line 116
    .local v4, "results":Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;
    :try_start_0
    instance-of v5, v2, Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 117
    iget-object v6, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    check-cast v2, Ljava/lang/String;

    .end local v2    # "obj":Ljava/lang/Object;
    const/4 v5, 0x1

    aget-object v5, p1, v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    # invokes: Lcom/inrix/sdk/utils/AddressLocator;->getLocationAddressFromString(Ljava/lang/String;I)Ljava/util/List;
    invoke-static {v6, v2, v5}, Lcom/inrix/sdk/utils/AddressLocator;->access$100(Lcom/inrix/sdk/utils/AddressLocator;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v5

    iput-object v5, v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->addressList:Ljava/util/List;

    .line 119
    sget-object v5, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->OK:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    iput-object v5, v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->status:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    .line 147
    :cond_0
    :goto_1
    return-object v4

    .line 110
    .end local v3    # "result":Landroid/location/Address;
    .end local v4    # "results":Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 123
    .restart local v2    # "obj":Ljava/lang/Object;
    .restart local v3    # "result":Landroid/location/Address;
    .restart local v4    # "results":Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;
    :cond_2
    instance-of v5, v2, Landroid/location/Location;

    if-eqz v5, :cond_4

    .line 124
    iget-object v5, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    check-cast v2, Landroid/location/Location;

    .end local v2    # "obj":Ljava/lang/Object;
    # invokes: Lcom/inrix/sdk/utils/AddressLocator;->getAddressFromLocation(Landroid/location/Location;)Landroid/location/Address;
    invoke-static {v5, v2}, Lcom/inrix/sdk/utils/AddressLocator;->access$200(Lcom/inrix/sdk/utils/AddressLocator;Landroid/location/Location;)Landroid/location/Address;

    move-result-object v3

    .line 132
    :cond_3
    :goto_2
    sget-object v5, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->OK:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    iput-object v5, v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->status:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    .line 133
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->addressList:Ljava/util/List;

    .line 137
    if-eqz v3, :cond_0

    .line 138
    iget-object v5, v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->addressList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;
    sget-object v5, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->GeocoderError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    iput-object v5, v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->status:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    goto :goto_1

    .line 125
    .end local v0    # "e":Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException;
    .restart local v2    # "obj":Ljava/lang/Object;
    :cond_4
    :try_start_1
    instance-of v5, v2, Ljava/lang/Float;

    if-eqz v5, :cond_3

    .line 126
    iget-object v6, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    const/4 v5, 0x0

    aget-object v5, p1, v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v7, v5

    const/4 v5, 0x1

    aget-object v5, p1, v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    float-to-double v9, v5

    # invokes: Lcom/inrix/sdk/utils/AddressLocator;->getAddressFromLocation(DD)Landroid/location/Address;
    invoke-static {v6, v7, v8, v9, v10}, Lcom/inrix/sdk/utils/AddressLocator;->access$300(Lcom/inrix/sdk/utils/AddressLocator;DD)Landroid/location/Address;
    :try_end_1
    .catch Lcom/inrix/sdk/utils/AddressLocator$ServiceUnavailableException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_2

    .line 143
    .end local v2    # "obj":Ljava/lang/Object;
    :catch_1
    move-exception v1

    .line 144
    .local v1, "ioex":Ljava/io/IOException;
    sget-object v5, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->NetworkError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    iput-object v5, v4, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->status:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->doInBackground([Ljava/lang/Object;)Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;)V
    .locals 4
    .param p1, "results"    # Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;

    .prologue
    .line 152
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    iget-object v2, v2, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    if-nez v2, :cond_0

    .line 186
    :goto_0
    return-void

    .line 156
    :cond_0
    if-nez p1, :cond_1

    .line 158
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    iget-object v2, v2, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    invoke-interface {v2}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onNetworkError()V

    goto :goto_0

    .line 162
    :cond_1
    iget-object v2, p1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->status:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    sget-object v3, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->OK:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    if-ne v2, v3, :cond_4

    .line 163
    iget-object v2, p1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->addressList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 164
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    iget-object v2, v2, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    invoke-interface {v2}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onNoAddressFound()V

    goto :goto_0

    .line 166
    :cond_2
    iget-object v2, p1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->addressList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 167
    .local v0, "currentAddress":Landroid/location/Address;
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    # invokes: Lcom/inrix/sdk/utils/AddressLocator;->fixAddressThoroughfare(Landroid/location/Address;)V
    invoke-static {v2, v0}, Lcom/inrix/sdk/utils/AddressLocator;->access$400(Lcom/inrix/sdk/utils/AddressLocator;Landroid/location/Address;)V

    goto :goto_1

    .line 170
    .end local v0    # "currentAddress":Landroid/location/Address;
    :cond_3
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    iget-object v2, v2, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    iget-object v3, p1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->addressList:Ljava/util/List;

    invoke-interface {v2, v3}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onAddressListFound(Ljava/util/List;)V

    goto :goto_0

    .line 174
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    sget-object v2, Lcom/inrix/sdk/utils/AddressLocator$1;->$SwitchMap$com$inrix$sdk$utils$AddressLocator$AddressLocatorStatusCode:[I

    iget-object v3, p1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;->status:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    invoke-virtual {v3}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 183
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    iget-object v2, v2, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    invoke-interface {v2}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onNetworkError()V

    goto :goto_0

    .line 176
    :pswitch_0
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    iget-object v2, v2, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    invoke-interface {v2}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onGeocoderError()V

    goto :goto_0

    .line 179
    :pswitch_1
    iget-object v2, p0, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->this$0:Lcom/inrix/sdk/utils/AddressLocator;

    iget-object v2, v2, Lcom/inrix/sdk/utils/AddressLocator;->addressLocatorListCallback:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;

    invoke-interface {v2}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;->onNetworkError()V

    goto :goto_0

    .line 174
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 107
    check-cast p1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/utils/AddressLocator$GeocoderAsyncTask;->onPostExecute(Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorResults;)V

    return-void
.end method
