.class public Lcom/inrix/sdk/utils/BasePreferences;
.super Ljava/lang/Object;
.source "BasePreferences.java"


# static fields
.field protected static editor:Landroid/content/SharedPreferences$Editor;

.field protected static settings:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static load(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    sget-object v0, Lcom/inrix/sdk/utils/BasePreferences;->settings:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 15
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/inrix/sdk/utils/BasePreferences;->settings:Landroid/content/SharedPreferences;

    .line 16
    sget-object v0, Lcom/inrix/sdk/utils/BasePreferences;->settings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sput-object v0, Lcom/inrix/sdk/utils/BasePreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 18
    :cond_0
    return-void
.end method

.method public static loaded()Z
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/inrix/sdk/utils/BasePreferences;->settings:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static save()Z
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/inrix/sdk/utils/BasePreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method
