.class public final enum Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;
.super Ljava/lang/Enum;
.source "AddressLocator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/utils/AddressLocator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AddressLocatorStatusCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

.field public static final enum GeocoderError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

.field public static final enum NetworkError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

.field public static final enum OK:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

.field public static final enum UnknownError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 333
    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    const-string/jumbo v1, "OK"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->OK:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    const-string/jumbo v1, "GeocoderError"

    invoke-direct {v0, v1, v3}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->GeocoderError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    const-string/jumbo v1, "NetworkError"

    invoke-direct {v0, v1, v4}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->NetworkError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    new-instance v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    const-string/jumbo v1, "UnknownError"

    invoke-direct {v0, v1, v5}, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->UnknownError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    .line 332
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    sget-object v1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->OK:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->GeocoderError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->NetworkError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->UnknownError:Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->$VALUES:[Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 332
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 332
    const-class v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;
    .locals 1

    .prologue
    .line 332
    sget-object v0, Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->$VALUES:[Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    invoke-virtual {v0}, [Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorStatusCode;

    return-object v0
.end method
