.class public Lcom/inrix/sdk/utils/GeoUtils;
.super Ljava/lang/Object;
.source "GeoUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static distance(DDDD)D
    .locals 18
    .param p0, "lat1"    # D
    .param p2, "lon1"    # D
    .param p4, "lat2"    # D
    .param p6, "lon2"    # D

    .prologue
    .line 38
    const-wide v0, 0x40b8e30000000000L    # 6371.0

    .line 39
    .local v0, "R":D
    sub-double v12, p4, p0

    invoke-static {v12, v13}, Lcom/inrix/sdk/utils/GeoUtils;->toRadians(D)D

    move-result-wide v8

    .line 40
    .local v8, "dLat":D
    sub-double v12, p6, p2

    invoke-static {v12, v13}, Lcom/inrix/sdk/utils/GeoUtils;->toRadians(D)D

    move-result-wide v10

    .line 41
    .local v10, "dLon":D
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v12, v8, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v14, v8, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    invoke-static/range {p0 .. p1}, Lcom/inrix/sdk/utils/GeoUtils;->toRadians(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    invoke-static/range {p4 .. p5}, Lcom/inrix/sdk/utils/GeoUtils;->toRadians(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v16, v10, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v16, v10, v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    add-double v2, v12, v14

    .line 44
    .local v2, "a":D
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    sub-double v16, v16, v2

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v14

    mul-double v4, v12, v14

    .line 45
    .local v4, "c":D
    mul-double v6, v0, v4

    .line 47
    .local v6, "d":D
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getSettingUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v12

    sget-object v13, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->METERS:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    if-ne v12, v13, :cond_0

    .end local v6    # "d":D
    :goto_0
    return-wide v6

    .restart local v6    # "d":D
    :cond_0
    invoke-static {v6, v7}, Lcom/inrix/sdk/utils/GeoUtils;->kmToMi(D)D

    move-result-wide v6

    goto :goto_0
.end method

.method public static distanceKM(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;)D
    .locals 8
    .param p0, "point1"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p1, "point2"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 58
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 59
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/inrix/sdk/utils/GeoUtils;->distance(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static isEurope(Lcom/inrix/sdk/model/GeoPoint;)Z
    .locals 4
    .param p0, "point"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v0

    const-wide/high16 v2, -0x3fc2000000000000L    # -30.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 10
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static kmToMi(D)D
    .locals 2
    .param p0, "km"    # D

    .prologue
    .line 17
    const-wide v0, 0x3ff9bfdf7e8038a0L    # 1.609344

    div-double v0, p0, v0

    return-wide v0
.end method

.method private static toRadians(D)D
    .locals 4
    .param p0, "deg"    # D

    .prologue
    .line 22
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, p0

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    return-wide v0
.end method
