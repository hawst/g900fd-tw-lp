.class public interface abstract Lcom/inrix/sdk/utils/AddressLocator$AddressLocatorListCallback;
.super Ljava/lang/Object;
.source "AddressLocator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/utils/AddressLocator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AddressLocatorListCallback"
.end annotation


# virtual methods
.method public abstract onAddressListFound(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onGeocoderError()V
.end method

.method public abstract onNetworkError()V
.end method

.method public abstract onNoAddressFound()V
.end method
