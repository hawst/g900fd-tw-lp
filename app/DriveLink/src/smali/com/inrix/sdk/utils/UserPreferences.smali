.class public Lcom/inrix/sdk/utils/UserPreferences;
.super Lcom/inrix/sdk/utils/BasePreferences;
.source "UserPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    }
.end annotation


# static fields
.field private static final ANALYTICS_SERVER:Ljava/lang/String; = "analytics_server"

.field private static final APP_VERSION:Ljava/lang/String; = "app_version"

.field private static final BASE_AUTHENTICATION_SERVER:Ljava/lang/String; = "base_auth_server"

.field private static final BASE_REGISTRATION_SERVER:Ljava/lang/String; = "base_reg_server"

.field private static final CAMERAS_SERVER:Ljava/lang/String; = "cameras_server"

.field private static final COMPOSITE_TILES_SERVER:Ljava/lang/String; = "comp_tiles_server"

.field private static final CS_AUTH_TOKEN:Ljava/lang/String; = "inrix_auth_token"

.field private static final CS_AUTH_TOKEN_EXPIRE_TIME:Ljava/lang/String; = "auth_token_expire_time"

.field private static final CS_DEVICE_ID:Ljava/lang/String; = "inrix_device_id"

.field private static final DUST_SERVER:Ljava/lang/String; = "dust_server"

.field private static final FUEL_SERVER:Ljava/lang/String; = "fuel_server"

.field private static final INCIDENTS_SERVER:Ljava/lang/String; = "incidents_server"

.field private static final INSTALLATION_ID_HASH:Ljava/lang/String; = "installation_id_hash"

.field private static final IS_REGISTERED:Ljava/lang/String; = "is_user_registered"

.field private static final MOBILE_SERVER:Ljava/lang/String; = "mobile_server"

.field private static final PARKING_SERVER:Ljava/lang/String; = "parking_server"

.field private static final REGISTRATION_REGION:Ljava/lang/String; = "registration_region"

.field private static final ROUTING_SERVER:Ljava/lang/String; = "routing_server"

.field private static final SETTINGS_SERVER:Ljava/lang/String; = "settings_server"

.field private static final SETTINGS_UNITS:Ljava/lang/String; = "unit"

.field private static final TILE_SERVER:Ljava/lang/String; = "tile_server"

.field private static final TRAFFIC_STATE_SERVER:Ljava/lang/String; = "traffic_state_server"

.field private static final TRAVEL_DATA_SERVER:Ljava/lang/String; = "travel_data_server"

.field private static final VEHICLE_ID:Ljava/lang/String; = "vehicleID"

.field public static final VEHICLE_ID_EXPIRATION:I = 0x278d00

.field private static final VEHICLE_ID_EXPIRES:Ljava/lang/String; = "vehicleIDExpires"

.field private static httpStack:Lcom/android/volley/toolbox/HttpStack;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/inrix/sdk/utils/UserPreferences;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/inrix/sdk/utils/BasePreferences;-><init>()V

    .line 13
    return-void
.end method

.method public static clear()V
    .locals 1

    .prologue
    .line 274
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 275
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 276
    const/4 v0, 0x0

    sput-object v0, Lcom/inrix/sdk/utils/UserPreferences;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    .line 277
    return-void
.end method

.method public static getAnalyticsServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 113
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "analytics_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAppVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "app_version"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAuthToken()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "inrix_auth_token"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAuthTokenExpireTime()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "auth_token_expire_time"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAuthenticationServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 216
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "base_auth_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCamerasServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 121
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "cameras_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCompositeTilesServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 129
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "comp_tiles_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDeviceId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 59
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "inrix_device_id"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDustServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 137
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "dust_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFuelServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 145
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "fuel_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHttpStack()Lcom/android/volley/toolbox/HttpStack;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    return-object v0
.end method

.method public static getIncidentsServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 153
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "incidents_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstallationIdHash()Ljava/lang/String;
    .locals 3

    .prologue
    .line 229
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "installation_id_hash"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMobileServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "mobile_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getParkingServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 161
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "parking_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRegion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 220
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "registration_region"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRegistrationServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 212
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "base_reg_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRoutingServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "routing_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSettingUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .locals 6

    .prologue
    .line 238
    sget-object v2, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "unit"

    sget-object v4, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->MILES:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    invoke-virtual {v4}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->ordinal()I

    move-result v4

    int-to-long v4, v4

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 239
    .local v0, "ord":J
    sget-object v2, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->MILES:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    invoke-virtual {v2}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->ordinal()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->MILES:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->METERS:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    goto :goto_0
.end method

.method public static getSettingsServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 177
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "settings_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTTSServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "tile_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTrafficStateServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 186
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "traffic_state_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTravelDataServer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 194
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "travel_data_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVehicleId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 285
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->renewVehicleIdIfExpired()V

    .line 286
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "vehicleID"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVehicleIdExpirationDate()J
    .locals 4

    .prologue
    .line 295
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "vehicleIDExpires"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static isLoggedIn()Z
    .locals 3

    .prologue
    .line 256
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "is_user_registered"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static final renewVehicleIdIfExpired()V
    .locals 8

    .prologue
    .line 302
    sget-object v4, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "vehicleID"

    const-string/jumbo v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, "id":Ljava/lang/String;
    sget-object v4, Lcom/inrix/sdk/utils/UserPreferences;->settings:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "vehicleIDExpires"

    const-wide/16 v6, -0x1

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 305
    .local v2, "idExpirationDate":J
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    .line 309
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 310
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v4, 0xd

    const v5, 0x278d00

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 312
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 313
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 315
    invoke-static {v1}, Lcom/inrix/sdk/utils/UserPreferences;->setVehicleId(Ljava/lang/String;)V

    .line 316
    invoke-static {v2, v3}, Lcom/inrix/sdk/utils/UserPreferences;->setVehicleIdExpirationDate(J)V

    .line 318
    .end local v0    # "calendar":Ljava/util/Calendar;
    :cond_1
    return-void
.end method

.method public static setAnalyticsServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 116
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "analytics_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 117
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 118
    return-void
.end method

.method public static setAppVersion(Ljava/lang/String;)V
    .locals 2
    .param p0, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 72
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "app_version"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 73
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 74
    return-void
.end method

.method public static setAuthToken(Ljava/lang/String;)V
    .locals 2
    .param p0, "authToken"    # Ljava/lang/String;

    .prologue
    .line 85
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "inrix_auth_token"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 86
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 87
    return-void
.end method

.method public static setAuthTokenExpireTime(Ljava/lang/String;)V
    .locals 2
    .param p0, "expireTime"    # Ljava/lang/String;

    .prologue
    .line 90
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "auth_token_expire_time"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 91
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 92
    return-void
.end method

.method public static setAuthenticationServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 207
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "base_auth_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 208
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 209
    return-void
.end method

.method public static setCamerasServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 124
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "cameras_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 125
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 126
    return-void
.end method

.method public static setCompositeTilesServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 132
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "comp_tiles_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 133
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 134
    return-void
.end method

.method public static setDeviceId(Ljava/lang/String;)V
    .locals 2
    .param p0, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 63
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "inrix_device_id"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 64
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 65
    return-void
.end method

.method public static setDustServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 140
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "dust_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 141
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 142
    return-void
.end method

.method public static setFuelServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 148
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "fuel_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 149
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 150
    return-void
.end method

.method public static setHttpStack(Lcom/android/volley/toolbox/HttpStack;)V
    .locals 0
    .param p0, "httpClientStack"    # Lcom/android/volley/toolbox/HttpStack;

    .prologue
    .line 55
    sput-object p0, Lcom/inrix/sdk/utils/UserPreferences;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    .line 56
    return-void
.end method

.method public static setIncidentsServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 156
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "incidents_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 157
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 158
    return-void
.end method

.method public static setInstallationIdHash(Ljava/lang/String;)V
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 233
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "installation_id_hash"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 234
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 235
    return-void
.end method

.method public static setLoggedIn(Z)V
    .locals 2
    .param p0, "value"    # Z

    .prologue
    .line 266
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "is_user_registered"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 267
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 268
    return-void
.end method

.method public static setMobileServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 100
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "mobile_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 101
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 102
    return-void
.end method

.method public static setParkingServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 164
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "parking_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 165
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 166
    return-void
.end method

.method public static setRegion(Ljava/lang/String;)V
    .locals 2
    .param p0, "region"    # Ljava/lang/String;

    .prologue
    .line 224
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "registration_region"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 225
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 226
    return-void
.end method

.method public static setRegistrationServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 202
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "base_reg_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 203
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 204
    return-void
.end method

.method public static setRoutingServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 172
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "routing_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 173
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 174
    return-void
.end method

.method public static setSettingUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)V
    .locals 4
    .param p0, "unit"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .prologue
    .line 243
    if-nez p0, :cond_0

    .line 244
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 246
    :cond_0
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "unit"

    invoke-virtual {p0}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->ordinal()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 247
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 248
    return-void
.end method

.method public static setSettingsServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 180
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "settings_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 181
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 182
    return-void
.end method

.method public static setTTSServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 108
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "tile_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 109
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 110
    return-void
.end method

.method public static setTrafficStateServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 189
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "traffic_state_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 190
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 191
    return-void
.end method

.method public static setTravelDataServer(Ljava/lang/String;)V
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 197
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "travel_data_server"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 198
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 199
    return-void
.end method

.method public static setVehicleId(Ljava/lang/String;)V
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 327
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "vehicleID"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 328
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 329
    return-void
.end method

.method public static setVehicleIdExpirationDate(J)V
    .locals 2
    .param p0, "dateMilli"    # J

    .prologue
    .line 338
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "vehicleIDExpires"

    invoke-interface {v0, v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 339
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 340
    return-void
.end method
