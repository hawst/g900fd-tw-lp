.class public Lcom/inrix/sdk/IncidentAlert;
.super Ljava/lang/Object;
.source "IncidentAlert.java"

# interfaces
.implements Lcom/inrix/sdk/ICancellable;
.implements Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;


# instance fields
.field private final MIN_SPEED_ALLOWED_MPH:F

.field private alertInterval:I

.field private currentRequest:Lcom/inrix/sdk/ICancellable;

.field private filter:Lcom/inrix/sdk/AlertsManager$IFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/inrix/sdk/AlertsManager$IFilter",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation
.end field

.field private forwardConeAngle:F

.field private incidentManager:Lcom/inrix/sdk/IncidentsManager;

.field private isInProgress:Z

.field private listener:Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;

.field private requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

.field private speedFactor:F

.field private timer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;IFLcom/inrix/sdk/AlertsManager$IFilter;F)V
    .locals 2
    .param p1, "listener"    # Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;
    .param p2, "alertInterval"    # I
    .param p3, "speedFactor"    # F
    .param p5, "forwardConeAngle"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;",
            "IF",
            "Lcom/inrix/sdk/AlertsManager$IFilter",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .local p4, "filter":Lcom/inrix/sdk/AlertsManager$IFilter;, "Lcom/inrix/sdk/AlertsManager$IFilter<Lcom/inrix/sdk/model/Incident;>;"
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/IncidentAlert;->isInProgress:Z

    .line 25
    iput-object v1, p0, Lcom/inrix/sdk/IncidentAlert;->currentRequest:Lcom/inrix/sdk/ICancellable;

    .line 26
    iput-object v1, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    .line 30
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/inrix/sdk/IncidentAlert;->MIN_SPEED_ALLOWED_MPH:F

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/inrix/sdk/IncidentAlert;->forwardConeAngle:F

    .line 39
    iput-object p1, p0, Lcom/inrix/sdk/IncidentAlert;->listener:Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;

    .line 40
    iput p2, p0, Lcom/inrix/sdk/IncidentAlert;->alertInterval:I

    .line 41
    iput p3, p0, Lcom/inrix/sdk/IncidentAlert;->speedFactor:F

    .line 42
    iput-object p4, p0, Lcom/inrix/sdk/IncidentAlert;->filter:Lcom/inrix/sdk/AlertsManager$IFilter;

    .line 43
    new-instance v0, Lcom/inrix/sdk/IncidentsManager;

    invoke-direct {v0}, Lcom/inrix/sdk/IncidentsManager;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->incidentManager:Lcom/inrix/sdk/IncidentsManager;

    .line 44
    iput p5, p0, Lcom/inrix/sdk/IncidentAlert;->forwardConeAngle:F

    .line 45
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentAlert;->requestCongestions()V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/inrix/sdk/IncidentAlert;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/IncidentAlert;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentAlert;->requestCongestions()V

    return-void
.end method

.method static synthetic access$100(Lcom/inrix/sdk/IncidentAlert;Ljava/util/List;)Z
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/IncidentAlert;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/inrix/sdk/IncidentAlert;->isFlowIncidentsPresent(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/inrix/sdk/IncidentAlert;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/IncidentAlert;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentAlert;->requestAllIncidents()V

    return-void
.end method

.method private filterData(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v7

    invoke-virtual {v7}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v3

    .line 203
    .local v3, "currentLocation":Landroid/location/Location;
    if-eqz v3, :cond_0

    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v7

    invoke-virtual {v7}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownBearing()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move-object v6, p1

    .line 220
    .end local v6    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    :cond_1
    return-object v6

    .line 207
    .restart local v6    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    :cond_2
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v7

    invoke-virtual {v7}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownBearing()F

    move-result v2

    .line 208
    .local v2, "currentHeading":F
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/inrix/sdk/model/Incident;

    .line 209
    .local v5, "incident":Lcom/inrix/sdk/model/Incident;
    float-to-double v7, v2

    invoke-virtual {v5, v3, v7, v8}, Lcom/inrix/sdk/model/Incident;->getBearingAngle(Landroid/location/Location;D)D

    move-result-wide v0

    .line 212
    .local v0, "angle":D
    iget-object v7, p0, Lcom/inrix/sdk/IncidentAlert;->filter:Lcom/inrix/sdk/AlertsManager$IFilter;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/inrix/sdk/IncidentAlert;->filter:Lcom/inrix/sdk/AlertsManager$IFilter;

    invoke-interface {v7, v5}, Lcom/inrix/sdk/AlertsManager$IFilter;->isItemAllowed(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 215
    :cond_4
    iget v7, p0, Lcom/inrix/sdk/IncidentAlert;->forwardConeAngle:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    float-to-double v7, v7

    cmpg-double v7, v0, v7

    if-gez v7, :cond_3

    .line 216
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private generateRadiusOptions()Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;
    .locals 7

    .prologue
    .line 170
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v1

    .line 172
    .local v1, "currentLocation":Landroid/location/Location;
    if-nez v1, :cond_0

    .line 173
    const-string/jumbo v3, "Unable to get current location"

    invoke-static {v3}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 174
    const/4 v3, 0x0

    .line 182
    :goto_0
    return-object v3

    .line 177
    :cond_0
    new-instance v0, Lcom/inrix/sdk/model/GeoPoint;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/inrix/sdk/model/GeoPoint;-><init>(DD)V

    .line 179
    .local v0, "curGeoPoint":Lcom/inrix/sdk/model/GeoPoint;
    invoke-virtual {v1}, Landroid/location/Location;->getSpeed()F

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 182
    .local v2, "speed":F
    new-instance v3, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    iget v4, p0, Lcom/inrix/sdk/IncidentAlert;->speedFactor:F

    div-float v4, v2, v4

    float-to-double v4, v4

    invoke-direct {v3, v0, v4, v5}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;-><init>(Lcom/inrix/sdk/model/GeoPoint;D)V

    goto :goto_0
.end method

.method private isFlowIncidentsPresent(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    const/4 v2, 0x0

    .line 135
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v2

    .line 139
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/inrix/sdk/model/Incident;

    .line 140
    .local v1, "incident":Lcom/inrix/sdk/model/Incident;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/Incident;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 141
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private requestAllIncidents()V
    .locals 2

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentAlert;->generateRadiusOptions()Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    .line 151
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    if-eqz v0, :cond_0

    .line 152
    const-string/jumbo v0, "Request all incidents"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->incidentManager:Lcom/inrix/sdk/IncidentsManager;

    iget-object v1, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    invoke-virtual {v0, p0, v1}, Lcom/inrix/sdk/IncidentsManager;->getIncidentsInRadius(Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;)Lcom/inrix/sdk/ICancellable;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->currentRequest:Lcom/inrix/sdk/ICancellable;

    .line 167
    :goto_0
    return-void

    .line 156
    :cond_0
    const-string/jumbo v0, "Request all incidents: current locaiton is not ready - waiting for current location"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 158
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    new-instance v1, Lcom/inrix/sdk/IncidentAlert$4;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/IncidentAlert$4;-><init>(Lcom/inrix/sdk/IncidentAlert;)V

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->addOnFixListener(Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;)V

    goto :goto_0
.end method

.method private requestCongestions()V
    .locals 3

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/inrix/sdk/IncidentAlert;->isInProgress:Z

    if-eqz v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentAlert;->generateRadiusOptions()Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    .line 80
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    if-eqz v0, :cond_1

    .line 81
    const-string/jumbo v0, "Request congestions"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/inrix/sdk/IncidentAlert;->isInProgress:Z

    .line 83
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->setIncidentType(I)Lcom/inrix/sdk/IncidentsManager$IncidentOptions;

    .line 85
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->incidentManager:Lcom/inrix/sdk/IncidentsManager;

    new-instance v1, Lcom/inrix/sdk/IncidentAlert$2;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/IncidentAlert$2;-><init>(Lcom/inrix/sdk/IncidentAlert;)V

    iget-object v2, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    invoke-virtual {v0, v1, v2}, Lcom/inrix/sdk/IncidentsManager;->getIncidentsInRadius(Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;)Lcom/inrix/sdk/ICancellable;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->currentRequest:Lcom/inrix/sdk/ICancellable;

    goto :goto_0

    .line 114
    :cond_1
    const-string/jumbo v0, "Request congestions: current locaiton is not ready - waiting for current location"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 116
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    new-instance v1, Lcom/inrix/sdk/IncidentAlert$3;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/IncidentAlert$3;-><init>(Lcom/inrix/sdk/IncidentAlert;)V

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->addOnFixListener(Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;)V

    goto :goto_0
.end method

.method private resetUpdateTimer()V
    .locals 4

    .prologue
    .line 60
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    .line 64
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    .line 65
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/inrix/sdk/IncidentAlert$1;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/IncidentAlert$1;-><init>(Lcom/inrix/sdk/IncidentAlert;)V

    iget v2, p0, Lcom/inrix/sdk/IncidentAlert;->alertInterval:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 72
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->currentRequest:Lcom/inrix/sdk/ICancellable;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->currentRequest:Lcom/inrix/sdk/ICancellable;

    invoke-interface {v0}, Lcom/inrix/sdk/ICancellable;->cancel()V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->timer:Ljava/util/Timer;

    .line 57
    :cond_1
    return-void
.end method

.method public getLastRequestedDistance()D
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->requestParams:Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    invoke-virtual {v0}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->getRadius()D

    move-result-wide v0

    .line 234
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public onError(Lcom/inrix/sdk/Error;)V
    .locals 1
    .param p1, "error"    # Lcom/inrix/sdk/Error;

    .prologue
    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/IncidentAlert;->isInProgress:Z

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->currentRequest:Lcom/inrix/sdk/ICancellable;

    .line 242
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentAlert;->resetUpdateTimer()V

    .line 243
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->listener:Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->listener:Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 246
    :cond_0
    return-void
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/IncidentAlert;->onResult(Ljava/util/List;)V

    return-void
.end method

.method public onResult(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/IncidentAlert;->isInProgress:Z

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->currentRequest:Lcom/inrix/sdk/ICancellable;

    .line 189
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentAlert;->resetUpdateTimer()V

    .line 190
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->listener:Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert;->listener:Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;

    invoke-direct {p0, p1}, Lcom/inrix/sdk/IncidentAlert;->filterData(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;->onResult(Ljava/lang/Object;)V

    .line 193
    :cond_0
    return-void
.end method
