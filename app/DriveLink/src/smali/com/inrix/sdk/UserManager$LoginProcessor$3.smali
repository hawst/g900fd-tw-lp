.class Lcom/inrix/sdk/UserManager$LoginProcessor$3;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/UserManager$LoginProcessor;->auth()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/DeviceAuth;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/UserManager$LoginProcessor;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$LoginProcessor$3;->this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/DeviceAuth;)V
    .locals 2
    .param p1, "response"    # Lcom/inrix/sdk/model/DeviceAuth;

    .prologue
    .line 421
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->isExpired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 424
    const-string/jumbo v0, "Server returned already expired token! Something is wrong.."

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->parseEndPointsFromServer()Z

    .line 429
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->storeEndPointsInUserPreferences()V

    .line 431
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v0

    iget-object v0, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->tokenExpireDateUTC:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setAuthTokenExpireTime(Ljava/lang/String;)V

    .line 433
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v0

    iget-object v0, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setAuthToken(Ljava/lang/String;)V

    .line 435
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->getAuthenticator()Lcom/inrix/sdk/auth/InrixAuthenticator;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->setAuthToken(Lcom/inrix/sdk/model/DeviceAuth;)V

    .line 438
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "auth complete: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v1

    iget-object v1, v1, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 441
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor$3;->this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;

    # getter for: Lcom/inrix/sdk/UserManager$LoginProcessor;->callback:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    invoke-static {v0}, Lcom/inrix/sdk/UserManager$LoginProcessor;->access$100(Lcom/inrix/sdk/UserManager$LoginProcessor;)Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor$3;->this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;

    # getter for: Lcom/inrix/sdk/UserManager$LoginProcessor;->callback:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    invoke-static {v0}, Lcom/inrix/sdk/UserManager$LoginProcessor;->access$100(Lcom/inrix/sdk/UserManager$LoginProcessor;)Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;->onResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 417
    check-cast p1, Lcom/inrix/sdk/model/DeviceAuth;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/UserManager$LoginProcessor$3;->onResponse(Lcom/inrix/sdk/model/DeviceAuth;)V

    return-void
.end method
