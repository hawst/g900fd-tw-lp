.class public Lcom/inrix/sdk/RouteManager$RouteOptions;
.super Ljava/lang/Object;
.source "RouteManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/RouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteOptions"
.end annotation


# instance fields
.field private end:Lcom/inrix/sdk/model/GeoPoint;

.field private heading:Ljava/lang/Integer;

.field private numAlternates:I

.field private outputFields:I

.field private start:Lcom/inrix/sdk/model/GeoPoint;

.field private tolerance:I

.field private units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

.field private waypoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GeoPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 1
    .param p1, "start"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "end"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->setStart(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/RouteManager$RouteOptions;

    .line 130
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/RouteManager$RouteOptions;->setEnd(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/RouteManager$RouteOptions;

    .line 131
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getSettingUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 132
    const/16 v0, 0x62

    iput v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    .line 134
    return-void
.end method

.method private areWayPointsValid(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GeoPoint;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "wayPointsList":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/GeoPoint;>;"
    const/4 v1, 0x1

    .line 291
    .local v1, "valid":Z
    if-eqz p1, :cond_1

    .line 292
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/inrix/sdk/model/GeoPoint;

    .line 293
    .local v2, "wayPoint":Lcom/inrix/sdk/model/GeoPoint;
    invoke-static {v2}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 294
    const/4 v1, 0x0

    .line 299
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "wayPoint":Lcom/inrix/sdk/model/GeoPoint;
    :cond_1
    return v1
.end method


# virtual methods
.method getEnd()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->end:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method getHeading()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->heading:Ljava/lang/Integer;

    return-object v0
.end method

.method getNumAlternates()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->numAlternates:I

    return v0
.end method

.method final getOutputFields()I
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    return v0
.end method

.method final getOutputFieldsAsString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 338
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 340
    .local v0, "output":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget v1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 341
    const-string/jumbo v1, "d"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    :cond_0
    iget v1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 345
    const-string/jumbo v1, "s"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_1
    iget v1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 349
    const-string/jumbo v1, "m"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    :cond_2
    iget v1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 353
    const-string/jumbo v1, "w"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    :cond_3
    iget v1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 357
    const-string/jumbo v1, "b"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_4
    iget v1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 361
    const-string/jumbo v1, "p"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_5
    iget v1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    .line 365
    const-string/jumbo v1, "i"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    :cond_6
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method getStart()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->start:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method getTolerance()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->tolerance:I

    return v0
.end method

.method public getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    return-object v0
.end method

.method getWaypoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GeoPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->waypoints:Ljava/util/List;

    return-object v0
.end method

.method public setEnd(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/RouteManager$RouteOptions;
    .locals 0
    .param p1, "point"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->end:Lcom/inrix/sdk/model/GeoPoint;

    .line 163
    return-object p0
.end method

.method public setHeading(I)Lcom/inrix/sdk/RouteManager$RouteOptions;
    .locals 2
    .param p1, "heading"    # I

    .prologue
    .line 223
    if-ltz p1, :cond_0

    const/16 v0, 0x168

    if-le p1, v0, :cond_1

    .line 225
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3fc

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 227
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->heading:Ljava/lang/Integer;

    .line 228
    return-object p0
.end method

.method public setNumAlternates(I)Lcom/inrix/sdk/RouteManager$RouteOptions;
    .locals 0
    .param p1, "numAlternates"    # I

    .prologue
    .line 276
    iput p1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->numAlternates:I

    .line 277
    return-object p0
.end method

.method public final setOutputFields(I)Lcom/inrix/sdk/RouteManager$RouteOptions;
    .locals 2
    .param p1, "outputFields"    # I

    .prologue
    .line 310
    iget v0, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    if-ne v0, p1, :cond_0

    .line 319
    :goto_0
    return-object p0

    .line 314
    :cond_0
    invoke-virtual {p0}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getOutputFieldsAsString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3f1

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 318
    :cond_1
    iput p1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->outputFields:I

    goto :goto_0
.end method

.method public setStart(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/RouteManager$RouteOptions;
    .locals 0
    .param p1, "point"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->start:Lcom/inrix/sdk/model/GeoPoint;

    .line 148
    return-object p0
.end method

.method public setTolerance(I)Lcom/inrix/sdk/RouteManager$RouteOptions;
    .locals 0
    .param p1, "tolerance"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->tolerance:I

    .line 256
    return-object p0
.end method

.method public setUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)V
    .locals 2
    .param p1, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .prologue
    .line 186
    if-nez p1, :cond_0

    .line 187
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 189
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 190
    return-void
.end method

.method public setWaypoints(Ljava/util/List;)Lcom/inrix/sdk/RouteManager$RouteOptions;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GeoPoint;",
            ">;)",
            "Lcom/inrix/sdk/RouteManager$RouteOptions;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/GeoPoint;>;"
    invoke-direct {p0, p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->areWayPointsValid(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3fb

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 210
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/RouteManager$RouteOptions;->waypoints:Ljava/util/List;

    .line 211
    return-object p0
.end method
