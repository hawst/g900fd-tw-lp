.class Lcom/inrix/sdk/UserManager$LoginProcessor$4;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/UserManager$LoginProcessor;->auth()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/UserManager$LoginProcessor;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$LoginProcessor$4;->this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor$4;->this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;

    # getter for: Lcom/inrix/sdk/UserManager$LoginProcessor;->callback:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    invoke-static {v0}, Lcom/inrix/sdk/UserManager$LoginProcessor;->access$100(Lcom/inrix/sdk/UserManager$LoginProcessor;)Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    move-result-object v0

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "register complete: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogV(Ljava/lang/String;)V

    .line 454
    return-void
.end method
