.class Lcom/inrix/sdk/ParkingManager$2;
.super Ljava/lang/Object;
.source "ParkingManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/ParkingManager;->getParkingLotsInRadius(Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/ParkingManager;

.field final synthetic val$listener:Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$2;->this$0:Lcom/inrix/sdk/ParkingManager;

    iput-object p2, p0, Lcom/inrix/sdk/ParkingManager$2;->val$listener:Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 529
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$2;->val$listener:Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 530
    return-void
.end method
