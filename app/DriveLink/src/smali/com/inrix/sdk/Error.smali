.class public Lcom/inrix/sdk/Error;
.super Ljava/lang/Object;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/Error$Type;
    }
.end annotation


# instance fields
.field private causeMessage:Ljava/lang/String;

.field private errorId:I

.field private type:Lcom/inrix/sdk/Error$Type;


# direct methods
.method public constructor <init>(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "cause"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/Error;->causeMessage:Ljava/lang/String;

    .line 27
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/inrix/sdk/Error;->errorId:I

    .line 28
    sget-object v0, Lcom/inrix/sdk/Error$Type;->SDKError:Lcom/inrix/sdk/Error$Type;

    iput-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    .line 38
    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/Error;->causeMessage:Ljava/lang/String;

    .line 39
    instance-of v0, p1, Lcom/inrix/sdk/network/response/InrixError;

    if-eqz v0, :cond_0

    .line 40
    sget-object v0, Lcom/inrix/sdk/Error$Type;->ServerError:Lcom/inrix/sdk/Error$Type;

    iput-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    move-object v0, p1

    .line 41
    check-cast v0, Lcom/inrix/sdk/network/response/InrixError;

    invoke-virtual {v0}, Lcom/inrix/sdk/network/response/InrixError;->getStatusId()I

    move-result v0

    iput v0, p0, Lcom/inrix/sdk/Error;->errorId:I

    .line 42
    check-cast p1, Lcom/inrix/sdk/network/response/InrixError;

    .end local p1    # "cause":Lcom/android/volley/VolleyError;
    invoke-virtual {p1}, Lcom/inrix/sdk/network/response/InrixError;->getStatusText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/Error;->causeMessage:Ljava/lang/String;

    .line 52
    :goto_0
    return-void

    .line 43
    .restart local p1    # "cause":Lcom/android/volley/VolleyError;
    :cond_0
    instance-of v0, p1, Lcom/android/volley/NetworkError;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/android/volley/TimeoutError;

    if-eqz v0, :cond_2

    .line 45
    :cond_1
    sget-object v0, Lcom/inrix/sdk/Error$Type;->NetworkError:Lcom/inrix/sdk/Error$Type;

    iput-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    goto :goto_0

    .line 46
    :cond_2
    instance-of v0, p1, Lcom/android/volley/ServerError;

    if-eqz v0, :cond_3

    .line 47
    sget-object v0, Lcom/inrix/sdk/Error$Type;->ServerError:Lcom/inrix/sdk/Error$Type;

    iput-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    goto :goto_0

    .line 49
    :cond_3
    sget-object v0, Lcom/inrix/sdk/Error$Type;->SDKError:Lcom/inrix/sdk/Error$Type;

    iput-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "cause"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/Error;->causeMessage:Ljava/lang/String;

    .line 27
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/inrix/sdk/Error;->errorId:I

    .line 28
    sget-object v0, Lcom/inrix/sdk/Error$Type;->SDKError:Lcom/inrix/sdk/Error$Type;

    iput-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    .line 61
    iput-object p1, p0, Lcom/inrix/sdk/Error;->causeMessage:Ljava/lang/String;

    .line 62
    sget-object v0, Lcom/inrix/sdk/Error$Type;->SDKError:Lcom/inrix/sdk/Error$Type;

    iput-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    .line 63
    return-void
.end method


# virtual methods
.method public getErrorId()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/inrix/sdk/Error;->errorId:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/inrix/sdk/Error;->causeMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorType()Lcom/inrix/sdk/Error$Type;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/inrix/sdk/Error;->type:Lcom/inrix/sdk/Error$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 101
    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget v1, p0, Lcom/inrix/sdk/Error;->errorId:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 103
    iget v1, p0, Lcom/inrix/sdk/Error;->errorId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 104
    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/inrix/sdk/Error;->causeMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
