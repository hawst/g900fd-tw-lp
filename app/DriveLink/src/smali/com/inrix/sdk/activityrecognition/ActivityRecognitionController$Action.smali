.class final enum Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;
.super Ljava/lang/Enum;
.source "ActivityRecognitionController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

.field public static final enum SIMULATE:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

.field public static final enum START:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

.field public static final enum STOP:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    const-string/jumbo v1, "START"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->START:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    new-instance v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    const-string/jumbo v1, "STOP"

    invoke-direct {v0, v1, v3}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->STOP:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    new-instance v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    const-string/jumbo v1, "SIMULATE"

    invoke-direct {v0, v1, v4}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->SIMULATE:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->START:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->STOP:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->SIMULATE:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    aput-object v1, v0, v4

    sput-object v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->$VALUES:[Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->$VALUES:[Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    invoke-virtual {v0}, [Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    return-object v0
.end method
