.class Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ActivityRecognitionController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ActivityBroadcastReceiver"
.end annotation


# instance fields
.field private currentActivity:Lcom/google/android/gms/location/DetectedActivity;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;->currentActivity:Lcom/google/android/gms/location/DetectedActivity;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 195
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "activity_recognition_broadcast_extra"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/DetectedActivity;

    .line 199
    .local v0, "activity":Lcom/google/android/gms/location/DetectedActivity;
    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;->currentActivity:Lcom/google/android/gms/location/DetectedActivity;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;->currentActivity:Lcom/google/android/gms/location/DetectedActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iput-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;->currentActivity:Lcom/google/android/gms/location/DetectedActivity;

    .line 206
    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v1

    if-nez v1, :cond_2

    .line 207
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->isDriving()Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    const-string/jumbo v1, "We are driving! Enable geolocation tracking"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 210
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/inrix/sdk/geolocation/GeolocationController;->setDriving(Z)V

    goto :goto_0

    .line 213
    :cond_2
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->isDriving()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    const-string/jumbo v1, "We are not driving! Disable geolocation tracking"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 216
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/inrix/sdk/geolocation/GeolocationController;->setDriving(Z)V

    goto :goto_0
.end method
