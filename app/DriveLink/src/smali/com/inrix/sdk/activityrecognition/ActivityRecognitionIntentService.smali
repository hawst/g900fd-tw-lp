.class public Lcom/inrix/sdk/activityrecognition/ActivityRecognitionIntentService;
.super Landroid/app/IntentService;
.source "ActivityRecognitionIntentService.java"


# static fields
.field public static final ACTIVITY_RECOGNITION_ACTION:Ljava/lang/String; = "activity_recognition_broadcast_action"

.field public static final ACTIVITY_RECOGNITION_EXTRA:Ljava/lang/String; = "activity_recognition_broadcast_extra"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    const-string/jumbo v0, "ActivityRecognitionIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->hasResult(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 22
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->extractResult(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v2

    .line 25
    .local v2, "result":Lcom/google/android/gms/location/ActivityRecognitionResult;
    invoke-virtual {v2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->getMostProbableActivity()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    .line 27
    .local v1, "mostProbableActivity":Lcom/google/android/gms/location/DetectedActivity;
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "activity_recognition_broadcast_action"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string/jumbo v3, "activity_recognition_broadcast_extra"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 30
    invoke-static {p0}, Lcom/inrix/sdk/google/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Lcom/inrix/sdk/google/LocalBroadcastManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/inrix/sdk/google/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 33
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "mostProbableActivity":Lcom/google/android/gms/location/DetectedActivity;
    .end local v2    # "result":Lcom/google/android/gms/location/ActivityRecognitionResult;
    :cond_0
    return-void
.end method
