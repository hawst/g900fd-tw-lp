.class public Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;
.super Ljava/lang/Object;
.source "ActivityRecognitionController.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;,
        Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;
    }
.end annotation


# static fields
.field public static final DETECTION_INTERVAL_MILLISECONDS:I = 0x7530

.field public static final DETECTION_INTERVAL_SECONDS:I = 0x1e

.field public static final MILLISECONDS_PER_SECOND:I = 0x3e8


# instance fields
.field private activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

.field private activityRecognitionPendingIntent:Landroid/app/PendingIntent;

.field private broadcastReceiver:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;

.field private context:Landroid/content/Context;

.field private curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

.field private isActive:Z

.field private isConnecting:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean v2, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isActive:Z

    .line 24
    iput-boolean v2, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isConnecting:Z

    .line 35
    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->START:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    iput-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    .line 47
    const-string/jumbo v1, "ActivityRecognitionController instantiated"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->context:Landroid/content/Context;

    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionIntentService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x8000000

    invoke-static {p1, v2, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionPendingIntent:Landroid/app/PendingIntent;

    .line 63
    new-instance v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;

    invoke-direct {v1}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;-><init>()V

    iput-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->broadcastReceiver:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;

    .line 64
    return-void
.end method

.method private declared-synchronized sendCommand()V
    .locals 2

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isConnecting:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 187
    :goto_0
    monitor-exit p0

    return-void

    .line 172
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    const-string/jumbo v0, "Google Play Services are not available on this device! Unable to stop activity recognition!"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 178
    :cond_1
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isConnecting:Z

    .line 180
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

    if-nez v0, :cond_2

    .line 181
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionClient;

    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Lcom/google/android/gms/location/ActivityRecognitionClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iput-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionClient;->connect()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private startSimulationMode()V
    .locals 3

    .prologue
    .line 155
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "activity_recognition_broadcast_action"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 156
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/inrix/sdk/google/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Lcom/inrix/sdk/google/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->broadcastReceiver:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/inrix/sdk/google/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 158
    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->SIMULATE:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    iput-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    .line 159
    invoke-direct {p0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->sendCommand()V

    .line 160
    const-string/jumbo v1, "Simulate activity recognition"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 161
    return-void
.end method


# virtual methods
.method public isActive()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isActive:Z

    return v0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Activity recognition connected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 108
    iput-boolean v2, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isConnecting:Z

    .line 109
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->START:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    if-ne v0, v1, :cond_1

    .line 110
    const-string/jumbo v0, "Request activity updates"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

    const-wide/16 v1, 0x7530

    iget-object v3, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/location/ActivityRecognitionClient;->requestActivityUpdates(JLandroid/app/PendingIntent;)V

    .line 115
    iput-boolean v4, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isActive:Z

    .line 132
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionClient;->disconnect()V

    .line 133
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->STOP:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    if-ne v0, v1, :cond_2

    .line 117
    const-string/jumbo v0, "Remove activity updates"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/ActivityRecognitionClient;->removeActivityUpdates(Landroid/app/PendingIntent;)V

    .line 120
    iput-boolean v2, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isActive:Z

    goto :goto_0

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->SIMULATE:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    if-ne v0, v1, :cond_0

    .line 122
    const-string/jumbo v0, "Remove activity updates"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 123
    const-string/jumbo v0, "Simulation mode is on"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isActive()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionClient:Lcom/google/android/gms/location/ActivityRecognitionClient;

    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->activityRecognitionPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/ActivityRecognitionClient;->removeActivityUpdates(Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 128
    :cond_3
    iput-boolean v4, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isActive:Z

    goto :goto_0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Activity recognition connection failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isConnecting:Z

    .line 148
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 137
    const-string/jumbo v0, "Activity recognition disconnected"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isConnecting:Z

    .line 140
    return-void
.end method

.method public startActivityRecognition()V
    .locals 3

    .prologue
    .line 70
    sget-object v1, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->START:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    iput-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    .line 71
    invoke-virtual {p0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    :goto_0
    return-void

    .line 75
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "activity_recognition_broadcast_action"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 76
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/inrix/sdk/google/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Lcom/inrix/sdk/google/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->broadcastReceiver:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/inrix/sdk/google/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 79
    invoke-direct {p0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->sendCommand()V

    .line 80
    const-string/jumbo v1, "Start activity recognition"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopActivityRecognition()V
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;->STOP:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    iput-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->curAction:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$Action;

    .line 89
    iget-object v0, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/inrix/sdk/google/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Lcom/inrix/sdk/google/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->broadcastReceiver:Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController$ActivityBroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/google/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 91
    invoke-direct {p0}, Lcom/inrix/sdk/activityrecognition/ActivityRecognitionController;->sendCommand()V

    .line 92
    const-string/jumbo v0, "Stop activity recognition"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 94
    return-void
.end method
