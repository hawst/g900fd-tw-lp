.class Lcom/inrix/sdk/LocationsManager$1;
.super Ljava/lang/Object;
.source "LocationsManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/LocationsManager;->requestSavedLocations(Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/LocationsCollection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/LocationsManager;

.field final synthetic val$listener:Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$1;->this$0:Lcom/inrix/sdk/LocationsManager;

    iput-object p2, p0, Lcom/inrix/sdk/LocationsManager$1;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/LocationsCollection;)V
    .locals 2
    .param p1, "response"    # Lcom/inrix/sdk/model/LocationsCollection;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$1;->val$listener:Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;

    invoke-virtual {p1}, Lcom/inrix/sdk/model/LocationsCollection;->getLocations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;->onResult(Ljava/lang/Object;)V

    .line 258
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 253
    check-cast p1, Lcom/inrix/sdk/model/LocationsCollection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/LocationsManager$1;->onResponse(Lcom/inrix/sdk/model/LocationsCollection;)V

    return-void
.end method
