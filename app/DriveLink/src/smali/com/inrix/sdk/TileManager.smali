.class public final Lcom/inrix/sdk/TileManager;
.super Ljava/lang/Object;
.source "TileManager.java"

# interfaces
.implements Lcom/inrix/sdk/IRefreshableActions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/TileManager$1;,
        Lcom/inrix/sdk/TileManager$TileOptions;,
        Lcom/inrix/sdk/TileManager$ACTIONS;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/IRefreshableActions",
        "<",
        "Lcom/inrix/sdk/TileManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION:Ljava/lang/String; = "Mobile.Tile"

.field private static final GET_TRAFFIC_TILE_INTERVAL:I = 0x5a

.field private static final MAX_LATITUDE:D = 85.05112878

.field private static final MAX_LONGITUDE:D = 180.0

.field private static final MAX_OPACITY_VALUE:I = 0x64

.field private static final MAX_ZOOM:I = 0x15

.field private static final MIN_LATITUDE:D = -85.05112878

.field private static final MIN_LONGITUDE:D = -180.0

.field private static final MIN_OPACITY_VALUE:I = 0x0

.field private static final MIN_ZOOM:I = 0x0

.field public static final TILE_COVERAGE_ALL:I = 0xff

.field public static final TILE_COVERAGE_HISTORICAL:I = 0x30

.field public static final TILE_COVERAGE_REALTIME_CORE:I = 0x1

.field public static final TILE_COVERAGE_REALTIME_CORE_EXTENDED:I = 0x8

.field public static final TILE_COVERAGE_REALTIME_EXTENDED:I = 0x4

.field public static final TILE_COVERAGE_REFERENCE:I = 0x20

.field public static final TILE_DEFAULT_HEIGHT:I = 0x100

.field public static final TILE_DEFAULT_OPACITY:I = 0x3c

.field public static final TILE_DEFAULT_PEN_WIDTH:I = 0x4

.field public static final TILE_DEFAULT_WIDTH:I = 0x100

.field public static final TILE_FORMAT_GIF:I = 0x1

.field private static final TILE_FORMAT_NAME_GIF:Ljava/lang/String; = "GIF"

.field private static final TILE_FORMAT_NAME_PNG:Ljava/lang/String; = "PNG"

.field public static final TILE_FORMAT_PNG:I = 0x0

.field public static final TILE_FRC_LEVEL_1:I = 0x1

.field public static final TILE_FRC_LEVEL_2:I = 0x2

.field public static final TILE_FRC_LEVEL_3:I = 0x4

.field public static final TILE_FRC_LEVEL_4:I = 0x8

.field public static final TILE_FRC_LEVEL_5:I = 0x10

.field public static final TILE_FRC_LEVEL_6:I = 0x20

.field public static final TILE_FRC_LEVEL_7:I = 0x40

.field public static final TILE_FRC_LEVEL_ALL:I = 0xff

.field private static final TILE_LAYER_TILE:Ljava/lang/String; = "T"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method static synthetic access$000(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 19
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->validateWidth(I)V

    return-void
.end method

.method static synthetic access$100(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 19
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->validateHeight(I)V

    return-void
.end method

.method static synthetic access$200(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 19
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->validateOpacity(I)V

    return-void
.end method

.method static synthetic access$300(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 19
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->validatePenWidth(I)V

    return-void
.end method

.method static synthetic access$400(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 19
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->validateFormat(I)V

    return-void
.end method

.method static synthetic access$500(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 19
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->validateFrcLevel(I)V

    return-void
.end method

.method static synthetic access$600(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 19
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->validateCoverage(I)V

    return-void
.end method

.method private static final clip(DDD)D
    .locals 2
    .param p0, "n"    # D
    .param p2, "minValue"    # D
    .param p4, "maxValue"    # D

    .prologue
    .line 903
    invoke-static {p0, p1, p2, p3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    invoke-static {v0, v1, p4, p5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static final geoPointToQuadKey(Lcom/inrix/sdk/model/GeoPoint;I)Ljava/lang/String;
    .locals 6
    .param p0, "point"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p1, "zoomLevel"    # I

    .prologue
    .line 705
    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5, p1}, Lcom/inrix/sdk/TileManager;->latLongToPixelXY(DDI)Landroid/util/Pair;

    move-result-object v0

    .line 706
    .local v0, "pixelXY":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v3, v2}, Lcom/inrix/sdk/TileManager;->pixelXYToTileXY(II)Landroid/util/Pair;

    move-result-object v1

    .line 707
    .local v1, "tileXY":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v3, v2, p1}, Lcom/inrix/sdk/TileManager;->tileXYtoQuadKey(III)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getUriBuilderWithCommonParameters(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/TileManager$TileOptions;)Landroid/net/Uri$Builder;
    .locals 7
    .param p1, "point"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "options"    # Lcom/inrix/sdk/TileManager$TileOptions;

    .prologue
    const/4 v6, -0x1

    .line 625
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    .line 627
    .local v3, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/inrix/sdk/network/request/RequestExecutor;->getAuthenticator()Lcom/inrix/sdk/auth/InrixAuthenticator;

    move-result-object v4

    sget-object v5, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-virtual {v4, v5}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getServerPath(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v2

    .line 628
    .local v2, "ttsUrl":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/inrix/sdk/network/request/RequestExecutor;->getAuthenticator()Lcom/inrix/sdk/auth/InrixAuthenticator;

    move-result-object v4

    invoke-virtual {v4}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    .line 633
    .local v1, "token":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 634
    :cond_0
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/inrix/sdk/network/request/RequestExecutor;->getAuthenticator()Lcom/inrix/sdk/auth/InrixAuthenticator;

    move-result-object v4

    invoke-virtual {v4}, Lcom/inrix/sdk/auth/InrixAuthenticator;->refreshToken()V

    .line 635
    const/4 v3, 0x0

    .line 663
    .end local v3    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_1
    :goto_0
    return-object v3

    .line 638
    .restart local v3    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_2
    invoke-virtual {v3, v2}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 641
    const-string/jumbo v4, "token"

    invoke-virtual {v3, v4, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 642
    const-string/jumbo v4, "action"

    const-string/jumbo v5, "Mobile.Tile"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 643
    const-string/jumbo v4, "layers"

    const-string/jumbo v5, "T"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 644
    const-string/jumbo v4, "opacity"

    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getOpacity()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 645
    const-string/jumbo v4, "penwidth"

    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getPenWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 646
    const-string/jumbo v4, "width"

    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 647
    const-string/jumbo v4, "height"

    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getHeight()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 648
    const-string/jumbo v4, "format"

    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getFormat()I

    move-result v5

    invoke-static {v5}, Lcom/inrix/sdk/TileManager;->resolveFormat(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 650
    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getCoverage()I

    move-result v4

    if-eq v4, v6, :cond_3

    .line 651
    const-string/jumbo v4, "coverage"

    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getCoverage()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 654
    :cond_3
    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getSpeedBucketId()I

    move-result v4

    if-eq v4, v6, :cond_4

    .line 655
    const-string/jumbo v4, "speedbucketid"

    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getSpeedBucketId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 658
    :cond_4
    invoke-virtual {p2}, Lcom/inrix/sdk/TileManager$TileOptions;->getFrcLevel()I

    move-result v4

    invoke-static {v4}, Lcom/inrix/sdk/TileManager;->resolveFrcLevel(I)Ljava/lang/String;

    move-result-object v0

    .line 659
    .local v0, "frcLevelValue":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 660
    const-string/jumbo v4, "frclevel"

    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto/16 :goto_0
.end method

.method private static final latLongToPixelXY(DDI)Landroid/util/Pair;
    .locals 19
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D
    .param p4, "zoomLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 728
    const-wide v2, -0x3faabcba4e5a8100L    # -85.05112878

    const-wide v4, 0x40554345b1a57f00L    # 85.05112878

    move-wide/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/inrix/sdk/TileManager;->clip(DDD)D

    move-result-wide v6

    .line 729
    .local v6, "adjusted_latitude":D
    const-wide v2, -0x3f99800000000000L    # -180.0

    const-wide v4, 0x4066800000000000L    # 180.0

    move-wide/from16 v0, p2

    invoke-static/range {v0 .. v5}, Lcom/inrix/sdk/TileManager;->clip(DDD)D

    move-result-wide v8

    .line 731
    .local v8, "adjusted_longitude":D
    const-wide v0, 0x4066800000000000L    # 180.0

    add-double/2addr v0, v8

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double v15, v0, v2

    .line 732
    .local v15, "x":D
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v6

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    .line 733
    .local v13, "sinLatitude":D
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v13

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v13

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide v4, 0x402921fb54442d18L    # 12.566370614359172

    div-double/2addr v2, v4

    sub-double v17, v0, v2

    .line 735
    .local v17, "y":D
    invoke-static/range {p4 .. p4}, Lcom/inrix/sdk/TileManager;->mapSize(I)I

    move-result v10

    .line 736
    .local v10, "mapSize":I
    int-to-double v0, v10

    mul-double/2addr v0, v15

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    const-wide/16 v2, 0x0

    add-int/lit8 v4, v10, -0x1

    int-to-double v4, v4

    invoke-static/range {v0 .. v5}, Lcom/inrix/sdk/TileManager;->clip(DDD)D

    move-result-wide v0

    double-to-int v11, v0

    .line 737
    .local v11, "pixelX":I
    int-to-double v0, v10

    mul-double v0, v0, v17

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    const-wide/16 v2, 0x0

    add-int/lit8 v4, v10, -0x1

    int-to-double v4, v4

    invoke-static/range {v0 .. v5}, Lcom/inrix/sdk/TileManager;->clip(DDD)D

    move-result-wide v0

    double-to-int v12, v0

    .line 739
    .local v12, "pixelY":I
    new-instance v0, Landroid/util/Pair;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static final mapSize(I)I
    .locals 1
    .param p0, "zoomLevel"    # I

    .prologue
    .line 916
    const/16 v0, 0x100

    shl-int/2addr v0, p0

    return v0
.end method

.method private static final pixelXYToLatLong(III)Lcom/inrix/sdk/model/GeoPoint;
    .locals 19
    .param p0, "pixelX"    # I
    .param p1, "pixelY"    # I
    .param p2, "zoomLevel"    # I

    .prologue
    .line 876
    invoke-static/range {p2 .. p2}, Lcom/inrix/sdk/TileManager;->mapSize(I)I

    move-result v1

    int-to-double v11, v1

    .line 877
    .local v11, "mapSize":D
    move/from16 v0, p0

    int-to-double v1, v0

    const-wide/16 v3, 0x0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double v5, v11, v5

    invoke-static/range {v1 .. v6}, Lcom/inrix/sdk/TileManager;->clip(DDD)D

    move-result-wide v1

    div-double/2addr v1, v11

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    sub-double v13, v1, v3

    .line 878
    .local v13, "x":D
    const-wide/high16 v17, 0x3fe0000000000000L    # 0.5

    move/from16 v0, p1

    int-to-double v1, v0

    const-wide/16 v3, 0x0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double v5, v11, v5

    invoke-static/range {v1 .. v6}, Lcom/inrix/sdk/TileManager;->clip(DDD)D

    move-result-wide v1

    div-double/2addr v1, v11

    sub-double v15, v17, v1

    .line 880
    .local v15, "y":D
    const-wide v1, 0x4056800000000000L    # 90.0

    const-wide v3, 0x4076800000000000L    # 360.0

    neg-double v5, v15

    const-wide/high16 v17, 0x4000000000000000L    # 2.0

    mul-double v5, v5, v17

    const-wide v17, 0x400921fb54442d18L    # Math.PI

    mul-double v5, v5, v17

    invoke-static {v5, v6}, Ljava/lang/Math;->exp(D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->atan(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    const-wide v5, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v3, v5

    sub-double v7, v1, v3

    .line 881
    .local v7, "latitude":D
    const-wide v1, 0x4076800000000000L    # 360.0

    mul-double v9, v1, v13

    .line 882
    .local v9, "longitude":D
    new-instance v1, Lcom/inrix/sdk/model/GeoPoint;

    invoke-direct {v1, v7, v8, v9, v10}, Lcom/inrix/sdk/model/GeoPoint;-><init>(DD)V

    return-object v1
.end method

.method private static final pixelXYToTileXY(II)Landroid/util/Pair;
    .locals 5
    .param p0, "pixelX"    # I
    .param p1, "pixelY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756
    div-int/lit16 v0, p0, 0x100

    .line 757
    .local v0, "tileX":I
    div-int/lit16 v1, p1, 0x100

    .line 758
    .local v1, "tileY":I
    new-instance v2, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method public static final quadKeyToGeoPoint(Ljava/lang/String;)Lcom/inrix/sdk/model/GeoPoint;
    .locals 9
    .param p0, "quadKey"    # Ljava/lang/String;

    .prologue
    .line 678
    invoke-static {p0}, Lcom/inrix/sdk/TileManager;->quadKeyToTileXY(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v5

    .line 680
    .local v5, "tileXY":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;Ljava/lang/Integer;>;"
    iget-object v8, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Landroid/util/Pair;

    iget-object v8, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 681
    .local v4, "tileX":I
    iget-object v8, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Landroid/util/Pair;

    iget-object v8, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 682
    .local v6, "tileY":I
    invoke-static {v4, v6}, Lcom/inrix/sdk/TileManager;->tileXYToPixelXY(II)Landroid/util/Pair;

    move-result-object v1

    .line 684
    .local v1, "pixelXY":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v8, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 685
    .local v0, "pixelX":I
    iget-object v8, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 686
    .local v2, "pixelY":I
    iget-object v8, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 687
    .local v7, "zoomLevel":I
    invoke-static {v0, v2, v7}, Lcom/inrix/sdk/TileManager;->pixelXYToLatLong(III)Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    .line 688
    .local v3, "point":Lcom/inrix/sdk/model/GeoPoint;
    return-object v3
.end method

.method private static quadKeyToTileXY(Ljava/lang/String;)Landroid/util/Pair;
    .locals 9
    .param p0, "quadKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 810
    const/4 v4, 0x0

    .line 811
    .local v4, "tileX":I
    const/4 v6, 0x0

    .line 812
    .local v6, "tileY":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 814
    .local v1, "levelOfDetail":I
    move v0, v1

    .local v0, "i":I
    :goto_0
    if-lez v0, :cond_0

    .line 815
    const/4 v7, 0x1

    add-int/lit8 v8, v0, -0x1

    shl-int v2, v7, v8

    .line 816
    .local v2, "mask":I
    sub-int v7, v1, v0

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 830
    new-instance v7, Lcom/inrix/sdk/exception/InrixException;

    const/16 v8, 0x403

    invoke-direct {v7, v8}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v7

    .line 820
    :pswitch_0
    or-int/2addr v4, v2

    .line 814
    :goto_1
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 823
    :pswitch_2
    or-int/2addr v6, v2

    .line 824
    goto :goto_1

    .line 826
    :pswitch_3
    or-int/2addr v4, v2

    .line 827
    or-int/2addr v6, v2

    .line 828
    goto :goto_1

    .line 834
    .end local v2    # "mask":I
    :cond_0
    new-instance v5, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v5, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 835
    .local v5, "tileXY":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v3, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 836
    .local v3, "result":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;Ljava/lang/Integer;>;"
    return-object v3

    .line 816
    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static final resolveFormat(I)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 988
    packed-switch p0, :pswitch_data_0

    .line 994
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x405

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 990
    :pswitch_0
    const-string/jumbo v0, "PNG"

    .line 992
    :goto_0
    return-object v0

    :pswitch_1
    const-string/jumbo v0, "GIF"

    goto :goto_0

    .line 988
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final resolveFrcLevel(I)Ljava/lang/String;
    .locals 6
    .param p0, "frcLevel"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 999
    and-int/lit16 v1, p0, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_0

    .line 1000
    const/4 v1, 0x0

    .line 1032
    :goto_0
    return-object v1

    .line 1003
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1004
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    and-int/lit8 v1, p0, 0x1

    if-ne v1, v3, :cond_1

    .line 1005
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1008
    :cond_1
    and-int/lit8 v1, p0, 0x2

    if-ne v1, v4, :cond_2

    .line 1009
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1012
    :cond_2
    and-int/lit8 v1, p0, 0x4

    if-ne v1, v5, :cond_3

    .line 1013
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1016
    :cond_3
    and-int/lit8 v1, p0, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 1017
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1020
    :cond_4
    and-int/lit8 v1, p0, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 1021
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1024
    :cond_5
    and-int/lit8 v1, p0, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 1025
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1028
    :cond_6
    and-int/lit8 v1, p0, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 1029
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1032
    :cond_7
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static final tileXYToPixelXY(II)Landroid/util/Pair;
    .locals 5
    .param p0, "tileX"    # I
    .param p1, "tileY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 853
    mul-int/lit16 v0, p0, 0x100

    .line 854
    .local v0, "pixelX":I
    mul-int/lit16 v1, p1, 0x100

    .line 855
    .local v1, "pixelY":I
    new-instance v2, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private static final tileXYtoQuadKey(III)Ljava/lang/String;
    .locals 6
    .param p0, "tileX"    # I
    .param p1, "tileY"    # I
    .param p2, "zoomLevel"    # I

    .prologue
    .line 782
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 784
    .local v3, "quadKey":Ljava/lang/StringBuilder;
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-lez v1, :cond_2

    .line 785
    const/16 v0, 0x30

    .line 786
    .local v0, "digit":C
    const/4 v4, 0x1

    add-int/lit8 v5, v1, -0x1

    shl-int v2, v4, v5

    .line 787
    .local v2, "mask":I
    and-int v4, p0, v2

    if-eqz v4, :cond_0

    .line 788
    const/16 v4, 0x31

    int-to-char v0, v4

    .line 790
    :cond_0
    and-int v4, p1, v2

    if-eqz v4, :cond_1

    .line 791
    add-int/lit8 v4, v0, 0x1

    int-to-char v0, v4

    .line 792
    add-int/lit8 v4, v0, 0x1

    int-to-char v0, v4

    .line 794
    :cond_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 784
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 797
    .end local v0    # "digit":C
    .end local v2    # "mask":I
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static final validateCoverage(I)V
    .locals 2
    .param p0, "coverage"    # I

    .prologue
    .line 948
    const/16 v0, 0xff

    if-eq p0, v0, :cond_0

    const/16 v0, 0x30

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    .line 949
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x406

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 951
    :cond_0
    return-void
.end method

.method private static final validateFormat(I)V
    .locals 2
    .param p0, "format"    # I

    .prologue
    .line 942
    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    if-eqz p0, :cond_0

    .line 943
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x405

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 945
    :cond_0
    return-void
.end method

.method private static final validateFrcLevel(I)V
    .locals 2
    .param p0, "frcLevel"    # I

    .prologue
    .line 954
    if-ltz p0, :cond_0

    const/16 v0, 0xff

    if-le p0, v0, :cond_1

    .line 955
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x407

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 957
    :cond_1
    return-void
.end method

.method private static final validateGeoPoint(Lcom/inrix/sdk/model/GeoPoint;I)V
    .locals 1
    .param p0, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p1, "errorMessageID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 966
    invoke-static {p0}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 967
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    invoke-direct {v0, p1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 969
    :cond_0
    return-void
.end method

.method private static final validateHeight(I)V
    .locals 2
    .param p0, "height"    # I

    .prologue
    .line 978
    if-gez p0, :cond_0

    .line 979
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x40d

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 981
    :cond_0
    return-void
.end method

.method private static final validateOpacity(I)V
    .locals 2
    .param p0, "opacity"    # I

    .prologue
    .line 930
    if-ltz p0, :cond_0

    const/16 v0, 0x64

    if-le p0, v0, :cond_1

    .line 931
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x401

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 933
    :cond_1
    return-void
.end method

.method private static final validatePenWidth(I)V
    .locals 2
    .param p0, "penWidth"    # I

    .prologue
    .line 936
    if-gtz p0, :cond_0

    .line 937
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x404

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 939
    :cond_0
    return-void
.end method

.method private static final validateQuadKey(Ljava/lang/String;)V
    .locals 2
    .param p0, "quadKey"    # Ljava/lang/String;

    .prologue
    .line 924
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 925
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x402

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 927
    :cond_0
    return-void
.end method

.method private static final validateWidth(I)V
    .locals 2
    .param p0, "width"    # I

    .prologue
    .line 972
    if-gez p0, :cond_0

    .line 973
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x40c

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 975
    :cond_0
    return-void
.end method

.method private static final validateZoom(I)V
    .locals 2
    .param p0, "zoom"    # I

    .prologue
    .line 960
    if-ltz p0, :cond_0

    const/16 v0, 0x15

    if-le p0, v0, :cond_1

    .line 961
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x40b

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 963
    :cond_1
    return-void
.end method


# virtual methods
.method public getRefreshInterval(Lcom/inrix/sdk/TileManager$ACTIONS;)I
    .locals 3
    .param p1, "action"    # Lcom/inrix/sdk/TileManager$ACTIONS;

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x412

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 37
    :cond_0
    sget-object v1, Lcom/inrix/sdk/TileManager$1;->$SwitchMap$com$inrix$sdk$TileManager$ACTIONS:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$ACTIONS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    .line 39
    const/16 v0, 0x5a

    .line 43
    .local v0, "interval":I
    return v0
.end method

.method public bridge synthetic getRefreshInterval(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/inrix/sdk/TileManager$ACTIONS;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/TileManager;->getRefreshInterval(Lcom/inrix/sdk/TileManager$ACTIONS;)I

    move-result v0

    return v0
.end method

.method public final getTrafficTileUrl(IIILcom/inrix/sdk/TileManager$TileOptions;)Ljava/lang/String;
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "zoom"    # I
    .param p4, "options"    # Lcom/inrix/sdk/TileManager$TileOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    .line 498
    invoke-static {p3}, Lcom/inrix/sdk/TileManager;->validateZoom(I)V

    .line 501
    if-ltz p1, :cond_0

    int-to-double v1, p3

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_1

    .line 502
    :cond_0
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x40e

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 505
    :cond_1
    if-ltz p2, :cond_2

    int-to-double v1, p3

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le p2, v1, :cond_3

    .line 506
    :cond_2
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x40f

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 509
    :cond_3
    invoke-static {p1, p2, p3}, Lcom/inrix/sdk/TileManager;->tileXYtoQuadKey(III)Ljava/lang/String;

    move-result-object v0

    .line 510
    .local v0, "quadKey":Ljava/lang/String;
    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/TileManager;->getTrafficTileUrl(Ljava/lang/String;Lcom/inrix/sdk/TileManager$TileOptions;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final getTrafficTileUrl(Lcom/inrix/sdk/model/GeoPoint;IIILcom/inrix/sdk/TileManager$TileOptions;)Ljava/lang/String;
    .locals 3
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "zoom"    # I
    .param p3, "north"    # I
    .param p4, "east"    # I
    .param p5, "options"    # Lcom/inrix/sdk/TileManager$TileOptions;

    .prologue
    .line 576
    const/16 v1, 0x408

    invoke-static {p1, v1}, Lcom/inrix/sdk/TileManager;->validateGeoPoint(Lcom/inrix/sdk/model/GeoPoint;I)V

    .line 577
    invoke-static {p2}, Lcom/inrix/sdk/TileManager;->validateZoom(I)V

    .line 580
    invoke-direct {p0, p1, p5}, Lcom/inrix/sdk/TileManager;->getUriBuilderWithCommonParameters(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/TileManager$TileOptions;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 581
    .local v0, "uriBuilder":Landroid/net/Uri$Builder;
    if-nez v0, :cond_0

    .line 582
    const/4 v1, 0x0

    .line 591
    :goto_0
    return-object v1

    .line 586
    :cond_0
    const-string/jumbo v1, "center"

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->toQueryParam()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 587
    const-string/jumbo v1, "zoom"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 588
    const-string/jumbo v1, "north"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 589
    const-string/jumbo v1, "east"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 591
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final getTrafficTileUrl(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/TileManager$TileOptions;)Ljava/lang/String;
    .locals 3
    .param p1, "corner1"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "corner2"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p3, "options"    # Lcom/inrix/sdk/TileManager$TileOptions;

    .prologue
    .line 533
    const/16 v1, 0x409

    invoke-static {p1, v1}, Lcom/inrix/sdk/TileManager;->validateGeoPoint(Lcom/inrix/sdk/model/GeoPoint;I)V

    .line 534
    const/16 v1, 0x40a

    invoke-static {p2, v1}, Lcom/inrix/sdk/TileManager;->validateGeoPoint(Lcom/inrix/sdk/model/GeoPoint;I)V

    .line 537
    invoke-direct {p0, p1, p3}, Lcom/inrix/sdk/TileManager;->getUriBuilderWithCommonParameters(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/TileManager$TileOptions;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 538
    .local v0, "uriBuilder":Landroid/net/Uri$Builder;
    if-nez v0, :cond_0

    .line 539
    const/4 v1, 0x0

    .line 546
    :goto_0
    return-object v1

    .line 543
    :cond_0
    const-string/jumbo v1, "corner1"

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->toQueryParam()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 544
    const-string/jumbo v1, "corner2"

    invoke-virtual {p2}, Lcom/inrix/sdk/model/GeoPoint;->toQueryParam()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 546
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final getTrafficTileUrl(Ljava/lang/String;Lcom/inrix/sdk/TileManager$TileOptions;)Ljava/lang/String;
    .locals 3
    .param p1, "quadKey"    # Ljava/lang/String;
    .param p2, "options"    # Lcom/inrix/sdk/TileManager$TileOptions;

    .prologue
    .line 609
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->validateQuadKey(Ljava/lang/String;)V

    .line 612
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->quadKeyToGeoPoint(Ljava/lang/String;)Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v0

    .line 613
    .local v0, "point":Lcom/inrix/sdk/model/GeoPoint;
    invoke-direct {p0, v0, p2}, Lcom/inrix/sdk/TileManager;->getUriBuilderWithCommonParameters(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/TileManager$TileOptions;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 614
    .local v1, "uriBuilder":Landroid/net/Uri$Builder;
    if-nez v1, :cond_0

    .line 615
    const/4 v2, 0x0

    .line 621
    :goto_0
    return-object v2

    .line 619
    :cond_0
    const-string/jumbo v2, "quadkey"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 621
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
