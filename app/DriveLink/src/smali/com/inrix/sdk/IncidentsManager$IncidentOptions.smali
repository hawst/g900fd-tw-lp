.class public abstract Lcom/inrix/sdk/IncidentsManager$IncidentOptions;
.super Ljava/lang/Object;
.source "IncidentsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/IncidentsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "IncidentOptions"
.end annotation


# instance fields
.field private incidentOutputFields:I

.field private incidentSource:I

.field private incidentType:I

.field private severity:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0xffff

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput v1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentType:I

    .line 187
    const/16 v0, 0xff

    iput v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentSource:I

    .line 188
    iput v1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentOutputFields:I

    return-void
.end method


# virtual methods
.method getIncidentOutputFields()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentOutputFields:I

    return v0
.end method

.method getIncidentSource()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentSource:I

    return v0
.end method

.method getIncidentType()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentType:I

    return v0
.end method

.method getSeverity()[Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->severity:[Ljava/lang/Integer;

    return-object v0
.end method

.method public setIncidentOutputFields(I)Lcom/inrix/sdk/IncidentsManager$IncidentOptions;
    .locals 0
    .param p1, "incidentOutputFields"    # I

    .prologue
    .line 245
    iput p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentOutputFields:I

    .line 246
    return-object p0
.end method

.method public setIncidentSource(I)Lcom/inrix/sdk/IncidentsManager$IncidentOptions;
    .locals 0
    .param p1, "incidentSource"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentSource:I

    .line 229
    return-object p0
.end method

.method public setIncidentType(I)Lcom/inrix/sdk/IncidentsManager$IncidentOptions;
    .locals 0
    .param p1, "incidentType"    # I

    .prologue
    .line 205
    iput p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->incidentType:I

    .line 206
    return-object p0
.end method

.method public setSeverity([Ljava/lang/Integer;)Lcom/inrix/sdk/IncidentsManager$IncidentOptions;
    .locals 4
    .param p1, "severity"    # [Ljava/lang/Integer;

    .prologue
    const/16 v3, 0x400

    .line 262
    if-eqz p1, :cond_3

    .line 264
    array-length v1, p1

    const/4 v2, 0x5

    if-le v1, v2, :cond_0

    .line 265
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    invoke-direct {v1, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 267
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 268
    aget-object v1, p1, v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ltz v1, :cond_1

    aget-object v1, p1, v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x4

    if-le v1, v2, :cond_2

    .line 269
    :cond_1
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    invoke-direct {v1, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 267
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274
    .end local v0    # "i":I
    :cond_3
    iput-object p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;->severity:[Ljava/lang/Integer;

    .line 275
    return-object p0
.end method
