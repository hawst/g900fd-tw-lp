.class public Lcom/inrix/sdk/GasStationManager;
.super Ljava/lang/Object;
.source "GasStationManager.java"

# interfaces
.implements Lcom/inrix/sdk/IRefreshableActions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/GasStationManager$7;,
        Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;,
        Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;,
        Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;,
        Lcom/inrix/sdk/GasStationManager$GasStationsOptions;,
        Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;,
        Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;,
        Lcom/inrix/sdk/GasStationManager$ACTIONS;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/IRefreshableActions",
        "<",
        "Lcom/inrix/sdk/GasStationManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final GET_GASSTATIONS_INTERVAL:I = 0xb4

.field private static final GET_GASSTATION_INFORMATION_INTERVAL:I = 0x258


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 724
    return-void
.end method


# virtual methods
.method public final getGasStationInformation(Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 6
    .param p1, "requestParameters"    # Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 179
    if-nez p2, :cond_0

    .line 180
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 183
    :cond_0
    if-nez p1, :cond_1

    .line 184
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 187
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->isGasStationIDValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 188
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ed

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 191
    :cond_2
    new-instance v0, Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->getStationID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->getOutputFieldsString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->getProductTypesString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/inrix/sdk/GasStationManager$5;

    invoke-direct {v4, p0, p2}, Lcom/inrix/sdk/GasStationManager$5;-><init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;)V

    new-instance v5, Lcom/inrix/sdk/GasStationManager$6;

    invoke-direct {v5, p0, p2}, Lcom/inrix/sdk/GasStationManager$6;-><init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;)V

    invoke-direct/range {v0 .. v5}, Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 210
    .local v0, "gasStationRequest":Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 212
    return-object v0
.end method

.method public final getGasStationsInBox(Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 13
    .param p1, "requestParameters"    # Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x3e9

    .line 131
    if-nez p2, :cond_0

    .line 132
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 135
    :cond_0
    if-nez p1, :cond_1

    .line 136
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 139
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->isStartValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 140
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 144
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->isEndValid()Z

    move-result v1

    if-nez v1, :cond_3

    .line 145
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 148
    :cond_3
    new-instance v0, Lcom/inrix/sdk/network/request/GasStationsBoxRequest;

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->getBoxStart()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->getBoxStart()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->getBoxEnd()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v5

    invoke-virtual {v5}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->getBoxEnd()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v7

    invoke-virtual {v7}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v7

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->getOutputFieldsString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsBoxOptions;->getProductTypesString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Lcom/inrix/sdk/GasStationManager$3;

    invoke-direct {v11, p0, p2}, Lcom/inrix/sdk/GasStationManager$3;-><init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)V

    new-instance v12, Lcom/inrix/sdk/GasStationManager$4;

    invoke-direct {v12, p0, p2}, Lcom/inrix/sdk/GasStationManager$4;-><init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)V

    invoke-direct/range {v0 .. v12}, Lcom/inrix/sdk/network/request/GasStationsBoxRequest;-><init>(DDDDLjava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 170
    .local v0, "gasStationsRequest":Lcom/inrix/sdk/network/request/GasStationsBoxRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 172
    return-object v0
.end method

.method public final getGasStationsInRadius(Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 12
    .param p1, "requestParameters"    # Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 82
    if-nez p2, :cond_0

    .line 83
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 86
    :cond_0
    if-nez p1, :cond_1

    .line 87
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 90
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->isCenterValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 91
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3e9

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 95
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->isRadiusValid()Z

    move-result v1

    if-nez v1, :cond_3

    .line 96
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ec

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 100
    :cond_3
    new-instance v0, Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->getCenter()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->getCenter()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->getRadius()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v7

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->getOutputFieldsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->getProductTypesString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/inrix/sdk/GasStationManager$1;

    invoke-direct {v10, p0, p2}, Lcom/inrix/sdk/GasStationManager$1;-><init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)V

    new-instance v11, Lcom/inrix/sdk/GasStationManager$2;

    invoke-direct {v11, p0, p2}, Lcom/inrix/sdk/GasStationManager$2;-><init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)V

    invoke-direct/range {v0 .. v11}, Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;-><init>(DDDLcom/inrix/sdk/utils/UserPreferences$UNIT;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 122
    .local v0, "gasStationsRequest":Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 124
    return-object v0
.end method

.method public getRefreshInterval(Lcom/inrix/sdk/GasStationManager$ACTIONS;)I
    .locals 3
    .param p1, "action"    # Lcom/inrix/sdk/GasStationManager$ACTIONS;

    .prologue
    .line 43
    if-nez p1, :cond_0

    .line 44
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x412

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 46
    :cond_0
    sget-object v1, Lcom/inrix/sdk/GasStationManager$7;->$SwitchMap$com$inrix$sdk$GasStationManager$ACTIONS:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/GasStationManager$ACTIONS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 52
    const/16 v0, 0x258

    .line 56
    .local v0, "interval":I
    :goto_0
    return v0

    .line 48
    .end local v0    # "interval":I
    :pswitch_0
    const/16 v0, 0xb4

    .line 49
    .restart local v0    # "interval":I
    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic getRefreshInterval(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/inrix/sdk/GasStationManager$ACTIONS;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/GasStationManager;->getRefreshInterval(Lcom/inrix/sdk/GasStationManager$ACTIONS;)I

    move-result v0

    return v0
.end method
