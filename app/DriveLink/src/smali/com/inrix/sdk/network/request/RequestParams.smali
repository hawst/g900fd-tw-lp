.class public final Lcom/inrix/sdk/network/request/RequestParams;
.super Ljava/lang/Object;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/network/request/RequestParams$IncidentSourceName;,
        Lcom/inrix/sdk/network/request/RequestParams$IncidentResultTypeName;,
        Lcom/inrix/sdk/network/request/RequestParams$IncidentOutputFieldName;
    }
.end annotation


# static fields
.field public static final ACTION:Ljava/lang/String; = "action"

.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final ADDRESS_TYPE:Ljava/lang/String; = "addresstype"

.field public static final ADDRESS_TYPE_EMAIL:Ljava/lang/String; = "email"

.field public static final ANNONYMOUS_VEHICLE_ID:Ljava/lang/String; = "AnonymousVehicleId"

.field public static final APP_VERSION:Ljava/lang/String; = "appversion"

.field public static final ARRIVAL_TIME:Ljava/lang/String; = "arrivaltime"

.field public static final AUTHTOKEN:Ljava/lang/String; = "authtoken"

.field public static final CENTER:Ljava/lang/String; = "center"

.field public static final COMPRESS:Ljava/lang/String; = "compress"

.field public static final CONFIRMED:Ljava/lang/String; = "confirmed"

.field public static final CORNER1:Ljava/lang/String; = "corner1"

.field public static final CORNER2:Ljava/lang/String; = "corner2"

.field public static final COVERAGE:Ljava/lang/String; = "coverage"

.field public static final CUSTOM_DATA:Ljava/lang/String; = "customdata"

.field public static final DEVICE_ID:Ljava/lang/String; = "deviceid"

.field public static final DEVICE_MODEL:Ljava/lang/String; = "devicemodel"

.field public static final DEVICE_NAME:Ljava/lang/String; = "devicename"

.field public static final EAST:Ljava/lang/String; = "east"

.field public static final EMAIL:Ljava/lang/String; = "email"

.field public static final ENABLED:Ljava/lang/String; = "enabled"

.field public static final END_LATITUDE:Ljava/lang/String; = "endlatitude"

.field public static final END_LONGITUDE:Ljava/lang/String; = "endlongitude"

.field public static final EVENT_CODE:Ljava/lang/String; = "eventcode"

.field public static final FORMAT:Ljava/lang/String; = "format"

.field public static final FRC_LEVEL:Ljava/lang/String; = "frclevel"

.field public static final GAS_STATIONS_OUTPUT_FIELDS:Ljava/lang/String; = "outputFields"

.field public static final GAS_STATIONS_PRODUCT_TYPE:Ljava/lang/String; = "productType"

.field public static final GAS_STATION_ID:Ljava/lang/String; = "gasStationId"

.field public static final GEOID:Ljava/lang/String; = "geoid"

.field public static final HARDWARE_ID:Ljava/lang/String; = "hardwareid"

.field public static final HEADING:Ljava/lang/String; = "wp_heading"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final INCIDENT_ID:Ljava/lang/String; = "incidentid"

.field public static final INCIDENT_OUTPUT_FIELDS:Ljava/lang/String; = "incidentoutputfields"

.field public static final INCIDENT_SOURCE:Ljava/lang/String; = "incidentsource"

.field public static final INCIDENT_TYPE:Ljava/lang/String; = "incidenttype"

.field public static final INCIDENT_VERSION:Ljava/lang/String; = "incidentversion"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LAYERS:Ljava/lang/String; = "layers"

.field public static final LOCALE:Ljava/lang/String; = "locale"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LOCATION_ID:Ljava/lang/String; = "locationid"

.field public static final LOCATION_TYPE:Ljava/lang/String; = "locationtype"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MAX_ALTERNATES:Ljava/lang/String; = "maxalternates"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NORTH:Ljava/lang/String; = "north"

.field public static final OLD_PASSWORD:Ljava/lang/String; = "oldpassword"

.field public static final OPACITY:Ljava/lang/String; = "opacity"

.field public static final ORDERING:Ljava/lang/String; = "ordering"

.field public static final OUTPUT_FIELDS:Ljava/lang/String; = "outputfields"

.field public static final PARKING_LOT_IDS:Ljava/lang/String; = "parkinglotids"

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final PEN_WIDTH:Ljava/lang/String; = "penwidth"

.field public static final PHS:Ljava/lang/String; = "phs"

.field public static final PHSX:Ljava/lang/String; = "phsx"

.field public static final QUADKEY:Ljava/lang/String; = "quadkey"

.field public static final RADIUS:Ljava/lang/String; = "radius"

.field public static final ROUTE_OUTPUT_FIELDS:Ljava/lang/String; = "routeoutputfields"

.field public static final SEVERITY:Ljava/lang/String; = "severity"

.field public static final SORT:Ljava/lang/String; = "sort"

.field public static final SPEED_BUCKET_ID:Ljava/lang/String; = "speedbucketid"

.field public static final START_LATITUDE:Ljava/lang/String; = "startlatitude"

.field public static final START_LONGITUDE:Ljava/lang/String; = "startlongitude"

.field public static final SYSTEM_VERSION:Ljava/lang/String; = "systemversion"

.field public static final TOKEN:Ljava/lang/String; = "token"

.field public static final TOLERANCE:Ljava/lang/String; = "geometrytolerance"

.field public static final TRAVEL_ARRIVAL_TIME:Ljava/lang/String; = "arrivaltime"

.field public static final TRAVEL_DEPARTURE_TIME:Ljava/lang/String; = "departuretime"

.field public static final TRAVEL_ROUTE_ID:Ljava/lang/String; = "routeid"

.field public static final TRAVEL_TIME_COUNT:Ljava/lang/String; = "traveltimecount"

.field public static final TRAVEL_TIME_INTERVAL:Ljava/lang/String; = "traveltimeinterval"

.field public static final UNITS:Ljava/lang/String; = "units"

.field public static final USER_INITIATED:Ljava/lang/String; = "UserInitiated"

.field public static final USE_TRAFFIC:Ljava/lang/String; = "usetraffic"

.field public static final VENDOR_ID:Ljava/lang/String; = "vendorid"

.field public static final WIDTH:Ljava/lang/String; = "width"

.field public static final ZOOM:Ljava/lang/String; = "zoom"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    return-void
.end method
