.class public Lcom/inrix/sdk/network/request/TravelTimeRequest;
.super Lcom/inrix/sdk/network/request/XMLInrixRequest;
.source "TravelTimeRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/XMLInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/TravelTimeResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION_TRAVEL_TIMES_REQUEST:Ljava/lang/String; = "Mobile.Route.TravelTimes"


# instance fields
.field private currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

.field private currentRoute:Lcom/inrix/sdk/model/Route;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/RouteManager$TravelTimeOptions;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 3
    .param p1, "requestParameters"    # Lcom/inrix/sdk/RouteManager$TravelTimeOptions;
    .param p3, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/RouteManager$TravelTimeOptions;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/TravelTimeResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/TravelTimeResponse;>;"
    invoke-direct {p0, p2, p3}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 42
    iput-object p1, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

    .line 43
    iget-object v0, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

    invoke-virtual {v0}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->getRoute()Lcom/inrix/sdk/model/Route;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRoute:Lcom/inrix/sdk/model/Route;

    .line 44
    iget-object v0, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRoute:Lcom/inrix/sdk/model/Route;

    if-eqz v0, :cond_0

    .line 45
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.Route.TravelTimes"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/TravelTimeRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 46
    const-string/jumbo v0, "routeid"

    iget-object v1, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRoute:Lcom/inrix/sdk/model/Route;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/Route;->getId()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/inrix/sdk/network/request/TravelTimeRequest;->setParameter(Ljava/lang/String;J)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 47
    const-string/jumbo v0, "departuretime"

    iget-object v1, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

    invoke-virtual {v1}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->getDepartureTimeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/TravelTimeRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 48
    const-string/jumbo v0, "arrivaltime"

    iget-object v1, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

    invoke-virtual {v1}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->getArrivalTimeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/TravelTimeRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 49
    const-string/jumbo v0, "traveltimecount"

    iget-object v1, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

    invoke-virtual {v1}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->getTravelTimeCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/TravelTimeRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 50
    const-string/jumbo v0, "traveltimeinterval"

    iget-object v1, p0, Lcom/inrix/sdk/network/request/TravelTimeRequest;->currentRequestParams:Lcom/inrix/sdk/RouteManager$TravelTimeOptions;

    invoke-virtual {v1}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->getTravelTimeInterval()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/TravelTimeRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 52
    :cond_0
    return-void
.end method


# virtual methods
.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/TravelTimeResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/TravelTimeResponse;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ROUTING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
