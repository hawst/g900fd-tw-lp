.class public abstract Lcom/inrix/sdk/network/request/NetworkRequest;
.super Lcom/android/volley/Request;
.source "NetworkRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/android/volley/Request",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final REQUEST_BACKOFF_MULT:I

.field protected final REQUEST_MAX_RETRIES:I

.field protected final REQUEST_TIMEOUT_MS:I

.field protected callback:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected errorCallback:Lcom/android/volley/Response$ErrorListener;

.field protected methodType:I

.field protected params:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 4
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    .local p1, "callback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    const/16 v3, 0x2710

    const/4 v2, 0x1

    .line 32
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->methodType:I

    .line 27
    iput v3, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->REQUEST_TIMEOUT_MS:I

    .line 28
    iput v2, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->REQUEST_MAX_RETRIES:I

    .line 29
    const/4 v0, 0x2

    iput v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->REQUEST_BACKOFF_MULT:I

    .line 33
    iput-object p1, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->callback:Lcom/android/volley/Response$Listener;

    .line 34
    iput-object p2, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->errorCallback:Lcom/android/volley/Response$ErrorListener;

    .line 36
    new-instance v0, Lcom/android/volley/DefaultRetryPolicy;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v3, v2, v1}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/network/request/NetworkRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected deliverResponse(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    .local p1, "response":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->callback:Lcom/android/volley/Response$Listener;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->callback:Lcom/android/volley/Response$Listener;

    invoke-interface {v0, p1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    .line 97
    :cond_0
    return-void
.end method

.method public getErrorListener()Lcom/android/volley/Response$ErrorListener;
    .locals 1

    .prologue
    .line 146
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->errorCallback:Lcom/android/volley/Response$ErrorListener;

    return-object v0
.end method

.method public getMethodType()I
    .locals 1

    .prologue
    .line 89
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->methodType:I

    return v0
.end method

.method public getParameter(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 136
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public abstract getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected getRequestParams()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    return-object v0
.end method

.method public getTrafficStatsTag()I
    .locals 1

    .prologue
    .line 84
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/NetworkRequest;->getWebServicePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/NetworkRequest;->getWebServicePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 6

    .prologue
    .line 68
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    .line 69
    .local v3, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/NetworkRequest;->getWebServicePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 70
    iget-object v5, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 72
    .local v2, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 73
    .local v1, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 74
    .local v4, "value":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 75
    invoke-virtual {v3, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 79
    .end local v1    # "key":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public abstract getWebServicePath()Ljava/lang/String;
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 4
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/NetworkRequest;->getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;

    move-result-object v1

    .line 49
    .local v1, "parser":Lcom/inrix/sdk/parser/INetworkResponseParser;, "Lcom/inrix/sdk/parser/INetworkResponseParser<TT;>;"
    if-nez v1, :cond_0

    .line 50
    const-string/jumbo v3, "Parser is missing"

    invoke-static {v3}, Lcom/inrix/sdk/InrixDebug;->LogW(Ljava/lang/String;)V

    .line 51
    const/4 v3, 0x0

    .line 60
    :goto_0
    return-object v3

    .line 55
    :cond_0
    :try_start_0
    invoke-interface {v1, p1}, Lcom/inrix/sdk/parser/INetworkResponseParser;->parse(Lcom/android/volley/NetworkResponse;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 60
    .local v2, "result":Ljava/lang/Object;, "TT;"
    invoke-static {p1}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_0

    .line 56
    .end local v2    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Lcom/android/volley/VolleyError;
    invoke-static {v0}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_0
.end method

.method public removeParameter(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 140
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    return-void
.end method

.method public setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "D)",
            "Lcom/inrix/sdk/network/request/NetworkRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    return-object p0
.end method

.method public setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/inrix/sdk/network/request/NetworkRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 107
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-object p0
.end method

.method public setParameter(Ljava/lang/String;J)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lcom/inrix/sdk/network/request/NetworkRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    return-object p0
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/inrix/sdk/network/request/NetworkRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    if-eqz p2, :cond_0

    .line 101
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_0
    return-object p0
.end method

.method public setParameter(Ljava/lang/String;Z)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/inrix/sdk/network/request/NetworkRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 131
    .local p0, "this":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/NetworkRequest;->params:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    return-object p0
.end method
