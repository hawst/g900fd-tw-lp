.class public abstract Lcom/inrix/sdk/network/request/JSONInrixRequest;
.super Lcom/inrix/sdk/network/request/InrixRequest;
.source "JSONInrixRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/inrix/sdk/network/request/InrixRequest",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 0
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 10
    .local p0, "this":Lcom/inrix/sdk/network/request/JSONInrixRequest;, "Lcom/inrix/sdk/network/request/JSONInrixRequest<TT;>;"
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/InrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 11
    return-void
.end method


# virtual methods
.method public abstract getApiName()Ljava/lang/String;
.end method

.method public getShortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    .local p0, "this":Lcom/inrix/sdk/network/request/JSONInrixRequest;, "Lcom/inrix/sdk/network/request/JSONInrixRequest<TT;>;"
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/JSONInrixRequest;->getApiName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebServicePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17
    .local p0, "this":Lcom/inrix/sdk/network/request/JSONInrixRequest;, "Lcom/inrix/sdk/network/request/JSONInrixRequest<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/JSONInrixRequest;->getServerPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/JSONInrixRequest;->getApiName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/inrix/sdk/network/request/NetworkRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/inrix/sdk/network/request/JSONInrixRequest;, "Lcom/inrix/sdk/network/request/JSONInrixRequest<TT;>;"
    const-string/jumbo v0, "token"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    const-string/jumbo p1, "authtoken"

    .line 32
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/inrix/sdk/network/request/InrixRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    move-result-object v0

    return-object v0
.end method
