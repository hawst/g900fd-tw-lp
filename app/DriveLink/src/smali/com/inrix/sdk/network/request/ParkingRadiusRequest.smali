.class public final Lcom/inrix/sdk/network/request/ParkingRadiusRequest;
.super Lcom/inrix/sdk/network/request/ParkingRequestBase;
.source "ParkingRadiusRequest.java"


# static fields
.field private static final API_NAME:Ljava/lang/String; = "GetParkingLotsInRadius"


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 0
    .param p3, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/ParkingLotCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/ParkingLotCollection;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/inrix/sdk/network/request/ParkingRequestBase;-><init>(Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 36
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string/jumbo v0, "GetParkingLotsInRadius"

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->PARKING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
