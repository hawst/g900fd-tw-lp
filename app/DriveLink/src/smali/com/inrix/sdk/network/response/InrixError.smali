.class public Lcom/inrix/sdk/network/response/InrixError;
.super Lcom/android/volley/VolleyError;
.source "InrixError.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private statusId:I

.field private statusText:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "statusId"    # I
    .param p2, "statusText"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/android/volley/VolleyError;-><init>()V

    .line 8
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/inrix/sdk/network/response/InrixError;->statusId:I

    .line 9
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/network/response/InrixError;->statusText:Ljava/lang/String;

    .line 12
    iput p1, p0, Lcom/inrix/sdk/network/response/InrixError;->statusId:I

    .line 13
    iput-object p2, p0, Lcom/inrix/sdk/network/response/InrixError;->statusText:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public getStatusId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/inrix/sdk/network/response/InrixError;->statusId:I

    return v0
.end method

.method public getStatusText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/inrix/sdk/network/response/InrixError;->statusText:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/inrix/sdk/network/response/InrixError;->statusId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/network/response/InrixError;->statusText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
