.class public Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "DeviceAuthRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/DeviceAuth;",
        ">;"
    }
.end annotation


# instance fields
.field private final API_NAME:Ljava/lang/String;

.field private final PARAM_DEVICE_ID:Ljava/lang/String;

.field private final PARAM_HARDWARE_ID:Ljava/lang/String;

.field private final PARAM_VENDOR_ID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/DeviceAuth;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/DeviceAuth;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 13
    const-string/jumbo v0, "vendorid"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->PARAM_VENDOR_ID:Ljava/lang/String;

    .line 14
    const-string/jumbo v0, "hardwareid"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->PARAM_HARDWARE_ID:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, "consumerid"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->PARAM_DEVICE_ID:Ljava/lang/String;

    .line 17
    const-string/jumbo v0, "DeviceAuth"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->API_NAME:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, "hardwareid"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getHardwareId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 24
    const-string/jumbo v0, "vendorid"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 25
    const-string/jumbo v0, "consumerid"

    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 26
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "DeviceAuth"

    return-object v0
.end method

.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/DeviceAuth;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/DeviceAuth;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->AUTHENTICATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method

.method public isTokenNeeded()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public shouldAttachPhs()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method
