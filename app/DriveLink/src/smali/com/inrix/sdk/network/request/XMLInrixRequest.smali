.class public abstract Lcom/inrix/sdk/network/request/XMLInrixRequest;
.super Lcom/inrix/sdk/network/request/InrixRequest;
.source "XMLInrixRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/inrix/sdk/network/request/InrixRequest",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 0
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/inrix/sdk/network/request/XMLInrixRequest;, "Lcom/inrix/sdk/network/request/XMLInrixRequest<TT;>;"
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/InrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 31
    return-void
.end method


# virtual methods
.method public getShortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    .local p0, "this":Lcom/inrix/sdk/network/request/XMLInrixRequest;, "Lcom/inrix/sdk/network/request/XMLInrixRequest<TT;>;"
    const-string/jumbo v0, "action"

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/network/request/XMLInrixRequest;->getParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebServicePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    .local p0, "this":Lcom/inrix/sdk/network/request/XMLInrixRequest;, "Lcom/inrix/sdk/network/request/XMLInrixRequest<TT;>;"
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/XMLInrixRequest;->getServerPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
