.class public final Lcom/inrix/sdk/network/request/RouteFindRequest;
.super Lcom/inrix/sdk/network/request/XMLInrixRequest;
.source "RouteFindRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/XMLInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/RoutesCollection;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION_NAME:Ljava/lang/String; = "Mobile.Route.Find"

.field public static final PARAM_WAYPOINT_FORMAT:Ljava/lang/String; = "wp_%s"


# instance fields
.field private final MAX_ALTERNATES:I

.field private final MAX_WAYPOINTS:I

.field private final REQUEST_BACKOFF_MULT:I

.field private final REQUEST_MAX_RETRIES:I

.field private final REQUEST_TIMEOUT_MS:I


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/utils/UserPreferences$UNIT;Ljava/util/List;ILjava/lang/Integer;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 12
    .param p1, "start"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "end"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p3, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .param p5, "tolerance"    # I
    .param p6, "heading"    # Ljava/lang/Integer;
    .param p7, "maxAlternates"    # I
    .param p8, "outputFields"    # Ljava/lang/String;
    .param p10, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "Lcom/inrix/sdk/utils/UserPreferences$UNIT;",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GeoPoint;",
            ">;I",
            "Ljava/lang/Integer;",
            "I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/RoutesCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    .local p4, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/GeoPoint;>;"
    .local p9, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/RoutesCollection;>;"
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-direct {p0, v0, v1}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 23
    const/16 v8, 0x8

    iput v8, p0, Lcom/inrix/sdk/network/request/RouteFindRequest;->MAX_WAYPOINTS:I

    .line 24
    const/4 v8, 0x2

    iput v8, p0, Lcom/inrix/sdk/network/request/RouteFindRequest;->MAX_ALTERNATES:I

    .line 26
    const/16 v8, 0x7530

    iput v8, p0, Lcom/inrix/sdk/network/request/RouteFindRequest;->REQUEST_TIMEOUT_MS:I

    .line 27
    const/4 v8, 0x0

    iput v8, p0, Lcom/inrix/sdk/network/request/RouteFindRequest;->REQUEST_MAX_RETRIES:I

    .line 28
    const/4 v8, 0x2

    iput v8, p0, Lcom/inrix/sdk/network/request/RouteFindRequest;->REQUEST_BACKOFF_MULT:I

    .line 63
    const-string/jumbo v8, "action"

    const-string/jumbo v9, "Mobile.Route.Find"

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 66
    move-object/from16 v3, p4

    .line 67
    .local v3, "acceptedWaypoints":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/GeoPoint;>;"
    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v8

    const/16 v9, 0x8

    if-le v8, v9, :cond_0

    .line 68
    const/4 v8, 0x0

    const/16 v9, 0x8

    move-object/from16 v0, p4

    invoke-interface {v0, v8, v9}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    .line 71
    :cond_0
    const/4 v5, 0x1

    .line 72
    .local v5, "waypointIndex":I
    const-string/jumbo v8, "wp_%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "waypointIndex":I
    .local v6, "waypointIndex":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 74
    if-eqz v3, :cond_2

    .line 75
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    move v5, v6

    .end local v6    # "waypointIndex":I
    .restart local v5    # "waypointIndex":I
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/inrix/sdk/model/GeoPoint;

    .line 76
    .local v7, "wp":Lcom/inrix/sdk/model/GeoPoint;
    if-eqz v7, :cond_1

    .line 77
    const-string/jumbo v8, "wp_%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "waypointIndex":I
    .restart local v6    # "waypointIndex":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Lcom/inrix/sdk/model/GeoPoint;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    move v5, v6

    .end local v6    # "waypointIndex":I
    .restart local v5    # "waypointIndex":I
    goto :goto_0

    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "waypointIndex":I
    .end local v7    # "wp":Lcom/inrix/sdk/model/GeoPoint;
    .restart local v6    # "waypointIndex":I
    :cond_2
    move v5, v6

    .line 82
    .end local v6    # "waypointIndex":I
    .restart local v5    # "waypointIndex":I
    :cond_3
    const-string/jumbo v8, "wp_%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "waypointIndex":I
    .restart local v6    # "waypointIndex":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/inrix/sdk/model/GeoPoint;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 85
    const/4 v8, 0x0

    const/4 v9, 0x2

    move/from16 v0, p7

    invoke-direct {p0, v0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->clamp(III)I

    move-result v2

    .line 88
    .local v2, "acceptedMaxAlternates":I
    const-string/jumbo v8, "maxalternates"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 90
    const-string/jumbo v8, "routeoutputfields"

    move-object/from16 v0, p8

    invoke-virtual {p0, v8, v0}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 91
    const-string/jumbo v8, "geometrytolerance"

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 92
    if-eqz p6, :cond_4

    .line 93
    const-string/jumbo v8, "wp_heading"

    invoke-virtual/range {p6 .. p6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 95
    :cond_4
    const-string/jumbo v8, "usetraffic"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 96
    const-string/jumbo v8, "units"

    invoke-virtual {p3}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->ordinal()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 100
    new-instance v8, Lcom/android/volley/DefaultRetryPolicy;

    const/16 v9, 0x7530

    const/4 v10, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    invoke-direct {v8, v9, v10, v11}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    invoke-virtual {p0, v8}, Lcom/inrix/sdk/network/request/RouteFindRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)V

    .line 103
    return-void
.end method

.method private final clamp(III)I
    .locals 0
    .param p1, "x"    # I
    .param p2, "low"    # I
    .param p3, "high"    # I

    .prologue
    .line 136
    if-ge p1, p2, :cond_0

    .end local p2    # "low":I
    :goto_0
    return p2

    .restart local p2    # "low":I
    :cond_0
    if-le p1, p3, :cond_1

    move p2, p3

    goto :goto_0

    :cond_1
    move p2, p1

    goto :goto_0
.end method


# virtual methods
.method public final getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/RoutesCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    new-instance v0, Lcom/inrix/sdk/network/request/RouteFindRequest$1;

    const-class v1, Lcom/inrix/sdk/model/RoutesCollection;

    invoke-direct {v0, p0, v1}, Lcom/inrix/sdk/network/request/RouteFindRequest$1;-><init>(Lcom/inrix/sdk/network/request/RouteFindRequest;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ROUTING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
