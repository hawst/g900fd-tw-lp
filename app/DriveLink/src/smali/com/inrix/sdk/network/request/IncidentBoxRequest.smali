.class public Lcom/inrix/sdk/network/request/IncidentBoxRequest;
.super Lcom/inrix/sdk/network/request/IncidentRequest;
.source "IncidentBoxRequest.java"


# static fields
.field private static final ACTION_INCIDENTS_BOX:Ljava/lang/String; = "Mobile.Incidents.Box"


# instance fields
.field private location:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "corner1"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "corner2"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p4, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/IncidentCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    .local p3, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/IncidentCollection;>;"
    invoke-direct {p0, p3, p4}, Lcom/inrix/sdk/network/request/IncidentRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 39
    iput-object p2, p0, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->location:Lcom/inrix/sdk/model/GeoPoint;

    .line 41
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.Incidents.Box"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 42
    const-string/jumbo v0, "incidenttype"

    const-string/jumbo v1, "all"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 44
    const-string/jumbo v0, "incidentoutputfields"

    const-string/jumbo v1, "all"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 46
    const-string/jumbo v0, "incidentsource"

    const-string/jumbo v1, "All"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 48
    const-string/jumbo v0, "corner1"

    invoke-virtual {p0, v0, p1}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 49
    const-string/jumbo v0, "corner2"

    invoke-virtual {p0, v0, p2}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "corner1"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "corner2"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p3, "incidentsType"    # Ljava/lang/String;
    .param p4, "incidentsSource"    # Ljava/lang/String;
    .param p5, "incidentOutputFields"    # Ljava/lang/String;
    .param p6, "severity"    # Ljava/lang/String;
    .param p8, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/IncidentCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p7, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/IncidentCollection;>;"
    invoke-direct {p0, p7, p8}, Lcom/inrix/sdk/network/request/IncidentRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 78
    iput-object p2, p0, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->location:Lcom/inrix/sdk/model/GeoPoint;

    .line 80
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.Incidents.Box"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 81
    const-string/jumbo v0, "incidenttype"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 82
    const-string/jumbo v0, "incidentoutputfields"

    invoke-virtual {p0, v0, p5}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 83
    const-string/jumbo v0, "incidentsource"

    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 84
    const-string/jumbo v0, "corner1"

    invoke-virtual {p0, v0, p1}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 85
    const-string/jumbo v0, "corner2"

    invoke-virtual {p0, v0, p2}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 86
    const-string/jumbo v0, "severity"

    invoke-virtual {p0, v0, p6}, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 87
    return-void
.end method


# virtual methods
.method public getLocation()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/inrix/sdk/network/request/IncidentBoxRequest;->location:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public bridge synthetic getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 1

    .prologue
    .line 17
    invoke-super {p0}, Lcom/inrix/sdk/network/request/IncidentRequest;->getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 17
    invoke-super {p0}, Lcom/inrix/sdk/network/request/IncidentRequest;->getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 17
    invoke-super {p0, p1, p2}, Lcom/inrix/sdk/network/request/IncidentRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    move-result-object v0

    return-object v0
.end method
