.class public Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "GasStationSingleGasStationRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/SingleGasStationResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION_GAS_STATIONS_RADIUS:Ljava/lang/String; = "GetGasStationInformation"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "gasStationID"    # Ljava/lang/String;
    .param p2, "outputFields"    # Ljava/lang/String;
    .param p3, "productTypes"    # Ljava/lang/String;
    .param p5, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/SingleGasStationResult;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p4, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/SingleGasStationResult;>;"
    invoke-direct {p0, p4, p5}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 30
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "GetGasStationInformation"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 31
    const-string/jumbo v0, "gasStationId"

    invoke-virtual {p0, v0, p1}, Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 32
    const-string/jumbo v0, "outputFields"

    invoke-virtual {p0, v0, p2}, Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 33
    const-string/jumbo v0, "productType"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/GasStationSingleGasStationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 34
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string/jumbo v0, "GetGasStationInformation"

    return-object v0
.end method

.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/SingleGasStationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/SingleGasStationResult;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->FUEL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
