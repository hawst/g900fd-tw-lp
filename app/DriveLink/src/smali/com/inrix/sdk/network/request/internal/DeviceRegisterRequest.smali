.class public Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;
.super Lcom/inrix/sdk/network/request/XMLInrixRequest;
.source "DeviceRegisterRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/XMLInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/DeviceRegister;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 7
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/DeviceRegister;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 15
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/DeviceRegister;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 17
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.Device.Register"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 19
    const-string/jumbo v0, "systemversion"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getSystemVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 21
    const-string/jumbo v0, "devicemodel"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getDeviceModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 22
    const-string/jumbo v0, "hardwareid"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getHardwareId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 23
    const-string/jumbo v0, "vendorid"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 24
    const-string/jumbo v6, "token"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getHardwareId()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getAppVersion()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorId()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getDeviceModel()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getSystemVersion()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorToken()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/inrix/sdk/auth/AuthHelper;->getTokenForDeviceRegister(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 31
    return-void
.end method


# virtual methods
.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/DeviceRegister;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/DeviceRegister;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->REGISTER:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method

.method public isTokenNeeded()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public shouldAttachPhs()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method
