.class public final Lcom/inrix/sdk/network/request/RequestParams$IncidentResultTypeName;
.super Ljava/lang/Object;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/network/request/RequestParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "IncidentResultTypeName"
.end annotation


# static fields
.field public static final ALL:Ljava/lang/String; = "all"

.field public static final CONSTRUCTION:Ljava/lang/String; = "construction"

.field public static final EVENTS:Ljava/lang/String; = "events"

.field public static final FLOW:Ljava/lang/String; = "flow"

.field public static final INCIDENTS:Ljava/lang/String; = "incidents"

.field public static final POLICE:Ljava/lang/String; = "police"

.field public static final WEATHER:Ljava/lang/String; = "weather"


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/network/request/RequestParams;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/network/request/RequestParams;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/inrix/sdk/network/request/RequestParams$IncidentResultTypeName;->this$0:Lcom/inrix/sdk/network/request/RequestParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
