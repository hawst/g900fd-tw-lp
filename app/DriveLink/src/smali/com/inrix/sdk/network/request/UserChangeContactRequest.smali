.class public Lcom/inrix/sdk/network/request/UserChangeContactRequest;
.super Lcom/inrix/sdk/network/request/XMLInrixRequest;
.source "UserChangeContactRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/XMLInrixRequest",
        "<",
        "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION:Ljava/lang/String; = "Mobile.UserContact.Set"


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .param p3, "emailAddrees"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/parser/xml/XMLEntityBase;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 46
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.UserContact.Set"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 47
    const-string/jumbo v0, "addresstype"

    const-string/jumbo v1, "email"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 48
    const-string/jumbo v0, "address"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 49
    const-string/jumbo v0, "enabled"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Z)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .param p3, "addressType"    # Ljava/lang/String;
    .param p4, "address"    # Ljava/lang/String;
    .param p5, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/parser/xml/XMLEntityBase;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 29
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.UserContact.Set"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 30
    const-string/jumbo v0, "addresstype"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 31
    const-string/jumbo v0, "address"

    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 32
    const-string/jumbo v0, "enabled"

    invoke-virtual {p0, v0, p5}, Lcom/inrix/sdk/network/request/UserChangeContactRequest;->setParameter(Ljava/lang/String;Z)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 33
    return-void
.end method


# virtual methods
.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;

    const-class v1, Lcom/inrix/sdk/parser/xml/XMLEntityBase;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->MOBILE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
