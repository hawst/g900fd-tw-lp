.class public abstract Lcom/inrix/sdk/network/request/InrixRequest;
.super Lcom/inrix/sdk/network/request/NetworkRequest;
.source "InrixRequest.java"

# interfaces
.implements Lcom/inrix/sdk/ICancellable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;,
        Lcom/inrix/sdk/network/request/InrixRequest$ServerType;,
        Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/inrix/sdk/network/request/NetworkRequest",
        "<TT;>;",
        "Lcom/inrix/sdk/ICancellable;"
    }
.end annotation


# instance fields
.field private final TOKEN_EXPIRED_STATUS_ID:I

.field private serverPath:Ljava/lang/String;

.field private tokenExpiredListener:Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/NetworkRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 23
    const/16 v0, 0x2b

    iput v0, p0, Lcom/inrix/sdk/network/request/InrixRequest;->TOKEN_EXPIRED_STATUS_ID:I

    .line 56
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/network/request/InrixRequest;->serverPath:Ljava/lang/String;

    .line 61
    const-string/jumbo v0, "appversion"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getAppVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/InrixRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 62
    const-string/jumbo v0, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/InrixRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 63
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 212
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    invoke-super {p0}, Lcom/inrix/sdk/network/request/NetworkRequest;->cancel()V

    .line 213
    return-void
.end method

.method public getServerPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/network/request/InrixRequest;->serverPath:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
.end method

.method public abstract getShortName()Ljava/lang/String;
.end method

.method public isBaseURLNeeded()Z
    .locals 1

    .prologue
    .line 151
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public isTokenNeeded()Z
    .locals 1

    .prologue
    .line 142
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method protected final logRequest(Lcom/android/volley/NetworkResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;

    .prologue
    .line 189
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    if-nez p1, :cond_0

    .line 203
    :goto_0
    return-void

    .line 193
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 194
    .local v0, "statusBuilder":Ljava/lang/StringBuilder;
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 195
    iget v1, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 197
    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/InrixRequest;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/InrixRequest;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected parseNetworkError(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;
    .locals 5
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 68
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/InrixRequest;->getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;

    move-result-object v1

    .line 70
    .local v1, "parser":Lcom/inrix/sdk/parser/INetworkResponseParser;, "Lcom/inrix/sdk/parser/INetworkResponseParser<TT;>;"
    if-nez v1, :cond_0

    .line 71
    const-string/jumbo v3, "Parser is missing"

    invoke-static {v3}, Lcom/inrix/sdk/InrixDebug;->LogW(Ljava/lang/String;)V

    .line 72
    const/4 v3, 0x0

    .line 102
    :goto_0
    return-object v3

    .line 78
    :cond_0
    instance-of v3, p1, Lcom/inrix/sdk/network/response/InrixError;

    if-nez v3, :cond_1

    iget-object v3, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v3, :cond_1

    .line 81
    :try_start_0
    iget-object v3, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    invoke-interface {v1, v3}, Lcom/inrix/sdk/parser/INetworkResponseParser;->parse(Lcom/android/volley/NetworkResponse;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/android/volley/ParseError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_1

    .line 92
    :cond_1
    :goto_1
    instance-of v3, p1, Lcom/inrix/sdk/network/response/InrixError;

    if-eqz v3, :cond_2

    move-object v3, p1

    check-cast v3, Lcom/inrix/sdk/network/response/InrixError;

    invoke-virtual {v3}, Lcom/inrix/sdk/network/response/InrixError;->getStatusId()I

    move-result v3

    const/16 v4, 0x2b

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/inrix/sdk/network/request/InrixRequest;->tokenExpiredListener:Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;

    if-eqz v3, :cond_2

    .line 95
    iget-object v3, p0, Lcom/inrix/sdk/network/request/InrixRequest;->tokenExpiredListener:Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;

    invoke-interface {v3, p0}, Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;->onTokenExpiredError(Lcom/inrix/sdk/network/request/InrixRequest;)V

    .line 98
    :cond_2
    iget-object v3, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v3, :cond_3

    .line 99
    iget-object v3, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    invoke-virtual {p0, v3}, Lcom/inrix/sdk/network/request/InrixRequest;->logRequest(Lcom/android/volley/NetworkResponse;)V

    :cond_3
    move-object v3, p1

    .line 102
    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Lcom/android/volley/ParseError;
    new-instance v2, Lcom/android/volley/ServerError;

    iget-object v3, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    invoke-direct {v2, v3}, Lcom/android/volley/ServerError;-><init>(Lcom/android/volley/NetworkResponse;)V

    .line 86
    .end local p1    # "volleyError":Lcom/android/volley/VolleyError;
    .local v2, "volleyError":Lcom/android/volley/VolleyError;
    invoke-virtual {v2, v0}, Lcom/android/volley/VolleyError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-object p1, v2

    .line 89
    .end local v2    # "volleyError":Lcom/android/volley/VolleyError;
    .restart local p1    # "volleyError":Lcom/android/volley/VolleyError;
    goto :goto_1

    .line 87
    .end local v0    # "e":Lcom/android/volley/ParseError;
    :catch_1
    move-exception v0

    .line 88
    .local v0, "e":Lcom/android/volley/VolleyError;
    move-object p1, v0

    goto :goto_1
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 2
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/network/request/InrixRequest;->logRequest(Lcom/android/volley/NetworkResponse;)V

    .line 115
    invoke-super {p0, p1}, Lcom/inrix/sdk/network/request/NetworkRequest;->parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;

    move-result-object v0

    .line 117
    .local v0, "result":Lcom/android/volley/Response;, "Lcom/android/volley/Response<TT;>;"
    iget-object v1, v0, Lcom/android/volley/Response;->error:Lcom/android/volley/VolleyError;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/volley/Response;->error:Lcom/android/volley/VolleyError;

    instance-of v1, v1, Lcom/inrix/sdk/network/response/InrixError;

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, v0, Lcom/android/volley/Response;->error:Lcom/android/volley/VolleyError;

    invoke-virtual {p0, v1}, Lcom/inrix/sdk/network/request/InrixRequest;->parseNetworkError(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;

    move-result-object v1

    invoke-static {v1}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v0

    .line 122
    :cond_0
    return-object v0
.end method

.method public setOnTokenExpiredListener(Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;

    .prologue
    .line 171
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    iput-object p1, p0, Lcom/inrix/sdk/network/request/InrixRequest;->tokenExpiredListener:Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;

    .line 172
    return-void
.end method

.method public setServerPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 130
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    iput-object p1, p0, Lcom/inrix/sdk/network/request/InrixRequest;->serverPath:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public shouldAttachPhs()Z
    .locals 1

    .prologue
    .line 161
    .local p0, "this":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<TT;>;"
    const/4 v0, 0x1

    return v0
.end method
