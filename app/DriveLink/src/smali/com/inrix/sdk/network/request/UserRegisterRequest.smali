.class public Lcom/inrix/sdk/network/request/UserRegisterRequest;
.super Lcom/inrix/sdk/network/request/XMLInrixRequest;
.source "UserRegisterRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/XMLInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/DeviceRegister;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .param p3, "email"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/DeviceRegister;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/DeviceRegister;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 38
    const-string/jumbo v1, "action"

    const-string/jumbo v2, "Mobile.User.Register"

    invoke-virtual {p0, v1, v2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "locale":Ljava/lang/String;
    const-string/jumbo v1, "email"

    invoke-virtual {p0, v1, p3}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 43
    const-string/jumbo v1, "password"

    invoke-virtual {p0, v1, p4}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 44
    const-string/jumbo v1, "locale"

    invoke-virtual {p0, v1, v0}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 45
    const-string/jumbo v1, "hardwareid"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getHardwareId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 46
    const-string/jumbo v1, "vendorid"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 47
    const-string/jumbo v1, "devicemodel"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getDeviceModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 48
    const-string/jumbo v1, "devicename"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 49
    const-string/jumbo v1, "systemversion"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getSystemVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 50
    const-string/jumbo v1, "token"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getHardwareId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0, p3, p4}, Lcom/inrix/sdk/auth/AuthHelper;->getTokenForDeviceRegisterEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 53
    return-void
.end method


# virtual methods
.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/DeviceRegister;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/DeviceRegister;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->REGISTER:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method

.method public isTokenNeeded()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method
