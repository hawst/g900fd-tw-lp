.class public Lcom/inrix/sdk/network/request/GasStationsBoxRequest;
.super Lcom/inrix/sdk/network/request/GasStationRequest;
.source "GasStationsBoxRequest.java"


# static fields
.field private static final ACTION_GAS_STATIONS_BOX:Ljava/lang/String; = "GetGasStationsInBox"


# direct methods
.method public constructor <init>(DDDDLjava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "startLatitude"    # D
    .param p3, "startLongitude"    # D
    .param p5, "endLatitude"    # D
    .param p7, "endLongitude"    # D
    .param p9, "outputFields"    # Ljava/lang/String;
    .param p10, "productTypes"    # Ljava/lang/String;
    .param p12, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDDD",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/GasStationCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    .local p11, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/GasStationCollection;>;"
    invoke-direct {p0, p11, p12, p9, p10}, Lcom/inrix/sdk/network/request/GasStationRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "GetGasStationsInBox"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/GasStationsBoxRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 33
    const-string/jumbo v0, "startlatitude"

    invoke-virtual {p0, v0, p1, p2}, Lcom/inrix/sdk/network/request/GasStationsBoxRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 34
    const-string/jumbo v0, "startlongitude"

    invoke-virtual {p0, v0, p3, p4}, Lcom/inrix/sdk/network/request/GasStationsBoxRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 35
    const-string/jumbo v0, "endlatitude"

    invoke-virtual {p0, v0, p5, p6}, Lcom/inrix/sdk/network/request/GasStationsBoxRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 36
    const-string/jumbo v0, "endlongitude"

    invoke-virtual {p0, v0, p7, p8}, Lcom/inrix/sdk/network/request/GasStationsBoxRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 37
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string/jumbo v0, "GetGasStationsInBox"

    return-object v0
.end method
