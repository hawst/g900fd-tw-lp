.class public final enum Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;
.super Ljava/lang/Enum;
.source "InrixRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/network/request/InrixRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerRegion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

.field public static final enum CN:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

.field public static final enum EU:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

.field public static final enum INVALID_REGION:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

.field public static final enum KR:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

.field public static final enum NA:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    const-string/jumbo v1, "INVALID_REGION"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->INVALID_REGION:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    .line 50
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    const-string/jumbo v1, "EU"

    invoke-direct {v0, v1, v3}, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->EU:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    .line 51
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    const-string/jumbo v1, "NA"

    invoke-direct {v0, v1, v4}, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->NA:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    .line 52
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    const-string/jumbo v1, "KR"

    invoke-direct {v0, v1, v5}, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->KR:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    .line 53
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    const-string/jumbo v1, "CN"

    invoke-direct {v0, v1, v6}, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->CN:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->INVALID_REGION:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->EU:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    aput-object v1, v0, v3

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->NA:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->KR:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->CN:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    aput-object v1, v0, v6

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->$VALUES:[Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->$VALUES:[Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    invoke-virtual {v0}, [Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    return-object v0
.end method
