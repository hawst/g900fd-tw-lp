.class public final enum Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
.super Ljava/lang/Enum;
.source "InrixRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/network/request/InrixRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/network/request/InrixRequest$ServerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum ANALYTICS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum AUTHENTICATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum BASE_URL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum CAMERAS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum COMPOSITE_TILES:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum DUST:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum FUEL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum INCIDENTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum MOBILE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum PARKING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum REGISTER:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum ROUTING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum SETTINGS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum TRAFFIC_STATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

.field public static final enum TTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "BASE_URL"

    invoke-direct {v0, v1, v3}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->BASE_URL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 31
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "REGISTER"

    invoke-direct {v0, v1, v4}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->REGISTER:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 32
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "AUTHENTICATE"

    invoke-direct {v0, v1, v5}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->AUTHENTICATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 33
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "MOBILE"

    invoke-direct {v0, v1, v6}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->MOBILE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 34
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "TTS"

    invoke-direct {v0, v1, v7}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 35
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "ANALYTICS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ANALYTICS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 36
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "CAMERAS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->CAMERAS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 37
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "COMPOSITE_TILES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->COMPOSITE_TILES:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 38
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "DUST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->DUST:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 39
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "FUEL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->FUEL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 40
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "INCIDENTS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->INCIDENTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 41
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "PARKING"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->PARKING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 42
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "ROUTING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ROUTING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 43
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "SETTINGS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->SETTINGS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 44
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "TRAFFIC_STATE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAFFIC_STATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 45
    new-instance v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v1, "TRAVEL_DATA"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .line 29
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->BASE_URL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->REGISTER:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->AUTHENTICATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->MOBILE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ANALYTICS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->CAMERAS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->COMPOSITE_TILES:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->DUST:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->FUEL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->INCIDENTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->PARKING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ROUTING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->SETTINGS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAFFIC_STATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->$VALUES:[Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->$VALUES:[Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-virtual {v0}, [Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
