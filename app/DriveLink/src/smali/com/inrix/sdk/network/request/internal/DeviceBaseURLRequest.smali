.class public Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "DeviceBaseURLRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/BaseURL;",
        ">;"
    }
.end annotation


# instance fields
.field private final API_NAME:Ljava/lang/String;

.field private final FORMAT_JSON:Ljava/lang/String;

.field private final PARAM_ACTION:Ljava/lang/String;

.field private final PARAM_LAT_LONG:Ljava/lang/String;

.field private final PARAM_OUTPUT_FORMAT:Ljava/lang/String;

.field private final PARAM_VENDOR_ID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Landroid/location/Location;)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .param p3, "location"    # Landroid/location/Location;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/BaseURL;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Landroid/location/Location;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/BaseURL;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 21
    const-string/jumbo v0, "mobile.baseurl.get"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->API_NAME:Ljava/lang/String;

    .line 22
    const-string/jumbo v0, "json"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->FORMAT_JSON:Ljava/lang/String;

    .line 23
    const-string/jumbo v0, "vendorid"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->PARAM_VENDOR_ID:Ljava/lang/String;

    .line 24
    const-string/jumbo v0, "format"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->PARAM_OUTPUT_FORMAT:Ljava/lang/String;

    .line 25
    const-string/jumbo v0, "latlong"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->PARAM_LAT_LONG:Ljava/lang/String;

    .line 26
    const-string/jumbo v0, "action"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->PARAM_ACTION:Ljava/lang/String;

    .line 41
    if-nez p3, :cond_0

    .line 42
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 47
    :cond_0
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "mobile.baseurl.get"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 48
    const-string/jumbo v0, "vendorid"

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 49
    const-string/jumbo v0, "format"

    const-string/jumbo v1, "json"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 50
    const-string/jumbo v0, "latlong"

    invoke-static {p3}, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->formatLatLong(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 51
    return-void
.end method

.method private static formatLatLong(Landroid/location/Location;)Ljava/lang/String;
    .locals 3
    .param p0, "location"    # Landroid/location/Location;

    .prologue
    .line 90
    if-eqz p0, :cond_0

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/BaseURL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/BaseURL;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->BASE_URL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method

.method public isBaseURLNeeded()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public isTokenNeeded()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method
