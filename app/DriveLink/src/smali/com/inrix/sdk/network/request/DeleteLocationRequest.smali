.class public Lcom/inrix/sdk/network/request/DeleteLocationRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "DeleteLocationRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/SingleLocationCollection;",
        ">;"
    }
.end annotation


# instance fields
.field private final API_NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 1
    .param p1, "locationId"    # J
    .param p4, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/SingleLocationCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 15
    .local p3, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/SingleLocationCollection;>;"
    invoke-direct {p0, p3, p4}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 11
    const-string/jumbo v0, "DeleteLocation"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/DeleteLocationRequest;->API_NAME:Ljava/lang/String;

    .line 16
    const-string/jumbo v0, "locationid"

    invoke-virtual {p0, v0, p1, p2}, Lcom/inrix/sdk/network/request/DeleteLocationRequest;->setParameter(Ljava/lang/String;J)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 17
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    const-string/jumbo v0, "DeleteLocation"

    return-object v0
.end method

.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/SingleLocationCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/SingleLocationCollection;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
