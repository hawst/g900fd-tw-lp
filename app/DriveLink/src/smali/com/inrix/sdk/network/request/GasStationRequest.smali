.class public abstract Lcom/inrix/sdk/network/request/GasStationRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "GasStationRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/GasStationCollection;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .param p3, "outputFields"    # Ljava/lang/String;
    .param p4, "productTypes"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/GasStationCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 16
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/GasStationCollection;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 17
    const-string/jumbo v0, "outputFields"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/GasStationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 18
    const-string/jumbo v0, "productType"

    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/network/request/GasStationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 20
    return-void
.end method


# virtual methods
.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/GasStationCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/GasStationCollection;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->FUEL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
