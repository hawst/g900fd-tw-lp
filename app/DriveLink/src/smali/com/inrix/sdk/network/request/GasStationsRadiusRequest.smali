.class public Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;
.super Lcom/inrix/sdk/network/request/GasStationRequest;
.source "GasStationsRadiusRequest.java"


# static fields
.field private static final ACTION_GAS_STATIONS_RADIUS:Ljava/lang/String; = "GetGasStationsInRadius"


# direct methods
.method public constructor <init>(DDDLcom/inrix/sdk/utils/UserPreferences$UNIT;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "centerLatitude"    # D
    .param p3, "centerLongitude"    # D
    .param p5, "radius"    # D
    .param p7, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .param p8, "outputFields"    # Ljava/lang/String;
    .param p9, "productTypes"    # Ljava/lang/String;
    .param p11, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDD",
            "Lcom/inrix/sdk/utils/UserPreferences$UNIT;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/GasStationCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    .local p10, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/GasStationCollection;>;"
    invoke-direct {p0, p10, p11, p8, p9}, Lcom/inrix/sdk/network/request/GasStationRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "GetGasStationsInRadius"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 40
    const-string/jumbo v0, "latitude"

    invoke-virtual {p0, v0, p1, p2}, Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 41
    const-string/jumbo v0, "longitude"

    invoke-virtual {p0, v0, p3, p4}, Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 42
    const-string/jumbo v0, "radius"

    invoke-virtual {p0, v0, p5, p6}, Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 43
    if-nez p7, :cond_0

    .line 44
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 46
    :cond_0
    const-string/jumbo v0, "units"

    invoke-virtual {p7}, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->ordinal()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/GasStationsRadiusRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 47
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string/jumbo v0, "GetGasStationsInRadius"

    return-object v0
.end method
