.class public Lcom/inrix/sdk/network/request/IncidentRadiusRequest;
.super Lcom/inrix/sdk/network/request/IncidentRequest;
.source "IncidentRadiusRequest.java"


# static fields
.field private static final ACTION_INCIDENTS_RADIUS:Ljava/lang/String; = "Mobile.Incidents.Radius"


# instance fields
.field private center:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;DLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "radius"    # D
    .param p5, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "D",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/IncidentCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    .local p4, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/IncidentCollection;>;"
    invoke-direct {p0, p4, p5}, Lcom/inrix/sdk/network/request/IncidentRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 39
    iput-object p1, p0, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 41
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.Incidents.Radius"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 42
    const-string/jumbo v0, "center"

    invoke-virtual {p0, v0, p1}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 43
    const-string/jumbo v0, "radius"

    invoke-virtual {p0, v0, p2, p3}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 44
    const-string/jumbo v0, "incidenttype"

    const-string/jumbo v1, "all"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 46
    const-string/jumbo v0, "incidentoutputfields"

    const-string/jumbo v1, "all"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 48
    const-string/jumbo v0, "incidentsource"

    const-string/jumbo v1, "All"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "radius"    # D
    .param p4, "incidentsType"    # Ljava/lang/String;
    .param p5, "incidentsSource"    # Ljava/lang/String;
    .param p6, "incidentOutputFields"    # Ljava/lang/String;
    .param p7, "units"    # I
    .param p8, "severity"    # Ljava/lang/String;
    .param p10, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "D",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/IncidentCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    .local p9, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/IncidentCollection;>;"
    invoke-direct {p0, p9, p10}, Lcom/inrix/sdk/network/request/IncidentRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 81
    iput-object p1, p0, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 83
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.Incidents.Radius"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 84
    const-string/jumbo v0, "center"

    invoke-virtual {p0, v0, p1}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 85
    const-string/jumbo v0, "radius"

    invoke-virtual {p0, v0, p2, p3}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 86
    const-string/jumbo v0, "incidenttype"

    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 87
    const-string/jumbo v0, "incidentoutputfields"

    invoke-virtual {p0, v0, p6}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 88
    const-string/jumbo v0, "incidentsource"

    invoke-virtual {p0, v0, p5}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 89
    const-string/jumbo v0, "units"

    invoke-virtual {p0, v0, p7}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 90
    const-string/jumbo v0, "severity"

    invoke-virtual {p0, v0, p8}, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 91
    return-void
.end method


# virtual methods
.method public getLocation()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/inrix/sdk/network/request/IncidentRadiusRequest;->center:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public bridge synthetic getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/inrix/sdk/network/request/IncidentRequest;->getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/inrix/sdk/network/request/IncidentRequest;->getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 16
    invoke-super {p0, p1, p2}, Lcom/inrix/sdk/network/request/IncidentRequest;->setParameter(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/network/request/NetworkRequest;

    move-result-object v0

    return-object v0
.end method
