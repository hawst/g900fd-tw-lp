.class public Lcom/inrix/sdk/network/request/UpdateLocationRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "UpdateLocationRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/SingleLocationCollection;",
        ">;"
    }
.end annotation


# instance fields
.field private final API_NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "locationId"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "address"    # Ljava/lang/String;
    .param p5, "customData"    # Ljava/lang/String;
    .param p6, "locationType"    # Ljava/lang/Integer;
    .param p7, "order"    # Ljava/lang/Integer;
    .param p9, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/SingleLocationCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 17
    .local p8, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/SingleLocationCollection;>;"
    invoke-direct {p0, p8, p9}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 11
    const-string/jumbo v0, "UpdateLocation"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/UpdateLocationRequest;->API_NAME:Ljava/lang/String;

    .line 19
    const-string/jumbo v0, "locationid"

    invoke-virtual {p0, v0, p1, p2}, Lcom/inrix/sdk/network/request/UpdateLocationRequest;->setParameter(Ljava/lang/String;J)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 21
    if-eqz p3, :cond_0

    .line 22
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/UpdateLocationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 24
    :cond_0
    if-eqz p4, :cond_1

    .line 25
    const-string/jumbo v0, "address"

    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/network/request/UpdateLocationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 27
    :cond_1
    if-eqz p5, :cond_2

    .line 28
    const-string/jumbo v0, "customdata"

    invoke-virtual {p0, v0, p5}, Lcom/inrix/sdk/network/request/UpdateLocationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 31
    :cond_2
    if-eqz p7, :cond_3

    .line 32
    const-string/jumbo v0, "ordering"

    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/UpdateLocationRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 35
    :cond_3
    if-eqz p6, :cond_4

    .line 36
    const-string/jumbo v0, "locationtype"

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/UpdateLocationRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 38
    :cond_4
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string/jumbo v0, "UpdateLocation"

    return-object v0
.end method

.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/SingleLocationCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/SingleLocationCollection;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
