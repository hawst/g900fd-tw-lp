.class public Lcom/inrix/sdk/network/request/RequestExecutor;
.super Ljava/lang/Object;
.source "RequestExecutor.java"

# interfaces
.implements Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
.implements Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;
.implements Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;


# static fields
.field private static instance:Lcom/inrix/sdk/network/request/RequestExecutor;


# instance fields
.field private authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

.field private httpStack:Lcom/android/volley/toolbox/HttpStack;

.field private isInitialised:Z

.field private queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/inrix/sdk/network/request/InrixRequest",
            "<*>;>;"
        }
    .end annotation
.end field

.field private requestQueue:Lcom/android/volley/RequestQueue;

.field private urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    .line 39
    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    .line 42
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->isInitialised:Z

    .line 56
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lcom/inrix/sdk/network/request/RequestExecutor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/inrix/sdk/network/request/RequestExecutor;->instance:Lcom/inrix/sdk/network/request/RequestExecutor;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/inrix/sdk/network/request/RequestExecutor;

    invoke-direct {v0}, Lcom/inrix/sdk/network/request/RequestExecutor;-><init>()V

    sput-object v0, Lcom/inrix/sdk/network/request/RequestExecutor;->instance:Lcom/inrix/sdk/network/request/RequestExecutor;

    .line 68
    :cond_0
    sget-object v0, Lcom/inrix/sdk/network/request/RequestExecutor;->instance:Lcom/inrix/sdk/network/request/RequestExecutor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private parseCachedBaseUrls()Lcom/inrix/sdk/model/BaseURL;
    .locals 8

    .prologue
    .line 102
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getRegion()Ljava/lang/String;

    move-result-object v5

    .line 103
    .local v5, "region":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getRegistrationServer()Ljava/lang/String;

    move-result-object v6

    .line 104
    .local v6, "registrationUrl":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getAuthenticationServer()Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "authUrl":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 108
    :cond_0
    const/4 v1, 0x0

    .line 131
    :goto_0
    return-object v1

    .line 112
    :cond_1
    new-instance v4, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    invoke-direct {v4}, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;-><init>()V

    .line 113
    .local v4, "entity":Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;
    iput-object v5, v4, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverRegion:Ljava/lang/String;

    .line 115
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v3, "endPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;>;"
    new-instance v2, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;

    invoke-direct {v2}, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;-><init>()V

    .line 118
    .local v2, "endPoint":Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;
    const-string/jumbo v7, "Registration"

    iput-object v7, v2, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointType:Ljava/lang/String;

    .line 119
    iput-object v6, v2, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointURL:Ljava/lang/String;

    .line 120
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    new-instance v2, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;

    .end local v2    # "endPoint":Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;
    invoke-direct {v2}, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;-><init>()V

    .line 123
    .restart local v2    # "endPoint":Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;
    const-string/jumbo v7, "Auth"

    iput-object v7, v2, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointType:Ljava/lang/String;

    .line 124
    iput-object v0, v2, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointURL:Ljava/lang/String;

    .line 125
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iput-object v3, v4, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverEndPoints:Ljava/util/List;

    .line 129
    new-instance v1, Lcom/inrix/sdk/model/BaseURL;

    invoke-direct {v1}, Lcom/inrix/sdk/model/BaseURL;-><init>()V

    .line 130
    .local v1, "baseUrl":Lcom/inrix/sdk/model/BaseURL;
    invoke-virtual {v1, v4}, Lcom/inrix/sdk/model/BaseURL;->setUrlInfo(Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;)V

    goto :goto_0
.end method

.method private declared-synchronized processInrixRequest(Lcom/inrix/sdk/network/request/InrixRequest;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/network/request/InrixRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "request":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<*>;"
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->isCanceled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 242
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 194
    :cond_1
    :try_start_1
    const-string/jumbo v2, ""

    .line 195
    .local v2, "token":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "process Inrix request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->getShortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/RequestExecutor;->haveBaseUrls()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->isBaseURLNeeded()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 199
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Base URL is not ready - queing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->getShortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/RequestExecutor;->getBaseURLs()V

    .line 202
    iget-object v3, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 191
    .end local v2    # "token":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 207
    .restart local v2    # "token":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->isTokenNeeded()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 208
    iget-object v3, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {v3}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getAuthToken()Ljava/lang/String;

    move-result-object v2

    .line 209
    if-nez v2, :cond_3

    .line 211
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Token is not ready - queing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->getShortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 213
    iget-object v3, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 217
    :cond_3
    iget-object v3, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getServerPath(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "serverPath":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 221
    invoke-virtual {p1, v1}, Lcom/inrix/sdk/network/request/InrixRequest;->setServerPath(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->isTokenNeeded()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 230
    const-string/jumbo v3, "token"

    invoke-virtual {p1, v3, v2}, Lcom/inrix/sdk/network/request/InrixRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 233
    :cond_4
    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->shouldAttachPhs()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 234
    invoke-static {}, Lcom/inrix/sdk/phs/PhsController;->getInstance()Lcom/inrix/sdk/phs/PhsController;

    move-result-object v3

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/inrix/sdk/phs/PhsController;->getPayload(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "phs":Ljava/lang/String;
    const-string/jumbo v3, "phsx"

    invoke-virtual {p1, v3, v0}, Lcom/inrix/sdk/network/request/InrixRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 239
    .end local v0    # "phs":Ljava/lang/String;
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "request is prepared: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 240
    invoke-virtual {p1, p0}, Lcom/inrix/sdk/network/request/InrixRequest;->setOnTokenExpiredListener(Lcom/inrix/sdk/network/request/InrixRequest$IOnTokenExpiredListener;)V

    .line 241
    iget-object v3, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v3, p1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    goto/16 :goto_0

    .line 224
    :cond_6
    iget-object v3, p1, Lcom/inrix/sdk/network/request/InrixRequest;->errorCallback:Lcom/android/volley/Response$ErrorListener;

    const/16 v4, 0x41d

    invoke-static {v4}, Lcom/inrix/sdk/exception/InrixException;->getVolleyError(I)Lcom/android/volley/VolleyError;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/volley/Response$ErrorListener;->onErrorResponse(Lcom/android/volley/VolleyError;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private processQueuedRequests()V
    .locals 6

    .prologue
    .line 345
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Process queued requests: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 346
    iget-object v4, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 347
    .local v3, "request":Ljava/lang/Object;
    iget-object v4, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 348
    check-cast v3, Lcom/inrix/sdk/network/request/InrixRequest;

    .end local v3    # "request":Ljava/lang/Object;
    invoke-direct {p0, v3}, Lcom/inrix/sdk/network/request/RequestExecutor;->processInrixRequest(Lcom/inrix/sdk/network/request/InrixRequest;)V

    .line 346
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 350
    :cond_0
    return-void
.end method


# virtual methods
.method public clearStoredBaseUrls()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->setBaseUrls(Lcom/inrix/sdk/model/BaseURL;)V

    .line 270
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setRegistrationServer(Ljava/lang/String;)V

    .line 271
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setAuthenticationServer(Ljava/lang/String;)V

    .line 272
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setRegion(Ljava/lang/String;)V

    .line 273
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setMobileServer(Ljava/lang/String;)V

    .line 274
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setTTSServer(Ljava/lang/String;)V

    .line 275
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setRoutingServer(Ljava/lang/String;)V

    .line 276
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setParkingServer(Ljava/lang/String;)V

    .line 277
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setFuelServer(Ljava/lang/String;)V

    .line 278
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setIncidentsServer(Ljava/lang/String;)V

    .line 279
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setDustServer(Ljava/lang/String;)V

    .line 280
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setSettingsServer(Ljava/lang/String;)V

    .line 281
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setAnalyticsServer(Ljava/lang/String;)V

    .line 282
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setCompositeTilesServer(Ljava/lang/String;)V

    .line 283
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setCamerasServer(Ljava/lang/String;)V

    .line 284
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setTravelDataServer(Ljava/lang/String;)V

    .line 285
    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setTrafficStateServer(Ljava/lang/String;)V

    .line 286
    return-void
.end method

.method public declared-synchronized execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/network/request/NetworkRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "request":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<*>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->isInitialised:Z

    if-nez v1, :cond_0

    .line 167
    const/16 v1, 0x415

    invoke-static {v1}, Lcom/inrix/sdk/exception/InrixException;->getVolleyError(I)Lcom/android/volley/VolleyError;

    move-result-object v0

    .line 169
    .local v0, "error":Lcom/android/volley/VolleyError;
    invoke-virtual {p1, v0}, Lcom/inrix/sdk/network/request/NetworkRequest;->deliverError(Lcom/android/volley/VolleyError;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    .end local v0    # "error":Lcom/android/volley/VolleyError;
    .end local p1    # "request":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<*>;"
    :goto_0
    monitor-exit p0

    return-void

    .line 172
    .restart local p1    # "request":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<*>;"
    :cond_0
    :try_start_1
    instance-of v1, p1, Lcom/inrix/sdk/network/request/InrixRequest;

    if-nez v1, :cond_1

    .line 176
    iget-object v1, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, p1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166
    .end local p1    # "request":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<*>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 178
    .restart local p1    # "request":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<*>;"
    :cond_1
    :try_start_2
    check-cast p1, Lcom/inrix/sdk/network/request/InrixRequest;

    .end local p1    # "request":Lcom/inrix/sdk/network/request/NetworkRequest;, "Lcom/inrix/sdk/network/request/NetworkRequest<*>;"
    invoke-direct {p0, p1}, Lcom/inrix/sdk/network/request/RequestExecutor;->processInrixRequest(Lcom/inrix/sdk/network/request/InrixRequest;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public failAllPendingRequestsWithError(Lcom/android/volley/VolleyError;)V
    .locals 6
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 353
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Fail all pending requests: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 354
    iget-object v4, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 355
    .local v3, "request":Ljava/lang/Object;
    iget-object v4, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    move-object v4, v3

    .line 356
    check-cast v4, Lcom/inrix/sdk/network/request/InrixRequest;

    invoke-virtual {v4}, Lcom/inrix/sdk/network/request/InrixRequest;->getErrorListener()Lcom/android/volley/Response$ErrorListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 357
    check-cast v3, Lcom/inrix/sdk/network/request/InrixRequest;

    .end local v3    # "request":Ljava/lang/Object;
    invoke-virtual {v3}, Lcom/inrix/sdk/network/request/InrixRequest;->getErrorListener()Lcom/android/volley/Response$ErrorListener;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/android/volley/Response$ErrorListener;->onErrorResponse(Lcom/android/volley/VolleyError;)V

    .line 354
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 361
    :cond_1
    return-void
.end method

.method public getAuthenticator()Lcom/inrix/sdk/auth/InrixAuthenticator;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    return-object v0
.end method

.method public getBaseURLs()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Lcom/inrix/sdk/auth/InrixURLFetcher;

    invoke-direct {v0, p0}, Lcom/inrix/sdk/auth/InrixURLFetcher;-><init>(Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;)V

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;

    .line 294
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;

    invoke-virtual {v0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->getBaseURL()V

    .line 296
    :cond_0
    return-void
.end method

.method public haveBaseUrls()Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getBaseUrls()Lcom/inrix/sdk/model/BaseURL;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized init(Landroid/content/Context;Lcom/android/volley/toolbox/HttpStack;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "httpStack"    # Lcom/android/volley/toolbox/HttpStack;

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    invoke-static {v0, v1}, Lcom/android/volley/toolbox/Volley;->newRequestQueue(Landroid/content/Context;Lcom/android/volley/toolbox/HttpStack;)Lcom/android/volley/RequestQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    .line 81
    new-instance v0, Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-direct {v0, p1, p0}, Lcom/inrix/sdk/auth/InrixAuthenticator;-><init>(Landroid/content/Context;Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;)V

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->isInitialised:Z

    .line 86
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-direct {p0}, Lcom/inrix/sdk/network/request/RequestExecutor;->parseCachedBaseUrls()Lcom/inrix/sdk/model/BaseURL;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->setBaseUrls(Lcom/inrix/sdk/model/BaseURL;)V

    .line 90
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getBaseUrls()Lcom/inrix/sdk/model/BaseURL;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getAuthToken()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :goto_0
    monitor-exit p0

    return-void

    .line 96
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/inrix/sdk/network/request/RequestExecutor;->getBaseURLs()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->isInitialised:Z

    return v0
.end method

.method public onAuthCompleted(Ljava/lang/String;Ljava/util/Date;)V
    .locals 0
    .param p1, "token"    # Ljava/lang/String;
    .param p2, "expirationDate"    # Ljava/util/Date;

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/inrix/sdk/network/request/RequestExecutor;->processQueuedRequests()V

    .line 248
    return-void
.end method

.method public onAuthError(Lcom/android/volley/VolleyError;)V
    .locals 0
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 253
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/network/request/RequestExecutor;->failAllPendingRequestsWithError(Lcom/android/volley/VolleyError;)V

    .line 254
    return-void
.end method

.method public onTokenExpiredError(Lcom/inrix/sdk/network/request/InrixRequest;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/network/request/InrixRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 309
    .local p1, "request":Lcom/inrix/sdk/network/request/InrixRequest;, "Lcom/inrix/sdk/network/request/InrixRequest<*>;"
    const-string/jumbo v0, "token"

    invoke-virtual {p1, v0}, Lcom/inrix/sdk/network/request/InrixRequest;->getParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {v1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const-string/jumbo v0, "TokenExpired error returned from CS. Invalidating existing token"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->setAuthToken(Lcom/inrix/sdk/model/DeviceAuth;)V

    .line 314
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->refreshToken()V

    .line 316
    :cond_0
    return-void
.end method

.method public onURLFetchFailed(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "URL fetch failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/network/request/RequestExecutor;->failAllPendingRequestsWithError(Lcom/android/volley/VolleyError;)V

    .line 338
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;

    .line 339
    return-void
.end method

.method public onURLFetchSuccess(Lcom/inrix/sdk/model/BaseURL;)V
    .locals 1
    .param p1, "baseURL"    # Lcom/inrix/sdk/model/BaseURL;

    .prologue
    .line 320
    if-eqz p1, :cond_0

    .line 321
    const-string/jumbo v0, "URL fetch success"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p1}, Lcom/inrix/sdk/model/BaseURL;->getRegistrationURL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setRegistrationServer(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p1}, Lcom/inrix/sdk/model/BaseURL;->getAuthenticationURL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setAuthenticationServer(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p1}, Lcom/inrix/sdk/model/BaseURL;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setRegion(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->authenticator:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-virtual {v0, p1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->setBaseUrls(Lcom/inrix/sdk/model/BaseURL;)V

    .line 329
    :cond_0
    invoke-direct {p0}, Lcom/inrix/sdk/network/request/RequestExecutor;->processQueuedRequests()V

    .line 330
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;

    .line 331
    return-void
.end method

.method public declared-synchronized shutdown()V
    .locals 2

    .prologue
    .line 138
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->httpStack:Lcom/android/volley/toolbox/HttpStack;

    .line 139
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->queuedRequests:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 140
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    new-instance v1, Lcom/inrix/sdk/network/request/RequestExecutor$1;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/network/request/RequestExecutor$1;-><init>(Lcom/inrix/sdk/network/request/RequestExecutor;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->cancelAll(Lcom/android/volley/RequestQueue$RequestFilter;)V

    .line 147
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->stop()V

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->requestQueue:Lcom/android/volley/RequestQueue;

    .line 150
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->isInitialised:Z

    .line 151
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;

    invoke-virtual {v0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->cancel()V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/network/request/RequestExecutor;->urlFetcher:Lcom/inrix/sdk/auth/InrixURLFetcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :cond_1
    monitor-exit p0

    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
