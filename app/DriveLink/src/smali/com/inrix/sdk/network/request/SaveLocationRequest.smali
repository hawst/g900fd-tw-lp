.class public Lcom/inrix/sdk/network/request/SaveLocationRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "SaveLocationRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/SingleLocationCollection;",
        ">;"
    }
.end annotation


# instance fields
.field private final API_NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;Ljava/lang/String;Ljava/lang/String;IILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "position"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "customData"    # Ljava/lang/String;
    .param p5, "locationType"    # I
    .param p6, "order"    # I
    .param p8, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/inrix/sdk/model/GeoPoint;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/SingleLocationCollection;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    .local p7, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/SingleLocationCollection;>;"
    invoke-direct {p0, p7, p8}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 13
    const-string/jumbo v0, "CreateLocation"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/SaveLocationRequest;->API_NAME:Ljava/lang/String;

    .line 20
    if-nez p2, :cond_0

    .line 21
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 24
    :cond_0
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0, p1}, Lcom/inrix/sdk/network/request/SaveLocationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 25
    const-string/jumbo v0, "latitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/inrix/sdk/network/request/SaveLocationRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 26
    const-string/jumbo v0, "longitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/inrix/sdk/network/request/SaveLocationRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 27
    const-string/jumbo v0, "address"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/SaveLocationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 28
    const-string/jumbo v0, "customdata"

    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/network/request/SaveLocationRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 29
    const-string/jumbo v0, "ordering"

    invoke-virtual {p0, v0, p6}, Lcom/inrix/sdk/network/request/SaveLocationRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 30
    const-string/jumbo v0, "locationtype"

    invoke-virtual {p0, v0, p5}, Lcom/inrix/sdk/network/request/SaveLocationRequest;->setParameter(Ljava/lang/String;I)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 31
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string/jumbo v0, "CreateLocation"

    return-object v0
.end method

.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/SingleLocationCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/SingleLocationCollection;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
