.class public final Lcom/inrix/sdk/network/request/TrafficQualityRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "TrafficQualityRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/TrafficQuality;",
        ">;"
    }
.end annotation


# static fields
.field private static final API_NAME:Ljava/lang/String; = "GetTrafficQuality"


# direct methods
.method public constructor <init>(DDLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p6, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DD",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/TrafficQuality;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    .local p5, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/TrafficQuality;>;"
    invoke-direct {p0, p5, p6}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 36
    const-string/jumbo v0, "latitude"

    invoke-virtual {p0, v0, p1, p2}, Lcom/inrix/sdk/network/request/TrafficQualityRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 37
    const-string/jumbo v0, "longitude"

    invoke-virtual {p0, v0, p3, p4}, Lcom/inrix/sdk/network/request/TrafficQualityRequest;->setParameter(Ljava/lang/String;D)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 38
    return-void
.end method


# virtual methods
.method public final getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string/jumbo v0, "GetTrafficQuality"

    return-object v0
.end method

.method public final getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/TrafficQuality;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/TrafficQuality;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAFFIC_STATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
