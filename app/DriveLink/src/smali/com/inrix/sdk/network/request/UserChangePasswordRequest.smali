.class public Lcom/inrix/sdk/network/request/UserChangePasswordRequest;
.super Lcom/inrix/sdk/network/request/XMLInrixRequest;
.source "UserChangePasswordRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/XMLInrixRequest",
        "<",
        "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .param p3, "oldPassword"    # Ljava/lang/String;
    .param p4, "newPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/parser/xml/XMLEntityBase;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 31
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.User.ChangePassword"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/UserChangePasswordRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 32
    const-string/jumbo v0, "password"

    invoke-virtual {p0, v0, p4}, Lcom/inrix/sdk/network/request/UserChangePasswordRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 33
    const-string/jumbo v0, "oldpassword"

    invoke-virtual {p0, v0, p3}, Lcom/inrix/sdk/network/request/UserChangePasswordRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 34
    return-void
.end method


# virtual methods
.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;

    const-class v1, Lcom/inrix/sdk/parser/xml/XMLEntityBase;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->REGISTER:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
