.class public Lcom/inrix/sdk/network/request/GetLastLocationsUpdateRequest;
.super Lcom/inrix/sdk/network/request/JSONInrixRequest;
.source "GetLastLocationsUpdateRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/JSONInrixRequest",
        "<",
        "Lcom/inrix/sdk/model/LastLocationsUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field private final API_NAME:Ljava/lang/String;

.field private final OUTPUT_FIELDS:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/model/LastLocationsUpdate;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/model/LastLocationsUpdate;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/JSONInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 11
    const-string/jumbo v0, "GetUpdatedTimeInfoForConsumer"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/GetLastLocationsUpdateRequest;->API_NAME:Ljava/lang/String;

    .line 12
    const-string/jumbo v0, "customroute,departurealert,location"

    iput-object v0, p0, Lcom/inrix/sdk/network/request/GetLastLocationsUpdateRequest;->OUTPUT_FIELDS:Ljava/lang/String;

    .line 18
    const-string/jumbo v0, "outputfields"

    const-string/jumbo v1, "customroute,departurealert,location"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/network/request/GetLastLocationsUpdateRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 19
    return-void
.end method


# virtual methods
.method public getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string/jumbo v0, "GetUpdatedTimeInfoForConsumer"

    return-object v0
.end method

.method public getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/model/LastLocationsUpdate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;

    const-class v1, Lcom/inrix/sdk/model/LastLocationsUpdate;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method
