.class public final Lcom/inrix/sdk/network/request/RequestParams$IncidentOutputFieldName;
.super Ljava/lang/Object;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/network/request/RequestParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "IncidentOutputFieldName"
.end annotation


# static fields
.field public static final ALL:Ljava/lang/String; = "all"

.field public static final AREA:Ljava/lang/String; = "area"

.field public static final DELAY_IMPACT:Ljava/lang/String; = "delayimpact"

.field public static final END_TIME:Ljava/lang/String; = "endtime"

.field public static final EVENT_CODE:Ljava/lang/String; = "eventcode"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IMPACTING:Ljava/lang/String; = "impacting"

.field public static final LAT_LONG:Ljava/lang/String; = "latlong"

.field public static final RDS:Ljava/lang/String; = "rds"

.field public static final SEVERITY:Ljava/lang/String; = "severity"

.field public static final START_TIME:Ljava/lang/String; = "starttime"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final VERSION:Ljava/lang/String; = "version"


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/network/request/RequestParams;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/network/request/RequestParams;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/inrix/sdk/network/request/RequestParams$IncidentOutputFieldName;->this$0:Lcom/inrix/sdk/network/request/RequestParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
