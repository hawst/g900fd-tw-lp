.class Lcom/inrix/sdk/UserManager$LoginProcessor$1;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/UserManager$LoginProcessor;->process(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/DeviceRegister;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/UserManager$LoginProcessor;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$LoginProcessor$1;->this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/DeviceRegister;)V
    .locals 2
    .param p1, "registerResult"    # Lcom/inrix/sdk/model/DeviceRegister;

    .prologue
    .line 389
    iget-object v0, p1, Lcom/inrix/sdk/model/DeviceRegister;->deviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setDeviceId(Ljava/lang/String;)V

    .line 390
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setLoggedIn(Z)V

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "register complete: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/inrix/sdk/model/DeviceRegister;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogV(Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor$1;->this$1:Lcom/inrix/sdk/UserManager$LoginProcessor;

    # invokes: Lcom/inrix/sdk/UserManager$LoginProcessor;->auth()V
    invoke-static {v0}, Lcom/inrix/sdk/UserManager$LoginProcessor;->access$000(Lcom/inrix/sdk/UserManager$LoginProcessor;)V

    .line 394
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 385
    check-cast p1, Lcom/inrix/sdk/model/DeviceRegister;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/UserManager$LoginProcessor$1;->onResponse(Lcom/inrix/sdk/model/DeviceRegister;)V

    return-void
.end method
