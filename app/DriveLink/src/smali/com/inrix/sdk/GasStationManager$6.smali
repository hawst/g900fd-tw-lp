.class Lcom/inrix/sdk/GasStationManager$6;
.super Ljava/lang/Object;
.source "GasStationManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/GasStationManager;->getGasStationInformation(Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/GasStationManager;

.field final synthetic val$listener:Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$6;->this$0:Lcom/inrix/sdk/GasStationManager;

    iput-object p2, p0, Lcom/inrix/sdk/GasStationManager$6;->val$listener:Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$6;->val$listener:Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 207
    return-void
.end method
