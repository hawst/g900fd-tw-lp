.class public final enum Lcom/inrix/sdk/AlertsManager$ACTIONS;
.super Ljava/lang/Enum;
.source "AlertsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/AlertsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ACTIONS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/AlertsManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/AlertsManager$ACTIONS;

.field public static final enum SMART_ALERT:Lcom/inrix/sdk/AlertsManager$ACTIONS;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    new-instance v0, Lcom/inrix/sdk/AlertsManager$ACTIONS;

    const-string/jumbo v1, "SMART_ALERT"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/AlertsManager$ACTIONS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/AlertsManager$ACTIONS;->SMART_ALERT:Lcom/inrix/sdk/AlertsManager$ACTIONS;

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/inrix/sdk/AlertsManager$ACTIONS;

    sget-object v1, Lcom/inrix/sdk/AlertsManager$ACTIONS;->SMART_ALERT:Lcom/inrix/sdk/AlertsManager$ACTIONS;

    aput-object v1, v0, v2

    sput-object v0, Lcom/inrix/sdk/AlertsManager$ACTIONS;->$VALUES:[Lcom/inrix/sdk/AlertsManager$ACTIONS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/AlertsManager$ACTIONS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    const-class v0, Lcom/inrix/sdk/AlertsManager$ACTIONS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/AlertsManager$ACTIONS;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/AlertsManager$ACTIONS;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/inrix/sdk/AlertsManager$ACTIONS;->$VALUES:[Lcom/inrix/sdk/AlertsManager$ACTIONS;

    invoke-virtual {v0}, [Lcom/inrix/sdk/AlertsManager$ACTIONS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/AlertsManager$ACTIONS;

    return-object v0
.end method
