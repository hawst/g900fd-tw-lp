.class Lcom/inrix/sdk/LocationsManager$9;
.super Ljava/lang/Object;
.source "LocationsManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/LocationsManager;->getLastLocationsUpdate(Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/LastLocationsUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/LocationsManager;

.field final synthetic val$listener:Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;)V
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$9;->this$0:Lcom/inrix/sdk/LocationsManager;

    iput-object p2, p0, Lcom/inrix/sdk/LocationsManager$9;->val$listener:Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/LastLocationsUpdate;)V
    .locals 1
    .param p1, "response"    # Lcom/inrix/sdk/model/LastLocationsUpdate;

    .prologue
    .line 417
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$9;->val$listener:Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;->onResult(Ljava/lang/Object;)V

    .line 418
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 413
    check-cast p1, Lcom/inrix/sdk/model/LastLocationsUpdate;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/LocationsManager$9;->onResponse(Lcom/inrix/sdk/model/LastLocationsUpdate;)V

    return-void
.end method
