.class public final Lcom/inrix/sdk/RouteManager;
.super Ljava/lang/Object;
.source "RouteManager.java"

# interfaces
.implements Lcom/inrix/sdk/IRefreshableActions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/RouteManager$5;,
        Lcom/inrix/sdk/RouteManager$TravelTimeOptions;,
        Lcom/inrix/sdk/RouteManager$RouteOptions;,
        Lcom/inrix/sdk/RouteManager$IRouteResponseListener;,
        Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;,
        Lcom/inrix/sdk/RouteManager$ACTIONS;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/IRefreshableActions",
        "<",
        "Lcom/inrix/sdk/RouteManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEEFAULT_INTERVAL:I = 0xb4

.field private static final MAXIMUM_TRAVEL_TIME_COUNT:I = 0x60

.field private static final MAXIMUM_TRAVEL_TIME_INTERVAL:I = 0x5a0

.field private static final MINIMUM_TRAVEL_TIME_COUNT:I = 0x1

.field private static final MINIMUM_TRAVEL_TIME_INTERVAL:I = 0x1

.field public static final ROUTE_OUTPUT_FIELD_ALL:I = 0xff

.field public static final ROUTE_OUTPUT_FIELD_BOUNDING_BOX:I = 0x10

.field public static final ROUTE_OUTPUT_FIELD_DESCRIPTION:I = 0x1

.field public static final ROUTE_OUTPUT_FIELD_INCIDENTS:I = 0x40

.field public static final ROUTE_OUTPUT_FIELD_MANEUVERS:I = 0x4

.field public static final ROUTE_OUTPUT_FIELD_POINTS:I = 0x20

.field public static final ROUTE_OUTPUT_FIELD_SUMMARY:I = 0x2

.field public static final ROUTE_OUTPUT_FIELD_WAYPOINTS:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    return-void
.end method


# virtual methods
.method public getRefreshInterval(Lcom/inrix/sdk/RouteManager$ACTIONS;)I
    .locals 3
    .param p1, "action"    # Lcom/inrix/sdk/RouteManager$ACTIONS;

    .prologue
    .line 91
    if-nez p1, :cond_0

    .line 92
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x412

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 94
    :cond_0
    sget-object v1, Lcom/inrix/sdk/RouteManager$5;->$SwitchMap$com$inrix$sdk$RouteManager$ACTIONS:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$ACTIONS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    .line 96
    const/16 v0, 0xb4

    .line 100
    .local v0, "interval":I
    return v0
.end method

.method public bridge synthetic getRefreshInterval(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Lcom/inrix/sdk/RouteManager$ACTIONS;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/RouteManager;->getRefreshInterval(Lcom/inrix/sdk/RouteManager$ACTIONS;)I

    move-result v0

    return v0
.end method

.method public requestRoutes(Lcom/inrix/sdk/RouteManager$RouteOptions;Lcom/inrix/sdk/RouteManager$IRouteResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 11
    .param p1, "params"    # Lcom/inrix/sdk/RouteManager$RouteOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/RouteManager$IRouteResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 671
    if-nez p2, :cond_0

    .line 672
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 675
    :cond_0
    if-nez p1, :cond_1

    .line 676
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 679
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getStart()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 680
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f8

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 683
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getEnd()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 684
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f9

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 687
    :cond_3
    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getTolerance()I

    move-result v1

    if-gez v1, :cond_4

    .line 688
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3fa

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 691
    :cond_4
    new-instance v0, Lcom/inrix/sdk/network/request/RouteFindRequest;

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getStart()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getEnd()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v2

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v3

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getWaypoints()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getTolerance()I

    move-result v5

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getHeading()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getNumAlternates()I

    move-result v7

    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$RouteOptions;->getOutputFieldsAsString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/inrix/sdk/RouteManager$3;

    invoke-direct {v9, p0, p2}, Lcom/inrix/sdk/RouteManager$3;-><init>(Lcom/inrix/sdk/RouteManager;Lcom/inrix/sdk/RouteManager$IRouteResponseListener;)V

    new-instance v10, Lcom/inrix/sdk/RouteManager$4;

    invoke-direct {v10, p0, p2}, Lcom/inrix/sdk/RouteManager$4;-><init>(Lcom/inrix/sdk/RouteManager;Lcom/inrix/sdk/RouteManager$IRouteResponseListener;)V

    invoke-direct/range {v0 .. v10}, Lcom/inrix/sdk/network/request/RouteFindRequest;-><init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/utils/UserPreferences$UNIT;Ljava/util/List;ILjava/lang/Integer;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 712
    .local v0, "request":Lcom/inrix/sdk/network/request/RouteFindRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 713
    return-object v0
.end method

.method public final requestTravelTimes(Lcom/inrix/sdk/RouteManager$TravelTimeOptions;Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 3
    .param p1, "requestParameters"    # Lcom/inrix/sdk/RouteManager$TravelTimeOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 387
    if-nez p2, :cond_0

    .line 388
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 391
    :cond_0
    if-nez p1, :cond_1

    .line 392
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 396
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->isTravelTimeCountValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 397
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3fd

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 401
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->isTravelTimeIntervalValid()Z

    move-result v1

    if-nez v1, :cond_3

    .line 402
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3fe

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 406
    :cond_3
    invoke-virtual {p1}, Lcom/inrix/sdk/RouteManager$TravelTimeOptions;->isRouteValid()Z

    move-result v1

    if-nez v1, :cond_4

    .line 407
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ff

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 411
    :cond_4
    new-instance v0, Lcom/inrix/sdk/network/request/TravelTimeRequest;

    new-instance v1, Lcom/inrix/sdk/RouteManager$1;

    invoke-direct {v1, p0, p2}, Lcom/inrix/sdk/RouteManager$1;-><init>(Lcom/inrix/sdk/RouteManager;Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;)V

    new-instance v2, Lcom/inrix/sdk/RouteManager$2;

    invoke-direct {v2, p0, p2}, Lcom/inrix/sdk/RouteManager$2;-><init>(Lcom/inrix/sdk/RouteManager;Lcom/inrix/sdk/RouteManager$ITravelTimeResponseListener;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/inrix/sdk/network/request/TravelTimeRequest;-><init>(Lcom/inrix/sdk/RouteManager$TravelTimeOptions;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 435
    .local v0, "travelTimeRequest":Lcom/inrix/sdk/network/request/TravelTimeRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 436
    return-object v0
.end method
