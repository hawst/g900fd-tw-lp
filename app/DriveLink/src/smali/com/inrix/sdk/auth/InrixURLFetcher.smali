.class public Lcom/inrix/sdk/auth/InrixURLFetcher;
.super Ljava/lang/Object;
.source "InrixURLFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;
    }
.end annotation


# static fields
.field private static final WAIT_FOR_LOCATION_FIX_DELAY:J = 0x1d4c0L


# instance fields
.field private currentRequest:Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;

.field private geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

.field private locationChecker:Ljava/util/Timer;

.field private resultListener:Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->locationChecker:Ljava/util/Timer;

    .line 31
    iput-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    .line 32
    iput-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->currentRequest:Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;

    .line 47
    if-nez p1, :cond_0

    .line 48
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 50
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->resultListener:Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;

    .line 51
    new-instance v0, Lcom/inrix/sdk/auth/InrixURLFetcher$1;

    invoke-direct {v0, p0}, Lcom/inrix/sdk/auth/InrixURLFetcher$1;-><init>(Lcom/inrix/sdk/auth/InrixURLFetcher;)V

    iput-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/inrix/sdk/auth/InrixURLFetcher;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixURLFetcher;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->locationChecker:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/inrix/sdk/auth/InrixURLFetcher;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixURLFetcher;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->resetGeoLocationTracker()V

    return-void
.end method

.method static synthetic access$200(Lcom/inrix/sdk/auth/InrixURLFetcher;)Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixURLFetcher;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/inrix/sdk/auth/InrixURLFetcher;)Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixURLFetcher;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->resultListener:Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;

    return-object v0
.end method

.method private resetGeoLocationTracker()V
    .locals 2

    .prologue
    .line 104
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/geolocation/GeolocationController;->isDriving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/geolocation/GeolocationController;->stopLocationTracking()V

    .line 110
    :cond_0
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->setPriority(Ljava/lang/Integer;)V

    .line 111
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 144
    const-string/jumbo v0, "Cancel base URL fetcher"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->currentRequest:Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->currentRequest:Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;->cancel()V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    if-eqz v0, :cond_1

    .line 149
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->removeOnFixListener(Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;)V

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->locationChecker:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 153
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->locationChecker:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 155
    :cond_2
    invoke-direct {p0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->resetGeoLocationTracker()V

    .line 156
    return-void
.end method

.method public getBaseURL()V
    .locals 5

    .prologue
    .line 64
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->locationChecker:Ljava/util/Timer;

    .line 65
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->locationChecker:Ljava/util/Timer;

    new-instance v2, Lcom/inrix/sdk/auth/InrixURLFetcher$2;

    invoke-direct {v2, p0}, Lcom/inrix/sdk/auth/InrixURLFetcher$2;-><init>(Lcom/inrix/sdk/auth/InrixURLFetcher;)V

    const-wide/32 v3, 0x1d4c0

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 85
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 87
    .local v0, "lastKnownLocation":Landroid/location/Location;
    if-eqz v0, :cond_0

    .line 88
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    invoke-interface {v1, v0}, Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;->onLocationAcquired(Landroid/location/Location;)V

    .line 97
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    const/16 v2, 0x69

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/inrix/sdk/geolocation/GeolocationController;->setPriority(Ljava/lang/Integer;)V

    .line 93
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    invoke-virtual {v1, v2}, Lcom/inrix/sdk/geolocation/GeolocationController;->addOnFixListener(Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;)V

    .line 95
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->startLocationTracking()V

    goto :goto_0
.end method

.method protected getBaseURL(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 115
    new-instance v0, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;

    new-instance v1, Lcom/inrix/sdk/auth/InrixURLFetcher$3;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/auth/InrixURLFetcher$3;-><init>(Lcom/inrix/sdk/auth/InrixURLFetcher;)V

    new-instance v2, Lcom/inrix/sdk/auth/InrixURLFetcher$4;

    invoke-direct {v2, p0}, Lcom/inrix/sdk/auth/InrixURLFetcher$4;-><init>(Lcom/inrix/sdk/auth/InrixURLFetcher;)V

    invoke-direct {v0, v1, v2, p1}, Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Landroid/location/Location;)V

    iput-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->currentRequest:Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;

    .line 133
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher;->currentRequest:Lcom/inrix/sdk/network/request/internal/DeviceBaseURLRequest;

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    const-string/jumbo v0, "RequestExecutor is not Running..."

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogW(Ljava/lang/String;)V

    goto :goto_0
.end method
