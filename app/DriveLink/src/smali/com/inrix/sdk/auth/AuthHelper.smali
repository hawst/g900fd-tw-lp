.class public Lcom/inrix/sdk/auth/AuthHelper;
.super Ljava/lang/Object;
.source "AuthHelper.java"


# static fields
.field private static serverTypeStringMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/inrix/sdk/network/request/InrixRequest$ServerType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    .line 26
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->MOBILE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Mobile"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "TTS"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ANALYTICS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Analytics"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->CAMERAS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Cameras"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->COMPOSITE_TILES:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "CompositeTiles"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->DUST:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Dust"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->FUEL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Fuel"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->INCIDENTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Incidents"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->PARKING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Parking"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ROUTING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Routing"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->SETTINGS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "Settings"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAFFIC_STATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "TrafficState"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    const-string/jumbo v2, "TravelData"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/inrix/sdk/Inrix;->getInstance()Lcom/inrix/sdk/Inrix;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/Inrix;->getConfiguration()Lcom/inrix/sdk/InrixConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/InrixConfig;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDeviceModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "android"

    return-object v0
.end method

.method public static getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public static getHardwareId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getInstallationIdHash()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "installationIdHash":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/inrix/sdk/utils/UserPreferences;->setDeviceId(Ljava/lang/String;)V

    .line 50
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    .line 51
    .local v1, "uuid":Ljava/util/UUID;
    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setInstallationIdHash(Ljava/lang/String;)V

    .line 54
    .end local v1    # "uuid":Ljava/util/UUID;
    :cond_0
    return-object v0
.end method

.method private static getSha1String(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "stringToEncode"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 163
    :try_start_0
    const-string/jumbo v7, "SHA1"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 164
    .local v1, "md":Ljava/security/MessageDigest;
    const-string/jumbo v7, "UTF-8"

    invoke-virtual {p0, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    .line 165
    .local v2, "messageDigest":[B
    new-instance v4, Ljava/math/BigInteger;

    const/4 v7, 0x1

    invoke-direct {v4, v7, v2}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 166
    .local v4, "number":Ljava/math/BigInteger;
    const/16 v7, 0x10

    invoke-virtual {v4, v7}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "hexValue":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x28

    if-ge v7, v8, :cond_0

    .line 169
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 171
    .end local v0    # "hexValue":Ljava/lang/String;
    .end local v1    # "md":Ljava/security/MessageDigest;
    .end local v2    # "messageDigest":[B
    .end local v4    # "number":Ljava/math/BigInteger;
    :catch_0
    move-exception v3

    .line 172
    .local v3, "nsae":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v3}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    move-object v0, v6

    .line 176
    .end local v3    # "nsae":Ljava/security/NoSuchAlgorithmException;
    :cond_0
    :goto_1
    return-object v0

    .line 174
    :catch_1
    move-exception v5

    .line 175
    .local v5, "uee":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    move-object v0, v6

    .line 176
    goto :goto_1
.end method

.method public static getSystemVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public static getTokenForDeviceRegister(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "hardwareId"    # Ljava/lang/String;
    .param p1, "appVersion"    # Ljava/lang/String;
    .param p2, "vendorId"    # Ljava/lang/String;
    .param p3, "deviceModel"    # Ljava/lang/String;
    .param p4, "systemVersion"    # Ljava/lang/String;
    .param p5, "vendorToken"    # Ljava/lang/String;

    .prologue
    .line 95
    const-string/jumbo v1, "%s|%s|%s|%s|%s|%s"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object p3, v2, v3

    const/4 v3, 0x4

    aput-object p4, v2, v3

    const/4 v3, 0x5

    aput-object p5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "params":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Not encrypted register token string: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 103
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->getSha1String(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTokenForDeviceRegisterEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "hardwareId"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/lang/String;
    .param p2, "email"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 122
    const-string/jumbo v1, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getAppVersion()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getDeviceModel()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getSystemVersion()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object p2, v2, v3

    const/4 v3, 0x6

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p1, v2, v3

    const/16 v3, 0x8

    aput-object p3, v2, v3

    const/16 v3, 0x9

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorToken()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "params":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Not encrypted register-email token string: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 135
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->getSha1String(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTokenForResetPassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "email"    # Ljava/lang/String;

    .prologue
    .line 146
    const-string/jumbo v1, "%s|%s|%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Lcom/inrix/sdk/auth/AuthHelper;->getVendorToken()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "params":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Not encrypted auth token string: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 151
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->getSha1String(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static final getVendorId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/inrix/sdk/Inrix;->getInstance()Lcom/inrix/sdk/Inrix;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/Inrix;->getConfiguration()Lcom/inrix/sdk/InrixConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/InrixConfig;->getVendorId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getVendorToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lcom/inrix/sdk/Inrix;->getInstance()Lcom/inrix/sdk/Inrix;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/Inrix;->getConfiguration()Lcom/inrix/sdk/InrixConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/InrixConfig;->getVendorToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .prologue
    .line 187
    sget-object v0, Lcom/inrix/sdk/auth/AuthHelper;->serverTypeStringMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
