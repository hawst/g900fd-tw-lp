.class Lcom/inrix/sdk/auth/InrixURLFetcher$2;
.super Ljava/util/TimerTask;
.source "InrixURLFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/auth/InrixURLFetcher;->getBaseURL()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/auth/InrixURLFetcher;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$2;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 74
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v1

    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$2;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    # getter for: Lcom/inrix/sdk/auth/InrixURLFetcher;->geoFixListener:Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;
    invoke-static {v2}, Lcom/inrix/sdk/auth/InrixURLFetcher;->access$200(Lcom/inrix/sdk/auth/InrixURLFetcher;)Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/inrix/sdk/geolocation/GeolocationController;->removeOnFixListener(Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;)V

    .line 76
    const-string/jumbo v1, "ERROR - Unable to get a LOCATION FIX"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/android/volley/VolleyError;

    const-string/jumbo v1, "ERROR - Unable to get a LOCATION FIX"

    invoke-direct {v0, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "ve":Lcom/android/volley/VolleyError;
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$2;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    # getter for: Lcom/inrix/sdk/auth/InrixURLFetcher;->resultListener:Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;
    invoke-static {v1}, Lcom/inrix/sdk/auth/InrixURLFetcher;->access$300(Lcom/inrix/sdk/auth/InrixURLFetcher;)Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;->onURLFetchFailed(Lcom/android/volley/VolleyError;)V

    .line 79
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$2;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    # invokes: Lcom/inrix/sdk/auth/InrixURLFetcher;->resetGeoLocationTracker()V
    invoke-static {v1}, Lcom/inrix/sdk/auth/InrixURLFetcher;->access$100(Lcom/inrix/sdk/auth/InrixURLFetcher;)V

    .line 80
    return-void
.end method
