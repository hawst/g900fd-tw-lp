.class public Lcom/inrix/sdk/auth/InrixAuthenticator;
.super Ljava/lang/Object;
.source "InrixAuthenticator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/auth/InrixAuthenticator$6;,
        Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    }
.end annotation


# instance fields
.field private authToken:Lcom/inrix/sdk/model/DeviceAuth;

.field private baseUrl:Lcom/inrix/sdk/model/BaseURL;

.field private volatile isInProgress:Z

.field private onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authCallback"    # Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->isInProgress:Z

    .line 27
    iput-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    .line 29
    iput-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    .line 43
    :try_start_0
    invoke-static {}, Lcom/inrix/sdk/model/DeviceAuth;->tryParseStoredAuthToken()Lcom/inrix/sdk/model/DeviceAuth;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    iput-object p2, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    .line 48
    return-void

    .line 44
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/inrix/sdk/auth/InrixAuthenticator;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixAuthenticator;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->register()V

    return-void
.end method

.method static synthetic access$100(Lcom/inrix/sdk/auth/InrixAuthenticator;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixAuthenticator;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->auth()V

    return-void
.end method

.method static synthetic access$202(Lcom/inrix/sdk/auth/InrixAuthenticator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixAuthenticator;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->isInProgress:Z

    return p1
.end method

.method static synthetic access$300(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixAuthenticator;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/model/DeviceAuth;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixAuthenticator;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    return-object v0
.end method

.method static synthetic access$402(Lcom/inrix/sdk/auth/InrixAuthenticator;Lcom/inrix/sdk/model/DeviceAuth;)Lcom/inrix/sdk/model/DeviceAuth;
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/auth/InrixAuthenticator;
    .param p1, "x1"    # Lcom/inrix/sdk/model/DeviceAuth;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    return-object p1
.end method

.method private auth()V
    .locals 3

    .prologue
    .line 119
    new-instance v0, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;

    new-instance v1, Lcom/inrix/sdk/auth/InrixAuthenticator$4;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/auth/InrixAuthenticator$4;-><init>(Lcom/inrix/sdk/auth/InrixAuthenticator;)V

    new-instance v2, Lcom/inrix/sdk/auth/InrixAuthenticator$5;

    invoke-direct {v2, p0}, Lcom/inrix/sdk/auth/InrixAuthenticator$5;-><init>(Lcom/inrix/sdk/auth/InrixAuthenticator;)V

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 185
    .local v0, "authRequest":Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 186
    return-void
.end method

.method private checkBaseURLAvailability()V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    if-nez v0, :cond_0

    .line 314
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->getBaseURLs()V

    .line 316
    :cond_0
    return-void
.end method

.method private clearBaseUrlsIfAuthTokenExpired()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth;->isExpired()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->isInProgress:Z

    if-nez v0, :cond_0

    .line 213
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->clearStoredBaseUrls()V

    .line 215
    :cond_0
    return-void
.end method

.method private register()V
    .locals 3

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->checkBaseURLAvailability()V

    .line 87
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    new-instance v0, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;

    new-instance v1, Lcom/inrix/sdk/auth/InrixAuthenticator$2;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/auth/InrixAuthenticator$2;-><init>(Lcom/inrix/sdk/auth/InrixAuthenticator;)V

    new-instance v2, Lcom/inrix/sdk/auth/InrixAuthenticator$3;

    invoke-direct {v2, p0}, Lcom/inrix/sdk/auth/InrixAuthenticator$3;-><init>(Lcom/inrix/sdk/auth/InrixAuthenticator;)V

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 109
    .local v0, "registerRequest":Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 113
    .end local v0    # "registerRequest":Lcom/inrix/sdk/network/request/internal/DeviceRegisterRequest;
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->auth()V

    goto :goto_0
.end method


# virtual methods
.method public getAuthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth;->isExpired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->clearBaseUrlsIfAuthTokenExpired()V

    .line 200
    invoke-virtual {p0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->refreshToken()V

    .line 201
    const/4 v0, 0x0

    .line 203
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v0

    iget-object v0, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    goto :goto_0
.end method

.method public getAuthTokenEntity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v0

    iget-object v0, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    .line 309
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBaseUrls()Lcom/inrix/sdk/model/BaseURL;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    return-object v0
.end method

.method public getServerPath(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;
    .locals 3
    .param p1, "type"    # Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .prologue
    .line 247
    const/4 v0, 0x0

    .line 249
    .local v0, "serverPath":Ljava/lang/String;
    sget-object v1, Lcom/inrix/sdk/auth/InrixAuthenticator$6;->$SwitchMap$com$inrix$sdk$network$request$InrixRequest$ServerType:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 290
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown server configuration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 294
    :cond_0
    :goto_0
    return-object v0

    .line 251
    :pswitch_0
    invoke-static {}, Lcom/inrix/sdk/Inrix;->getInstance()Lcom/inrix/sdk/Inrix;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/Inrix;->getConfiguration()Lcom/inrix/sdk/InrixConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/InrixConfig;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 253
    goto :goto_0

    .line 255
    :pswitch_1
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    if-eqz v1, :cond_1

    .line 256
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/BaseURL;->getRegistrationURL()Ljava/lang/String;

    move-result-object v0

    .line 258
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x419

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 263
    :pswitch_2
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    if-eqz v1, :cond_2

    .line 264
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/BaseURL;->getAuthenticationURL()Ljava/lang/String;

    move-result-object v0

    .line 266
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x41a

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 283
    :pswitch_3
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    if-nez v1, :cond_3

    .line 284
    const-string/jumbo v1, "not authenticated!!!"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogV(Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :cond_3
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->getURL(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v0

    .line 288
    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final hasToken()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 225
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/DeviceAuth;->isExpired()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v0

    .line 229
    :cond_1
    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v1

    iget-object v1, v1, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public refreshToken()V
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->isInProgress:Z

    if-eqz v0, :cond_0

    .line 63
    const-string/jumbo v0, "refreshToken skipped - already in progress"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 77
    :goto_0
    return-void

    .line 67
    :cond_0
    const-string/jumbo v0, "refreshToken"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->isInProgress:Z

    .line 70
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/inrix/sdk/auth/InrixAuthenticator$1;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/auth/InrixAuthenticator$1;-><init>(Lcom/inrix/sdk/auth/InrixAuthenticator;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public setAuthToken(Lcom/inrix/sdk/model/DeviceAuth;)V
    .locals 0
    .param p1, "deviceToken"    # Lcom/inrix/sdk/model/DeviceAuth;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    .line 299
    return-void
.end method

.method public setBaseUrls(Lcom/inrix/sdk/model/BaseURL;)V
    .locals 0
    .param p1, "baseUrl"    # Lcom/inrix/sdk/model/BaseURL;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->baseUrl:Lcom/inrix/sdk/model/BaseURL;

    .line 320
    return-void
.end method

.method public setOnAuthCompleteListener(Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    .prologue
    .line 51
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth;->isExpired()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v0

    iget-object v0, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    iget-object v1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/DeviceAuth;->getExpirationDate()Ljava/util/Date;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;->onAuthCompleted(Ljava/lang/String;Ljava/util/Date;)V

    .line 55
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    .line 56
    return-void
.end method

.method public setTokenExpirationTime(Ljava/util/Date;)V
    .locals 1
    .param p1, "value"    # Ljava/util/Date;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-virtual {v0, p1}, Lcom/inrix/sdk/model/DeviceAuth;->setExpirationDate(Ljava/util/Date;)V

    .line 303
    return-void
.end method
