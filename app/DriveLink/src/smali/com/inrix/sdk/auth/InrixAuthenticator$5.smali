.class Lcom/inrix/sdk/auth/InrixAuthenticator$5;
.super Ljava/lang/Object;
.source "InrixAuthenticator.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/auth/InrixAuthenticator;->auth()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/auth/InrixAuthenticator;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$5;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$5;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    const/4 v1, 0x0

    # setter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->isInProgress:Z
    invoke-static {v0, v1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$202(Lcom/inrix/sdk/auth/InrixAuthenticator;Z)Z

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "auth error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$5;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$300(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$5;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$300(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;->onAuthError(Lcom/android/volley/VolleyError;)V

    .line 182
    :cond_0
    return-void
.end method
