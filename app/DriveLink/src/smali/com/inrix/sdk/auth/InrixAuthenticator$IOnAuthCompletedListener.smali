.class public interface abstract Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
.super Ljava/lang/Object;
.source "InrixAuthenticator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/auth/InrixAuthenticator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IOnAuthCompletedListener"
.end annotation


# virtual methods
.method public abstract onAuthCompleted(Ljava/lang/String;Ljava/util/Date;)V
.end method

.method public abstract onAuthError(Lcom/android/volley/VolleyError;)V
.end method
