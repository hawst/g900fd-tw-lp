.class Lcom/inrix/sdk/auth/InrixURLFetcher$1;
.super Ljava/lang/Object;
.source "InrixURLFetcher.java"

# interfaces
.implements Lcom/inrix/sdk/geolocation/GeolocationController$IOnFixListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/auth/InrixURLFetcher;-><init>(Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/auth/InrixURLFetcher;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$1;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationAcquired(Landroid/location/Location;)V
    .locals 1
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$1;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    # getter for: Lcom/inrix/sdk/auth/InrixURLFetcher;->locationChecker:Ljava/util/Timer;
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->access$000(Lcom/inrix/sdk/auth/InrixURLFetcher;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 56
    const-string/jumbo v0, "Current location acquired. Fetch base urls"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$1;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    invoke-virtual {v0, p1}, Lcom/inrix/sdk/auth/InrixURLFetcher;->getBaseURL(Landroid/location/Location;)V

    .line 58
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$1;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    # invokes: Lcom/inrix/sdk/auth/InrixURLFetcher;->resetGeoLocationTracker()V
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->access$100(Lcom/inrix/sdk/auth/InrixURLFetcher;)V

    .line 59
    return-void
.end method
