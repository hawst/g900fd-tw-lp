.class Lcom/inrix/sdk/auth/InrixURLFetcher$3;
.super Ljava/lang/Object;
.source "InrixURLFetcher.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/auth/InrixURLFetcher;->getBaseURL(Landroid/location/Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/BaseURL;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/auth/InrixURLFetcher;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$3;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/BaseURL;)V
    .locals 1
    .param p1, "response"    # Lcom/inrix/sdk/model/BaseURL;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$3;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    # getter for: Lcom/inrix/sdk/auth/InrixURLFetcher;->resultListener:Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->access$300(Lcom/inrix/sdk/auth/InrixURLFetcher;)Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixURLFetcher$3;->this$0:Lcom/inrix/sdk/auth/InrixURLFetcher;

    # getter for: Lcom/inrix/sdk/auth/InrixURLFetcher;->resultListener:Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixURLFetcher;->access$300(Lcom/inrix/sdk/auth/InrixURLFetcher;)Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;->onURLFetchSuccess(Lcom/inrix/sdk/model/BaseURL;)V

    .line 122
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 115
    check-cast p1, Lcom/inrix/sdk/model/BaseURL;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/auth/InrixURLFetcher$3;->onResponse(Lcom/inrix/sdk/model/BaseURL;)V

    return-void
.end method
