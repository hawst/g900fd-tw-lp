.class public interface abstract Lcom/inrix/sdk/auth/InrixURLFetcher$IURLFetchListener;
.super Ljava/lang/Object;
.source "InrixURLFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/auth/InrixURLFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IURLFetchListener"
.end annotation


# virtual methods
.method public abstract onURLFetchFailed(Lcom/android/volley/VolleyError;)V
.end method

.method public abstract onURLFetchSuccess(Lcom/inrix/sdk/model/BaseURL;)V
.end method
