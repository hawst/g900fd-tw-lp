.class Lcom/inrix/sdk/auth/InrixAuthenticator$4;
.super Ljava/lang/Object;
.source "InrixAuthenticator.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/auth/InrixAuthenticator;->auth()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/DeviceAuth;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/auth/InrixAuthenticator;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private notifyError(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-static {p1}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    const/4 v1, 0x0

    # setter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    invoke-static {v0, v1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$402(Lcom/inrix/sdk/auth/InrixAuthenticator;Lcom/inrix/sdk/model/DeviceAuth;)Lcom/inrix/sdk/model/DeviceAuth;

    .line 124
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$300(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    invoke-static {v0}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$300(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    move-result-object v0

    new-instance v1, Lcom/android/volley/VolleyError;

    invoke-direct {v1, p1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;->onAuthError(Lcom/android/volley/VolleyError;)V

    .line 127
    :cond_0
    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/DeviceAuth;)V
    .locals 5
    .param p1, "response"    # Lcom/inrix/sdk/model/DeviceAuth;

    .prologue
    .line 131
    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    const/4 v3, 0x0

    # setter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->isInProgress:Z
    invoke-static {v2, v3}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$202(Lcom/inrix/sdk/auth/InrixAuthenticator;Z)Z

    .line 133
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->isExpired()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    const-string/jumbo v2, "ERROR: Server returned already expired token! Something is wrong.."

    invoke-direct {p0, v2}, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->notifyError(Ljava/lang/String;)V

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->parseEndPointsFromServer()Z

    move-result v0

    .line 142
    .local v0, "endPointsAvailable":Z
    if-nez v0, :cond_2

    .line 144
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/inrix/sdk/network/request/RequestExecutor;->clearStoredBaseUrls()V

    .line 147
    const-string/jumbo v2, "ERROR: Server did not provide the API end points. "

    invoke-direct {p0, v2}, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->notifyError(Ljava/lang/String;)V

    .line 151
    const/16 v2, 0x41c

    invoke-static {v2}, Lcom/inrix/sdk/exception/InrixException;->getVolleyError(I)Lcom/android/volley/VolleyError;

    move-result-object v1

    .line 153
    .local v1, "ve":Lcom/android/volley/VolleyError;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->failAllPendingRequestsWithError(Lcom/android/volley/VolleyError;)V

    goto :goto_0

    .line 157
    .end local v1    # "ve":Lcom/android/volley/VolleyError;
    :cond_2
    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # setter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    invoke-static {v2, p1}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$402(Lcom/inrix/sdk/auth/InrixAuthenticator;Lcom/inrix/sdk/model/DeviceAuth;)Lcom/inrix/sdk/model/DeviceAuth;

    .line 159
    invoke-virtual {p1}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->storeEndPointsInUserPreferences()V

    .line 160
    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    invoke-static {v2}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$400(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/model/DeviceAuth;

    move-result-object v2

    invoke-virtual {v2}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v2

    iget-object v2, v2, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->tokenExpireDateUTC:Ljava/lang/String;

    invoke-static {v2}, Lcom/inrix/sdk/utils/UserPreferences;->setAuthTokenExpireTime(Ljava/lang/String;)V

    .line 162
    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    invoke-static {v2}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$400(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/model/DeviceAuth;

    move-result-object v2

    invoke-virtual {v2}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v2

    iget-object v2, v2, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    invoke-static {v2}, Lcom/inrix/sdk/utils/UserPreferences;->setAuthToken(Ljava/lang/String;)V

    .line 164
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "auth complete: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    invoke-static {v3}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$400(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/model/DeviceAuth;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v3

    iget-object v3, v3, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 167
    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    invoke-static {v2}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$300(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 168
    iget-object v2, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->onAuthCompletedListener:Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;
    invoke-static {v2}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$300(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;

    move-result-object v2

    iget-object v3, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    invoke-static {v3}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$400(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/model/DeviceAuth;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/DeviceAuth;->getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-result-object v3

    iget-object v3, v3, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    iget-object v4, p0, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->this$0:Lcom/inrix/sdk/auth/InrixAuthenticator;

    # getter for: Lcom/inrix/sdk/auth/InrixAuthenticator;->authToken:Lcom/inrix/sdk/model/DeviceAuth;
    invoke-static {v4}, Lcom/inrix/sdk/auth/InrixAuthenticator;->access$400(Lcom/inrix/sdk/auth/InrixAuthenticator;)Lcom/inrix/sdk/model/DeviceAuth;

    move-result-object v4

    invoke-virtual {v4}, Lcom/inrix/sdk/model/DeviceAuth;->getExpirationDate()Ljava/util/Date;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/inrix/sdk/auth/InrixAuthenticator$IOnAuthCompletedListener;->onAuthCompleted(Ljava/lang/String;Ljava/util/Date;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 119
    check-cast p1, Lcom/inrix/sdk/model/DeviceAuth;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/auth/InrixAuthenticator$4;->onResponse(Lcom/inrix/sdk/model/DeviceAuth;)V

    return-void
.end method
