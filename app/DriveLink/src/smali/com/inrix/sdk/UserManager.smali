.class public Lcom/inrix/sdk/UserManager;
.super Ljava/lang/Object;
.source "UserManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/UserManager$LoginProcessor;,
        Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;,
        Lcom/inrix/sdk/UserManager$ChangePasswordOptions;,
        Lcom/inrix/sdk/UserManager$RegisterUserOptions;,
        Lcom/inrix/sdk/UserManager$ResetPasswordOptions;,
        Lcom/inrix/sdk/UserManager$IsUserAvailableOptions;,
        Lcom/inrix/sdk/UserManager$EmailOptions;
    }
.end annotation


# instance fields
.field private testRegisterCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    return-void
.end method

.method static synthetic access$202(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/network/request/UserRegisterRequest;)Lcom/inrix/sdk/network/request/UserRegisterRequest;
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/UserManager;
    .param p1, "x1"    # Lcom/inrix/sdk/network/request/UserRegisterRequest;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/inrix/sdk/UserManager;->testRegisterCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

    return-object p1
.end method


# virtual methods
.method public changePassword(Lcom/inrix/sdk/UserManager$ChangePasswordOptions;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 5
    .param p1, "params"    # Lcom/inrix/sdk/UserManager$ChangePasswordOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 313
    if-nez p2, :cond_0

    .line 314
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 317
    :cond_0
    if-nez p1, :cond_1

    .line 318
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 321
    :cond_1
    invoke-virtual {p0}, Lcom/inrix/sdk/UserManager;->isLoggedIn()Z

    move-result v1

    if-nez v1, :cond_2

    .line 322
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f7

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 325
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->getOldPassword()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 326
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f4

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 329
    :cond_3
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->getNewPassword()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 330
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f5

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 333
    :cond_4
    new-instance v0, Lcom/inrix/sdk/network/request/UserChangePasswordRequest;

    new-instance v1, Lcom/inrix/sdk/UserManager$5;

    invoke-direct {v1, p0, p2}, Lcom/inrix/sdk/UserManager$5;-><init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V

    new-instance v2, Lcom/inrix/sdk/UserManager$6;

    invoke-direct {v2, p0, p2}, Lcom/inrix/sdk/UserManager$6;-><init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V

    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->getOldPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$ChangePasswordOptions;->getNewPassword()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/inrix/sdk/network/request/UserChangePasswordRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    .local v0, "changePassword":Lcom/inrix/sdk/network/request/UserChangePasswordRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 354
    return-object v0
.end method

.method public getRegisterCall()Lcom/inrix/sdk/network/request/UserRegisterRequest;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/inrix/sdk/UserManager;->testRegisterCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

    return-object v0
.end method

.method public isLoggedIn()Z
    .locals 1

    .prologue
    .line 246
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->isLoggedIn()Z

    move-result v0

    return v0
.end method

.method public isUserAvailable(Lcom/inrix/sdk/UserManager$IsUserAvailableOptions;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 4
    .param p1, "params"    # Lcom/inrix/sdk/UserManager$IsUserAvailableOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 147
    if-nez p2, :cond_0

    .line 148
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 151
    :cond_0
    if-nez p1, :cond_1

    .line 152
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 155
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$IsUserAvailableOptions;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 156
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f3

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 159
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$IsUserAvailableOptions;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/utils/StringUtils;->isValidEmail(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 160
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f2

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 163
    :cond_3
    new-instance v0, Lcom/inrix/sdk/network/request/UserCheckRequest;

    new-instance v1, Lcom/inrix/sdk/UserManager$1;

    invoke-direct {v1, p0, p2}, Lcom/inrix/sdk/UserManager$1;-><init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V

    new-instance v2, Lcom/inrix/sdk/UserManager$2;

    invoke-direct {v2, p0, p2}, Lcom/inrix/sdk/UserManager$2;-><init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V

    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$IsUserAvailableOptions;->getEmail()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/network/request/UserCheckRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;)V

    .line 182
    .local v0, "checkIfUserExists":Lcom/inrix/sdk/network/request/UserCheckRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 183
    return-object v0
.end method

.method public logOut()Z
    .locals 1

    .prologue
    .line 233
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->clear()V

    .line 237
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public registerUser(Lcom/inrix/sdk/UserManager$RegisterUserOptions;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 3
    .param p1, "params"    # Lcom/inrix/sdk/UserManager$RegisterUserOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 198
    if-nez p2, :cond_0

    .line 199
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 202
    :cond_0
    if-nez p1, :cond_1

    .line 203
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 206
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 207
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f3

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 210
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/utils/StringUtils;->isValidEmail(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 211
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f2

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 214
    :cond_3
    invoke-virtual {p0}, Lcom/inrix/sdk/UserManager;->isLoggedIn()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 215
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f6

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 218
    :cond_4
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 219
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f4

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 222
    :cond_5
    new-instance v0, Lcom/inrix/sdk/UserManager$LoginProcessor;

    invoke-direct {v0, p0, p2}, Lcom/inrix/sdk/UserManager$LoginProcessor;-><init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V

    .line 223
    .local v0, "proccesor":Lcom/inrix/sdk/UserManager$LoginProcessor;
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/inrix/sdk/UserManager$LoginProcessor;->process(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    return-object v0
.end method

.method public resetPassword(Lcom/inrix/sdk/UserManager$ResetPasswordOptions;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 4
    .param p1, "params"    # Lcom/inrix/sdk/UserManager$ResetPasswordOptions;
    .param p2, "listener"    # Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 261
    if-nez p2, :cond_0

    .line 262
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 265
    :cond_0
    if-nez p1, :cond_1

    .line 266
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 269
    :cond_1
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$ResetPasswordOptions;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 270
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f3

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 273
    :cond_2
    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$ResetPasswordOptions;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/utils/StringUtils;->isValidEmail(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 274
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3f2

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 277
    :cond_3
    new-instance v0, Lcom/inrix/sdk/network/request/UserResetPasswordRequest;

    new-instance v1, Lcom/inrix/sdk/UserManager$3;

    invoke-direct {v1, p0, p2}, Lcom/inrix/sdk/UserManager$3;-><init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V

    new-instance v2, Lcom/inrix/sdk/UserManager$4;

    invoke-direct {v2, p0, p2}, Lcom/inrix/sdk/UserManager$4;-><init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V

    invoke-virtual {p1}, Lcom/inrix/sdk/UserManager$ResetPasswordOptions;->getEmail()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/network/request/UserResetPasswordRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;)V

    .line 296
    .local v0, "resetPasswordRequest":Lcom/inrix/sdk/network/request/UserResetPasswordRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 297
    return-object v0
.end method
