.class Lcom/inrix/sdk/GasStationManager$1;
.super Ljava/lang/Object;
.source "GasStationManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/GasStationManager;->getGasStationsInRadius(Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/GasStationCollection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/GasStationManager;

.field final synthetic val$listener:Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$1;->this$0:Lcom/inrix/sdk/GasStationManager;

    iput-object p2, p0, Lcom/inrix/sdk/GasStationManager$1;->val$listener:Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/GasStationCollection;)V
    .locals 1
    .param p1, "response"    # Lcom/inrix/sdk/model/GasStationCollection;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$1;->val$listener:Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;->onResult(Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 107
    check-cast p1, Lcom/inrix/sdk/model/GasStationCollection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/GasStationManager$1;->onResponse(Lcom/inrix/sdk/model/GasStationCollection;)V

    return-void
.end method
