.class public Lcom/inrix/sdk/UserManager$RegisterUserOptions;
.super Lcom/inrix/sdk/UserManager$EmailOptions;
.source "UserManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/UserManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RegisterUserOptions"
.end annotation


# instance fields
.field private password:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "email"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/inrix/sdk/UserManager$EmailOptions;-><init>(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->setPassword(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$RegisterUserOptions;

    .line 80
    return-void
.end method


# virtual methods
.method getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->password:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic setEmail(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$EmailOptions;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/inrix/sdk/UserManager$EmailOptions;->setEmail(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$EmailOptions;

    move-result-object v0

    return-object v0
.end method

.method public setPassword(Ljava/lang/String;)Lcom/inrix/sdk/UserManager$RegisterUserOptions;
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$RegisterUserOptions;->password:Ljava/lang/String;

    .line 88
    return-object p0
.end method
