.class public Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;
.super Lcom/inrix/sdk/GasStationManager$GasStationsOptions;
.source "GasStationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/GasStationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SingleGasStationOptions"
.end annotation


# instance fields
.field private gasStationID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GasStation;)V
    .locals 0
    .param p1, "gasStation"    # Lcom/inrix/sdk/model/GasStation;

    .prologue
    .line 728
    invoke-direct {p0}, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;-><init>()V

    .line 729
    invoke-direct {p0, p1}, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->initGasStationID(Lcom/inrix/sdk/model/GasStation;)V

    .line 730
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GasStation;II)V
    .locals 0
    .param p1, "gasStation"    # Lcom/inrix/sdk/model/GasStation;
    .param p2, "outputFields"    # I
    .param p3, "productTypes"    # I

    .prologue
    .line 733
    invoke-direct {p0, p2, p3}, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;-><init>(II)V

    .line 734
    invoke-direct {p0, p1}, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->initGasStationID(Lcom/inrix/sdk/model/GasStation;)V

    .line 735
    return-void
.end method

.method private initGasStationID(Lcom/inrix/sdk/model/GasStation;)V
    .locals 1
    .param p1, "gasStation"    # Lcom/inrix/sdk/model/GasStation;

    .prologue
    .line 742
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->gasStationID:Ljava/lang/String;

    .line 743
    if-eqz p1, :cond_0

    .line 744
    invoke-virtual {p1}, Lcom/inrix/sdk/model/GasStation;->getID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->gasStationID:Ljava/lang/String;

    .line 746
    :cond_0
    return-void
.end method


# virtual methods
.method public getStationID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->gasStationID:Ljava/lang/String;

    return-object v0
.end method

.method public isGasStationIDValid()Z
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;->gasStationID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
