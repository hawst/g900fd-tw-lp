.class public final Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;
.super Ljava/lang/Object;
.source "TrafficManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/TrafficManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TrafficQualityOptions"
.end annotation


# instance fields
.field private final center:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 2
    .param p1, "location"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 34
    return-void
.end method


# virtual methods
.method public final getLocation()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method
