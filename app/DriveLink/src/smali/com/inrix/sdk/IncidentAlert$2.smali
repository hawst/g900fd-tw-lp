.class Lcom/inrix/sdk/IncidentAlert$2;
.super Ljava/lang/Object;
.source "IncidentAlert.java"

# interfaces
.implements Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/IncidentAlert;->requestCongestions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/IncidentAlert;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/IncidentAlert;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/inrix/sdk/IncidentAlert$2;->this$0:Lcom/inrix/sdk/IncidentAlert;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/inrix/sdk/Error;)V
    .locals 1
    .param p1, "error"    # Lcom/inrix/sdk/Error;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert$2;->this$0:Lcom/inrix/sdk/IncidentAlert;

    invoke-virtual {v0, p1}, Lcom/inrix/sdk/IncidentAlert;->onError(Lcom/inrix/sdk/Error;)V

    .line 110
    return-void
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 86
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/IncidentAlert$2;->onResult(Ljava/util/List;)V

    return-void
.end method

.method public onResult(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert$2;->this$0:Lcom/inrix/sdk/IncidentAlert;

    # invokes: Lcom/inrix/sdk/IncidentAlert;->isFlowIncidentsPresent(Ljava/util/List;)Z
    invoke-static {v0, p1}, Lcom/inrix/sdk/IncidentAlert;->access$100(Lcom/inrix/sdk/IncidentAlert;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    const-string/jumbo v0, "No congestions around - returning empty list"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 94
    if-eqz p1, :cond_0

    .line 97
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert$2;->this$0:Lcom/inrix/sdk/IncidentAlert;

    invoke-virtual {v0, p1}, Lcom/inrix/sdk/IncidentAlert;->onResult(Ljava/util/List;)V

    .line 105
    :goto_0
    return-void

    .line 101
    :cond_1
    const-string/jumbo v0, "Congestions present - requesting all incidents"

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/inrix/sdk/IncidentAlert$2;->this$0:Lcom/inrix/sdk/IncidentAlert;

    # invokes: Lcom/inrix/sdk/IncidentAlert;->requestAllIncidents()V
    invoke-static {v0}, Lcom/inrix/sdk/IncidentAlert;->access$200(Lcom/inrix/sdk/IncidentAlert;)V

    goto :goto_0
.end method
