.class Lcom/inrix/sdk/ParkingManager$1;
.super Ljava/lang/Object;
.source "ParkingManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/ParkingManager;->getParkingLotsInRadius(Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/ParkingLotCollection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/ParkingManager;

.field final synthetic val$listener:Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$1;->this$0:Lcom/inrix/sdk/ParkingManager;

    iput-object p2, p0, Lcom/inrix/sdk/ParkingManager$1;->val$listener:Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/ParkingLotCollection;)V
    .locals 2
    .param p1, "response"    # Lcom/inrix/sdk/model/ParkingLotCollection;

    .prologue
    .line 523
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$1;->val$listener:Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;

    invoke-virtual {p1}, Lcom/inrix/sdk/model/ParkingLotCollection;->getParkingLots()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;->onResult(Ljava/lang/Object;)V

    .line 524
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 520
    check-cast p1, Lcom/inrix/sdk/model/ParkingLotCollection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/ParkingManager$1;->onResponse(Lcom/inrix/sdk/model/ParkingLotCollection;)V

    return-void
.end method
