.class Lcom/inrix/sdk/TrafficManager$1;
.super Ljava/lang/Object;
.source "TrafficManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/TrafficManager;->getTrafficQuality(Lcom/inrix/sdk/TrafficManager$TrafficQualityOptions;Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/TrafficQuality;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/TrafficManager;

.field final synthetic val$listener:Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/TrafficManager;Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/inrix/sdk/TrafficManager$1;->this$0:Lcom/inrix/sdk/TrafficManager;

    iput-object p2, p0, Lcom/inrix/sdk/TrafficManager$1;->val$listener:Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/TrafficQuality;)V
    .locals 1
    .param p1, "result"    # Lcom/inrix/sdk/model/TrafficQuality;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/inrix/sdk/TrafficManager$1;->val$listener:Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/TrafficManager$ITrafficQualityResponseListener;->onResult(Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 73
    check-cast p1, Lcom/inrix/sdk/model/TrafficQuality;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/TrafficManager$1;->onResponse(Lcom/inrix/sdk/model/TrafficQuality;)V

    return-void
.end method
