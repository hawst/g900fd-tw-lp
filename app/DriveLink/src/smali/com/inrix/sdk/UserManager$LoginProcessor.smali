.class Lcom/inrix/sdk/UserManager$LoginProcessor;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/inrix/sdk/ICancellable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/UserManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginProcessor"
.end annotation


# instance fields
.field private authRequest:Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;

.field private final callback:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

.field private registerCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

.field final synthetic this$0:Lcom/inrix/sdk/UserManager;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;)V
    .locals 0
    .param p2, "loginCallback"    # Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    .prologue
    .line 372
    iput-object p1, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->this$0:Lcom/inrix/sdk/UserManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    iput-object p2, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->callback:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    .line 374
    return-void
.end method

.method static synthetic access$000(Lcom/inrix/sdk/UserManager$LoginProcessor;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/UserManager$LoginProcessor;

    .prologue
    .line 360
    invoke-direct {p0}, Lcom/inrix/sdk/UserManager$LoginProcessor;->auth()V

    return-void
.end method

.method static synthetic access$100(Lcom/inrix/sdk/UserManager$LoginProcessor;)Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/UserManager$LoginProcessor;

    .prologue
    .line 360
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->callback:Lcom/inrix/sdk/UserManager$ILoginOperationResponseListener;

    return-object v0
.end method

.method private auth()V
    .locals 3

    .prologue
    .line 417
    new-instance v0, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;

    new-instance v1, Lcom/inrix/sdk/UserManager$LoginProcessor$3;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/UserManager$LoginProcessor$3;-><init>(Lcom/inrix/sdk/UserManager$LoginProcessor;)V

    new-instance v2, Lcom/inrix/sdk/UserManager$LoginProcessor$4;

    invoke-direct {v2, p0}, Lcom/inrix/sdk/UserManager$LoginProcessor$4;-><init>(Lcom/inrix/sdk/UserManager$LoginProcessor;)V

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    iput-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->authRequest:Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;

    .line 457
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->authRequest:Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 458
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->registerCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->registerCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/UserRegisterRequest;->cancel()V

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->authRequest:Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;

    if-eqz v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->authRequest:Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;

    invoke-virtual {v0}, Lcom/inrix/sdk/network/request/internal/DeviceAuthRequest;->cancel()V

    .line 470
    :cond_1
    return-void
.end method

.method public process(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "email"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 385
    new-instance v0, Lcom/inrix/sdk/network/request/UserRegisterRequest;

    new-instance v1, Lcom/inrix/sdk/UserManager$LoginProcessor$1;

    invoke-direct {v1, p0}, Lcom/inrix/sdk/UserManager$LoginProcessor$1;-><init>(Lcom/inrix/sdk/UserManager$LoginProcessor;)V

    new-instance v2, Lcom/inrix/sdk/UserManager$LoginProcessor$2;

    invoke-direct {v2, p0}, Lcom/inrix/sdk/UserManager$LoginProcessor$2;-><init>(Lcom/inrix/sdk/UserManager$LoginProcessor;)V

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/inrix/sdk/network/request/UserRegisterRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->registerCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

    .line 409
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->registerCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

    invoke-virtual {v0, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 410
    iget-object v0, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->this$0:Lcom/inrix/sdk/UserManager;

    iget-object v1, p0, Lcom/inrix/sdk/UserManager$LoginProcessor;->registerCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;

    # setter for: Lcom/inrix/sdk/UserManager;->testRegisterCall:Lcom/inrix/sdk/network/request/UserRegisterRequest;
    invoke-static {v0, v1}, Lcom/inrix/sdk/UserManager;->access$202(Lcom/inrix/sdk/UserManager;Lcom/inrix/sdk/network/request/UserRegisterRequest;)Lcom/inrix/sdk/network/request/UserRegisterRequest;

    .line 411
    return-void
.end method
