.class public Lcom/inrix/sdk/GasStationManager$GasStationsOptions;
.super Ljava/lang/Object;
.source "GasStationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/GasStationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GasStationsOptions"
.end annotation


# static fields
.field public static final OUTPUT_FIELDS_ADDRESS:I = 0x4

.field public static final OUTPUT_FIELDS_ALL:I = 0xffff

.field public static final OUTPUT_FIELDS_BRAND:I = 0x1

.field public static final OUTPUT_FIELDS_CURRENCY_CODE:I = 0x10

.field public static final OUTPUT_FIELDS_LOCATION:I = 0x2

.field public static final OUTPUT_FIELDS_PRODUCTS:I = 0x8

.field private static final OUTPUT_FIELD_STRING_ADDRESS:Ljava/lang/String; = "Address"

.field private static final OUTPUT_FIELD_STRING_ALL:Ljava/lang/String; = "All"

.field private static final OUTPUT_FIELD_STRING_BRAND:Ljava/lang/String; = "Brand"

.field private static final OUTPUT_FIELD_STRING_CURRENCY_CODE:Ljava/lang/String; = "CurrencyCode"

.field private static final OUTPUT_FIELD_STRING_LOCATION:Ljava/lang/String; = "LatLong"

.field private static final OUTPUT_FIELD_STRING_PRODUCTS:Ljava/lang/String; = "Products"

.field public static final PRODUCT_TYPE_ALL:I = 0xffff

.field public static final PRODUCT_TYPE_BIODIESEL:I = 0x1

.field public static final PRODUCT_TYPE_DIESEL:I = 0x2

.field public static final PRODUCT_TYPE_DIESEL_PLUS:I = 0x4

.field public static final PRODUCT_TYPE_DIESEL_TRUCK:I = 0x8

.field public static final PRODUCT_TYPE_GASOLINE_E85:I = 0x200

.field public static final PRODUCT_TYPE_GASOLINE_MIDGRADE:I = 0x80

.field public static final PRODUCT_TYPE_GASOLINE_NORMAL:I = 0x400

.field public static final PRODUCT_TYPE_GASOLINE_PREMIUM:I = 0x100

.field public static final PRODUCT_TYPE_GASOLINE_REGULAR:I = 0x40

.field public static final PRODUCT_TYPE_GASOLINE_SP92:I = 0x800

.field public static final PRODUCT_TYPE_GASOLINE_SP95:I = 0x1000

.field public static final PRODUCT_TYPE_GASOLINE_SP95_E10:I = 0x2000

.field public static final PRODUCT_TYPE_GASOLINE_SP98:I = 0x4000

.field public static final PRODUCT_TYPE_LPG:I = 0x10

.field public static final PRODUCT_TYPE_METHANE:I = 0x20

.field private static final PRODUCT_TYPE_STRING_BIODIESEL:Ljava/lang/String; = "biodiesel"

.field private static final PRODUCT_TYPE_STRING_DIESEL:Ljava/lang/String; = "Diesel"

.field private static final PRODUCT_TYPE_STRING_DIESEL_PLUS:Ljava/lang/String; = "Dieselplus"

.field private static final PRODUCT_TYPE_STRING_DIESEL_TRUCK:Ljava/lang/String; = "truckdiesel"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_E85:Ljava/lang/String; = "E85"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_MIDGRADE:Ljava/lang/String; = "MidGrade"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_NORMAL:Ljava/lang/String; = "normal"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_PREMIUM:Ljava/lang/String; = "Premium"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_REGULAR:Ljava/lang/String; = "Regular"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_SP92:Ljava/lang/String; = "SP92"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_SP95:Ljava/lang/String; = "SP95"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_SP95_E10:Ljava/lang/String; = "SP95-E10"

.field private static final PRODUCT_TYPE_STRING_GASOLINE_SP98:Ljava/lang/String; = "SP98"

.field private static final PRODUCT_TYPE_STRING_LPG:Ljava/lang/String; = "LPG"

.field private static final PRODUCT_TYPE_STRING_METHANE:Ljava/lang/String; = "methane"


# instance fields
.field private outputFields:I

.field private productTypes:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const v0, 0xffff

    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    iput v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    .line 306
    iput v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    .line 307
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "outputFields"    # I
    .param p2, "productTypes"    # I

    .prologue
    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    iput p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    .line 316
    iput p2, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    .line 317
    return-void
.end method


# virtual methods
.method public getOutputFields()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    return v0
.end method

.method public getOutputFieldsString()Ljava/lang/String;
    .locals 3

    .prologue
    const v2, 0xffff

    .line 357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 360
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    and-int/2addr v1, v2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    if-nez v1, :cond_1

    .line 362
    :cond_0
    const-string/jumbo v1, "All"

    .line 385
    :goto_0
    return-object v1

    .line 365
    :cond_1
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 366
    const-string/jumbo v1, "Brand"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    :cond_2
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 370
    const-string/jumbo v1, "LatLong"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    :cond_3
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 374
    const-string/jumbo v1, "Address"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    :cond_4
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 378
    const-string/jumbo v1, "Products"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    :cond_5
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 382
    const-string/jumbo v1, "CurrencyCode"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    :cond_6
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getProductTypes()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    return v0
.end method

.method public getProductTypesString()Ljava/lang/String;
    .locals 3

    .prologue
    const v2, 0xffff

    .line 393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 396
    .local v0, "productTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/2addr v1, v2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    if-nez v1, :cond_1

    .line 398
    :cond_0
    const/4 v1, 0x0

    .line 446
    :goto_0
    return-object v1

    .line 400
    :cond_1
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 401
    const-string/jumbo v1, "biodiesel"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 403
    :cond_2
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 404
    const-string/jumbo v1, "Diesel"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 406
    :cond_3
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 407
    const-string/jumbo v1, "Dieselplus"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_4
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 410
    const-string/jumbo v1, "truckdiesel"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    :cond_5
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 413
    const-string/jumbo v1, "LPG"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_6
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 416
    const-string/jumbo v1, "methane"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    :cond_7
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_8

    .line 419
    const-string/jumbo v1, "Regular"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    :cond_8
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_9

    .line 422
    const-string/jumbo v1, "MidGrade"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    :cond_9
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_a

    .line 425
    const-string/jumbo v1, "Premium"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    :cond_a
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_b

    .line 428
    const-string/jumbo v1, "E85"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_b
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_c

    .line 431
    const-string/jumbo v1, "normal"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    :cond_c
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_d

    .line 434
    const-string/jumbo v1, "SP92"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 436
    :cond_d
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_e

    .line 437
    const-string/jumbo v1, "SP95"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 439
    :cond_e
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_f

    .line 440
    const-string/jumbo v1, "SP95-E10"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_f
    iget v1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_10

    .line 443
    const-string/jumbo v1, "SP98"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    :cond_10
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public setOutputFields(I)V
    .locals 0
    .param p1, "outputFields"    # I

    .prologue
    .line 324
    iput p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->outputFields:I

    .line 325
    return-void
.end method

.method public setProductTypes(I)V
    .locals 0
    .param p1, "productTypes"    # I

    .prologue
    .line 340
    iput p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;->productTypes:I

    .line 341
    return-void
.end method
