.class Lcom/inrix/sdk/GasStationManager$2;
.super Ljava/lang/Object;
.source "GasStationManager.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/GasStationManager;->getGasStationsInRadius(Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/GasStationManager;

.field final synthetic val$listener:Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$2;->this$0:Lcom/inrix/sdk/GasStationManager;

    iput-object p2, p0, Lcom/inrix/sdk/GasStationManager$2;->val$listener:Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$2;->val$listener:Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;

    new-instance v1, Lcom/inrix/sdk/Error;

    invoke-direct {v1, p1}, Lcom/inrix/sdk/Error;-><init>(Lcom/android/volley/VolleyError;)V

    invoke-interface {v0, v1}, Lcom/inrix/sdk/GasStationManager$IGasStationResponseListener;->onError(Lcom/inrix/sdk/Error;)V

    .line 119
    return-void
.end method
