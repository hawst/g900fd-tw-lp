.class public Lcom/inrix/sdk/parser/json/JSONEntityBase;
.super Ljava/lang/Object;
.source "JSONEntityBase.java"


# instance fields
.field public statusId:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "StatusId"
    .end annotation
.end field

.field public statusText:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "StatusText"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/inrix/sdk/parser/json/JSONEntityBase;->statusId:I

    .line 10
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/parser/json/JSONEntityBase;->statusText:Ljava/lang/String;

    return-void
.end method
