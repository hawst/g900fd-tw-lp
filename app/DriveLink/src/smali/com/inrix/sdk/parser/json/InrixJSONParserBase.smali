.class public Lcom/inrix/sdk/parser/json/InrixJSONParserBase;
.super Ljava/lang/Object;
.source "InrixJSONParserBase.java"

# interfaces
.implements Lcom/inrix/sdk/parser/INetworkResponseParser;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/inrix/sdk/parser/json/JSONEntityBase;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/parser/INetworkResponseParser",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected entity:Lcom/inrix/sdk/parser/json/JSONEntityBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final gson:Lcom/google/gson/Gson;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/inrix/sdk/parser/json/InrixJSONParserBase;, "Lcom/inrix/sdk/parser/json/InrixJSONParserBase<TT;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->gson:Lcom/google/gson/Gson;

    .line 18
    iput-object p1, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->clazz:Ljava/lang/Class;

    .line 19
    return-void
.end method


# virtual methods
.method public parse(Lcom/android/volley/NetworkResponse;)Lcom/inrix/sdk/parser/json/JSONEntityBase;
    .locals 5
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/inrix/sdk/parser/json/InrixJSONParserBase;, "Lcom/inrix/sdk/parser/json/InrixJSONParserBase<TT;>;"
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->data:[B

    iget-object v3, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    invoke-static {v3}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCharset(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 28
    .local v1, "json":Ljava/lang/String;
    iget-object v2, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->gson:Lcom/google/gson/Gson;

    iget-object v3, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->clazz:Ljava/lang/Class;

    invoke-virtual {v2, v1, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/inrix/sdk/parser/json/JSONEntityBase;

    iput-object v2, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->entity:Lcom/inrix/sdk/parser/json/JSONEntityBase;

    .line 30
    iget-object v2, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->entity:Lcom/inrix/sdk/parser/json/JSONEntityBase;

    iget v2, v2, Lcom/inrix/sdk/parser/json/JSONEntityBase;->statusId:I

    if-eqz v2, :cond_1

    .line 31
    iget-object v2, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->entity:Lcom/inrix/sdk/parser/json/JSONEntityBase;

    iget v2, v2, Lcom/inrix/sdk/parser/json/JSONEntityBase;->statusId:I

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_0

    .line 32
    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, p1}, Lcom/android/volley/ParseError;-><init>(Lcom/android/volley/NetworkResponse;)V

    throw v2
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 38
    .end local v1    # "json":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Lcom/android/volley/VolleyError;
    throw v0

    .line 34
    .end local v0    # "e":Lcom/android/volley/VolleyError;
    .restart local v1    # "json":Ljava/lang/String;
    :cond_0
    :try_start_1
    new-instance v2, Lcom/inrix/sdk/network/response/InrixError;

    iget-object v3, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->entity:Lcom/inrix/sdk/parser/json/JSONEntityBase;

    iget v3, v3, Lcom/inrix/sdk/parser/json/JSONEntityBase;->statusId:I

    iget-object v4, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->entity:Lcom/inrix/sdk/parser/json/JSONEntityBase;

    iget-object v4, v4, Lcom/inrix/sdk/parser/json/JSONEntityBase;->statusText:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/inrix/sdk/network/response/InrixError;-><init>(ILjava/lang/String;)V

    throw v2
    :try_end_1
    .catch Lcom/android/volley/VolleyError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 40
    .end local v1    # "json":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, v0}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 37
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "json":Ljava/lang/String;
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->entity:Lcom/inrix/sdk/parser/json/JSONEntityBase;
    :try_end_2
    .catch Lcom/android/volley/VolleyError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object v2
.end method

.method public bridge synthetic parse(Lcom/android/volley/NetworkResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .prologue
    .line 11
    .local p0, "this":Lcom/inrix/sdk/parser/json/InrixJSONParserBase;, "Lcom/inrix/sdk/parser/json/InrixJSONParserBase<TT;>;"
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/parser/json/InrixJSONParserBase;->parse(Lcom/android/volley/NetworkResponse;)Lcom/inrix/sdk/parser/json/JSONEntityBase;

    move-result-object v0

    return-object v0
.end method
