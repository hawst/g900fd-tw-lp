.class public abstract Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;
.super Ljava/lang/Object;
.source "InrixXMLParserBase.java"

# interfaces
.implements Lcom/inrix/sdk/parser/INetworkResponseParser;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/parser/INetworkResponseParser",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final ATTR_STATUS_ID:Ljava/lang/String; = "statusId"

.field private static final ATTR_STATUS_TEXT:Ljava/lang/String; = "statusText"

.field protected static final ELEM_INRIX:Ljava/lang/String; = "Inrix"


# instance fields
.field private statusId:I

.field private statusText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLParserBase<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusId:I

    .line 28
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getStatusId()I
    .locals 1

    .prologue
    .line 79
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLParserBase<TT;>;"
    iget v0, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusId:I

    return v0
.end method

.method public getStatusText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLParserBase<TT;>;"
    iget-object v0, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusText:Ljava/lang/String;

    return-object v0
.end method

.method public parse(Lcom/android/volley/NetworkResponse;)Ljava/lang/Object;
    .locals 5
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLParserBase<TT;>;"
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->parseStatus(Lcom/android/volley/NetworkResponse;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 34
    .local v1, "root":Lorg/w3c/dom/Node;
    iget v2, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusId:I

    if-eqz v2, :cond_1

    .line 35
    iget v2, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusId:I

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_0

    .line 36
    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, p1}, Lcom/android/volley/ParseError;-><init>(Lcom/android/volley/NetworkResponse;)V

    throw v2
    :try_end_0
    .catch Lcom/inrix/sdk/network/response/InrixError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 43
    .end local v1    # "root":Lorg/w3c/dom/Node;
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Lcom/inrix/sdk/network/response/InrixError;
    throw v0

    .line 38
    .end local v0    # "e":Lcom/inrix/sdk/network/response/InrixError;
    .restart local v1    # "root":Lorg/w3c/dom/Node;
    :cond_0
    :try_start_1
    new-instance v2, Lcom/inrix/sdk/network/response/InrixError;

    invoke-virtual {p0}, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->getStatusId()I

    move-result v3

    invoke-virtual {p0}, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->getStatusText()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/inrix/sdk/network/response/InrixError;-><init>(ILjava/lang/String;)V

    throw v2
    :try_end_1
    .catch Lcom/inrix/sdk/network/response/InrixError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 45
    .end local v1    # "root":Lorg/w3c/dom/Node;
    :catch_1
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, v0}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 42
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "root":Lorg/w3c/dom/Node;
    :cond_1
    :try_start_2
    invoke-virtual {p0, v1}, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->parseEntity(Lorg/w3c/dom/Node;)Ljava/lang/Object;
    :try_end_2
    .catch Lcom/inrix/sdk/network/response/InrixError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    return-object v2
.end method

.method protected abstract parseEntity(Lorg/w3c/dom/Node;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Node;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected parseStatus(Lcom/android/volley/NetworkResponse;)Lorg/w3c/dom/Node;
    .locals 9
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLParserBase<TT;>;"
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 55
    .local v1, "dbfac":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v2, 0x0

    .line 56
    .local v2, "doc":Lorg/w3c/dom/Document;
    const/4 v5, 0x0

    .line 57
    .local v5, "root":Lorg/w3c/dom/Node;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v3

    .line 58
    .local v3, "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v6, Ljava/lang/String;

    iget-object v7, p1, Lcom/android/volley/NetworkResponse;->data:[B

    iget-object v8, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    invoke-static {v8}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCharset(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 60
    .local v6, "xml":Ljava/lang/String;
    new-instance v7, Lorg/xml/sax/InputSource;

    new-instance v8, Ljava/io/StringReader;

    invoke-direct {v8, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v3, v7}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 61
    invoke-interface {v2}, Lorg/w3c/dom/Document;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v5

    .line 63
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    .line 65
    .local v0, "attrs":Lorg/w3c/dom/NamedNodeMap;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v0}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 66
    invoke-interface {v0, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "statusId"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 67
    invoke-interface {v0, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusId:I

    .line 70
    :cond_0
    invoke-interface {v0, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "statusText"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 71
    invoke-interface {v0, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/inrix/sdk/parser/xml/InrixXMLParserBase;->statusText:Ljava/lang/String;

    .line 65
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 75
    :cond_2
    return-object v5
.end method
