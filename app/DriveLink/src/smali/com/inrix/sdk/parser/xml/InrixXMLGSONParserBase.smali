.class public Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;
.super Ljava/lang/Object;
.source "InrixXMLGSONParserBase.java"

# interfaces
.implements Lcom/inrix/sdk/parser/INetworkResponseParser;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/inrix/sdk/parser/xml/XMLEntityBase;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/parser/INetworkResponseParser",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected entity:Lcom/inrix/sdk/parser/xml/XMLEntityBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase<TT;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;->clazz:Ljava/lang/Class;

    .line 22
    return-void
.end method


# virtual methods
.method public getSerializationStrategy()Lorg/simpleframework/xml/strategy/Strategy;
    .locals 1

    .prologue
    .line 56
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public parse(Lcom/android/volley/NetworkResponse;)Lcom/inrix/sdk/parser/xml/XMLEntityBase;
    .locals 7
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase<TT;>;"
    :try_start_0
    new-instance v3, Ljava/lang/String;

    iget-object v4, p1, Lcom/android/volley/NetworkResponse;->data:[B

    iget-object v5, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    invoke-static {v5}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCharset(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 31
    .local v3, "xml":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;->getSerializationStrategy()Lorg/simpleframework/xml/strategy/Strategy;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v2, Lorg/simpleframework/xml/core/Persister;

    invoke-direct {v2}, Lorg/simpleframework/xml/core/Persister;-><init>()V

    .line 33
    .local v2, "serializer":Lorg/simpleframework/xml/Serializer;
    :goto_0
    iget-object v4, p0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;->clazz:Ljava/lang/Class;

    invoke-interface {v2, v4, v3}, Lorg/simpleframework/xml/Serializer;->read(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/inrix/sdk/parser/xml/XMLEntityBase;

    .line 35
    .local v1, "entity":Lcom/inrix/sdk/parser/xml/XMLEntityBase;, "TT;"
    invoke-virtual {v1}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->getStatusId()I

    move-result v4

    if-eqz v4, :cond_2

    .line 36
    invoke-virtual {v1}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->getStatusId()I

    move-result v4

    const/high16 v5, -0x80000000

    if-ne v4, v5, :cond_1

    .line 37
    new-instance v4, Lcom/android/volley/ParseError;

    invoke-direct {v4, p1}, Lcom/android/volley/ParseError;-><init>(Lcom/android/volley/NetworkResponse;)V

    throw v4
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 44
    .end local v1    # "entity":Lcom/inrix/sdk/parser/xml/XMLEntityBase;, "TT;"
    .end local v2    # "serializer":Lorg/simpleframework/xml/Serializer;
    .end local v3    # "xml":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Lcom/android/volley/VolleyError;
    throw v0

    .line 31
    .end local v0    # "e":Lcom/android/volley/VolleyError;
    .restart local v3    # "xml":Ljava/lang/String;
    :cond_0
    :try_start_1
    new-instance v2, Lorg/simpleframework/xml/core/Persister;

    invoke-virtual {p0}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;->getSerializationStrategy()Lorg/simpleframework/xml/strategy/Strategy;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/simpleframework/xml/core/Persister;-><init>(Lorg/simpleframework/xml/strategy/Strategy;)V
    :try_end_1
    .catch Lcom/android/volley/VolleyError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 46
    .end local v3    # "xml":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/android/volley/ParseError;

    invoke-direct {v4, v0}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 39
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entity":Lcom/inrix/sdk/parser/xml/XMLEntityBase;, "TT;"
    .restart local v2    # "serializer":Lorg/simpleframework/xml/Serializer;
    .restart local v3    # "xml":Ljava/lang/String;
    :cond_1
    :try_start_2
    new-instance v4, Lcom/inrix/sdk/network/response/InrixError;

    invoke-virtual {v1}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->getStatusId()I

    move-result v5

    invoke-virtual {v1}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->getStatusText()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/inrix/sdk/network/response/InrixError;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_2
    .catch Lcom/android/volley/VolleyError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 43
    :cond_2
    return-object v1
.end method

.method public bridge synthetic parse(Lcom/android/volley/NetworkResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;, "Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase<TT;>;"
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;->parse(Lcom/android/volley/NetworkResponse;)Lcom/inrix/sdk/parser/xml/XMLEntityBase;

    move-result-object v0

    return-object v0
.end method
