.class public Lcom/inrix/sdk/parser/xml/XMLEntityBase;
.super Ljava/lang/Object;
.source "XMLEntityBase.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private statusId:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private statusText:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->statusId:I

    .line 12
    const-string/jumbo v0, "Default Status"

    iput-object v0, p0, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->statusText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getStatusId()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->statusId:I

    return v0
.end method

.method public getStatusText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/inrix/sdk/parser/xml/XMLEntityBase;->statusText:Ljava/lang/String;

    return-object v0
.end method
