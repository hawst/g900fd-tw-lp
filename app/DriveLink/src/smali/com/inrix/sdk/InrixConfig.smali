.class public Lcom/inrix/sdk/InrixConfig;
.super Lcom/inrix/sdk/Settings;
.source "InrixConfig.java"


# instance fields
.field private baseUrl:Ljava/lang/String;
    .annotation runtime Lcom/inrix/sdk/PropertyName;
        name = "baseURL"
    .end annotation
.end field

.field private logLevel:I
    .annotation runtime Lcom/inrix/sdk/PropertyName;
        name = "logLevel"
    .end annotation
.end field

.field private vendorId:Ljava/lang/String;
    .annotation runtime Lcom/inrix/sdk/PropertyName;
        name = "vendorId"
    .end annotation
.end field

.field private vendorToken:Ljava/lang/String;
    .annotation runtime Lcom/inrix/sdk/PropertyName;
        name = "vendorToken"
    .end annotation
.end field

.field private version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/inrix/sdk/Settings;-><init>()V

    .line 20
    const-string/jumbo v0, "http://api.mobile.inrix.com/MobileGateway/mobile.ashx"

    iput-object v0, p0, Lcom/inrix/sdk/InrixConfig;->baseUrl:Ljava/lang/String;

    .line 23
    const/4 v0, 0x6

    iput v0, p0, Lcom/inrix/sdk/InrixConfig;->logLevel:I

    .line 26
    const-string/jumbo v0, "android_sdk_v1"

    iput-object v0, p0, Lcom/inrix/sdk/InrixConfig;->version:Ljava/lang/String;

    return-void
.end method

.method public static loadDefaultOptions(Landroid/content/Context;)Lcom/inrix/sdk/InrixConfig;
    .locals 1
    .param p0, "paramContext"    # Landroid/content/Context;

    .prologue
    .line 36
    new-instance v0, Lcom/inrix/sdk/InrixConfig;

    invoke-direct {v0}, Lcom/inrix/sdk/InrixConfig;-><init>()V

    .line 37
    .local v0, "localAirshipConfigOptions":Lcom/inrix/sdk/InrixConfig;
    invoke-virtual {v0, p0}, Lcom/inrix/sdk/InrixConfig;->loadFromProperties(Landroid/content/Context;)V

    .line 38
    return-object v0
.end method

.method public static loadOptions(Landroid/content/Context;Ljava/lang/String;)Lcom/inrix/sdk/InrixConfig;
    .locals 3
    .param p0, "paramContext"    # Landroid/content/Context;
    .param p1, "configFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 57
    if-nez p0, :cond_0

    .line 58
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x414

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 61
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x413

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 64
    :cond_1
    new-instance v0, Lcom/inrix/sdk/InrixConfig;

    invoke-direct {v0}, Lcom/inrix/sdk/InrixConfig;-><init>()V

    .line 65
    .local v0, "configOptions":Lcom/inrix/sdk/InrixConfig;
    invoke-virtual {v0, p0, p1}, Lcom/inrix/sdk/InrixConfig;->loadFromProperties(Landroid/content/Context;Ljava/lang/String;)V

    .line 66
    return-object v0
.end method


# virtual methods
.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/inrix/sdk/InrixConfig;->baseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultPropertiesFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string/jumbo v0, "inrixconfig.properties"

    return-object v0
.end method

.method public getLogLevel()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/inrix/sdk/InrixConfig;->logLevel:I

    return v0
.end method

.method public getVendorId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/inrix/sdk/InrixConfig;->vendorId:Ljava/lang/String;

    return-object v0
.end method

.method public getVendorToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/inrix/sdk/InrixConfig;->vendorToken:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/inrix/sdk/InrixConfig;->version:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/inrix/sdk/InrixConfig;->vendorId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/InrixConfig;->vendorToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/InrixConfig;->baseUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic loadFromProperties(Landroid/content/Context;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/inrix/sdk/Settings;->loadFromProperties(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic loadFromProperties(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-super {p0, p1, p2}, Lcom/inrix/sdk/Settings;->loadFromProperties(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public setBaseUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/inrix/sdk/InrixConfig;->baseUrl:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setVendorId(Ljava/lang/String;)V
    .locals 0
    .param p1, "localVendorId"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/inrix/sdk/InrixConfig;->vendorId:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setVendorToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "localVendorToken"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/inrix/sdk/InrixConfig;->vendorToken:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "localVersion"    # Ljava/lang/String;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/inrix/sdk/InrixConfig;->version:Ljava/lang/String;

    .line 168
    return-void
.end method
