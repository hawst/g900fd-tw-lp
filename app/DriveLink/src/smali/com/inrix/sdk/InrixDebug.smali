.class public Lcom/inrix/sdk/InrixDebug;
.super Ljava/lang/Object;
.source "InrixDebug.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/InrixDebug$1;,
        Lcom/inrix/sdk/InrixDebug$LogEntry;,
        Lcom/inrix/sdk/InrixDebug$LogType;
    }
.end annotation


# static fields
.field static currentLoglevel:Lcom/inrix/sdk/InrixDebug$LogType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method public static LogD(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 82
    const/4 v0, 0x0

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Debug:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {p0, v0, v1}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 83
    return-void
.end method

.method public static LogD(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 94
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Debug:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {p0, p1, v0}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 95
    return-void
.end method

.method public static LogE(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x0

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {p0, v0, v1}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 105
    return-void
.end method

.method public static LogE(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 116
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {p0, p1, v0}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 117
    return-void
.end method

.method public static LogE(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 126
    const-string/jumbo v0, "[Error]"

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {v0, p0, v1}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 127
    return-void
.end method

.method private static LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V
    .locals 5
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "exception"    # Ljava/lang/Throwable;
    .param p2, "type"    # Lcom/inrix/sdk/InrixDebug$LogType;

    .prologue
    const/16 v4, 0xa

    .line 190
    invoke-static {p2}, Lcom/inrix/sdk/InrixDebug;->canBeLogged(Lcom/inrix/sdk/InrixDebug$LogType;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 222
    :goto_0
    return-void

    .line 194
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 195
    .local v2, "logMsgBuilder":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_2

    .line 196
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 197
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 206
    .local v1, "logMsg":Ljava/lang/String;
    invoke-static {v1, p2}, Lcom/inrix/sdk/InrixDebug;->generateLogEntity(Ljava/lang/String;Lcom/inrix/sdk/InrixDebug$LogType;)Lcom/inrix/sdk/InrixDebug$LogEntry;

    move-result-object v0

    .line 208
    .local v0, "log":Lcom/inrix/sdk/InrixDebug$LogEntry;
    sget-object v3, Lcom/inrix/sdk/InrixDebug$1;->$SwitchMap$com$inrix$sdk$InrixDebug$LogType:[I

    invoke-virtual {p2}, Lcom/inrix/sdk/InrixDebug$LogType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 210
    :pswitch_0
    iget-object v3, v0, Lcom/inrix/sdk/InrixDebug$LogEntry;->fileName:Ljava/lang/String;

    iget-object v4, v0, Lcom/inrix/sdk/InrixDebug$LogEntry;->verboseMsg:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 213
    :pswitch_1
    iget-object v3, v0, Lcom/inrix/sdk/InrixDebug$LogEntry;->fileName:Ljava/lang/String;

    iget-object v4, v0, Lcom/inrix/sdk/InrixDebug$LogEntry;->verboseMsg:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 216
    :pswitch_2
    iget-object v3, v0, Lcom/inrix/sdk/InrixDebug$LogEntry;->fileName:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 219
    :pswitch_3
    iget-object v3, v0, Lcom/inrix/sdk/InrixDebug$LogEntry;->fileName:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 208
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static LogV(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 158
    const/4 v0, 0x0

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {p0, v0, v1}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 159
    return-void
.end method

.method public static varargs LogV([Ljava/lang/Object;)V
    .locals 5
    .param p0, "objs"    # [Ljava/lang/Object;

    .prologue
    .line 168
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 170
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 172
    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 176
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/inrix/sdk/InrixDebug$LogType;->Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {v2, v3, v4}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 177
    return-void
.end method

.method public static LogW(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 136
    const/4 v0, 0x0

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Warning:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {p0, v0, v1}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 137
    return-void
.end method

.method public static LogW(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 148
    sget-object v0, Lcom/inrix/sdk/InrixDebug$LogType;->Warning:Lcom/inrix/sdk/InrixDebug$LogType;

    invoke-static {p0, p1, v0}, Lcom/inrix/sdk/InrixDebug;->LogEvent(Ljava/lang/String;Ljava/lang/Throwable;Lcom/inrix/sdk/InrixDebug$LogType;)V

    .line 149
    return-void
.end method

.method private static canBeLogged(Lcom/inrix/sdk/InrixDebug$LogType;)Z
    .locals 3
    .param p0, "type"    # Lcom/inrix/sdk/InrixDebug$LogType;

    .prologue
    const/4 v0, 0x1

    .line 262
    sget-object v1, Lcom/inrix/sdk/InrixDebug;->currentLoglevel:Lcom/inrix/sdk/InrixDebug$LogType;

    sget-object v2, Lcom/inrix/sdk/InrixDebug$LogType;->Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

    if-ne v1, v2, :cond_1

    .line 279
    :cond_0
    :goto_0
    return v0

    .line 266
    :cond_1
    sget-object v1, Lcom/inrix/sdk/InrixDebug;->currentLoglevel:Lcom/inrix/sdk/InrixDebug$LogType;

    sget-object v2, Lcom/inrix/sdk/InrixDebug$LogType;->Debug:Lcom/inrix/sdk/InrixDebug$LogType;

    if-ne v1, v2, :cond_2

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Verbose:Lcom/inrix/sdk/InrixDebug$LogType;

    if-ne p0, v1, :cond_0

    .line 270
    :cond_2
    sget-object v1, Lcom/inrix/sdk/InrixDebug;->currentLoglevel:Lcom/inrix/sdk/InrixDebug$LogType;

    sget-object v2, Lcom/inrix/sdk/InrixDebug$LogType;->Warning:Lcom/inrix/sdk/InrixDebug$LogType;

    if-ne v1, v2, :cond_3

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Warning:Lcom/inrix/sdk/InrixDebug$LogType;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    if-eq p0, v1, :cond_0

    .line 275
    :cond_3
    sget-object v1, Lcom/inrix/sdk/InrixDebug;->currentLoglevel:Lcom/inrix/sdk/InrixDebug$LogType;

    sget-object v2, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    if-ne v1, v2, :cond_4

    sget-object v1, Lcom/inrix/sdk/InrixDebug$LogType;->Error:Lcom/inrix/sdk/InrixDebug$LogType;

    if-eq p0, v1, :cond_0

    .line 279
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static generateLogEntity(Ljava/lang/String;Lcom/inrix/sdk/InrixDebug$LogType;)Lcom/inrix/sdk/InrixDebug$LogEntry;
    .locals 7
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "type"    # Lcom/inrix/sdk/InrixDebug$LogType;

    .prologue
    const/4 v6, 0x0

    .line 235
    new-instance v1, Lcom/inrix/sdk/InrixDebug$LogEntry;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/inrix/sdk/InrixDebug$LogEntry;-><init>(Lcom/inrix/sdk/InrixDebug$1;)V

    .line 237
    .local v1, "log":Lcom/inrix/sdk/InrixDebug$LogEntry;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x5

    aget-object v2, v3, v4

    .line 238
    .local v2, "stackElem":Ljava/lang/StackTraceElement;
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ".java"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    iput-object v3, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->fileName:Ljava/lang/String;

    .line 239
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 240
    .local v0, "classNameIndex":I
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->className:Ljava/lang/String;

    .line 241
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->methodName:Ljava/lang/String;

    .line 242
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    iput v3, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->lineNum:I

    .line 243
    iput-object p0, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->msg:Ljava/lang/String;

    .line 245
    const-string/jumbo v3, "%s (%s:%s): %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->className:Ljava/lang/String;

    aput-object v5, v4, v6

    const/4 v5, 0x1

    iget-object v6, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->methodName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->lineNum:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->msg:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/inrix/sdk/InrixDebug$LogEntry;->verboseMsg:Ljava/lang/String;

    .line 251
    return-object v1
.end method

.method public static final isOnEmulator()Z
    .locals 2

    .prologue
    .line 72
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "google_sdk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "sdk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final setLogLevel(I)V
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 63
    invoke-static {p0}, Lcom/inrix/sdk/InrixDebug$LogType;->valueOf(I)Lcom/inrix/sdk/InrixDebug$LogType;

    move-result-object v0

    sput-object v0, Lcom/inrix/sdk/InrixDebug;->currentLoglevel:Lcom/inrix/sdk/InrixDebug$LogType;

    .line 64
    return-void
.end method
