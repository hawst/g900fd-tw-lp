.class public Lcom/inrix/sdk/LocationsManager;
.super Ljava/lang/Object;
.source "LocationsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;,
        Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;,
        Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;,
        Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;,
        Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;,
        Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;,
        Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    return-void
.end method


# virtual methods
.method public final createLocation(Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 9
    .param p1, "listener"    # Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;
    .param p2, "params"    # Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 284
    if-nez p1, :cond_0

    .line 285
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 287
    :cond_0
    if-nez p2, :cond_1

    .line 288
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 291
    :cond_1
    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->getPosition()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 292
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3e9

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 294
    :cond_2
    new-instance v0, Lcom/inrix/sdk/network/request/SaveLocationRequest;

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->getPosition()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v2

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->getCustomData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->getLocationType()I

    move-result v5

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->getOrder()I

    move-result v6

    new-instance v7, Lcom/inrix/sdk/LocationsManager$3;

    invoke-direct {v7, p0, p1}, Lcom/inrix/sdk/LocationsManager$3;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;)V

    new-instance v8, Lcom/inrix/sdk/LocationsManager$4;

    invoke-direct {v8, p0, p1}, Lcom/inrix/sdk/LocationsManager$4;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;)V

    invoke-direct/range {v0 .. v8}, Lcom/inrix/sdk/network/request/SaveLocationRequest;-><init>(Ljava/lang/String;Lcom/inrix/sdk/model/GeoPoint;Ljava/lang/String;Ljava/lang/String;IILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 313
    .local v0, "request":Lcom/inrix/sdk/network/request/SaveLocationRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 314
    return-object v0
.end method

.method public deleteLocation(Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 5
    .param p1, "listener"    # Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;
    .param p2, "params"    # Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 329
    if-nez p2, :cond_0

    .line 330
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 332
    :cond_0
    if-nez p1, :cond_1

    .line 333
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 336
    :cond_1
    new-instance v0, Lcom/inrix/sdk/network/request/DeleteLocationRequest;

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$DeleteLocationOptions;->getLocationId()J

    move-result-wide v1

    new-instance v3, Lcom/inrix/sdk/LocationsManager$5;

    invoke-direct {v3, p0, p1}, Lcom/inrix/sdk/LocationsManager$5;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;)V

    new-instance v4, Lcom/inrix/sdk/LocationsManager$6;

    invoke-direct {v4, p0, p1}, Lcom/inrix/sdk/LocationsManager$6;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationDeleteResponseListener;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/inrix/sdk/network/request/DeleteLocationRequest;-><init>(JLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 352
    .local v0, "request":Lcom/inrix/sdk/network/request/DeleteLocationRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 353
    return-object v0
.end method

.method public getLastLocationsUpdate(Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 3
    .param p1, "listener"    # Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 410
    if-nez p1, :cond_0

    .line 411
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 413
    :cond_0
    new-instance v0, Lcom/inrix/sdk/network/request/GetLastLocationsUpdateRequest;

    new-instance v1, Lcom/inrix/sdk/LocationsManager$9;

    invoke-direct {v1, p0, p1}, Lcom/inrix/sdk/LocationsManager$9;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;)V

    new-instance v2, Lcom/inrix/sdk/LocationsManager$10;

    invoke-direct {v2, p0, p1}, Lcom/inrix/sdk/LocationsManager$10;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILastLocationsUpdateResponseListener;)V

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/GetLastLocationsUpdateRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 427
    .local v0, "request":Lcom/inrix/sdk/network/request/GetLastLocationsUpdateRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 428
    return-object v0
.end method

.method public final requestSavedLocations(Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;)Lcom/inrix/sdk/ICancellable;
    .locals 3
    .param p1, "listener"    # Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 250
    if-nez p1, :cond_0

    .line 251
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 253
    :cond_0
    new-instance v0, Lcom/inrix/sdk/network/request/GetLocationsRequest;

    new-instance v1, Lcom/inrix/sdk/LocationsManager$1;

    invoke-direct {v1, p0, p1}, Lcom/inrix/sdk/LocationsManager$1;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;)V

    new-instance v2, Lcom/inrix/sdk/LocationsManager$2;

    invoke-direct {v2, p0, p1}, Lcom/inrix/sdk/LocationsManager$2;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationsGetResponseListener;)V

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/network/request/GetLocationsRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 267
    .local v0, "request":Lcom/inrix/sdk/network/request/GetLocationsRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 268
    return-object v0
.end method

.method public updateLocation(Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 10
    .param p1, "listener"    # Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;
    .param p2, "params"    # Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 368
    if-nez p2, :cond_0

    .line 369
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3eb

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 371
    :cond_0
    if-nez p1, :cond_1

    .line 372
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x3ea

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 374
    :cond_1
    new-instance v0, Lcom/inrix/sdk/network/request/UpdateLocationRequest;

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->getLocationId()J

    move-result-wide v1

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->getCustomData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->getLocationType()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p2}, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->getOrder()Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/inrix/sdk/LocationsManager$7;

    invoke-direct {v8, p0, p1}, Lcom/inrix/sdk/LocationsManager$7;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;)V

    new-instance v9, Lcom/inrix/sdk/LocationsManager$8;

    invoke-direct {v9, p0, p1}, Lcom/inrix/sdk/LocationsManager$8;-><init>(Lcom/inrix/sdk/LocationsManager;Lcom/inrix/sdk/LocationsManager$ILocationSaveResponseListener;)V

    invoke-direct/range {v0 .. v9}, Lcom/inrix/sdk/network/request/UpdateLocationRequest;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 394
    .local v0, "request":Lcom/inrix/sdk/network/request/UpdateLocationRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 395
    return-object v0
.end method
