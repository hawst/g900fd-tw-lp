.class Lcom/inrix/sdk/IncidentsManager$1;
.super Ljava/lang/Object;
.source "IncidentsManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/IncidentsManager;->getIncidentsInBox(Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;Lcom/inrix/sdk/IncidentsManager$IncidentBoxOptions;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/IncidentCollection;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/IncidentsManager;

.field final synthetic val$listener:Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/IncidentsManager;Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lcom/inrix/sdk/IncidentsManager$1;->this$0:Lcom/inrix/sdk/IncidentsManager;

    iput-object p2, p0, Lcom/inrix/sdk/IncidentsManager$1;->val$listener:Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/IncidentCollection;)V
    .locals 2
    .param p1, "response"    # Lcom/inrix/sdk/model/IncidentCollection;

    .prologue
    .line 475
    iget-object v0, p0, Lcom/inrix/sdk/IncidentsManager$1;->val$listener:Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;

    invoke-virtual {p1}, Lcom/inrix/sdk/model/IncidentCollection;->getIncidents()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/inrix/sdk/IncidentsManager$IIncidentsResponseListener;->onResult(Ljava/lang/Object;)V

    .line 476
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 471
    check-cast p1, Lcom/inrix/sdk/model/IncidentCollection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/IncidentsManager$1;->onResponse(Lcom/inrix/sdk/model/IncidentCollection;)V

    return-void
.end method
