.class public final enum Lcom/inrix/sdk/RouteManager$ACTIONS;
.super Ljava/lang/Enum;
.source "RouteManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/RouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ACTIONS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/RouteManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/RouteManager$ACTIONS;

.field public static final enum REQUEST_ROUTES:Lcom/inrix/sdk/RouteManager$ACTIONS;

.field public static final enum REQUEST_TRAVELTIMES:Lcom/inrix/sdk/RouteManager$ACTIONS;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 77
    new-instance v0, Lcom/inrix/sdk/RouteManager$ACTIONS;

    const-string/jumbo v1, "REQUEST_ROUTES"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/RouteManager$ACTIONS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/RouteManager$ACTIONS;->REQUEST_ROUTES:Lcom/inrix/sdk/RouteManager$ACTIONS;

    new-instance v0, Lcom/inrix/sdk/RouteManager$ACTIONS;

    const-string/jumbo v1, "REQUEST_TRAVELTIMES"

    invoke-direct {v0, v1, v3}, Lcom/inrix/sdk/RouteManager$ACTIONS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/RouteManager$ACTIONS;->REQUEST_TRAVELTIMES:Lcom/inrix/sdk/RouteManager$ACTIONS;

    .line 76
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/inrix/sdk/RouteManager$ACTIONS;

    sget-object v1, Lcom/inrix/sdk/RouteManager$ACTIONS;->REQUEST_ROUTES:Lcom/inrix/sdk/RouteManager$ACTIONS;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/RouteManager$ACTIONS;->REQUEST_TRAVELTIMES:Lcom/inrix/sdk/RouteManager$ACTIONS;

    aput-object v1, v0, v3

    sput-object v0, Lcom/inrix/sdk/RouteManager$ACTIONS;->$VALUES:[Lcom/inrix/sdk/RouteManager$ACTIONS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/RouteManager$ACTIONS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 76
    const-class v0, Lcom/inrix/sdk/RouteManager$ACTIONS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/RouteManager$ACTIONS;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/RouteManager$ACTIONS;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/inrix/sdk/RouteManager$ACTIONS;->$VALUES:[Lcom/inrix/sdk/RouteManager$ACTIONS;

    invoke-virtual {v0}, [Lcom/inrix/sdk/RouteManager$ACTIONS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/RouteManager$ACTIONS;

    return-object v0
.end method
