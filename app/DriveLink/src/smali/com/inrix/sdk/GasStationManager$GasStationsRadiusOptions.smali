.class public Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;
.super Lcom/inrix/sdk/GasStationManager$GasStationsOptions;
.source "GasStationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/GasStationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GasStationsRadiusOptions"
.end annotation


# instance fields
.field private center:Lcom/inrix/sdk/model/GeoPoint;

.field private radius:D

.field private units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;D)V
    .locals 1
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "radius"    # D

    .prologue
    .line 473
    invoke-direct {p0}, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;-><init>()V

    .line 474
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 475
    iput-wide p2, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->radius:D

    .line 476
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getSettingUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 477
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;DLcom/inrix/sdk/utils/UserPreferences$UNIT;)V
    .locals 2
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "radius"    # D
    .param p4, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .prologue
    .line 486
    invoke-direct {p0}, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;-><init>()V

    .line 487
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 488
    iput-wide p2, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->radius:D

    .line 489
    if-nez p4, :cond_0

    .line 490
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 492
    :cond_0
    iput-object p4, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 493
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;DLcom/inrix/sdk/utils/UserPreferences$UNIT;II)V
    .locals 2
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "radius"    # D
    .param p4, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .param p5, "outputFields"    # I
    .param p6, "productTypes"    # I

    .prologue
    .line 506
    invoke-direct {p0, p5, p6}, Lcom/inrix/sdk/GasStationManager$GasStationsOptions;-><init>(II)V

    .line 508
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 509
    iput-wide p2, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->radius:D

    .line 510
    if-nez p4, :cond_0

    .line 511
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 513
    :cond_0
    iput-object p4, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 514
    return-void
.end method


# virtual methods
.method public getCenter()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public getRadius()D
    .locals 2

    .prologue
    .line 559
    iget-wide v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->radius:D

    return-wide v0
.end method

.method public getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    return-object v0
.end method

.method public isCenterValid()Z
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    invoke-static {v0}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    return v0
.end method

.method public isRadiusValid()Z
    .locals 4

    .prologue
    .line 581
    iget-wide v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->radius:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 582
    const/4 v0, 0x0

    .line 584
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isUnitsValid()Z
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    if-eqz v0, :cond_0

    .line 611
    const/4 v0, 0x1

    .line 613
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCenter(Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 0
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 528
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 529
    return-void
.end method

.method public setRadius(D)V
    .locals 0
    .param p1, "radius"    # D

    .prologue
    .line 552
    iput-wide p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->radius:D

    .line 553
    return-void
.end method

.method public setUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)V
    .locals 2
    .param p1, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .prologue
    .line 591
    if-nez p1, :cond_0

    .line 592
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 594
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$GasStationsRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 595
    return-void
.end method
