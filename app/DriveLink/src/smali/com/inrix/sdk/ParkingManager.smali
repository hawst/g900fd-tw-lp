.class public final Lcom/inrix/sdk/ParkingManager;
.super Ljava/lang/Object;
.source "ParkingManager.java"

# interfaces
.implements Lcom/inrix/sdk/IRefreshableActions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/ParkingManager$7;,
        Lcom/inrix/sdk/ParkingManager$SORT_BY;,
        Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;,
        Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;,
        Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;,
        Lcom/inrix/sdk/ParkingManager$ParkingOptions;,
        Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;,
        Lcom/inrix/sdk/ParkingManager$ACTIONS;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/IRefreshableActions",
        "<",
        "Lcom/inrix/sdk/ParkingManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final GET_PARKINGLOTS_INTERVAL:I = 0xb4

.field private static final GET_PARKINGLOT_INFORMATION_INTERVAL:I = 0x258

.field public static final PARKING_OUTPUT_FIELD_ALL:I = 0xff

.field public static final PARKING_OUTPUT_FIELD_BASIC:I = 0x1

.field public static final PARKING_OUTPUT_FIELD_DYNAMIC:I = 0x8

.field public static final PARKING_OUTPUT_FIELD_GEOMETRY:I = 0x4

.field public static final PARKING_OUTPUT_FIELD_PRICING:I = 0x2

.field public static final PARKING_OUTPUT_FIELD_STATIC:I = 0x10

.field private static final PARKING_OUTPUT_FIELD_VALUE_ALL:Ljava/lang/String; = "all"

.field private static final PARKING_OUTPUT_FIELD_VALUE_BASIC:Ljava/lang/String; = "basic"

.field private static final PARKING_OUTPUT_FIELD_VALUE_DYNAMIC:Ljava/lang/String; = "dynamic"

.field private static final PARKING_OUTPUT_FIELD_VALUE_GEOMETRY:Ljava/lang/String; = "geometry"

.field private static final PARKING_OUTPUT_FIELD_VALUE_PRICING:Ljava/lang/String; = "pricing"

.field private static final PARKING_OUTPUT_FIELD_VALUE_STATIC:Ljava/lang/String; = "static"

.field private static final PARKING_SORT_BY_DISTANCE_VALUE:Ljava/lang/String; = "distance"

.field private static final PARKING_SORT_BY_NONE_VALUE:Ljava/lang/String; = "none"

.field private static final UNITS_METERS_VALUE:Ljava/lang/String; = "1"

.field private static final UNITS_MILES_VALUE:Ljava/lang/String; = "0"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478
    return-void
.end method

.method static synthetic access$000(Ljava/util/Date;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/Date;

    .prologue
    .line 30
    invoke-static {p0}, Lcom/inrix/sdk/ParkingManager;->validateArrivalDate(Ljava/util/Date;)V

    return-void
.end method

.method static synthetic access$100(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 30
    invoke-static {p0}, Lcom/inrix/sdk/ParkingManager;->validateOutputFields(I)V

    return-void
.end method

.method private static final resolveOutputFields(I)Ljava/lang/String;
    .locals 3
    .param p0, "outputFields"    # I

    .prologue
    .line 675
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 676
    .local v0, "fields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    and-int/lit8 v1, p0, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 677
    const-string/jumbo v1, "basic"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    :cond_0
    and-int/lit8 v1, p0, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 681
    const-string/jumbo v1, "pricing"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 684
    :cond_1
    and-int/lit8 v1, p0, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 685
    const-string/jumbo v1, "geometry"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 688
    :cond_2
    and-int/lit8 v1, p0, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 689
    const-string/jumbo v1, "dynamic"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 692
    :cond_3
    and-int/lit8 v1, p0, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 693
    const-string/jumbo v1, "static"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 696
    :cond_4
    and-int/lit16 v1, p0, 0xff

    const/16 v2, 0xff

    if-ne v1, v2, :cond_5

    .line 697
    const-string/jumbo v1, "all"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 700
    :cond_5
    const-string/jumbo v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static final resolveSortValue(Lcom/inrix/sdk/ParkingManager$SORT_BY;)Ljava/lang/String;
    .locals 1
    .param p0, "sort"    # Lcom/inrix/sdk/ParkingManager$SORT_BY;

    .prologue
    .line 704
    sget-object v0, Lcom/inrix/sdk/ParkingManager$SORT_BY;->DISTANCE:Lcom/inrix/sdk/ParkingManager$SORT_BY;

    if-ne p0, v0, :cond_0

    .line 705
    const-string/jumbo v0, "distance"

    .line 707
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "none"

    goto :goto_0
.end method

.method private static final resolveUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)Ljava/lang/String;
    .locals 1
    .param p0, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .prologue
    .line 712
    sget-object v0, Lcom/inrix/sdk/utils/UserPreferences$UNIT;->METERS:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    if-ne p0, v0, :cond_0

    .line 713
    const-string/jumbo v0, "1"

    .line 715
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "0"

    goto :goto_0
.end method

.method private static final validateArrivalDate(Ljava/util/Date;)V
    .locals 3
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    const/16 v2, 0x417

    .line 650
    if-nez p0, :cond_0

    .line 651
    const-string/jumbo v1, "Invalid arrival date. Arrival date should not be null"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 653
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 655
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 657
    .local v0, "curDate":Ljava/util/Date;
    invoke-virtual {v0, p0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 658
    const-string/jumbo v1, "Invalid arrival date. Arrival date should be in the past"

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogE(Ljava/lang/String;)V

    .line 660
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 662
    :cond_1
    return-void
.end method

.method private static final validateOutputFields(I)V
    .locals 2
    .param p0, "outputFields"    # I

    .prologue
    .line 665
    if-ltz p0, :cond_0

    const/16 v0, 0xff

    if-le p0, v0, :cond_1

    .line 666
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3f1

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 668
    :cond_1
    return-void
.end method


# virtual methods
.method public final getParkingLotInformation(Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 5
    .param p1, "listener"    # Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;
    .param p2, "options"    # Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 610
    if-nez p1, :cond_0

    .line 611
    new-instance v2, Lcom/inrix/sdk/exception/InrixException;

    const/16 v3, 0x3ea

    invoke-direct {v2, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v2

    .line 613
    :cond_0
    if-nez p2, :cond_1

    .line 614
    new-instance v2, Lcom/inrix/sdk/exception/InrixException;

    const/16 v3, 0x3eb

    invoke-direct {v2, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v2

    .line 617
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 618
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "parkinglotids"

    const-string/jumbo v3, ","

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->getIds()Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->getArrivalDate()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 621
    const-string/jumbo v2, "arrivaltime"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->getArrivalDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    :cond_2
    const-string/jumbo v2, "units"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    const-string/jumbo v2, "outputfields"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->getOutputFields()I

    move-result v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveOutputFields(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    const-string/jumbo v2, "sort"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInfoOptions;->getSortBy()Lcom/inrix/sdk/ParkingManager$SORT_BY;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveSortValue(Lcom/inrix/sdk/ParkingManager$SORT_BY;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    new-instance v1, Lcom/inrix/sdk/network/request/ParkingGetLotInformationRequest;

    new-instance v2, Lcom/inrix/sdk/ParkingManager$5;

    invoke-direct {v2, p0, p1}, Lcom/inrix/sdk/ParkingManager$5;-><init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V

    new-instance v3, Lcom/inrix/sdk/ParkingManager$6;

    invoke-direct {v3, p0, p1}, Lcom/inrix/sdk/ParkingManager$6;-><init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V

    invoke-direct {v1, v0, v2, v3}, Lcom/inrix/sdk/network/request/ParkingGetLotInformationRequest;-><init>(Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 643
    .local v1, "request":Lcom/inrix/sdk/network/request/ParkingGetLotInformationRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 644
    return-object v1
.end method

.method public final getParkingLotsInBox(Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 5
    .param p1, "listener"    # Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;
    .param p2, "options"    # Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 552
    if-nez p1, :cond_0

    .line 553
    new-instance v2, Lcom/inrix/sdk/exception/InrixException;

    const/16 v3, 0x3ea

    invoke-direct {v2, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v2

    .line 555
    :cond_0
    if-nez p2, :cond_1

    .line 556
    new-instance v2, Lcom/inrix/sdk/exception/InrixException;

    const/16 v3, 0x3eb

    invoke-direct {v2, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v2

    .line 559
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 560
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "startlatitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getCorner1()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    const-string/jumbo v2, "startlongitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getCorner1()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    const-string/jumbo v2, "endlatitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getCorner2()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    const-string/jumbo v2, "endlongitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getCorner2()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getArrivalDate()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 569
    const-string/jumbo v2, "arrivaltime"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getArrivalDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    :cond_2
    const-string/jumbo v2, "units"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    const-string/jumbo v2, "outputfields"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getOutputFields()I

    move-result v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveOutputFields(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    const-string/jumbo v2, "sort"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInBoxOptions;->getSortBy()Lcom/inrix/sdk/ParkingManager$SORT_BY;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveSortValue(Lcom/inrix/sdk/ParkingManager$SORT_BY;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    new-instance v1, Lcom/inrix/sdk/network/request/ParkingBoxRequest;

    new-instance v2, Lcom/inrix/sdk/ParkingManager$3;

    invoke-direct {v2, p0, p1}, Lcom/inrix/sdk/ParkingManager$3;-><init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V

    new-instance v3, Lcom/inrix/sdk/ParkingManager$4;

    invoke-direct {v3, p0, p1}, Lcom/inrix/sdk/ParkingManager$4;-><init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V

    invoke-direct {v1, v0, v2, v3}, Lcom/inrix/sdk/network/request/ParkingBoxRequest;-><init>(Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 591
    .local v1, "request":Lcom/inrix/sdk/network/request/ParkingBoxRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 592
    return-object v1
.end method

.method public final getParkingLotsInRadius(Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;)Lcom/inrix/sdk/ICancellable;
    .locals 5
    .param p1, "listener"    # Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;
    .param p2, "options"    # Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 497
    if-nez p1, :cond_0

    .line 498
    new-instance v2, Lcom/inrix/sdk/exception/InrixException;

    const/16 v3, 0x3ea

    invoke-direct {v2, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v2

    .line 500
    :cond_0
    if-nez p2, :cond_1

    .line 501
    new-instance v2, Lcom/inrix/sdk/exception/InrixException;

    const/16 v3, 0x3eb

    invoke-direct {v2, v3}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v2

    .line 504
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 505
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "latitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getCenter()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    const-string/jumbo v2, "longitude"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getCenter()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    const-string/jumbo v2, "radius"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getRadius()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getArrivalDate()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 511
    const-string/jumbo v2, "arrivaltime"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getArrivalDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    :cond_2
    const-string/jumbo v2, "units"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    const-string/jumbo v2, "outputfields"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getOutputFields()I

    move-result v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveOutputFields(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    const-string/jumbo v2, "sort"

    invoke-virtual {p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->getSortBy()Lcom/inrix/sdk/ParkingManager$SORT_BY;

    move-result-object v3

    invoke-static {v3}, Lcom/inrix/sdk/ParkingManager;->resolveSortValue(Lcom/inrix/sdk/ParkingManager$SORT_BY;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    new-instance v1, Lcom/inrix/sdk/network/request/ParkingRadiusRequest;

    new-instance v2, Lcom/inrix/sdk/ParkingManager$1;

    invoke-direct {v2, p0, p1}, Lcom/inrix/sdk/ParkingManager$1;-><init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V

    new-instance v3, Lcom/inrix/sdk/ParkingManager$2;

    invoke-direct {v3, p0, p1}, Lcom/inrix/sdk/ParkingManager$2;-><init>(Lcom/inrix/sdk/ParkingManager;Lcom/inrix/sdk/ParkingManager$IParkingResponseListener;)V

    invoke-direct {v1, v0, v2, v3}, Lcom/inrix/sdk/network/request/ParkingRadiusRequest;-><init>(Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 533
    .local v1, "request":Lcom/inrix/sdk/network/request/ParkingRadiusRequest;
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 534
    return-object v1
.end method

.method public getRefreshInterval(Lcom/inrix/sdk/ParkingManager$ACTIONS;)I
    .locals 3
    .param p1, "action"    # Lcom/inrix/sdk/ParkingManager$ACTIONS;

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x412

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 54
    :cond_0
    sget-object v1, Lcom/inrix/sdk/ParkingManager$7;->$SwitchMap$com$inrix$sdk$ParkingManager$ACTIONS:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/ParkingManager$ACTIONS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 60
    const/16 v0, 0x258

    .line 64
    .local v0, "interval":I
    :goto_0
    return v0

    .line 56
    .end local v0    # "interval":I
    :pswitch_0
    const/16 v0, 0xb4

    .line 57
    .restart local v0    # "interval":I
    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic getRefreshInterval(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lcom/inrix/sdk/ParkingManager$ACTIONS;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/ParkingManager;->getRefreshInterval(Lcom/inrix/sdk/ParkingManager$ACTIONS;)I

    move-result v0

    return v0
.end method
