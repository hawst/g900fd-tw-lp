.class public final enum Lcom/inrix/sdk/model/ParkingLot$GateType;
.super Ljava/lang/Enum;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/ParkingLot$GateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/ParkingLot$GateType;

.field public static final enum PedestrianEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

.field public static final enum PedestrianExit:Lcom/inrix/sdk/model/ParkingLot$GateType;

.field public static final enum Unknown:Lcom/inrix/sdk/model/ParkingLot$GateType;

.field public static final enum VehicleEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

.field public static final enum VehicleExit:Lcom/inrix/sdk/model/ParkingLot$GateType;

.field public static final enum VehicleExitAndEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

.field public static final enum VehicleRentalReturn:Lcom/inrix/sdk/model/ParkingLot$GateType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 446
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    const-string/jumbo v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/ParkingLot$GateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$GateType;

    .line 447
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    const-string/jumbo v1, "VehicleEntrance"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/ParkingLot$GateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

    .line 448
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    const-string/jumbo v1, "VehicleExit"

    invoke-direct {v0, v1, v6, v6}, Lcom/inrix/sdk/model/ParkingLot$GateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleExit:Lcom/inrix/sdk/model/ParkingLot$GateType;

    .line 449
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    const-string/jumbo v1, "VehicleRentalReturn"

    invoke-direct {v0, v1, v7, v7}, Lcom/inrix/sdk/model/ParkingLot$GateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleRentalReturn:Lcom/inrix/sdk/model/ParkingLot$GateType;

    .line 450
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    const-string/jumbo v1, "VehicleExitAndEntrance"

    invoke-direct {v0, v1, v8, v8}, Lcom/inrix/sdk/model/ParkingLot$GateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleExitAndEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

    .line 451
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    const-string/jumbo v1, "PedestrianEntrance"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$GateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->PedestrianEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

    .line 452
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    const-string/jumbo v1, "PedestrianExit"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$GateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->PedestrianExit:Lcom/inrix/sdk/model/ParkingLot$GateType;

    .line 445
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/inrix/sdk/model/ParkingLot$GateType;

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$GateType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$GateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleExit:Lcom/inrix/sdk/model/ParkingLot$GateType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleRentalReturn:Lcom/inrix/sdk/model/ParkingLot$GateType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$GateType;->VehicleExitAndEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$GateType;->PedestrianEntrance:Lcom/inrix/sdk/model/ParkingLot$GateType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$GateType;->PedestrianExit:Lcom/inrix/sdk/model/ParkingLot$GateType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$GateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 456
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 457
    iput p3, p0, Lcom/inrix/sdk/model/ParkingLot$GateType;->value:I

    .line 458
    return-void
.end method

.method public static final fromValue(I)Lcom/inrix/sdk/model/ParkingLot$GateType;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 472
    invoke-static {}, Lcom/inrix/sdk/model/ParkingLot$GateType;->values()[Lcom/inrix/sdk/model/ParkingLot$GateType;

    move-result-object v4

    .line 473
    .local v4, "values":[Lcom/inrix/sdk/model/ParkingLot$GateType;
    move-object v0, v4

    .local v0, "arr$":[Lcom/inrix/sdk/model/ParkingLot$GateType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 474
    .local v1, "current":Lcom/inrix/sdk/model/ParkingLot$GateType;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot$GateType;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 479
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$GateType;
    :goto_1
    return-object v1

    .line 473
    .restart local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$GateType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 479
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$GateType;
    :cond_1
    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$GateType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$GateType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/ParkingLot$GateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 445
    const-class v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/ParkingLot$GateType;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/ParkingLot$GateType;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Lcom/inrix/sdk/model/ParkingLot$GateType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$GateType;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/ParkingLot$GateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/ParkingLot$GateType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 461
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$GateType;->value:I

    return v0
.end method
