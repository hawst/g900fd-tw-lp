.class public Lcom/inrix/sdk/model/DeviceAuth;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "DeviceAuth.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/DeviceAuth$1;,
        Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;,
        Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;
    }
.end annotation


# instance fields
.field private final INRIX_DEFAULT_TIMEZONE:Ljava/lang/String;

.field private final INRIX_TIME_FORMAT:Ljava/lang/String;

.field private entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation
.end field

.field private expirationDate:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    .line 23
    const-string/jumbo v0, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth;->INRIX_TIME_FORMAT:Ljava/lang/String;

    .line 24
    const-string/jumbo v0, "UTC"

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth;->INRIX_DEFAULT_TIMEZONE:Ljava/lang/String;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth;->expirationDate:Ljava/util/Date;

    .line 37
    return-void
.end method

.method public static tryParseStoredAuthToken()Lcom/inrix/sdk/model/DeviceAuth;
    .locals 19

    .prologue
    .line 41
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getAuthToken()Ljava/lang/String;

    move-result-object v5

    .line 42
    .local v5, "authToken":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getAuthTokenExpireTime()Ljava/lang/String;

    move-result-object v4

    .line 43
    .local v4, "authExpireTime":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getMobileServer()Ljava/lang/String;

    move-result-object v11

    .line 44
    .local v11, "mobileServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getTTSServer()Ljava/lang/String;

    move-result-object v17

    .line 45
    .local v17, "ttsServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getAnalyticsServer()Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "analyticsServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getCamerasServer()Ljava/lang/String;

    move-result-object v6

    .line 47
    .local v6, "camerasServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getCompositeTilesServer()Ljava/lang/String;

    move-result-object v7

    .line 48
    .local v7, "compositeTilesServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getDustServer()Ljava/lang/String;

    move-result-object v8

    .line 49
    .local v8, "dustServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getFuelServer()Ljava/lang/String;

    move-result-object v9

    .line 50
    .local v9, "fuelServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getIncidentsServer()Ljava/lang/String;

    move-result-object v10

    .line 51
    .local v10, "incidentsServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getParkingServer()Ljava/lang/String;

    move-result-object v12

    .line 52
    .local v12, "parkingServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getRoutingServer()Ljava/lang/String;

    move-result-object v13

    .line 53
    .local v13, "routingServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getSettingsServer()Ljava/lang/String;

    move-result-object v14

    .line 54
    .local v14, "settingsServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getTrafficStateServer()Ljava/lang/String;

    move-result-object v15

    .line 55
    .local v15, "trafficStateServer":Ljava/lang/String;
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getTravelDataServer()Ljava/lang/String;

    move-result-object v16

    .line 57
    .local v16, "travelDataServer":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 66
    :cond_0
    const/4 v3, 0x0

    .line 86
    :goto_0
    return-object v3

    .line 68
    :cond_1
    new-instance v3, Lcom/inrix/sdk/model/DeviceAuth;

    invoke-direct {v3}, Lcom/inrix/sdk/model/DeviceAuth;-><init>()V

    .line 69
    .local v3, "auth":Lcom/inrix/sdk/model/DeviceAuth;
    new-instance v18, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    invoke-direct/range {v18 .. v18}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;-><init>()V

    move-object/from16 v0, v18

    iput-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    .line 70
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->token:Ljava/lang/String;

    .line 71
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->tokenExpireDateUTC:Ljava/lang/String;

    .line 72
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v11, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->mobileServer:Ljava/lang/String;

    .line 73
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->ttsServer:Ljava/lang/String;

    .line 74
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->analyticsServer:Ljava/lang/String;

    .line 75
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v6, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->camerasServer:Ljava/lang/String;

    .line 76
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v7, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->compositeTilesServer:Ljava/lang/String;

    .line 77
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v8, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->dustServer:Ljava/lang/String;

    .line 78
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v9, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->fuelServer:Ljava/lang/String;

    .line 79
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v10, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->incidentsServer:Ljava/lang/String;

    .line 80
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v12, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->parkingServer:Ljava/lang/String;

    .line 81
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v13, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->routingServer:Ljava/lang/String;

    .line 82
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v14, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->settingsServer:Ljava/lang/String;

    .line 83
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v15, v0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->trafficStateServer:Ljava/lang/String;

    .line 84
    iget-object v0, v3, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->travelDataServer:Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method public getEntity()Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    return-object v0
.end method

.method public getExpirationDate()Ljava/util/Date;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v2, p0, Lcom/inrix/sdk/model/DeviceAuth;->expirationDate:Ljava/util/Date;

    if-nez v2, :cond_0

    .line 95
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 96
    .local v1, "format":Ljava/text/DateFormat;
    const-string/jumbo v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 98
    :try_start_0
    iget-object v2, p0, Lcom/inrix/sdk/model/DeviceAuth;->entity:Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;

    iget-object v2, v2, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->tokenExpireDateUTC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lcom/inrix/sdk/model/DeviceAuth;->expirationDate:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    .end local v1    # "format":Ljava/text/DateFormat;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/inrix/sdk/model/DeviceAuth;->expirationDate:Ljava/util/Date;

    return-object v2

    .line 100
    .restart local v1    # "format":Ljava/text/DateFormat;
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public isExpired()Z
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/inrix/sdk/model/DeviceAuth;->getExpirationDate()Ljava/util/Date;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method public setExpirationDate(Ljava/util/Date;)V
    .locals 0
    .param p1, "value"    # Ljava/util/Date;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/inrix/sdk/model/DeviceAuth;->expirationDate:Ljava/util/Date;

    .line 268
    return-void
.end method
