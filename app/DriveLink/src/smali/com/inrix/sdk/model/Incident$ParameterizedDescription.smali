.class public Lcom/inrix/sdk/model/Incident$ParameterizedDescription;
.super Ljava/lang/Object;
.source "Incident.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/Incident;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParameterizedDescription"
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private crossroad1:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Crossroad1"
        required = false
    .end annotation
.end field

.field private crossroad2:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Crossroad2"
        required = false
    .end annotation
.end field

.field private direction:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Direction"
        required = false
    .end annotation
.end field

.field private eventCode:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private eventText:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "EventText"
    .end annotation
.end field

.field private fromLocation:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "FromLocation"
        required = false
    .end annotation
.end field

.field private position1:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Position1"
        required = false
    .end annotation
.end field

.field private position2:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Position2"
        required = false
    .end annotation
.end field

.field private roadName:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "RoadName"
        required = false
    .end annotation
.end field

.field private toLocation:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "ToLocation"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDirection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->direction:Ljava/lang/String;

    return-object v0
.end method

.method public getEventCode()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->eventCode:I

    return v0
.end method

.method public getEventText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->eventText:Ljava/lang/String;

    return-object v0
.end method

.method public getFromLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->fromLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->position1:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->position2:Ljava/lang/String;

    return-object v0
.end method

.method public getRoadName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->roadName:Ljava/lang/String;

    return-object v0
.end method

.method public getToLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$ParameterizedDescription;->toLocation:Ljava/lang/String;

    return-object v0
.end method
