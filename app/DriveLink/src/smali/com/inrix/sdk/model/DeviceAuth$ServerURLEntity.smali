.class public Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;
.super Ljava/lang/Object;
.source "DeviceAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/DeviceAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ServerURLEntity"
.end annotation


# instance fields
.field public endPointType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field

.field public region:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "region"
    .end annotation
.end field

.field final synthetic this$0:Lcom/inrix/sdk/model/DeviceAuth;

.field public url:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "href"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/DeviceAuth;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;->this$0:Lcom/inrix/sdk/model/DeviceAuth;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
