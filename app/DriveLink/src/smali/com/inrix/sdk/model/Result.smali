.class Lcom/inrix/sdk/model/Result;
.super Ljava/lang/Object;
.source "LastLocationsUpdate.java"


# instance fields
.field private lastCustomRoutesUpdateStr:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CustomRoute"
    .end annotation
.end field

.field private lastDepartureAlertsUpdateStr:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DepartureAlert"
    .end annotation
.end field

.field private lastLocationsUpdateStr:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Location"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLastCustomRoutesUpdateStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/inrix/sdk/model/Result;->lastCustomRoutesUpdateStr:Ljava/lang/String;

    return-object v0
.end method

.method public getLastDepartureAlertsUpdateStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/inrix/sdk/model/Result;->lastDepartureAlertsUpdateStr:Ljava/lang/String;

    return-object v0
.end method

.method public getLastLocationsUpdateStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/inrix/sdk/model/Result;->lastLocationsUpdateStr:Ljava/lang/String;

    return-object v0
.end method
