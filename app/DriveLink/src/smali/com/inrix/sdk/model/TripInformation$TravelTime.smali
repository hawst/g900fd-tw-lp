.class public Lcom/inrix/sdk/model/TripInformation$TravelTime;
.super Ljava/lang/Object;
.source "TripInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/TripInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TravelTime"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;
    }
.end annotation


# instance fields
.field private abnormalityMinutes:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private arrivalTimeStringUtc:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "arrivalTime"
        required = false
    .end annotation
.end field

.field private averageSpeed:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private departureTimeStringUtc:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "departureTime"
        required = false
    .end annotation
.end field

.field private routeHasClosures:Z
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

.field private routeRestricted:Z
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private travelTimeMinutes:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    iput v2, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->travelTimeMinutes:I

    .line 277
    iput v2, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->abnormalityMinutes:I

    .line 308
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->departureTimeStringUtc:Ljava/lang/String;

    .line 309
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->arrivalTimeStringUtc:Ljava/lang/String;

    .line 310
    iput v2, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->travelTimeMinutes:I

    .line 311
    iput v2, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->abnormalityMinutes:I

    .line 312
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->averageSpeed:D

    .line 313
    iput-boolean v2, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeHasClosures:Z

    .line 314
    iput-boolean v2, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeRestricted:Z

    .line 316
    sget-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Unknown:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    iput-object v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    .line 317
    return-void
.end method

.method private determineRouteQualityFromUncongestedMinutes(I)V
    .locals 8
    .param p1, "uncongestedMinutes"    # I

    .prologue
    .line 426
    invoke-virtual {p0}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->doesRouteHaveClosures()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->isRouteRestricted()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 427
    :cond_0
    sget-object v4, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Closed:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    iput-object v4, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    .line 430
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getTravelTime()I

    move-result v4

    if-nez v4, :cond_3

    .line 431
    :cond_2
    sget-object v4, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->FreeFlow:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    iput-object v4, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    .line 446
    :goto_0
    return-void

    .line 433
    :cond_3
    invoke-virtual {p0}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getTravelTime()I

    move-result v4

    int-to-double v4, v4

    int-to-double v6, p1

    div-double v0, v4, v6

    .line 435
    .local v0, "abnormalRatio":D
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v6, v0, v6

    mul-double v2, v4, v6

    .line 436
    .local v2, "qualityMetric":D
    const-wide/high16 v4, 0x4039000000000000L    # 25.0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_4

    .line 437
    sget-object v4, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->FreeFlow:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    iput-object v4, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    goto :goto_0

    .line 438
    :cond_4
    const-wide/high16 v4, 0x4049000000000000L    # 50.0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_5

    .line 439
    sget-object v4, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Moderate:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    iput-object v4, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    goto :goto_0

    .line 440
    :cond_5
    const-wide v4, 0x4052c00000000000L    # 75.0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_6

    .line 441
    sget-object v4, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Heavy:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    iput-object v4, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    goto :goto_0

    .line 443
    :cond_6
    sget-object v4, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->StopAndGo:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    iput-object v4, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    goto :goto_0
.end method


# virtual methods
.method public doesRouteHaveClosures()Z
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeHasClosures:Z

    return v0
.end method

.method public getAbnormailtyMinutes()I
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->abnormalityMinutes:I

    return v0
.end method

.method public getArrivalTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 344
    const/4 v0, 0x0

    .line 346
    .local v0, "returnDate":Ljava/util/Date;
    :try_start_0
    iget-object v1, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->arrivalTimeStringUtc:Ljava/lang/String;

    invoke-static {v1}, Lcom/inrix/sdk/utils/InrixDateUtils;->getDateFromString(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 350
    :goto_0
    return-object v0

    .line 348
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getAverageSpeed()D
    .locals 2

    .prologue
    .line 380
    iget-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->averageSpeed:D

    return-wide v0
.end method

.method public getDepartureTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 327
    const/4 v0, 0x0

    .line 329
    .local v0, "returnDate":Ljava/util/Date;
    :try_start_0
    iget-object v1, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->departureTimeStringUtc:Ljava/lang/String;

    invoke-static {v1}, Lcom/inrix/sdk/utils/InrixDateUtils;->getDateFromString(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 333
    :goto_0
    return-object v0

    .line 331
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getRouteQuality(I)Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;
    .locals 2
    .param p1, "uncongestedMinutes"    # I

    .prologue
    .line 411
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    sget-object v1, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Unknown:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    if-ne v0, v1, :cond_0

    .line 412
    invoke-direct {p0, p1}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->determineRouteQualityFromUncongestedMinutes(I)V

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeQuality:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    return-object v0
.end method

.method public getTravelTime()I
    .locals 1

    .prologue
    .line 360
    iget v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->travelTimeMinutes:I

    return v0
.end method

.method public isRouteRestricted()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime;->routeRestricted:Z

    return v0
.end method
