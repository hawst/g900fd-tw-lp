.class public final Lcom/inrix/sdk/model/ParkingLot;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;,
        Lcom/inrix/sdk/model/ParkingLot$Reservability;,
        Lcom/inrix/sdk/model/ParkingLot$Tendency;,
        Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;,
        Lcom/inrix/sdk/model/ParkingLot$DynamicContent;,
        Lcom/inrix/sdk/model/ParkingLot$ParkingDuration;,
        Lcom/inrix/sdk/model/ParkingLot$ParkingTime;,
        Lcom/inrix/sdk/model/ParkingLot$PricingPayment;,
        Lcom/inrix/sdk/model/ParkingLot$PaymentMethods;,
        Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;,
        Lcom/inrix/sdk/model/ParkingLot$OpeningHours;,
        Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;,
        Lcom/inrix/sdk/model/ParkingLot$GateInformation;,
        Lcom/inrix/sdk/model/ParkingLot$GateType;,
        Lcom/inrix/sdk/model/ParkingLot$Specification;,
        Lcom/inrix/sdk/model/ParkingLot$ParkingType;,
        Lcom/inrix/sdk/model/ParkingLot$Photo;,
        Lcom/inrix/sdk/model/ParkingLot$Address;,
        Lcom/inrix/sdk/model/ParkingLot$Information;,
        Lcom/inrix/sdk/model/ParkingLot$StaticContent;
    }
.end annotation


# instance fields
.field private dynamicContent:Lcom/inrix/sdk/model/ParkingLot$DynamicContent;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DynamicContent"
    .end annotation
.end field

.field private id:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Id"
    .end annotation
.end field

.field private latitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "latitude"
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "longitude"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Name"
    .end annotation
.end field

.field private staticContent:Lcom/inrix/sdk/model/ParkingLot$StaticContent;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "StaticContent"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/high16 v1, 0x7ff8000000000000L    # NaN

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot;->id:I

    .line 23
    iput-wide v1, p0, Lcom/inrix/sdk/model/ParkingLot;->latitude:D

    .line 26
    iput-wide v1, p0, Lcom/inrix/sdk/model/ParkingLot;->longitude:D

    .line 875
    return-void
.end method


# virtual methods
.method public getDistance(Lcom/inrix/sdk/model/GeoPoint;)D
    .locals 8
    .param p1, "from"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 92
    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/inrix/sdk/model/ParkingLot;->latitude:D

    invoke-virtual {p0}, Lcom/inrix/sdk/model/ParkingLot;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/inrix/sdk/utils/GeoUtils;->distance(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getDynamicContent()Lcom/inrix/sdk/model/ParkingLot$DynamicContent;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot;->dynamicContent:Lcom/inrix/sdk/model/ParkingLot$DynamicContent;

    return-object v0
.end method

.method public final getGeoPoint()Lcom/inrix/sdk/model/GeoPoint;
    .locals 5

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot;->latitude:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot;->longitude:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/inrix/sdk/model/GeoPoint;

    iget-wide v1, p0, Lcom/inrix/sdk/model/ParkingLot;->latitude:D

    iget-wide v3, p0, Lcom/inrix/sdk/model/ParkingLot;->longitude:D

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/inrix/sdk/model/GeoPoint;-><init>(DD)V

    goto :goto_0
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot;->id:I

    return v0
.end method

.method public final getLatitude()D
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot;->latitude:D

    return-wide v0
.end method

.method public final getLongitude()D
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot;->longitude:D

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getStaticContent()Lcom/inrix/sdk/model/ParkingLot$StaticContent;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot;->staticContent:Lcom/inrix/sdk/model/ParkingLot$StaticContent;

    return-object v0
.end method
