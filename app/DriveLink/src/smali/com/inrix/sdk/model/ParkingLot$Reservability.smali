.class public final enum Lcom/inrix/sdk/model/ParkingLot$Reservability;
.super Ljava/lang/Enum;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reservability"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/ParkingLot$Reservability;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/ParkingLot$Reservability;

.field public static final enum NotReservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

.field public static final enum PartlyReservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

.field public static final enum Reservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

.field public static final enum ReservationRequired:Lcom/inrix/sdk/model/ParkingLot$Reservability;

.field public static final enum Unknown:Lcom/inrix/sdk/model/ParkingLot$Reservability;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 837
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;

    const-string/jumbo v1, "Unknown"

    invoke-direct {v0, v1, v2, v2}, Lcom/inrix/sdk/model/ParkingLot$Reservability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->Unknown:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    .line 838
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;

    const-string/jumbo v1, "PartlyReservable"

    invoke-direct {v0, v1, v3, v3}, Lcom/inrix/sdk/model/ParkingLot$Reservability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->PartlyReservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    .line 839
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;

    const-string/jumbo v1, "Reservable"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/ParkingLot$Reservability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->Reservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    .line 840
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;

    const-string/jumbo v1, "NotReservable"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/ParkingLot$Reservability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->NotReservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    .line 841
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;

    const-string/jumbo v1, "ReservationRequired"

    invoke-direct {v0, v1, v6, v6}, Lcom/inrix/sdk/model/ParkingLot$Reservability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->ReservationRequired:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    .line 836
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/inrix/sdk/model/ParkingLot$Reservability;

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Reservability;->Unknown:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Reservability;->PartlyReservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    aput-object v1, v0, v3

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Reservability;->Reservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Reservability;->NotReservable:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Reservability;->ReservationRequired:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    aput-object v1, v0, v6

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$Reservability;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 845
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 846
    iput p3, p0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->value:I

    .line 847
    return-void
.end method

.method public static final fromValue(I)Lcom/inrix/sdk/model/ParkingLot$Reservability;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 861
    invoke-static {}, Lcom/inrix/sdk/model/ParkingLot$Reservability;->values()[Lcom/inrix/sdk/model/ParkingLot$Reservability;

    move-result-object v4

    .line 862
    .local v4, "values":[Lcom/inrix/sdk/model/ParkingLot$Reservability;
    move-object v0, v4

    .local v0, "arr$":[Lcom/inrix/sdk/model/ParkingLot$Reservability;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 863
    .local v1, "current":Lcom/inrix/sdk/model/ParkingLot$Reservability;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot$Reservability;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 868
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$Reservability;
    :goto_1
    return-object v1

    .line 862
    .restart local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$Reservability;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 868
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$Reservability;
    :cond_1
    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Reservability;->Unknown:Lcom/inrix/sdk/model/ParkingLot$Reservability;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/ParkingLot$Reservability;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 836
    const-class v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/ParkingLot$Reservability;
    .locals 1

    .prologue
    .line 836
    sget-object v0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$Reservability;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/ParkingLot$Reservability;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/ParkingLot$Reservability;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 850
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$Reservability;->value:I

    return v0
.end method
