.class public Lcom/inrix/sdk/model/TripInformation$TravelPoint;
.super Ljava/lang/Object;
.source "TripInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/TripInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TravelPoint"
.end annotation


# instance fields
.field private latitude:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->latitude:D

    .line 130
    iput-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->longitude:D

    .line 131
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 0
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-wide p1, p0, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->latitude:D

    .line 143
    iput-wide p3, p0, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->longitude:D

    .line 144
    return-void
.end method


# virtual methods
.method public getLatitude()D
    .locals 2

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->longitude:D

    return-wide v0
.end method
