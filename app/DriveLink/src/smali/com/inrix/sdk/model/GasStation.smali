.class public Lcom/inrix/sdk/model/GasStation;
.super Ljava/lang/Object;
.source "GasStation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/GasStation$Address;,
        Lcom/inrix/sdk/model/GasStation$Product;
    }
.end annotation


# instance fields
.field private address:Lcom/inrix/sdk/model/GasStation$Address;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Address"
    .end annotation
.end field

.field private brand:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Brand"
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Id"
    .end annotation
.end field

.field private latitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "latitude"
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "longitude"
    .end annotation
.end field

.field private productList:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Products"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GasStation$Product;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;DD)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "brand"    # Ljava/lang/String;
    .param p3, "latitude"    # D
    .param p5, "longitude"    # D

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation;->id:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/inrix/sdk/model/GasStation;->brand:Ljava/lang/String;

    .line 42
    iput-wide p3, p0, Lcom/inrix/sdk/model/GasStation;->latitude:D

    .line 43
    iput-wide p5, p0, Lcom/inrix/sdk/model/GasStation;->longitude:D

    .line 44
    new-instance v0, Lcom/inrix/sdk/model/GasStation$Address;

    invoke-direct {v0, p0}, Lcom/inrix/sdk/model/GasStation$Address;-><init>(Lcom/inrix/sdk/model/GasStation;)V

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation;->address:Lcom/inrix/sdk/model/GasStation$Address;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation;->productList:Ljava/util/List;

    .line 46
    return-void
.end method


# virtual methods
.method public getAddress()Lcom/inrix/sdk/model/GasStation$Address;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation;->address:Lcom/inrix/sdk/model/GasStation$Address;

    return-object v0
.end method

.method public getBrand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation;->brand:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance(Lcom/inrix/sdk/model/GeoPoint;)D
    .locals 8
    .param p1, "from"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 94
    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/inrix/sdk/model/GasStation;->latitude:D

    invoke-virtual {p0}, Lcom/inrix/sdk/model/GasStation;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/inrix/sdk/utils/GeoUtils;->distance(DDDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/inrix/sdk/model/GasStation;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/inrix/sdk/model/GasStation;->longitude:D

    return-wide v0
.end method

.method public getProducts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GasStation$Product;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation;->productList:Ljava/util/List;

    return-object v0
.end method

.method public hasProducts()Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation;->productList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation;->productList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAddress(Lcom/inrix/sdk/model/GasStation$Address;)V
    .locals 0
    .param p1, "address"    # Lcom/inrix/sdk/model/GasStation$Address;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation;->address:Lcom/inrix/sdk/model/GasStation$Address;

    .line 141
    return-void
.end method

.method public setBrand(Ljava/lang/String;)V
    .locals 0
    .param p1, "brand"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation;->brand:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation;->id:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "latitude"    # D

    .prologue
    .line 126
    iput-wide p1, p0, Lcom/inrix/sdk/model/GasStation;->latitude:D

    .line 127
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "longitude"    # D

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/inrix/sdk/model/GasStation;->longitude:D

    .line 134
    return-void
.end method

.method public setProducts(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GasStation$Product;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "productList":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/GasStation$Product;>;"
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation;->productList:Ljava/util/List;

    .line 148
    return-void
.end method
