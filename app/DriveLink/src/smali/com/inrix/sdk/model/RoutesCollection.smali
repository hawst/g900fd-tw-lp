.class public Lcom/inrix/sdk/model/RoutesCollection;
.super Lcom/inrix/sdk/parser/xml/XMLEntityBase;
.source "RoutesCollection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/RoutesCollection$Trip;
    }
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private incidentBodiesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation
.end field

.field private incidents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        name = "Incidents"
        required = false
    .end annotation
.end field

.field private trip:Lcom/inrix/sdk/model/RoutesCollection$Trip;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Trip"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/model/RoutesCollection;->incidents:Ljava/util/List;

    .line 71
    return-void
.end method

.method private initIncidentsMap()V
    .locals 5
    .annotation runtime Lorg/simpleframework/xml/core/Validate;
    .end annotation

    .prologue
    .line 39
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/inrix/sdk/model/RoutesCollection;->incidentBodiesMap:Ljava/util/Map;

    .line 40
    iget-object v2, p0, Lcom/inrix/sdk/model/RoutesCollection;->incidents:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/Incident;

    .line 41
    .local v0, "i":Lcom/inrix/sdk/model/Incident;
    iget-object v2, p0, Lcom/inrix/sdk/model/RoutesCollection;->incidentBodiesMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/Incident;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 43
    .end local v0    # "i":Lcom/inrix/sdk/model/Incident;
    :cond_0
    return-void
.end method

.method private matchIncidents()V
    .locals 8
    .annotation runtime Lorg/simpleframework/xml/core/Commit;
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/inrix/sdk/model/RoutesCollection;->getRoutes()Ljava/util/List;

    move-result-object v5

    if-nez v5, :cond_1

    .line 69
    :cond_0
    return-void

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/inrix/sdk/model/RoutesCollection;->getRoutes()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/inrix/sdk/model/Route;

    .line 58
    .local v4, "r":Lcom/inrix/sdk/model/Route;
    invoke-virtual {v4}, Lcom/inrix/sdk/model/Route;->getIncidents()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/inrix/sdk/model/Route;->getIncidents()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 61
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v3, "newIncidents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/inrix/sdk/model/Incident;>;"
    invoke-virtual {v4}, Lcom/inrix/sdk/model/Route;->getIncidents()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/Incident;

    .line 63
    .local v0, "i":Lcom/inrix/sdk/model/Incident;
    iget-object v5, p0, Lcom/inrix/sdk/model/RoutesCollection;->incidentBodiesMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/Incident;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 64
    iget-object v5, p0, Lcom/inrix/sdk/model/RoutesCollection;->incidentBodiesMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/Incident;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 67
    .end local v0    # "i":Lcom/inrix/sdk/model/Incident;
    :cond_4
    invoke-virtual {v4, v3}, Lcom/inrix/sdk/model/Route;->setIncidents(Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public getRoutes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Route;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/inrix/sdk/model/RoutesCollection;->trip:Lcom/inrix/sdk/model/RoutesCollection$Trip;

    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/RoutesCollection;->trip:Lcom/inrix/sdk/model/RoutesCollection$Trip;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/RoutesCollection$Trip;->getRoutes()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
