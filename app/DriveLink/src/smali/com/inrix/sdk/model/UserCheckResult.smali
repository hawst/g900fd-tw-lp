.class public Lcom/inrix/sdk/model/UserCheckResult;
.super Lcom/inrix/sdk/parser/xml/XMLEntityBase;
.source "UserCheckResult.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private isEmailInUse:Z
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "EmailInUse"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;-><init>()V

    return-void
.end method


# virtual methods
.method public isUserExists()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/inrix/sdk/model/UserCheckResult;->isEmailInUse:Z

    return v0
.end method
