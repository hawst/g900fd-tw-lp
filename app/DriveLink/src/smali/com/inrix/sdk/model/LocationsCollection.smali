.class public Lcom/inrix/sdk/model/LocationsCollection;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "LocationsCollection.java"


# instance fields
.field private locations:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getLocations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    iget-object v0, p0, Lcom/inrix/sdk/model/LocationsCollection;->locations:Ljava/util/List;

    return-object v0
.end method
