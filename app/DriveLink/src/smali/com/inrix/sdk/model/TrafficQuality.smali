.class public final Lcom/inrix/sdk/model/TrafficQuality;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "TrafficQuality.java"


# instance fields
.field private result:Lcom/inrix/sdk/model/TrafficQualityResult;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    return-void
.end method


# virtual methods
.method public final getBucket()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    if-nez v0, :cond_0

    .line 50
    const/high16 v0, -0x80000000

    .line 53
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/TrafficQualityResult;->getBucket()I

    move-result v0

    goto :goto_0
.end method

.method public final getCityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/TrafficQualityResult;->getCityName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getCollectionTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/TrafficQualityResult;->getCollectionTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDelayTime()F
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x1

    .line 27
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/TrafficQualityResult;->getDelayTime()F

    move-result v0

    goto :goto_0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/TrafficQualityResult;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getValue()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    if-nez v0, :cond_0

    .line 37
    const/high16 v0, -0x80000000

    .line 40
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQuality;->result:Lcom/inrix/sdk/model/TrafficQualityResult;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/TrafficQualityResult;->getValue()I

    move-result v0

    goto :goto_0
.end method
