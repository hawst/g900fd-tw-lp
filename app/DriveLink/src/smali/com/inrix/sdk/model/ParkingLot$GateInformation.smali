.class public final Lcom/inrix/sdk/model/ParkingLot$GateInformation;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GateInformation"
.end annotation


# instance fields
.field private latitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "latitude"
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "longitude"
    .end annotation
.end field

.field private type:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    iput-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot$GateInformation;->latitude:D

    .line 493
    iput-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot$GateInformation;->longitude:D

    return-void
.end method


# virtual methods
.method public final getLatitude()D
    .locals 2

    .prologue
    .line 512
    iget-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot$GateInformation;->latitude:D

    return-wide v0
.end method

.method public final getLongitude()D
    .locals 2

    .prologue
    .line 522
    iget-wide v0, p0, Lcom/inrix/sdk/model/ParkingLot$GateInformation;->longitude:D

    return-wide v0
.end method

.method public final getType()Lcom/inrix/sdk/model/ParkingLot$GateType;
    .locals 1

    .prologue
    .line 502
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$GateInformation;->type:I

    invoke-static {v0}, Lcom/inrix/sdk/model/ParkingLot$GateType;->fromValue(I)Lcom/inrix/sdk/model/ParkingLot$GateType;

    move-result-object v0

    return-object v0
.end method
