.class public final Lcom/inrix/sdk/model/BoundingBox$BoundingBoxConverter;
.super Ljava/lang/Object;
.source "BoundingBox.java"

# interfaces
.implements Lorg/simpleframework/xml/convert/Converter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/BoundingBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BoundingBoxConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/simpleframework/xml/convert/Converter",
        "<",
        "Lcom/inrix/sdk/model/BoundingBox;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final read(Lorg/simpleframework/xml/stream/InputNode;)Lcom/inrix/sdk/model/BoundingBox;
    .locals 5
    .param p1, "node"    # Lorg/simpleframework/xml/stream/InputNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 24
    const-string/jumbo v2, "Corner1"

    invoke-interface {p1, v2}, Lorg/simpleframework/xml/stream/InputNode;->getAttribute(Ljava/lang/String;)Lorg/simpleframework/xml/stream/InputNode;

    move-result-object v2

    invoke-interface {v2}, Lorg/simpleframework/xml/stream/InputNode;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 25
    .local v0, "corner1Value":Ljava/lang/String;
    const-string/jumbo v2, "Corner2"

    invoke-interface {p1, v2}, Lorg/simpleframework/xml/stream/InputNode;->getAttribute(Ljava/lang/String;)Lorg/simpleframework/xml/stream/InputNode;

    move-result-object v2

    invoke-interface {v2}, Lorg/simpleframework/xml/stream/InputNode;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "corner2Value":Ljava/lang/String;
    new-instance v2, Lcom/inrix/sdk/model/BoundingBox;

    invoke-static {v0}, Lcom/inrix/sdk/model/GeoPoint;->parse(Ljava/lang/String;)Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v3

    invoke-static {v1}, Lcom/inrix/sdk/model/GeoPoint;->parse(Ljava/lang/String;)Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/inrix/sdk/model/BoundingBox;-><init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;)V

    return-object v2
.end method

.method public bridge synthetic read(Lorg/simpleframework/xml/stream/InputNode;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/simpleframework/xml/stream/InputNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/model/BoundingBox$BoundingBoxConverter;->read(Lorg/simpleframework/xml/stream/InputNode;)Lcom/inrix/sdk/model/BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public write(Lorg/simpleframework/xml/stream/OutputNode;Lcom/inrix/sdk/model/BoundingBox;)V
    .locals 2
    .param p1, "node"    # Lorg/simpleframework/xml/stream/OutputNode;
    .param p2, "box"    # Lcom/inrix/sdk/model/BoundingBox;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    const-string/jumbo v0, "Corner1"

    invoke-virtual {p2}, Lcom/inrix/sdk/model/BoundingBox;->getCorner1()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/model/GeoPoint;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/simpleframework/xml/stream/OutputNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Lorg/simpleframework/xml/stream/OutputNode;

    .line 37
    const-string/jumbo v0, "Corner2"

    invoke-virtual {p2}, Lcom/inrix/sdk/model/BoundingBox;->getCorner2()Lcom/inrix/sdk/model/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/inrix/sdk/model/GeoPoint;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/simpleframework/xml/stream/OutputNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Lorg/simpleframework/xml/stream/OutputNode;

    .line 38
    return-void
.end method

.method public bridge synthetic write(Lorg/simpleframework/xml/stream/OutputNode;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Lorg/simpleframework/xml/stream/OutputNode;
    .param p2, "x1"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 16
    check-cast p2, Lcom/inrix/sdk/model/BoundingBox;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/inrix/sdk/model/BoundingBox$BoundingBoxConverter;->write(Lorg/simpleframework/xml/stream/OutputNode;Lcom/inrix/sdk/model/BoundingBox;)V

    return-void
.end method
