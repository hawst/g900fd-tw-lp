.class public Lcom/inrix/sdk/model/SingleGasStationResult;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "SingleGasStationResult.java"


# instance fields
.field private gasStation:Lcom/inrix/sdk/model/GasStation;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 17
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    .line 18
    new-instance v0, Lcom/inrix/sdk/model/GasStation;

    const-string/jumbo v1, "11111"

    const-string/jumbo v2, "UNKNOWN"

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/inrix/sdk/model/GasStation;-><init>(Ljava/lang/String;Ljava/lang/String;DD)V

    iput-object v0, p0, Lcom/inrix/sdk/model/SingleGasStationResult;->gasStation:Lcom/inrix/sdk/model/GasStation;

    .line 19
    return-void
.end method


# virtual methods
.method public getGasStation()Lcom/inrix/sdk/model/GasStation;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/inrix/sdk/model/SingleGasStationResult;->gasStation:Lcom/inrix/sdk/model/GasStation;

    return-object v0
.end method
