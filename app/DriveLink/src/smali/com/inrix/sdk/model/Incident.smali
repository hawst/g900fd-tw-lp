.class public Lcom/inrix/sdk/model/Incident;
.super Ljava/lang/Object;
.source "Incident.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/Incident$Contributor;,
        Lcom/inrix/sdk/model/Incident$Community;,
        Lcom/inrix/sdk/model/Incident$Description;,
        Lcom/inrix/sdk/model/Incident$ParameterizedDescription;,
        Lcom/inrix/sdk/model/Incident$DelayImpact;
    }
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private community:Lcom/inrix/sdk/model/Incident$Community;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Community"
        required = false
    .end annotation
.end field

.field private delayImpact:Lcom/inrix/sdk/model/Incident$DelayImpact;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "DelayImpact"
        required = false
    .end annotation
.end field

.field private distance:D

.field private endTime:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private eventCode:Ljava/lang/Integer;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private fullDescription:Lcom/inrix/sdk/model/Incident$Description;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "FullDesc"
        required = false
    .end annotation
.end field

.field private id:J
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private impacting:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private latitude:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private parameterizedDescription:Lcom/inrix/sdk/model/Incident$ParameterizedDescription;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "ParameterizedDescription"
        required = false
    .end annotation
.end field

.field private severity:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private shortDescription:Lcom/inrix/sdk/model/Incident$Description;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "ShortDesc"
        required = false
    .end annotation
.end field

.field private source:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private startTime:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private type:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field

.field private version:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/inrix/sdk/model/Incident;->id:J

    .line 63
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/inrix/sdk/model/Incident;->distance:D

    .line 437
    return-void
.end method


# virtual methods
.method public getBearingAngle(Landroid/location/Location;D)D
    .locals 7
    .param p1, "userLocation"    # Landroid/location/Location;
    .param p2, "currentHeading"    # D

    .prologue
    .line 224
    new-instance v4, Landroid/location/Location;

    const-string/jumbo v5, ""

    invoke-direct {v4, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 225
    .local v4, "eventLocation":Landroid/location/Location;
    invoke-virtual {p0}, Lcom/inrix/sdk/model/Incident;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/location/Location;->setLatitude(D)V

    .line 226
    invoke-virtual {p0}, Lcom/inrix/sdk/model/Incident;->getLongitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/location/Location;->setLongitude(D)V

    .line 227
    invoke-virtual {p1, v4}, Landroid/location/Location;->bearingTo(Landroid/location/Location;)F

    move-result v5

    float-to-double v2, v5

    .line 229
    .local v2, "directionToIncident":D
    const-wide v5, 0x4070e00000000000L    # 270.0

    cmpl-double v5, p2, v5

    if-lez v5, :cond_1

    const-wide v5, 0x4056800000000000L    # 90.0

    cmpg-double v5, v2, v5

    if-gez v5, :cond_1

    .line 230
    const-wide v5, 0x4076800000000000L    # 360.0

    add-double/2addr v2, v5

    .line 235
    :cond_0
    :goto_0
    sub-double v5, p2, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 237
    .local v0, "angle":D
    return-wide v0

    .line 231
    .end local v0    # "angle":D
    :cond_1
    const-wide v5, 0x4070e00000000000L    # 270.0

    cmpl-double v5, v2, v5

    if-lez v5, :cond_0

    const-wide v5, 0x4056800000000000L    # 90.0

    cmpg-double v5, p2, v5

    if-gez v5, :cond_0

    .line 232
    const-wide v5, 0x4076800000000000L    # 360.0

    add-double/2addr p2, v5

    goto :goto_0
.end method

.method public getCommunity()Lcom/inrix/sdk/model/Incident$Community;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->community:Lcom/inrix/sdk/model/Incident$Community;

    return-object v0
.end method

.method public getDelayImpact()Lcom/inrix/sdk/model/Incident$DelayImpact;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->delayImpact:Lcom/inrix/sdk/model/Incident$DelayImpact;

    return-object v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident;->distance:D

    return-wide v0
.end method

.method public getDistance(Lcom/inrix/sdk/model/GeoPoint;)D
    .locals 8
    .param p1, "from"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 90
    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/inrix/sdk/model/Incident;->latitude:D

    invoke-virtual {p0}, Lcom/inrix/sdk/model/Incident;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/inrix/sdk/utils/GeoUtils;->distance(DDDD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/inrix/sdk/model/Incident;->distance:D

    .line 92
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident;->distance:D

    return-wide v0
.end method

.method public getEndTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->endTime:Ljava/lang/String;

    return-object v0
.end method

.method public getEventCode()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->eventCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public getFullDescription()Lcom/inrix/sdk/model/Incident$Description;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->fullDescription:Lcom/inrix/sdk/model/Incident$Description;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident;->id:J

    return-wide v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 140
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident;->longitude:D

    return-wide v0
.end method

.method public getParameterizedDescription()Lcom/inrix/sdk/model/Incident$ParameterizedDescription;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->parameterizedDescription:Lcom/inrix/sdk/model/Incident$ParameterizedDescription;

    return-object v0
.end method

.method public getSeverity()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/inrix/sdk/model/Incident;->severity:I

    return v0
.end method

.method public getShortDescription()Lcom/inrix/sdk/model/Incident$Description;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->shortDescription:Lcom/inrix/sdk/model/Incident$Description;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident;->startTime:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/inrix/sdk/model/Incident;->type:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/inrix/sdk/model/Incident;->version:I

    return v0
.end method

.method public isImpacting()Z
    .locals 2

    .prologue
    .line 147
    const-string/jumbo v0, "Y"

    iget-object v1, p0, Lcom/inrix/sdk/model/Incident;->impacting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setDistance(D)V
    .locals 0
    .param p1, "value"    # D

    .prologue
    .line 81
    iput-wide p1, p0, Lcom/inrix/sdk/model/Incident;->distance:D

    .line 82
    return-void
.end method
