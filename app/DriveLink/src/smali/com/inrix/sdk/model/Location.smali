.class public Lcom/inrix/sdk/model/Location;
.super Ljava/lang/Object;
.source "Location.java"


# instance fields
.field private address:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Address"
    .end annotation
.end field

.field private consumerId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsumerId"
    .end annotation
.end field

.field private customData:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CustomData"
    .end annotation
.end field

.field private latitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Latitude"
    .end annotation
.end field

.field private locationId:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LocationId"
    .end annotation
.end field

.field private locationType:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LocationType"
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Longitude"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Name"
    .end annotation
.end field

.field private order:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Ordering"
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;DDI)V
    .locals 1
    .param p1, "locationId"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "type"    # I
    .param p5, "address"    # Ljava/lang/String;
    .param p6, "customData"    # Ljava/lang/String;
    .param p7, "lat"    # D
    .param p9, "lon"    # D
    .param p11, "order"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p3, p0, Lcom/inrix/sdk/model/Location;->name:Ljava/lang/String;

    .line 11
    iput-wide p1, p0, Lcom/inrix/sdk/model/Location;->locationId:J

    .line 12
    iput p4, p0, Lcom/inrix/sdk/model/Location;->locationType:I

    .line 13
    iput-object p5, p0, Lcom/inrix/sdk/model/Location;->address:Ljava/lang/String;

    .line 14
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/model/Location;->consumerId:Ljava/lang/String;

    .line 15
    iput-object p6, p0, Lcom/inrix/sdk/model/Location;->customData:Ljava/lang/String;

    .line 16
    iput-wide p7, p0, Lcom/inrix/sdk/model/Location;->latitude:D

    .line 17
    iput-wide p9, p0, Lcom/inrix/sdk/model/Location;->longitude:D

    .line 18
    iput p11, p0, Lcom/inrix/sdk/model/Location;->order:I

    .line 19
    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/inrix/sdk/model/Location;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/inrix/sdk/model/Location;->customData:Ljava/lang/String;

    return-object v0
.end method

.method public getGeoPoint()Lcom/inrix/sdk/model/GeoPoint;
    .locals 5

    .prologue
    .line 57
    new-instance v0, Lcom/inrix/sdk/model/GeoPoint;

    iget-wide v1, p0, Lcom/inrix/sdk/model/Location;->latitude:D

    iget-wide v3, p0, Lcom/inrix/sdk/model/Location;->longitude:D

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/inrix/sdk/model/GeoPoint;-><init>(DD)V

    return-object v0
.end method

.method public getLocationId()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/inrix/sdk/model/Location;->locationId:J

    return-wide v0
.end method

.method public getLocationType()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/inrix/sdk/model/Location;->locationType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/inrix/sdk/model/Location;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/inrix/sdk/model/Location;->order:I

    return v0
.end method
