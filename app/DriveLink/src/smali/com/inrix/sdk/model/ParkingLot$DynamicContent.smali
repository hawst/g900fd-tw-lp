.class public final Lcom/inrix/sdk/model/ParkingLot$DynamicContent;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DynamicContent"
.end annotation


# instance fields
.field private currentCapacity:Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currentCapacity"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCurrentCapacity()Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$DynamicContent;->currentCapacity:Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;

    return-object v0
.end method
