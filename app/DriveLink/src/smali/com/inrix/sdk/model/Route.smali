.class public Lcom/inrix/sdk/model/Route;
.super Ljava/lang/Object;
.source "Route.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/Route$Road;,
        Lcom/inrix/sdk/model/Route$Summary;
    }
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    name = "Route"
    strict = false
.end annotation


# instance fields
.field private abnormalityMinutes:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private averageSpeed:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private boundingBox:Lcom/inrix/sdk/model/BoundingBox;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "BoundingBox"
        required = false
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/convert/Convert;
        value = Lcom/inrix/sdk/model/BoundingBox$BoundingBoxConverter;
    .end annotation
.end field

.field private hasClosures:Z
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private id:J
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private incidents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        name = "Incidents"
        required = false
    .end annotation
.end field

.field private points:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Point;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        name = "Points"
        required = false
    .end annotation
.end field

.field private routeQuality:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private statusId:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private summary:Lcom/inrix/sdk/model/Route$Summary;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Summary"
        required = false
    .end annotation
.end field

.field private totalDistance:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private trafficConsidered:Z
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private travelTimeMinutes:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private uncongestedTravelTimeMinutes:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/model/Route;->incidents:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/model/Route;->points:Ljava/util/List;

    .line 198
    return-void
.end method

.method private encodeDiff(I)Ljava/lang/String;
    .locals 7
    .param p1, "coordinateDiff"    # I

    .prologue
    .line 159
    const/16 v0, 0x1f

    .line 160
    .local v0, "FIVE_BIT_MASK":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 163
    .local v2, "encodedCoordinate":Ljava/lang/StringBuilder;
    shl-int/lit8 v5, p1, 0x1

    .line 164
    .local v5, "shifted":I
    if-gez p1, :cond_0

    .line 165
    xor-int/lit8 v5, v5, -0x1

    .line 168
    :cond_0
    shr-int/lit8 v4, v5, 0x5

    .line 169
    .local v4, "next":I
    if-lez v4, :cond_2

    const/4 v3, 0x1

    .line 170
    .local v3, "hasNext":Z
    :goto_0
    and-int/lit8 v1, v5, 0x1f

    .line 171
    .local v1, "encVal":I
    if-eqz v3, :cond_1

    .line 172
    or-int/lit8 v1, v1, 0x20

    .line 173
    :cond_1
    add-int/lit8 v1, v1, 0x3f

    .line 174
    move v5, v4

    .line 175
    int-to-char v6, v1

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    if-nez v3, :cond_0

    .line 178
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 169
    .end local v1    # "encVal":I
    .end local v3    # "hasNext":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAbnormalityMinutes()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/inrix/sdk/model/Route;->abnormalityMinutes:I

    return v0
.end method

.method public getAverageSpeed()D
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/inrix/sdk/model/Route;->averageSpeed:D

    return-wide v0
.end method

.method public final getBoundingBox()Lcom/inrix/sdk/model/BoundingBox;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/inrix/sdk/model/Route;->boundingBox:Lcom/inrix/sdk/model/BoundingBox;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/inrix/sdk/model/Route;->id:J

    return-wide v0
.end method

.method public getIncidents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/inrix/sdk/model/Route;->incidents:Ljava/util/List;

    return-object v0
.end method

.method public getPoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Point;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/inrix/sdk/model/Route;->points:Ljava/util/List;

    return-object v0
.end method

.method public getPolyline()Ljava/lang/String;
    .locals 9

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 137
    .local v0, "compressed":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/inrix/sdk/model/Route;->getPoints()Ljava/util/List;

    move-result-object v7

    .line 138
    .local v7, "routePoints":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Point;>;"
    const/4 v2, 0x0

    .line 139
    .local v2, "lastLat":I
    const/4 v3, 0x0

    .line 140
    .local v3, "lastLng":I
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/inrix/sdk/model/Point;

    .line 141
    .local v6, "point":Lcom/inrix/sdk/model/Point;
    invoke-virtual {v6}, Lcom/inrix/sdk/model/Point;->getLatitudeE5()I

    move-result v4

    .line 142
    .local v4, "lat":I
    invoke-virtual {v6}, Lcom/inrix/sdk/model/Point;->getLongitudeE5()I

    move-result v5

    .line 143
    .local v5, "lng":I
    sub-int v8, v4, v2

    invoke-direct {p0, v8}, Lcom/inrix/sdk/model/Route;->encodeDiff(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    sub-int v8, v5, v3

    invoke-direct {p0, v8}, Lcom/inrix/sdk/model/Route;->encodeDiff(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    move v2, v4

    .line 146
    move v3, v5

    .line 147
    goto :goto_0

    .line 148
    .end local v4    # "lat":I
    .end local v5    # "lng":I
    .end local v6    # "point":Lcom/inrix/sdk/model/Point;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public getRouteQuality()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/inrix/sdk/model/Route;->routeQuality:I

    return v0
.end method

.method public getStatusId()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/inrix/sdk/model/Route;->statusId:I

    return v0
.end method

.method public getSummary()Lcom/inrix/sdk/model/Route$Summary;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/inrix/sdk/model/Route;->summary:Lcom/inrix/sdk/model/Route$Summary;

    return-object v0
.end method

.method public getTotalDistance()D
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/inrix/sdk/model/Route;->totalDistance:D

    return-wide v0
.end method

.method public getTravelTimeMinutes()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/inrix/sdk/model/Route;->travelTimeMinutes:I

    return v0
.end method

.method public getUncongestedTravelTimeMinutes()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/inrix/sdk/model/Route;->uncongestedTravelTimeMinutes:I

    return v0
.end method

.method public hasClosures()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/inrix/sdk/model/Route;->hasClosures:Z

    return v0
.end method

.method public isTrafficConsidered()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/inrix/sdk/model/Route;->trafficConsidered:Z

    return v0
.end method

.method public setIncidents(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "incidents":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Incident;>;"
    iput-object p1, p0, Lcom/inrix/sdk/model/Route;->incidents:Ljava/util/List;

    .line 67
    return-void
.end method

.method public setPoints(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/Point;>;"
    iput-object p1, p0, Lcom/inrix/sdk/model/Route;->points:Ljava/util/List;

    .line 75
    return-void
.end method
