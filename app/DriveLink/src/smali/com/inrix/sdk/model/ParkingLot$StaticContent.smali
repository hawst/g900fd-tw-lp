.class public final Lcom/inrix/sdk/model/ParkingLot$StaticContent;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StaticContent"
.end annotation


# instance fields
.field private geometry:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Geometry"
    .end annotation
.end field

.field private information:Lcom/inrix/sdk/model/ParkingLot$Information;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ParkingInfo"
    .end annotation
.end field

.field private openingHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHours;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OpeningHours"
    .end annotation
.end field

.field private paymentMethods:Lcom/inrix/sdk/model/ParkingLot$PaymentMethods;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PaymentMethods"
    .end annotation
.end field

.field private pricing:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PricingPayments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot$PricingPayment;",
            ">;"
        }
    .end annotation
.end field

.field private specification:Lcom/inrix/sdk/model/ParkingLot$Specification;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ParkingSpecification"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getGeometry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$StaticContent;->geometry:Ljava/lang/String;

    return-object v0
.end method

.method public final getInformation()Lcom/inrix/sdk/model/ParkingLot$Information;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$StaticContent;->information:Lcom/inrix/sdk/model/ParkingLot$Information;

    return-object v0
.end method

.method public final getOpeningHours()Lcom/inrix/sdk/model/ParkingLot$OpeningHours;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$StaticContent;->openingHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHours;

    return-object v0
.end method

.method public final getPaymentMethods()Lcom/inrix/sdk/model/ParkingLot$PaymentMethods;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$StaticContent;->paymentMethods:Lcom/inrix/sdk/model/ParkingLot$PaymentMethods;

    return-object v0
.end method

.method public final getPricingPayment()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot$PricingPayment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$StaticContent;->pricing:Ljava/util/List;

    return-object v0
.end method

.method public final getSpecification()Lcom/inrix/sdk/model/ParkingLot$Specification;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$StaticContent;->specification:Lcom/inrix/sdk/model/ParkingLot$Specification;

    return-object v0
.end method
