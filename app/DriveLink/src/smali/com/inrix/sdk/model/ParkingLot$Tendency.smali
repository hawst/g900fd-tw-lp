.class public final enum Lcom/inrix/sdk/model/ParkingLot$Tendency;
.super Ljava/lang/Enum;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Tendency"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/ParkingLot$Tendency;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum Emptying:Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum EmptyingQuickly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum EmptyingSlowly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum FfillingQuickly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum Filling:Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum FillingSlowly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum Unchanging:Lcom/inrix/sdk/model/ParkingLot$Tendency;

.field public static final enum Unknown:Lcom/inrix/sdk/model/ParkingLot$Tendency;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 798
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Unknown:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 799
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "FfillingQuickly"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->FfillingQuickly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 800
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "Filling"

    invoke-direct {v0, v1, v6, v6}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Filling:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 801
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "FillingSlowly"

    invoke-direct {v0, v1, v7, v7}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->FillingSlowly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 802
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "Unchanging"

    invoke-direct {v0, v1, v8, v8}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Unchanging:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 803
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "EmptyingSlowly"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->EmptyingSlowly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 804
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "Emptying"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Emptying:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 805
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    const-string/jumbo v1, "EmptyingQuickly"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$Tendency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->EmptyingQuickly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    .line 797
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/inrix/sdk/model/ParkingLot$Tendency;

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Unknown:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Tendency;->FfillingQuickly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Filling:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v1, v0, v6

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Tendency;->FillingSlowly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v1, v0, v7

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Unchanging:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$Tendency;->EmptyingSlowly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Emptying:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$Tendency;->EmptyingQuickly:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    aput-object v2, v0, v1

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$Tendency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 809
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 810
    iput p3, p0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->value:I

    .line 811
    return-void
.end method

.method public static final fromValue(I)Lcom/inrix/sdk/model/ParkingLot$Tendency;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 825
    invoke-static {}, Lcom/inrix/sdk/model/ParkingLot$Tendency;->values()[Lcom/inrix/sdk/model/ParkingLot$Tendency;

    move-result-object v4

    .line 826
    .local v4, "values":[Lcom/inrix/sdk/model/ParkingLot$Tendency;
    move-object v0, v4

    .local v0, "arr$":[Lcom/inrix/sdk/model/ParkingLot$Tendency;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 827
    .local v1, "current":Lcom/inrix/sdk/model/ParkingLot$Tendency;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot$Tendency;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 832
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$Tendency;
    :goto_1
    return-object v1

    .line 826
    .restart local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$Tendency;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 832
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$Tendency;
    :cond_1
    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$Tendency;->Unknown:Lcom/inrix/sdk/model/ParkingLot$Tendency;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/ParkingLot$Tendency;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 797
    const-class v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/ParkingLot$Tendency;
    .locals 1

    .prologue
    .line 797
    sget-object v0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$Tendency;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/ParkingLot$Tendency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/ParkingLot$Tendency;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 814
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$Tendency;->value:I

    return v0
.end method
