.class public Lcom/inrix/sdk/model/Route$Road;
.super Ljava/lang/Object;
.source "Route.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/Route;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Road"
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    name = "Road"
    strict = false
.end annotation


# instance fields
.field private name:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Name"
    .end annotation
.end field

.field private roadClass:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "roadClass"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/inrix/sdk/model/Route$Road;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRoadClass()I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/inrix/sdk/model/Route$Road;->roadClass:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
