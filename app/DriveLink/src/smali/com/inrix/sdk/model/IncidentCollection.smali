.class public Lcom/inrix/sdk/model/IncidentCollection;
.super Lcom/inrix/sdk/parser/xml/XMLEntityBase;
.source "IncidentCollection.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private incidents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        name = "Incidents"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;-><init>()V

    return-void
.end method

.method private validate()V
    .locals 6
    .annotation runtime Lorg/simpleframework/xml/core/Validate;
    .end annotation

    .prologue
    .line 29
    iget-object v2, p0, Lcom/inrix/sdk/model/IncidentCollection;->incidents:Ljava/util/List;

    if-nez v2, :cond_1

    .line 37
    :cond_0
    return-void

    .line 32
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/inrix/sdk/model/IncidentCollection;->incidents:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 33
    iget-object v2, p0, Lcom/inrix/sdk/model/IncidentCollection;->incidents:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/inrix/sdk/model/Incident;

    invoke-virtual {v2}, Lcom/inrix/sdk/model/Incident;->getId()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 34
    iget-object v2, p0, Lcom/inrix/sdk/model/IncidentCollection;->incidents:Ljava/util/List;

    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    .line 32
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getIncidents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/inrix/sdk/model/IncidentCollection;->incidents:Ljava/util/List;

    return-object v0
.end method
