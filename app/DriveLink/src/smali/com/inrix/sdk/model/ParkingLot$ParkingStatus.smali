.class public final enum Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
.super Ljava/lang/Enum;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ParkingStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

.field public static final enum Busy:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

.field public static final enum Closed:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

.field public static final enum Full:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

.field public static final enum NoParkingAllowed:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

.field public static final enum SpecialConditionsApply:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

.field public static final enum Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

.field public static final enum Vacant:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 760
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    const-string/jumbo v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    .line 761
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    const-string/jumbo v1, "Full"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Full:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    .line 762
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    const-string/jumbo v1, "Busy"

    invoke-direct {v0, v1, v6, v6}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Busy:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    .line 763
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    const-string/jumbo v1, "Vacant"

    invoke-direct {v0, v1, v7, v7}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Vacant:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    .line 764
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    const-string/jumbo v1, "Closed"

    invoke-direct {v0, v1, v8, v8}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Closed:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    .line 765
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    const-string/jumbo v1, "NoParkingAllowed"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->NoParkingAllowed:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    .line 766
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    const-string/jumbo v1, "SpecialConditionsApply"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->SpecialConditionsApply:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    .line 759
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Full:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Busy:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Vacant:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Closed:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->NoParkingAllowed:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->SpecialConditionsApply:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 770
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 771
    iput p3, p0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->value:I

    .line 772
    return-void
.end method

.method public static final fromValue(I)Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 786
    invoke-static {}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->values()[Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    move-result-object v4

    .line 787
    .local v4, "values":[Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    move-object v0, v4

    .local v0, "arr$":[Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 788
    .local v1, "current":Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 793
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    :goto_1
    return-object v1

    .line 787
    .restart local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 793
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    :cond_1
    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 759
    const-class v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    .locals 1

    .prologue
    .line 759
    sget-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 775
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->value:I

    return v0
.end method
