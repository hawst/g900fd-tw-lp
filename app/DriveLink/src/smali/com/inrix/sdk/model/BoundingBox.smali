.class public final Lcom/inrix/sdk/model/BoundingBox;
.super Ljava/lang/Object;
.source "BoundingBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/BoundingBox$BoundingBoxConverter;
    }
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private corner1:Lcom/inrix/sdk/model/GeoPoint;

.field private corner2:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Lcom/inrix/sdk/model/GeoPoint;)V
    .locals 0
    .param p1, "corner1"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "corner2"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/inrix/sdk/model/BoundingBox;->corner1:Lcom/inrix/sdk/model/GeoPoint;

    .line 60
    iput-object p2, p0, Lcom/inrix/sdk/model/BoundingBox;->corner2:Lcom/inrix/sdk/model/GeoPoint;

    .line 61
    return-void
.end method


# virtual methods
.method public final getCorner1()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/inrix/sdk/model/BoundingBox;->corner1:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public final getCorner2()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/inrix/sdk/model/BoundingBox;->corner2:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method
