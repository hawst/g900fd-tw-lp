.class public Lcom/inrix/sdk/model/SingleLocationCollection;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "SingleLocationCollection.java"


# instance fields
.field private location:Lcom/inrix/sdk/model/Location;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getLocation()Lcom/inrix/sdk/model/Location;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/inrix/sdk/model/SingleLocationCollection;->location:Lcom/inrix/sdk/model/Location;

    return-object v0
.end method
