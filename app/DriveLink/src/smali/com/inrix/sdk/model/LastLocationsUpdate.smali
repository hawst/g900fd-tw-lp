.class public Lcom/inrix/sdk/model/LastLocationsUpdate;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "LastLocationsUpdate.java"


# instance fields
.field private final INRIX_DEFAULT_TIMEZONE:Ljava/lang/String;

.field private final INRIX_TIME_FORMAT:Ljava/lang/String;

.field private lastCustomRoutesUpdate:Ljava/util/Date;

.field private lastDepartureAlertsUpdate:Ljava/util/Date;

.field private lastLocationsUpdate:Ljava/util/Date;

.field private result:Lcom/inrix/sdk/model/Result;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    .line 17
    const-string/jumbo v0, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    iput-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->INRIX_TIME_FORMAT:Ljava/lang/String;

    .line 18
    const-string/jumbo v0, "UTC"

    iput-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->INRIX_DEFAULT_TIMEZONE:Ljava/lang/String;

    return-void
.end method

.method private getDateFromString(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p1, "str"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    const/4 v2, 0x0

    .line 91
    :goto_0
    return-object v2

    .line 83
    :cond_0
    const/4 v2, 0x0

    .line 84
    .local v2, "result":Ljava/util/Date;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 85
    .local v1, "format":Ljava/text/DateFormat;
    const-string/jumbo v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 87
    :try_start_0
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getLastCustomRoutesUpdate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->result:Lcom/inrix/sdk/model/Result;

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 58
    :goto_0
    return-object v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastCustomRoutesUpdate:Ljava/util/Date;

    if-nez v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->result:Lcom/inrix/sdk/model/Result;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/Result;->getLastCustomRoutesUpdateStr()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/inrix/sdk/model/LastLocationsUpdate;->getDateFromString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastCustomRoutesUpdate:Ljava/util/Date;

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastCustomRoutesUpdate:Ljava/util/Date;

    goto :goto_0
.end method

.method public getLastDepartureAlertsUpdate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->result:Lcom/inrix/sdk/model/Result;

    if-nez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastDepartureAlertsUpdate:Ljava/util/Date;

    if-nez v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->result:Lcom/inrix/sdk/model/Result;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/Result;->getLastDepartureAlertsUpdateStr()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/inrix/sdk/model/LastLocationsUpdate;->getDateFromString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastDepartureAlertsUpdate:Ljava/util/Date;

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastDepartureAlertsUpdate:Ljava/util/Date;

    goto :goto_0
.end method

.method public getLastLocationsUpdate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->result:Lcom/inrix/sdk/model/Result;

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 41
    :goto_0
    return-object v0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastLocationsUpdate:Ljava/util/Date;

    if-nez v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->result:Lcom/inrix/sdk/model/Result;

    invoke-virtual {v0}, Lcom/inrix/sdk/model/Result;->getLastLocationsUpdateStr()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/inrix/sdk/model/LastLocationsUpdate;->getDateFromString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastLocationsUpdate:Ljava/util/Date;

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/inrix/sdk/model/LastLocationsUpdate;->lastLocationsUpdate:Ljava/util/Date;

    goto :goto_0
.end method
