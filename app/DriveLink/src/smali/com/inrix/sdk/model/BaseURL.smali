.class public Lcom/inrix/sdk/model/BaseURL;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "BaseURL.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;,
        Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;
    }
.end annotation


# static fields
.field public static final AUTHENTICATION_LABEL:Ljava/lang/String; = "Auth"

.field public static final REGISTRATION_LABEL:Ljava/lang/String; = "Registration"


# instance fields
.field private authenticationURL:Ljava/lang/String;

.field private registrationURL:Ljava/lang/String;

.field private urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Result"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    .line 30
    iput-object v0, p0, Lcom/inrix/sdk/model/BaseURL;->registrationURL:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/inrix/sdk/model/BaseURL;->authenticationURL:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public getAuthenticationURL()Ljava/lang/String;
    .locals 4

    .prologue
    .line 78
    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->authenticationURL:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    iget-object v2, v2, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverEndPoints:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    iget-object v2, v2, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverEndPoints:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 81
    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    iget-object v2, v2, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverEndPoints:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;

    .line 82
    .local v1, "sep":Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;
    iget-object v2, v1, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointType:Ljava/lang/String;

    const-string/jumbo v3, "Auth"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    iget-object v2, v1, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointURL:Ljava/lang/String;

    iput-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->authenticationURL:Ljava/lang/String;

    .line 88
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sep":Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;
    :cond_1
    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->authenticationURL:Ljava/lang/String;

    return-object v2
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    iget-object v0, v0, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverRegion:Ljava/lang/String;

    .line 101
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRegistrationURL()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->registrationURL:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    iget-object v2, v2, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverEndPoints:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    iget-object v2, v2, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverEndPoints:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 62
    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    iget-object v2, v2, Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;->serverEndPoints:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;

    .line 63
    .local v1, "sep":Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;
    iget-object v2, v1, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointType:Ljava/lang/String;

    const-string/jumbo v3, "Registration"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    iget-object v2, v1, Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;->endPointURL:Ljava/lang/String;

    iput-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->registrationURL:Ljava/lang/String;

    .line 69
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sep":Lcom/inrix/sdk/model/BaseURL$ServerEndPoint;
    :cond_1
    iget-object v2, p0, Lcom/inrix/sdk/model/BaseURL;->registrationURL:Ljava/lang/String;

    return-object v2
.end method

.method public getUrlInfo()Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    return-object v0
.end method

.method public setUrlInfo(Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;)V
    .locals 0
    .param p1, "entity"    # Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/inrix/sdk/model/BaseURL;->urlEntity:Lcom/inrix/sdk/model/BaseURL$BaseURLEntity;

    .line 51
    return-void
.end method
