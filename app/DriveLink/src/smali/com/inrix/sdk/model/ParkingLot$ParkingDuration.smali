.class public final Lcom/inrix/sdk/model/ParkingLot$ParkingDuration;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ParkingDuration"
.end annotation


# instance fields
.field private hours:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hours"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getHours()I
    .locals 1

    .prologue
    .line 738
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$ParkingDuration;->hours:I

    return v0
.end method
