.class public final enum Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
.super Ljava/lang/Enum;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OpeningHoursType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

.field public static final enum EntryHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

.field public static final enum ExitHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

.field public static final enum MaximumStayTime:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

.field public static final enum Unknown:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 527
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    const-string/jumbo v1, "Unknown"

    invoke-direct {v0, v1, v2, v2}, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    .line 528
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    const-string/jumbo v1, "EntryHours"

    invoke-direct {v0, v1, v3, v3}, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->EntryHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    .line 529
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    const-string/jumbo v1, "ExitHours"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->ExitHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    .line 530
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    const-string/jumbo v1, "MaximumStayTime"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->MaximumStayTime:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    .line 526
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->EntryHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->ExitHours:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->MaximumStayTime:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 534
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 535
    iput p3, p0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->value:I

    .line 536
    return-void
.end method

.method public static final fromValue(I)Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 550
    invoke-static {}, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->values()[Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    move-result-object v4

    .line 551
    .local v4, "values":[Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    move-object v0, v4

    .local v0, "arr$":[Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 552
    .local v1, "current":Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 557
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    :goto_1
    return-object v1

    .line 551
    .restart local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 557
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    :cond_1
    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 526
    const-class v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 539
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->value:I

    return v0
.end method
