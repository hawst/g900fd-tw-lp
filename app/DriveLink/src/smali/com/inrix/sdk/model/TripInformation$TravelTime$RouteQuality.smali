.class public final enum Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;
.super Ljava/lang/Enum;
.source "TripInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/TripInformation$TravelTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RouteQuality"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

.field public static final enum Closed:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

.field public static final enum FreeFlow:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

.field public static final enum Heavy:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

.field public static final enum Moderate:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

.field public static final enum StopAndGo:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

.field public static final enum Unknown:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;


# instance fields
.field public routeQualityId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 295
    new-instance v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    const-string/jumbo v1, "StopAndGo"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->StopAndGo:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    new-instance v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    const-string/jumbo v1, "Heavy"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Heavy:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    new-instance v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    const-string/jumbo v1, "Moderate"

    invoke-direct {v0, v1, v6, v6}, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Moderate:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    new-instance v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    const-string/jumbo v1, "FreeFlow"

    invoke-direct {v0, v1, v7, v7}, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->FreeFlow:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    new-instance v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    const-string/jumbo v1, "Closed"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v8, v2}, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Closed:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    new-instance v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    const-string/jumbo v1, "Unknown"

    const/4 v2, 0x5

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Unknown:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    .line 294
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    sget-object v1, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->StopAndGo:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Heavy:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Moderate:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    aput-object v1, v0, v6

    sget-object v1, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->FreeFlow:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    aput-object v1, v0, v7

    sget-object v1, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Closed:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->Unknown:Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    aput-object v2, v0, v1

    sput-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->$VALUES:[Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "pRouteQualityId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 299
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 300
    iput p3, p0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->routeQualityId:I

    .line 301
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 294
    const-class v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->$VALUES:[Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/TripInformation$TravelTime$RouteQuality;

    return-object v0
.end method
