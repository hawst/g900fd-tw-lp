.class public Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;
.super Ljava/lang/Object;
.source "DeviceAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/DeviceAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceAuthEntity"
.end annotation


# instance fields
.field public analyticsServer:Ljava/lang/String;

.field public camerasServer:Ljava/lang/String;

.field public compositeTilesServer:Ljava/lang/String;

.field public dustServer:Ljava/lang/String;

.field public fuelServer:Ljava/lang/String;

.field public incidentsServer:Ljava/lang/String;

.field public mobileServer:Ljava/lang/String;

.field public parkingServer:Ljava/lang/String;

.field public routingServer:Ljava/lang/String;

.field public serverURLs:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ServerPaths"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;",
            ">;"
        }
    .end annotation
.end field

.field public settingsServer:Ljava/lang/String;

.field public token:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Token"
    .end annotation
.end field

.field public tokenExpireDateUTC:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TokenExpireDtUtc"
    .end annotation
.end field

.field public trafficStateServer:Ljava/lang/String;

.field public travelDataServer:Ljava/lang/String;

.field public ttsServer:Ljava/lang/String;

.field public varNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->mobileServer:Ljava/lang/String;

    .line 121
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->ttsServer:Ljava/lang/String;

    .line 122
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->analyticsServer:Ljava/lang/String;

    .line 123
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->camerasServer:Ljava/lang/String;

    .line 124
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->compositeTilesServer:Ljava/lang/String;

    .line 125
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->dustServer:Ljava/lang/String;

    .line 126
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->fuelServer:Ljava/lang/String;

    .line 127
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->incidentsServer:Ljava/lang/String;

    .line 128
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->parkingServer:Ljava/lang/String;

    .line 129
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->routingServer:Ljava/lang/String;

    .line 130
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->settingsServer:Ljava/lang/String;

    .line 131
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->trafficStateServer:Ljava/lang/String;

    .line 132
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->travelDataServer:Ljava/lang/String;

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    .line 109
    invoke-direct {p0}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->buildVarNameMap()V

    .line 110
    return-void
.end method

.method private buildVarNameMap()V
    .locals 3

    .prologue
    .line 237
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    .line 238
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->MOBILE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "mobileServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ttsServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ANALYTICS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "analyticsServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->CAMERAS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "camerasServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->COMPOSITE_TILES:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "compositeTilesServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->DUST:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "dustServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->FUEL:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "fuelServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->INCIDENTS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "incidentsServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->PARKING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "parkingServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ROUTING:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "routingServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->SETTINGS:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "settingsServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAFFIC_STATE:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "trafficStateServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    sget-object v1, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->TRAVEL_DATA:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    invoke-static {v1}, Lcom/inrix/sdk/auth/AuthHelper;->resolveServerStringFromType(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "travelDataServer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    return-void
.end method

.method private storeServerEndPoint(Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;)V
    .locals 3
    .param p1, "urlEnt"    # Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;

    .prologue
    .line 171
    if-eqz p1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->varNameMap:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;->endPointType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 174
    .local v0, "field":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 177
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iget-object v2, p1, Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;->url:Ljava/lang/String;

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v0    # "field":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 178
    .restart local v0    # "field":Ljava/lang/String;
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public getURL(Lcom/inrix/sdk/network/request/InrixRequest$ServerType;)Ljava/lang/String;
    .locals 3
    .param p1, "endPointType"    # Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    .prologue
    .line 185
    const/4 v0, 0x0

    .line 186
    .local v0, "returnURL":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 187
    sget-object v1, Lcom/inrix/sdk/model/DeviceAuth$1;->$SwitchMap$com$inrix$sdk$network$request$InrixRequest$ServerType:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 228
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->mobileServer:Ljava/lang/String;

    .line 232
    :cond_0
    :goto_0
    return-object v0

    .line 189
    :pswitch_0
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->mobileServer:Ljava/lang/String;

    .line 190
    goto :goto_0

    .line 192
    :pswitch_1
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->ttsServer:Ljava/lang/String;

    .line 193
    goto :goto_0

    .line 195
    :pswitch_2
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->analyticsServer:Ljava/lang/String;

    .line 196
    goto :goto_0

    .line 198
    :pswitch_3
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->camerasServer:Ljava/lang/String;

    .line 199
    goto :goto_0

    .line 201
    :pswitch_4
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->compositeTilesServer:Ljava/lang/String;

    .line 202
    goto :goto_0

    .line 204
    :pswitch_5
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->dustServer:Ljava/lang/String;

    .line 205
    goto :goto_0

    .line 207
    :pswitch_6
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->fuelServer:Ljava/lang/String;

    .line 208
    goto :goto_0

    .line 210
    :pswitch_7
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->incidentsServer:Ljava/lang/String;

    .line 211
    goto :goto_0

    .line 213
    :pswitch_8
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->parkingServer:Ljava/lang/String;

    .line 214
    goto :goto_0

    .line 216
    :pswitch_9
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->routingServer:Ljava/lang/String;

    .line 217
    goto :goto_0

    .line 219
    :pswitch_a
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->settingsServer:Ljava/lang/String;

    .line 220
    goto :goto_0

    .line 222
    :pswitch_b
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->trafficStateServer:Ljava/lang/String;

    .line 223
    goto :goto_0

    .line 225
    :pswitch_c
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->travelDataServer:Ljava/lang/String;

    .line 226
    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public parseEndPointsFromServer()Z
    .locals 5

    .prologue
    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "bReturn":Z
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getRegion()Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, "region":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 140
    sget-object v4, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->NA:Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;

    invoke-virtual {v4}, Lcom/inrix/sdk/network/request/InrixRequest$ServerRegion;->name()Ljava/lang/String;

    move-result-object v2

    .line 142
    :cond_0
    iget-object v4, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->serverURLs:Ljava/util/List;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->serverURLs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 143
    iget-object v4, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->serverURLs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;

    .line 144
    .local v3, "urlEnt":Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;
    iget-object v4, v3, Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;->region:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 145
    const/4 v0, 0x1

    .line 146
    invoke-direct {p0, v3}, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->storeServerEndPoint(Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;)V

    goto :goto_0

    .line 150
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "urlEnt":Lcom/inrix/sdk/model/DeviceAuth$ServerURLEntity;
    :cond_2
    return v0
.end method

.method public storeEndPointsInUserPreferences()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->mobileServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setMobileServer(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->ttsServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setTTSServer(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->analyticsServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setAnalyticsServer(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->camerasServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setCamerasServer(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->compositeTilesServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setCompositeTilesServer(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->dustServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setDustServer(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->fuelServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setFuelServer(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->incidentsServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setIncidentsServer(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->parkingServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setParkingServer(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->routingServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setRoutingServer(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->settingsServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setSettingsServer(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->trafficStateServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setTrafficStateServer(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/inrix/sdk/model/DeviceAuth$DeviceAuthEntity;->travelDataServer:Ljava/lang/String;

    invoke-static {v0}, Lcom/inrix/sdk/utils/UserPreferences;->setTravelDataServer(Ljava/lang/String;)V

    .line 167
    return-void
.end method
