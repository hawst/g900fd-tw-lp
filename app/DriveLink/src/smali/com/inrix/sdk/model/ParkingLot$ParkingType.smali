.class public final enum Lcom/inrix/sdk/model/ParkingLot$ParkingType;
.super Ljava/lang/Enum;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ParkingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/ParkingLot$ParkingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Campground:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Carpool:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Covered:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Downtown:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum DropoffMechanical:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum DropoffWithValet:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Field:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Highway:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum KissAndRide:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum MultiStorey:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Nested:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum OpenSpace:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum ParkAndRide:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum ParkingZone:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Roadside:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Special:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Temporary:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Underground:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

.field public static final enum Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 354
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 355
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Special"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Special:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 356
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "OpenSpace"

    invoke-direct {v0, v1, v6, v6}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->OpenSpace:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 357
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "MultiStorey"

    invoke-direct {v0, v1, v7, v7}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->MultiStorey:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 358
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Underground"

    invoke-direct {v0, v1, v8, v8}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Underground:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 359
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Covered"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Covered:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 360
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Nested"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Nested:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 361
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Field"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Field:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 362
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Roadside"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Roadside:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 363
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "DropoffWithValet"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->DropoffWithValet:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 364
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "DropoffMechanical"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->DropoffMechanical:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 365
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Highway"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Highway:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 366
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "ParkAndRide"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->ParkAndRide:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 367
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Carpool"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Carpool:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 368
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Campground"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Campground:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 369
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "ParkingZone"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->ParkingZone:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 370
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Downtown"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Downtown:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 371
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "Temporary"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Temporary:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 372
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    const-string/jumbo v1, "KissAndRide"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->KissAndRide:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    .line 353
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Special:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->OpenSpace:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->MultiStorey:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Underground:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Covered:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Nested:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Field:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Roadside:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->DropoffWithValet:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->DropoffMechanical:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Highway:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->ParkAndRide:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Carpool:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Campground:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->ParkingZone:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Downtown:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Temporary:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->KissAndRide:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 376
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 377
    iput p3, p0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->value:I

    .line 378
    return-void
.end method

.method public static final fromValue(I)Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 392
    invoke-static {}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->values()[Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    move-result-object v4

    .line 393
    .local v4, "values":[Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    move-object v0, v4

    .local v0, "arr$":[Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 394
    .local v1, "current":Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 399
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    :goto_1
    return-object v1

    .line 393
    .restart local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 399
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    :cond_1
    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 353
    const-class v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/ParkingLot$ParkingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->value:I

    return v0
.end method
