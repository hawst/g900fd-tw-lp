.class public Lcom/inrix/sdk/model/GasStationCollection;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "GasStationCollection.java"


# instance fields
.field private gasStations:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GasStation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStationCollection;->gasStations:Ljava/util/List;

    .line 22
    return-void
.end method


# virtual methods
.method public getGasStations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/GasStation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStationCollection;->gasStations:Ljava/util/List;

    return-object v0
.end method
