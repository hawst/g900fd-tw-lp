.class public Lcom/inrix/sdk/model/TripInformation;
.super Ljava/lang/Object;
.source "TripInformation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/model/TripInformation$TravelTime;,
        Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;,
        Lcom/inrix/sdk/model/TripInformation$TravelPoint;
    }
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private description:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private destination:Lcom/inrix/sdk/model/TripInformation$TravelPoint;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Destination"
    .end annotation
.end field

.field private id:J
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private origin:Lcom/inrix/sdk/model/TripInformation$TravelPoint;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Origin"
    .end annotation
.end field

.field private route:Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Route"
    .end annotation
.end field

.field private statusId:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private voiceTag:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDestination()Lcom/inrix/sdk/model/GeoPoint;
    .locals 5

    .prologue
    .line 97
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation;->destination:Lcom/inrix/sdk/model/TripInformation$TravelPoint;

    if-eqz v0, :cond_0

    .line 98
    new-instance v0, Lcom/inrix/sdk/model/GeoPoint;

    iget-object v1, p0, Lcom/inrix/sdk/model/TripInformation;->destination:Lcom/inrix/sdk/model/TripInformation$TravelPoint;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->getLatitude()D

    move-result-wide v1

    iget-object v3, p0, Lcom/inrix/sdk/model/TripInformation;->destination:Lcom/inrix/sdk/model/TripInformation$TravelPoint;

    invoke-virtual {v3}, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/inrix/sdk/model/GeoPoint;-><init>(DD)V

    .line 101
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOrigin()Lcom/inrix/sdk/model/GeoPoint;
    .locals 5

    .prologue
    .line 85
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation;->origin:Lcom/inrix/sdk/model/TripInformation$TravelPoint;

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Lcom/inrix/sdk/model/GeoPoint;

    iget-object v1, p0, Lcom/inrix/sdk/model/TripInformation;->origin:Lcom/inrix/sdk/model/TripInformation$TravelPoint;

    invoke-virtual {v1}, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->getLatitude()D

    move-result-wide v1

    iget-object v3, p0, Lcom/inrix/sdk/model/TripInformation;->origin:Lcom/inrix/sdk/model/TripInformation$TravelPoint;

    invoke-virtual {v3}, Lcom/inrix/sdk/model/TripInformation$TravelPoint;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/inrix/sdk/model/GeoPoint;-><init>(DD)V

    .line 88
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRoute()Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation;->route:Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;

    return-object v0
.end method

.method public getStatusId()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/inrix/sdk/model/TripInformation;->statusId:I

    return v0
.end method

.method public getTripID()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/inrix/sdk/model/TripInformation;->id:J

    return-wide v0
.end method

.method public getVoiceTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation;->voiceTag:Ljava/lang/String;

    return-object v0
.end method
