.class public Lcom/inrix/sdk/model/Incident$Community;
.super Ljava/lang/Object;
.source "Incident.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/Incident;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Community"
.end annotation


# instance fields
.field private accuracy:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private contributor:Lcom/inrix/sdk/model/Incident$Contributor;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Contributor"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccuracy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$Community;->accuracy:Ljava/lang/String;

    return-object v0
.end method

.method public getContributor()Lcom/inrix/sdk/model/Incident$Contributor;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$Community;->contributor:Lcom/inrix/sdk/model/Incident$Contributor;

    return-object v0
.end method
