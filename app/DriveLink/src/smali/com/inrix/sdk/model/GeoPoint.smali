.class public final Lcom/inrix/sdk/model/GeoPoint;
.super Ljava/lang/Object;
.source "GeoPoint.java"


# static fields
.field private static final MAX_LATITUDE:D = 90.0

.field private static final MAX_LONGITUDE:D = 180.0

.field private static final MIN_LATITUDE:D = -90.0

.field private static final MIN_LONGITUDE:D = -180.0

.field private static final SEPARTOR_COMMA:Ljava/lang/String; = ","


# instance fields
.field private final latitude:D

.field private final longitude:D


# direct methods
.method public constructor <init>(DD)V
    .locals 0
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-wide p1, p0, Lcom/inrix/sdk/model/GeoPoint;->latitude:D

    .line 57
    iput-wide p3, p0, Lcom/inrix/sdk/model/GeoPoint;->longitude:D

    .line 58
    return-void
.end method

.method public static isValid(Lcom/inrix/sdk/model/GeoPoint;)Z
    .locals 5
    .param p0, "point"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    const/4 v0, 0x0

    .line 28
    if-nez p0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 32
    :cond_1
    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-nez v1, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v1

    const-wide v3, -0x3fa9800000000000L    # -90.0

    cmpg-double v1, v1, v3

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLatitude()D

    move-result-wide v1

    const-wide v3, 0x4056800000000000L    # 90.0

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_0

    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v1

    const-wide v3, -0x3f99800000000000L    # -180.0

    cmpg-double v1, v1, v3

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/inrix/sdk/model/GeoPoint;->getLongitude()D

    move-result-wide v1

    const-wide v3, 0x4066800000000000L    # 180.0

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_0

    .line 43
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final parse(Ljava/lang/String;)Lcom/inrix/sdk/model/GeoPoint;
    .locals 7
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 96
    new-instance v5, Ljava/security/InvalidParameterException;

    const-string/jumbo v6, "Value can\'t be null."

    invoke-direct {v5, v6}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 99
    :cond_0
    const-string/jumbo v5, ","

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "chunks":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    .line 101
    .local v1, "latitude":D
    const/4 v5, 0x1

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    .line 102
    .local v3, "longitude":D
    new-instance v5, Lcom/inrix/sdk/model/GeoPoint;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/inrix/sdk/model/GeoPoint;-><init>(DD)V

    return-object v5
.end method


# virtual methods
.method public final getLatitude()D
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/inrix/sdk/model/GeoPoint;->latitude:D

    return-wide v0
.end method

.method public final getLongitude()D
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/inrix/sdk/model/GeoPoint;->longitude:D

    return-wide v0
.end method

.method public final toQueryParam()Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/inrix/sdk/model/GeoPoint;->latitude:D

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/inrix/sdk/model/GeoPoint;->longitude:D

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/inrix/sdk/model/GeoPoint;->latitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/inrix/sdk/model/GeoPoint;->longitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
