.class public Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;
.super Ljava/lang/Object;
.source "TripInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/TripInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteTravelTime"
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private id:J
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private routeInstanceId:J
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private travelTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/TripInformation$TravelTime;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        name = "TravelTimes"
    .end annotation
.end field

.field private uncongestedTravelTimeMinutes:I
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->id:J

    .line 191
    iput-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->routeInstanceId:J

    .line 192
    const/4 v0, 0x0

    iput v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->uncongestedTravelTimeMinutes:I

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->travelTimes:Ljava/util/List;

    .line 194
    return-void
.end method


# virtual methods
.method public getRouteID()J
    .locals 2

    .prologue
    .line 202
    iget-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->id:J

    return-wide v0
.end method

.method public getRouteInstanceID()J
    .locals 2

    .prologue
    .line 211
    iget-wide v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->routeInstanceId:J

    return-wide v0
.end method

.method public getTravelTimes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/TripInformation$TravelTime;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->travelTimes:Ljava/util/List;

    return-object v0
.end method

.method public getTravelTimesInterval()I
    .locals 13

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 240
    const-wide/32 v0, 0xea60

    .line 241
    .local v0, "MS_PER_MINUTE":J
    iget-object v9, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->travelTimes:Ljava/util/List;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->travelTimes:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-le v9, v10, :cond_0

    .line 242
    iget-object v9, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->travelTimes:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/inrix/sdk/model/TripInformation$TravelTime;

    .line 243
    .local v8, "travelTimeZero":Lcom/inrix/sdk/model/TripInformation$TravelTime;
    iget-object v9, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->travelTimes:Ljava/util/List;

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/inrix/sdk/model/TripInformation$TravelTime;

    .line 244
    .local v7, "travelTimeOne":Lcom/inrix/sdk/model/TripInformation$TravelTime;
    invoke-virtual {v8}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getDepartureTime()Ljava/util/Date;

    move-result-object v9

    if-nez v9, :cond_1

    invoke-virtual {v8}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getArrivalTime()Ljava/util/Date;

    move-result-object v2

    .line 246
    .local v2, "date0":Ljava/util/Date;
    :goto_0
    invoke-virtual {v7}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getDepartureTime()Ljava/util/Date;

    move-result-object v9

    if-nez v9, :cond_2

    invoke-virtual {v7}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getArrivalTime()Ljava/util/Date;

    move-result-object v3

    .line 248
    .local v3, "date1":Ljava/util/Date;
    :goto_1
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 249
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    sub-long v4, v9, v11

    .line 250
    .local v4, "timeDiffMS":J
    div-long v9, v4, v0

    long-to-int v6, v9

    .line 254
    .end local v2    # "date0":Ljava/util/Date;
    .end local v3    # "date1":Ljava/util/Date;
    .end local v4    # "timeDiffMS":J
    .end local v7    # "travelTimeOne":Lcom/inrix/sdk/model/TripInformation$TravelTime;
    .end local v8    # "travelTimeZero":Lcom/inrix/sdk/model/TripInformation$TravelTime;
    :cond_0
    return v6

    .line 244
    .restart local v7    # "travelTimeOne":Lcom/inrix/sdk/model/TripInformation$TravelTime;
    .restart local v8    # "travelTimeZero":Lcom/inrix/sdk/model/TripInformation$TravelTime;
    :cond_1
    invoke-virtual {v8}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getDepartureTime()Ljava/util/Date;

    move-result-object v2

    goto :goto_0

    .line 246
    .restart local v2    # "date0":Ljava/util/Date;
    :cond_2
    invoke-virtual {v7}, Lcom/inrix/sdk/model/TripInformation$TravelTime;->getDepartureTime()Ljava/util/Date;

    move-result-object v3

    goto :goto_1
.end method

.method public getUncongestedTravelTime()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/inrix/sdk/model/TripInformation$RouteTravelTime;->uncongestedTravelTimeMinutes:I

    return v0
.end method
