.class public Lcom/inrix/sdk/model/GasStation$Address;
.super Ljava/lang/Object;
.source "GasStation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/GasStation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Address"
.end annotation


# instance fields
.field private city:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "city"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field

.field private phoneNumber:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "phoneNumber"
    .end annotation
.end field

.field private state:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "state"
    .end annotation
.end field

.field private street:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "street"
    .end annotation
.end field

.field final synthetic this$0:Lcom/inrix/sdk/model/GasStation;

.field private zipCode:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "zipCode"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GasStation;)V
    .locals 1

    .prologue
    .line 307
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->this$0:Lcom/inrix/sdk/model/GasStation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->name:Ljava/lang/String;

    .line 309
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->street:Ljava/lang/String;

    .line 310
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->city:Ljava/lang/String;

    .line 311
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->state:Ljava/lang/String;

    .line 312
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->zipCode:Ljava/lang/String;

    .line 313
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->phoneNumber:Ljava/lang/String;

    .line 314
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GasStation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "street"    # Ljava/lang/String;
    .param p4, "city"    # Ljava/lang/String;
    .param p5, "state"    # Ljava/lang/String;
    .param p6, "zipCode"    # Ljava/lang/String;
    .param p7, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->this$0:Lcom/inrix/sdk/model/GasStation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 326
    iput-object p2, p0, Lcom/inrix/sdk/model/GasStation$Address;->name:Ljava/lang/String;

    .line 327
    iput-object p3, p0, Lcom/inrix/sdk/model/GasStation$Address;->street:Ljava/lang/String;

    .line 328
    iput-object p4, p0, Lcom/inrix/sdk/model/GasStation$Address;->city:Ljava/lang/String;

    .line 329
    iput-object p5, p0, Lcom/inrix/sdk/model/GasStation$Address;->state:Ljava/lang/String;

    .line 330
    iput-object p6, p0, Lcom/inrix/sdk/model/GasStation$Address;->zipCode:Ljava/lang/String;

    .line 331
    iput-object p7, p0, Lcom/inrix/sdk/model/GasStation$Address;->phoneNumber:Ljava/lang/String;

    .line 332
    return-void
.end method


# virtual methods
.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getStreet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->street:Ljava/lang/String;

    return-object v0
.end method

.method public getZipCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Address;->zipCode:Ljava/lang/String;

    return-object v0
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .param p1, "city"    # Ljava/lang/String;

    .prologue
    .line 400
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->city:Ljava/lang/String;

    .line 401
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 386
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->name:Ljava/lang/String;

    .line 387
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 421
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->phoneNumber:Ljava/lang/String;

    .line 422
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->state:Ljava/lang/String;

    .line 408
    return-void
.end method

.method public setStreet(Ljava/lang/String;)V
    .locals 0
    .param p1, "street"    # Ljava/lang/String;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->street:Ljava/lang/String;

    .line 394
    return-void
.end method

.method public setZipCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "zipCode"    # Ljava/lang/String;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Address;->zipCode:Ljava/lang/String;

    .line 415
    return-void
.end method
