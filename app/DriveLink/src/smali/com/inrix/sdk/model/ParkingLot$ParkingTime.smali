.class public final Lcom/inrix/sdk/model/ParkingLot$ParkingTime;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ParkingTime"
.end annotation


# instance fields
.field private duration:Lcom/inrix/sdk/model/ParkingLot$ParkingDuration;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "duration"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDuration()Lcom/inrix/sdk/model/ParkingLot$ParkingDuration;
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$ParkingTime;->duration:Lcom/inrix/sdk/model/ParkingLot$ParkingDuration;

    return-object v0
.end method
