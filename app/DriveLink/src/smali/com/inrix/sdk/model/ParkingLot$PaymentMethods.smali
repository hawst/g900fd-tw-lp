.class public final Lcom/inrix/sdk/model/ParkingLot$PaymentMethods;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentMethods"
.end annotation


# instance fields
.field private methods:[I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "paymentMethod"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMethods()[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    .locals 6

    .prologue
    .line 645
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 646
    .local v3, "types":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;>;"
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethods;->methods:[I

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget v4, v0, v1

    .line 647
    .local v4, "value":I
    invoke-static {v4}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->fromValue(I)Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 650
    .end local v4    # "value":I
    :cond_0
    const/4 v5, 0x0

    new-array v5, v5, [Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    invoke-interface {v3, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    return-object v5
.end method
