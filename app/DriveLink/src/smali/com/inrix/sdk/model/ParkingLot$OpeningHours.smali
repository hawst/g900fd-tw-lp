.class public final Lcom/inrix/sdk/model/ParkingLot$OpeningHours;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OpeningHours"
.end annotation


# instance fields
.field private notes:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "notes"
    .end annotation
.end field

.field private type:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "openingHoursType"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNotes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$OpeningHours;->notes:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;
    .locals 1

    .prologue
    .line 577
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$OpeningHours;->type:I

    invoke-static {v0}, Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;->fromValue(I)Lcom/inrix/sdk/model/ParkingLot$OpeningHoursType;

    move-result-object v0

    return-object v0
.end method
