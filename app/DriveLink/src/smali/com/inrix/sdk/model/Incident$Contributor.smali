.class public Lcom/inrix/sdk/model/Incident$Contributor;
.super Ljava/lang/Object;
.source "Incident.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/Incident;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Contributor"
.end annotation


# instance fields
.field private isReporter:Z
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "isreporter"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "displayName"
    .end annotation
.end field

.field private trustLevel:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "trustLevel"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$Contributor;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getTrustLevel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/inrix/sdk/model/Incident$Contributor;->trustLevel:Ljava/lang/String;

    return-object v0
.end method

.method public isReporter()Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Lcom/inrix/sdk/model/Incident$Contributor;->isReporter:Z

    return v0
.end method
