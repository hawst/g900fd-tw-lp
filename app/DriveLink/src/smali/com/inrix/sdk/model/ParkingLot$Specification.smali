.class public final Lcom/inrix/sdk/model/ParkingLot$Specification;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Specification"
.end annotation


# instance fields
.field private capacity:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "capacity"
    .end annotation
.end field

.field private gateInfo:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "gateInfo"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot$GateInformation;",
            ">;"
        }
    .end annotation
.end field

.field private type:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$Specification;->type:I

    .line 413
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$Specification;->capacity:I

    return-void
.end method


# virtual methods
.method public final getCapacity()I
    .locals 1

    .prologue
    .line 441
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$Specification;->capacity:I

    return v0
.end method

.method public final getGateInformation()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot$GateInformation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$Specification;->gateInfo:Ljava/util/List;

    return-object v0
.end method

.method public final getType()Lcom/inrix/sdk/model/ParkingLot$ParkingType;
    .locals 1

    .prologue
    .line 431
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$Specification;->type:I

    invoke-static {v0}, Lcom/inrix/sdk/model/ParkingLot$ParkingType;->fromValue(I)Lcom/inrix/sdk/model/ParkingLot$ParkingType;

    move-result-object v0

    return-object v0
.end method
