.class public final Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CurrentCapacity"
.end annotation


# instance fields
.field private availableSpaces:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "availableSpaces"
    .end annotation
.end field

.field private final dateFormat:Ljava/text/SimpleDateFormat;

.field private fillState:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "fillState"
    .end annotation
.end field

.field private fillStateRate:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "fillStateRate"
    .end annotation
.end field

.field private occupancyPercentage:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parkingOccupancyPercentage"
    .end annotation
.end field

.field private reservability:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "reservability"
    .end annotation
.end field

.field private tendency:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tendency"
    .end annotation
.end field

.field private timestamp:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "timestampDataAquisition"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v0, -0x80000000

    .line 899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 879
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->availableSpaces:I

    .line 882
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->occupancyPercentage:I

    .line 885
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->fillState:I

    .line 888
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->fillStateRate:I

    .line 891
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->tendency:I

    .line 894
    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->reservability:I

    .line 900
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 901
    return-void
.end method


# virtual methods
.method public final getAvailableSpaces()I
    .locals 1

    .prologue
    .line 924
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->availableSpaces:I

    return v0
.end method

.method public final getFillState()Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;
    .locals 1

    .prologue
    .line 942
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->fillState:I

    invoke-static {v0}, Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;->fromValue(I)Lcom/inrix/sdk/model/ParkingLot$ParkingStatus;

    move-result-object v0

    return-object v0
.end method

.method public final getFillStateRate()I
    .locals 1

    .prologue
    .line 951
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->fillStateRate:I

    return v0
.end method

.method public final getOccupancyPercentage()I
    .locals 1

    .prologue
    .line 933
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->occupancyPercentage:I

    return v0
.end method

.method public final getReservability()Lcom/inrix/sdk/model/ParkingLot$Reservability;
    .locals 1

    .prologue
    .line 970
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->reservability:I

    invoke-static {v0}, Lcom/inrix/sdk/model/ParkingLot$Reservability;->fromValue(I)Lcom/inrix/sdk/model/ParkingLot$Reservability;

    move-result-object v0

    return-object v0
.end method

.method public final getTendency()Lcom/inrix/sdk/model/ParkingLot$Tendency;
    .locals 1

    .prologue
    .line 961
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->tendency:I

    invoke-static {v0}, Lcom/inrix/sdk/model/ParkingLot$Tendency;->fromValue(I)Lcom/inrix/sdk/model/ParkingLot$Tendency;

    move-result-object v0

    return-object v0
.end method

.method public final getTimestamp()Ljava/util/Date;
    .locals 3

    .prologue
    .line 910
    :try_start_0
    iget-object v1, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->dateFormat:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/inrix/sdk/model/ParkingLot$CurrentCapacity;->timestamp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 915
    :goto_0
    return-object v1

    .line 911
    :catch_0
    move-exception v0

    .line 912
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    .line 915
    const/4 v1, 0x0

    goto :goto_0
.end method
