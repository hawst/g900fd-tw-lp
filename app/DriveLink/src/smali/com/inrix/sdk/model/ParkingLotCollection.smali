.class public final Lcom/inrix/sdk/model/ParkingLotCollection;
.super Lcom/inrix/sdk/parser/json/JSONEntityBase;
.source "ParkingLotCollection.java"


# instance fields
.field private parkingLots:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "result"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/inrix/sdk/parser/json/JSONEntityBase;-><init>()V

    return-void
.end method

.method private preFilter(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "original":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/ParkingLot;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 35
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lcom/inrix/sdk/model/ParkingLot;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/inrix/sdk/model/ParkingLot;

    .line 36
    .local v1, "item":Lcom/inrix/sdk/model/ParkingLot;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot;->getId()I

    move-result v3

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_0

    .line 40
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 43
    .end local v1    # "item":Lcom/inrix/sdk/model/ParkingLot;
    :cond_1
    return-object v2
.end method


# virtual methods
.method public final getParkingLots()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/inrix/sdk/model/ParkingLot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLotCollection;->parkingLots:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/inrix/sdk/model/ParkingLotCollection;->preFilter(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
