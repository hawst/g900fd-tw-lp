.class public Lcom/inrix/sdk/model/TravelTimeResponse;
.super Lcom/inrix/sdk/parser/xml/XMLEntityBase;
.source "TravelTimeResponse.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private tripInfo:Lcom/inrix/sdk/model/TripInformation;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Trip"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/inrix/sdk/parser/xml/XMLEntityBase;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/inrix/sdk/model/TravelTimeResponse;->tripInfo:Lcom/inrix/sdk/model/TripInformation;

    .line 25
    return-void
.end method


# virtual methods
.method public getTripInformation()Lcom/inrix/sdk/model/TripInformation;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/inrix/sdk/model/TravelTimeResponse;->tripInfo:Lcom/inrix/sdk/model/TripInformation;

    return-object v0
.end method
