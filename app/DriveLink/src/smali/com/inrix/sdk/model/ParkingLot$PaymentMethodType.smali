.class public final enum Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
.super Ljava/lang/Enum;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaymentMethodType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum Cash:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum CreditCard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum DirectCashTransfer:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum ElectronicSettlement:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum MobilePhone:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum PrepayCard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum RFID:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum Smartcard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum Ticket:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum Token:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

.field public static final enum Unknown:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 591
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 592
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "Cash"

    invoke-direct {v0, v1, v5, v5}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Cash:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 593
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "CreditCard"

    invoke-direct {v0, v1, v6, v6}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->CreditCard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 594
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "ElectronicSettlement"

    invoke-direct {v0, v1, v7, v7}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->ElectronicSettlement:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 595
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "Ticket"

    invoke-direct {v0, v1, v8, v8}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Ticket:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 596
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "Token"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Token:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 597
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "DirectCashTransfer"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->DirectCashTransfer:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 598
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "RFID"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->RFID:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 599
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "PrepayCard"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->PrepayCard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 600
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "MobilePhone"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->MobilePhone:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 601
    new-instance v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    const-string/jumbo v1, "Smartcard"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Smartcard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    .line 590
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Cash:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->CreditCard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->ElectronicSettlement:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Ticket:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Token:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->DirectCashTransfer:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->RFID:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->PrepayCard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->MobilePhone:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Smartcard:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 605
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 606
    iput p3, p0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->value:I

    .line 607
    return-void
.end method

.method public static final fromValue(I)Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 621
    invoke-static {}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->values()[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    move-result-object v4

    .line 622
    .local v4, "values":[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    move-object v0, v4

    .local v0, "arr$":[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 623
    .local v1, "current":Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    invoke-virtual {v1}, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->getValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 628
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    :goto_1
    return-object v1

    .line 622
    .restart local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 628
    .end local v1    # "current":Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    :cond_1
    sget-object v1, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->Unknown:Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 590
    const-class v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;
    .locals 1

    .prologue
    .line 590
    sget-object v0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->$VALUES:[Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    invoke-virtual {v0}, [Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 610
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$PaymentMethodType;->value:I

    return v0
.end method
