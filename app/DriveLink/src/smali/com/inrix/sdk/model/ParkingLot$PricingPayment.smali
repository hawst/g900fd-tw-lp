.class public final Lcom/inrix/sdk/model/ParkingLot$PricingPayment;
.super Ljava/lang/Object;
.source "ParkingLot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/ParkingLot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PricingPayment"
.end annotation


# instance fields
.field private amount:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "amount"
    .end annotation
.end field

.field private currency:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currencyType"
    .end annotation
.end field

.field private notes:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "notes"
    .end annotation
.end field

.field private time:Lcom/inrix/sdk/model/ParkingLot$ParkingTime;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "time"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661
    const/high16 v0, -0x31000000

    iput v0, p0, Lcom/inrix/sdk/model/ParkingLot$PricingPayment;->amount:F

    return-void
.end method


# virtual methods
.method public final getAmount()F
    .locals 1

    .prologue
    .line 685
    iget v0, p0, Lcom/inrix/sdk/model/ParkingLot$PricingPayment;->amount:F

    return v0
.end method

.method public final getCurrency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$PricingPayment;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public final getNotes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$PricingPayment;->notes:Ljava/lang/String;

    return-object v0
.end method

.method public final getTime()Lcom/inrix/sdk/model/ParkingLot$ParkingTime;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/inrix/sdk/model/ParkingLot$PricingPayment;->time:Lcom/inrix/sdk/model/ParkingLot$ParkingTime;

    return-object v0
.end method
