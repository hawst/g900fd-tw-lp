.class public Lcom/inrix/sdk/model/GasStation$Product;
.super Ljava/lang/Object;
.source "GasStation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/GasStation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Product"
.end annotation


# instance fields
.field private currencyCode:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currencyCode"
    .end annotation
.end field

.field private price:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "price"
    .end annotation
.end field

.field final synthetic this$0:Lcom/inrix/sdk/model/GasStation;

.field private type:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field

.field private updateDateStr:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "updateDate"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GasStation;)V
    .locals 1

    .prologue
    .line 180
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Product;->this$0:Lcom/inrix/sdk/model/GasStation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->type:Ljava/lang/String;

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->price:F

    .line 183
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->currencyCode:Ljava/lang/String;

    .line 184
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->updateDateStr:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/model/GasStation;Ljava/lang/String;FLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "price"    # F
    .param p4, "currencyCode"    # Ljava/lang/String;
    .param p5, "updateDate"    # Ljava/lang/String;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Product;->this$0:Lcom/inrix/sdk/model/GasStation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    iput-object p2, p0, Lcom/inrix/sdk/model/GasStation$Product;->type:Ljava/lang/String;

    .line 196
    iput p3, p0, Lcom/inrix/sdk/model/GasStation$Product;->price:F

    .line 197
    iput-object p4, p0, Lcom/inrix/sdk/model/GasStation$Product;->currencyCode:Ljava/lang/String;

    .line 198
    iput-object p5, p0, Lcom/inrix/sdk/model/GasStation$Product;->updateDateStr:Ljava/lang/String;

    .line 199
    return-void
.end method


# virtual methods
.method public getCurrencyCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->currencyCode:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()F
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->price:F

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateDate()Ljava/util/Date;
    .locals 3

    .prologue
    .line 230
    const/4 v1, 0x0

    .line 232
    .local v1, "updateDate":Ljava/util/Date;
    :try_start_0
    iget-object v2, p0, Lcom/inrix/sdk/model/GasStation$Product;->updateDateStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/inrix/sdk/utils/InrixDateUtils;->getDateFromString(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 236
    :goto_0
    return-object v1

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUpdateDateString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/inrix/sdk/model/GasStation$Product;->updateDateStr:Ljava/lang/String;

    return-object v0
.end method

.method public setCurrencyCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "currencyCode"    # Ljava/lang/String;

    .prologue
    .line 268
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Product;->currencyCode:Ljava/lang/String;

    .line 269
    return-void
.end method

.method public setPrice(F)V
    .locals 0
    .param p1, "price"    # F

    .prologue
    .line 260
    iput p1, p0, Lcom/inrix/sdk/model/GasStation$Product;->price:F

    .line 261
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Product;->type:Ljava/lang/String;

    .line 253
    return-void
.end method

.method public setUpdateDateString(Ljava/lang/String;)V
    .locals 0
    .param p1, "strDateString"    # Ljava/lang/String;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/inrix/sdk/model/GasStation$Product;->updateDateStr:Ljava/lang/String;

    .line 277
    return-void
.end method
