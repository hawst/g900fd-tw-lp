.class public Lcom/inrix/sdk/model/Incident$DelayImpact;
.super Ljava/lang/Object;
.source "Incident.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/model/Incident;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DelayImpact"
.end annotation


# instance fields
.field private abnormal:Z
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "abnormal"
        required = false
    .end annotation
.end field

.field private distance:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "distance"
    .end annotation
.end field

.field private freeFlowMinutes:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "fromFreeFlowMinutes"
    .end annotation
.end field

.field private typicalMinutes:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "fromTypicalMinutes"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDistance()D
    .locals 2

    .prologue
    .line 273
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident$DelayImpact;->distance:D

    return-wide v0
.end method

.method public getFreeFlowMinutes()D
    .locals 2

    .prologue
    .line 266
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident$DelayImpact;->freeFlowMinutes:D

    return-wide v0
.end method

.method public getTypicalMinutes()D
    .locals 2

    .prologue
    .line 259
    iget-wide v0, p0, Lcom/inrix/sdk/model/Incident$DelayImpact;->typicalMinutes:D

    return-wide v0
.end method

.method public isAbnormal()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/inrix/sdk/model/Incident$DelayImpact;->abnormal:Z

    return v0
.end method
