.class final Lcom/inrix/sdk/model/TrafficQualityResult;
.super Ljava/lang/Object;
.source "TrafficQualityResult.java"


# instance fields
.field private bucket:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TrafficQualityBucket"
    .end annotation
.end field

.field private cityName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CityName"
    .end annotation
.end field

.field private collectionTime:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CollectionTime"
    .end annotation
.end field

.field private delayTime:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OverallDelayTime"
    .end annotation
.end field

.field private text:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TrafficQualityText"
    .end annotation
.end field

.field private value:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TrafficQualityValue"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public final getBucket()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/inrix/sdk/model/TrafficQualityResult;->bucket:I

    return v0
.end method

.method public final getCityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQualityResult;->cityName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCollectionTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQualityResult;->collectionTime:Ljava/util/Date;

    return-object v0
.end method

.method public final getDelayTime()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/inrix/sdk/model/TrafficQualityResult;->delayTime:F

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/inrix/sdk/model/TrafficQualityResult;->text:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/inrix/sdk/model/TrafficQualityResult;->value:I

    return v0
.end method
