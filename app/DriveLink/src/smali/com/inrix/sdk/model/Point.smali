.class public Lcom/inrix/sdk/model/Point;
.super Ljava/lang/Object;
.source "Point.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    strict = false
.end annotation


# instance fields
.field private latitude:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field

.field private longitude:D
    .annotation runtime Lorg/simpleframework/xml/Attribute;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLatitude()D
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/inrix/sdk/model/Point;->latitude:D

    return-wide v0
.end method

.method public getLatitudeE5()I
    .locals 4

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/inrix/sdk/model/Point;->latitude:D

    const-wide v2, 0x40f86a0000000000L    # 100000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/inrix/sdk/model/Point;->longitude:D

    return-wide v0
.end method

.method public getLongitudeE5()I
    .locals 4

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/inrix/sdk/model/Point;->longitude:D

    const-wide v2, 0x40f86a0000000000L    # 100000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public setLatitude(D)V
    .locals 0
    .param p1, "latitude"    # D

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/inrix/sdk/model/Point;->latitude:D

    .line 23
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .param p1, "longitude"    # D

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/inrix/sdk/model/Point;->longitude:D

    .line 35
    return-void
.end method
