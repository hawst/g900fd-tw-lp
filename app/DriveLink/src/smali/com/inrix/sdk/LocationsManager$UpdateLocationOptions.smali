.class public Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;
.super Ljava/lang/Object;
.source "LocationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/LocationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateLocationOptions"
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private customData:Ljava/lang/String;

.field private locationId:J

.field private name:Ljava/lang/String;

.field private order:Ljava/lang/Integer;

.field private type:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(J)V
    .locals 2
    .param p1, "locationId"    # J

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x410

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 174
    :cond_0
    iput-wide p1, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->locationId:J

    .line 175
    return-void
.end method


# virtual methods
.method getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->address:Ljava/lang/String;

    return-object v0
.end method

.method getCustomData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->customData:Ljava/lang/String;

    return-object v0
.end method

.method getLocationId()J
    .locals 2

    .prologue
    .line 178
    iget-wide v0, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->locationId:J

    return-wide v0
.end method

.method getLocationType()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->type:Ljava/lang/Integer;

    return-object v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->name:Ljava/lang/String;

    return-object v0
.end method

.method getOrder()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->order:Ljava/lang/Integer;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->address:Ljava/lang/String;

    .line 205
    return-object p0
.end method

.method public setCustomData(Ljava/lang/String;)Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;
    .locals 0
    .param p1, "customData"    # Ljava/lang/String;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->customData:Ljava/lang/String;

    .line 214
    return-object p0
.end method

.method public setLocationType(Ljava/lang/Integer;)Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;
    .locals 0
    .param p1, "type"    # Ljava/lang/Integer;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->type:Ljava/lang/Integer;

    .line 223
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->name:Ljava/lang/String;

    .line 187
    return-object p0
.end method

.method public setOrder(Ljava/lang/Integer;)Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;
    .locals 0
    .param p1, "order"    # Ljava/lang/Integer;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$UpdateLocationOptions;->order:Ljava/lang/Integer;

    .line 196
    return-object p0
.end method
