.class public Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;
.super Ljava/lang/Object;
.source "AlertsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/AlertsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IncidentAlertOptions"
.end annotation


# instance fields
.field public final FORWARD_CONE_ANGLE_DEFAULT:F

.field public final FORWARD_CONE_ANGLE_MAX:F

.field public final FORWARD_CONE_ANGLE_MIN:F

.field private alertInterval:I

.field private filter:Lcom/inrix/sdk/AlertsManager$IFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/inrix/sdk/AlertsManager$IFilter",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation
.end field

.field private forwardConeAngle:F

.field private speedFactor:F


# direct methods
.method public constructor <init>(ILcom/inrix/sdk/AlertsManager$IFilter;)V
    .locals 2
    .param p1, "alertIntervalSeconds"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/inrix/sdk/AlertsManager$IFilter",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "filter":Lcom/inrix/sdk/AlertsManager$IFilter;, "Lcom/inrix/sdk/AlertsManager$IFilter<Lcom/inrix/sdk/model/Incident;>;"
    const/high16 v1, 0x42b40000    # 90.0f

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput v1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->FORWARD_CONE_ANGLE_DEFAULT:F

    .line 89
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->FORWARD_CONE_ANGLE_MIN:F

    .line 93
    const v0, 0x43b38000    # 359.0f

    iput v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->FORWARD_CONE_ANGLE_MAX:F

    .line 98
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->speedFactor:F

    .line 100
    iput v1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->forwardConeAngle:F

    .line 114
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->setInterval(I)V

    .line 115
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->setFilter(Lcom/inrix/sdk/AlertsManager$IFilter;)V

    .line 116
    invoke-virtual {p0, v1}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->setForwardConeAngle(F)V

    .line 117
    return-void
.end method

.method public constructor <init>(ILcom/inrix/sdk/AlertsManager$IFilter;F)V
    .locals 2
    .param p1, "alertIntervalSeconds"    # I
    .param p3, "forwardAngle"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/inrix/sdk/AlertsManager$IFilter",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .local p2, "filter":Lcom/inrix/sdk/AlertsManager$IFilter;, "Lcom/inrix/sdk/AlertsManager$IFilter<Lcom/inrix/sdk/model/Incident;>;"
    const/high16 v1, 0x42b40000    # 90.0f

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput v1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->FORWARD_CONE_ANGLE_DEFAULT:F

    .line 89
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->FORWARD_CONE_ANGLE_MIN:F

    .line 93
    const v0, 0x43b38000    # 359.0f

    iput v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->FORWARD_CONE_ANGLE_MAX:F

    .line 98
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->speedFactor:F

    .line 100
    iput v1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->forwardConeAngle:F

    .line 131
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->setInterval(I)V

    .line 132
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->setFilter(Lcom/inrix/sdk/AlertsManager$IFilter;)V

    .line 133
    invoke-virtual {p0, p3}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->setForwardConeAngle(F)V

    .line 134
    return-void
.end method


# virtual methods
.method getFilter()Lcom/inrix/sdk/AlertsManager$IFilter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/AlertsManager$IFilter",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->filter:Lcom/inrix/sdk/AlertsManager$IFilter;

    return-object v0
.end method

.method getForwardConeAngle()F
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->forwardConeAngle:F

    return v0
.end method

.method getInterval()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->alertInterval:I

    return v0
.end method

.method getSpeedFactor()F
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->speedFactor:F

    return v0
.end method

.method public setFilter(Lcom/inrix/sdk/AlertsManager$IFilter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/inrix/sdk/AlertsManager$IFilter",
            "<",
            "Lcom/inrix/sdk/model/Incident;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "filter":Lcom/inrix/sdk/AlertsManager$IFilter;, "Lcom/inrix/sdk/AlertsManager$IFilter<Lcom/inrix/sdk/model/Incident;>;"
    iput-object p1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->filter:Lcom/inrix/sdk/AlertsManager$IFilter;

    .line 163
    return-void
.end method

.method public setForwardConeAngle(F)V
    .locals 2
    .param p1, "angle"    # F

    .prologue
    .line 193
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const v0, 0x43b38000    # 359.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 194
    :cond_0
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x418

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 196
    :cond_1
    iput p1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->forwardConeAngle:F

    .line 197
    return-void
.end method

.method public setInterval(I)V
    .locals 2
    .param p1, "seconds"    # I

    .prologue
    .line 175
    if-gtz p1, :cond_0

    .line 176
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3ee

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 178
    :cond_0
    iput p1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->alertInterval:I

    .line 179
    return-void
.end method

.method public setSpeedFactor(F)V
    .locals 2
    .param p1, "speedFactor"    # F

    .prologue
    .line 146
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 147
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3ef

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 149
    :cond_0
    iput p1, p0, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->speedFactor:F

    .line 150
    return-void
.end method
