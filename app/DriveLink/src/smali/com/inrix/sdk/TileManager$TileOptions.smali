.class public final Lcom/inrix/sdk/TileManager$TileOptions;
.super Ljava/lang/Object;
.source "TileManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/TileManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TileOptions"
.end annotation


# static fields
.field static final NO_VALUE:I = -0x1


# instance fields
.field private coverage:I

.field private format:I

.field private frcLevel:I

.field private height:I

.field private opacity:I

.field private penWidth:I

.field private speedBucketId:I

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x100

    const/4 v0, -0x1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->coverage:I

    .line 59
    iput v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->speedBucketId:I

    .line 65
    iput v1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->width:I

    .line 66
    iput v1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->height:I

    .line 67
    const/16 v0, 0x3c

    iput v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->opacity:I

    .line 68
    const/4 v0, 0x4

    iput v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->penWidth:I

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->format:I

    .line 70
    const/16 v0, 0xff

    iput v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->frcLevel:I

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/inrix/sdk/TileManager$TileOptions;)V
    .locals 1
    .param p1, "in"    # Lcom/inrix/sdk/TileManager$TileOptions;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/inrix/sdk/TileManager$TileOptions;-><init>()V

    .line 81
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setWidth(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 82
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setHeight(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 83
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getOpacity()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setOpacity(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 84
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getPenWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setPenWidth(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 85
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getFormat()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setFormat(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 86
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getFrcLevel()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setFrcLevel(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 87
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getCoverage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setCoverage(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 88
    invoke-virtual {p1}, Lcom/inrix/sdk/TileManager$TileOptions;->getSpeedBucketId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/TileManager$TileOptions;->setSpeedBucketId(I)Lcom/inrix/sdk/TileManager$TileOptions;

    .line 89
    return-void
.end method


# virtual methods
.method final getAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    const-string/jumbo v0, "Mobile.Tile"

    return-object v0
.end method

.method final getCoverage()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->coverage:I

    return v0
.end method

.method final getFormat()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->format:I

    return v0
.end method

.method final getFrcLevel()I
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->frcLevel:I

    return v0
.end method

.method final getHeight()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->height:I

    return v0
.end method

.method final getOpacity()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->opacity:I

    return v0
.end method

.method final getPenWidth()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->penWidth:I

    return v0
.end method

.method final getSpeedBucketId()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->speedBucketId:I

    return v0
.end method

.method final getWidth()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/inrix/sdk/TileManager$TileOptions;->width:I

    return v0
.end method

.method public final setCoverage(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "coverage"    # I

    .prologue
    .line 299
    # invokes: Lcom/inrix/sdk/TileManager;->validateCoverage(I)V
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->access$600(I)V

    .line 300
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->coverage:I

    .line 301
    return-object p0
.end method

.method public final setFormat(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "format"    # I

    .prologue
    .line 225
    # invokes: Lcom/inrix/sdk/TileManager;->validateFormat(I)V
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->access$400(I)V

    .line 226
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->format:I

    .line 227
    return-object p0
.end method

.method public final setFrcLevel(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "frcLevel"    # I

    .prologue
    .line 263
    # invokes: Lcom/inrix/sdk/TileManager;->validateFrcLevel(I)V
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->access$500(I)V

    .line 264
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->frcLevel:I

    .line 265
    return-object p0
.end method

.method public final setHeight(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 140
    # invokes: Lcom/inrix/sdk/TileManager;->validateHeight(I)V
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->access$100(I)V

    .line 141
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->height:I

    .line 142
    return-object p0
.end method

.method public final setOpacity(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "opacity"    # I

    .prologue
    .line 168
    # invokes: Lcom/inrix/sdk/TileManager;->validateOpacity(I)V
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->access$200(I)V

    .line 169
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->opacity:I

    .line 170
    return-object p0
.end method

.method public final setPenWidth(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "penWidth"    # I

    .prologue
    .line 196
    # invokes: Lcom/inrix/sdk/TileManager;->validatePenWidth(I)V
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->access$300(I)V

    .line 197
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->penWidth:I

    .line 198
    return-object p0
.end method

.method public final setSpeedBucketId(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "speedBucketId"    # I

    .prologue
    .line 328
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->speedBucketId:I

    .line 329
    return-object p0
.end method

.method public final setWidth(I)Lcom/inrix/sdk/TileManager$TileOptions;
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 112
    # invokes: Lcom/inrix/sdk/TileManager;->validateWidth(I)V
    invoke-static {p1}, Lcom/inrix/sdk/TileManager;->access$000(I)V

    .line 113
    iput p1, p0, Lcom/inrix/sdk/TileManager$TileOptions;->width:I

    .line 114
    return-object p0
.end method
