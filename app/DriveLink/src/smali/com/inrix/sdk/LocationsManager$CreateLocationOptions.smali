.class public Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
.super Ljava/lang/Object;
.source "LocationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/LocationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CreateLocationOptions"
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private customData:Ljava/lang/String;

.field private locationType:I

.field private name:Ljava/lang/String;

.field private order:I

.field private position:Lcom/inrix/sdk/model/GeoPoint;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;Ljava/lang/String;)V
    .locals 0
    .param p1, "position"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->setPosition(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;

    .line 54
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->setName(Ljava/lang/String;)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;

    .line 55
    return-void
.end method


# virtual methods
.method getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->address:Ljava/lang/String;

    return-object v0
.end method

.method getCustomData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->customData:Ljava/lang/String;

    return-object v0
.end method

.method getLocationType()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->locationType:I

    return v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->name:Ljava/lang/String;

    return-object v0
.end method

.method getOrder()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->order:I

    return v0
.end method

.method getPosition()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->position:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->address:Ljava/lang/String;

    .line 83
    return-object p0
.end method

.method public setCustomData(Ljava/lang/String;)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
    .locals 0
    .param p1, "customData"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->customData:Ljava/lang/String;

    .line 116
    return-object p0
.end method

.method public setLocationType(I)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->locationType:I

    .line 135
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x411

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 67
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->name:Ljava/lang/String;

    .line 68
    return-object p0
.end method

.method public setOrder(I)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
    .locals 0
    .param p1, "order"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->order:I

    .line 154
    return-object p0
.end method

.method public setPosition(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;
    .locals 2
    .param p1, "position"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 97
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 100
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/LocationsManager$CreateLocationOptions;->position:Lcom/inrix/sdk/model/GeoPoint;

    .line 101
    return-object p0
.end method
