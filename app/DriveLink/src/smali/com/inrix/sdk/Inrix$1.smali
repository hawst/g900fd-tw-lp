.class final Lcom/inrix/sdk/Inrix$1;
.super Ljava/lang/Object;
.source "Inrix.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/Inrix;->initialize(Landroid/content/Context;Lcom/inrix/sdk/InrixConfig;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$autoEnableActivityRecognition:Z

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/inrix/sdk/Inrix$1;->val$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/inrix/sdk/Inrix$1;->val$autoEnableActivityRecognition:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 107
    # getter for: Lcom/inrix/sdk/Inrix;->instance:Lcom/inrix/sdk/Inrix;
    invoke-static {}, Lcom/inrix/sdk/Inrix;->access$000()Lcom/inrix/sdk/Inrix;

    move-result-object v1

    monitor-enter v1

    .line 108
    :try_start_0
    # getter for: Lcom/inrix/sdk/Inrix;->isInitialized:Z
    invoke-static {}, Lcom/inrix/sdk/Inrix;->access$100()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLocationSource()Lcom/inrix/sdk/geolocation/IGeolocationSource;

    move-result-object v0

    if-nez v0, :cond_0

    .line 111
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    new-instance v2, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;

    iget-object v3, p0, Lcom/inrix/sdk/Inrix$1;->val$context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/inrix/sdk/geolocation/PlayServicesLocationSource;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/inrix/sdk/geolocation/GeolocationController;->setLocationSource(Lcom/inrix/sdk/geolocation/IGeolocationSource;)V

    .line 116
    :cond_0
    iget-boolean v0, p0, Lcom/inrix/sdk/Inrix$1;->val$autoEnableActivityRecognition:Z

    if-eqz v0, :cond_1

    .line 117
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/inrix/sdk/geolocation/GeolocationController;->startLocationTracking()V

    .line 119
    iget-object v0, p0, Lcom/inrix/sdk/Inrix$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/inrix/sdk/Inrix;->startActivityRecognition(Landroid/content/Context;)V

    .line 122
    :cond_1
    monitor-exit v1

    .line 123
    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
