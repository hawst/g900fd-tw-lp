.class public final enum Lcom/inrix/sdk/IncidentsManager$ACTIONS;
.super Ljava/lang/Enum;
.source "IncidentsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/IncidentsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ACTIONS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/inrix/sdk/IncidentsManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/inrix/sdk/IncidentsManager$ACTIONS;

.field public static final enum GET_INCIDENTS:Lcom/inrix/sdk/IncidentsManager$ACTIONS;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 158
    new-instance v0, Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    const-string/jumbo v1, "GET_INCIDENTS"

    invoke-direct {v0, v1, v2}, Lcom/inrix/sdk/IncidentsManager$ACTIONS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/inrix/sdk/IncidentsManager$ACTIONS;->GET_INCIDENTS:Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    .line 157
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    sget-object v1, Lcom/inrix/sdk/IncidentsManager$ACTIONS;->GET_INCIDENTS:Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    aput-object v1, v0, v2

    sput-object v0, Lcom/inrix/sdk/IncidentsManager$ACTIONS;->$VALUES:[Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/inrix/sdk/IncidentsManager$ACTIONS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 157
    const-class v0, Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    return-object v0
.end method

.method public static values()[Lcom/inrix/sdk/IncidentsManager$ACTIONS;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/inrix/sdk/IncidentsManager$ACTIONS;->$VALUES:[Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    invoke-virtual {v0}, [Lcom/inrix/sdk/IncidentsManager$ACTIONS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/inrix/sdk/IncidentsManager$ACTIONS;

    return-object v0
.end method
