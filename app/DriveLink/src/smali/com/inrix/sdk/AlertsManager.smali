.class public Lcom/inrix/sdk/AlertsManager;
.super Ljava/lang/Object;
.source "AlertsManager.java"

# interfaces
.implements Lcom/inrix/sdk/IRefreshableActions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/AlertsManager$1;,
        Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;,
        Lcom/inrix/sdk/AlertsManager$IFilter;,
        Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;,
        Lcom/inrix/sdk/AlertsManager$ACTIONS;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/inrix/sdk/IRefreshableActions",
        "<",
        "Lcom/inrix/sdk/AlertsManager$ACTIONS;",
        ">;"
    }
.end annotation


# static fields
.field private static final SMART_ALERT_INTERVAL:I = 0x1e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method


# virtual methods
.method public final createIncidentAlert(Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;)Lcom/inrix/sdk/IncidentAlert;
    .locals 6
    .param p1, "listener"    # Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;
    .param p2, "alertParams"    # Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/inrix/sdk/exception/InrixException;
        }
    .end annotation

    .prologue
    .line 215
    if-nez p1, :cond_0

    .line 216
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3ea

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 218
    :cond_0
    if-nez p2, :cond_1

    .line 219
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3eb

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 221
    :cond_1
    new-instance v0, Lcom/inrix/sdk/IncidentAlert;

    invoke-virtual {p2}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->getInterval()I

    move-result v2

    invoke-virtual {p2}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->getSpeedFactor()F

    move-result v3

    invoke-virtual {p2}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->getFilter()Lcom/inrix/sdk/AlertsManager$IFilter;

    move-result-object v4

    invoke-virtual {p2}, Lcom/inrix/sdk/AlertsManager$IncidentAlertOptions;->getForwardConeAngle()F

    move-result v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/inrix/sdk/IncidentAlert;-><init>(Lcom/inrix/sdk/AlertsManager$IIncidentsAlertListener;IFLcom/inrix/sdk/AlertsManager$IFilter;F)V

    return-object v0
.end method

.method public getRefreshInterval(Lcom/inrix/sdk/AlertsManager$ACTIONS;)I
    .locals 3
    .param p1, "action"    # Lcom/inrix/sdk/AlertsManager$ACTIONS;

    .prologue
    .line 58
    if-nez p1, :cond_0

    .line 59
    new-instance v1, Lcom/inrix/sdk/exception/InrixException;

    const/16 v2, 0x412

    invoke-direct {v1, v2}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v1

    .line 61
    :cond_0
    sget-object v1, Lcom/inrix/sdk/AlertsManager$1;->$SwitchMap$com$inrix$sdk$AlertsManager$ACTIONS:[I

    invoke-virtual {p1}, Lcom/inrix/sdk/AlertsManager$ACTIONS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    .line 63
    const/16 v0, 0x1e

    .line 67
    .local v0, "interval":I
    return v0
.end method

.method public bridge synthetic getRefreshInterval(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p1, Lcom/inrix/sdk/AlertsManager$ACTIONS;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/AlertsManager;->getRefreshInterval(Lcom/inrix/sdk/AlertsManager$ACTIONS;)I

    move-result v0

    return v0
.end method
