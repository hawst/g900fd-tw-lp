.class Lcom/inrix/sdk/GasStationManager$5;
.super Ljava/lang/Object;
.source "GasStationManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/inrix/sdk/GasStationManager;->getGasStationInformation(Lcom/inrix/sdk/GasStationManager$SingleGasStationOptions;Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;)Lcom/inrix/sdk/ICancellable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/inrix/sdk/model/SingleGasStationResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/GasStationManager;

.field final synthetic val$listener:Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/GasStationManager;Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/inrix/sdk/GasStationManager$5;->this$0:Lcom/inrix/sdk/GasStationManager;

    iput-object p2, p0, Lcom/inrix/sdk/GasStationManager$5;->val$listener:Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/inrix/sdk/model/SingleGasStationResult;)V
    .locals 1
    .param p1, "response"    # Lcom/inrix/sdk/model/SingleGasStationResult;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/inrix/sdk/GasStationManager$5;->val$listener:Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;

    invoke-interface {v0, p1}, Lcom/inrix/sdk/GasStationManager$ISingleGasStationResponseListener;->onResult(Ljava/lang/Object;)V

    .line 200
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 195
    check-cast p1, Lcom/inrix/sdk/model/SingleGasStationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/GasStationManager$5;->onResponse(Lcom/inrix/sdk/model/SingleGasStationResult;)V

    return-void
.end method
