.class public Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;
.super Lcom/inrix/sdk/ParkingManager$ParkingOptions;
.source "ParkingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/ParkingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParkingInRadiusOptions"
.end annotation


# instance fields
.field private center:Lcom/inrix/sdk/model/GeoPoint;

.field private radius:I


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;I)V
    .locals 0
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "radius"    # I

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/inrix/sdk/ParkingManager$ParkingOptions;-><init>()V

    .line 230
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->setCenter(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;

    .line 231
    invoke-virtual {p0, p2}, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->setRadius(I)Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;

    .line 232
    return-void
.end method


# virtual methods
.method public getCenter()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method public getRadius()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->radius:I

    return v0
.end method

.method public setCenter(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;
    .locals 2
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 242
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 245
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 246
    return-object p0
.end method

.method public setRadius(I)Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;
    .locals 2
    .param p1, "radius"    # I

    .prologue
    .line 263
    if-gtz p1, :cond_0

    .line 264
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3ec

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 266
    :cond_0
    iput p1, p0, Lcom/inrix/sdk/ParkingManager$ParkingInRadiusOptions;->radius:I

    .line 267
    return-object p0
.end method
