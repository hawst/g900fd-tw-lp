.class public Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;
.super Lcom/inrix/sdk/IncidentsManager$IncidentOptions;
.source "IncidentsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/IncidentsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IncidentRadiusOptions"
.end annotation


# instance fields
.field private center:Lcom/inrix/sdk/model/GeoPoint;

.field private radius:D

.field private units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/model/GeoPoint;D)V
    .locals 1
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;
    .param p2, "radius"    # D

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/inrix/sdk/IncidentsManager$IncidentOptions;-><init>()V

    .line 356
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->setCenter(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    .line 357
    invoke-virtual {p0, p2, p3}, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->setRadius(D)Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;

    .line 358
    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getSettingUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 359
    return-void
.end method


# virtual methods
.method getCenter()Lcom/inrix/sdk/model/GeoPoint;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    return-object v0
.end method

.method getRadius()D
    .locals 2

    .prologue
    .line 401
    iget-wide v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->radius:D

    return-wide v0
.end method

.method getUnits()Lcom/inrix/sdk/utils/UserPreferences$UNIT;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    return-object v0
.end method

.method public setCenter(Lcom/inrix/sdk/model/GeoPoint;)Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;
    .locals 2
    .param p1, "center"    # Lcom/inrix/sdk/model/GeoPoint;

    .prologue
    .line 374
    invoke-static {p1}, Lcom/inrix/sdk/model/GeoPoint;->isValid(Lcom/inrix/sdk/model/GeoPoint;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3e9

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 377
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->center:Lcom/inrix/sdk/model/GeoPoint;

    .line 378
    return-object p0
.end method

.method public setRadius(D)Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;
    .locals 2
    .param p1, "radius"    # D

    .prologue
    .line 388
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 389
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x3ec

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 391
    :cond_0
    iput-wide p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->radius:D

    .line 392
    return-object p0
.end method

.method public setUnits(Lcom/inrix/sdk/utils/UserPreferences$UNIT;)V
    .locals 2
    .param p1, "units"    # Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .prologue
    .line 414
    if-nez p1, :cond_0

    .line 415
    new-instance v0, Lcom/inrix/sdk/exception/InrixException;

    const/16 v1, 0x412

    invoke-direct {v0, v1}, Lcom/inrix/sdk/exception/InrixException;-><init>(I)V

    throw v0

    .line 417
    :cond_0
    iput-object p1, p0, Lcom/inrix/sdk/IncidentsManager$IncidentRadiusOptions;->units:Lcom/inrix/sdk/utils/UserPreferences$UNIT;

    .line 418
    return-void
.end method
