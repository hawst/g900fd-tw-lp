.class public final Lcom/inrix/sdk/phs/PhsQueue;
.super Ljava/util/concurrent/ConcurrentLinkedQueue;
.source "PhsQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/ConcurrentLinkedQueue",
        "<",
        "Lcom/inrix/sdk/phs/Phs;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private capacity:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/inrix/sdk/phs/PhsQueue;-><init>(I)V

    .line 28
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "capacity"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    .line 20
    iput p1, p0, Lcom/inrix/sdk/phs/PhsQueue;->capacity:I

    .line 21
    return-void
.end method


# virtual methods
.method public final declared-synchronized add(Lcom/inrix/sdk/phs/Phs;)Z
    .locals 2
    .param p1, "item"    # Lcom/inrix/sdk/phs/Phs;

    .prologue
    .line 37
    monitor-enter p0

    if-nez p1, :cond_0

    .line 38
    :try_start_0
    invoke-super {p0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 44
    :goto_0
    monitor-exit p0

    return v0

    .line 40
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/inrix/sdk/phs/PhsQueue;->size()I

    move-result v0

    iget v1, p0, Lcom/inrix/sdk/phs/PhsQueue;->capacity:I

    if-lt v0, v1, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/inrix/sdk/phs/PhsQueue;->poll()Ljava/lang/Object;

    .line 44
    :cond_1
    invoke-super {p0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 8
    check-cast p1, Lcom/inrix/sdk/phs/Phs;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/phs/PhsQueue;->add(Lcom/inrix/sdk/phs/Phs;)Z

    move-result v0

    return v0
.end method
