.class public interface abstract Lcom/inrix/sdk/phs/IPhsTimer;
.super Ljava/lang/Object;
.source "IPhsTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;,
        Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;
    }
.end annotation


# virtual methods
.method public abstract start(I)V
.end method

.method public abstract start(II)V
.end method

.method public abstract startNormal()V
.end method

.method public abstract startTurbo()V
.end method

.method public abstract stop()V
.end method
