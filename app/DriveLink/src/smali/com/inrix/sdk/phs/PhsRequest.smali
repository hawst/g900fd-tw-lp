.class public Lcom/inrix/sdk/phs/PhsRequest;
.super Lcom/inrix/sdk/network/request/XMLInrixRequest;
.source "PhsRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/inrix/sdk/network/request/XMLInrixRequest",
        "<",
        "Lcom/inrix/sdk/phs/PhsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION:Ljava/lang/String; = "Mobile.Send.PHS"


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p2, "errorCallback"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/inrix/sdk/phs/PhsResult;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 15
    .local p1, "successCallback":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/inrix/sdk/phs/PhsResult;>;"
    invoke-direct {p0, p1, p2}, Lcom/inrix/sdk/network/request/XMLInrixRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 17
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "Mobile.Send.PHS"

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/phs/PhsRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 18
    return-void
.end method


# virtual methods
.method public final getParser()Lcom/inrix/sdk/parser/INetworkResponseParser;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/inrix/sdk/parser/INetworkResponseParser",
            "<",
            "Lcom/inrix/sdk/phs/PhsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;

    const-class v1, Lcom/inrix/sdk/phs/PhsResult;

    invoke-direct {v0, v1}, Lcom/inrix/sdk/parser/xml/InrixXMLGSONParserBase;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public final getServerType()Lcom/inrix/sdk/network/request/InrixRequest$ServerType;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/inrix/sdk/network/request/InrixRequest$ServerType;->DUST:Lcom/inrix/sdk/network/request/InrixRequest$ServerType;

    return-object v0
.end method

.method public final shouldAttachPhs()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method
