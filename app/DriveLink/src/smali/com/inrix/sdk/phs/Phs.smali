.class public final Lcom/inrix/sdk/phs/Phs;
.super Ljava/lang/Object;
.source "Phs.java"


# static fields
.field private static final METERS_PER_SEC_OVER_MPH:F = 2.2369363f

.field private static final MIN_COLLECT_DISTANCE:D = 10.0

.field private static final SEPARATOR:C = '|'


# instance fields
.field private final anonymousVehicleId:Ljava/lang/String;

.field private final heading:I

.field private final horizontalAccuracy:F

.field private final latitude:D

.field private final longitude:D

.field private final speed:F

.field private final time:J


# direct methods
.method public constructor <init>(DDFFFJLjava/lang/String;)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "heading"    # F
    .param p6, "speed"    # F
    .param p7, "accuracy"    # F
    .param p8, "dateTime"    # J
    .param p10, "vehicleId"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-wide p1, p0, Lcom/inrix/sdk/phs/Phs;->latitude:D

    .line 113
    iput-wide p3, p0, Lcom/inrix/sdk/phs/Phs;->longitude:D

    .line 114
    float-to-int v0, p5

    iput v0, p0, Lcom/inrix/sdk/phs/Phs;->heading:I

    .line 115
    const v0, 0x400f29f7

    mul-float/2addr v0, p6

    iput v0, p0, Lcom/inrix/sdk/phs/Phs;->speed:F

    .line 116
    iput p7, p0, Lcom/inrix/sdk/phs/Phs;->horizontalAccuracy:F

    .line 117
    iput-wide p8, p0, Lcom/inrix/sdk/phs/Phs;->time:J

    .line 118
    iput-object p10, p0, Lcom/inrix/sdk/phs/Phs;->anonymousVehicleId:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public constructor <init>(Landroid/location/Location;Ljava/lang/String;)V
    .locals 11
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "vehicleId"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v5

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v6

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    move-object v0, p0

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/inrix/sdk/phs/Phs;-><init>(DDFFFJLjava/lang/String;)V

    .line 91
    return-void
.end method


# virtual methods
.method public final distanceTo(Lcom/inrix/sdk/phs/Phs;)[F
    .locals 9
    .param p1, "point"    # Lcom/inrix/sdk/phs/Phs;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 147
    if-nez p1, :cond_0

    .line 148
    new-array v8, v0, [F

    const/4 v0, 0x0

    aput v0, v8, v1

    .line 153
    :goto_0
    return-object v8

    .line 151
    :cond_0
    new-array v8, v0, [F

    const v0, 0x4121999a    # 10.1f

    aput v0, v8, v1

    .line 152
    .local v8, "results":[F
    invoke-virtual {p0}, Lcom/inrix/sdk/phs/Phs;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/inrix/sdk/phs/Phs;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/inrix/sdk/phs/Phs;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/inrix/sdk/phs/Phs;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    goto :goto_0
.end method

.method public final getLatitude()D
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/inrix/sdk/phs/Phs;->latitude:D

    return-wide v0
.end method

.method public final getLongitude()D
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/inrix/sdk/phs/Phs;->longitude:D

    return-wide v0
.end method

.method public final hasMinDistanceTo(Lcom/inrix/sdk/phs/Phs;)Z
    .locals 6
    .param p1, "point"    # Lcom/inrix/sdk/phs/Phs;

    .prologue
    const/4 v1, 0x0

    .line 164
    invoke-virtual {p0, p1}, Lcom/inrix/sdk/phs/Phs;->distanceTo(Lcom/inrix/sdk/phs/Phs;)[F

    move-result-object v0

    .line 165
    .local v0, "results":[F
    aget v2, v0, v1

    float-to-double v2, v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final toQueryString(Z)Ljava/lang/String;
    .locals 5
    .param p1, "includeVehicleId"    # Z

    .prologue
    const/16 v4, 0x7c

    .line 174
    iget-wide v2, p0, Lcom/inrix/sdk/phs/Phs;->time:J

    invoke-static {v2, v3}, Lcom/inrix/sdk/utils/InrixDateUtils;->getCsTimeFormatFromMs(J)Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "timeValue":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-wide v2, p0, Lcom/inrix/sdk/phs/Phs;->latitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 178
    iget-wide v2, p0, Lcom/inrix/sdk/phs/Phs;->longitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    iget v2, p0, Lcom/inrix/sdk/phs/Phs;->heading:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    iget v2, p0, Lcom/inrix/sdk/phs/Phs;->speed:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 182
    if-eqz p1, :cond_0

    .line 183
    iget-object v2, p0, Lcom/inrix/sdk/phs/Phs;->anonymousVehicleId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 185
    :cond_0
    iget v2, p0, Lcom/inrix/sdk/phs/Phs;->horizontalAccuracy:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
