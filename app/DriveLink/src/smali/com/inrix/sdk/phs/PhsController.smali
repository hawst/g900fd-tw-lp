.class public final Lcom/inrix/sdk/phs/PhsController;
.super Ljava/lang/Object;
.source "PhsController.java"

# interfaces
.implements Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;


# static fields
.field static final BREADCRUMB_QUALITY_THRESHOLD:F = 96560.64f

.field static final LOGGING_ENABLED:Z = true

.field static final MAX_BREADCRUMBS_QUEUE_SIZE:I = 0x5

.field static final MAX_RETRY_TIMES_ON_EMPTY_QUEUE:I = 0x1e

.field static final MAX_URL_LENGTH:I = 0x514

.field static final TAG:Ljava/lang/String; = "PHS"

.field private static instance:Lcom/inrix/sdk/phs/PhsController;


# instance fields
.field private alternateVehicleID:Ljava/lang/String;

.field private breadcrumbs:Lcom/inrix/sdk/phs/PhsQueue;

.field private geolocationController:Lcom/inrix/sdk/geolocation/GeolocationController;

.field private volatile lastPoint:Lcom/inrix/sdk/phs/Phs;

.field private queue:Lcom/inrix/sdk/phs/PhsQueue;

.field private volatile retryTimes:I

.field private sender:Lcom/inrix/sdk/phs/IPhsSender;

.field private timer:Lcom/inrix/sdk/phs/IPhsTimer;

.field private trackingEnabled:Z

.field private userInitiated:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/inrix/sdk/phs/PhsTimer;

    invoke-direct {v0, p0}, Lcom/inrix/sdk/phs/PhsTimer;-><init>(Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;)V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->timer:Lcom/inrix/sdk/phs/IPhsTimer;

    .line 64
    new-instance v0, Lcom/inrix/sdk/phs/PhsSender;

    invoke-direct {v0}, Lcom/inrix/sdk/phs/PhsSender;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->sender:Lcom/inrix/sdk/phs/IPhsSender;

    .line 65
    new-instance v0, Lcom/inrix/sdk/phs/PhsQueue;

    invoke-direct {v0}, Lcom/inrix/sdk/phs/PhsQueue;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    .line 66
    new-instance v0, Lcom/inrix/sdk/phs/PhsQueue;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/inrix/sdk/phs/PhsQueue;-><init>(I)V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->breadcrumbs:Lcom/inrix/sdk/phs/PhsQueue;

    .line 67
    invoke-static {}, Lcom/inrix/sdk/geolocation/GeolocationController;->getInstance()Lcom/inrix/sdk/geolocation/GeolocationController;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->geolocationController:Lcom/inrix/sdk/geolocation/GeolocationController;

    .line 68
    return-void
.end method

.method constructor <init>(Lcom/inrix/sdk/phs/IPhsTimer;Lcom/inrix/sdk/phs/IPhsSender;Lcom/inrix/sdk/geolocation/GeolocationController;)V
    .locals 2
    .param p1, "timer"    # Lcom/inrix/sdk/phs/IPhsTimer;
    .param p2, "sender"    # Lcom/inrix/sdk/phs/IPhsSender;
    .param p3, "geolocationController"    # Lcom/inrix/sdk/geolocation/GeolocationController;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/inrix/sdk/phs/PhsController;->timer:Lcom/inrix/sdk/phs/IPhsTimer;

    .line 53
    iput-object p2, p0, Lcom/inrix/sdk/phs/PhsController;->sender:Lcom/inrix/sdk/phs/IPhsSender;

    .line 54
    new-instance v0, Lcom/inrix/sdk/phs/PhsQueue;

    invoke-direct {v0}, Lcom/inrix/sdk/phs/PhsQueue;-><init>()V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    .line 55
    new-instance v0, Lcom/inrix/sdk/phs/PhsQueue;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/inrix/sdk/phs/PhsQueue;-><init>(I)V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->breadcrumbs:Lcom/inrix/sdk/phs/PhsQueue;

    .line 56
    iput-object p3, p0, Lcom/inrix/sdk/phs/PhsController;->geolocationController:Lcom/inrix/sdk/geolocation/GeolocationController;

    .line 57
    return-void
.end method

.method public static getInstance()Lcom/inrix/sdk/phs/PhsController;
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lcom/inrix/sdk/phs/PhsController;->instance:Lcom/inrix/sdk/phs/PhsController;

    if-nez v0, :cond_0

    .line 77
    const-class v1, Lcom/inrix/sdk/phs/PhsController;

    monitor-enter v1

    .line 78
    :try_start_0
    new-instance v0, Lcom/inrix/sdk/phs/PhsController;

    invoke-direct {v0}, Lcom/inrix/sdk/phs/PhsController;-><init>()V

    sput-object v0, Lcom/inrix/sdk/phs/PhsController;->instance:Lcom/inrix/sdk/phs/PhsController;

    .line 79
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :cond_0
    sget-object v0, Lcom/inrix/sdk/phs/PhsController;->instance:Lcom/inrix/sdk/phs/PhsController;

    return-object v0

    .line 79
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getBreadcrumbs()Lcom/inrix/sdk/phs/PhsQueue;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->breadcrumbs:Lcom/inrix/sdk/phs/PhsQueue;

    return-object v0
.end method

.method public final declared-synchronized getBreadcrumbsPayload(Z)Ljava/lang/String;
    .locals 17
    .param p1, "addVehicleId"    # Z

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 298
    .local v12, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x5

    new-array v11, v1, [Lcom/inrix/sdk/phs/Phs;

    .line 299
    .local v11, "breadCrumbsArray":[Lcom/inrix/sdk/phs/Phs;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/inrix/sdk/phs/PhsController;->breadcrumbs:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v1, v11}, Lcom/inrix/sdk/phs/PhsQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 301
    move-object v10, v11

    .local v10, "arr$":[Lcom/inrix/sdk/phs/Phs;
    array-length v14, v10

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_0
    if-ge v13, v14, :cond_2

    aget-object v15, v10, v13

    .line 302
    .local v15, "point":Lcom/inrix/sdk/phs/Phs;
    if-nez v15, :cond_0

    .line 301
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 306
    :cond_0
    const/4 v1, 0x1

    new-array v9, v1, [F

    .line 307
    .local v9, "curDist":[F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/inrix/sdk/phs/PhsController;->geolocationController:Lcom/inrix/sdk/geolocation/GeolocationController;

    invoke-virtual {v1}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/inrix/sdk/phs/PhsController;->geolocationController:Lcom/inrix/sdk/geolocation/GeolocationController;

    invoke-virtual {v3}, Lcom/inrix/sdk/geolocation/GeolocationController;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v15}, Lcom/inrix/sdk/phs/Phs;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v15}, Lcom/inrix/sdk/phs/Phs;->getLongitude()D

    move-result-wide v7

    invoke-static/range {v1 .. v9}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 314
    const/4 v1, 0x0

    aget v1, v9, v1

    const v2, 0x47bc9852

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    .line 315
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/inrix/sdk/phs/PhsController;->breadcrumbs:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v1, v15}, Lcom/inrix/sdk/phs/PhsQueue;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 296
    .end local v9    # "curDist":[F
    .end local v10    # "arr$":[Lcom/inrix/sdk/phs/Phs;
    .end local v11    # "breadCrumbsArray":[Lcom/inrix/sdk/phs/Phs;
    .end local v12    # "builder":Ljava/lang/StringBuilder;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "point":Lcom/inrix/sdk/phs/Phs;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 319
    .restart local v9    # "curDist":[F
    .restart local v10    # "arr$":[Lcom/inrix/sdk/phs/Phs;
    .restart local v11    # "breadCrumbsArray":[Lcom/inrix/sdk/phs/Phs;
    .restart local v12    # "builder":Ljava/lang/StringBuilder;
    .restart local v13    # "i$":I
    .restart local v14    # "len$":I
    .restart local v15    # "point":Lcom/inrix/sdk/phs/Phs;
    :cond_1
    :try_start_1
    move/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/inrix/sdk/phs/Phs;->toQueryString(Z)Ljava/lang/String;

    move-result-object v16

    .line 320
    .local v16, "query":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 324
    .end local v9    # "curDist":[F
    .end local v15    # "point":Lcom/inrix/sdk/phs/Phs;
    .end local v16    # "query":Ljava/lang/String;
    :cond_2
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 325
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 327
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "BreadcrumbsPayload: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 331
    :cond_3
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1
.end method

.method public final declared-synchronized getPayload(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 242
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/inrix/sdk/phs/PhsController;->getPayload(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getPayload(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "addVehicleId"    # Z

    .prologue
    .line 255
    monitor-enter p0

    if-nez p1, :cond_0

    .line 256
    const/4 v5, 0x0

    .line 283
    :goto_0
    monitor-exit p0

    return-object v5

    .line 259
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    rsub-int v0, v5, 0x514

    .line 260
    .local v0, "availableSize":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 262
    .local v1, "builder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 263
    .local v2, "point":Lcom/inrix/sdk/phs/Phs;
    :goto_1
    iget-object v5, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v5}, Lcom/inrix/sdk/phs/PhsQueue;->peek()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "point":Lcom/inrix/sdk/phs/Phs;
    check-cast v2, Lcom/inrix/sdk/phs/Phs;

    .restart local v2    # "point":Lcom/inrix/sdk/phs/Phs;
    if-eqz v2, :cond_1

    .line 264
    invoke-virtual {v2, p2}, Lcom/inrix/sdk/phs/Phs;->toQueryString(Z)Ljava/lang/String;

    move-result-object v3

    .line 265
    .local v3, "query":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v4, v5, 0x1

    .line 266
    .local v4, "queryLength":I
    if-ge v0, v4, :cond_3

    .line 276
    .end local v3    # "query":Ljava/lang/String;
    .end local v4    # "queryLength":I
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 277
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 279
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Payload: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 283
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 270
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v4    # "queryLength":I
    :cond_3
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 271
    sub-int/2addr v0, v4

    .line 272
    iget-object v5, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v5}, Lcom/inrix/sdk/phs/PhsQueue;->poll()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 255
    .end local v0    # "availableSize":I
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "point":Lcom/inrix/sdk/phs/Phs;
    .end local v3    # "query":Ljava/lang/String;
    .end local v4    # "queryLength":I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized getQueue()Lcom/inrix/sdk/phs/PhsQueue;
    .locals 1

    .prologue
    .line 340
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getTotalPointsInQueue()I
    .locals 1

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v0}, Lcom/inrix/sdk/phs/PhsQueue;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onTimerDurationEnded()V
    .locals 1

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->trackingEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 231
    :goto_0
    monitor-exit p0

    return-void

    .line 230
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->timer:Lcom/inrix/sdk/phs/IPhsTimer;

    invoke-interface {v0}, Lcom/inrix/sdk/phs/IPhsTimer;->startNormal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onTimerTick()V
    .locals 2

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->trackingEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 205
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/inrix/sdk/phs/PhsController;->getTotalPointsInQueue()I

    move-result v0

    if-gtz v0, :cond_2

    .line 206
    iget v0, p0, Lcom/inrix/sdk/phs/PhsController;->retryTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/inrix/sdk/phs/PhsController;->retryTimes:I

    .line 207
    iget v0, p0, Lcom/inrix/sdk/phs/PhsController;->retryTimes:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/inrix/sdk/phs/PhsController;->stopTracking()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 214
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lcom/inrix/sdk/phs/PhsController;->retryTimes:I

    .line 215
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->sender:Lcom/inrix/sdk/phs/IPhsSender;

    iget-boolean v1, p0, Lcom/inrix/sdk/phs/PhsController;->userInitiated:Z

    invoke-interface {v0, v1}, Lcom/inrix/sdk/phs/IPhsSender;->send(Z)V

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->userInitiated:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized startTracking()V
    .locals 1

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->trackingEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 96
    :goto_0
    monitor-exit p0

    return-void

    .line 93
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->timer:Lcom/inrix/sdk/phs/IPhsTimer;

    invoke-interface {v0}, Lcom/inrix/sdk/phs/IPhsTimer;->startNormal()V

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/inrix/sdk/phs/PhsController;->retryTimes:I

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->trackingEnabled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized startTurboTracking()V
    .locals 1

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->timer:Lcom/inrix/sdk/phs/IPhsTimer;

    invoke-interface {v0}, Lcom/inrix/sdk/phs/IPhsTimer;->startTurbo()V

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Lcom/inrix/sdk/phs/PhsController;->retryTimes:I

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->trackingEnabled:Z

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->userInitiated:Z

    .line 107
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->alternateVehicleID:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized stopTracking()V
    .locals 1

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->timer:Lcom/inrix/sdk/phs/IPhsTimer;

    invoke-interface {v0}, Lcom/inrix/sdk/phs/IPhsTimer;->stop()V

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lcom/inrix/sdk/phs/PhsController;->retryTimes:I

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/inrix/sdk/phs/PhsController;->trackingEnabled:Z

    .line 119
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v0}, Lcom/inrix/sdk/phs/PhsQueue;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized trackLocation(Landroid/location/Location;)V
    .locals 5
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 139
    monitor-enter p0

    if-nez p1, :cond_1

    .line 141
    :try_start_0
    const-string/jumbo v2, "Tracking point skipped, point is null."

    invoke-static {v2}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 148
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-nez v2, :cond_3

    .line 150
    :cond_2
    const-string/jumbo v2, "Tracking point skipped, location doesn\'t have required parameters."

    invoke-static {v2}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 157
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lcom/inrix/sdk/phs/PhsController;->startTracking()V

    .line 160
    new-instance v0, Lcom/inrix/sdk/phs/Phs;

    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getVehicleId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/inrix/sdk/phs/Phs;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    .line 164
    .local v0, "point":Lcom/inrix/sdk/phs/Phs;
    iget-object v2, p0, Lcom/inrix/sdk/phs/PhsController;->lastPoint:Lcom/inrix/sdk/phs/Phs;

    if-eqz v2, :cond_4

    .line 165
    iget-object v2, p0, Lcom/inrix/sdk/phs/PhsController;->lastPoint:Lcom/inrix/sdk/phs/Phs;

    invoke-virtual {v0, v2}, Lcom/inrix/sdk/phs/Phs;->hasMinDistanceTo(Lcom/inrix/sdk/phs/Phs;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 167
    const-string/jumbo v2, "Tracking point skipped, location too close to the previous point."

    invoke-static {v2}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_4
    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsController;->lastPoint:Lcom/inrix/sdk/phs/Phs;

    .line 176
    iget-object v2, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v2, v0}, Lcom/inrix/sdk/phs/PhsQueue;->add(Lcom/inrix/sdk/phs/Phs;)Z

    .line 177
    iget-object v2, p0, Lcom/inrix/sdk/phs/PhsController;->breadcrumbs:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v2, v0}, Lcom/inrix/sdk/phs/PhsQueue;->add(Lcom/inrix/sdk/phs/Phs;)Z

    .line 180
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Point added to queue. Point: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/inrix/sdk/phs/PhsController;->lastPoint:Lcom/inrix/sdk/phs/Phs;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/inrix/sdk/phs/Phs;->toQueryString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 184
    iget-boolean v2, p0, Lcom/inrix/sdk/phs/PhsController;->userInitiated:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 185
    new-instance v1, Lcom/inrix/sdk/phs/Phs;

    iget-object v2, p0, Lcom/inrix/sdk/phs/PhsController;->alternateVehicleID:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Lcom/inrix/sdk/phs/Phs;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    .line 186
    .local v1, "turboPoint":Lcom/inrix/sdk/phs/Phs;
    iget-object v2, p0, Lcom/inrix/sdk/phs/PhsController;->queue:Lcom/inrix/sdk/phs/PhsQueue;

    invoke-virtual {v2, v1}, Lcom/inrix/sdk/phs/PhsQueue;->add(Lcom/inrix/sdk/phs/Phs;)Z

    .line 189
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Point added to queue. Turbo Point: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/inrix/sdk/phs/Phs;->toQueryString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
