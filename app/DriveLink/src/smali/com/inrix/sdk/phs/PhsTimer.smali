.class public final Lcom/inrix/sdk/phs/PhsTimer;
.super Ljava/lang/Object;
.source "PhsTimer.java"

# interfaces
.implements Lcom/inrix/sdk/phs/IPhsTimer;


# static fields
.field public static final NORMAL_INTERVAL:I = 0xea60

.field public static final SECONDS:I = 0x3e8

.field public static final TURBO_DURATION:I = 0x1d4c0

.field public static final TURBO_INTERVAL:I = 0x3a98


# instance fields
.field private currentDuration:I

.field private currentInterval:I

.field private currentMode:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

.field private final listener:Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

.field private final timerHandler:Landroid/os/Handler;

.field private final timerRunner:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;)V
    .locals 2
    .param p1, "eventListener"    # Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/inrix/sdk/phs/PhsTimer$1;

    invoke-direct {v0, p0}, Lcom/inrix/sdk/phs/PhsTimer$1;-><init>(Lcom/inrix/sdk/phs/PhsTimer;)V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerRunner:Ljava/lang/Runnable;

    .line 59
    iput-object p1, p0, Lcom/inrix/sdk/phs/PhsTimer;->listener:Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerHandler:Landroid/os/Handler;

    .line 61
    sget-object v0, Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;->Unknown:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentMode:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/inrix/sdk/phs/PhsTimer;)Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/phs/PhsTimer;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->listener:Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

    return-object v0
.end method

.method static synthetic access$100(Lcom/inrix/sdk/phs/PhsTimer;)I
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/phs/PhsTimer;

    .prologue
    .line 11
    iget v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I

    return v0
.end method

.method static synthetic access$120(Lcom/inrix/sdk/phs/PhsTimer;I)I
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/phs/PhsTimer;
    .param p1, "x1"    # I

    .prologue
    .line 11
    iget v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I

    return v0
.end method

.method static synthetic access$200(Lcom/inrix/sdk/phs/PhsTimer;)I
    .locals 1
    .param p0, "x0"    # Lcom/inrix/sdk/phs/PhsTimer;

    .prologue
    .line 11
    iget v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I

    return v0
.end method

.method static synthetic access$300(Lcom/inrix/sdk/phs/PhsTimer;)V
    .locals 0
    .param p0, "x0"    # Lcom/inrix/sdk/phs/PhsTimer;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/inrix/sdk/phs/PhsTimer;->restart()V

    return-void
.end method

.method private final restart()V
    .locals 4

    .prologue
    .line 120
    iget v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerRunner:Ljava/lang/Runnable;

    iget v2, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 124
    :cond_0
    return-void
.end method


# virtual methods
.method public final getCurrentMode()Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentMode:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    return-object v0
.end method

.method public final getDuration()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I

    return v0
.end method

.method public final getInterval()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I

    return v0
.end method

.method public start(I)V
    .locals 1
    .param p1, "interval"    # I

    .prologue
    .line 98
    const/high16 v0, -0x80000000

    invoke-virtual {p0, p1, v0}, Lcom/inrix/sdk/phs/PhsTimer;->start(II)V

    .line 99
    return-void
.end method

.method public start(II)V
    .locals 4
    .param p1, "interval"    # I
    .param p2, "duration"    # I

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/inrix/sdk/phs/PhsTimer;->stop()V

    .line 110
    iput p1, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I

    .line 111
    iput p2, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I

    .line 113
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerRunner:Ljava/lang/Runnable;

    iget v2, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 114
    return-void
.end method

.method public startNormal()V
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;->Normal:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentMode:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    .line 151
    const v0, 0xea60

    invoke-virtual {p0, v0}, Lcom/inrix/sdk/phs/PhsTimer;->start(I)V

    .line 154
    const-string/jumbo v0, "Timer has started in normal mode."

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method public startTurbo()V
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;->Turbo:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentMode:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    .line 166
    const/16 v0, 0x3a98

    const v1, 0x1d4c0

    invoke-virtual {p0, v0, v1}, Lcom/inrix/sdk/phs/PhsTimer;->start(II)V

    .line 169
    const-string/jumbo v0, "Timer has started in TURBO mode."

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 134
    const-string/jumbo v0, "Timer stopped."

    invoke-static {v0}, Lcom/inrix/sdk/InrixDebug;->LogD(Ljava/lang/String;)V

    .line 137
    iput v1, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I

    .line 138
    iput v1, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I

    .line 139
    sget-object v0, Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;->Unknown:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    iput-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->currentMode:Lcom/inrix/sdk/phs/IPhsTimer$PhsTimerMode;

    .line 140
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/inrix/sdk/phs/PhsTimer;->timerRunner:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 141
    return-void
.end method
