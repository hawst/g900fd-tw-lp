.class Lcom/inrix/sdk/phs/PhsTimer$1;
.super Ljava/lang/Object;
.source "PhsTimer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inrix/sdk/phs/PhsTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/inrix/sdk/phs/PhsTimer;


# direct methods
.method constructor <init>(Lcom/inrix/sdk/phs/PhsTimer;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # getter for: Lcom/inrix/sdk/phs/PhsTimer;->listener:Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$000(Lcom/inrix/sdk/phs/PhsTimer;)Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # getter for: Lcom/inrix/sdk/phs/PhsTimer;->listener:Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$000(Lcom/inrix/sdk/phs/PhsTimer;)Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

    move-result-object v0

    invoke-interface {v0}, Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;->onTimerTick()V

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # getter for: Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$100(Lcom/inrix/sdk/phs/PhsTimer;)I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 37
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    iget-object v1, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # getter for: Lcom/inrix/sdk/phs/PhsTimer;->currentInterval:I
    invoke-static {v1}, Lcom/inrix/sdk/phs/PhsTimer;->access$200(Lcom/inrix/sdk/phs/PhsTimer;)I

    move-result v1

    # -= operator for: Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I
    invoke-static {v0, v1}, Lcom/inrix/sdk/phs/PhsTimer;->access$120(Lcom/inrix/sdk/phs/PhsTimer;I)I

    .line 38
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # getter for: Lcom/inrix/sdk/phs/PhsTimer;->currentDuration:I
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$100(Lcom/inrix/sdk/phs/PhsTimer;)I

    move-result v0

    if-gtz v0, :cond_2

    .line 39
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    invoke-virtual {v0}, Lcom/inrix/sdk/phs/PhsTimer;->stop()V

    .line 40
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # getter for: Lcom/inrix/sdk/phs/PhsTimer;->listener:Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$000(Lcom/inrix/sdk/phs/PhsTimer;)Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # getter for: Lcom/inrix/sdk/phs/PhsTimer;->listener:Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$000(Lcom/inrix/sdk/phs/PhsTimer;)Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;

    move-result-object v0

    invoke-interface {v0}, Lcom/inrix/sdk/phs/IPhsTimer$IPhsTimerEvents;->onTimerDurationEnded()V

    .line 49
    :cond_1
    :goto_0
    return-void

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # invokes: Lcom/inrix/sdk/phs/PhsTimer;->restart()V
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$300(Lcom/inrix/sdk/phs/PhsTimer;)V

    goto :goto_0

    .line 47
    :cond_3
    iget-object v0, p0, Lcom/inrix/sdk/phs/PhsTimer$1;->this$0:Lcom/inrix/sdk/phs/PhsTimer;

    # invokes: Lcom/inrix/sdk/phs/PhsTimer;->restart()V
    invoke-static {v0}, Lcom/inrix/sdk/phs/PhsTimer;->access$300(Lcom/inrix/sdk/phs/PhsTimer;)V

    goto :goto_0
.end method
