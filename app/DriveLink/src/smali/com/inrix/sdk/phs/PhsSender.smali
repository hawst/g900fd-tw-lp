.class public final Lcom/inrix/sdk/phs/PhsSender;
.super Ljava/lang/Object;
.source "PhsSender.java"

# interfaces
.implements Lcom/inrix/sdk/phs/IPhsSender;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public send(Z)V
    .locals 5
    .param p1, "userInitiated"    # Z

    .prologue
    .line 25
    new-instance v1, Lcom/inrix/sdk/phs/PhsRequest;

    new-instance v2, Lcom/inrix/sdk/phs/PhsSender$1;

    invoke-direct {v2, p0}, Lcom/inrix/sdk/phs/PhsSender$1;-><init>(Lcom/inrix/sdk/phs/PhsSender;)V

    new-instance v3, Lcom/inrix/sdk/phs/PhsSender$2;

    invoke-direct {v3, p0}, Lcom/inrix/sdk/phs/PhsSender$2;-><init>(Lcom/inrix/sdk/phs/PhsSender;)V

    invoke-direct {v1, v2, v3}, Lcom/inrix/sdk/phs/PhsRequest;-><init>(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 38
    .local v1, "request":Lcom/inrix/sdk/phs/PhsRequest;
    const-string/jumbo v2, "phs"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/inrix/sdk/phs/PhsRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 39
    const-string/jumbo v2, "AnonymousVehicleId"

    invoke-static {}, Lcom/inrix/sdk/utils/UserPreferences;->getVehicleId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/inrix/sdk/phs/PhsRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 42
    if-eqz p1, :cond_0

    .line 43
    const-string/jumbo v2, "UserInitiated"

    invoke-virtual {v1, v2, p1}, Lcom/inrix/sdk/phs/PhsRequest;->setParameter(Ljava/lang/String;Z)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 47
    :cond_0
    invoke-static {}, Lcom/inrix/sdk/phs/PhsController;->getInstance()Lcom/inrix/sdk/phs/PhsController;

    move-result-object v2

    invoke-virtual {v1}, Lcom/inrix/sdk/phs/PhsRequest;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/inrix/sdk/phs/PhsController;->getPayload(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "payload":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string/jumbo v2, "phs"

    invoke-virtual {v1, v2, v0}, Lcom/inrix/sdk/phs/PhsRequest;->setParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/inrix/sdk/network/request/NetworkRequest;

    .line 55
    invoke-static {}, Lcom/inrix/sdk/network/request/RequestExecutor;->getInstance()Lcom/inrix/sdk/network/request/RequestExecutor;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/inrix/sdk/network/request/RequestExecutor;->execute(Lcom/inrix/sdk/network/request/NetworkRequest;)V

    .line 58
    const-string/jumbo v2, "PHS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "PHS request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/inrix/sdk/phs/PhsRequest;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
