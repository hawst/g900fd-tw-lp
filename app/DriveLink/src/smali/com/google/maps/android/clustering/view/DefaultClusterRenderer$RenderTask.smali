.class Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;
.super Ljava/lang/Object;
.source "DefaultClusterRenderer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenderTask"
.end annotation


# instance fields
.field final clusters:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<+",
            "Lcom/google/maps/android/clustering/Cluster",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private mCallback:Ljava/lang/Runnable;

.field private mMapZoom:F

.field private mProjection:Lcom/google/android/gms/maps/Projection;

.field private mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

.field final synthetic this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;


# direct methods
.method private constructor <init>(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<+",
            "Lcom/google/maps/android/clustering/Cluster",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 287
    .local p2, "clusters":Ljava/util/Set;, "Ljava/util/Set<+Lcom/google/maps/android/clustering/Cluster<TT;>;>;"
    iput-object p1, p0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    iput-object p2, p0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->clusters:Ljava/util/Set;

    .line 289
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Ljava/util/Set;Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;)V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0, p1, p2}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;-><init>(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Ljava/util/Set;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 24
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->clusters:Ljava/util/Set;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v20, v0

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->mClusters:Ljava/util/Set;
    invoke-static/range {v20 .. v20}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$2(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;)Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mCallback:Ljava/lang/Runnable;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/lang/Runnable;->run()V

    .line 403
    :goto_0
    return-void

    .line 316
    :cond_0
    new-instance v8, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v8, v0, v1}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;-><init>(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;)V

    .line 318
    .local v8, "markerModifier":Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;, "Lcom/google/maps/android/clustering/view/DefaultClusterRenderer<TT;>.MarkerModifier;"
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mMapZoom:F

    move/from16 v16, v0

    .line 319
    .local v16, "zoom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->mZoom:F
    invoke-static/range {v19 .. v19}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$1(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;)F

    move-result v19

    cmpl-float v19, v16, v19

    if-lez v19, :cond_5

    const/16 v18, 0x1

    .line 320
    .local v18, "zoomingIn":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->mZoom:F
    invoke-static/range {v19 .. v19}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$1(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;)F

    move-result v19

    sub-float v17, v16, v19

    .line 322
    .local v17, "zoomDelta":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->mMarkers:Ljava/util/Set;
    invoke-static/range {v19 .. v19}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$3(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;)Ljava/util/Set;

    move-result-object v9

    .line 323
    .local v9, "markersToRemove":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mProjection:Lcom/google/android/gms/maps/Projection;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/maps/Projection;->getVisibleRegion()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v15, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->latLngBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 328
    .local v15, "visibleBounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    const/4 v6, 0x0

    .line 329
    .local v6, "existingClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->mClusters:Ljava/util/Set;
    invoke-static/range {v19 .. v19}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$2(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;)Ljava/util/Set;

    move-result-object v19

    if-eqz v19, :cond_2

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->SHOULD_ANIMATE:Z
    invoke-static {}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$4()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 330
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "existingClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 331
    .restart local v6    # "existingClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->mClusters:Ljava/util/Set;
    invoke-static/range {v19 .. v19}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$2(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;)Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_1
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_6

    .line 340
    :cond_2
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 341
    .local v11, "newMarkers":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->clusters:Ljava/util/Set;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_7

    .line 358
    invoke-virtual {v8}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->waitUntilFree()V

    .line 362
    invoke-interface {v9, v11}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 366
    const/4 v10, 0x0

    .line 367
    .local v10, "newClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->SHOULD_ANIMATE:Z
    invoke-static {}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$4()Z

    move-result v19

    if-eqz v19, :cond_4

    .line 368
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "newClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 369
    .restart local v10    # "newClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->clusters:Ljava/util/Set;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_3
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_a

    .line 378
    :cond_4
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_5
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_b

    .line 396
    invoke-virtual {v8}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->waitUntilFree()V

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v11}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$6(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Ljava/util/Set;)V

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->clusters:Ljava/util/Set;

    move-object/from16 v20, v0

    invoke-static/range {v19 .. v20}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$7(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Ljava/util/Set;)V

    .line 400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$8(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;F)V

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mCallback:Ljava/lang/Runnable;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0

    .line 319
    .end local v6    # "existingClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    .end local v9    # "markersToRemove":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;>;"
    .end local v10    # "newClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    .end local v11    # "newMarkers":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;>;"
    .end local v15    # "visibleBounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    .end local v17    # "zoomDelta":F
    .end local v18    # "zoomingIn":Z
    :cond_5
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 331
    .restart local v6    # "existingClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    .restart local v9    # "markersToRemove":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;>;"
    .restart local v15    # "visibleBounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    .restart local v17    # "zoomDelta":F
    .restart local v18    # "zoomingIn":Z
    :cond_6
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/maps/android/clustering/Cluster;

    .line 332
    .local v4, "c":Lcom/google/maps/android/clustering/Cluster;, "Lcom/google/maps/android/clustering/Cluster<TT;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->shouldRenderAsCluster(Lcom/google/maps/android/clustering/Cluster;)Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-interface {v4}, Lcom/google/maps/android/clustering/Cluster;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/google/android/gms/maps/model/LatLngBounds;->contains(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

    move-object/from16 v20, v0

    invoke-interface {v4}, Lcom/google/maps/android/clustering/Cluster;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/google/maps/android/projection/SphericalMercatorProjection;->toPoint(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/android/projection/Point;

    move-result-object v14

    .line 334
    .local v14, "point":Lcom/google/maps/android/geometry/Point;
    invoke-interface {v6, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 341
    .end local v4    # "c":Lcom/google/maps/android/clustering/Cluster;, "Lcom/google/maps/android/clustering/Cluster<TT;>;"
    .end local v14    # "point":Lcom/google/maps/android/geometry/Point;
    .restart local v11    # "newMarkers":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;>;"
    :cond_7
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/maps/android/clustering/Cluster;

    .line 342
    .restart local v4    # "c":Lcom/google/maps/android/clustering/Cluster;, "Lcom/google/maps/android/clustering/Cluster<TT;>;"
    invoke-interface {v4}, Lcom/google/maps/android/clustering/Cluster;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/google/android/gms/maps/model/LatLngBounds;->contains(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v12

    .line 343
    .local v12, "onScreen":Z
    if-eqz v18, :cond_9

    if-eqz v12, :cond_9

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->SHOULD_ANIMATE:Z
    invoke-static {}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$4()Z

    move-result v20

    if-eqz v20, :cond_9

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

    move-object/from16 v20, v0

    invoke-interface {v4}, Lcom/google/maps/android/clustering/Cluster;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/google/maps/android/projection/SphericalMercatorProjection;->toPoint(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/android/projection/Point;

    move-result-object v14

    .line 345
    .restart local v14    # "point":Lcom/google/maps/android/geometry/Point;
    # invokes: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->findClosestCluster(Ljava/util/List;Lcom/google/maps/android/geometry/Point;)Lcom/google/maps/android/geometry/Point;
    invoke-static {v6, v14}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$5(Ljava/util/List;Lcom/google/maps/android/geometry/Point;)Lcom/google/maps/android/geometry/Point;

    move-result-object v5

    .line 346
    .local v5, "closest":Lcom/google/maps/android/geometry/Point;
    if-eqz v5, :cond_8

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/google/maps/android/projection/SphericalMercatorProjection;->toLatLng(Lcom/google/maps/android/geometry/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    .line 348
    .local v3, "animateTo":Lcom/google/android/gms/maps/model/LatLng;
    const/16 v20, 0x1

    new-instance v21, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v4, v11, v3}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;-><init>(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Lcom/google/maps/android/clustering/Cluster;Ljava/util/Set;Lcom/google/android/gms/maps/model/LatLng;)V

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->add(ZLcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;)V

    goto/16 :goto_3

    .line 350
    .end local v3    # "animateTo":Lcom/google/android/gms/maps/model/LatLng;
    :cond_8
    const/16 v20, 0x1

    new-instance v21, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v4, v11, v2}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;-><init>(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Lcom/google/maps/android/clustering/Cluster;Ljava/util/Set;Lcom/google/android/gms/maps/model/LatLng;)V

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->add(ZLcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;)V

    goto/16 :goto_3

    .line 353
    .end local v5    # "closest":Lcom/google/maps/android/geometry/Point;
    .end local v14    # "point":Lcom/google/maps/android/geometry/Point;
    :cond_9
    new-instance v20, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v4, v11, v2}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;-><init>(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;Lcom/google/maps/android/clustering/Cluster;Ljava/util/Set;Lcom/google/android/gms/maps/model/LatLng;)V

    move-object/from16 v0, v20

    invoke-virtual {v8, v12, v0}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->add(ZLcom/google/maps/android/clustering/view/DefaultClusterRenderer$CreateMarkerTask;)V

    goto/16 :goto_3

    .line 369
    .end local v4    # "c":Lcom/google/maps/android/clustering/Cluster;, "Lcom/google/maps/android/clustering/Cluster<TT;>;"
    .end local v12    # "onScreen":Z
    .restart local v10    # "newClustersOnScreen":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/android/geometry/Point;>;"
    :cond_a
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/maps/android/clustering/Cluster;

    .line 370
    .restart local v4    # "c":Lcom/google/maps/android/clustering/Cluster;, "Lcom/google/maps/android/clustering/Cluster<TT;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->shouldRenderAsCluster(Lcom/google/maps/android/clustering/Cluster;)Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface {v4}, Lcom/google/maps/android/clustering/Cluster;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/google/android/gms/maps/model/LatLngBounds;->contains(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

    move-object/from16 v20, v0

    invoke-interface {v4}, Lcom/google/maps/android/clustering/Cluster;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/google/maps/android/projection/SphericalMercatorProjection;->toPoint(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/android/projection/Point;

    move-result-object v13

    .line 372
    .local v13, "p":Lcom/google/maps/android/geometry/Point;
    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 378
    .end local v4    # "c":Lcom/google/maps/android/clustering/Cluster;, "Lcom/google/maps/android/clustering/Cluster<TT;>;"
    .end local v13    # "p":Lcom/google/maps/android/geometry/Point;
    :cond_b
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;

    .line 379
    .local v7, "marker":Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;, "Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;"
    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->position:Lcom/google/android/gms/maps/model/LatLng;
    invoke-static {v7}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->access$0(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/google/android/gms/maps/model/LatLngBounds;->contains(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v12

    .line 382
    .restart local v12    # "onScreen":Z
    if-nez v18, :cond_d

    const/high16 v20, -0x3fc00000    # -3.0f

    cmpl-float v20, v17, v20

    if-lez v20, :cond_d

    if-eqz v12, :cond_d

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->SHOULD_ANIMATE:Z
    invoke-static {}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$4()Z

    move-result v20

    if-eqz v20, :cond_d

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

    move-object/from16 v20, v0

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->position:Lcom/google/android/gms/maps/model/LatLng;
    invoke-static {v7}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->access$0(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/google/maps/android/projection/SphericalMercatorProjection;->toPoint(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/android/projection/Point;

    move-result-object v14

    .line 384
    .restart local v14    # "point":Lcom/google/maps/android/geometry/Point;
    # invokes: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->findClosestCluster(Ljava/util/List;Lcom/google/maps/android/geometry/Point;)Lcom/google/maps/android/geometry/Point;
    invoke-static {v10, v14}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$5(Ljava/util/List;Lcom/google/maps/android/geometry/Point;)Lcom/google/maps/android/geometry/Point;

    move-result-object v5

    .line 385
    .restart local v5    # "closest":Lcom/google/maps/android/geometry/Point;
    if-eqz v5, :cond_c

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/google/maps/android/projection/SphericalMercatorProjection;->toLatLng(Lcom/google/maps/android/geometry/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    .line 387
    .restart local v3    # "animateTo":Lcom/google/android/gms/maps/model/LatLng;
    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->position:Lcom/google/android/gms/maps/model/LatLng;
    invoke-static {v7}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->access$0(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v7, v0, v3}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->animateThenRemove(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    goto/16 :goto_5

    .line 389
    .end local v3    # "animateTo":Lcom/google/android/gms/maps/model/LatLng;
    :cond_c
    const/16 v20, 0x1

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->marker:Lcom/google/android/gms/maps/model/Marker;
    invoke-static {v7}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->access$1(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v21

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->remove(ZLcom/google/android/gms/maps/model/Marker;)V

    goto/16 :goto_5

    .line 392
    .end local v5    # "closest":Lcom/google/maps/android/geometry/Point;
    .end local v14    # "point":Lcom/google/maps/android/geometry/Point;
    :cond_d
    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->marker:Lcom/google/android/gms/maps/model/Marker;
    invoke-static {v7}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;->access$1(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerWithPosition;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v12, v0}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$MarkerModifier;->remove(ZLcom/google/android/gms/maps/model/Marker;)V

    goto/16 :goto_5
.end method

.method public setCallback(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mCallback:Ljava/lang/Runnable;

    .line 298
    return-void
.end method

.method public setMapZoom(F)V
    .locals 7
    .param p1, "zoom"    # F

    .prologue
    .line 305
    iput p1, p0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mMapZoom:F

    .line 306
    new-instance v0, Lcom/google/maps/android/projection/SphericalMercatorProjection;

    const-wide/high16 v1, 0x4070000000000000L    # 256.0

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    iget-object v5, p0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->this$0:Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;

    # getter for: Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->mZoom:F
    invoke-static {v5}, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;->access$1(Lcom/google/maps/android/clustering/view/DefaultClusterRenderer;)F

    move-result v5

    invoke-static {p1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    float-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    mul-double/2addr v1, v3

    invoke-direct {v0, v1, v2}, Lcom/google/maps/android/projection/SphericalMercatorProjection;-><init>(D)V

    iput-object v0, p0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mSphericalMercatorProjection:Lcom/google/maps/android/projection/SphericalMercatorProjection;

    .line 307
    return-void
.end method

.method public setProjection(Lcom/google/android/gms/maps/Projection;)V
    .locals 0
    .param p1, "projection"    # Lcom/google/android/gms/maps/Projection;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/google/maps/android/clustering/view/DefaultClusterRenderer$RenderTask;->mProjection:Lcom/google/android/gms/maps/Projection;

    .line 302
    return-void
.end method
