.class public Lcom/google/maps/android/quadtree/PointQuadTree;
.super Ljava/lang/Object;
.source "PointQuadTree.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/maps/android/quadtree/PointQuadTree$Item;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/maps/android/quadtree/PointQuadTree$Item;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final MAX_DEPTH:I = 0x1e

.field private static final MAX_ELEMENTS:I = 0x3c


# instance fields
.field private final mBounds:Lcom/google/maps/android/geometry/Bounds;

.field private mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/maps/android/quadtree/PointQuadTree",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mDepth:I

.field private mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(DDDD)V
    .locals 9
    .param p1, "minX"    # D
    .param p3, "maxX"    # D
    .param p5, "minY"    # D
    .param p7, "maxY"    # D

    .prologue
    .line 75
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    new-instance v0, Lcom/google/maps/android/geometry/Bounds;

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    move-wide/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/maps/android/geometry/Bounds;-><init>(DDDD)V

    invoke-direct {p0, v0}, Lcom/google/maps/android/quadtree/PointQuadTree;-><init>(Lcom/google/maps/android/geometry/Bounds;)V

    .line 76
    return-void
.end method

.method private constructor <init>(DDDDI)V
    .locals 10
    .param p1, "minX"    # D
    .param p3, "maxX"    # D
    .param p5, "minY"    # D
    .param p7, "maxY"    # D
    .param p9, "depth"    # I

    .prologue
    .line 83
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    new-instance v1, Lcom/google/maps/android/geometry/Bounds;

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move-wide/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/google/maps/android/geometry/Bounds;-><init>(DDDD)V

    move/from16 v0, p9

    invoke-direct {p0, v1, v0}, Lcom/google/maps/android/quadtree/PointQuadTree;-><init>(Lcom/google/maps/android/geometry/Bounds;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Lcom/google/maps/android/geometry/Bounds;)V
    .locals 1
    .param p1, "bounds"    # Lcom/google/maps/android/geometry/Bounds;

    .prologue
    .line 79
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/maps/android/quadtree/PointQuadTree;-><init>(Lcom/google/maps/android/geometry/Bounds;I)V

    .line 80
    return-void
.end method

.method private constructor <init>(Lcom/google/maps/android/geometry/Bounds;I)V
    .locals 1
    .param p1, "bounds"    # Lcom/google/maps/android/geometry/Bounds;
    .param p2, "depth"    # I

    .prologue
    .line 86
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    .line 87
    iput-object p1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    .line 88
    iput p2, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mDepth:I

    .line 89
    return-void
.end method

.method private insert(DDLcom/google/maps/android/quadtree/PointQuadTree$Item;)Z
    .locals 9
    .param p1, "x"    # D
    .param p3, "y"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDTT;)Z"
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    .local p5, "item":Lcom/google/maps/android/quadtree/PointQuadTree$Item;, "TT;"
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/maps/android/geometry/Bounds;->contains(DD)Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    const/4 v1, 0x0

    .line 119
    :goto_0
    return v1

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    if-eqz v1, :cond_3

    .line 104
    iget-object v7, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    array-length v8, v7

    const/4 v1, 0x0

    move v6, v1

    :goto_1
    if-lt v6, v8, :cond_1

    .line 109
    const/4 v1, 0x0

    goto :goto_0

    .line 104
    :cond_1
    aget-object v0, v7, v6

    .local v0, "quad":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    .line 105
    invoke-direct/range {v0 .. v5}, Lcom/google/maps/android/quadtree/PointQuadTree;->insert(DDLcom/google/maps/android/quadtree/PointQuadTree$Item;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 106
    const/4 v1, 0x1

    goto :goto_0

    .line 104
    :cond_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    .line 112
    .end local v0    # "quad":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    :cond_3
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    if-nez v1, :cond_4

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    .line 115
    :cond_4
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    invoke-interface {v1, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x3c

    if-le v1, v2, :cond_5

    iget v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mDepth:I

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_5

    .line 117
    invoke-direct {p0}, Lcom/google/maps/android/quadtree/PointQuadTree;->split()V

    .line 119
    :cond_5
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private remove(DDLcom/google/maps/android/quadtree/PointQuadTree$Item;)Z
    .locals 10
    .param p1, "x"    # D
    .param p3, "y"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDTT;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    .local p5, "item":Lcom/google/maps/android/quadtree/PointQuadTree$Item;, "TT;"
    const/4 v6, 0x0

    .line 153
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/maps/android/geometry/Bounds;->contains(DD)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v6

    .line 164
    :goto_0
    return v1

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    if-eqz v1, :cond_3

    .line 157
    iget-object v8, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    array-length v9, v8

    move v7, v6

    :goto_1
    if-lt v7, v9, :cond_1

    move v1, v6

    .line 162
    goto :goto_0

    .line 157
    :cond_1
    aget-object v0, v8, v7

    .local v0, "quad":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    .line 158
    invoke-direct/range {v0 .. v5}, Lcom/google/maps/android/quadtree/PointQuadTree;->remove(DDLcom/google/maps/android/quadtree/PointQuadTree$Item;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    const/4 v1, 0x1

    goto :goto_0

    .line 157
    :cond_2
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    .line 164
    .end local v0    # "quad":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    :cond_3
    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    invoke-interface {v1, p5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method private search(Lcom/google/maps/android/geometry/Bounds;Ljava/util/Collection;)V
    .locals 5
    .param p1, "searchBounds"    # Lcom/google/maps/android/geometry/Bounds;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/android/geometry/Bounds;",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    .local p2, "results":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    iget-object v2, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    invoke-virtual {v2, p1}, Lcom/google/maps/android/geometry/Bounds;->intersects(Lcom/google/maps/android/geometry/Bounds;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 200
    :cond_0
    return-void

    .line 189
    :cond_1
    iget-object v2, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    if-eqz v2, :cond_2

    .line 190
    iget-object v3, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 191
    .local v1, "quad":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    invoke-direct {v1, p1, p2}, Lcom/google/maps/android/quadtree/PointQuadTree;->search(Lcom/google/maps/android/geometry/Bounds;Ljava/util/Collection;)V

    .line 190
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 193
    .end local v1    # "quad":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    :cond_2
    iget-object v2, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 194
    iget-object v2, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/android/quadtree/PointQuadTree$Item;

    .line 195
    .local v0, "item":Lcom/google/maps/android/quadtree/PointQuadTree$Item;, "TT;"
    invoke-interface {v0}, Lcom/google/maps/android/quadtree/PointQuadTree$Item;->getPoint()Lcom/google/maps/android/geometry/Point;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/maps/android/geometry/Bounds;->contains(Lcom/google/maps/android/geometry/Point;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 196
    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private split()V
    .locals 14

    .prologue
    .line 126
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    const/4 v0, 0x4

    new-array v12, v0, [Lcom/google/maps/android/quadtree/PointQuadTree;

    const/4 v13, 0x0

    .line 127
    new-instance v0, Lcom/google/maps/android/quadtree/PointQuadTree;

    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v1, v1, Lcom/google/maps/android/geometry/Bounds;->minX:D

    iget-object v3, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v3, v3, Lcom/google/maps/android/geometry/Bounds;->midX:D

    iget-object v5, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v5, v5, Lcom/google/maps/android/geometry/Bounds;->minY:D

    iget-object v7, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v7, v7, Lcom/google/maps/android/geometry/Bounds;->midY:D

    iget v9, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mDepth:I

    add-int/lit8 v9, v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/maps/android/quadtree/PointQuadTree;-><init>(DDDDI)V

    aput-object v0, v12, v13

    const/4 v13, 0x1

    .line 128
    new-instance v0, Lcom/google/maps/android/quadtree/PointQuadTree;

    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v1, v1, Lcom/google/maps/android/geometry/Bounds;->midX:D

    iget-object v3, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v3, v3, Lcom/google/maps/android/geometry/Bounds;->maxX:D

    iget-object v5, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v5, v5, Lcom/google/maps/android/geometry/Bounds;->minY:D

    iget-object v7, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v7, v7, Lcom/google/maps/android/geometry/Bounds;->midY:D

    iget v9, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mDepth:I

    add-int/lit8 v9, v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/maps/android/quadtree/PointQuadTree;-><init>(DDDDI)V

    aput-object v0, v12, v13

    const/4 v13, 0x2

    .line 129
    new-instance v0, Lcom/google/maps/android/quadtree/PointQuadTree;

    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v1, v1, Lcom/google/maps/android/geometry/Bounds;->minX:D

    iget-object v3, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v3, v3, Lcom/google/maps/android/geometry/Bounds;->midX:D

    iget-object v5, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v5, v5, Lcom/google/maps/android/geometry/Bounds;->midY:D

    iget-object v7, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v7, v7, Lcom/google/maps/android/geometry/Bounds;->maxY:D

    iget v9, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mDepth:I

    add-int/lit8 v9, v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/maps/android/quadtree/PointQuadTree;-><init>(DDDDI)V

    aput-object v0, v12, v13

    const/4 v13, 0x3

    .line 130
    new-instance v0, Lcom/google/maps/android/quadtree/PointQuadTree;

    iget-object v1, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v1, v1, Lcom/google/maps/android/geometry/Bounds;->midX:D

    iget-object v3, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v3, v3, Lcom/google/maps/android/geometry/Bounds;->maxX:D

    iget-object v5, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v5, v5, Lcom/google/maps/android/geometry/Bounds;->midY:D

    iget-object v7, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mBounds:Lcom/google/maps/android/geometry/Bounds;

    iget-wide v7, v7, Lcom/google/maps/android/geometry/Bounds;->maxY:D

    iget v9, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mDepth:I

    add-int/lit8 v9, v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/maps/android/quadtree/PointQuadTree;-><init>(DDDDI)V

    aput-object v0, v12, v13

    .line 126
    iput-object v12, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    .line 133
    iget-object v11, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    .line 134
    .local v11, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    .line 136
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 140
    return-void

    .line 136
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/maps/android/quadtree/PointQuadTree$Item;

    .line 138
    .local v10, "item":Lcom/google/maps/android/quadtree/PointQuadTree$Item;, "TT;"
    invoke-virtual {p0, v10}, Lcom/google/maps/android/quadtree/PointQuadTree;->add(Lcom/google/maps/android/quadtree/PointQuadTree$Item;)V

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/google/maps/android/quadtree/PointQuadTree$Item;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    .local p1, "item":Lcom/google/maps/android/quadtree/PointQuadTree$Item;, "TT;"
    invoke-interface {p1}, Lcom/google/maps/android/quadtree/PointQuadTree$Item;->getPoint()Lcom/google/maps/android/geometry/Point;

    move-result-object v6

    .line 96
    .local v6, "point":Lcom/google/maps/android/geometry/Point;
    iget-wide v1, v6, Lcom/google/maps/android/geometry/Point;->x:D

    iget-wide v3, v6, Lcom/google/maps/android/geometry/Point;->y:D

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/maps/android/quadtree/PointQuadTree;->insert(DDLcom/google/maps/android/quadtree/PointQuadTree$Item;)Z

    .line 97
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 169
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mChildren:[Lcom/google/maps/android/quadtree/PointQuadTree;

    .line 170
    iget-object v0, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/maps/android/quadtree/PointQuadTree;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 173
    :cond_0
    return-void
.end method

.method public remove(Lcom/google/maps/android/quadtree/PointQuadTree$Item;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 148
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    .local p1, "item":Lcom/google/maps/android/quadtree/PointQuadTree$Item;, "TT;"
    invoke-interface {p1}, Lcom/google/maps/android/quadtree/PointQuadTree$Item;->getPoint()Lcom/google/maps/android/geometry/Point;

    move-result-object v6

    .line 149
    .local v6, "point":Lcom/google/maps/android/geometry/Point;
    iget-wide v1, v6, Lcom/google/maps/android/geometry/Point;->x:D

    iget-wide v3, v6, Lcom/google/maps/android/geometry/Point;->y:D

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/maps/android/quadtree/PointQuadTree;->remove(DDLcom/google/maps/android/quadtree/PointQuadTree$Item;)Z

    move-result v0

    return v0
.end method

.method public search(Lcom/google/maps/android/geometry/Bounds;)Ljava/util/Collection;
    .locals 1
    .param p1, "searchBounds"    # Lcom/google/maps/android/geometry/Bounds;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/android/geometry/Bounds;",
            ")",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 179
    .local p0, "this":Lcom/google/maps/android/quadtree/PointQuadTree;, "Lcom/google/maps/android/quadtree/PointQuadTree<TT;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v0, "results":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0, p1, v0}, Lcom/google/maps/android/quadtree/PointQuadTree;->search(Lcom/google/maps/android/geometry/Bounds;Ljava/util/Collection;)V

    .line 181
    return-object v0
.end method
