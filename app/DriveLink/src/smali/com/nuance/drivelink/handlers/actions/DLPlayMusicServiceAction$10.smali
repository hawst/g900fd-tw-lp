.class Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$10;
.super Ljava/lang/Object;
.source "DLPlayMusicServiceAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->handleMusicSearchResult(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$10;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 643
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 648
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$10;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$10;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$7(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicService(Ljava/util/ArrayList;)V

    .line 649
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSearchResultFlag(Z)V

    .line 650
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 657
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 663
    return-void
.end method
