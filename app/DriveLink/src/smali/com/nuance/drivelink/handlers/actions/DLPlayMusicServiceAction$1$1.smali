.class Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1$1;
.super Ljava/lang/Object;
.source "DLPlayMusicServiceAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->onResponseRequestSearchAllMusic(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1$1;->this$1:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 128
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1$1;->this$1:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;
    invoke-static {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->access$0(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1$1;->this$1:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->access$0(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v1

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$7(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicService(Ljava/util/ArrayList;)V

    .line 134
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    .line 135
    const/4 v1, 0x1

    .line 134
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSearchResultFlag(Z)V

    .line 136
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 143
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 149
    return-void
.end method
