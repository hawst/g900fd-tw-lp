.class public Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;
.super Lcom/nuance/sample/handlers/SampleProcessCommandHandler;
.source "DLProcessCommandHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;

    .line 26
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 25
    sput-object v0, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->TAG:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 32
    const-string/jumbo v3, "Command"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "command":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->isMusicDomain(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 35
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-nez v3, :cond_1

    .line 148
    :cond_0
    :goto_0
    return v6

    .line 39
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    iput-boolean v5, v3, Lcom/sec/android/automotive/drivelink/music/MusicService;->playByVoice:Z

    .line 42
    :cond_2
    const-string/jumbo v3, "yes"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 43
    const-string/jumbo v3, "DM_CONFIRM_YES"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_3
    const-string/jumbo v3, "send"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 45
    const-string/jumbo v3, "DM_CONFIRM_SEND"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 46
    :cond_4
    const-string/jumbo v3, "cancel"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 48
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->isMusicDomain(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 49
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_5
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 54
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->ismUserPausePriority()Z

    move-result v3

    if-nez v3, :cond_7

    .line 55
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "DM_MAIN"

    if-eq v3, v4, :cond_6

    .line 56
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "DM_MUSIC_PLAYER"

    .line 55
    if-ne v3, v4, :cond_7

    .line 59
    :cond_6
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3, v5, v5, v5}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 60
    sget-object v3, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "MUSICService Voice User Setting Pause"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    .line 63
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoPauseMusic(Z)V

    .line 67
    :cond_7
    const-string/jumbo v3, "DM_CANCEL"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_8
    const-string/jumbo v3, "no"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 69
    const-string/jumbo v3, "DM_CONFIRM_NO"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 70
    :cond_9
    const-string/jumbo v3, "execute"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 71
    const-string/jumbo v3, "DM_CONFIRM_EXCUTE"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 72
    :cond_a
    const-string/jumbo v3, "ignore"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 73
    const-string/jumbo v3, "DM_CONFIRM_IGNORE"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    :cond_b
    const-string/jumbo v3, "repeat"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 75
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->isMusicDomain(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 76
    invoke-virtual {p0, v0}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->noSupportInMusic(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 78
    :cond_c
    const-string/jumbo v3, "DM_READ"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 79
    :cond_d
    const-string/jumbo v3, "lookup"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 80
    const-string/jumbo v3, "DM_CONFIRM_LOOKUP"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 81
    :cond_e
    const-string/jumbo v3, "reset"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 82
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->isMusicDomain(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 83
    invoke-virtual {p0, v0}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->noSupportInMusic(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 86
    :cond_f
    const-string/jumbo v3, "DM_CONFIRM_RESET"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 87
    :cond_10
    const-string/jumbo v3, "route"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 88
    const-string/jumbo v3, "DM_CONFIRM_ROUTE"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 89
    :cond_11
    const-string/jumbo v3, "nop"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 91
    const-string/jumbo v3, "Domain"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "domain":Ljava/lang/String;
    const-string/jumbo v3, "sms"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 93
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->noSupportInMsg()V

    goto/16 :goto_0

    .line 96
    :cond_12
    const-string/jumbo v3, "DM_CONFIRM_ROUTE_NOP"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 98
    .end local v1    # "domain":Ljava/lang/String;
    :cond_13
    const-string/jumbo v3, "previous"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "prev"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 99
    :cond_14
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "DM_MAIN"

    if-eq v3, v4, :cond_15

    .line 100
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "DM_MUSIC_PLAYER"

    .line 99
    if-ne v3, v4, :cond_0

    .line 101
    :cond_15
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 103
    :cond_16
    const-string/jumbo v3, "next"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 104
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "flowID":Ljava/lang/String;
    const-string/jumbo v3, "DM_MAIN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 106
    const-string/jumbo v3, "DM_MUSIC_PLAYER"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 107
    :cond_17
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 109
    :cond_18
    const-string/jumbo v3, "DM_NEXT"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 111
    .end local v2    # "flowID":Ljava/lang/String;
    :cond_19
    const-string/jumbo v3, "pause"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 112
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->isNavigationDomain(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 114
    const-string/jumbo v3, "DM_STOP_NAVIGATION"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 116
    :cond_1a
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 118
    :cond_1b
    const-string/jumbo v3, "start"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 119
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :cond_1c
    const-string/jumbo v3, "restart"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 121
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    :cond_1d
    const-string/jumbo v3, "resume"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 123
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 124
    :cond_1e
    const-string/jumbo v3, "volumeup"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 125
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 126
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    .line 127
    const v4, 0xea60

    .line 126
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    goto/16 :goto_0

    .line 129
    :cond_1f
    const-string/jumbo v3, "volumedown"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 130
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 131
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    .line 132
    const v4, 0xea61

    .line 131
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    goto/16 :goto_0

    .line 134
    :cond_20
    const-string/jumbo v3, "mute"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 135
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 136
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    .line 137
    const v4, 0xea62

    .line 136
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    goto/16 :goto_0

    .line 139
    :cond_21
    const-string/jumbo v3, "unmute"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 140
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 141
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    .line 142
    const v4, 0xea63

    .line 141
    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setVolumeCmd(I)V

    goto/16 :goto_0

    .line 145
    :cond_22
    sget-object v3, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "unknown command :  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected isMusicDomain(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 151
    const-string/jumbo v0, "music"

    const-string/jumbo v1, "Domain"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isNavigationDomain(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 155
    const-string/jumbo v0, "navigation"

    const-string/jumbo v1, "Domain"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/nuance/sample/music/PlayMusicType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 234
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->playByVoice:Z

    .line 239
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    invoke-virtual {p0, v0, v1}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 240
    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name(Ljava/lang/String;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->queue()V

    .line 241
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "music-play-list"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method noSupportInMsg()V
    .locals 3

    .prologue
    .line 196
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 197
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 198
    const v2, 0x7f0a0127

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 200
    new-instance v2, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler$2;

    invoke-direct {v2, p0}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler$2;-><init>(Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;)V

    .line 199
    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 231
    return-void
.end method

.method noSupportInMusic(Ljava/lang/String;)V
    .locals 5
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 160
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 161
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 162
    const v2, 0x7f0a01e7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 163
    if-eqz p1, :cond_0

    .end local p1    # "command":Ljava/lang/String;
    :goto_0
    aput-object p1, v3, v4

    .line 162
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 165
    new-instance v2, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler$1;

    invoke-direct {v2, p0}, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler$1;-><init>(Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;)V

    .line 164
    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 193
    return-void

    .line 163
    .end local v0    # "systemTurnText":Ljava/lang/String;
    .restart local p1    # "command":Ljava/lang/String;
    :cond_0
    const-string/jumbo p1, ""

    goto :goto_0
.end method
