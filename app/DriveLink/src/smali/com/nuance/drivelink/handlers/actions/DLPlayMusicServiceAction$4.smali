.class Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$4;
.super Ljava/lang/Object;
.source "DLPlayMusicServiceAction.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/music/MusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->execute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$4;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnBTBtnCallingState()V
    .locals 0

    .prologue
    .line 400
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 401
    return-void
.end method

.method public OnBTBtnClicked()V
    .locals 0

    .prologue
    .line 410
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 411
    return-void
.end method

.method public OnBTBtnPlay()V
    .locals 0

    .prologue
    .line 405
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 406
    return-void
.end method

.method public OnBTBtnSeek()V
    .locals 0

    .prologue
    .line 415
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 416
    return-void
.end method

.method public OnError(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 390
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 391
    return-void
.end method

.method public OnFinished()V
    .locals 0

    .prologue
    .line 385
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 386
    return-void
.end method

.method public OnSeekComplete()V
    .locals 0

    .prologue
    .line 395
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 396
    return-void
.end method

.method public OnSetVolumeControl(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 426
    return-void
.end method

.method public OnShuffle(Z)V
    .locals 0
    .param p1, "bShuffle"    # Z

    .prologue
    .line 420
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 421
    return-void
.end method

.method public OnVolumeControlCommand(I)V
    .locals 0
    .param p1, "command"    # I

    .prologue
    .line 432
    return-void
.end method

.method public onMusicChanged(Z)V
    .locals 0
    .param p1, "isPlayed"    # Z

    .prologue
    .line 381
    return-void
.end method
