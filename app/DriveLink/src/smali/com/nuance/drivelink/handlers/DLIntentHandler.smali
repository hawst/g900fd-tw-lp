.class public Lcom/nuance/drivelink/handlers/DLIntentHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "DLIntentHandler.java"


# static fields
.field public static final ERROR_RUN_CURRENT_VLINGO_APP:Ljava/lang/String; = "run current vlingo app"


# instance fields
.field private launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

.field private spokenForm:Ljava/lang/String;

.field private systemTurnDisplayText:Ljava/lang/String;

.field private systemTurnTtsText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private tryLaunchApp(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 155
    new-instance v0, Lcom/vlingo/core/internal/util/OpenAppUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;-><init>()V

    .line 156
    .local v0, "oau":Lcom/vlingo/core/internal/util/OpenAppUtil;
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/util/OpenAppUtil;->buildMatchingAppList(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 159
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    invoke-virtual {p0, v1}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    move v1, v2

    .line 163
    :cond_0
    return v1
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionFail(Ljava/lang/String;)V

    .line 182
    const-string/jumbo v1, "run current vlingo app"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 184
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0292

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "spokenText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 187
    .end local v0    # "spokenText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 18
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 33
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 35
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 38
    const-string/jumbo v13, "ExecName"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 39
    .local v4, "execName":Ljava/lang/String;
    const-string/jumbo v13, "ExecPackage"

    .line 40
    const/4 v14, 0x0

    .line 39
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 42
    .local v5, "execPackage":Ljava/lang/String;
    const-string/jumbo v13, "action.prompt"

    const/4 v14, 0x0

    .line 41
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    .line 43
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    if-nez v13, :cond_0

    .line 44
    const-string/jumbo v13, "TTS"

    .line 45
    const/4 v14, 0x0

    .line 44
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    .line 49
    :cond_0
    const-string/jumbo v13, "action.prompt.spoken"

    const/4 v14, 0x0

    .line 48
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnTtsText:Ljava/lang/String;

    .line 50
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnTtsText:Ljava/lang/String;

    if-nez v13, :cond_1

    .line 51
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnTtsText:Ljava/lang/String;

    .line 54
    :cond_1
    const-string/jumbo v13, "SpokenForm"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    .line 55
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    if-nez v13, :cond_2

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v13

    invoke-interface {v13}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v13

    .line 57
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 58
    const v14, 0x7f0a00cc

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 56
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    .line 60
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnTtsText:Ljava/lang/String;

    if-nez v13, :cond_3

    .line 61
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnTtsText:Ljava/lang/String;

    .line 63
    :cond_3
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 64
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 65
    const-string/jumbo v13, "AppName"

    .line 66
    const/4 v14, 0x0

    .line 65
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "appName":Ljava/lang/String;
    const-string/jumbo v13, "Extras"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 69
    .local v6, "extra":Ljava/lang/String;
    const-string/jumbo v13, "ExecAction"

    const/4 v14, 0x0

    .line 68
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 74
    .local v7, "intentAction":Ljava/lang/String;
    const/high16 v12, -0x80000000

    .line 75
    .local v12, "versionLimit":I
    invoke-static {v5, v12}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 76
    sget-object v13, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 77
    const-class v14, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    .line 76
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    .line 78
    invoke-virtual {v13, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->enclosingPackage(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v13

    invoke-virtual {v13, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activity(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v13

    .line 79
    invoke-virtual {v13, v6}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v13

    invoke-virtual {v13, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v13

    invoke-virtual {v13, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->app(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v13

    .line 76
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 81
    :cond_4
    const/4 v4, 0x0

    .line 82
    const/4 v5, 0x0

    .line 84
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    if-nez v13, :cond_a

    .line 87
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 88
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    .line 93
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->tryLaunchApp(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 95
    const/4 v13, 0x0

    .line 150
    .end local v1    # "appName":Ljava/lang/String;
    .end local v7    # "intentAction":Ljava/lang/String;
    .end local v12    # "versionLimit":I
    :goto_0
    return v13

    .line 98
    .restart local v1    # "appName":Ljava/lang/String;
    .restart local v7    # "intentAction":Ljava/lang/String;
    .restart local v12    # "versionLimit":I
    :cond_6
    const-string/jumbo v13, "Not found"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->actionFail(Ljava/lang/String;)V

    .line 99
    const/4 v13, 0x0

    goto :goto_0

    .line 103
    .end local v1    # "appName":Ljava/lang/String;
    .end local v6    # "extra":Ljava/lang/String;
    .end local v7    # "intentAction":Ljava/lang/String;
    .end local v12    # "versionLimit":I
    :cond_7
    const-string/jumbo v13, "IntentName"

    .line 104
    const/4 v14, 0x1

    .line 103
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 105
    .local v10, "name":Ljava/lang/String;
    const-string/jumbo v13, "IntentArgument"

    .line 106
    const/4 v14, 0x0

    .line 105
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "arg":Ljava/lang/String;
    const-string/jumbo v13, "Extras"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 108
    .restart local v6    # "extra":Ljava/lang/String;
    const-string/jumbo v13, "ClassName"

    .line 109
    const/4 v14, 0x0

    .line 108
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "className":Ljava/lang/String;
    const-string/jumbo v13, "Type"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    .line 112
    .local v11, "type":Ljava/lang/String;
    const-string/jumbo v13, "broadcast"

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 111
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v8

    .line 113
    .local v8, "isBroadcast":Z
    const-string/jumbo v13, "service"

    .line 114
    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 113
    move-object/from16 v0, p1

    invoke-static {v0, v13, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v9

    .line 120
    .local v9, "isService":Z
    sget-object v13, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 121
    const-class v14, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    .line 120
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    .line 121
    invoke-virtual {v13, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v13

    invoke-virtual {v13, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v13

    .line 122
    invoke-virtual {v13, v6}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v13

    invoke-virtual {v13, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v13

    invoke-virtual {v13, v8}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v13

    .line 123
    invoke-virtual {v13, v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->service(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v13

    invoke-virtual {v13, v11}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->type(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v13

    .line 120
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 125
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    check-cast v13, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isAvailable()Z

    move-result v13

    if-nez v13, :cond_a

    .line 126
    const-string/jumbo v13, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-virtual {v10, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_a

    .line 127
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 128
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    .line 131
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->spokenForm:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->tryLaunchApp(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 133
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 137
    :cond_9
    const-string/jumbo v13, "Not found"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->actionFail(Ljava/lang/String;)V

    .line 138
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 141
    .end local v2    # "arg":Ljava/lang/String;
    .end local v3    # "className":Ljava/lang/String;
    .end local v8    # "isBroadcast":Z
    .end local v9    # "isService":Z
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "type":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    if-eqz v13, :cond_c

    .line 142
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_b

    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    .line 144
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnTtsText:Ljava/lang/String;

    .line 143
    invoke-interface {v13, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    .line 147
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v13

    invoke-interface {v13}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 150
    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method public launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V
    .locals 4
    .param p1, "appInfo"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 167
    const-string/jumbo v0, "launch"

    .line 168
    .local v0, "landingPageId":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnDisplayText:Ljava/lang/String;

    .line 173
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLIntentHandler;->systemTurnTtsText:Ljava/lang/String;

    .line 172
    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 176
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    invoke-virtual {p0, v1, v2}, Lcom/nuance/drivelink/handlers/DLIntentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    move-result-object v1

    .line 177
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->queue()V

    .line 178
    return-void
.end method
