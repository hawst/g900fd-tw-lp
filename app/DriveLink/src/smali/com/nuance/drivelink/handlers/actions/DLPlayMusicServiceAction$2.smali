.class Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$2;
.super Ljava/lang/Object;
.source "DLPlayMusicServiceAction.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicService(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

.field private final synthetic val$_musicList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$2;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    iput-object p2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$2;->val$_musicList:Ljava/util/ArrayList;

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 265
    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$2()Ljava/lang/String;

    move-result-object v0

    .line 266
    const-string/jumbo v1, "MusicService setMusicPlayerVolume by Voice playMusicService"

    .line 265
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v2, v2, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 268
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 269
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$2;->val$_musicList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayList(Ljava/util/ArrayList;ZZ)V

    .line 271
    return-void
.end method
