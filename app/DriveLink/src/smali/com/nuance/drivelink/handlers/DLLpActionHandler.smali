.class public Lcom/nuance/drivelink/handlers/DLLpActionHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;
.source "DLLpActionHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v1, 0x1

    .line 16
    if-eqz p1, :cond_5

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "LPAction"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 18
    const-string/jumbo v2, "Action"

    .line 19
    const/4 v3, 0x0

    .line 18
    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "actionValue":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 22
    const-string/jumbo v2, "safereader:read"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 23
    const-string/jumbo v2, "message:read"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 24
    :cond_0
    const-string/jumbo v2, "DM_READ"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 39
    .end local v0    # "actionValue":Ljava/lang/String;
    :goto_0
    return v1

    .line 26
    .restart local v0    # "actionValue":Ljava/lang/String;
    :cond_1
    const-string/jumbo v2, "safereader:call"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 27
    const-string/jumbo v2, "DM_CALL"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 29
    :cond_2
    const-string/jumbo v2, "safereader:reply"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 30
    const-string/jumbo v2, "DM_REPLY"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 32
    :cond_3
    const-string/jumbo v2, "safereader:next"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 33
    const-string/jumbo v2, "message:next"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 34
    :cond_4
    const-string/jumbo v2, "DM_NEXT"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    .end local v0    # "actionValue":Ljava/lang/String;
    :cond_5
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    goto :goto_0
.end method
