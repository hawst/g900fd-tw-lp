.class Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;
.super Ljava/lang/Object;
.source "DLPlayMusicServiceAction.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    return-object v0
.end method


# virtual methods
.method public onResponseRequestAlbumList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "albumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;>;"
    return-void
.end method

.method public onResponseRequestArtistList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "artistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;>;"
    return-void
.end method

.method public onResponseRequestFolderList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "folderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;>;"
    return-void
.end method

.method public onResponseRequestMusicAlbumArt(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p1, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;

    .prologue
    .line 235
    return-void
.end method

.method public onResponseRequestMusicList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByAlbum(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;)V
    .locals 0
    .param p2, "album"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLAlbum;",
            ")V"
        }
    .end annotation

    .prologue
    .line 205
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByArtist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;)V
    .locals 0
    .param p2, "artist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLArtist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByFolder(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;)V
    .locals 0
    .param p2, "folder"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLFolder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByMusic(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;)V
    .locals 0
    .param p2, "music"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ")V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListByPlaylist(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;)V
    .locals 0
    .param p2, "playlist"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ")V"
        }
    .end annotation

    .prologue
    .line 177
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestMusicListFromSearchResult(Ljava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;I)V
    .locals 0
    .param p2, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    return-void
.end method

.method public onResponseRequestPlaylistList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "playlistList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPlaylist;>;"
    return-void
.end method

.method public onResponseRequestSearchAllMusic(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 96
    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$2()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResponseRequestMusicListFromSearchResult - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;
    invoke-static {v2}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$3(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 97
    const-string/jumbo v2, "count :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    invoke-static {v0, p1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$6(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/util/ArrayList;)V

    .line 100
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$7(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 101
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$7(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$8(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$7(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 103
    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 104
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 105
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 107
    const v3, 0x7f0a0151

    new-array v4, v4, [Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;
    invoke-static {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$9(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;
    invoke-static {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$9(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v0

    .line 109
    :goto_0
    aput-object v0, v4, v5

    .line 106
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-static {v1, v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$10(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V

    .line 120
    :goto_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSearchedMusicCountMsg:Ljava/lang/String;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$11(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v1

    .line 121
    new-instance v2, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1$1;

    invoke-direct {v2, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1$1;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;)V

    .line 120
    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 157
    :goto_2
    return-void

    .line 109
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 112
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 113
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 115
    const v3, 0x7f0a0150

    new-array v4, v4, [Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;
    invoke-static {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$9(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;
    invoke-static {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$9(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v0

    .line 117
    :goto_3
    aput-object v0, v4, v5

    .line 114
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-static {v1, v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$10(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V

    goto :goto_1

    .line 117
    :cond_2
    const-string/jumbo v0, ""

    goto :goto_3

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$3(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->noPlayMusicList(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$5(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onResponseRequestSearchMusic(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;)V
    .locals 3
    .param p1, "musicSearchResult"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;

    .prologue
    .line 80
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getHomeMode()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "HOME_MODE_MULTIWINDOW"

    if-ne v0, v1, :cond_0

    .line 81
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCallMultiwindow(Z)V

    .line 83
    :cond_0
    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$2()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onResponseRequestSearchMusic Result - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;
    invoke-static {v2}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$3(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 84
    const-string/jumbo v2, "count :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusicSearchResult;->getTotalMusicCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    iget-object v0, v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # invokes: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$4(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;
    invoke-static {v2}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$3(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchAllMusic(Landroid/content/Context;Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    # getter for: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;
    invoke-static {v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$3(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->noPlayMusicList(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->access$5(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V

    goto :goto_0
.end method
