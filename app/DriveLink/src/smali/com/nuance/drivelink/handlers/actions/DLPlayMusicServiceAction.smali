.class public Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;
.super Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;
.source "DLPlayMusicServiceAction.java"


# static fields
.field public static final MSG_PLAY_ALL_SHUFFLE_MUSIC:I = 0x0

.field private static final MUSIC_SEARCH_TRY_LIMIT_COUNT:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static mMusicServiceListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

.field public static mVoiceMusicNoSearchInHomeActivity:Z

.field public static mVoiceMusicSearchTryCount:I


# instance fields
.field private mContext:Landroid/content/Context;

.field mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

.field private mMusicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicListSize:Ljava/lang/String;

.field mMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

.field private mNoMatchMsg:Ljava/lang/String;

.field private mSearchedMusicCountMsg:Ljava/lang/String;

.field private mSetMusicListHandler:Landroid/os/Handler;

.field private name:Ljava/lang/String;

.field private playMusicType:Lcom/nuance/sample/music/PlayMusicType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    const-class v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 39
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 38
    sput-object v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    .line 46
    sput v1, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicSearchTryCount:I

    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicServiceListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 49
    sput-boolean v1, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicNoSearchInHomeActivity:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;

    .line 52
    iput-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mNoMatchMsg:Ljava/lang/String;

    .line 53
    iput-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSearchedMusicCountMsg:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSetMusicListHandler:Landroid/os/Handler;

    .line 75
    new-instance v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;

    invoke-direct {v0, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$1;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    iput-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkMusicListener;

    .line 37
    return-void
.end method

.method static synthetic access$10(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSearchedMusicCountMsg:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$11(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSearchedMusicCountMsg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0, p1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->noPlayMusicList(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$6(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$7(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    return-object v0
.end method

.method private handleMusicSearchResult(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 617
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;

    .line 619
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 620
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    .line 621
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 623
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 624
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 625
    const v2, 0x7f0a0151

    new-array v3, v3, [Ljava/lang/Object;

    .line 626
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    :goto_0
    aput-object v0, v3, v4

    .line 625
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 622
    iput-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSearchedMusicCountMsg:Ljava/lang/String;

    .line 635
    :goto_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSearchedMusicCountMsg:Ljava/lang/String;

    .line 636
    new-instance v2, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$10;

    invoke-direct {v2, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$10;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    .line 635
    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 670
    :goto_2
    return-void

    .line 626
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    .line 629
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 630
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 631
    const v2, 0x7f0a0150

    new-array v3, v3, [Ljava/lang/Object;

    .line 632
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicListSize:Ljava/lang/String;

    :goto_3
    aput-object v0, v3, v4

    .line 631
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 628
    iput-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSearchedMusicCountMsg:Ljava/lang/String;

    goto :goto_1

    .line 632
    :cond_2
    const-string/jumbo v0, ""

    goto :goto_3

    .line 668
    :cond_3
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->noPlayMusicList(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static increaseVoiceMusicSearchTryCount()V
    .locals 1

    .prologue
    .line 248
    sget v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicSearchTryCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicSearchTryCount:I

    .line 249
    return-void
.end method

.method public static isOverMusicSearchTryLimit()Z
    .locals 2

    .prologue
    .line 239
    sget v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicSearchTryCount:I

    if-eqz v0, :cond_0

    .line 240
    sget v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicSearchTryCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 244
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static ismVoiceMusicNoSearchInHomeActivity()Z
    .locals 1

    .prologue
    .line 299
    sget-boolean v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicNoSearchInHomeActivity:Z

    return v0
.end method

.method private noPlayMusicList(Ljava/lang/String;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 308
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 310
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->increaseVoiceMusicSearchTryCount()V

    .line 311
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->isOverMusicSearchTryLimit()Z

    move-result v2

    if-nez v2, :cond_2

    .line 312
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 313
    const v3, 0x7f0a0152

    new-array v4, v5, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    .end local p1    # "name":Ljava/lang/String;
    :goto_0
    aput-object p1, v4, v6

    .line 312
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mNoMatchMsg:Ljava/lang/String;

    .line 314
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mContext:Landroid/content/Context;

    .line 315
    iget-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "promptForMusic":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mNoMatchMsg:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 319
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "DM_MUSIC_PLAYER"

    if-ne v2, v3, :cond_1

    .line 320
    const-string/jumbo v2, "DM_MUSIC_PLAYER"

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 369
    .end local v1    # "promptForMusic":Ljava/lang/String;
    :goto_1
    return-void

    .line 313
    .restart local p1    # "name":Ljava/lang/String;
    :cond_0
    const-string/jumbo p1, ""

    goto :goto_0

    .line 323
    .end local p1    # "name":Ljava/lang/String;
    .restart local v1    # "promptForMusic":Ljava/lang/String;
    :cond_1
    sput-boolean v5, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicNoSearchInHomeActivity:Z

    .line 324
    const-string/jumbo v2, "DM_MUSIC_PLAYER"

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_1

    .line 329
    .end local v1    # "promptForMusic":Ljava/lang/String;
    .restart local p1    # "name":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 330
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 331
    const v3, 0x7f0a0153

    new-array v4, v5, [Ljava/lang/Object;

    .line 332
    if-eqz p1, :cond_3

    .end local p1    # "name":Ljava/lang/String;
    :goto_2
    aput-object p1, v4, v6

    .line 331
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 328
    iput-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mNoMatchMsg:Ljava/lang/String;

    .line 333
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mNoMatchMsg:Ljava/lang/String;

    .line 334
    new-instance v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$3;

    invoke-direct {v4, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$3;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    .line 333
    invoke-interface {v2, v3, v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    goto :goto_1

    .line 332
    .restart local p1    # "name":Ljava/lang/String;
    :cond_3
    const-string/jumbo p1, ""

    goto :goto_2
.end method

.method public static resetVoiceMusicSearchTryCount()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    sput v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicSearchTryCount:I

    .line 253
    return-void
.end method

.method private setShufflePlayList()V
    .locals 2

    .prologue
    .line 372
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicShuffleList(Z)V

    .line 373
    return-void
.end method

.method public static setmVoiceMusicNoSearchInHomeActivity(Z)V
    .locals 0
    .param p0, "mVoiceMusicNoSearchInHomeActivity"    # Z

    .prologue
    .line 304
    sput-boolean p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mVoiceMusicNoSearchInHomeActivity:Z

    .line 305
    return-void
.end method


# virtual methods
.method protected declared-synchronized execute()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 377
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicServiceListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    if-nez v3, :cond_0

    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 378
    new-instance v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$4;

    invoke-direct {v3, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$4;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    sput-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicServiceListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    .line 434
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    sget-object v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mMusicServiceListener:Lcom/sec/android/automotive/drivelink/music/MusicListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->registerListener(Lcom/sec/android/automotive/drivelink/music/MusicListener;)V

    .line 437
    :cond_0
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 439
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 440
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    const-string/jumbo v4, " "

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    .line 441
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "execute : name = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    if-nez v3, :cond_1

    .line 445
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v3

    .line 444
    iput-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 448
    :cond_1
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 450
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mDLServiceInterface:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    .line 451
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->getAllMusicSearchList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 452
    .local v0, "musicSearchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    invoke-direct {p0, v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->handleMusicSearchResult(Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    .end local v0    # "musicSearchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 456
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v3, v4, :cond_7

    .line 457
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "execute : PLAY "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 460
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 468
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->ismUserPausePriority()Z

    move-result v3

    if-nez v3, :cond_6

    .line 470
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 471
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    .line 472
    const-string/jumbo v4, "MusicService setMusicPlayerVolume by Voice execute PLAY ismUserPausePriority false"

    .line 471
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 474
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 475
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 476
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$5;

    invoke-direct {v4, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$5;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    .line 484
    const-wide/16 v5, 0x258

    .line 476
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 507
    :cond_4
    :goto_1
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 609
    :cond_5
    :goto_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "DM_MAIN"

    if-eq v3, v4, :cond_2

    .line 610
    const-string/jumbo v3, "DM_MUSIC_PLAYER"

    .line 611
    const/4 v4, 0x0

    .line 610
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 612
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 377
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 490
    :cond_6
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 491
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 492
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    .line 493
    const-string/jumbo v4, "MusicService setMusicPlayerVolume by Voice execute PLAY ismUserPausePriority true"

    .line 492
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 495
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 496
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 497
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$6;

    invoke-direct {v4, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$6;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    .line 505
    const-wide/16 v5, 0x258

    .line 497
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 508
    :cond_7
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->PLAYALL:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v3, v4, :cond_8

    .line 509
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "execute : PLAYALL "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 512
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 513
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 514
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 515
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$7;

    invoke-direct {v4, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$7;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    .line 523
    const-wide/16 v5, 0x258

    .line 515
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 526
    :cond_8
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v3, v4, :cond_9

    .line 527
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "execute : PAUSE "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 529
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 530
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "MUSICService Voice User Setting Pause"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setUserSettingPauseOrStop()V

    goto/16 :goto_2

    .line 533
    :cond_9
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v3, v4, :cond_c

    .line 534
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "execute : NEXT "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 536
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMediaPlayerListCount()I

    move-result v3

    if-ne v3, v5, :cond_a

    .line 537
    invoke-direct {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->setShufflePlayList()V

    .line 539
    :cond_a
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMediaPlayerListCount()I

    move-result v3

    if-ne v3, v5, :cond_b

    .line 541
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v3

    .line 542
    invoke-interface {v3}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 543
    const v4, 0x7f0a0117

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 544
    .local v2, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 551
    .end local v2    # "systemTurnText":Ljava/lang/String;
    :cond_b
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 552
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 553
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 554
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$8;

    invoke-direct {v4, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$8;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    .line 562
    const-wide/16 v5, 0x3e8

    .line 554
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 565
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    goto/16 :goto_2

    .line 567
    :cond_c
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v3, v4, :cond_f

    .line 568
    sget-object v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "execute : PREVIOUS "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 570
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMediaPlayerListCount()I

    move-result v3

    if-ne v3, v5, :cond_d

    .line 571
    invoke-direct {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->setShufflePlayList()V

    .line 573
    :cond_d
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->getMediaPlayerListCount()I

    move-result v3

    if-ne v3, v5, :cond_e

    .line 575
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v3

    .line 576
    invoke-interface {v3}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 577
    const v4, 0x7f0a0117

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 578
    .restart local v2    # "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 585
    .end local v2    # "systemTurnText":Ljava/lang/String;
    :cond_e
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 586
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 587
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 588
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$9;

    invoke-direct {v4, p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$9;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V

    .line 596
    const-wide/16 v5, 0x3e8

    .line 588
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 599
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    goto/16 :goto_2

    .line 603
    :cond_f
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    .line 605
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 604
    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 606
    .local v1, "noMatchMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2
.end method

.method public name(Ljava/lang/String;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public bridge synthetic name(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name(Ljava/lang/String;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    return-object v0
.end method

.method protected playMusicService(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    sget-object v2, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "playMusic"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 258
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 259
    move-object v0, p1

    .line 261
    .local v0, "_musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    iget-object v2, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->mSetMusicListHandler:Landroid/os/Handler;

    new-instance v3, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$2;

    invoke-direct {v3, p0, v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$2;-><init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 276
    .end local v0    # "_musicList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLMusic;>;"
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "DM_MAIN"

    if-ne v2, v3, :cond_1

    .line 277
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getHomeMode()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "HOME_MODE_MULTIWINDOW"

    if-eq v2, v3, :cond_1

    .line 278
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 279
    .local v1, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v2, "DM_MUSIC_PLAYER"

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 281
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 282
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 281
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 283
    const-string/jumbo v2, "CM04"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 286
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_1
    sget-object v2, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Stop Bluetooth sco - on playMusicService"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 289
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 296
    return-void
.end method

.method public playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;
    .locals 0
    .param p1, "playMusicType"    # Lcom/nuance/sample/music/PlayMusicType;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    .line 67
    return-object p0
.end method

.method public bridge synthetic playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    return-object v0
.end method
