.class public Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "DLRecognitionFailedHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static failedString:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mRepeated:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;

    .line 17
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 16
    sput-object v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->failedString:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private static getFailedTTS()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    sget-object v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getFailedTTS repeat :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    sget v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    if-nez v0, :cond_0

    .line 68
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 69
    const v1, 0x7f0a0126

    .line 68
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 70
    :cond_0
    sget v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 71
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 72
    const v1, 0x7f0a0127

    .line 71
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    :cond_1
    sget v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 74
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 75
    const v1, 0x7f0a0128

    .line 74
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_2
    sget-object v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getFailedTTS(invalid repeat count) repeat :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    sget v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static resetRepeated()V
    .locals 3

    .prologue
    .line 58
    sget-object v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "resetRepeated - flowID :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const/4 v0, 0x0

    sput v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    .line 62
    return-void
.end method

.method public static showFiledTTS()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 31
    invoke-static {}, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->getFailedTTS()Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "failiedTTS":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 33
    const v3, 0x7f0a0129

    .line 32
    invoke-virtual {v2, v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "failedText":Ljava/lang/String;
    sget-object v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "showFiledTTS String :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 38
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 39
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 41
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->displayError(Ljava/lang/CharSequence;)V

    .line 44
    sget v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    if-ge v2, v6, :cond_0

    .line 45
    sget-object v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Start  listening "

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-static {}, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->startListening()V

    .line 49
    :cond_0
    sget v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    .line 51
    sget v2, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    if-le v2, v6, :cond_1

    .line 52
    sput v5, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->mRepeated:I

    .line 55
    :cond_1
    return-void
.end method

.method private static startListening()V
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startListening "

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 88
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 24
    sget-object v0, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "executeAction action:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    invoke-static {}, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;->showFiledTTS()V

    .line 27
    const/4 v0, 0x0

    return v0
.end method
