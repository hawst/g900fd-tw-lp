.class public Lcom/nuance/drivelink/handlers/DLDialogCancelHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DialogCancelHandler;
.source "DLDialogCancelHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/nuance/drivelink/handlers/DLDialogCancelHandler;

    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 10
    sput-object v0, Lcom/nuance/drivelink/handlers/DLDialogCancelHandler;->TAG:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DialogCancelHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 2
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 18
    sget-object v0, Lcom/nuance/drivelink/handlers/DLDialogCancelHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "DialogCancelHandler"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DialogCancelHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method
