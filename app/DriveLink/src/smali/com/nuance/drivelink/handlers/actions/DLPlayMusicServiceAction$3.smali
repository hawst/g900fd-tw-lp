.class Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$3;
.super Ljava/lang/Object;
.source "DLPlayMusicServiceAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->noPlayMusicList(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction$3;->this$0:Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 340
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v1, 0x1

    .line 345
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 346
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v1, v1, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 347
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 348
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    .line 349
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/music/MusicService;->startPlayerWithAllMusicShuffled()V

    .line 350
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicSearchResultFlag(Z)V

    .line 352
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 354
    :cond_0
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "arg1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 360
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 365
    return-void
.end method
