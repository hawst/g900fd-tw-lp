.class public Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "DLShowPlayMusicWidgetHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field protected shuffleOnOff:Ljava/lang/String;

.field protected type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;

    .line 44
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 43
    sput-object v0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->TAG:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 62
    iput-object v0, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->shuffleOnOff:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private getNoMatchAnyMusic()Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ANYMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    return-object v0
.end method

.method private isAnyMusic()Z
    .locals 2

    .prologue
    .line 159
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getMusicCount()I

    move-result v0

    .line 161
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 162
    const/4 v1, 0x1

    .line 165
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public actionSuccess()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    iget-object v0, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    .line 150
    const-string/jumbo v1, "Music:Pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v0, v1, v2, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 154
    :cond_0
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 155
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 70
    sget-object v3, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "execAction : action = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 76
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    const-string/jumbo v3, "Type"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    .line 81
    const-string/jumbo v3, "shuffle"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 80
    iput-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->shuffleOnOff:Ljava/lang/String;

    .line 84
    :cond_0
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->getQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "name":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v3

    .line 86
    invoke-interface {v3}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 87
    .local v0, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    invoke-static {v0, v3, v1}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requested(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->shuffleOnOff:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 89
    const-string/jumbo v3, "no"

    iput-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->shuffleOnOff:Ljava/lang/String;

    .line 92
    :cond_1
    invoke-direct {p0}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->isAnyMusic()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 93
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v4, "Music:Play"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    .line 94
    const-string/jumbo v4, "Music:Generic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 95
    :cond_2
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 96
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->shuffleOnOff:Ljava/lang/String;

    const-string/jumbo v4, "yes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 97
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 98
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setShuffle(Z)V

    .line 144
    :cond_3
    :goto_0
    return v6

    .line 101
    :cond_4
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_5
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v4, "Music:PlayAll"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 105
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAYALL:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_6
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v4, "Music:Pause"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 108
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_7
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v4, "Music:Prev"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 111
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_8
    iget-object v3, p0, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v4, "Music:Next"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 114
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_9
    const-string/jumbo v3, "Which"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 120
    .local v2, "ordinal":Ljava/lang/String;
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v7, :cond_b

    .line 121
    :cond_a
    const-string/jumbo v1, " "

    .line 124
    :cond_b
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 125
    invoke-static {p2, v2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_d

    .line 127
    :cond_c
    sget-object v3, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "execAction: name = ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {p0, v3, v1}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 133
    :cond_d
    sget-object v3, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "execAction: ordinals exist "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 142
    .end local v2    # "ordinal":Ljava/lang/String;
    :cond_e
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v3

    invoke-direct {p0}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->getNoMatchAnyMusic()Lcom/vlingo/core/internal/ResourceIdProvider$string;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto/16 :goto_0
.end method

.method protected getQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/lang/String;
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v2, 0x0

    .line 189
    const-string/jumbo v1, "Query"

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "query":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 192
    const-string/jumbo v1, "Artist"

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 195
    :cond_0
    if-nez v0, :cond_1

    .line 196
    const-string/jumbo v1, "Title"

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 199
    :cond_1
    if-nez v0, :cond_2

    .line 200
    const-string/jumbo v1, "Album"

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 203
    :cond_2
    if-nez v0, :cond_3

    .line 204
    const-string/jumbo v1, "Playlist"

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 207
    :cond_3
    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 51
    return-void
.end method

.method launchMusicPlayer(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 173
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    invoke-virtual {p0, v0, v1}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 174
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v0, v1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name(Ljava/lang/String;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->queue()V

    .line 175
    return-void
.end method

.method launchMusicPlayerWithType(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/nuance/sample/music/PlayMusicType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 178
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/automotive/drivelink/music/MusicService;->playByVoice:Z

    .line 183
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    invoke-virtual {p0, v0, v1}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 184
    invoke-virtual {v0, p1}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->name(Ljava/lang/String;)Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->queue()V

    .line 185
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "music-play-list"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 60
    return-void
.end method
