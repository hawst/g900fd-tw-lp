.class Lcom/nuance/drivelink/DLAppUiUpdater$2;
.super Ljava/lang/Object;
.source "DLAppUiUpdater.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/drivelink/DLAppUiUpdater;->init()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/DLAppUiUpdater;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/DLAppUiUpdater;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater$2;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1, "args"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 212
    if-nez p2, :cond_0

    .line 226
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater$2;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    # getter for: Lcom/nuance/drivelink/DLAppUiUpdater;->SAMPLE_APP_SERVER_KEYS:Ljava/util/List;
    invoke-static {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->access$3(Lcom/nuance/drivelink/DLAppUiUpdater;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218
    invoke-static {}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getInstance()Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    move-result-object v1

    .line 217
    invoke-static {v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->updateServerInfo(Lcom/vlingo/core/internal/util/CoreServerInfo;)V

    .line 221
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 222
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 223
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 224
    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 225
    const-string/jumbo v3, "SETTINGS/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 223
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 225
    const/4 v3, 0x0

    .line 222
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
