.class Lcom/nuance/drivelink/DLAppUiUpdater$1;
.super Landroid/os/Handler;
.source "DLAppUiUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/drivelink/DLAppUiUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/DLAppUiUpdater;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/DLAppUiUpdater;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater$1;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    .line 1167
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1171
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1173
    # getter for: Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mPhraseSpotterHandler : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1189
    :goto_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater$1;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    invoke-virtual {v0, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->setCancelByTouch(Z)V

    .line 1191
    return-void

    .line 1177
    :pswitch_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater$1;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    # invokes: Lcom/nuance/drivelink/DLAppUiUpdater;->ChangedPhraseSpotterState(Z)V
    invoke-static {v0, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->access$1(Lcom/nuance/drivelink/DLAppUiUpdater;Z)V

    goto :goto_0

    .line 1181
    :pswitch_1
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater$1;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    # invokes: Lcom/nuance/drivelink/DLAppUiUpdater;->ChangedPhraseSpotterState(Z)V
    invoke-static {v0, v4}, Lcom/nuance/drivelink/DLAppUiUpdater;->access$1(Lcom/nuance/drivelink/DLAppUiUpdater;Z)V

    .line 1182
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater$1;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    # invokes: Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V
    invoke-static {v0, v4}, Lcom/nuance/drivelink/DLAppUiUpdater;->access$2(Lcom/nuance/drivelink/DLAppUiUpdater;Z)V

    goto :goto_0

    .line 1175
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
