.class public Lcom/nuance/drivelink/utils/DLContactVacUtils;
.super Ljava/lang/Object;
.source "DLContactVacUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 15
    .param p0, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .prologue
    const/4 v6, 0x0

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 39
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 40
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 39
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneticName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v3

    .line 41
    const/4 v5, 0x0

    .line 38
    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 43
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneNumberList()Ljava/util/ArrayList;

    move-result-object v14

    .line 45
    .local v14, "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 53
    return-object v0

    .line 45
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 46
    .local v13, "number":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->isMainPhoneNumber()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v12, 0x1

    .line 47
    .local v12, "isDefault":I
    :goto_1
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 48
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v10

    .line 49
    invoke-virtual {v13}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v11

    move-object v8, v0

    .line 47
    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 50
    .local v7, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_0

    .end local v7    # "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v12    # "isDefault":I
    :cond_1
    move v12, v6

    .line 46
    goto :goto_1
.end method

.method public static getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;Ljava/util/ArrayList;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 10
    .param p0, "dlContact"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;",
            ">;)",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 60
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 59
    invoke-virtual {p0, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getPhoneticName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;->getContactId()J

    move-result-wide v3

    .line 61
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 58
    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 63
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object v8, p1

    .line 65
    .local v8, "numberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    return-object v0

    .line 65
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;

    .line 66
    .local v7, "number":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->isMainPhoneNumber()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v6, 0x1

    .line 67
    .local v6, "isDefault":I
    :goto_1
    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 68
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 69
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;->getPhoneType()I

    move-result v5

    move-object v2, v0

    .line 67
    invoke-direct/range {v1 .. v6}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 70
    .local v1, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_0

    .line 66
    .end local v1    # "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v6    # "isDefault":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static getContactMatchList(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "dlContactList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .local v1, "contactMatchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-nez p0, :cond_1

    .line 25
    const/4 v1, 0x0

    .line 33
    .end local v1    # "contactMatchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_0
    return-object v1

    .line 28
    .restart local v1    # "contactMatchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    .line 29
    .local v2, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-static {v2}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 30
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getDlCallLog(Lcom/nuance/sample/util/calllog/LoggedCall;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    .locals 8
    .param p0, "loggedCall"    # Lcom/nuance/sample/util/calllog/LoggedCall;

    .prologue
    const/4 v5, 0x0

    .line 98
    if-nez p0, :cond_0

    .line 106
    :goto_0
    return-object v5

    .line 102
    :cond_0
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;

    sget-object v1, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;

    .line 103
    sget-object v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;->UNKNOWN:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;

    invoke-virtual {p0}, Lcom/nuance/sample/util/calllog/LoggedCall;->getContactName()Ljava/lang/String;

    move-result-object v3

    .line 104
    invoke-virtual {p0}, Lcom/nuance/sample/util/calllog/LoggedCall;->getContactNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/nuance/sample/util/calllog/LoggedCall;->getDate()J

    move-result-wide v6

    .line 102
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/calllog/DLCallLogImpl;-><init>(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$CALLLOG_TYPE;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog$LOG_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .local v0, "callLog":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;
    move-object v5, v0

    .line 106
    goto :goto_0
.end method

.method public static getDlContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .locals 10
    .param p0, "contactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 77
    if-nez p0, :cond_0

    .line 78
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    .line 81
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v7, "phoneNumberList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 89
    new-instance v0, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    iget-wide v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 90
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    .line 91
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v6

    .line 89
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 94
    .local v0, "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    goto :goto_0

    .line 83
    .end local v0    # "dlContact":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 84
    .local v8, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    new-instance v9, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;

    .line 85
    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v2

    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/ContactData;->getNormalizedAddress()Ljava/lang/String;

    move-result-object v3

    .line 84
    invoke-direct {v9, v2, v3}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLPhoneNumberImpl;-><init>(ILjava/lang/String;)V

    .line 86
    .local v9, "dlPhoneNumber":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
