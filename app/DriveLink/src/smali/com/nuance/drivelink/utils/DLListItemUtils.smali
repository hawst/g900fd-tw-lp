.class public Lcom/nuance/drivelink/utils/DLListItemUtils;
.super Ljava/lang/Object;
.source "DLListItemUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mOrdinal:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/nuance/drivelink/utils/DLListItemUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    .line 14
    new-instance v0, Lcom/nuance/drivelink/utils/DLListItemUtils$1;

    invoke-direct {v0}, Lcom/nuance/drivelink/utils/DLListItemUtils$1;-><init>()V

    sput-object v0, Lcom/nuance/drivelink/utils/DLListItemUtils;->mOrdinal:Ljava/util/HashMap;

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMultiNameIndex(Ljava/lang/String;)Z
    .locals 8
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const/4 v3, 0x0

    .line 158
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 160
    .local v2, "isAvail":Z
    if-nez p0, :cond_0

    move v4, v5

    .line 190
    :goto_0
    return v4

    .line 164
    :cond_0
    sget-object v4, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "getMultiNameIndex : name - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v4

    .line 166
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_1

    .line 170
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 173
    :cond_1
    if-eqz v3, :cond_2

    .line 174
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_3

    .line 187
    .end local v1    # "i":I
    :cond_2
    if-nez v2, :cond_5

    move v4, v5

    .line 188
    goto :goto_0

    .line 176
    .restart local v1    # "i":I
    :cond_3
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 177
    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 179
    sget-object v4, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "getNameIndex : matched[ "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 180
    iget v7, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 179
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v2, 0x1

    .line 174
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 190
    .end local v1    # "i":I
    :cond_5
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static getNameIndex(Ljava/lang/String;)Z
    .locals 10
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 69
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const/4 v4, 0x0

    .line 70
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 71
    .local v2, "isMatched":Z
    const/4 v5, 0x0

    .line 72
    .local v5, "matchCount":I
    const/4 v6, -0x1

    .line 74
    .local v6, "partialMatched":I
    if-nez p0, :cond_0

    move v3, v2

    .line 119
    .end local v2    # "isMatched":Z
    .local v3, "isMatched":I
    :goto_0
    return v3

    .line 78
    .end local v3    # "isMatched":I
    .restart local v2    # "isMatched":Z
    :cond_0
    sget-object v7, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "getNameIndex : name - "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v7

    .line 80
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_1

    .line 84
    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 87
    :cond_1
    if-eqz v4, :cond_2

    .line 88
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v1, v7, :cond_6

    .line 102
    .end local v1    # "i":I
    :cond_2
    if-nez v2, :cond_4

    .line 103
    invoke-static {p0, v4}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getNameIndexPartial(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v6

    .line 105
    const/4 v7, -0x1

    if-eq v6, v7, :cond_4

    .line 106
    if-eqz v0, :cond_3

    .line 107
    iput v6, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 108
    sget-object v7, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "getNameIndex : partial matched[ "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 109
    iget v9, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 108
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_3
    const/4 v2, 0x1

    .line 115
    :cond_4
    const/4 v7, 0x1

    if-le v5, v7, :cond_5

    .line 116
    const/4 v2, 0x0

    :cond_5
    move v3, v2

    .line 119
    .restart local v3    # "isMatched":I
    goto :goto_0

    .line 90
    .end local v3    # "isMatched":I
    .restart local v1    # "i":I
    :cond_6
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v7, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 91
    if-eqz v0, :cond_7

    .line 92
    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 93
    sget-object v7, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "getNameIndex : matched[ "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 94
    iget v9, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 93
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_7
    const/4 v2, 0x1

    .line 97
    add-int/lit8 v5, v5, 0x1

    .line 88
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static getNameIndexPartial(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 8
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, -0x1

    .line 124
    .local v3, "matchedIndex":I
    const/4 v2, 0x0

    .line 126
    .local v2, "matchCount":I
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v4, v3

    .line 151
    .end local v3    # "matchedIndex":I
    .local v4, "matchedIndex":I
    :goto_0
    return v4

    .line 130
    .end local v4    # "matchedIndex":I
    .restart local v3    # "matchedIndex":I
    :cond_1
    sget-object v5, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "getNameIndexPartial : name - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v1, v5, :cond_3

    .line 143
    const/4 v5, 0x1

    if-le v2, v5, :cond_2

    .line 144
    const/4 v3, -0x1

    :cond_2
    :goto_2
    move v4, v3

    .line 151
    .end local v3    # "matchedIndex":I
    .restart local v4    # "matchedIndex":I
    goto :goto_0

    .line 135
    .end local v4    # "matchedIndex":I
    .restart local v3    # "matchedIndex":I
    :cond_3
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, ".*(?i)"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".*"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 136
    move v3, v1

    .line 138
    sget-object v5, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "getNameIndex : matched[ "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    add-int/lit8 v2, v2, 0x1

    .line 133
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, -0x1

    goto :goto_2
.end method

.method public static getOrdinalIndex(Ljava/lang/String;)Z
    .locals 6
    .param p0, "which"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 29
    const/4 v1, 0x0

    .line 30
    .local v1, "index":I
    const/4 v0, 0x0

    .line 32
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-nez p0, :cond_0

    move v2, v3

    .line 64
    :goto_0
    return v2

    .line 36
    :cond_0
    sget-object v2, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "getOrdinalIndex : which - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    .line 38
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    .line 42
    :cond_1
    sget-object v2, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "getOrdinalIndex : false - null"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 43
    goto :goto_0

    .line 46
    :cond_2
    sget-object v2, Lcom/nuance/drivelink/utils/DLListItemUtils;->mOrdinal:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    move v2, v3

    .line 47
    goto :goto_0

    .line 50
    :cond_3
    sget-object v2, Lcom/nuance/drivelink/utils/DLListItemUtils;->mOrdinal:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 52
    if-lez v1, :cond_5

    .line 53
    iget-object v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v1, v2, :cond_4

    .line 54
    add-int/lit8 v2, v1, -0x1

    iput v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 59
    :goto_1
    sget-object v2, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getOrdinalIndex : matched[ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 60
    iget v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 59
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const/4 v2, 0x1

    goto :goto_0

    .line 56
    :cond_4
    const/4 v2, -0x1

    iput v2, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    goto :goto_1

    :cond_5
    move v2, v3

    .line 64
    goto :goto_0
.end method

.method public static setForcedIndex(I)Z
    .locals 6
    .param p0, "index"    # I

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const/4 v1, 0x0

    .line 197
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 199
    .local v2, "ret":Z
    sget-object v3, Lcom/nuance/drivelink/utils/DLListItemUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "setForcedIndex : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    .line 201
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_0

    .line 205
    iget-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionList:Ljava/util/ArrayList;

    .line 208
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p0, v3, :cond_1

    .line 209
    iput p0, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSuggestionSelected:I

    .line 210
    const/4 v2, 0x1

    .line 213
    :cond_1
    return v2
.end method
