.class Lcom/nuance/drivelink/utils/DLLocationVACUtils$1;
.super Ljava/lang/Object;
.source "DLLocationVACUtils.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/drivelink/utils/DLLocationVACUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onNotifySmartAlert(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;Ljava/lang/String;)V
    .locals 0
    .param p1, "incidentInfo"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLIncidentInfo;
    .param p2, "msgErro"    # Ljava/lang/String;

    .prologue
    .line 280
    return-void
.end method

.method public onResponseCreateUserProfile(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 249
    return-void
.end method

.method public onResponseRemoveGroupShared(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 243
    return-void
.end method

.method public onResponseRequestAcceptLocationShared(Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 0
    .param p1, "friend"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 237
    return-void
.end method

.method public onResponseRequestAcceptShareMyLocation(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 267
    return-void
.end method

.method public onResponseRequestAddFriendsToGroup(ZLjava/util/ArrayList;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "friendAdded"    # Z
    .param p3, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;",
            ">;",
            "Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 231
    .local p2, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;>;"
    return-void
.end method

.method public onResponseRequestAllParticipantTracking(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "allTracks":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;>;>;"
    return-void
.end method

.method public onResponseRequestChangeGroupDestination(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "response"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 217
    return-void
.end method

.method public onResponseRequestCreateGroupShare(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "groupCreated"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 210
    return-void
.end method

.method public onResponseRequestFriendLocationShare(Ljava/lang/Exception;Z)V
    .locals 0
    .param p1, "error"    # Ljava/lang/Exception;
    .param p2, "result"    # Z

    .prologue
    .line 203
    return-void
.end method

.method public onResponseRequestGroupSharedUpdateInfo(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "updated"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 198
    return-void
.end method

.method public onResponseRequestQuitFromGroup(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 191
    return-void
.end method

.method public onResponseRequestRecommendedLocationList(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "locationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const/4 v9, 0x0

    .line 111
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$0()Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    move-result-object v6

    if-eqz v6, :cond_0

    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mKey:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$1()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    const/4 v0, 0x0

    .line 116
    .local v0, "home":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    const/4 v4, 0x0

    .line 118
    .local v4, "office":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_b

    .line 128
    const/4 v1, 0x0

    .line 129
    .local v1, "homeLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    const/4 v5, 0x0

    .line 131
    .local v5, "officeLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    if-eqz v0, :cond_3

    .line 132
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v1

    .line 135
    :cond_3
    if-eqz v4, :cond_4

    .line 136
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v5

    .line 139
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidAddres()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 140
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v6

    if-nez v6, :cond_5

    .line 142
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v6

    .line 143
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v7

    .line 141
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getLatLngFromAddress(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    .line 145
    .local v3, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    if-eqz v3, :cond_5

    .line 146
    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLatitude(D)V

    .line 147
    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLongitude(D)V

    .line 151
    .end local v3    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_5
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidAddres()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 152
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v6

    if-nez v6, :cond_6

    .line 154
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v6

    .line 155
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v7

    .line 153
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/location/map/util/GeocodingAPI;->getLatLngFromAddress(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    .line 157
    .restart local v3    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    if-eqz v3, :cond_6

    .line 158
    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLatitude(D)V

    .line 159
    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLongitude(D)V

    .line 163
    .end local v3    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_6
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mKey:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$1()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "key_settings_home"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 164
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidAddres()Z

    move-result v6

    if-nez v6, :cond_d

    .line 165
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v6

    if-nez v6, :cond_d

    .line 166
    :cond_7
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$0()Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    move-result-object v6

    invoke-interface {v6, v9}, Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;->onResponse(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 172
    :cond_8
    :goto_2
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mKey:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$1()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "key_settings_office"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 173
    if-eqz v5, :cond_9

    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidAddres()Z

    move-result v6

    if-nez v6, :cond_e

    .line 174
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->isValidCoords()Z

    move-result v6

    if-nez v6, :cond_e

    .line 175
    :cond_9
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$0()Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    move-result-object v6

    invoke-interface {v6, v9}, Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;->onResponse(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 181
    :cond_a
    :goto_3
    invoke-static {v9}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$2(Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;)V

    .line 182
    invoke-static {v9}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$3(Ljava/lang/String;)V

    .line 183
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v6

    .line 184
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$4()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    move-result-object v7

    .line 183
    invoke-virtual {v6, v7}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->removeLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    goto/16 :goto_0

    .line 118
    .end local v1    # "homeLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .end local v5    # "officeLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    :cond_b
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 119
    .local v2, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "location_home"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 120
    move-object v0, v2

    .line 123
    :cond_c
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "location_office"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 124
    move-object v4, v2

    goto/16 :goto_1

    .line 168
    .end local v2    # "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    .restart local v1    # "homeLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .restart local v5    # "officeLocation":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    :cond_d
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$0()Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    move-result-object v6

    invoke-interface {v6, v1}, Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;->onResponse(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    goto :goto_2

    .line 177
    :cond_e
    # getter for: Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;
    invoke-static {}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->access$0()Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    move-result-object v6

    invoke-interface {v6, v5}, Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;->onResponse(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    goto :goto_3
.end method

.method public onResponseRequestReinviteFriendToGroup(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;)V
    .locals 0
    .param p1, "arg0"    # Z
    .param p2, "arg1"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;
    .param p3, "arg2"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONBuddy;

    .prologue
    .line 256
    return-void
.end method

.method public onResponseRequestRestartGroupShared(ZLcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;)V
    .locals 0
    .param p1, "response"    # Z
    .param p2, "group"    # Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLPlaceONGroup;

    .prologue
    .line 105
    return-void
.end method

.method public onResponseRequestSetMyLocation(Z)V
    .locals 0
    .param p1, "locationSetted"    # Z

    .prologue
    .line 98
    return-void
.end method

.method public onResponseRequestShareMyLocation(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 273
    return-void
.end method

.method public onResponseRequestUpdateParticipantStatus(Z)V
    .locals 0
    .param p1, "mResult"    # Z

    .prologue
    .line 261
    return-void
.end method
