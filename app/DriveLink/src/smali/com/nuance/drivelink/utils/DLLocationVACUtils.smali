.class public Lcom/nuance/drivelink/utils/DLLocationVACUtils;
.super Ljava/lang/Object;
.source "DLLocationVACUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;
    }
.end annotation


# static fields
.field public static final KEY_HOME:Ljava/lang/String; = "key_settings_home"

.field public static final KEY_OFFICE:Ljava/lang/String; = "key_settings_office"

.field private static final NAME_HOME:Ljava/lang/String; = "location_home"

.field private static final NAME_OFFICE:Ljava/lang/String; = "location_office"

.field private static mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

.field private static mKey:Ljava/lang/String;

.field private static mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    sput-object v0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    .line 38
    sput-object v0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mKey:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/nuance/drivelink/utils/DLLocationVACUtils$1;

    invoke-direct {v0}, Lcom/nuance/drivelink/utils/DLLocationVACUtils$1;-><init>()V

    sput-object v0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    .line 281
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;)V
    .locals 0

    .prologue
    .line 37
    sput-object p0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    return-void
.end method

.method static synthetic access$3(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    sput-object p0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mKey:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    return-object v0
.end method

.method public static getMyPlace(Landroid/content/Context;Ljava/lang/String;Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    .prologue
    .line 41
    sput-object p2, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mCallback:Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;

    .line 42
    sput-object p1, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mKey:Ljava/lang/String;

    .line 44
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 45
    .local v0, "serviceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 46
    sget-object v2, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->mLocationListener:Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->addLocationReqListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkLocationListener;)V

    .line 47
    const/16 v1, 0x64

    invoke-interface {v0, p0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestRecommendedLocationList(Landroid/content/Context;I)V

    .line 48
    return-void
.end method

.method public static startConfirmNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/automotive/drivelink/location/LocationConfirmActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    .local v1, "intentMsg":Landroid/content/Intent;
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 78
    if-nez p1, :cond_0

    .line 79
    const-string/jumbo v2, "address"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    :goto_0
    invoke-static {p0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 90
    return-void

    .line 81
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 82
    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLocationAddress()Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "addr":Ljava/lang/String;
    :goto_1
    const-string/jumbo v2, "address"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string/jumbo v2, "latitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 85
    const-string/jumbo v2, "longitude"

    invoke-virtual {p1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    goto :goto_0

    .end local v0    # "addr":Ljava/lang/String;
    :cond_1
    move-object v0, p2

    .line 82
    goto :goto_1
.end method

.method public static startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    const-wide/16 v0, 0x0

    .line 52
    if-nez p1, :cond_0

    .line 53
    new-instance p1, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;

    .end local p1    # "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-direct {p1}, Lcom/sec/android/automotive/drivelink/framework/manager/content/location/DLLocationImp;-><init>()V

    .line 54
    .restart local p1    # "location":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;
    invoke-virtual {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLocationAddress(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1, p2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLocationName(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLatitude(D)V

    .line 57
    invoke-virtual {p1, v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;->setLongitude(D)V

    .line 60
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 61
    return-void
.end method
