.class Lcom/nuance/drivelink/DLAppUiUpdater$3;
.super Ljava/lang/Object;
.source "DLAppUiUpdater.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/drivelink/DLAppUiUpdater;->onPhraseSpotterStopped()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/drivelink/DLAppUiUpdater;


# direct methods
.method constructor <init>(Lcom/nuance/drivelink/DLAppUiUpdater;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater$3;->this$0:Lcom/nuance/drivelink/DLAppUiUpdater;

    .line 1030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1033
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 1034
    const/4 v2, 0x0

    .line 1033
    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v0

    .line 1036
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 1038
    # getter for: Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->access$0()Ljava/lang/String;

    move-result-object v1

    .line 1039
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onClick() mMicState IDLE, startUserFlow error, returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1040
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1039
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1038
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    :cond_0
    return-void
.end method
