.class public Lcom/nuance/drivelink/DLAppUiUpdater;
.super Ljava/lang/Object;
.source "DLAppUiUpdater.java"

# interfaces
.implements Lcom/nuance/sample/MicStateMaster;
.implements Lcom/nuance/sample/UiUpdater;
.implements Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;
.implements Lcom/sec/android/automotive/drivelink/music/MusicListener;
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
.implements Lcom/vlingo/core/internal/util/VlingoApplicationInterface;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field private static final HOST_SETTINGS_UTILS:[Ljava/lang/String;

.field private static final SHARED_PREFS_NAME:Ljava/lang/String; = "lmttutility"

.field private static final TAG:Ljava/lang/String;

.field public static final VAC_STATE_INIT_DESTORYED:I = -0x1

.field public static final VAC_STATE_INIT_FAILED_PROVIDER_INVALID:I = 0x2

.field public static final VAC_STATE_INIT_FAILED_TOS_NOT_ACCEPTED:I = 0x1

.field public static final VAC_STATE_INIT_SUCCESS:I = 0x0

.field private static final c_SVN:Ljava/lang/String; = "11111"

.field private static mInstance:Lcom/nuance/drivelink/DLAppUiUpdater;


# instance fields
.field private SAMPLE_APP_SERVER_KEYS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppVersion:Ljava/lang/String;

.field private mCurrentMicState:Lcom/nuance/sample/MicState;

.field private mCurrentVoiceActivty:Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

.field private mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

.field private mInitState:I

.field private mIsCancelByTouch:Z

.field private mIsMicDisplayed:Z

.field private mIsTTSPlaying:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLastMicState:Lcom/nuance/sample/MicState;

.field private mNotiMode:Z

.field private mNotiVoiceUiUpdater:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/drivelink/DLUiUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private mPausedMicState:Lcom/nuance/sample/MicState;

.field private mPhraseSpotterHandler:Landroid/os/Handler;

.field private mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

.field private mVoiceLocale:Ljava/util/Locale;

.field private mVoiceUiUpdater:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/drivelink/DLUiUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private misForeground:Z

.field private preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 84
    const-class v0, Lcom/nuance/drivelink/DLAppUiUpdater;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    .line 128
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 129
    const-string/jumbo v2, "com.nuance.nuancedebugutil"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "com.vlingo.midas.lmttutility"

    aput-object v2, v0, v1

    .line 128
    sput-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->HOST_SETTINGS_UTILS:[Ljava/lang/String;

    .line 132
    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    const-string/jumbo v0, "11.0.11111"

    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mAppVersion:Ljava/lang/String;

    .line 93
    iput-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    .line 102
    iput-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentVoiceActivty:Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInitState:I

    .line 111
    iput-boolean v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->misForeground:Z

    .line 112
    iput-boolean v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    .line 113
    iput-boolean v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsMicDisplayed:Z

    .line 115
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mLastMicState:Lcom/nuance/sample/MicState;

    .line 119
    iput-boolean v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsCancelByTouch:Z

    .line 121
    iput-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 123
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 124
    const-string/jumbo v1, "SERVER_NAME"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "SERVICES_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 125
    const-string/jumbo v2, "EVENTLOG_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "HELLO_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 126
    const-string/jumbo v2, "LMTT_HOST_NAME"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->SAMPLE_APP_SERVER_KEYS:Ljava/util/List;

    .line 1167
    new-instance v0, Lcom/nuance/drivelink/DLAppUiUpdater$1;

    invoke-direct {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater$1;-><init>(Lcom/nuance/drivelink/DLAppUiUpdater;)V

    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mPhraseSpotterHandler:Landroid/os/Handler;

    .line 135
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 136
    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    .line 139
    iput-boolean v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    .line 140
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mPausedMicState:Lcom/nuance/sample/MicState;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 141
    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    .line 144
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->getInstance()Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver;->registerHeadsetPlugListener(Lcom/sec/android/automotive/drivelink/common/receiver/HeadsetPlugReceiver$HeadsetPlugListener;)V

    .line 145
    return-void
.end method

.method private ChangedPhraseSpotterState(Z)V
    .locals 5
    .param p1, "isSpotting"    # Z

    .prologue
    .line 1195
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "ChangedPhraseSpotterState : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-nez v2, :cond_1

    .line 1200
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 1201
    :try_start_0
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1202
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1200
    monitor-exit v3

    .line 1210
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_1
    return-void

    .line 1203
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 1204
    .local v1, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_0

    .line 1205
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->onPhraseSpotterStateChanged(Z)V

    goto :goto_0

    .line 1200
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/nuance/drivelink/DLAppUiUpdater;Z)V
    .locals 0

    .prologue
    .line 1194
    invoke-direct {p0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->ChangedPhraseSpotterState(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/nuance/drivelink/DLAppUiUpdater;Z)V
    .locals 0

    .prologue
    .line 903
    invoke-direct {p0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/nuance/drivelink/DLAppUiUpdater;)Ljava/util/List;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->SAMPLE_APP_SERVER_KEYS:Ljava/util/List;

    return-object v0
.end method

.method private changeMicDisplayed(Z)V
    .locals 3
    .param p1, "isPlayed"    # Z

    .prologue
    .line 1303
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 1304
    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1303
    check-cast v0, Landroid/media/AudioManager;

    .line 1305
    .local v0, "audioMgr":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->changeMicDisplayed(ZZ)V

    .line 1306
    return-void
.end method

.method private changeMicDisplayed(ZZ)V
    .locals 4
    .param p1, "isPlayed"    # Z
    .param p2, "isHeadsetPlugged"    # Z

    .prologue
    .line 1309
    if-eqz p1, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1310
    if-nez p2, :cond_2

    .line 1311
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsMicDisplayed:Z

    .line 1321
    :cond_0
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-eqz v2, :cond_4

    .line 1322
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 1323
    :try_start_0
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1324
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1322
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1342
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :goto_1
    return-void

    .line 1314
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsMicDisplayed:Z

    .line 1315
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 1325
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_3
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 1326
    .local v1, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_1

    .line 1327
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsMicDisplayed:Z

    invoke-interface {v1, v2}, Lcom/nuance/drivelink/DLUiUpdater;->onDisplayMic(Z)V

    goto :goto_0

    .line 1322
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 1332
    :cond_4
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 1333
    :try_start_2
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1334
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_5
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1332
    monitor-exit v3

    goto :goto_1

    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 1335
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_6
    :try_start_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 1336
    .restart local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_5

    .line 1337
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsMicDisplayed:Z

    invoke-interface {v1, v2}, Lcom/nuance/drivelink/DLUiUpdater;->onDisplayMic(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2
.end method

.method private createServerInitParameters()Ljava/util/Map;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 373
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 375
    .local v0, "context":Landroid/content/Context;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 376
    .local v5, "serverParameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 377
    .local v2, "hostSettingsUtilsContext":Landroid/content/Context;
    sget-object v9, Lcom/nuance/drivelink/DLAppUiUpdater;->HOST_SETTINGS_UTILS:[Ljava/lang/String;

    array-length v10, v9

    move v7, v8

    :goto_0
    if-lt v7, v10, :cond_1

    .line 398
    :cond_0
    return-object v5

    .line 377
    :cond_1
    aget-object v3, v9, v7

    .line 379
    .local v3, "packageName":Ljava/lang/String;
    const/4 v11, 0x0

    :try_start_0
    invoke-virtual {v0, v3, v11}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 385
    :goto_1
    if-eqz v2, :cond_3

    .line 386
    iget-object v11, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->SAMPLE_APP_SERVER_KEYS:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_4

    .line 393
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v11

    if-gtz v11, :cond_0

    .line 377
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 380
    :catch_0
    move-exception v1

    .line 382
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v11, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Couldn\'t create "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 383
    const-string/jumbo v13, "package context for host settings utility"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 382
    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 386
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 388
    .local v4, "serverKey":Ljava/lang/String;
    const-string/jumbo v12, "lmttutility"

    .line 387
    invoke-virtual {v2, v12, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 388
    const/4 v13, 0x0

    invoke-interface {v12, v4, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 389
    .local v6, "sharedHost":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 390
    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method private getHomeGrammerContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 5

    .prologue
    .line 1104
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1105
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;-><init>()V

    .line 1106
    .local v0, "builder":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1108
    .local v2, "generalSpeakables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const v4, 0x7f0a024f

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1109
    const v4, 0x7f0a0250

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1110
    const v4, 0x7f0a0252

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1111
    const v4, 0x7f0a0253

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1112
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->generalSpeakables(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    .line 1113
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->build()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v3

    .line 1115
    .local v3, "toReturn":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    return-object v3
.end method

.method public static getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInstance:Lcom/nuance/drivelink/DLAppUiUpdater;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Lcom/nuance/drivelink/DLAppUiUpdater;

    invoke-direct {v0}, Lcom/nuance/drivelink/DLAppUiUpdater;-><init>()V

    sput-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInstance:Lcom/nuance/drivelink/DLAppUiUpdater;

    .line 152
    :cond_0
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInstance:Lcom/nuance/drivelink/DLAppUiUpdater;

    return-object v0
.end method

.method private hasUserAcceptedTermsInSvoice()Z
    .locals 2

    .prologue
    .line 369
    const-string/jumbo v0, "tos_accepted"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private isHome(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)Z
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    .prologue
    .line 486
    const/4 v0, 0x0

    .line 487
    .local v0, "ret":Z
    if-nez p1, :cond_0

    .line 488
    const/4 v0, 0x1

    .line 491
    :cond_0
    return v0
.end method

.method private pauseDialogMode()V
    .locals 4

    .prologue
    .line 686
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 687
    :try_start_0
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 688
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 686
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 695
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    .line 696
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    iput-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mPausedMicState:Lcom/nuance/sample/MicState;

    .line 697
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->endpointReco()V

    .line 698
    return-void

    .line 689
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 690
    .local v1, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_0

    .line 691
    sget-object v2, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-interface {v1, v2}, Lcom/nuance/drivelink/DLUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    goto :goto_0

    .line 686
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private resumeDialogMode()V
    .locals 2

    .prologue
    .line 701
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mPausedMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 711
    :goto_0
    :pswitch_0
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mPausedMicState:Lcom/nuance/sample/MicState;

    .line 712
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    .line 713
    return-void

    .line 705
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    goto :goto_0

    .line 701
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setClickable(Z)V
    .locals 5
    .param p1, "isClickable"    # Z

    .prologue
    .line 904
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setClickable:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", NotiMode?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-eqz v2, :cond_2

    .line 906
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 907
    :try_start_0
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 908
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 906
    monitor-exit v3

    .line 926
    :goto_1
    return-void

    .line 909
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 910
    .local v1, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_0

    .line 911
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->onClickable(Z)V

    goto :goto_0

    .line 906
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 916
    :cond_2
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 917
    :try_start_1
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 918
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 916
    monitor-exit v3

    goto :goto_1

    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2

    .line 919
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_4
    :try_start_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 920
    .restart local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_3

    .line 921
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->onClickable(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2
.end method


# virtual methods
.method public OnBTBtnCallingState()V
    .locals 0

    .prologue
    .line 1249
    return-void
.end method

.method public OnBTBtnClicked()V
    .locals 0

    .prologue
    .line 1261
    return-void
.end method

.method public OnBTBtnPlay()V
    .locals 0

    .prologue
    .line 1255
    return-void
.end method

.method public OnBTBtnSeek()V
    .locals 0

    .prologue
    .line 1267
    return-void
.end method

.method public OnError(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 1237
    return-void
.end method

.method public OnFinished()V
    .locals 0

    .prologue
    .line 1231
    return-void
.end method

.method public OnSeekComplete()V
    .locals 0

    .prologue
    .line 1243
    return-void
.end method

.method public OnSetVolumeControl(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 1283
    return-void
.end method

.method public OnShuffle(Z)V
    .locals 0
    .param p1, "bShuffle"    # Z

    .prologue
    .line 1273
    return-void
.end method

.method public OnVolumeControlCommand(I)V
    .locals 0
    .param p1, "command"    # I

    .prologue
    .line 1289
    return-void
.end method

.method public addNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 3
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 643
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    invoke-interface {p1, v0}, Lcom/nuance/drivelink/DLUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 644
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 645
    invoke-direct {p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->pauseDialogMode()V

    .line 648
    :cond_0
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 649
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 648
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 652
    const-string/jumbo v0, "UiUpdater"

    .line 653
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "addNotiVoiceUiUpdater("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 654
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 653
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 652
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    return-void

    .line 648
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public addVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 3
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-nez v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    invoke-interface {p1, v0}, Lcom/nuance/drivelink/DLUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 608
    :cond_0
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 609
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 608
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 612
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "addVoiceUiUpdater("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 613
    const-string/jumbo v2, ") - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 612
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    return-void

    .line 608
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public destroy(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)V
    .locals 4
    .param p1, "activity"    # Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    .prologue
    .line 577
    iget v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInitState:I

    if-eqz v1, :cond_0

    .line 601
    :goto_0
    return-void

    .line 581
    :cond_0
    invoke-direct {p0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isHome(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)Z

    move-result v0

    .line 583
    .local v0, "isHome":Z
    invoke-virtual {p0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->pause(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)V

    .line 585
    sget-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[destroy]"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " / Home :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    if-eqz v0, :cond_1

    .line 588
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerDestroy()V

    .line 589
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->destroyCore()V

    .line 591
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 592
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    .line 591
    invoke-interface {v1, v2, p0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 594
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    if-eqz v1, :cond_1

    .line 595
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->destroyFlow()V

    .line 599
    :cond_1
    const/4 v1, 0x0

    sput-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->mInstance:Lcom/nuance/drivelink/DLAppUiUpdater;

    .line 600
    const/4 v1, -0x1

    iput v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInitState:I

    goto :goto_0
.end method

.method public displayError(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 717
    sget-object v3, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "> displayError : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    check-cast p1, Ljava/lang/String;

    .line 719
    .end local p1    # "cs":Ljava/lang/CharSequence;
    invoke-static {v3, p1}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->convertTTSText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 722
    .local v0, "convertCs":Ljava/lang/CharSequence;
    iget-boolean v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-nez v3, :cond_1

    .line 725
    iget-object v4, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v4

    .line 726
    :try_start_0
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 727
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 725
    monitor-exit v4

    .line 735
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_1
    return-void

    .line 728
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/nuance/drivelink/DLUiUpdater;

    .line 729
    .local v2, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v2, :cond_0

    .line 730
    invoke-interface {v2, v0}, Lcom/nuance/drivelink/DLUiUpdater;->displayError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 725
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v2    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 753
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "> displaySystemTurn : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-nez v2, :cond_1

    .line 758
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 759
    :try_start_0
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 760
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 758
    monitor-exit v3

    .line 768
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_1
    return-void

    .line 761
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 762
    .local v1, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_0

    .line 763
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->displaySystemTurn(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 758
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 739
    sget-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "> displayUserTurn : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    const-string/jumbo v1, "HIDDEN_FEATURE_DISP_USERTURN"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 742
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 743
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 745
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "UserTurn : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 746
    const/4 v2, 0x1

    .line 745
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 746
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 749
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 772
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "> displayWidgetContent : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    return-void
.end method

.method public getCancelByTouch()Z
    .locals 3

    .prologue
    .line 1218
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getCancelByTouch : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsCancelByTouch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    iget-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsCancelByTouch:Z

    return v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 4

    .prologue
    .line 1087
    const/4 v0, 0x0

    .line 1090
    .local v0, "grammer":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentVoiceActivty:Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    if-eqz v1, :cond_0

    .line 1091
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentVoiceActivty:Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;->getDLGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v0

    .line 1097
    :goto_0
    sget-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[getGrammarContext]"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentVoiceActivty:Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1098
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1097
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    return-object v0

    .line 1094
    :cond_0
    invoke-direct {p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->getHomeGrammerContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v0

    goto :goto_0
.end method

.method public getInitState()I
    .locals 3

    .prologue
    .line 475
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[getInitState] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInitState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInitState:I

    if-eqz v0, :cond_0

    .line 478
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[getInitState] retry init."

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :cond_0
    iget v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInitState:I

    return v0
.end method

.method protected getKeyguardManager(Landroid/content/Context;)Landroid/app/KeyguardManager;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v0, :cond_0

    .line 158
    const-string/jumbo v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 157
    iput-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mKeyguardManager:Landroid/app/KeyguardManager;

    return-object v0
.end method

.method public getLastMicState()Lcom/nuance/sample/MicState;
    .locals 3

    .prologue
    .line 1150
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getLastMicState : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mLastMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v2}, Lcom/nuance/sample/MicState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1152
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mLastMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public getMainActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1076
    const-class v0, Lcom/sec/android/automotive/drivelink/home/HomeActivity;

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 877
    const-string/jumbo v1, "samsung_wakeup_engine_enable"

    .line 876
    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 877
    if-eqz v1, :cond_0

    .line 878
    sget-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    .line 879
    const-string/jumbo v2, "initPhraseSpotter Samsung Seamless and Samsung Multiple Wake-up"

    .line 878
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 881
    const-class v2, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;

    .line 880
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 883
    invoke-static {}, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 900
    .local v0, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_0
    return-object v0

    .line 885
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_0
    const-string/jumbo v1, "samsung_multi_engine_enable"

    .line 884
    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 885
    if-eqz v1, :cond_1

    .line 886
    sget-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    .line 887
    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless and Samsung Multiple Wake-up"

    .line 886
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 889
    const-class v2, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;

    .line 888
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 891
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 892
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 891
    invoke-static {v1}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 893
    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0

    .line 894
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_1
    sget-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless Only"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 896
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 897
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 896
    invoke-static {v1}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 895
    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0
.end method

.method public getVoiceLocale()Ljava/util/Locale;
    .locals 3

    .prologue
    .line 1162
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getVoiceLocale : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceLocale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1164
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 2

    .prologue
    .line 1081
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleUserCancel"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    const-string/jumbo v0, "DM_CANCEL"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 1083
    return-void
.end method

.method public hasNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 672
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 673
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 672
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public init()Z
    .locals 14

    .prologue
    const/4 v13, -0x1

    const/4 v7, 0x0

    .line 199
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 202
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v8

    invoke-interface {v8, v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->init(Landroid/content/Context;)V

    .line 204
    new-instance v8, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    iget-object v9, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mAppVersion:Ljava/lang/String;

    invoke-direct {v8, v0, p0, v9}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;)V

    iput-object v8, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    .line 205
    iget-object v8, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    invoke-virtual {v8}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->initCore()V

    .line 207
    new-instance v8, Lcom/nuance/drivelink/DLAppUiUpdater$2;

    invoke-direct {v8, p0}, Lcom/nuance/drivelink/DLAppUiUpdater$2;-><init>(Lcom/nuance/drivelink/DLAppUiUpdater;)V

    iput-object v8, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 228
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    .line 229
    iget-object v9, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v8, v9}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 231
    const-string/jumbo v8, "tos_accepted"

    invoke-static {v8, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 241
    const-string/jumbo v8, "hello_request_complete"

    invoke-static {v8, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 242
    if-nez v8, :cond_0

    .line 245
    invoke-static {}, Lcom/vlingo/core/internal/util/SayHello;->sendHello()Z

    .line 285
    :cond_0
    :goto_0
    const-string/jumbo v8, "stream_volume"

    invoke-static {v8, v13}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 286
    const-string/jumbo v8, "system_volume"

    invoke-static {v8, v13}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 288
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 289
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v3, :cond_1

    .line 290
    const-string/jumbo v8, "endpoint.speechdetect_min_voice_level"

    const/high16 v9, 0x42340000    # 45.0f

    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 291
    invoke-static {v3}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 293
    iput v7, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mInitState:I

    .line 294
    const/4 v7, 0x1

    .line 296
    :cond_1
    return v7

    .line 250
    .end local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/lmtt/MdsoUtils;->isSlave()Z

    move-result v8

    if-nez v8, :cond_0

    .line 251
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    .line 252
    .local v1, "currentLanguage":Ljava/lang/String;
    move-object v5, v1

    .line 253
    .local v5, "originalLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v2

    .line 254
    .local v2, "currentLocale":Ljava/util/Locale;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    .line 255
    .local v6, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 256
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v9

    array-length v10, v9

    move v8, v7

    :goto_1
    if-lt v8, v10, :cond_3

    .line 262
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 263
    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 256
    :cond_3
    aget-object v4, v9, v8

    .line 257
    .local v4, "lang":Ljava/lang/CharSequence;
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 258
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 259
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 256
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public initPhraseSpotter()V
    .locals 2

    .prologue
    .line 841
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 842
    return-void
.end method

.method public isAddedVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)Z
    .locals 2
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 633
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 634
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 635
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    .line 638
    :goto_0
    return v0

    .line 634
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 638
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAppInForeground()Z
    .locals 3

    .prologue
    .line 1071
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "isAppInForeground : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->misForeground:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    iget-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->misForeground:Z

    return v0
.end method

.method public isMicDisplayed()Z
    .locals 1

    .prologue
    .line 1276
    iget-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsMicDisplayed:Z

    return v0
.end method

.method public isTTSPlaying()Z
    .locals 1

    .prologue
    .line 1136
    iget-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    return v0
.end method

.method public onHeadsetPlugChange(Z)V
    .locals 2
    .param p1, "isPlugged"    # Z

    .prologue
    .line 1293
    const/4 v0, 0x0

    .line 1295
    .local v0, "isPlayingMusic":Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1296
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v0

    .line 1299
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->changeMicDisplayed(ZZ)V

    .line 1300
    return-void
.end method

.method public onMusicChanged(Z)V
    .locals 0
    .param p1, "isPlayed"    # Z

    .prologue
    .line 1224
    invoke-direct {p0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->changeMicDisplayed(Z)V

    .line 1225
    return-void
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 3
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 961
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "phraseDetected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    const-string/jumbo v0, "DLPhraseSpotter"

    const-string/jumbo v1, "[onPhraseDetected]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 5
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 967
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 969
    .local v0, "context":Landroid/content/Context;
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "phraseSpotted: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    const-string/jumbo v2, "DLPhraseSpotter"

    const-string/jumbo v3, "[stop] : DLAppUiUpdater - onPhraseSpotted()"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 973
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getCarModeState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 975
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->checkIfCallerIsForegroundUser()Z

    move-result v2

    if-nez v2, :cond_0

    .line 976
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "phraseSpotted: Canceled - Invalid User"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    :goto_0
    return-void

    .line 980
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getWakeupState()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "BG_WAKEUP_AP"

    if-eq v2, v3, :cond_1

    .line 981
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "phraseSpotted: Canceled - not resume state"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 985
    :cond_1
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsWakeup(Landroid/content/Context;Z)V

    .line 987
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/automotive/drivelink/firstaccess/WakeUpActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 988
    .local v1, "mIntent":Landroid/content/Intent;
    const/high16 v2, 0x30000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 992
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 996
    .end local v1    # "mIntent":Landroid/content/Intent;
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->getKeyguardManager(Landroid/content/Context;)Landroid/app/KeyguardManager;

    move-result-object v2

    .line 997
    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 998
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "phraseSpotted Canceled by keyguard : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 999
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 998
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1003
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    goto :goto_0
.end method

.method public onPhraseSpotterStarted()V
    .locals 3

    .prologue
    .line 1051
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotter started"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    const-string/jumbo v0, "DLPhraseSpotter"

    const-string/jumbo v1, "[onPhraseSpotterStarted]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1054
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mPhraseSpotterHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1056
    const-string/jumbo v0, "HIDDEN_FEATURE_DISP_WAKEUP_NOTI"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1057
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 1058
    const-string/jumbo v1, "PhraseSpotter Started"

    const/4 v2, 0x0

    .line 1057
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1058
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1061
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 1062
    const-string/jumbo v0, "CM22"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 1063
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1015
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotter stopped"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    const-string/jumbo v0, "DLPhraseSpotter"

    const-string/jumbo v1, "[onPhraseSpotterStopped]"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mPhraseSpotterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1020
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getCarModeState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1021
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v0

    .line 1022
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 1021
    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->onApWakeupStopped(Landroid/content/Context;)V

    .line 1047
    :cond_0
    :goto_0
    return-void

    .line 1026
    :cond_1
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    .line 1027
    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->isStartRecoOnSpotterStop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1028
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    .line 1029
    invoke-virtual {v0, v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->setStartRecoOnSpotterStop(Z)V

    .line 1030
    new-instance v0, Lcom/nuance/drivelink/DLAppUiUpdater$3;

    invoke-direct {v0, p0}, Lcom/nuance/drivelink/DLAppUiUpdater$3;-><init>(Lcom/nuance/drivelink/DLAppUiUpdater;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    const/4 v2, 0x0

    .line 953
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "cancelled to play tts"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    iput-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    .line 955
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V

    .line 956
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 957
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v2, 0x0

    .line 937
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "completed to play tts"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    iput-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    .line 939
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V

    .line 940
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 941
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    const/4 v2, 0x0

    .line 945
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "ignored to play tts"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    iput-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    .line 947
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V

    .line 948
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 949
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 930
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "tts will be played"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    .line 932
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V

    .line 933
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 0
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 1011
    return-void
.end method

.method public onTaskWaitingToStart(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 1121
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 1122
    return-void
.end method

.method public pause(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)V
    .locals 4
    .param p1, "activity"    # Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    .prologue
    .line 495
    invoke-direct {p0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isHome(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)Z

    move-result v0

    .line 497
    .local v0, "isHome":Z
    sget-object v1, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[pause]"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " / Home :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    const-string/jumbo v1, "DLPhraseSpotter"

    const-string/jumbo v2, "[stop] : DLAppUiUpdater - pasue()."

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 501
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 502
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 504
    if-eqz v0, :cond_0

    .line 505
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 508
    :cond_0
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    if-eqz v1, :cond_1

    .line 509
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->stopFlow()V

    .line 512
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-static {v1}, Lcom/nuance/sample/settings/SampleAppSettings;->resetVolumeNormal(Landroid/content/Context;)V

    .line 516
    return-void
.end method

.method public removeAllVoiceUiUpdater()V
    .locals 2

    .prologue
    .line 625
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 626
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 625
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    const-string/jumbo v0, "UiUpdater"

    const-string/jumbo v1, "removeAllVoiceUiUpdater"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    return-void

    .line 625
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public removeNotiAllVoiceUiUpdater()V
    .locals 2

    .prologue
    .line 678
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 679
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 678
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 682
    const-string/jumbo v0, "UiUpdater"

    const-string/jumbo v1, "removeNotiAllVoiceUiUpdater"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    return-void

    .line 678
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public removeNotiVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 3
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 658
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 659
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 658
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 662
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 663
    invoke-direct {p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->resumeDialogMode()V

    .line 666
    :cond_0
    const-string/jumbo v0, "UiUpdater"

    .line 667
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "removeNotiVoiceUiUpdater("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 668
    const-string/jumbo v2, ") - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 667
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 666
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    return-void

    .line 658
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public removeVoiceUiUpdater(Lcom/nuance/drivelink/DLUiUpdater;)V
    .locals 3
    .param p1, "viv"    # Lcom/nuance/drivelink/DLUiUpdater;

    .prologue
    .line 617
    const-string/jumbo v0, "UiUpdater"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "removeVoiceUiUpdater("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 618
    const-string/jumbo v2, ") - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 617
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget-object v1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v1

    .line 620
    :try_start_0
    iget-object v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 619
    monitor-exit v1

    .line 622
    return-void

    .line 619
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resume(Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;)Z
    .locals 6
    .param p1, "activity"    # Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    .prologue
    const/4 v3, 0x0

    .line 520
    invoke-direct {p0, p1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isHome(Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;)Z

    move-result v1

    .line 521
    .local v1, "isHome":Z
    sget-object v4, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[resume] Activity : "

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 522
    if-nez p1, :cond_0

    move-object v2, p1

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 523
    const-string/jumbo v5, " / Home :"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 521
    invoke-static {v4, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    invoke-virtual {p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInitState()I

    move-result v2

    if-eqz v2, :cond_1

    .line 526
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[resume] not initialize. what happend?"

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 572
    :goto_1
    return v2

    .line 522
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 523
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 530
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 532
    .local v0, "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentVoiceActivty:Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;

    .line 553
    if-eqz v1, :cond_2

    .line 554
    const-string/jumbo v2, "DLPhraseSpotter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[start] : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " - resume()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    const-wide/16 v4, 0x258

    invoke-virtual {v2, v4, v5}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 557
    iput-boolean v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    .line 558
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsCanceledtoMain(Z)V

    .line 561
    :cond_2
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    if-nez v2, :cond_3

    .line 562
    new-instance v2, Lcom/nuance/sample/coreaccess/DialogFlowManager;

    new-instance v3, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

    invoke-direct {v3, v0, p0}, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;-><init>(Landroid/content/Context;Lcom/nuance/sample/UiUpdater;)V

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, p0, v0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;-><init>(Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    .line 568
    :goto_2
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    invoke-virtual {v2}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->initFlow()V

    .line 570
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 572
    const/4 v2, 0x1

    goto :goto_1

    .line 565
    :cond_3
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    invoke-virtual {v2}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->stopFlow()V

    goto :goto_2
.end method

.method public setCancelByTouch(Z)V
    .locals 3
    .param p1, "touch"    # Z

    .prologue
    .line 1213
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setCancelByTouch : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsCancelByTouch:Z

    .line 1215
    return-void
.end method

.method public setInForeground(Z)V
    .locals 0
    .param p1, "inForeground"    # Z

    .prologue
    .line 1066
    iput-boolean p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->misForeground:Z

    .line 1067
    return-void
.end method

.method public setLastMicState(Lcom/nuance/sample/MicState;)V
    .locals 3
    .param p1, "state"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 1145
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setLastMicState : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    iput-object p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mLastMicState:Lcom/nuance/sample/MicState;

    .line 1147
    return-void
.end method

.method public setTTSWillPlay(Z)V
    .locals 3
    .param p1, "willPlay"    # Z

    .prologue
    .line 1140
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setTTSWillPlay : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    iput-boolean p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    .line 1142
    return-void
.end method

.method public setTosAccept()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 350
    invoke-static {v1}, Lcom/vlingo/core/internal/settings/Settings;->setTOSAccepted(Z)V

    .line 351
    const-string/jumbo v0, "iux_complete"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 352
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onTosAccepted()V

    .line 353
    invoke-static {}, Lcom/nuance/sample/services/SampleServicesUtil;->getInstance()Lcom/nuance/sample/services/SampleServicesUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/services/SampleServicesUtil;->startLocalServices()Z

    .line 357
    return-void
.end method

.method public setTtsSpeed(I)V
    .locals 3
    .param p1, "speed"    # I

    .prologue
    .line 845
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 847
    .local v0, "context":Landroid/content/Context;
    const v1, 0x3f99999a    # 1.2f

    .line 849
    .local v1, "speechRate":F
    packed-switch p1, :pswitch_data_0

    .line 870
    :goto_0
    :pswitch_0
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->ttsEngine(Landroid/content/Context;)Lcom/vlingo/core/facade/tts/ITTSEngine;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/tts/ITTSEngine;->setTtsSpeechRate(F)V

    .line 871
    return-void

    .line 851
    :pswitch_1
    const v1, 0x3fe66666    # 1.8f

    .line 852
    goto :goto_0

    .line 855
    :pswitch_2
    const v1, 0x3fb33333    # 1.4f

    .line 856
    goto :goto_0

    .line 859
    :pswitch_3
    const v1, 0x3f19999a    # 0.6f

    .line 860
    goto :goto_0

    .line 863
    :pswitch_4
    const v1, 0x3e4ccccd    # 0.2f

    .line 864
    goto :goto_0

    .line 849
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setVoiceLocale(Ljava/util/Locale;)V
    .locals 3
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 1156
    sget-object v0, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setVoiceLocale : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    iput-object p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceLocale:Ljava/util/Locale;

    .line 1159
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 402
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "start"

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 404
    .local v0, "context":Landroid/content/Context;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsMicDisplayed:Z

    .line 406
    invoke-static {v0, v5}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Landroid/content/Context;Z)I

    .line 408
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    .line 409
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    .line 408
    invoke-interface {v2, v3, p0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 411
    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 412
    const-string/jumbo v3, "PREF_SETTINGS_READOUT_SPEED"

    const/4 v4, 0x2

    .line 411
    invoke-virtual {v2, v3, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v1

    .line 413
    .local v1, "speed":I
    invoke-virtual {p0, v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->setTtsSpeed(I)V

    .line 414
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onUiShown()V

    .line 441
    invoke-virtual {p0, v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->setInForeground(Z)V

    .line 442
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->languageUpdateConfiguration()V

    .line 445
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->getSVoiceLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->setVoiceLocale(Ljava/util/Locale;)V

    .line 447
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->initVolumeSetting()V

    .line 449
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->initSeamlessSetting()V

    .line 450
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->initCustomWaekupSetting()V

    .line 452
    invoke-virtual {p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->initPhraseSpotter()V

    .line 472
    return-void
.end method

.method public startMainActivity()V
    .locals 5

    .prologue
    .line 169
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    .line 170
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 173
    .local v0, "context":Landroid/content/Context;
    const/4 v2, 0x1

    .line 175
    .local v2, "iux":Z
    if-eqz v2, :cond_0

    .line 176
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    .line 177
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/nuance/drivelink/DLAppUiUpdater;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v4

    .line 176
    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    .local v1, "i":Landroid/content/Intent;
    const/high16 v3, 0x14000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 180
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 182
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public startMainService(Z)V
    .locals 3
    .param p1, "arg0"    # Z

    .prologue
    .line 189
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    .line 190
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    .line 189
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 192
    const-string/jumbo v1, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 195
    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 196
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 4
    .param p1, "rmsValue"    # I

    .prologue
    .line 812
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-eqz v2, :cond_2

    .line 813
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 814
    :try_start_0
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 815
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 813
    monitor-exit v3

    .line 833
    :goto_1
    return-void

    .line 816
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 817
    .local v1, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_0

    .line 818
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->updateMicRMSChange(I)V

    goto :goto_0

    .line 813
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 823
    :cond_2
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 824
    :try_start_1
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 825
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 823
    monitor-exit v3

    goto :goto_1

    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2

    .line 826
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_4
    :try_start_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 827
    .restart local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_3

    .line 828
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->updateMicRMSChange(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 5
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    .line 778
    sget-object v2, Lcom/nuance/drivelink/DLAppUiUpdater;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "> updateMicState : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mIsTTSPlaying:Z

    if-eqz v2, :cond_1

    .line 780
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V

    .line 785
    :goto_0
    iput-object p1, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mCurrentMicState:Lcom/nuance/sample/MicState;

    .line 787
    iget-boolean v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiMode:Z

    if-eqz v2, :cond_3

    .line 788
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 789
    :try_start_0
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mNotiVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 790
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 788
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 808
    :goto_2
    return-void

    .line 782
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_1
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->setClickable(Z)V

    goto :goto_0

    .line 791
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_2
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 792
    .local v1, "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_0

    .line 793
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    goto :goto_1

    .line 788
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    .end local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 798
    :cond_3
    iget-object v3, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    monitor-enter v3

    .line 799
    :try_start_2
    iget-object v2, p0, Lcom/nuance/drivelink/DLAppUiUpdater;->mVoiceUiUpdater:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 800
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 798
    monitor-exit v3

    goto :goto_2

    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 801
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/nuance/drivelink/DLUiUpdater;>;"
    :cond_5
    :try_start_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/drivelink/DLUiUpdater;

    .line 802
    .restart local v1    # "updater":Lcom/nuance/drivelink/DLUiUpdater;
    if-eqz v1, :cond_4

    .line 803
    invoke-interface {v1, p1}, Lcom/nuance/drivelink/DLUiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3
.end method
