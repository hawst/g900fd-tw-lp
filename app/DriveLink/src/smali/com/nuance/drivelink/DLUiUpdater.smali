.class public interface abstract Lcom/nuance/drivelink/DLUiUpdater;
.super Ljava/lang/Object;
.source "DLUiUpdater.java"

# interfaces
.implements Lcom/nuance/sample/UiUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;
    }
.end annotation


# virtual methods
.method public abstract getMicState()Lcom/nuance/sample/MicState;
.end method

.method public abstract onClickable(Z)V
.end method

.method public abstract onDisplayMic(Z)V
.end method

.method public abstract onPhraseSpotterStateChanged(Z)V
.end method

.method public abstract setOnMicStateChangeListener(Lcom/nuance/drivelink/DLUiUpdater$OnMicStateChangeListener;)V
.end method
