.class public final Lcom/nuance/embeddeddialogmanager/EDMResults;
.super Ljava/lang/Object;
.source "EDMResults.java"


# instance fields
.field private final actionResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldId:Ljava/lang/String;

.field private final state:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;[BLjava/util/List;)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "state"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[B",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p3, "actionResults":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMResults;->fieldId:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/nuance/embeddeddialogmanager/EDMResults;->state:[B

    .line 15
    iput-object p3, p0, Lcom/nuance/embeddeddialogmanager/EDMResults;->actionResults:Ljava/util/List;

    .line 16
    return-void
.end method


# virtual methods
.method public getActionResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMResults;->actionResults:Ljava/util/List;

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMResults;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getState()[B
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMResults;->state:[B

    return-object v0
.end method
