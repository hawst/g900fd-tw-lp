.class public Lcom/nuance/embeddeddialogmanager/EDMInputStateData;
.super Ljava/lang/Object;
.source "EDMInputStateData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/embeddeddialogmanager/EDMInputStateData$1;,
        Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULT_FIELD_ID:Ljava/lang/String; = "dm_main"

.field private static final DEFAULT_LANGUAGE:Ljava/lang/String; = "en-US"


# instance fields
.field private final clientDateTime:Lcom/vlingo/dialog/util/DateTime;

.field private final dialogStateBytes:[B

.field private final eventJson:Ljava/lang/String;

.field private final incomingFieldId:Ljava/lang/String;

.field private final language:Ljava/lang/String;

.field private final listPosition:Ljava/lang/String;

.field private final nluActionJson:Ljava/lang/String;

.field private final viewableListSize:Ljava/lang/Integer;


# direct methods
.method private constructor <init>([BLjava/lang/String;Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "dialogStateBytes"    # [B
    .param p2, "incomingFieldId"    # Ljava/lang/String;
    .param p3, "clientDateTime"    # Lcom/vlingo/dialog/util/DateTime;
    .param p4, "nluJson"    # Ljava/lang/String;
    .param p5, "eventJson"    # Ljava/lang/String;
    .param p6, "language"    # Ljava/lang/String;
    .param p7, "viewableListSize"    # Ljava/lang/Integer;
    .param p8, "listPosition"    # Ljava/lang/String;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->dialogStateBytes:[B

    .line 139
    iput-object p2, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->incomingFieldId:Ljava/lang/String;

    .line 140
    iput-object p3, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 141
    iput-object p4, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->nluActionJson:Ljava/lang/String;

    .line 142
    iput-object p5, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->eventJson:Ljava/lang/String;

    .line 143
    iput-object p6, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->language:Ljava/lang/String;

    .line 144
    iput-object p7, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->viewableListSize:Ljava/lang/Integer;

    .line 145
    iput-object p8, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->listPosition:Ljava/lang/String;

    .line 146
    return-void
.end method

.method synthetic constructor <init>([BLjava/lang/String;Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/nuance/embeddeddialogmanager/EDMInputStateData$1;)V
    .locals 0
    .param p1, "x0"    # [B
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/vlingo/dialog/util/DateTime;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Ljava/lang/String;
    .param p7, "x6"    # Ljava/lang/Integer;
    .param p8, "x7"    # Ljava/lang/String;
    .param p9, "x8"    # Lcom/nuance/embeddeddialogmanager/EDMInputStateData$1;

    .prologue
    .line 15
    invoke-direct/range {p0 .. p8}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;-><init>([BLjava/lang/String;Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getClientDateTime()Lcom/vlingo/dialog/util/DateTime;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    return-object v0
.end method

.method public getDialogStateBytes()[B
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->dialogStateBytes:[B

    return-object v0
.end method

.method public getEventString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->eventJson:Ljava/lang/String;

    return-object v0
.end method

.method public getIncomingFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->incomingFieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getListPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->listPosition:Ljava/lang/String;

    return-object v0
.end method

.method public getNluActionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->nluActionJson:Ljava/lang/String;

    return-object v0
.end method

.method public getViewableListSize()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->viewableListSize:Ljava/lang/Integer;

    return-object v0
.end method
