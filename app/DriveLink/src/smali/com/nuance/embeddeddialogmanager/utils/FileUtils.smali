.class public final Lcom/nuance/embeddeddialogmanager/utils/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# static fields
.field private static final LOGGER:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/nuance/embeddeddialogmanager/utils/FileUtils;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/nuance/embeddeddialogmanager/utils/FileUtils;->LOGGER:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static copyAsset(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    .line 24
    .local v3, "manager":Landroid/content/res/AssetManager;
    const/4 v2, 0x0

    .line 25
    .local v2, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 27
    .local v4, "os":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v3, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 28
    const/4 v6, 0x0

    invoke-virtual {p0, p1, v6}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 30
    const/4 v5, 0x0

    .line 31
    .local v5, "read":I
    const/16 v6, 0x1000

    new-array v0, v6, [B

    .line 33
    .local v0, "bytes":[B
    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 34
    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 37
    .end local v0    # "bytes":[B
    .end local v5    # "read":I
    :catch_0
    move-exception v1

    .line 38
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    if-eqz v2, :cond_0

    .line 42
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 46
    :goto_1
    if-eqz v4, :cond_0

    .line 47
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    :cond_0
    throw v6

    .line 36
    .restart local v0    # "bytes":[B
    .restart local v5    # "read":I
    :cond_1
    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 40
    if-eqz v2, :cond_2

    .line 42
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 46
    :goto_2
    if-eqz v4, :cond_2

    .line 47
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 53
    :cond_2
    return-void

    .line 43
    :catch_1
    move-exception v6

    goto :goto_2

    .end local v0    # "bytes":[B
    .end local v5    # "read":I
    :catch_2
    move-exception v7

    goto :goto_1
.end method
