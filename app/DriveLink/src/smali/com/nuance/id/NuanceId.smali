.class public final Lcom/nuance/id/NuanceId;
.super Ljava/lang/Object;
.source "NuanceId.java"


# static fields
.field public static final INCLUDE_ALL:I = 0xf

.field public static final INCLUDE_ANDROIDID:I = 0x8

.field public static final INCLUDE_IMEI:I = 0x1

.field public static final INCLUDE_MAC:I = 0x4

.field public static final INCLUDE_SERIAL:I = 0x2

.field public static final NULL_ANDROIDID:Ljava/lang/String; = "00000000000000000"

.field public static final NULL_HASH:Ljava/lang/String; = "00000000000000000000000000000000000000000000000000000000000000000"

.field public static final NULL_IMEI:Ljava/lang/String; = "00000000000000000"

.field public static final NULL_MAC_ADDRESS:Ljava/lang/String; = "000000000000"

.field public static final NULL_SERIAL:Ljava/lang/String; = "0000000000000000000000"


# instance fields
.field private context:Landroid/content/Context;

.field private hashingInternal:Lcom/nuance/id/NuanceIdImpl;

.field private id:Ljava/lang/String;

.field private idsUsed:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    const/16 v0, 0xf

    invoke-direct {p0, p1, v0}, Lcom/nuance/id/NuanceId;-><init>(Landroid/content/Context;I)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "idsUsed"    # I

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    .line 90
    new-instance v0, Lcom/nuance/id/NuanceIdImpl;

    invoke-direct {v0}, Lcom/nuance/id/NuanceIdImpl;-><init>()V

    iput-object v0, p0, Lcom/nuance/id/NuanceId;->hashingInternal:Lcom/nuance/id/NuanceIdImpl;

    .line 91
    iput p2, p0, Lcom/nuance/id/NuanceId;->idsUsed:I

    .line 92
    if-nez p2, :cond_0

    .line 93
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Must include at least one device IDs to generate NuanceID"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    return-void
.end method

.method protected static getCompatibilityDeviceProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "prop"    # Ljava/lang/String;

    .prologue
    .line 114
    :try_start_0
    const-class v2, Landroid/os/Build;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 115
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 116
    const-class v2, Landroid/os/Build;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 118
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return-object v2

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getCountOfOnesInBinary(II)[I
    .locals 8
    .param p1, "val"    # I
    .param p2, "mask"    # I

    .prologue
    const/4 v7, 0x1

    .line 291
    const/4 v0, 0x0

    .line 292
    .local v0, "count":I
    const/4 v3, -0x1

    .line 293
    .local v3, "mask_pos":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v5, 0x4

    if-ge v2, v5, :cond_2

    .line 294
    shl-int v1, v7, v2

    .line 295
    .local v1, "current_mask":I
    if-ne v1, p2, :cond_0

    .line 296
    move v3, v0

    .line 298
    :cond_0
    and-int v4, p1, v1

    .line 299
    .local v4, "masked_val":I
    if-eqz v4, :cond_1

    .line 300
    add-int/lit8 v0, v0, 0x1

    .line 293
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 303
    .end local v1    # "current_mask":I
    .end local v4    # "masked_val":I
    :cond_2
    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    aput v0, v5, v6

    aput v3, v5, v7

    return-object v5
.end method

.method private getPlatformIdHash(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p1, "nuanceId"    # Ljava/lang/String;
    .param p2, "mask"    # I

    .prologue
    .line 314
    invoke-direct {p0, p2}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 315
    const/4 v5, 0x0

    .line 325
    :goto_0
    return-object v5

    .line 317
    :cond_0
    iget v5, p0, Lcom/nuance/id/NuanceId;->idsUsed:I

    invoke-direct {p0, v5, p2}, Lcom/nuance/id/NuanceId;->getCountOfOnesInBinary(II)[I

    move-result-object v0

    .line 318
    .local v0, "count_and_mask":[I
    const/4 v5, 0x1

    aget v2, v0, v5

    .line 319
    .local v2, "mask_pos":I
    const/4 v3, 0x0

    .line 320
    .local v3, "offset":I
    invoke-direct {p0}, Lcom/nuance/id/NuanceId;->isAllIncluded()Z

    move-result v5

    if-nez v5, :cond_1

    .line 321
    const/4 v3, 0x1

    .line 323
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v3

    const/4 v6, 0x0

    aget v6, v0, v6

    div-int v1, v5, v6

    .line 324
    .local v1, "hash_len":I
    mul-int v5, v2, v1

    add-int v4, v3, v5

    .line 325
    .local v4, "sub_str_start":I
    add-int v5, v4, v1

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private isAllIncluded()Z
    .locals 2

    .prologue
    .line 202
    iget v0, p0, Lcom/nuance/id/NuanceId;->idsUsed:I

    and-int/lit8 v0, v0, 0xf

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isIdIncluded(I)Z
    .locals 1
    .param p1, "mask"    # I

    .prologue
    .line 198
    iget v0, p0, Lcom/nuance/id/NuanceId;->idsUsed:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected checkPermission(Ljava/lang/String;)Z
    .locals 2
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 98
    iget-object v1, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 101
    :cond_0
    return v0
.end method

.method public equals(Lcom/nuance/id/NuanceId;)Z
    .locals 7
    .param p1, "nid"    # Lcom/nuance/id/NuanceId;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 262
    iget v5, p0, Lcom/nuance/id/NuanceId;->idsUsed:I

    iget v6, p1, Lcom/nuance/id/NuanceId;->idsUsed:I

    and-int v0, v5, v6

    .line 263
    .local v0, "common_ids_used":I
    if-nez v0, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v3

    .line 268
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v5, 0x4

    if-ge v2, v5, :cond_3

    .line 269
    shl-int v1, v4, v2

    .line 270
    .local v1, "current_mask":I
    and-int v5, v0, v1

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v1}, Lcom/nuance/id/NuanceId;->getPlatformIdHash(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/nuance/id/NuanceId;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p1, v6, v1}, Lcom/nuance/id/NuanceId;->getPlatformIdHash(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 268
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v1    # "current_mask":I
    :cond_3
    move v3, v4

    .line 277
    goto :goto_0
.end method

.method protected getAndroidId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 187
    .local v0, "id":Ljava/lang/String;
    iget-object v1, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    :cond_0
    if-nez v0, :cond_1

    .line 191
    const-string/jumbo v0, "00000000000000000"

    .line 193
    :cond_1
    return-object v0
.end method

.method public getAndroidIdHash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getId()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/nuance/id/NuanceId;->getPlatformIdHash(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExternalId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/nuance/id/NuanceId;->hashingInternal:Lcom/nuance/id/NuanceIdImpl;

    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getAndroidId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/id/NuanceIdImpl;->sha1hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getIMEI()Ljava/lang/String;
    .locals 4

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 129
    .local v0, "imeiDeviceId":Ljava/lang/String;
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android.permission.READ_PHONE_STATE"

    invoke-virtual {p0, v2}, Lcom/nuance/id/NuanceId;->checkPermission(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    const-string/jumbo v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 133
    .local v1, "manager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 135
    .end local v1    # "manager":Landroid/telephony/TelephonyManager;
    :cond_0
    if-nez v0, :cond_1

    .line 136
    const-string/jumbo v0, "00000000000000000"

    .line 138
    :cond_1
    return-object v0
.end method

.method public getIMEIHash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/nuance/id/NuanceId;->getPlatformIdHash(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 210
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->id:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 213
    .local v0, "buf":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/nuance/id/NuanceId;->isAllIncluded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 214
    iget v2, p0, Lcom/nuance/id/NuanceId;->idsUsed:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 215
    .local v1, "idUsedStr":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    .end local v1    # "idUsedStr":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 218
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->hashingInternal:Lcom/nuance/id/NuanceIdImpl;

    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getIMEI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/id/NuanceIdImpl;->generateHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    :cond_1
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 221
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->hashingInternal:Lcom/nuance/id/NuanceIdImpl;

    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getSerial()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/id/NuanceIdImpl;->generateHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :cond_2
    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 224
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->hashingInternal:Lcom/nuance/id/NuanceIdImpl;

    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getMac()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/id/NuanceIdImpl;->generateHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_3
    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 227
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->hashingInternal:Lcom/nuance/id/NuanceIdImpl;

    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getAndroidId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/id/NuanceIdImpl;->generateHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/id/NuanceId;->id:Ljava/lang/String;

    .line 231
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    :cond_5
    iget-object v2, p0, Lcom/nuance/id/NuanceId;->id:Ljava/lang/String;

    return-object v2
.end method

.method protected getMac()Ljava/lang/String;
    .locals 5

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 161
    .local v1, "macAddress":Ljava/lang/String;
    iget-object v3, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    if-eqz v3, :cond_0

    const/4 v3, 0x4

    invoke-direct {p0, v3}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "android.permission.ACCESS_WIFI_STATE"

    invoke-virtual {p0, v3}, Lcom/nuance/id/NuanceId;->checkPermission(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 164
    iget-object v3, p0, Lcom/nuance/id/NuanceId;->context:Landroid/content/Context;

    const-string/jumbo v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 166
    .local v2, "manager":Landroid/net/wifi/WifiManager;
    if-eqz v2, :cond_0

    .line 167
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 168
    .local v0, "info":Landroid/net/wifi/WifiInfo;
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    .line 171
    const-string/jumbo v3, ":"

    const-string/jumbo v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    .end local v0    # "info":Landroid/net/wifi/WifiInfo;
    .end local v2    # "manager":Landroid/net/wifi/WifiManager;
    :cond_0
    if-nez v1, :cond_1

    .line 177
    const-string/jumbo v1, "000000000000"

    .line 179
    :cond_1
    return-object v1
.end method

.method public getMacHash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/nuance/id/NuanceId;->getPlatformIdHash(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSerial()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "serial":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/nuance/id/NuanceId;->isIdIncluded(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    const-string/jumbo v1, "SERIAL"

    invoke-static {v1}, Lcom/nuance/id/NuanceId;->getCompatibilityDeviceProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    :cond_0
    if-nez v0, :cond_1

    .line 151
    const-string/jumbo v0, "0000000000000000000000"

    .line 153
    :cond_1
    return-object v0
.end method

.method public getSerialHash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/nuance/id/NuanceId;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/nuance/id/NuanceId;->getPlatformIdHash(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
