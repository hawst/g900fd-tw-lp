.class Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;
.super Ljava/lang/Object;
.source "SampleUiFocusActivity.java"

# interfaces
.implements Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/uifocus/SampleUiFocusActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnUiFocusChangeListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/uifocus/SampleUiFocusActivity;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/nuance/sample/uifocus/SampleUiFocusActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;-><init>(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;)V

    return-void
.end method


# virtual methods
.method public onUiFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/nuance/sample/uifocus/SampleUiFocusActivity;

    # getter for: Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    invoke-static {v0}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->access$0(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 99
    :cond_0
    # getter for: Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->access$1()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onUiFocusChange() focusChange="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 101
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 105
    :pswitch_0
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/nuance/sample/uifocus/SampleUiFocusActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->access$2(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    .line 106
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/nuance/sample/uifocus/SampleUiFocusActivity;

    invoke-virtual {v0}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->finish()V

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
