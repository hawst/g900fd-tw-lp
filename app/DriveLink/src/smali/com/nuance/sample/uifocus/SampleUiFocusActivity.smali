.class public Lcom/nuance/sample/uifocus/SampleUiFocusActivity;
.super Landroid/app/Activity;
.source "SampleUiFocusActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;
    }
.end annotation


# static fields
.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private mContinueBtn:Landroid/widget/Button;

.field private mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    .line 21
    sput-object v0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    return-object v0
.end method

.method static synthetic access$1()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    return-void
.end method

.method private exitApp()V
    .locals 3

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    .line 85
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    invoke-virtual {p0, v0}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 89
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v1, "onBackPressed()"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->exitApp()V

    .line 82
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mContinueBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mContinueBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 65
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    if-eqz v0, :cond_0

    .line 66
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->requestUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 70
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.nuance.sample.ACTION_UIFOCUS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 74
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const v1, 0x7f030078

    invoke-virtual {p0, v1}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->setContentView(I)V

    .line 32
    const v1, 0x7f090252

    invoke-virtual {p0, v1}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mContinueBtn:Landroid/widget/Button;

    .line 33
    const v1, 0x7f090256

    invoke-virtual {p0, v1}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 35
    .local v0, "tv":Landroid/widget/TextView;
    const-string/jumbo v1, "Click by mic - if you want to use CC"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    iget-object v1, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mContinueBtn:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 43
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;-><init>(Lcom/nuance/sample/uifocus/SampleUiFocusActivity;Lcom/nuance/sample/uifocus/SampleUiFocusActivity$OnUiFocusChangeListenerImpl;)V

    iput-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 45
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->waitForUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->finish()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 55
    iget-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 57
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->abandonUiFocus()V

    .line 59
    :cond_0
    return-void
.end method
