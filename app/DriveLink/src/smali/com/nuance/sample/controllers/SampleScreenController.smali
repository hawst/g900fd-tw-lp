.class public Lcom/nuance/sample/controllers/SampleScreenController;
.super Lcom/nuance/sample/controllers/SampleBaseDMController;
.source "SampleScreenController.java"


# instance fields
.field private log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;-><init>()V

    .line 18
    const-class v0, Lcom/nuance/sample/controllers/SampleScreenController;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleScreenController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 15
    return-void
.end method

.method private screenOff()V
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleScreenController;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "Screen off!"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 47
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 3
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 22
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 25
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleScreenController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "executeAction: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 26
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "LPAction"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p0, p1}, Lcom/nuance/sample/controllers/SampleScreenController;->handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z

    .line 29
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 34
    const-string/jumbo v2, "Action"

    invoke-static {p1, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "actionValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 36
    const-string/jumbo v2, "screen:off"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleScreenController;->screenOff()V

    .line 38
    const/4 v1, 0x1

    .line 41
    :cond_0
    return v1
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method
