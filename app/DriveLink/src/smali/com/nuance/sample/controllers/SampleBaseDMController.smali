.class public abstract Lcom/nuance/sample/controllers/SampleBaseDMController;
.super Lcom/vlingo/core/internal/dialogmanager/StateController;
.source "SampleBaseDMController.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;-><init>()V

    return-void
.end method

.method private saySystem(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoText(Ljava/lang/String;)V

    .line 73
    return-void
.end method


# virtual methods
.method protected DLActionLog(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 7
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 261
    const/4 v3, 0x0

    .line 263
    .local v3, "value":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v1, "keys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v4, "Type"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    const-string/jumbo v4, "Query"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    const-string/jumbo v4, "Command"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    const-string/jumbo v4, "name"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    const-string/jumbo v4, "Which"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    const-string/jumbo v4, "PhoneType"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    const-string/jumbo v4, "Message"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    const-string/jumbo v4, "Mode"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    const-string/jumbo v4, "Search"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    const-string/jumbo v4, "Location"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    const-string/jumbo v4, "recipient"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    const-string/jumbo v4, "SpokenForm"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    const-string/jumbo v4, "AppName"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    const-string/jumbo v4, "choices"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    const-string/jumbo v4, "Contact"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    const-string/jumbo v4, "Artist"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    const-string/jumbo v4, "Album"

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    const-string/jumbo v2, "DL_VAC_Action - "

    .line 285
    .local v2, "logString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 293
    const-string/jumbo v4, "DL_VAC_Action"

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    return-void

    .line 285
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 286
    .local v0, "key":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-static {p1, v0, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 288
    if-eqz v3, :cond_0

    .line 289
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method protected cleanup()V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method protected decline()Z
    .locals 2

    .prologue
    .line 237
    const-string/jumbo v0, "DLPhraseSpotter"

    const-string/jumbo v1, "[stop] : SampleBaseDMController - decline()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 239
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 241
    const-string/jumbo v0, "DM_CANCEL"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->cleanup()V

    .line 244
    const/4 v0, 0x0

    return v0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 190
    .line 191
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 192
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method protected getActionConfirmationKeys()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v0, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v1, "yes"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    return-object v0
.end method

.method protected getActionDeclineKeys()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v0, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v1, "cancel"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    const-string/jumbo v1, "no"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    const-string/jumbo v1, "nop"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    return-object v0
.end method

.method protected abstract getActivityClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method protected getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    return-object v0
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 1
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method protected isCalledFromMain()Z
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    .line 92
    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getPreservedState()Ljava/lang/Object;

    move-result-object v1

    .line 91
    check-cast v1, Ljava/util/Map;

    .line 93
    .local v1, "previousState":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;>;"
    if-eqz v1, :cond_0

    .line 94
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 95
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 96
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 97
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 95
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 97
    if-nez v2, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 98
    .local v0, "isCalledFromMain":Z
    :goto_0
    return v0

    .line 93
    .end local v0    # "isCalledFromMain":Z
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected processCommand(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v1, 0x0

    .line 251
    const-string/jumbo v2, "Command"

    invoke-static {p1, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "command":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->PROCESS_COMMAND:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-virtual {v2}, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 253
    const-string/jumbo v2, "next"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "prev"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 254
    invoke-virtual {p0, p1}, Lcom/nuance/sample/controllers/SampleBaseDMController;->resolveConfirmation(Lcom/vlingo/sdk/recognition/VLAction;)Z

    .line 255
    const/4 v1, 0x1

    .line 257
    :cond_0
    return v1
.end method

.method public resolveConfirmation(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 202
    .line 203
    const-string/jumbo v1, "Command"

    const/4 v2, 0x0

    .line 202
    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "command":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getActionConfirmationKeys()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->runFinalActionAndCloseFlow()Z

    move-result v1

    .line 207
    :goto_0
    return v1

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getActionDeclineKeys()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->decline()Z

    move-result v1

    goto :goto_0

    .line 209
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Command "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 210
    const-string/jumbo v3, "isn\'t supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 209
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected abstract runFinalAction()Z
.end method

.method protected runFinalActionAndCloseFlow()Z
    .locals 3

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->runFinalAction()Z

    move-result v0

    .line 229
    .local v0, "result":Z
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 230
    const-string/jumbo v2, "[stop] : SampleBaseDMController - runFinalActionAndCloseFlow()"

    .line 229
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 232
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->finishDialog()V

    .line 233
    return v0
.end method

.method protected show(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method protected showAndSay(I)V
    .locals 2
    .param p1, "prompt"    # I

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->showAndSay(Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method protected showAndSay(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-virtual {p0, p1, p1}, Lcom/nuance/sample/controllers/SampleBaseDMController;->showAndSay(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method protected showAndSay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "prompt"    # Ljava/lang/String;
    .param p2, "tts"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-virtual {p0, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->show(Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleBaseDMController;->saySystem(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method protected varargs startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldId"    # Lcom/nuance/sample/CCFieldIds;
    .param p2, "prompts"    # [Ljava/lang/String;

    .prologue
    .line 102
    invoke-virtual {p0, p1, p2, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method protected startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 14
    .param p1, "fieldId"    # Lcom/nuance/sample/CCFieldIds;
    .param p2, "prompts"    # [Ljava/lang/String;
    .param p3, "tts"    # [Ljava/lang/String;

    .prologue
    .line 108
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 110
    .local v1, "buf":Ljava/lang/StringBuffer;
    move-object/from16 v0, p2

    array-length v12, v0

    const/4 v11, 0x0

    :goto_0
    if-lt v11, v12, :cond_1

    .line 114
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    .line 116
    .local v8, "prompt":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuffer;

    .end local v1    # "buf":Ljava/lang/StringBuffer;
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 118
    .restart local v1    # "buf":Ljava/lang/StringBuffer;
    move-object/from16 v0, p3

    array-length v12, v0

    const/4 v11, 0x0

    :goto_1
    if-lt v11, v12, :cond_2

    .line 122
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 124
    .local v10, "ttsStr":Ljava/lang/String;
    const-string/jumbo v11, "VAC_DRIVELINK"

    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 125
    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, "flowID":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    .line 128
    .local v2, "curFlowID":Ljava/lang/String;
    if-eqz v3, :cond_3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 129
    new-instance v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 130
    .local v4, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {v3, v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 167
    .end local v2    # "curFlowID":Ljava/lang/String;
    .end local v3    # "flowID":Ljava/lang/String;
    .end local v4    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    :goto_2
    return-void

    .line 110
    .end local v8    # "prompt":Ljava/lang/String;
    .end local v10    # "ttsStr":Ljava/lang/String;
    :cond_1
    aget-object v9, p2, v11

    .line 111
    .local v9, "str":Ljava/lang/String;
    const-string/jumbo v13, ". "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 112
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 110
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 118
    .end local v9    # "str":Ljava/lang/String;
    .restart local v8    # "prompt":Ljava/lang/String;
    :cond_2
    aget-object v9, p3, v11

    .line 119
    .restart local v9    # "str":Ljava/lang/String;
    const-string/jumbo v13, ". "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 120
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 132
    .end local v9    # "str":Ljava/lang/String;
    .restart local v2    # "curFlowID":Ljava/lang/String;
    .restart local v3    # "flowID":Ljava/lang/String;
    .restart local v10    # "ttsStr":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v8}, Lcom/nuance/sample/controllers/SampleBaseDMController;->showAndSay(Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v11

    .line 134
    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v12

    .line 133
    invoke-interface {v11, v12}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 135
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->startRecording()V

    goto :goto_2

    .line 141
    .end local v2    # "curFlowID":Ljava/lang/String;
    .end local v3    # "flowID":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->isCalledFromMain()Z

    move-result v7

    .line 142
    .local v7, "isCalledFromMain":Z
    if-eqz v7, :cond_5

    .line 143
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getActivityClass()Ljava/lang/Class;

    move-result-object v5

    .line 146
    .local v5, "getClase":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;>;"
    if-eqz v5, :cond_0

    .line 147
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v6, v11, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 150
    .local v6, "intent":Landroid/content/Intent;
    if-eqz v6, :cond_0

    .line 151
    const/high16 v11, 0x10000000

    invoke-virtual {v6, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 152
    const-string/jumbo v11, "extra_prompt_string"

    invoke-virtual {v6, v11, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string/jumbo v11, "extra_tts_string"

    invoke-virtual {v6, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string/jumbo v11, "extra_field_id"

    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setAbandonInSync(Z)V

    .line 157
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocusInSync()V

    .line 158
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v6}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 162
    .end local v5    # "getClase":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;>;"
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p0, v8, v10}, Lcom/nuance/sample/controllers/SampleBaseDMController;->showAndSay(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v11

    .line 164
    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v12

    .line 163
    invoke-interface {v11, v12}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 165
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->startRecording()V

    goto/16 :goto_2
.end method

.method protected varargs startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V
    .locals 6
    .param p1, "flowID"    # Ljava/lang/String;
    .param p2, "flowParam"    # Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .param p3, "prompts"    # [Ljava/lang/String;

    .prologue
    .line 172
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 173
    const-string/jumbo v0, ""

    .line 174
    .local v0, "prompt":Ljava/lang/String;
    array-length v3, p3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 183
    .end local v0    # "prompt":Ljava/lang/String;
    :cond_0
    invoke-static {p1, p2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 185
    return-void

    .line 174
    .restart local v0    # "prompt":Ljava/lang/String;
    :cond_1
    aget-object v1, p3, v2

    .line 175
    .local v1, "str":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 176
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    iput-object v0, p2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    .line 174
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected startRecording()V
    .locals 2

    .prologue
    .line 80
    const-string/jumbo v0, "DLPhraseSpotter"

    .line 81
    const-string/jumbo v1, "[stop] : SampleBaseDMController - startRecording()"

    .line 80
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 83
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 84
    return-void
.end method
