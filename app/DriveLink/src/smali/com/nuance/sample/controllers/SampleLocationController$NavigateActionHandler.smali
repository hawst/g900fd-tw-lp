.class Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;
.super Ljava/lang/Object;
.source "SampleLocationController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleLocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NavigateActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleLocationController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;)V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;)V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v5, 0x0

    const v6, 0x7f0a01fa

    const/4 v2, 0x0

    .line 333
    const-string/jumbo v3, "SampleLocationController"

    const-string/jumbo v4, "Navigate to, by voice command"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const-string/jumbo v4, "To"

    invoke-static {p1, v4, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/sample/controllers/SampleLocationController;->setTo(Ljava/lang/String;)V

    .line 336
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const-string/jumbo v4, "Which"

    invoke-static {p1, v4, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/sample/controllers/SampleLocationController;->setWhich(Ljava/lang/String;)V

    .line 337
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 338
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const-string/jumbo v4, "Query"

    invoke-static {p1, v4, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/sample/controllers/SampleLocationController;->setTo(Ljava/lang/String;)V

    .line 340
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 341
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const-string/jumbo v4, "Which"

    invoke-static {p1, v4, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/sample/controllers/SampleLocationController;->setTo(Ljava/lang/String;)V

    .line 343
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 345
    const-string/jumbo v3, "DM_MAIN"

    .line 346
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v4

    .line 345
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 346
    if-eqz v3, :cond_1

    .line 348
    const-string/jumbo v3, "SampleLocationController"

    .line 349
    const-string/jumbo v4, "Navigate from Home to Navigation module"

    .line 348
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    const-string/jumbo v3, "DM_LOCATION"

    .line 351
    invoke-static {v3, v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 405
    :goto_0
    return v2

    .line 355
    :cond_0
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getOrdinalIndex(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 357
    const-string/jumbo v3, "SampleLocationController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Navigate to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v5}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 358
    const-string/jumbo v5, " by voice/Ordinal command"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 357
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const-string/jumbo v3, "DM_LIST_ORDINAL"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 366
    :cond_1
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 367
    const-string/jumbo v3, "SampleLocationController"

    const-string/jumbo v4, "Navigation without desti, opening the Map."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v3

    .line 369
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v4

    .line 368
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    goto :goto_0

    .line 372
    :cond_2
    const-string/jumbo v3, "SampleLocationController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Navigate to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v5}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " by voice command"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a02f2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "MyPlace":Ljava/lang/String;
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 378
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "\u30de\u30a4\u30d7\u30ec\u30a4\u30b9"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 379
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "myplace"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 380
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "my place"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 381
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "my\u30d7\u30ec\u30a4\u30b9"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 382
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "my \u30d7\u30ec\u30a4\u30b9"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 383
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 384
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 385
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v3

    .line 386
    invoke-virtual {v3}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 387
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 389
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v3, v6}, Lcom/nuance/sample/controllers/SampleLocationController;->showToast(I)V

    goto/16 :goto_0

    .line 393
    :cond_4
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 394
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v3, "android.settings.MY_PLACE_SETTINGS"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v3

    .line 397
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v3

    .line 399
    const v4, 0xddd5

    .line 398
    invoke-virtual {v3, v1, v4}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 405
    .end local v1    # "i":Landroid/content/Intent;
    :cond_5
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    # invokes: Lcom/nuance/sample/controllers/SampleLocationController;->checkIfAddressAvailableAndRequest()Z
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleLocationController;->access$1(Lcom/nuance/sample/controllers/SampleLocationController;)Z

    move-result v2

    goto/16 :goto_0
.end method
