.class public abstract Lcom/nuance/sample/controllers/SampleContactSearchController;
.super Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;
.source "SampleContactSearchController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;,
        Lcom/nuance/sample/controllers/SampleContactSearchController$ConfirmedTypeActionHandler;,
        Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;,
        Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;,
        Lcom/nuance/sample/controllers/SampleContactSearchController$DeclinedTypeActionHandler;,
        Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;,
        Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;
    }
.end annotation


# static fields
.field private static final CONFIRM_TYPE:Ljava/lang/String; = "confirm"

.field protected static final CONTACT_TYPE:Ljava/lang/String; = "contact"

.field protected static final LOGS_TYPE:Ljava/lang/String; = "logs"

.field protected static final PHONE_TYPE:Ljava/lang/String; = "type"

.field protected static final RECOMMENDATIONS_TYPE:Ljava/lang/String; = "recommendations"

.field private static final TAG:Ljava/lang/String;

.field protected static final TO_PARAMETER:Ljava/lang/String; = "To"

.field protected static final WHICH_PARAMETER:Ljava/lang/String; = "Which"


# instance fields
.field private phoneTypeOrder:I

.field private which:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/controllers/SampleContactSearchController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;-><init>()V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->phoneTypeOrder:I

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/controllers/SampleContactSearchController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->followWithPhoneNumber(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/controllers/SampleContactSearchController;)V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startInboxSearch()V

    return-void
.end method

.method private followWithPhoneNumber(Ljava/lang/String;)V
    .locals 10
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 255
    sget-object v1, Lcom/nuance/sample/controllers/SampleContactSearchController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "followWithPhoneNumber - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v3, "country_detector"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/location/CountryDetector;

    .line 257
    .local v8, "detector":Landroid/location/CountryDetector;
    invoke-virtual {v8}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v1

    invoke-virtual {v1}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 258
    .local v9, "formatPhoneNumber":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 259
    move-object p1, v9

    .line 261
    :cond_0
    sget-object v1, Lcom/nuance/sample/controllers/SampleContactSearchController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "followWithPhoneNumber format - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const-wide/16 v3, 0x0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 264
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v2, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v3, v0

    move-object v5, p1

    move v7, v6

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 265
    .local v2, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 266
    invoke-virtual {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setTo(Ljava/lang/String;)V

    .line 267
    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 268
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 269
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 270
    return-void
.end method

.method private startInboxSearch()V
    .locals 6

    .prologue
    .line 243
    const-string/jumbo v1, "VAC_DRIVELINK"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getTo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getNameIndex(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 245
    const-string/jumbo v1, "DM_LIST_SELECTED"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 248
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a01d3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getTo()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    .line 249
    const-string/jumbo v1, "DM_SMS_INBOX"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method


# virtual methods
.method protected buildPhoneStrings(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 4
    .param p1, "types"    # Ljava/lang/StringBuilder;
    .param p2, "ttsTypes"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 470
    .local p3, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 478
    return-void

    .line 470
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 471
    .local v0, "contactPhone":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 472
    const-string/jumbo v2, ", "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    const-string/jumbo v2, ", "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    :cond_1
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getPhoneTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 476
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getPhoneTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public changePage(Ljava/lang/String;I)V
    .locals 3
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "maxItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/sample/controllers/PageException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v0

    .line 142
    .local v0, "pageSize":I
    const-string/jumbo v1, "next"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    iget v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->currentPage:I

    mul-int/2addr v1, v0

    if-lt v1, p2, :cond_0

    .line 144
    new-instance v1, Lcom/nuance/sample/controllers/CannotMoveToNextPageException;

    invoke-direct {v1}, Lcom/nuance/sample/controllers/CannotMoveToNextPageException;-><init>()V

    throw v1

    .line 146
    :cond_0
    iget v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->currentPage:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->currentPage:I

    .line 148
    :cond_1
    const-string/jumbo v1, "prev"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 149
    iget v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->currentPage:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_2

    .line 150
    new-instance v1, Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;

    invoke-direct {v1}, Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;-><init>()V

    throw v1

    .line 152
    :cond_2
    iget v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->currentPage:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->currentPage:I

    .line 154
    :cond_3
    return-void
.end method

.method protected cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 459
    invoke-super {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->cleanup()V

    .line 460
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 461
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 462
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactMatches(Ljava/util/List;)V

    .line 463
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPage(I)V

    .line 464
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPhoneType(I)V

    .line 465
    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setTo(Ljava/lang/String;)V

    .line 466
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v4, 0x0

    .line 62
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 63
    invoke-virtual {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleContactSearchController;->processCommand(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 101
    :goto_0
    return v4

    .line 67
    :cond_0
    invoke-virtual {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->saveCustomParams(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 68
    const-string/jumbo v5, "Action"

    invoke-static {p1, v5, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 69
    .local v3, "type":Ljava/lang/String;
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 73
    const-string/jumbo v5, "To"

    invoke-static {p1, v5, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 74
    .local v2, "name":Ljava/lang/String;
    const-string/jumbo v5, "logs"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string/jumbo v5, "inbox"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 75
    :cond_1
    const-string/jumbo v3, "logs"

    .line 81
    :cond_2
    :goto_1
    if-nez v3, :cond_9

    .line 82
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "fieldId":Ljava/lang/String;
    const-string/jumbo v5, "cc_dm_dial_home"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "cc_dm_sms_home"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 84
    const-string/jumbo v5, "cc_dm_location_home"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "cc_dm_location_contact_disambig"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 85
    :cond_3
    const-string/jumbo v3, "contact"

    .line 87
    :cond_4
    const-string/jumbo v5, "cc_dm_dial_type"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string/jumbo v5, "cc_dm_sms_type"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string/jumbo v5, "cc_dm_location_type"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 88
    :cond_5
    const-string/jumbo v3, "type"

    .line 90
    :cond_6
    if-nez v3, :cond_9

    .line 91
    sget-object v5, Lcom/nuance/sample/controllers/SampleContactSearchController;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "Empty type."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 76
    .end local v1    # "fieldId":Ljava/lang/String;
    :cond_7
    const-string/jumbo v5, "recommendation"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    const-string/jumbo v5, "suggestion"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 77
    :cond_8
    const-string/jumbo v3, "recommendations"

    goto :goto_1

    .line 96
    :cond_9
    invoke-virtual {p0, v3}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;

    move-result-object v0

    .line 97
    .local v0, "actionHandler":Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;
    if-nez v0, :cond_a

    .line 98
    sget-object v5, Lcom/nuance/sample/controllers/SampleContactSearchController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Handler not found for action: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 101
    :cond_a
    invoke-interface {v0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v4

    goto/16 :goto_0
.end method

.method protected followWithSingleContact()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 200
    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 202
    .local v0, "matchedContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-string/jumbo v5, "VAC_DRIVELINK"

    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 203
    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    if-eqz v0, :cond_6

    .line 209
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getPhoneType()I

    move-result v5

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/nuance/sample/controllers/SampleContactSearchController;->filterPhonesByType(ILjava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 211
    .local v2, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getPhoneTypeOrder()I

    move-result v1

    .line 212
    .local v1, "order":I
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v1, v5, :cond_3

    .line 213
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 218
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v10, :cond_4

    .line 219
    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-static {v5}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 220
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0

    .line 214
    :cond_3
    if-lez v1, :cond_2

    .line 215
    new-array v6, v10, [Lcom/vlingo/core/internal/contacts/ContactData;

    add-int/lit8 v5, v1, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    aput-object v5, v6, v9

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    .line 221
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 222
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactNotFoundString()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :cond_5
    invoke-virtual {p0, v0, v2}, Lcom/nuance/sample/controllers/SampleContactSearchController;->updateContactMatchPhonesAvailable(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 227
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 228
    .local v4, "types":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 229
    .local v3, "ttsTypes":Ljava/lang/StringBuilder;
    invoke-virtual {p0, v4, v3, v2}, Lcom/nuance/sample/controllers/SampleContactSearchController;->buildPhoneStringsForCurrentPage(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 230
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v5

    .line 231
    new-array v6, v11, [Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getMultiplePhoneTypeString()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    .line 232
    new-array v7, v11, [Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getMultiplePhoneTypeString()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    .line 230
    invoke-virtual {p0, v5, v6, v7}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 236
    .end local v1    # "order":I
    .end local v2    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v3    # "ttsTypes":Ljava/lang/StringBuilder;
    .end local v4    # "types":Ljava/lang/StringBuilder;
    :cond_6
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactRequestString()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;
    .locals 5
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 114
    const-string/jumbo v1, "contact"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    new-instance v1, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;

    invoke-direct {v1, p0}, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    .line 127
    :goto_0
    return-object v1

    .line 116
    :cond_0
    const-string/jumbo v1, "type"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    new-instance v1, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;

    invoke-direct {v1, p0}, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    goto :goto_0

    .line 118
    :cond_1
    const-string/jumbo v1, "confirm"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 119
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    const-string/jumbo v2, "Command"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "command":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionConfirmationKeys()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    new-instance v1, Lcom/nuance/sample/controllers/SampleContactSearchController$ConfirmedTypeActionHandler;

    invoke-direct {v1, p0, v4}, Lcom/nuance/sample/controllers/SampleContactSearchController$ConfirmedTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchController;Lcom/nuance/sample/controllers/SampleContactSearchController$ConfirmedTypeActionHandler;)V

    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionDeclineKeys()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 123
    new-instance v1, Lcom/nuance/sample/controllers/SampleContactSearchController$DeclinedTypeActionHandler;

    invoke-direct {v1, p0, v4}, Lcom/nuance/sample/controllers/SampleContactSearchController$DeclinedTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchController;Lcom/nuance/sample/controllers/SampleContactSearchController$DeclinedTypeActionHandler;)V

    goto :goto_0

    .line 125
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Command "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "isn\'t supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 127
    .end local v0    # "command":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getCreateTypeActionHandler()Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;

    move-result-object v1

    goto :goto_0
.end method

.method public getContactMatchesCurrentPage()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v3, "resultMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v2

    .line 175
    .local v2, "pageSize":I
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getPage()I

    move-result v1

    .line 176
    .local v1, "page":I
    add-int/lit8 v4, v1, -0x1

    mul-int v0, v4, v2

    .local v0, "i":I
    :goto_0
    mul-int v4, v1, v2

    if-ge v0, v4, :cond_0

    sget-object v4, Lcom/nuance/sample/controllers/SampleContactSearchController;->contactMatches:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 179
    :cond_0
    return-object v3

    .line 177
    :cond_1
    sget-object v4, Lcom/nuance/sample/controllers/SampleContactSearchController;->contactMatches:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getCreateTypeActionHandler()Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;
    .locals 1

    .prologue
    .line 273
    new-instance v0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;

    invoke-direct {v0, p0}, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    return-object v0
.end method

.method public getPhoneDataCurrentPage()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .local v4, "resultMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v3

    .line 185
    .local v3, "pageSize":I
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getPage()I

    move-result v2

    .line 186
    .local v2, "page":I
    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v0

    .line 187
    .local v0, "allPhoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    add-int/lit8 v5, v2, -0x1

    mul-int v1, v5, v3

    .local v1, "i":I
    :goto_0
    mul-int v5, v2, v3

    if-ge v1, v5, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lt v1, v5, :cond_1

    .line 190
    :cond_0
    return-object v4

    .line 188
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getPhoneTypeOrder()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->phoneTypeOrder:I

    return v0
.end method

.method public getWhich()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->which:Ljava/lang/String;

    return-object v0
.end method

.method protected saveCustomParams(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 0
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 111
    return-void
.end method

.method public setPhoneType(Ljava/lang/String;)V
    .locals 1
    .param p1, "phoneType"    # Ljava/lang/String;

    .prologue
    .line 159
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneType(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPhoneType(I)V

    .line 160
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeOrder(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPhoneTypeOrder(I)V

    .line 161
    return-void
.end method

.method public setPhoneTypeOrder(I)V
    .locals 0
    .param p1, "phoneTypeOrder"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->phoneTypeOrder:I

    .line 169
    return-void
.end method

.method public setWhich(Ljava/lang/String;)V
    .locals 0
    .param p1, "which"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController;->which:Ljava/lang/String;

    .line 138
    return-void
.end method
