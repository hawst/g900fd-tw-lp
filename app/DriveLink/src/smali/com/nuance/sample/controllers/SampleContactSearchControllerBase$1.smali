.class Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;
.super Ljava/lang/Object;
.source "SampleContactSearchControllerBase.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/ContactMatchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startContactSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;


# direct methods
.method constructor <init>(Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 222
    return-void
.end method

.method public onContactMatchResultsUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method public onContactMatchingFailed()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onContactMatchingFinished(Ljava/util/List;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    # getter for: Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->access$0()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "PhoneContactMatcher onContactMatchingFinished contacts: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 117
    .local v4, "context":Landroid/content/Context;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_1

    .line 118
    # getter for: Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->access$0()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "PhoneContactMatcher onContactMatchingFinished not contacts"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const-string/jumbo v11, "VAC_DRIVELINK"

    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 121
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const v14, 0x7f0a0123

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v4, v14, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v11, v12}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->noContactsSearched([Ljava/lang/String;)V

    .line 210
    :goto_0
    return-void

    .line 123
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v12

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v15}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactNotFoundString()I

    move-result v15

    invoke-virtual {v4, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v15}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactRequestString()I

    move-result v15

    invoke-virtual {v4, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_a

    .line 126
    # getter for: Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->access$0()Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "PhoneContactMatcher onContactMatchingFinished one contact"

    invoke-static {v11, v12}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 128
    .local v6, "matchedContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {v6}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 130
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneType()I

    move-result v12

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->filterPhonesByType(ILjava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 131
    .local v7, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 132
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11, v6, v7}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->updateContactMatchPhonesAvailable(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    .line 143
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 145
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v11

    .line 144
    check-cast v11, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 145
    invoke-virtual {v11}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v11

    .line 146
    iget-wide v12, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 145
    invoke-virtual {v11, v4, v12, v13}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v11

    .line 146
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 147
    .local v8, "phoneNumberSize":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v11

    sget-object v12, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    if-ne v11, v12, :cond_3

    const/4 v11, 0x1

    if-le v8, v11, :cond_3

    .line 148
    new-instance v5, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v5}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 149
    .local v5, "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v6, v5, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 150
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0a0137

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v5, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 152
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    const-string/jumbo v12, "DM_DIAL_TYPE"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v5, v13}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 153
    .end local v5    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v11

    sget-object v12, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    if-ne v11, v12, :cond_4

    const/4 v11, 0x1

    if-le v8, v11, :cond_4

    .line 154
    new-instance v5, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v5}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 155
    .restart local v5    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v6, v5, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 156
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0a0137

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v5, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 158
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    const-string/jumbo v12, "DM_SMS_TYPE"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v5, v13}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 160
    .end local v5    # "fp":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v12

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v15}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactNotFoundString()I

    move-result v15

    invoke-virtual {v4, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v15}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactRequestString()I

    move-result v15

    invoke-virtual {v4, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 162
    .end local v8    # "phoneNumberSize":I
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_6

    .line 163
    const/4 v11, 0x0

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-static {v11}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 164
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11, v6}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto/16 :goto_0

    .line 167
    :cond_6
    const-string/jumbo v11, "VAC_DRIVELINK"

    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 168
    const/4 v11, 0x0

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-static {v11}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 169
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11, v6}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto/16 :goto_0

    .line 171
    :cond_7
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    .local v10, "types":Ljava/lang/StringBuilder;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .local v9, "ttsTypes":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v11, v10, v9, v7}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->buildPhoneStringsForCurrentPage(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 174
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_8

    .line 182
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v12

    .line 183
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    .line 184
    new-instance v15, Ljava/lang/StringBuilder;

    const v16, 0x7f0a0053

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    .line 185
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    .line 186
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v15}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getMultiplePhoneTypeString()I

    move-result v15

    invoke-virtual {v4, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    .line 187
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    .line 188
    new-instance v16, Ljava/lang/StringBuilder;

    const v17, 0x7f0a0053

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    .line 189
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getMultiplePhoneTypeString()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    .line 182
    invoke-virtual {v11, v12, v13, v14}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    :cond_8
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 175
    .local v2, "contactPhone":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_9

    .line 176
    const-string/jumbo v12, ", "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    const-string/jumbo v12, ", "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v12, v2}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v12, v2}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 195
    .end local v2    # "contactPhone":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v6    # "matchedContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v7    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v9    # "ttsTypes":Ljava/lang/StringBuilder;
    .end local v10    # "types":Ljava/lang/StringBuilder;
    :cond_a
    const-string/jumbo v11, "VAC_DRIVELINK"

    invoke-static {v11}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 196
    invoke-static/range {p1 .. p1}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatches(Ljava/util/List;)V

    .line 197
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->multiContactsSearched(Ljava/util/List;)V

    goto/16 :goto_0

    .line 199
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 200
    .local v3, "contactsStr":Ljava/lang/StringBuilder;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_c

    .line 206
    invoke-static/range {p1 .. p1}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatches(Ljava/util/List;)V

    .line 207
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v12

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const v15, 0x7f0a0068

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 200
    :cond_c
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 201
    .local v1, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_d

    .line 202
    const-string/jumbo v12, ", "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    :cond_d
    iget-object v12, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
