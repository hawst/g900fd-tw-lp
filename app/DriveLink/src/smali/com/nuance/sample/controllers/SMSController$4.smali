.class Lcom/nuance/sample/controllers/SMSController$4;
.super Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$IntegratedFlowHandler;
.source "SMSController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/controllers/SMSController;->getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SMSController;


# direct methods
.method constructor <init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/controllers/SMSController$4;->this$0:Lcom/nuance/sample/controllers/SMSController;

    .line 515
    invoke-direct {p0, p2}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$IntegratedFlowHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V

    return-void
.end method


# virtual methods
.method protected getFlowID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    const-string/jumbo v0, "DM_SMS_INBOX"

    return-object v0
.end method

.method protected getFlowParams()Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .locals 11

    .prologue
    const v10, 0x7f0a0147

    const/4 v9, 0x0

    const/4 v7, 0x1

    .line 517
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 518
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v3

    .line 519
    invoke-interface {v3}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 521
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v3

    .line 522
    invoke-virtual {v3}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getUnreadMessageCount()I

    move-result v2

    .line 524
    .local v2, "msgCount":I
    if-nez v2, :cond_0

    .line 526
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 528
    new-array v4, v7, [Ljava/lang/Object;

    .line 529
    const-string/jumbo v5, ""

    aput-object v5, v4, v9

    .line 527
    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 525
    iput-object v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 552
    :goto_0
    return-object v1

    .line 531
    :cond_0
    if-ne v2, v7, :cond_1

    .line 533
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 535
    new-array v4, v7, [Ljava/lang/Object;

    .line 536
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 538
    const v6, 0x7f0a0181

    new-array v7, v7, [Ljava/lang/Object;

    .line 539
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    .line 537
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 534
    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 532
    iput-object v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto :goto_0

    .line 542
    :cond_1
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 544
    new-array v4, v7, [Ljava/lang/Object;

    .line 545
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 547
    const v6, 0x7f0a0180

    new-array v7, v7, [Ljava/lang/Object;

    .line 548
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    .line 546
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 543
    invoke-virtual {v3, v10, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 541
    iput-object v3, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto :goto_0
.end method
