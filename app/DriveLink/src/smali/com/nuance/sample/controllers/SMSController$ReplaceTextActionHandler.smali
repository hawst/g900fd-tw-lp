.class final Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;
.super Ljava/lang/Object;
.source "SMSController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SMSController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ReplaceTextActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SMSController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SMSController;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;)V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 234
    iget-object v1, p0, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-static {v1, v6}, Lcom/nuance/sample/controllers/SMSController;->access$0(Lcom/nuance/sample/controllers/SMSController;Z)V

    .line 235
    const-string/jumbo v1, "Message"

    invoke-static {p1, v1, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "newMessage":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 239
    const v5, 0x7f0a0025

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 238
    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    .line 245
    :goto_0
    return v6

    .line 241
    :cond_0
    invoke-static {v0}, Lcom/nuance/sample/controllers/SMSController;->setMessage(Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 243
    const v5, 0x7f0a0024

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 242
    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0
.end method
