.class Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;
.super Ljava/lang/Object;
.source "SampleLocationController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleLocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OfficeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleLocationController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;)V

    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)Lcom/nuance/sample/controllers/SampleLocationController;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    return-object v0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x0

    .line 105
    const-string/jumbo v1, "SampleLocationController"

    const-string/jumbo v2, "Navigate to Work by voice command"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string/jumbo v1, "VAC_DRIVELINK"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 109
    const-string/jumbo v2, "key_settings_office"

    .line 110
    new-instance v3, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;

    invoke-direct {v3, p0}, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;-><init>(Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)V

    .line 108
    invoke-static {v1, v2, v3}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->getMyPlace(Landroid/content/Context;Ljava/lang/String;Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;)V

    .line 179
    :goto_0
    return v4

    .line 169
    :cond_0
    const-string/jumbo v0, "Cambridge, MA"

    .line 170
    .local v0, "address":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const v2, 0x7f0a0619

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(I)V

    goto :goto_0

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const-string/jumbo v2, " "

    const-string/jumbo v3, "+"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->setTo(Ljava/lang/String;)V

    .line 174
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Navigation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    .line 175
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0
.end method
