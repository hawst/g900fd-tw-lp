.class final Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;
.super Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;
.source "SMSController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SMSController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SMSContactTypeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SMSController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SMSController;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 16
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 294
    const-string/jumbo v10, "Which"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 295
    .local v9, "which":Ljava/lang/String;
    if-eqz v9, :cond_2

    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactMatches()Ljava/util/List;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 298
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactMatches()Ljava/util/List;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 300
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactMatches()Ljava/util/List;

    move-result-object v10

    .line 299
    invoke-static {v10, v9}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v10}, Lcom/nuance/sample/controllers/SMSController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 308
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v10}, Lcom/nuance/sample/controllers/SMSController;->followWithSingleContact()V

    .line 400
    :cond_1
    :goto_0
    const/4 v10, 0x0

    :goto_1
    return v10

    .line 309
    :cond_2
    if-eqz v9, :cond_4

    .line 310
    invoke-static {v9}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getOrdinalIndex(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 311
    const/4 v4, 0x0

    .line 314
    .local v4, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v10

    .line 313
    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v4

    .line 315
    if-eqz v4, :cond_3

    .line 317
    const-string/jumbo v10, "Message"

    const/4 v11, 0x0

    .line 316
    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 319
    .local v2, "body":Ljava/lang/String;
    iput-object v2, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 328
    .end local v2    # "body":Ljava/lang/String;
    :cond_3
    const-string/jumbo v10, "DM_LIST_ORDINAL"

    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 329
    const/4 v10, 0x0

    goto :goto_1

    .line 332
    .end local v4    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_4
    const-string/jumbo v10, "To"

    .line 333
    const/4 v11, 0x0

    .line 332
    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    .line 334
    .local v8, "to":Ljava/lang/String;
    const-string/jumbo v10, "Pn"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 335
    .local v7, "pn":Ljava/lang/String;
    const-string/jumbo v10, "Message"

    .line 336
    const/4 v11, 0x0

    .line 335
    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 339
    .restart local v2    # "body":Ljava/lang/String;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SMSController;->setMessage(Ljava/lang/String;)V

    .line 345
    if-eqz v7, :cond_6

    .line 346
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    .line 347
    .local v3, "flowID":Ljava/lang/String;
    const-string/jumbo v10, "DM_SMS_INBOX"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 348
    const/4 v5, 0x0

    .line 349
    .local v5, "phoneNumber":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    # invokes: Lcom/nuance/sample/controllers/SMSController;->removeHyphenForPhonenumber(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v10, v7}, Lcom/nuance/sample/controllers/SMSController;->access$2(Lcom/nuance/sample/controllers/SMSController;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 350
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v10, v5}, Lcom/nuance/sample/controllers/SMSController;->setTo(Ljava/lang/String;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    # invokes: Lcom/nuance/sample/controllers/SMSController;->startInboxSearch()V
    invoke-static {v10}, Lcom/nuance/sample/controllers/SMSController;->access$3(Lcom/nuance/sample/controllers/SMSController;)V

    goto :goto_0

    .line 353
    .end local v5    # "phoneNumber":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    # invokes: Lcom/nuance/sample/controllers/SMSController;->followWithPhoneNumber(Ljava/lang/String;)V
    invoke-static {v10, v7}, Lcom/nuance/sample/controllers/SMSController;->access$4(Lcom/nuance/sample/controllers/SMSController;Ljava/lang/String;)V

    goto :goto_0

    .line 356
    .end local v3    # "flowID":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v10, v8}, Lcom/nuance/sample/controllers/SMSController;->setTo(Ljava/lang/String;)V

    .line 358
    const-string/jumbo v10, "PhoneType"

    const/4 v11, 0x0

    .line 357
    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 359
    .local v6, "phoneType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v10}, Lcom/nuance/sample/controllers/SMSController;->getTo()Ljava/lang/String;

    move-result-object v1

    .line 360
    .local v1, "address":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 361
    if-eqz v6, :cond_7

    .line 362
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    .line 363
    sget-object v11, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 362
    invoke-static {v11, v6}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneType(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/nuance/sample/controllers/SMSController;->setPhoneType(I)V

    .line 365
    :cond_7
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    .line 366
    .restart local v3    # "flowID":Ljava/lang/String;
    const-string/jumbo v10, "DM_SMS_INBOX"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 368
    const-string/jumbo v10, "DM_SMS_INBOX_SEARCH_LIST"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 372
    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    # invokes: Lcom/nuance/sample/controllers/SMSController;->startInboxSearch()V
    invoke-static {v10}, Lcom/nuance/sample/controllers/SMSController;->access$3(Lcom/nuance/sample/controllers/SMSController;)V

    goto/16 :goto_0

    .line 374
    :cond_9
    const/4 v4, 0x0

    .line 377
    .restart local v4    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v10

    .line 376
    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v4

    .line 378
    if-eqz v4, :cond_a

    .line 379
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 47
    :cond_a
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v10}, Lcom/nuance/sample/controllers/SMSController;->startContactSearch()V

    goto/16 :goto_0

    .line 385
    .end local v3    # "flowID":Ljava/lang/String;
    .end local v4    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_b
    const-string/jumbo v10, "VAC_DRIVELINK"

    invoke-static {v10}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 387
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    .line 390
    .restart local v3    # "flowID":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v10

    .line 389
    invoke-static {v3, v10}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 392
    .end local v3    # "flowID":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    .line 393
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v11}, Lcom/nuance/sample/controllers/SMSController;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    .line 394
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v14}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v14

    .line 395
    const v15, 0x7f0a05e8

    .line 394
    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    .line 392
    invoke-virtual {v10, v11, v12}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_0
.end method
