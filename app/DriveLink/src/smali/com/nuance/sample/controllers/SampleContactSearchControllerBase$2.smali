.class Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;
.super Ljava/lang/Object;
.source "SampleContactSearchControllerBase.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startDLContactSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;


# direct methods
.method constructor <init>(Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    .line 325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseRequestContactList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    return-void
.end method

.method public onResponseRequestSearchedContactList(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;>;"
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 329
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 332
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0a0123

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v7}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->noContactsSearched([Ljava/lang/String;)V

    .line 347
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v6, :cond_1

    .line 334
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;

    invoke-static {v2}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 336
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 337
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 339
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v2, v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0

    .line 342
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    invoke-static {p1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContactMatchList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 344
    .local v1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatches(Ljava/util/List;)V

    .line 345
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v2, v1}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->multiContactsSearched(Ljava/util/List;)V

    goto :goto_0
.end method
