.class public Lcom/nuance/sample/controllers/SampleChatbotController;
.super Lcom/nuance/sample/controllers/SampleBaseDMController;
.source "SampleChatbotController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$controllers$SampleChatbotController$NewsCommand:[I


# instance fields
.field private final TAG:Ljava/lang/String;

.field private errorPrompt:Ljava/lang/String;

.field private final handler:Landroid/os/Handler;

.field mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field private final newsManager:Lcom/nuance/sample/news/NewsManager;

.field private prompt:Ljava/lang/String;

.field public wasSuccessful:Z

.field private xmlResponse:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$controllers$SampleChatbotController$NewsCommand()[I
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController;->$SWITCH_TABLE$com$nuance$sample$controllers$SampleChatbotController$NewsCommand:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->values()[Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Cancel:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Down:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Mute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_3
    :try_start_3
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Next:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    :goto_4
    :try_start_4
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->No:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    :goto_5
    :try_start_5
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->None:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_6
    :try_start_6
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Pause:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    :goto_7
    :try_start_7
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Prev:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_8
    :try_start_8
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Read:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :goto_9
    :try_start_9
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Repeat:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    :goto_a
    :try_start_a
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Reset:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_b
    :try_start_b
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Restart:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_c
    :try_start_c
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Resume:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    :goto_d
    :try_start_d
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Start:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    :goto_e
    :try_start_e
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Stop:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_f
    :try_start_f
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Unmute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_10
    :try_start_10
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Up:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_11
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController;->$SWITCH_TABLE$com$nuance$sample$controllers$SampleChatbotController$NewsCommand:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_11

    :catch_1
    move-exception v1

    goto :goto_10

    :catch_2
    move-exception v1

    goto :goto_f

    :catch_3
    move-exception v1

    goto :goto_e

    :catch_4
    move-exception v1

    goto :goto_d

    :catch_5
    move-exception v1

    goto :goto_c

    :catch_6
    move-exception v1

    goto :goto_b

    :catch_7
    move-exception v1

    goto :goto_a

    :catch_8
    move-exception v1

    goto :goto_9

    :catch_9
    move-exception v1

    goto :goto_8

    :catch_a
    move-exception v1

    goto :goto_7

    :catch_b
    move-exception v1

    goto/16 :goto_6

    :catch_c
    move-exception v1

    goto/16 :goto_5

    :catch_d
    move-exception v1

    goto/16 :goto_4

    :catch_e
    move-exception v1

    goto/16 :goto_3

    :catch_f
    move-exception v1

    goto/16 :goto_2

    :catch_10
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->wasSuccessful:Z

    .line 44
    const-string/jumbo v0, "SampleChatbotController"

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->TAG:Ljava/lang/String;

    .line 45
    invoke-static {}, Lcom/nuance/sample/news/NewsManager;->getInstance()Lcom/nuance/sample/news/NewsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->newsManager:Lcom/nuance/sample/news/NewsManager;

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->handler:Landroid/os/Handler;

    .line 48
    iput-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->xmlResponse:Ljava/lang/String;

    .line 49
    iput-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    .line 50
    iput-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    .line 214
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$1;

    invoke-direct {v0, p0}, Lcom/nuance/sample/controllers/SampleChatbotController$1;-><init>(Lcom/nuance/sample/controllers/SampleChatbotController;)V

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 53
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->newsManager:Lcom/nuance/sample/news/NewsManager;

    invoke-virtual {v0, p0}, Lcom/nuance/sample/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 54
    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/nuance/sample/news/NewsManager;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->newsManager:Lcom/nuance/sample/news/NewsManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lcom/nuance/sample/controllers/SampleChatbotController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/nuance/sample/controllers/SampleChatbotController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleChatbotController;->playOutNews(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$6(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    return-object v0
.end method

.method private getNewsCommand(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
    .locals 1
    .param p1, "xmlFragment"    # Ljava/lang/String;

    .prologue
    .line 336
    if-eqz p1, :cond_f

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 339
    const-string/jumbo v0, "news:read"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Read:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 380
    :goto_0
    return-object v0

    .line 341
    :cond_0
    const-string/jumbo v0, "prev"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Prev:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 343
    :cond_1
    const-string/jumbo v0, "next"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Next:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 345
    :cond_2
    const-string/jumbo v0, "stop"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 346
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Stop:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 347
    :cond_3
    const-string/jumbo v0, "cancel"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 348
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Cancel:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 349
    :cond_4
    const-string/jumbo v0, "no"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 350
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->No:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 351
    :cond_5
    const-string/jumbo v0, "volumeup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 352
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Up:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 353
    :cond_6
    const-string/jumbo v0, "volumedown"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 354
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Down:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 355
    :cond_7
    const-string/jumbo v0, "unmute"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 356
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Unmute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 357
    :cond_8
    const-string/jumbo v0, "mute"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 358
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Mute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 359
    :cond_9
    const-string/jumbo v0, "repeat"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 360
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Repeat:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto :goto_0

    .line 361
    :cond_a
    const-string/jumbo v0, "reset"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 362
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Reset:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto/16 :goto_0

    .line 363
    :cond_b
    const-string/jumbo v0, "start"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 364
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Start:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto/16 :goto_0

    .line 365
    :cond_c
    const-string/jumbo v0, "restart"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 366
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Restart:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto/16 :goto_0

    .line 367
    :cond_d
    const-string/jumbo v0, "resume"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 368
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Resume:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto/16 :goto_0

    .line 369
    :cond_e
    const-string/jumbo v0, "pause"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 370
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Pause:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto/16 :goto_0

    .line 380
    :cond_f
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->None:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    goto/16 :goto_0
.end method

.method private isNewsActionType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xmlFragment"    # Ljava/lang/String;

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleChatbotController;->isNewsActionTypeFaked(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isNewsActionTypeFaked(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xmlFragment"    # Ljava/lang/String;

    .prologue
    .line 295
    if-eqz p1, :cond_0

    .line 296
    const-string/jumbo v0, "<custom_action><action_type>news:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    const/4 v0, 0x1

    .line 298
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playOutNews(Ljava/lang/String;)V
    .locals 4
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 258
    if-eqz p1, :cond_0

    .line 259
    const-string/jumbo v2, "\u00ad"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 260
    :cond_0
    invoke-static {p1}, Lcom/nuance/sample/util/TtsUtil;->getTtsArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 262
    .local v1, "ttsStrings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS_MULTI:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 263
    const-class v3, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    .line 261
    invoke-virtual {p0, v2, v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    .line 264
    .local v0, "action":Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;
    if-nez v0, :cond_1

    .line 265
    const-string/jumbo v2, "VoiceNews"

    const-string/jumbo v3, "did not get PLAY_OUT_NEWS_MULTI action, was it registered"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :cond_1
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->tts(Ljava/util/List;)V

    .line 269
    invoke-virtual {v0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->queue()V

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_2
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 396
    invoke-super {p0, p1}, Lcom/nuance/sample/controllers/SampleBaseDMController;->actionFail(Ljava/lang/String;)V

    .line 397
    return-void
.end method

.method public actionSuccess()V
    .locals 2

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 386
    invoke-super {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;->actionSuccess()V

    .line 387
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->newsManager:Lcom/nuance/sample/news/NewsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/sample/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 389
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->wasSuccessful:Z

    .line 391
    return-void
.end method

.method public doNewsCommand(Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;)Z
    .locals 14
    .param p1, "command"    # Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .prologue
    const/4 v13, 0x0

    const v12, 0x7f0a07d0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 83
    invoke-static {}, Lcom/nuance/sample/news/NewsManager;->getInstance()Lcom/nuance/sample/news/NewsManager;

    move-result-object v4

    .line 84
    .local v4, "nm":Lcom/nuance/sample/news/NewsManager;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    .line 85
    .local v3, "listener":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 86
    .local v0, "context":Landroid/content/Context;
    const/4 v2, 0x0

    .line 88
    .local v2, "item":Lcom/nuance/sample/news/NewsItem;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 89
    .local v5, "resources":Landroid/content/res/Resources;
    const v9, 0x7f0a07d3

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    .line 91
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->isAvailable()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 92
    const-string/jumbo v9, "SampleChatbotController"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "doNewsCommand command="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-static {}, Lcom/nuance/sample/controllers/SampleChatbotController;->$SWITCH_TABLE$com$nuance$sample$controllers$SampleChatbotController$NewsCommand()[I

    move-result-object v9

    invoke-virtual {p1}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 182
    :cond_0
    :goto_0
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/nuance/sample/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 183
    invoke-virtual {v2}, Lcom/nuance/sample/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 184
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 185
    iget-object v7, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    iget-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    invoke-interface {v3, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :goto_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowChatbotWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v7, v9, v13, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 191
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 192
    invoke-virtual {v2}, Lcom/nuance/sample/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v3, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 195
    :cond_1
    invoke-virtual {v2}, Lcom/nuance/sample/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/nuance/sample/controllers/SampleChatbotController;->playOutNews(Ljava/lang/String;)V

    move v7, v8

    .line 211
    :cond_2
    :goto_2
    return v7

    .line 95
    :pswitch_0
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->getCurrentNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    .line 97
    iput-object v13, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    .line 98
    if-eqz v2, :cond_0

    .line 99
    move-object v6, v5

    .line 100
    .local v6, "rsc":Landroid/content/res/Resources;
    invoke-virtual {v2}, Lcom/nuance/sample/news/NewsItem;->getNewsCP()I

    move-result v9

    if-ne v9, v8, :cond_3

    .line 102
    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v8, [Ljava/lang/Object;

    .line 103
    const v11, 0x7f0a07df

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    .line 101
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    goto :goto_0

    .line 106
    :cond_3
    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v8, [Ljava/lang/Object;

    .line 107
    const v11, 0x7f0a07e0

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    .line 105
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    goto :goto_0

    .line 112
    .end local v6    # "rsc":Landroid/content/res/Resources;
    :pswitch_1
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->getNextNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    .line 113
    const v9, 0x7f0a07d1

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    .line 114
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->hasCachedNews()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 116
    const v9, 0x7f0a07d5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 115
    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 120
    :pswitch_2
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->getPrevNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    .line 121
    const v9, 0x7f0a07d2

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    .line 122
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->hasCachedNews()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 124
    const v9, 0x7f0a07d4

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 123
    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 131
    :pswitch_3
    const v8, 0x7f0a07d6

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    .line 132
    iget-object v8, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    iget-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    invoke-interface {v3, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 136
    :pswitch_4
    const/4 v2, 0x0

    .line 137
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementCurrentStreamVolume()Z

    move-result v9

    if-nez v9, :cond_4

    .line 138
    const-string/jumbo v9, "Volume has alredy set maximum"

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 141
    :cond_4
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->getCurrentNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    .line 142
    goto/16 :goto_0

    .line 145
    :pswitch_5
    const/4 v2, 0x0

    .line 146
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->decrementCurrentStreamVolume()Z

    move-result v9

    if-nez v9, :cond_5

    .line 147
    const-string/jumbo v9, "Volume has alredy set minimum"

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 150
    :cond_5
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->getCurrentNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    .line 151
    goto/16 :goto_0

    .line 154
    :pswitch_6
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->muteCurrentStream()Z

    move-result v8

    if-nez v8, :cond_2

    .line 155
    const-string/jumbo v8, "Volume is already muted"

    iput-object v8, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_2

    .line 160
    :pswitch_7
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->unmuteCurrentStream()Z

    move-result v8

    if-nez v8, :cond_2

    .line 161
    const-string/jumbo v8, "Volume is already unmuted"

    iput-object v8, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_2

    .line 166
    :pswitch_8
    const-string/jumbo v9, "Chatbot read first news"

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    .line 167
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->getFirstNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    .line 168
    goto/16 :goto_0

    .line 174
    :pswitch_9
    const-string/jumbo v9, "Chatbot repeat news"

    iput-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    .line 175
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->getCurrentNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    .line 176
    goto/16 :goto_0

    .line 187
    :cond_6
    iget-object v7, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;

    invoke-interface {v3, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoText(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 198
    :cond_7
    if-nez v2, :cond_8

    iget-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    if-eqz v9, :cond_8

    .line 199
    iget-object v8, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    iget-object v9, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    invoke-interface {v3, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 201
    :cond_8
    invoke-virtual {v4}, Lcom/nuance/sample/news/NewsManager;->fetchNews()Z

    move v7, v8

    .line 202
    goto/16 :goto_2

    .line 206
    :cond_9
    const-string/jumbo v8, "SampleChatbotController"

    const-string/jumbo v9, "NewsManager not available"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const v8, 0x7f0a07e1

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 208
    .local v1, "errorStr":Ljava/lang/String;
    invoke-interface {v3, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 61
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v3

    const-string/jumbo v4, "chatbot"

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 63
    const-string/jumbo v3, "xmlresponse"

    invoke-static {p1, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->xmlResponse:Ljava/lang/String;

    .line 66
    const-string/jumbo v3, "SampleChatbotController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "xmlresponse=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->xmlResponse:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string/jumbo v3, "Command"

    invoke-static {p1, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "type":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 70
    invoke-direct {p0, v1}, Lcom/nuance/sample/controllers/SampleChatbotController;->getNewsCommand(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    move-result-object v0

    .line 71
    .local v0, "command":Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleChatbotController;->doNewsCommand(Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;)Z

    move-result v2

    .line 78
    .end local v0    # "command":Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
    :cond_0
    :goto_0
    return v2

    .line 73
    :cond_1
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->xmlResponse:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->isNewsActionType(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->xmlResponse:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->getNewsCommand(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    move-result-object v0

    .line 75
    .restart local v0    # "command":Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleChatbotController;->doNewsCommand(Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;)Z

    move-result v2

    goto :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 511
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWasSuccessful()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->wasSuccessful:Z

    return v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "unused"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 493
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 494
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.VolumeUp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 495
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Up:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleChatbotController;->doNewsCommand(Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;)Z

    .line 500
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.VolumeDown"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Down:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleChatbotController;->doNewsCommand(Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;)Z

    goto :goto_0
.end method

.method public onRequestFailed()V
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->newsManager:Lcom/nuance/sample/news/NewsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/sample/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 409
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 411
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 0
    .param p1, "prompt"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 506
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 416
    return-void
.end method

.method public onResponseReceived()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->newsManager:Lcom/nuance/sample/news/NewsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/sample/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 421
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleChatbotController;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/nuance/sample/controllers/SampleChatbotController$2;

    invoke-direct {v1, p0}, Lcom/nuance/sample/controllers/SampleChatbotController$2;-><init>(Lcom/nuance/sample/controllers/SampleChatbotController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 487
    return-void
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 517
    const/4 v0, 0x0

    return v0
.end method
