.class public Lcom/nuance/sample/controllers/SampleMusicController;
.super Lcom/nuance/sample/controllers/SampleBaseDMController;
.source "SampleMusicController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field protected static final ALBUM_PARAM:Ljava/lang/String; = "Album"

.field protected static final ARTIST_PARAM:Ljava/lang/String; = "Artist"

.field private static final MUSIC_TYPES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/nuance/sample/music/PlayMusicType;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PLAYLIST_PARAM:Ljava/lang/String; = "Playlist"

.field protected static final QUERY_PARAM:Ljava/lang/String; = "Query"

.field protected static final SCROLL_PARAM:Ljava/lang/String; = "Scroll"

.field private static final TAG:Ljava/lang/String;

.field protected static final TITLE_PARAM:Ljava/lang/String; = "Title"

.field protected static final TYPE_PARAM:Ljava/lang/String; = "Type"

.field protected static final WHICH_PARAM:Ljava/lang/String; = "Which"


# instance fields
.field private album:Ljava/lang/String;

.field private artist:Ljava/lang/String;

.field private playlist:Ljava/lang/String;

.field private query:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const-class v1, Lcom/nuance/sample/controllers/SampleMusicController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 64
    .local v0, "musicTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;>;"
    const-string/jumbo v1, "next"

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string/jumbo v1, "prev"

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string/jumbo v1, "pause"

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const-string/jumbo v1, "resume"

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sput-object v0, Lcom/nuance/sample/controllers/SampleMusicController;->MUSIC_TYPES:Ljava/util/Map;

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;-><init>()V

    .line 71
    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->query:Ljava/lang/String;

    .line 72
    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->playlist:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->title:Ljava/lang/String;

    .line 74
    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->artist:Ljava/lang/String;

    .line 75
    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->album:Ljava/lang/String;

    .line 46
    return-void
.end method

.method private addParamToQuery(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/StringBuilder;
    .param p2, "param"    # Ljava/lang/String;

    .prologue
    .line 265
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 266
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 267
    const-string/jumbo v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_1
    return-void
.end method

.method private extractSearchMusicParams(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 2
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 242
    const-string/jumbo v0, "Query"

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->query:Ljava/lang/String;

    .line 243
    const-string/jumbo v0, "Playlist"

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->playlist:Ljava/lang/String;

    .line 245
    const-string/jumbo v0, "Title"

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->title:Ljava/lang/String;

    .line 246
    const-string/jumbo v0, "Artist"

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->artist:Ljava/lang/String;

    .line 247
    const-string/jumbo v0, "Album"

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleMusicController;->album:Ljava/lang/String;

    .line 248
    return-void
.end method

.method private getMusicItemsWithPage(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;
    .locals 8
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443
    invoke-static {p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v0

    .line 444
    .local v0, "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ListControlData;->getCurrentPage()I

    move-result v5

    .line 445
    .local v5, "page":I
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ListControlData;->getPageSize()I

    move-result v6

    .line 446
    .local v6, "pageSize":I
    invoke-static {p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v3

    .line 447
    .local v3, "musicDetailsStoredList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 448
    .local v4, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    add-int/lit8 v7, v5, -0x1

    mul-int v1, v7, v6

    .local v1, "i":I
    :goto_0
    mul-int v7, v5, v6

    if-ge v1, v7, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-lt v1, v7, :cond_1

    .line 452
    :cond_0
    return-object v4

    .line 449
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/nuance/sample/music/MusicDetails;

    .line 450
    .local v2, "music":Lcom/nuance/sample/music/MusicDetails;
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getMusicListBySearchParams()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/nuance/sample/controllers/SampleMusicController;->getMusicListBySearchParams(Z)Ljava/util/List;

    move-result-object v0

    .line 289
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v0, :cond_0

    .line 290
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 294
    .end local v0    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :goto_0
    return-object v0

    .restart local v0    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/nuance/sample/controllers/SampleMusicController;->getMusicListBySearchParams(Z)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private getMusicListBySearchParams(Z)Ljava/util/List;
    .locals 6
    .param p1, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 312
    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->query:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    .line 314
    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->query:Ljava/lang/String;

    .line 313
    invoke-static {v3, v4, p1}, Lcom/nuance/sample/music/SearchMusic;->getGenericList(Landroid/content/Context;Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v3

    .line 314
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    .line 355
    :goto_0
    return-object v3

    .line 317
    :cond_0
    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->playlist:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 318
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    .line 319
    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->playlist:Ljava/lang/String;

    .line 318
    invoke-static {v3, v4, p1}, Lcom/nuance/sample/music/SearchMusic;->getPlaylistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .line 322
    :cond_1
    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->title:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 323
    .local v2, "titleNotEmpty":Z
    :goto_1
    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->artist:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 324
    .local v1, "artistNotEmpty":Z
    :goto_2
    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->album:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v0, v3

    .line 325
    .local v0, "albumNotEmpty":Z
    :goto_3
    if-eqz p1, :cond_7

    .line 326
    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    .line 327
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    .line 328
    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->title:Ljava/lang/String;

    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->artist:Ljava/lang/String;

    .line 327
    invoke-static {v3, v4, v5}, Lcom/nuance/sample/music/SearchMusic;->byTitleAndArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .end local v0    # "albumNotEmpty":Z
    .end local v1    # "artistNotEmpty":Z
    .end local v2    # "titleNotEmpty":Z
    :cond_2
    move v2, v4

    .line 322
    goto :goto_1

    .restart local v2    # "titleNotEmpty":Z
    :cond_3
    move v1, v4

    .line 323
    goto :goto_2

    .restart local v1    # "artistNotEmpty":Z
    :cond_4
    move v0, v4

    .line 324
    goto :goto_3

    .line 330
    .restart local v0    # "albumNotEmpty":Z
    :cond_5
    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    .line 331
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    .line 332
    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->title:Ljava/lang/String;

    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->album:Ljava/lang/String;

    .line 331
    invoke-static {v3, v4, v5}, Lcom/nuance/sample/music/SearchMusic;->byTitleAndAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .line 334
    :cond_6
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 335
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    .line 336
    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->album:Ljava/lang/String;

    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleMusicController;->artist:Ljava/lang/String;

    .line 335
    invoke-static {v3, v4, v5}, Lcom/nuance/sample/music/SearchMusic;->byArtistAndAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .line 340
    :cond_7
    if-eqz v2, :cond_8

    .line 341
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    .line 342
    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->title:Ljava/lang/String;

    .line 341
    invoke-static {v3, v4, p1}, Lcom/nuance/sample/music/SearchMusic;->getTitleList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_0

    .line 345
    :cond_8
    if-eqz v0, :cond_9

    .line 346
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    .line 347
    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->album:Ljava/lang/String;

    .line 346
    invoke-static {v3, v4, p1}, Lcom/nuance/sample/music/SearchMusic;->getAlbumList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_0

    .line 350
    :cond_9
    if-eqz v1, :cond_a

    .line 352
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleMusicController;->artist:Ljava/lang/String;

    .line 351
    invoke-static {v3, v4, p1}, Lcom/nuance/sample/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_0

    .line 355
    :cond_a
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_0
.end method

.method private getQuery()Ljava/lang/String;
    .locals 2

    .prologue
    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleMusicController;->query:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleMusicController;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleMusicController;->title:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/controllers/SampleMusicController;->addParamToQuery(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleMusicController;->album:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/controllers/SampleMusicController;->addParamToQuery(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 258
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleMusicController;->artist:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/controllers/SampleMusicController;->addParamToQuery(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 259
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleMusicController;->playlist:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/controllers/SampleMusicController;->addParamToQuery(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 261
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private isAnyMusic()Z
    .locals 2

    .prologue
    .line 236
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 237
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 238
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/nuance/sample/music/SearchMusic;->isAnyMusic(Landroid/content/Context;)Z

    move-result v1

    return v1
.end method

.method private playByName(Ljava/lang/String;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 376
    const/4 v5, 0x4

    new-array v3, v5, [Lcom/nuance/sample/music/PlayMusicType;

    sget-object v5, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v5, v3, v4

    const/4 v5, 0x1

    .line 377
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v6, v3, v5

    const/4 v5, 0x2

    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v6, v3, v5

    const/4 v5, 0x3

    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v6, v3, v5

    .line 378
    .local v3, "musicTypes":[Lcom/nuance/sample/music/PlayMusicType;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 379
    .local v0, "activityContext":Landroid/content/Context;
    array-length v5, v3

    :goto_0
    if-lt v4, v5, :cond_0

    .line 389
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 390
    const v5, 0x7f0a003a

    .line 389
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSay(Ljava/lang/String;)V

    .line 391
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    .line 392
    sget-object v5, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_HOME:Lcom/nuance/sample/CCFieldIds;

    .line 393
    invoke-virtual {v5}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 392
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    .line 391
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 394
    :goto_1
    return-void

    .line 379
    :cond_0
    aget-object v2, v3, v4

    .line 380
    .local v2, "musicType":Lcom/nuance/sample/music/PlayMusicType;
    invoke-virtual {v2, v0, p1}, Lcom/nuance/sample/music/PlayMusicType;->getMusicList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 382
    .local v1, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    invoke-static {v1}, Lcom/nuance/sample/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 383
    invoke-direct {p0, v1, v2, p1}, Lcom/nuance/sample/controllers/SampleMusicController;->playMusicList(Ljava/util/List;Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_1

    .line 379
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private playFromDetails(Lcom/nuance/sample/music/MusicDetails;)V
    .locals 2
    .param p1, "md"    # Lcom/nuance/sample/music/MusicDetails;

    .prologue
    .line 275
    invoke-virtual {p1}, Lcom/nuance/sample/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    invoke-virtual {p1}, Lcom/nuance/sample/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 277
    .local v0, "name":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->playByName(Ljava/lang/String;)V

    .line 278
    return-void

    .line 276
    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/nuance/sample/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private playMusicList(Ljava/util/List;Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;)V
    .locals 9
    .param p2, "musicType"    # Lcom/nuance/sample/music/PlayMusicType;
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;",
            "Lcom/nuance/sample/music/PlayMusicType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 398
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    sget-object v6, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    .line 399
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "execAction::playing music list. List size is: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 400
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 399
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 398
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 402
    .local v2, "listSize":I
    new-array v3, v2, [J

    .line 403
    .local v3, "mSongList":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 406
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_playing_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 407
    aput-object p3, v7, v8

    .line 406
    invoke-static {v6, v7}, Lcom/nuance/sample/controllers/SampleMusicController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 408
    .local v5, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 410
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v6, "com.sec.android.app.music.intent.action.PLAY_VIA"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 412
    .local v4, "musicIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 414
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    .line 413
    invoke-static {p2, p3, p0, v6}, Lcom/nuance/sample/music/MusicLauncher;->launchMusicPlayer(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 418
    :goto_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v6

    const-string/jumbo v7, "music-play-list"

    invoke-interface {v6, v7}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 421
    invoke-virtual {p2}, Lcom/nuance/sample/music/PlayMusicType;->toString()Ljava/lang/String;

    move-result-object v6

    .line 420
    invoke-static {v6, p3, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolved(Ljava/lang/String;Ljava/lang/String;[J)Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    move-result-object v0

    .line 422
    .local v0, "acceptedText":Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
    if-eqz v0, :cond_0

    .line 423
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 425
    :cond_0
    return-void

    .line 404
    .end local v0    # "acceptedText":Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
    .end local v4    # "musicIntent":Landroid/content/Intent;
    .end local v5    # "text":Ljava/lang/String;
    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/nuance/sample/music/MusicDetails;

    invoke-virtual {v6}, Lcom/nuance/sample/music/MusicDetails;->getSongId()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v1

    .line 403
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 416
    .restart local v4    # "musicIntent":Landroid/content/Intent;
    .restart local v5    # "text":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    invoke-static {v3, p0, v6}, Lcom/nuance/sample/music/MusicLauncher;->launchList([JLcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    goto :goto_1
.end method

.method private showAndSayCommand(Ljava/lang/String;)V
    .locals 4
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0838

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 429
    aput-object p1, v2, v3

    .line 428
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSay(Ljava/lang/String;)V

    .line 430
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x0

    .line 93
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 94
    sget-object v6, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    .line 95
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "SampleMusicController handling action="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 96
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 95
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 94
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string/jumbo v6, "Command"

    invoke-static {p1, v6, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "command":Ljava/lang/String;
    const-string/jumbo v6, "Scroll"

    invoke-static {p1, v6, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, "scroll":Ljava/lang/String;
    const-string/jumbo v6, "true"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 103
    invoke-virtual {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleMusicController;->executePlayAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v5

    .line 135
    :cond_0
    :goto_0
    return v5

    .line 106
    :cond_1
    invoke-static {v0}, Lcom/nuance/sample/music/VolumeAction;->getActionByKey(Ljava/lang/String;)Lcom/nuance/sample/music/VolumeAction;

    move-result-object v4

    .line 107
    .local v4, "volumeAction":Lcom/nuance/sample/music/VolumeAction;
    if-eqz v4, :cond_2

    .line 108
    invoke-direct {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSayCommand(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v4}, Lcom/nuance/sample/music/VolumeAction;->execute()Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "errorMsg":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 111
    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSay(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    .end local v1    # "errorMsg":Ljava/lang/String;
    :cond_2
    sget-object v6, Lcom/nuance/sample/controllers/SampleMusicController;->MUSIC_TYPES:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/nuance/sample/music/PlayMusicType;

    .line 117
    .local v3, "type":Lcom/nuance/sample/music/PlayMusicType;
    if-eqz v3, :cond_3

    .line 118
    invoke-direct {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSayCommand(Ljava/lang/String;)V

    .line 119
    const-string/jumbo v6, " "

    invoke-static {v3, v6, p0, p2}, Lcom/nuance/sample/music/MusicLauncher;->launchMusicPlayer(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    goto :goto_0

    .line 124
    :cond_3
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 125
    invoke-direct {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSayCommand(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 127
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Samsung TBD music "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " command"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 128
    const/4 v8, 0x1

    .line 126
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    .line 128
    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 130
    :cond_4
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Parameter Command is null or doesn\'t supported by "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 132
    sget-object v7, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " controller"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 130
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public executePlayAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 13
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 140
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 142
    const-string/jumbo v9, "Command"

    .line 143
    const/4 v10, 0x0

    .line 142
    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "command":Ljava/lang/String;
    const-string/jumbo v9, "next"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string/jumbo v9, "prev"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 145
    :cond_0
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v2

    .line 146
    .local v2, "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    const/4 v4, 0x0

    .line 147
    .local v4, "errorPageChange":Ljava/lang/String;
    const-string/jumbo v9, "next"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 149
    :try_start_0
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :goto_0
    if-eqz v4, :cond_1

    .line 161
    invoke-virtual {p0, v4}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSay(Ljava/lang/String;)V

    .line 163
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->promptForDisambiguation()V

    .line 165
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v11, 0x0

    invoke-direct {p0, p2}, Lcom/nuance/sample/controllers/SampleMusicController;->getMusicItemsWithPage(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v9, v10, v11, v12, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 166
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v9

    const-string/jumbo v10, "music-play-disambiguate"

    invoke-interface {v9, v10}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->startRecording()V

    .line 168
    const/4 v9, 0x0

    .line 232
    .end local v2    # "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    .end local v4    # "errorPageChange":Ljava/lang/String;
    :goto_1
    return v9

    .line 150
    .restart local v2    # "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    .restart local v4    # "errorPageChange":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 151
    .local v3, "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    const-string/jumbo v4, "There are no more items. "

    .line 153
    goto :goto_0

    .line 155
    .end local v3    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V
    :try_end_1
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 156
    :catch_1
    move-exception v3

    .line 157
    .restart local v3    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    const-string/jumbo v4, "There are no more items. "

    goto :goto_0

    .line 171
    .end local v2    # "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    .end local v3    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    .end local v4    # "errorPageChange":Ljava/lang/String;
    :cond_3
    sget-object v9, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    .line 172
    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "SampleMusicController handling action="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 173
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 172
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 171
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleMusicController;->extractSearchMusicParams(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 175
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getQuery()Ljava/lang/String;

    move-result-object v7

    .line 176
    .local v7, "name":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v9

    .line 177
    invoke-interface {v9}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 179
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v9, "Type"

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 178
    invoke-static {v1, v9, v7}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requested(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->isAnyMusic()Z

    move-result v9

    if-nez v9, :cond_4

    .line 184
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 185
    const v10, 0x7f0a0117

    .line 184
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSay(Ljava/lang/String;)V

    .line 186
    const/4 v9, 0x0

    goto :goto_1

    .line 191
    :cond_4
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 192
    sget-object v9, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    const-string/jumbo v10, " "

    .line 193
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v11

    .line 192
    invoke-static {v9, v10, p0, v11}, Lcom/nuance/sample/music/MusicLauncher;->launchMusicPlayer(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 194
    const/4 v9, 0x0

    goto :goto_1

    .line 201
    :cond_5
    const-string/jumbo v9, "Which"

    const/4 v10, 0x0

    .line 200
    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    .line 202
    .local v8, "ordinal":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 203
    invoke-static {p2, v8}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/nuance/sample/music/MusicDetails;

    .line 205
    .local v6, "musicDetailsSelection":Lcom/nuance/sample/music/MusicDetails;
    if-eqz v6, :cond_6

    .line 206
    invoke-direct {p0, v6}, Lcom/nuance/sample/controllers/SampleMusicController;->playFromDetails(Lcom/nuance/sample/music/MusicDetails;)V

    .line 207
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 208
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 212
    .end local v6    # "musicDetailsSelection":Lcom/nuance/sample/music/MusicDetails;
    :cond_6
    sget-object v9, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "execAction::search by params. Params"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getMusicListBySearchParams()Ljava/util/List;

    move-result-object v5

    .line 216
    .local v5, "info":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    invoke-static {v5}, Lcom/nuance/sample/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 217
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 218
    const v10, 0x7f0a003a

    .line 217
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSay(Ljava/lang/String;)V

    .line 219
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 222
    :cond_7
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_8

    .line 223
    invoke-static {p2, v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 224
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->promptForDisambiguation()V

    .line 225
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v11, 0x0

    invoke-direct {p0, p2}, Lcom/nuance/sample/controllers/SampleMusicController;->getMusicItemsWithPage(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v9, v10, v11, v12, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 226
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v9

    const-string/jumbo v10, "music-play-disambiguate"

    invoke-interface {v9, v10}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->startRecording()V

    .line 232
    :goto_2
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 229
    :cond_8
    const/4 v9, 0x0

    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/nuance/sample/music/MusicDetails;

    invoke-virtual {v9}, Lcom/nuance/sample/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/nuance/sample/controllers/SampleMusicController;->playByName(Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 434
    const-class v0, Lcom/sec/android/automotive/drivelink/music/MusicPlayerActivity;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    move-object v0, p2

    .line 81
    check-cast v0, Lcom/nuance/sample/music/MusicDetails;

    .line 83
    .local v0, "md":Lcom/nuance/sample/music/MusicDetails;
    if-eqz v0, :cond_0

    .line 84
    sget-object v1, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "SampleMusicController::handleIntent:: MusicDetail = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0}, Lcom/nuance/sample/music/MusicDetails;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 84
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-direct {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->playFromDetails(Lcom/nuance/sample/music/MusicDetails;)V

    .line 88
    :cond_0
    return-void
.end method

.method protected promptForDisambiguation()V
    .locals 2

    .prologue
    .line 359
    sget-object v0, Lcom/nuance/sample/controllers/SampleMusicController;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "execAction::disambiguation"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0032

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleMusicController;->showAndSay(Ljava/lang/String;)V

    .line 361
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleMusicController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_PLAY_TITLE_CHOICE:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 362
    return-void
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 439
    const/4 v0, 0x0

    return v0
.end method
