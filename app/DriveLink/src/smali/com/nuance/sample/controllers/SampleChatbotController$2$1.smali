.class Lcom/nuance/sample/controllers/SampleChatbotController$2$1;
.super Ljava/lang/Object;
.source "SampleChatbotController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/controllers/SampleChatbotController$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/nuance/sample/controllers/SampleChatbotController$2;


# direct methods
.method constructor <init>(Lcom/nuance/sample/controllers/SampleChatbotController$2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2$1;->this$1:Lcom/nuance/sample/controllers/SampleChatbotController$2;

    .line 455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 467
    const-string/jumbo v0, "DLPhraseSpotter"

    .line 468
    const-string/jumbo v1, "[stop] : SampleChatbotController - onResponseReceived()"

    .line 467
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    .line 470
    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 472
    const-string/jumbo v0, "DLPhraseSpotter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[start] : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 473
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 474
    const-string/jumbo v2, " - onResponseReceived()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 472
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    .line 476
    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 477
    return-void
.end method
