.class Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;
.super Ljava/lang/Object;
.source "SampleLocationController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleLocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddWaypointOfficeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleLocationController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 318
    const-string/jumbo v0, "Cambridge, MA"

    .line 319
    .local v0, "address":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const v2, 0x7f0a0619

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(I)V

    .line 326
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 322
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const-string/jumbo v2, " "

    const-string/jumbo v3, "+"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->setTo(Ljava/lang/String;)V

    .line 323
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->AddWaypoint:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    .line 324
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0
.end method
