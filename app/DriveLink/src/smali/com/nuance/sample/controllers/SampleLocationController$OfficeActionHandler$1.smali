.class Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;
.super Ljava/lang/Object;
.source "SampleLocationController.java"

# interfaces
.implements Lcom/nuance/drivelink/utils/DLLocationVACUtils$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;


# direct methods
.method constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;->this$1:Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V
    .locals 3
    .param p1, "location"    # Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    .prologue
    .line 114
    if-eqz p1, :cond_2

    .line 115
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;->this$1:Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;

    # getter for: Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->access$1(Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)Lcom/nuance/sample/controllers/SampleLocationController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->checkGoogleAccountAndTtsToast()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->getInstance()Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    move-result-object v1

    .line 118
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;->this$1:Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;

    # getter for: Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->access$1(Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)Lcom/nuance/sample/controllers/SampleLocationController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;->this$1:Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;

    # getter for: Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->access$1(Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)Lcom/nuance/sample/controllers/SampleLocationController;

    move-result-object v1

    # invokes: Lcom/nuance/sample/controllers/SampleLocationController;->notifyEnableGPS()V
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->access$0(Lcom/nuance/sample/controllers/SampleLocationController;)V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v1

    .line 135
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 136
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    .line 133
    invoke-virtual {v1, v2, p1}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    goto :goto_0

    .line 142
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v1

    .line 143
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getDrivingStatus()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 144
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 146
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 147
    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 148
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler$1;->this$1:Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;

    # getter for: Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;
    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;->access$1(Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)Lcom/nuance/sample/controllers/SampleLocationController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 149
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 151
    const v2, 0x7f0a01dd

    .line 150
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    goto :goto_0

    .line 156
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 157
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "android.settings.MY_PLACE_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 160
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 163
    const v2, 0xddd5

    .line 161
    invoke-virtual {v1, v0, v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
