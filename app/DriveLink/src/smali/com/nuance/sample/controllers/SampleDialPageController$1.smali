.class Lcom/nuance/sample/controllers/SampleDialPageController$1;
.super Ljava/lang/Object;
.source "SampleDialPageController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/controllers/SampleDialPageController;->runFinalAction()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleDialPageController;


# direct methods
.method constructor <init>(Lcom/nuance/sample/controllers/SampleDialPageController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleDialPageController$1;->this$0:Lcom/nuance/sample/controllers/SampleDialPageController;

    .line 396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionAbort()V
    .locals 2

    .prologue
    .line 409
    # getter for: Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleDialPageController;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DialUtil.dial actionAbort()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    return-void
.end method

.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 404
    # getter for: Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleDialPageController;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DialUtil.dial actionFail(\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    return-void
.end method

.method public actionSuccess()V
    .locals 2

    .prologue
    .line 399
    # getter for: Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleDialPageController;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DialUtil.dial actionSuccess()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    return-void
.end method
