.class Lcom/nuance/sample/controllers/SampleChatbotController$2;
.super Ljava/lang/Object;
.source "SampleChatbotController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/controllers/SampleChatbotController;->onResponseReceived()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleChatbotController;


# direct methods
.method constructor <init>(Lcom/nuance/sample/controllers/SampleChatbotController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    .line 421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const v3, 0x7f0a07d0

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 424
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # getter for: Lcom/nuance/sample/controllers/SampleChatbotController;->newsManager:Lcom/nuance/sample/news/NewsManager;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$1(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/nuance/sample/news/NewsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/sample/news/NewsManager;->getCurrentNews()Lcom/nuance/sample/news/NewsItem;

    move-result-object v0

    .line 426
    .local v0, "item":Lcom/nuance/sample/news/NewsItem;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/nuance/sample/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 427
    invoke-virtual {v0}, Lcom/nuance/sample/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 428
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # getter for: Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$2(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 429
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    .line 430
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 431
    .local v1, "rsc":Landroid/content/res/Resources;
    invoke-virtual {v0}, Lcom/nuance/sample/news/NewsItem;->getNewsCP()I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 432
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    .line 433
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    .line 434
    const v5, 0x7f0a07df

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 432
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$4(Lcom/nuance/sample/controllers/SampleChatbotController;Ljava/lang/String;)V

    .line 441
    .end local v1    # "rsc":Landroid/content/res/Resources;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 442
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # getter for: Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;
    invoke-static {v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$2(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # getter for: Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;
    invoke-static {v4}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$2(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :goto_1
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowChatbotWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v4, 0x0

    .line 448
    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    .line 447
    invoke-interface {v2, v3, v4, v0, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 449
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 450
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-virtual {v0}, Lcom/nuance/sample/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    .line 453
    :cond_1
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    invoke-virtual {v0}, Lcom/nuance/sample/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->playOutNews(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$5(Lcom/nuance/sample/controllers/SampleChatbotController;Ljava/lang/String;)V

    .line 455
    new-instance v2, Lcom/nuance/sample/controllers/SampleChatbotController$2$1;

    invoke-direct {v2, p0}, Lcom/nuance/sample/controllers/SampleChatbotController$2$1;-><init>(Lcom/nuance/sample/controllers/SampleChatbotController$2;)V

    .line 479
    const-wide/16 v3, 0x5dc

    .line 455
    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 485
    :goto_2
    return-void

    .line 436
    .restart local v1    # "rsc":Landroid/content/res/Resources;
    :cond_2
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    .line 437
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    .line 438
    const v5, 0x7f0a07e0

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 436
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$4(Lcom/nuance/sample/controllers/SampleChatbotController;Ljava/lang/String;)V

    goto :goto_0

    .line 444
    .end local v1    # "rsc":Landroid/content/res/Resources;
    :cond_3
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # getter for: Lcom/nuance/sample/controllers/SampleChatbotController;->prompt:Ljava/lang/String;
    invoke-static {v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$2(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoText(Ljava/lang/String;)V

    goto :goto_1

    .line 481
    :cond_4
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    .line 482
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # getter for: Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;
    invoke-static {v3}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$6(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # getter for: Lcom/nuance/sample/controllers/SampleChatbotController;->errorPrompt:Ljava/lang/String;
    invoke-static {v4}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$6(Lcom/nuance/sample/controllers/SampleChatbotController;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleChatbotController$2;->this$0:Lcom/nuance/sample/controllers/SampleChatbotController;

    # invokes: Lcom/nuance/sample/controllers/SampleChatbotController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/nuance/sample/controllers/SampleChatbotController;->access$3(Lcom/nuance/sample/controllers/SampleChatbotController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_2
.end method
