.class public Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;
.super Ljava/lang/Object;
.source "SampleContactSearchController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleContactSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CreateTypeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;


# direct methods
.method protected constructor <init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 279
    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    const-string/jumbo v5, "To"

    invoke-static {p1, v5, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setTo(Ljava/lang/String;)V

    .line 280
    const-string/jumbo v4, "Pn"

    invoke-static {p1, v4, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "phoneNumber":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 282
    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    const-string/jumbo v5, "PhoneType"

    invoke-static {p1, v5, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPhoneType(Ljava/lang/String;)V

    .line 284
    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getTo()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 285
    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    iget-object v5, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v5}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v5

    new-array v2, v2, [Ljava/lang/String;

    iget-object v6, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v6}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v7}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactRequestString()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v4, v5, v2}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    :goto_0
    move v2, v3

    .line 300
    :goto_1
    return v2

    .line 287
    :cond_0
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startContactSearch()V

    goto :goto_1

    .line 291
    :cond_1
    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getTo()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 292
    const-string/jumbo v4, "PhoneType"

    invoke-static {p1, v4, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 293
    .local v1, "phoneType":Ljava/lang/String;
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v4, v1}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneType(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPhoneType(I)V

    .line 295
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startContactSearch()V

    goto :goto_1

    .line 298
    .end local v1    # "phoneType":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    # invokes: Lcom/nuance/sample/controllers/SampleContactSearchController;->followWithPhoneNumber(Ljava/lang/String;)V
    invoke-static {v2, v0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->access$0(Lcom/nuance/sample/controllers/SampleContactSearchController;Ljava/lang/String;)V

    goto :goto_0
.end method
