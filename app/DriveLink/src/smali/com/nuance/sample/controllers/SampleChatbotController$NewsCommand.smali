.class public final enum Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
.super Ljava/lang/Enum;
.source "SampleChatbotController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleChatbotController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NewsCommand"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Cancel:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Down:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Mute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Next:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum No:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum None:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Pause:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Prev:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Read:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Repeat:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Reset:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Restart:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Resume:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Start:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Stop:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Unmute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

.field public static final enum Up:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 302
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "None"

    invoke-direct {v0, v1, v3}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->None:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Read"

    invoke-direct {v0, v1, v4}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Read:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 303
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Next"

    invoke-direct {v0, v1, v5}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 304
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Next:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 305
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Prev"

    invoke-direct {v0, v1, v6}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 306
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Prev:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 307
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Stop"

    invoke-direct {v0, v1, v7}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 308
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Stop:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 309
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "No"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 310
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->No:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 311
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Cancel"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 312
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Cancel:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 313
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Pause"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 314
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Pause:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 315
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Resume"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 316
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Resume:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 317
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Up"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 318
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Up:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 319
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Down"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 320
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Down:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 321
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Mute"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 322
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Mute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 323
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Unmute"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 324
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Unmute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 325
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Reset"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 326
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Reset:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 327
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Start"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 328
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Start:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 329
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Restart"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 330
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Restart:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 331
    new-instance v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    const-string/jumbo v1, "Repeat"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;-><init>(Ljava/lang/String;I)V

    .line 332
    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Repeat:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    .line 301
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->None:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Read:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Next:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Prev:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Stop:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->No:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Cancel:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Pause:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Resume:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Up:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Down:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Mute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Unmute:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Reset:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Start:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Restart:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->Repeat:Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ENUM$VALUES:[Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;->ENUM$VALUES:[Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/controllers/SampleChatbotController$NewsCommand;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
