.class public abstract Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$IntegratedFlowHandler;
.super Ljava/lang/Object;
.source "SampleContactSearchControllerBase.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "IntegratedFlowHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;


# direct methods
.method protected constructor <init>(Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$IntegratedFlowHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$IntegratedFlowHandler;->getFlowID()Ljava/lang/String;

    move-result-object v0

    .line 361
    .local v0, "flowID":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$IntegratedFlowHandler;->getFlowParams()Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v1

    .line 362
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 363
    const/4 v2, 0x0

    return v2
.end method

.method protected abstract getFlowID()Ljava/lang/String;
.end method

.method protected abstract getFlowParams()Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
.end method
