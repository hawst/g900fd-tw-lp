.class public Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;
.super Ljava/lang/Object;
.source "SampleContactSearchController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleContactSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PhoneTypeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;


# direct methods
.method protected constructor <init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v8, 0x0

    .line 308
    const-string/jumbo v3, "Which"

    invoke-static {p1, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 319
    .local v2, "which":Ljava/lang/String;
    const-string/jumbo v3, "DM_DIAL_TYPE"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 320
    .local v0, "f":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v0, :cond_0

    .line 321
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v3}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 324
    :cond_0
    if-eqz v2, :cond_2

    .line 325
    invoke-static {v2}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getOrdinalIndex(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 326
    const-string/jumbo v3, "DM_LIST_ORDINAL"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 362
    :cond_1
    :goto_0
    return v8

    .line 354
    :cond_2
    const-string/jumbo v3, "PhoneType"

    invoke-static {p1, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 355
    .local v1, "phoneType":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 356
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v3, v1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPhoneType(Ljava/lang/String;)V

    .line 357
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleContactSearchController;->followWithSingleContact()V

    goto :goto_0

    .line 359
    :cond_3
    iget-object v3, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    iget-object v4, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v6}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$PhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v7}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getMultiplePhoneTypeString()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0
.end method
