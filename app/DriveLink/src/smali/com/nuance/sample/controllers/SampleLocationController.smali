.class public Lcom/nuance/sample/controllers/SampleLocationController;
.super Lcom/nuance/sample/controllers/SampleContactSearchController;
.source "SampleLocationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$GroupShareActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$HomeActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$Mode;,
        Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$RequestActionHandler;,
        Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$controllers$SampleLocationController$Mode:[I = null

.field private static final CLASS_NAME:Ljava/lang/String; = "com.google.android.apps.maps,com.google.android.maps.driveabout.app.NavigationActivity"

.field private static final GROUPSHARE_ACTION:Ljava/lang/String; = "groupshare"

.field private static final HOME_ACTION:Ljava/lang/String; = "home"

.field private static final MAP_ACTION:Ljava/lang/String; = "map"

.field private static final NAVIGATE_ACTION:Ljava/lang/String; = "navigate"

.field private static final NAVIGATE_WAYPOINT_ACTION:Ljava/lang/String; = "waypoint"

.field private static final NAVIGATE_WAYPOINT_HOME_ACTION:Ljava/lang/String; = "waypointhome"

.field private static final NAVIGATE_WAYPOINT_OFFICE_ACTION:Ljava/lang/String; = "waypointoffice"

.field private static final NAV_URL_PATTERN:Ljava/lang/String; = "google.navigation:q=%s"

.field private static final OFFICE_ACTION:Ljava/lang/String; = "office"

.field private static final REQUEST_ACTION:Ljava/lang/String; = "request"

.field private static final SHARE_ACTION:Ljava/lang/String; = "share"

.field private static final TAG:Ljava/lang/String; = "SampleLocationController"

.field private static mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;


# instance fields
.field private mToast:Landroid/widget/Toast;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$controllers$SampleLocationController$Mode()[I
    .locals 3

    .prologue
    .line 60
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController;->$SWITCH_TABLE$com$nuance$sample$controllers$SampleLocationController$Mode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->values()[Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->AddWaypoint:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->DestinationChanged:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupJoinConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_3
    :try_start_3
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupLocationExpiration:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_4
    :try_start_4
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupShare:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_5
    :try_start_5
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Navigation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_6
    :try_start_6
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Request:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_7
    :try_start_7
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestApproved:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_8
    :try_start_8
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestShareConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_9
    :try_start_9
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_a
    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController;->$SWITCH_TABLE$com$nuance$sample$controllers$SampleLocationController$Mode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_a

    :catch_1
    move-exception v1

    goto :goto_9

    :catch_2
    move-exception v1

    goto :goto_8

    :catch_3
    move-exception v1

    goto :goto_7

    :catch_4
    move-exception v1

    goto :goto_6

    :catch_5
    move-exception v1

    goto :goto_5

    :catch_6
    move-exception v1

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Navigation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController;->mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/controllers/SampleLocationController;)V
    .locals 0

    .prologue
    .line 952
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->notifyEnableGPS()V

    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/controllers/SampleLocationController;)Z
    .locals 1

    .prologue
    .line 674
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->checkIfAddressAvailableAndRequest()Z

    move-result v0

    return v0
.end method

.method private checkIfAddressAvailableAndRequest()Z
    .locals 10

    .prologue
    const v9, 0x7f0a01fb

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 675
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getInstance()Lcom/sec/android/automotive/drivelink/common/DLServiceManager;

    move-result-object v7

    .line 676
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/common/DLServiceManager;->getLocationSuggestionList()Ljava/util/ArrayList;

    move-result-object v3

    .line 677
    .local v3, "mLocationList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v3, v7}, Lcom/nuance/sample/controllers/SampleLocationController;->isContains(Ljava/util/List;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    move-result-object v2

    .line 678
    .local v2, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    const-string/jumbo v7, "VAC_DRIVELINK"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 679
    if-eqz v2, :cond_0

    .line 680
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v6

    .line 681
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v7

    .line 682
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocation()Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;

    move-result-object v8

    .line 680
    invoke-virtual {v6, v7, v8}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    .line 725
    :goto_0
    return v5

    .line 687
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v7

    .line 688
    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 686
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v7

    .line 690
    const-string/jumbo v8, "PREF_SETTINGS_MY_NAVIGATION"

    .line 689
    invoke-virtual {v7, v8, v5}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v4

    .line 692
    .local v4, "mapID":I
    sget v7, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v4, v7, :cond_1

    .line 693
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/nuance/sample/controllers/SampleLocationController;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 694
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 695
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 697
    invoke-virtual {p0, v9}, Lcom/nuance/sample/controllers/SampleLocationController;->showToast(I)V

    .line 698
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v6

    .line 699
    const-wide/16 v7, 0x258

    invoke-virtual {v6, v7, v8}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto :goto_0

    .line 702
    :cond_1
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 703
    .local v1, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mLocationName:Ljava/lang/String;

    .line 704
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 705
    const v8, 0x7f0a015c

    new-array v6, v6, [Ljava/lang/Object;

    .line 706
    iget-object v9, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mLocationName:Ljava/lang/String;

    aput-object v9, v6, v5

    .line 704
    invoke-virtual {v7, v8, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 708
    const-string/jumbo v6, "DM_LOCATION_NAV_SEARCH_CONFIRM"

    .line 707
    invoke-static {v6, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 713
    .end local v1    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v4    # "mapID":I
    :cond_2
    const-string/jumbo v7, "onFlowListSelected"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, " Else "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    .line 716
    const-wide/16 v7, 0xc8

    :try_start_0
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 720
    :goto_1
    sget-object v7, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_HOME:Lcom/nuance/sample/CCFieldIds;

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 721
    const v9, 0x7f0a015f

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v5

    .line 720
    invoke-virtual {p0, v7, v6}, Lcom/nuance/sample/controllers/SampleLocationController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 717
    :catch_0
    move-exception v0

    .line 718
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 724
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/nuance/sample/controllers/SampleLocationController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    move v5, v6

    .line 725
    goto/16 :goto_0
.end method

.method public static getMode()Lcom/nuance/sample/controllers/SampleLocationController$Mode;
    .locals 1

    .prologue
    .line 732
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController;->mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    return-object v0
.end method

.method private hasGoogleAccount(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 961
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 962
    .local v1, "accMan":Landroid/accounts/AccountManager;
    const-string/jumbo v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 963
    .local v0, "accArray":[Landroid/accounts/Account;
    array-length v3, v0

    if-lt v3, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isContains(Ljava/util/List;Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    .locals 6
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;"
        }
    .end annotation

    .prologue
    .local p1, "mLocationList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;>;"
    const/4 v1, 0x0

    .line 661
    if-nez p1, :cond_0

    move-object v0, v1

    .line 671
    :goto_0
    return-object v0

    .line 665
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v1

    .line 671
    goto :goto_0

    .line 665
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;

    .line 666
    .local v0, "item":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocationItem;->getLocationTitle()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "\\s+"

    const-string/jumbo v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 667
    invoke-virtual {v3, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0
.end method

.method private notifyEnableGPS()V
    .locals 3

    .prologue
    .line 953
    new-instance v0, Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;-><init>()V

    .line 955
    .local v0, "dialog":Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    .line 956
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "dialog 2"

    .line 955
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/notification/view/NotificationEnableGPSDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 958
    return-void
.end method

.method public static setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V
    .locals 0
    .param p0, "mode"    # Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .prologue
    .line 736
    sput-object p0, Lcom/nuance/sample/controllers/SampleLocationController;->mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 737
    return-void
.end method


# virtual methods
.method public checkGoogleAccountAndTtsToast()Z
    .locals 5

    .prologue
    const v4, 0x7f0a01fb

    const/4 v1, 0x0

    .line 508
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 507
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;

    move-result-object v2

    .line 509
    const-string/jumbo v3, "PREF_SETTINGS_MY_NAVIGATION"

    .line 508
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseSharedPreferences;->getValue(Ljava/lang/String;I)I

    move-result v0

    .line 510
    .local v0, "mapID":I
    sget v2, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationUtils;->APP_NAME_GOOGLE_MAPS:I

    if-ne v0, v2, :cond_0

    .line 511
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->hasGoogleAccount(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 512
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 513
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/TTSUtil;->promptTTS(Ljava/lang/String;)V

    .line 515
    invoke-virtual {p0, v4}, Lcom/nuance/sample/controllers/SampleLocationController;->showToast(I)V

    .line 516
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    .line 517
    const-wide/16 v3, 0x258

    invoke-virtual {v2, v3, v4}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 520
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected cleanup()V
    .locals 1

    .prologue
    .line 849
    invoke-super {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->cleanup()V

    .line 850
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Navigation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController;->mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 851
    return-void
.end method

.method protected followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 13
    .param p1, "contactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const v12, 0x7f0a05ed

    const v11, 0x7f0a015c

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 558
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 559
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v0

    .line 560
    .local v0, "address":Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->$SWITCH_TABLE$com$nuance$sample$controllers$SampleLocationController$Mode()[I

    move-result-object v6

    sget-object v7, Lcom/nuance/sample/controllers/SampleLocationController;->mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v7}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 657
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 562
    :pswitch_1
    const-string/jumbo v6, "VAC_DRIVELINK"

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 574
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/nuance/drivelink/utils/DLLocationVACUtils;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;Ljava/lang/String;)V

    goto :goto_0

    .line 577
    :cond_1
    const-string/jumbo v6, "+"

    const-string/jumbo v7, " "

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 580
    new-array v6, v9, [Ljava/lang/Object;

    aput-object v0, v6, v10

    .line 579
    invoke-virtual {v1, v11, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 581
    .local v5, "teste":Ljava/lang/String;
    const-string/jumbo v6, "DEBUG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Say message: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    sget-object v6, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    new-array v7, v9, [Ljava/lang/String;

    .line 584
    new-array v8, v9, [Ljava/lang/Object;

    aput-object v0, v8, v10

    .line 583
    invoke-virtual {v1, v11, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    .line 582
    invoke-virtual {p0, v6, v7}, Lcom/nuance/sample/controllers/SampleLocationController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0

    .line 588
    .end local v5    # "teste":Ljava/lang/String;
    :pswitch_2
    const-string/jumbo v6, "+"

    const-string/jumbo v7, " "

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 590
    goto :goto_0

    .line 592
    :pswitch_3
    const-string/jumbo v6, "VAC_DRIVELINK"

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 593
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    .line 594
    .local v3, "flowID":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v4

    .line 595
    .local v4, "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v4, :cond_0

    .line 596
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 598
    iget-object v6, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v6, :cond_2

    .line 599
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 600
    new-array v7, v9, [Ljava/lang/Object;

    .line 601
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    iget-object v8, v8, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v8, v7, v10

    .line 599
    invoke-virtual {v6, v12, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 604
    :cond_2
    iput-boolean v9, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mIsShareMyLocation:Z

    goto/16 :goto_0

    .line 611
    .end local v3    # "flowID":Ljava/lang/String;
    .end local v4    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    const-string/jumbo v2, ""

    .line 612
    .local v2, "displayName":Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 613
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    iget-object v2, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 615
    :cond_4
    sget-object v6, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    new-array v7, v9, [Ljava/lang/String;

    .line 616
    new-array v8, v9, [Ljava/lang/Object;

    .line 617
    aput-object v2, v8, v10

    .line 616
    invoke-virtual {v1, v12, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    .line 615
    invoke-virtual {p0, v6, v7}, Lcom/nuance/sample/controllers/SampleLocationController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 621
    .end local v2    # "displayName":Ljava/lang/String;
    :pswitch_4
    const-string/jumbo v6, "VAC_DRIVELINK"

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 622
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    .line 623
    .restart local v3    # "flowID":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v4

    .line 624
    .restart local v4    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v4, :cond_0

    .line 625
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 626
    iget-object v6, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v6, :cond_5

    .line 627
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 628
    const v7, 0x7f0a0213

    new-array v8, v9, [Ljava/lang/Object;

    .line 629
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v9

    iget-object v9, v9, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v9, v8, v10

    .line 627
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 631
    :cond_5
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 632
    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactData;->getNormalizedAddress()Ljava/lang/String;

    move-result-object v6

    .line 631
    iput-object v6, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPhoneNum:Ljava/lang/String;

    goto/16 :goto_0

    .line 638
    .end local v3    # "flowID":Ljava/lang/String;
    .end local v4    # "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_6
    const-string/jumbo v2, ""

    .line 639
    .restart local v2    # "displayName":Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 640
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v6

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    goto/16 :goto_0

    .line 560
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected getActionConfirmationKeys()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 840
    invoke-super {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionConfirmationKeys()Ljava/util/List;

    move-result-object v0

    .line 841
    .local v0, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v1, "route"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 842
    const-string/jumbo v1, "execute"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 843
    const-string/jumbo v1, "extend"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 844
    return-object v0
.end method

.method protected getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 482
    const-string/jumbo v0, "home"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$HomeActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$HomeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$HomeActionHandler;)V

    .line 503
    :goto_0
    return-object v0

    .line 484
    :cond_0
    const-string/jumbo v0, "office"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 485
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$OfficeActionHandler;)V

    goto :goto_0

    .line 486
    :cond_1
    const-string/jumbo v0, "request"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 487
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$RequestActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$RequestActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$RequestActionHandler;)V

    goto :goto_0

    .line 488
    :cond_2
    const-string/jumbo v0, "navigate"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 489
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;

    invoke-direct {v0, p0, v1, v1}, Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;Lcom/nuance/sample/controllers/SampleLocationController$NavigateActionHandler;)V

    goto :goto_0

    .line 490
    :cond_3
    const-string/jumbo v0, "waypoint"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 491
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointActionHandler;)V

    goto :goto_0

    .line 492
    :cond_4
    const-string/jumbo v0, "waypointhome"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 493
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;)V

    goto :goto_0

    .line 494
    :cond_5
    const-string/jumbo v0, "waypointoffice"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 495
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointOfficeActionHandler;)V

    goto :goto_0

    .line 496
    :cond_6
    const-string/jumbo v0, "share"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 497
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;)V

    goto :goto_0

    .line 498
    :cond_7
    const-string/jumbo v0, "groupshare"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 499
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$GroupShareActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$GroupShareActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$GroupShareActionHandler;)V

    goto :goto_0

    .line 500
    :cond_8
    const-string/jumbo v0, "map"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 501
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;)V

    goto/16 :goto_0

    .line 503
    :cond_9
    invoke-super {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    const-class v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    return-object v0
.end method

.method protected getContactFieldID()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 860
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getContactNotFoundString()I
    .locals 1

    .prologue
    .line 881
    const v0, 0x7f0a0613

    return v0
.end method

.method protected getContactRequestString()I
    .locals 2

    .prologue
    .line 870
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController;->mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    if-ne v0, v1, :cond_0

    const v0, 0x7f0a0610

    :goto_0
    return v0

    .line 871
    :cond_0
    const v0, 0x7f0a0611

    goto :goto_0
.end method

.method protected getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 865
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_TYPE:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getMultiplePhoneTypeString()I
    .locals 1

    .prologue
    .line 876
    const v0, 0x7f0a0612

    return v0
.end method

.method protected getSearchContactType()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 855
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method protected multiContactsSearched(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 934
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 935
    .local v1, "params":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object p1, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 937
    const-string/jumbo v2, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPromptString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 938
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 936
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 940
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v0

    .line 942
    .local v0, "flowID":Ljava/lang/String;
    const-string/jumbo v2, "DM_LOCATION_CONTACT_REQUEST"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 944
    const-string/jumbo v2, "DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST"

    .line 943
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 945
    :cond_1
    const-string/jumbo v2, "DM_LOCATION_CONTACT_SHARE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 947
    const-string/jumbo v2, "DM_LOCATION_CONTACT_SHARE_SEARCH_LIST"

    .line 946
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method protected varargs noContactsSearched([Ljava/lang/String;)V
    .locals 7
    .param p1, "prompts"    # [Ljava/lang/String;

    .prologue
    .line 898
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 899
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 900
    const v3, 0x7f0a033d

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 899
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 901
    const-string/jumbo v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 902
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a024c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 905
    .local v1, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 907
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getMode()Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    move-result-object v2

    sget-object v3, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    if-ne v2, v3, :cond_1

    .line 908
    const-string/jumbo v2, "DM_LOCATION_CONTACT_SHARE"

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 914
    :cond_0
    :goto_0
    return-void

    .line 910
    :cond_1
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getMode()Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    move-result-object v2

    sget-object v3, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Request:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    if-ne v2, v3, :cond_0

    .line 912
    const-string/jumbo v2, "DM_LOCATION_CONTACT_REQUEST"

    .line 911
    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method

.method public resolveConfirmation(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 6
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 527
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v2

    .line 528
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v3}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 530
    const-string/jumbo v2, "Command"

    .line 529
    invoke-static {p1, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 531
    .local v0, "command":Ljava/lang/String;
    const-string/jumbo v2, "ignore"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 533
    const-string/jumbo v2, "DLPhraseSpotter"

    .line 534
    const-string/jumbo v3, "[stop] : SampleLocationController - resolveConfirmation()"

    .line 533
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    .line 536
    invoke-virtual {v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 537
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->finishDialog()V

    .line 553
    .end local v0    # "command":Ljava/lang/String;
    :goto_0
    return v1

    .line 539
    .restart local v0    # "command":Ljava/lang/String;
    :cond_0
    const-string/jumbo v2, "lookup"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 540
    const-string/jumbo v2, "Cambridge"

    invoke-virtual {p0, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->setTo(Ljava/lang/String;)V

    .line 541
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    .line 543
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    new-array v3, v4, [Ljava/lang/String;

    .line 544
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 545
    const v5, 0x7f0a006c

    .line 544
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 542
    invoke-virtual {p0, v2, v3}, Lcom/nuance/sample/controllers/SampleLocationController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0

    .line 547
    :cond_1
    const-string/jumbo v2, "execute"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 548
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 549
    const v3, 0x7f0a05f0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 548
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    goto :goto_0

    .line 553
    .end local v0    # "command":Ljava/lang/String;
    :cond_2
    invoke-super {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->resolveConfirmation(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v1

    goto :goto_0
.end method

.method protected runFinalAction()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const v4, 0x7f0a0602

    const v7, 0x7f0a05f0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 741
    const/4 v0, 0x0

    .line 742
    .local v0, "result":Z
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->$SWITCH_TABLE$com$nuance$sample$controllers$SampleLocationController$Mode()[I

    move-result-object v1

    sget-object v3, Lcom/nuance/sample/controllers/SampleLocationController;->mode:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 834
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->cleanup()V

    .line 835
    return v0

    .line 744
    :pswitch_0
    const-string/jumbo v1, "VAC_DRIVELINK"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 746
    const-string/jumbo v1, "DM_CONFIRM_YES"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 749
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 750
    new-array v2, v5, [Ljava/lang/Object;

    .line 751
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v3

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v3, v2, v6

    .line 749
    invoke-virtual {v1, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 752
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 754
    new-array v3, v5, [Ljava/lang/Object;

    .line 756
    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v4

    iget-object v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 753
    invoke-virtual {v2, v7, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 748
    invoke-virtual {p0, v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 759
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Requesting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v3

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 760
    const-string/jumbo v3, " Phone: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactData()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v3

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 759
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 757
    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 761
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 765
    :pswitch_1
    const-string/jumbo v1, "VAC_DRIVELINK"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 767
    const-string/jumbo v1, "DM_CONFIRM_YES"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 769
    :cond_2
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    .line 772
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 773
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Sharing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v3

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 774
    const-string/jumbo v3, " Phone: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/nuance/sample/controllers/SampleLocationController;->getContactData()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v3

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 773
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 771
    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 775
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 779
    :pswitch_2
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    .line 781
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "Sharing group location"

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 782
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 786
    :pswitch_3
    const-string/jumbo v1, "VAC_DRIVELINK"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 795
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v1

    const-string/jumbo v3, "navigate"

    invoke-interface {v1, v3}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 796
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 797
    const-class v3, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    .line 796
    invoke-virtual {p0, v1, v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    .line 798
    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    .line 799
    const-string/jumbo v3, "google.navigation:q=%s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    .line 801
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->isGoogleNavAvailable()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v2

    .line 800
    :goto_1
    invoke-virtual {v3, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    .line 802
    invoke-virtual {v1, v6}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 803
    const/4 v0, 0x1

    .line 805
    goto/16 :goto_0

    .line 802
    :cond_3
    const-string/jumbo v1, "com.google.android.apps.maps,com.google.android.maps.driveabout.app.NavigationActivity"

    goto :goto_1

    .line 807
    :pswitch_4
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v3, "Adding waypoint"

    invoke-static {v1, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 808
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v1

    const-string/jumbo v3, "navigate"

    invoke-interface {v1, v3}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 809
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v1, v3}, Lcom/nuance/sample/controllers/SampleLocationController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    .line 810
    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    .line 811
    const-string/jumbo v3, "google.navigation:q=%s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    .line 812
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->isGoogleNavAvailable()Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    .line 813
    invoke-virtual {v1, v6}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    .line 814
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 815
    const/4 v0, 0x1

    .line 816
    goto/16 :goto_0

    .line 812
    :cond_4
    const-string/jumbo v2, "com.google.android.apps.maps,com.google.android.maps.driveabout.app.NavigationActivity"

    goto :goto_2

    .line 818
    :pswitch_5
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 819
    const v2, 0x7f0a05fb

    .line 818
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 822
    :pswitch_6
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 823
    const v2, 0x7f0a05fc

    .line 822
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 826
    :pswitch_7
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 827
    const v2, 0x7f0a05fd

    .line 826
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 830
    :pswitch_8
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 831
    const v2, 0x7f0a061a

    .line 830
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 742
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method

.method public showToast(I)V
    .locals 2
    .param p1, "textID"    # I

    .prologue
    .line 967
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 968
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 970
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    .line 971
    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v0

    const/4 v1, 0x1

    .line 970
    invoke-static {v0, p1, v1}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController;->mToast:Landroid/widget/Toast;

    .line 972
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 973
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 974
    :cond_1
    return-void
.end method
