.class final Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;
.super Ljava/lang/Object;
.source "SMSController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SMSController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TextTypeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SMSController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SMSController;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v5, 0x0

    .line 157
    iget-object v1, p0, Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-static {v1, v5}, Lcom/nuance/sample/controllers/SMSController;->access$0(Lcom/nuance/sample/controllers/SMSController;Z)V

    .line 160
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 162
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    const-string/jumbo v1, "Message"

    .line 161
    invoke-static {p1, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 163
    iget-object v1, p0, Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 164
    const v2, 0x7f0a013d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    aput-object v4, v3, v5

    .line 163
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 166
    # getter for: Lcom/nuance/sample/controllers/SMSController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[TextTypeActionHandler executeAction ] text : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 167
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 166
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const-string/jumbo v1, "DM_SMS_COMPLETE"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 171
    return v5
.end method
