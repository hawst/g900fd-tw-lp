.class Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;
.super Ljava/lang/Object;
.source "SampleLocationController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleLocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddWaypointHomeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleLocationController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;)V
    .locals 0

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x0

    .line 303
    const-string/jumbo v1, "car_nav_home_address"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "address":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const v2, 0x7f0a006c

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->showAndSay(I)V

    .line 311
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 307
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    const-string/jumbo v2, " "

    const-string/jumbo v3, "+"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController;->setTo(Ljava/lang/String;)V

    .line 308
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->AddWaypoint:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    .line 309
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$AddWaypointHomeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v1, v4}, Lcom/nuance/sample/controllers/SampleLocationController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0
.end method
