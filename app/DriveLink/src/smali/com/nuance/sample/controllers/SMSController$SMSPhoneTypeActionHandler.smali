.class final Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;
.super Ljava/lang/Object;
.source "SMSController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SMSController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SMSPhoneTypeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SMSController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SMSController;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v8, 0x0

    .line 252
    const-string/jumbo v3, "Which"

    invoke-static {p1, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 253
    .local v2, "which":Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v3

    if-nez v3, :cond_0

    .line 255
    const-string/jumbo v3, "DM_SMS_TYPE"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 256
    .local v0, "f":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    if-eqz v0, :cond_0

    .line 257
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v3}, Lcom/nuance/sample/controllers/SMSController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 260
    .end local v0    # "f":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    if-eqz v2, :cond_2

    .line 261
    invoke-static {v2}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getOrdinalIndex(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 263
    const-string/jumbo v3, "DM_LIST_ORDINAL"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 284
    :cond_1
    :goto_0
    return v8

    .line 274
    :cond_2
    const-string/jumbo v3, "PhoneType"

    .line 273
    invoke-static {p1, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 275
    .local v1, "phoneType":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 276
    iget-object v3, p0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v4, v1}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneType(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/nuance/sample/controllers/SMSController;->setPhoneType(I)V

    .line 278
    iget-object v3, p0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v3}, Lcom/nuance/sample/controllers/SMSController;->followWithSingleContact()V

    goto :goto_0

    .line 280
    :cond_3
    iget-object v3, p0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SMSController;->getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v6}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 281
    iget-object v7, p0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v7}, Lcom/nuance/sample/controllers/SMSController;->getMultiplePhoneTypeString()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 280
    invoke-virtual {v3, v4, v5}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0
.end method
