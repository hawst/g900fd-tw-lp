.class Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;
.super Ljava/lang/Object;
.source "SampleLocationController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleLocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShareActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleLocationController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;)V
    .locals 0

    .prologue
    .line 409
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 2
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 412
    const-string/jumbo v0, "VAC_DRIVELINK"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    invoke-static {}, Lcom/nuance/drivelink/handlers/DLUnsupportedDomainHandler;->showErrorTTS()V

    .line 429
    :goto_0
    return v1

    .line 418
    :cond_0
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    .line 423
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController$ShareActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v0}, Lcom/nuance/sample/controllers/SampleLocationController;->getTo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    goto :goto_0
.end method
