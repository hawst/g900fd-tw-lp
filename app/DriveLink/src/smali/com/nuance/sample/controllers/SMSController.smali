.class public Lcom/nuance/sample/controllers/SMSController;
.super Lcom/nuance/sample/controllers/SampleContactSearchController;
.source "SMSController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/controllers/SMSController$AppendTextActionHandler;,
        Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;,
        Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;,
        Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;,
        Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;,
        Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;
    }
.end annotation


# static fields
.field private static final APPEND_TYPE:Ljava/lang/String; = "append"

.field private static final INBOX_TYPE:Ljava/lang/String; = "inbox"

.field private static final REPLACE_TYPE:Ljava/lang/String; = "replace"

.field private static final TAG:Ljava/lang/String;

.field private static final TEXT_TYPE:Ljava/lang/String; = "text"

.field private static message:Ljava/lang/String;


# instance fields
.field private appendMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/controllers/SMSController;->TAG:Ljava/lang/String;

    .line 475
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;-><init>()V

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/sample/controllers/SMSController;->appendMode:Z

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/controllers/SMSController;Z)V
    .locals 0

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/nuance/sample/controllers/SMSController;->appendMode:Z

    return-void
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/nuance/sample/controllers/SMSController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/controllers/SMSController;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SMSController;->removeHyphenForPhonenumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/nuance/sample/controllers/SMSController;)V
    .locals 0

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SMSController;->startInboxSearch()V

    return-void
.end method

.method static synthetic access$4(Lcom/nuance/sample/controllers/SMSController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 458
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SMSController;->followWithPhoneNumber(Ljava/lang/String;)V

    return-void
.end method

.method private followWithPhoneNumber(Ljava/lang/String;)V
    .locals 8
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 459
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const-wide/16 v3, 0x0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 461
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v2, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v3, v0

    move-object v5, p1

    move v7, v6

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 463
    .local v2, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 464
    invoke-virtual {p0, p1}, Lcom/nuance/sample/controllers/SMSController;->setTo(Ljava/lang/String;)V

    .line 465
    invoke-static {v0}, Lcom/nuance/sample/controllers/SMSController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 466
    invoke-static {v2}, Lcom/nuance/sample/controllers/SMSController;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 467
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SMSController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 468
    return-void
.end method

.method public static getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 585
    sget-object v0, Lcom/nuance/sample/controllers/SMSController;->message:Ljava/lang/String;

    return-object v0
.end method

.method private removeHyphenForPhonenumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "originNumber"    # Ljava/lang/String;

    .prologue
    .line 413
    const/4 v0, 0x0

    .line 415
    .local v0, "converted":Ljava/lang/String;
    const-string/jumbo v1, "-"

    const-string/jumbo v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 416
    return-object v0
.end method

.method public static setMessage(Ljava/lang/String;)V
    .locals 0
    .param p0, "messageValue"    # Ljava/lang/String;

    .prologue
    .line 589
    sput-object p0, Lcom/nuance/sample/controllers/SMSController;->message:Ljava/lang/String;

    .line 590
    return-void
.end method

.method private startInboxSearch()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 420
    const-string/jumbo v3, "VAC_DRIVELINK"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 421
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getNameIndex(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 423
    const-string/jumbo v3, "DM_LIST_SELECTED"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getMultiNameIndex(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 428
    const/4 v0, 0x0

    .line 430
    .local v0, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v0

    .line 432
    iget-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v8, :cond_2

    .line 433
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 434
    const v4, 0x7f0a0069

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 435
    iget-object v6, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedInobxList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getTo()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 433
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 437
    const-string/jumbo v3, "DM_SMS_INBOX_SEARCH_LIST"

    .line 436
    invoke-static {v3, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 440
    :cond_2
    const-string/jumbo v3, "DM_LIST_SELECTED"

    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto :goto_0

    .line 443
    .end local v0    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_3
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 444
    .local v1, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iget-object v3, p0, Lcom/nuance/sample/controllers/SMSController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    const-string/jumbo v4, "Pn"

    invoke-static {v3, v4, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 445
    .local v2, "pn":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 446
    const-string/jumbo v3, ""

    const-string/jumbo v4, " "

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 447
    invoke-virtual {p0, v2}, Lcom/nuance/sample/controllers/SMSController;->setTo(Ljava/lang/String;)V

    .line 452
    :cond_4
    const-string/jumbo v3, "DM_SMS_INBOX"

    invoke-static {v3, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0
.end method


# virtual methods
.method protected followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 13
    .param p1, "matchedContact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v12, 0x0

    const v9, 0x7f0a014a

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 617
    iput-boolean v10, p0, Lcom/nuance/sample/controllers/SMSController;->appendMode:Z

    .line 618
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 620
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v7, "VAC_DRIVELINK"

    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 621
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 622
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 624
    .local v2, "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 625
    iput-object p1, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 627
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v3, v7, :cond_0

    .line 633
    :goto_1
    const-string/jumbo v7, "DM_SMS_COMPLETE"

    invoke-virtual {p0, v7, v2, v12}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    .line 737
    .end local v2    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "i":I
    :goto_2
    return-void

    .line 628
    .restart local v2    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v3    # "i":I
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v7, v7, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    if-ne v7, v11, :cond_1

    .line 629
    iput v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_1

    .line 627
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 647
    .end local v2    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "i":I
    :cond_2
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 648
    .restart local v2    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v1

    .line 651
    .local v1, "flowID":Ljava/lang/String;
    const-string/jumbo v7, "DM_SMS_TYPE"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 652
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v3, v7, :cond_4

    .line 702
    :cond_3
    :goto_4
    iput-object p1, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 703
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 704
    iget-object v8, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 705
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v7, v7, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 704
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 705
    if-eqz v7, :cond_9

    .line 706
    iget-object v4, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 707
    .local v4, "phonNumber":Ljava/lang/String;
    const-string/jumbo v7, ""

    const-string/jumbo v8, " "

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 709
    new-array v7, v11, [Ljava/lang/Object;

    aput-object v4, v7, v10

    .line 708
    invoke-virtual {v0, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 724
    .end local v4    # "phonNumber":Ljava/lang/String;
    :goto_5
    const-string/jumbo v7, "DM_SMS_COMPOSE"

    invoke-virtual {p0, v7, v2, v12}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    goto :goto_2

    .line 653
    :cond_4
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v7, v7, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    if-ne v7, v11, :cond_5

    .line 654
    iput v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_4

    .line 652
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 659
    .end local v3    # "i":I
    :cond_6
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getPhoneType()I

    move-result v6

    .line 661
    .local v6, "type":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_6
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v3, v7, :cond_7

    .line 668
    :goto_7
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getPhoneType()I

    move-result v7

    if-ltz v7, :cond_3

    .line 669
    iget v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_3

    .line 670
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ne v3, v7, :cond_3

    .line 672
    packed-switch v6, :pswitch_data_0

    .line 686
    const v5, 0x7f0a035d

    .line 690
    .local v5, "temp":I
    :goto_8
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 691
    const v8, 0x7f0a0138

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    .line 692
    iget-object v9, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v9, v8, v10

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 693
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    .line 690
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 694
    iput-object p1, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 695
    const-string/jumbo v7, "DM_SMS_TYPE"

    invoke-virtual {p0, v7, v2, v12}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    goto/16 :goto_2

    .line 662
    .end local v5    # "temp":I
    :cond_7
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v7

    if-ne v7, v6, :cond_8

    .line 663
    iput v3, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_7

    .line 661
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 674
    :pswitch_0
    const v5, 0x7f0a035c

    .line 675
    .restart local v5    # "temp":I
    goto :goto_8

    .line 677
    .end local v5    # "temp":I
    :pswitch_1
    const v5, 0x7f0a035d

    .line 678
    .restart local v5    # "temp":I
    goto :goto_8

    .line 680
    .end local v5    # "temp":I
    :pswitch_2
    const v5, 0x7f0a035e

    .line 681
    .restart local v5    # "temp":I
    goto :goto_8

    .line 683
    .end local v5    # "temp":I
    :pswitch_3
    const v5, 0x7f0a035f

    .line 684
    .restart local v5    # "temp":I
    goto :goto_8

    .line 711
    .end local v5    # "temp":I
    .end local v6    # "type":I
    :cond_9
    iget-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v7, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v7, :cond_a

    .line 713
    iget-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v7, v7, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 715
    new-array v7, v11, [Ljava/lang/Object;

    .line 716
    iget-object v8, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    aput-object v8, v7, v10

    .line 714
    invoke-virtual {v0, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto/16 :goto_5

    .line 719
    :cond_a
    new-array v7, v11, [Ljava/lang/Object;

    .line 720
    iget-object v8, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v8, v7, v10

    .line 718
    invoke-virtual {v0, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    goto/16 :goto_5

    .line 728
    .end local v1    # "flowID":Ljava/lang/String;
    .end local v2    # "flowParam":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "i":I
    :cond_b
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_c

    .line 729
    sget-object v7, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

    new-array v8, v11, [Ljava/lang/String;

    .line 730
    const v9, 0x7f0a0024

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    .line 729
    invoke-virtual {p0, v7, v8}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_2

    .line 732
    :cond_c
    sget-object v7, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    new-array v8, v11, [Ljava/lang/String;

    .line 733
    const v9, 0x7f0a0025

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    .line 732
    invoke-virtual {p0, v7, v8}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_2

    .line 672
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getActionConfirmationKeys()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 579
    invoke-super {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionConfirmationKeys()Ljava/util/List;

    move-result-object v0

    .line 580
    .local v0, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v1, "send"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581
    return-object v0
.end method

.method protected getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 481
    const-string/jumbo v0, "text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$TextTypeActionHandler;)V

    .line 574
    :goto_0
    return-object v0

    .line 483
    :cond_0
    const-string/jumbo v0, "contact"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 484
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$SMSContactTypeActionHandler;)V

    goto :goto_0

    .line 485
    :cond_1
    const-string/jumbo v0, "append"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 486
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$AppendTextActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SMSController$AppendTextActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$AppendTextActionHandler;)V

    goto :goto_0

    .line 487
    :cond_2
    const-string/jumbo v0, "replace"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 488
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$ReplaceTextActionHandler;)V

    goto :goto_0

    .line 489
    :cond_3
    const-string/jumbo v0, "recommendations"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 490
    const-string/jumbo v0, "VAC_DRIVELINK"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 491
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$2;

    invoke-direct {v0, p0, p0}, Lcom/nuance/sample/controllers/SMSController$2;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V

    goto :goto_0

    .line 501
    :cond_4
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$3;

    invoke-direct {v0, p0, p0}, Lcom/nuance/sample/controllers/SMSController$3;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    goto :goto_0

    .line 513
    :cond_5
    const-string/jumbo v0, "inbox"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "logs"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 514
    :cond_6
    const-string/jumbo v0, "VAC_DRIVELINK"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 515
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$4;

    invoke-direct {v0, p0, p0}, Lcom/nuance/sample/controllers/SMSController$4;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V

    goto :goto_0

    .line 560
    :cond_7
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$5;

    invoke-direct {v0, p0, p0}, Lcom/nuance/sample/controllers/SMSController$5;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    goto :goto_0

    .line 571
    :cond_8
    const-string/jumbo v0, "type"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 572
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$SMSPhoneTypeActionHandler;)V

    goto/16 :goto_0

    .line 574
    :cond_9
    invoke-super {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 597
    const-class v0, Lcom/sec/android/automotive/drivelink/message/MessageActivity;

    return-object v0
.end method

.method protected getContactFieldID()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 607
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getContactNotFoundString()I
    .locals 1

    .prologue
    .line 796
    const v0, 0x7f0a0039

    return v0
.end method

.method protected getContactRequestString()I
    .locals 1

    .prologue
    .line 778
    const v0, 0x7f0a0026

    return v0
.end method

.method protected getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 612
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_TYPE:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getCreateTypeActionHandler()Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;)V

    return-object v0
.end method

.method protected getMultiplePhoneTypeString()I
    .locals 1

    .prologue
    .line 791
    const v0, 0x7f0a05f3

    return v0
.end method

.method protected getSearchContactType()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 602
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method protected multiContactsSearched(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 759
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 760
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object p1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 762
    const-string/jumbo v1, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPromptString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 763
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getTo()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 761
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 764
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mMessageText:Ljava/lang/String;

    .line 766
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getPhoneType()I

    move-result v1

    if-ltz v1, :cond_0

    .line 767
    sget-object v1, Lcom/nuance/sample/controllers/SMSController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[multiContactsSearched] getType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getPhoneType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getPhoneType()I

    move-result v1

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 772
    :cond_0
    const-string/jumbo v1, "DM_SMS_CONTACT_SEARCH_LIST"

    .line 771
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 773
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/nuance/sample/controllers/SMSController;->setMessage(Ljava/lang/String;)V

    .line 774
    return-void
.end method

.method protected varargs noContactsSearched([Ljava/lang/String;)V
    .locals 7
    .param p1, "prompts"    # [Ljava/lang/String;

    .prologue
    .line 743
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 745
    .local v0, "buf":Ljava/lang/StringBuffer;
    array-length v5, p1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 750
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 752
    .local v2, "prompt":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 753
    .local v1, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    .line 754
    const-string/jumbo v4, "DM_SMS_CONTACT"

    invoke-static {v4, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 756
    return-void

    .line 745
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v2    # "prompt":Ljava/lang/String;
    :cond_0
    aget-object v3, p1, v4

    .line 746
    .local v3, "str":Ljava/lang/String;
    const-string/jumbo v6, ". "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 747
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 745
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method noSupport()V
    .locals 3

    .prologue
    .line 193
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 194
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 195
    const v2, 0x7f0a0127

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "systemTurnText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 197
    new-instance v2, Lcom/nuance/sample/controllers/SMSController$1;

    invoke-direct {v2, p0}, Lcom/nuance/sample/controllers/SMSController$1;-><init>(Lcom/nuance/sample/controllers/SMSController;)V

    .line 196
    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 229
    return-void
.end method

.method protected runFinalAction()Z
    .locals 3

    .prologue
    .line 783
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 784
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    sget-object v1, Lcom/nuance/sample/controllers/SMSController;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[runFinalAction] "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    const-string/jumbo v1, "DM_SMS_SEND"

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 786
    const/4 v1, 0x1

    return v1
.end method

.method protected varargs startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V
    .locals 10
    .param p1, "fieldId"    # Lcom/nuance/sample/CCFieldIds;
    .param p2, "prompts"    # [Ljava/lang/String;

    .prologue
    .line 98
    const-string/jumbo v4, ""

    .line 99
    .local v4, "prompt":Ljava/lang/String;
    array-length v7, p2

    const/4 v6, 0x0

    :goto_0
    if-lt v6, v7, :cond_0

    .line 105
    const-string/jumbo v6, "VAC_DRIVELINK"

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 106
    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "flowID":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 142
    .end local v0    # "flowID":Ljava/lang/String;
    :goto_1
    return-void

    .line 99
    :cond_0
    aget-object v5, p2, v6

    .line 100
    .local v5, "str":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 101
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, ". "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 102
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 99
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 111
    .end local v5    # "str":Ljava/lang/String;
    .restart local v0    # "flowID":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->name()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "CC_SMS_CONTACT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 112
    const-string/jumbo v6, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 113
    const-string/jumbo v0, "DM_SMS_CONTACT"

    .line 116
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 117
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 118
    .local v1, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_1

    .line 120
    .end local v1    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_4
    invoke-virtual {p0, v4}, Lcom/nuance/sample/controllers/SMSController;->showAndSay(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    .line 122
    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v7

    .line 121
    invoke-interface {v6, v7}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 123
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->startRecording()V

    goto :goto_1

    .line 129
    .end local v0    # "flowID":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->isCalledFromMain()Z

    move-result v3

    .line 130
    .local v3, "isCalledFromMain":Z
    if-eqz v3, :cond_6

    .line 131
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getActivityClass()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 133
    const-string/jumbo v6, "extra_prompt_string"

    invoke-virtual {v2, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string/jumbo v6, "extra_field_id"

    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 137
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    invoke-virtual {p0, v4}, Lcom/nuance/sample/controllers/SMSController;->showAndSay(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    .line 139
    invoke-virtual {p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v7

    .line 138
    invoke-interface {v6, v7}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 140
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SMSController;->startRecording()V

    goto/16 :goto_1
.end method
