.class public final enum Lcom/nuance/sample/controllers/SampleLocationController$Mode;
.super Ljava/lang/Enum;
.source "SampleLocationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleLocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/controllers/SampleLocationController$Mode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AddWaypoint:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum DestinationChanged:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum GroupJoinConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum GroupLocationExpiration:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum GroupShare:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum Navigation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum Request:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum RequestApproved:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum RequestShareConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

.field public static final enum Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "Navigation"

    invoke-direct {v0, v1, v3}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Navigation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 77
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "AddWaypoint"

    invoke-direct {v0, v1, v4}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->AddWaypoint:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 78
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "Share"

    invoke-direct {v0, v1, v5}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 79
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "GroupShare"

    invoke-direct {v0, v1, v6}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupShare:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 80
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "Request"

    invoke-direct {v0, v1, v7}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Request:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 81
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "RequestShareConfirmation"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestShareConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 82
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "GroupJoinConfirmation"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupJoinConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 83
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "DestinationChanged"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->DestinationChanged:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 84
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "RequestApproved"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestApproved:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 85
    new-instance v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    const-string/jumbo v1, "GroupLocationExpiration"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/controllers/SampleLocationController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupLocationExpiration:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 75
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Navigation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->AddWaypoint:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupShare:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Request:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestShareConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupJoinConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->DestinationChanged:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestApproved:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupLocationExpiration:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ENUM$VALUES:[Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleLocationController$Mode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/controllers/SampleLocationController$Mode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->ENUM$VALUES:[Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
