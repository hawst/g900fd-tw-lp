.class Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;
.super Ljava/lang/Object;
.source "SampleLocationController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleLocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MapActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleLocationController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SampleLocationController;Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;)V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleLocationController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v3, 0x0

    .line 458
    const-string/jumbo v0, "SampleLocationController"

    const-string/jumbo v1, "Open Map by voice command"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    const-string/jumbo v0, "DM_LOCATION"

    .line 461
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v1

    .line 460
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 461
    if-nez v0, :cond_1

    .line 476
    :cond_0
    :goto_0
    return v3

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v0}, Lcom/nuance/sample/controllers/SampleLocationController;->checkGoogleAccountAndTtsToast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->getInstance()Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelink/location/geolocation/Geolocation;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 470
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleLocationController$MapActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleLocationController;

    # invokes: Lcom/nuance/sample/controllers/SampleLocationController;->notifyEnableGPS()V
    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleLocationController;->access$0(Lcom/nuance/sample/controllers/SampleLocationController;)V

    goto :goto_0

    .line 474
    :cond_2
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->getInstance()Lcom/sec/android/automotive/drivelink/location/NavigationActivity;

    move-result-object v0

    .line 475
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    const/4 v2, 0x0

    .line 474
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/location/NavigationActivity;->startNavigation(Landroid/content/Context;Lcom/sec/android/automotive/drivelink/framework/iface/data/DLLocation;)V

    goto :goto_0
.end method
