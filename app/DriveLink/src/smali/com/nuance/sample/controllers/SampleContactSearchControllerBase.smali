.class public abstract Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;
.super Lcom/nuance/sample/controllers/SampleBaseDMController;
.source "SampleContactSearchControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$IntegratedFlowHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field protected static contactData:Lcom/vlingo/core/internal/contacts/ContactData;

.field protected static contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field private static contactMatch_static:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field protected static contactMatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected currentPage:I

.field private phoneTypeOrder:I

.field private phoneTypeParsed:I

.field private to:Ljava/lang/String;

.field private video:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 37
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->currentPage:I

    .line 45
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->video:Ljava/lang/Boolean;

    .line 67
    iput v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->phoneTypeParsed:I

    .line 68
    iput v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->phoneTypeOrder:I

    .line 37
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getContactData()Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactData:Lcom/vlingo/core/internal/contacts/ContactData;

    return-object v0
.end method

.method public static getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 1

    .prologue
    .line 387
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    return-object v0
.end method

.method public static getContactMatches()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactMatches:Ljava/util/List;

    return-object v0
.end method

.method public static setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V
    .locals 0
    .param p0, "contactDataValue"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 399
    sput-object p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactData:Lcom/vlingo/core/internal/contacts/ContactData;

    .line 400
    return-void
.end method

.method public static setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p0, "contactMatchValue"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 391
    sput-object p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 392
    return-void
.end method

.method public static setContactMatches(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 411
    .local p0, "contactMatchesVar":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    sput-object p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactMatches:Ljava/util/List;

    .line 412
    return-void
.end method

.method private startDLContactSearch()V
    .locals 3

    .prologue
    .line 323
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v0

    .line 325
    .local v0, "dlServiceInterface":Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;
    new-instance v1, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;

    invoke-direct {v1, p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$2;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->setOnDriveLinkContactListener(Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface$OnDriveLinkContactListener;)V

    .line 353
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;->requestSearchedContactList(Landroid/content/Context;Ljava/lang/String;)V

    .line 354
    return-void
.end method


# virtual methods
.method public buildPhoneStringsForCurrentPage(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 6
    .param p1, "types"    # Ljava/lang/StringBuilder;
    .param p2, "ttsTypes"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p3, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v3

    .line 263
    .local v3, "pageSize":I
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPage()I

    move-result v2

    .line 264
    .local v2, "page":I
    add-int/lit8 v4, v2, -0x1

    mul-int v1, v4, v3

    .local v1, "i":I
    :goto_0
    mul-int v4, v2, v3

    if-ge v1, v4, :cond_0

    .line 265
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    .line 264
    if-lt v1, v4, :cond_1

    .line 278
    :cond_0
    return-void

    .line 266
    :cond_1
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 267
    .local v0, "contactPhone":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 268
    const-string/jumbo v4, ", "

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string/jumbo v4, ", "

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_2
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 272
    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 274
    const-string/jumbo v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 276
    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 275
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected filterPhonesByType(ILjava/util/List;)Ljava/util/List;
    .locals 4
    .param p1, "phoneType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    .local p2, "originalPhoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 231
    .local v1, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v2, -0x1

    if-eq p1, v2, :cond_3

    .line 232
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 242
    :cond_1
    :goto_1
    return-object v1

    .line 232
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 233
    .local v0, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v3, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v3, p1, :cond_0

    .line 234
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 238
    .end local v0    # "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_3
    if-eqz p2, :cond_1

    .line 239
    invoke-interface {v1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method protected abstract followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
.end method

.method protected abstract getContactFieldID()Lcom/nuance/sample/CCFieldIds;
.end method

.method protected abstract getContactNotFoundString()I
.end method

.method protected abstract getContactRequestString()I
.end method

.method public getContactStringForCurrentPage(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 307
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v4

    .line 308
    .local v4, "pageSize":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    .local v1, "contactsStr":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPage()I

    move-result v3

    .line 310
    .local v3, "page":I
    add-int/lit8 v5, v3, -0x1

    mul-int v2, v5, v4

    .local v2, "i":I
    :goto_0
    mul-int v5, v3, v4

    if-ge v2, v5, :cond_0

    .line 311
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 310
    if-lt v2, v5, :cond_1

    .line 318
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 312
    :cond_1
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 313
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 314
    const-string/jumbo v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    :cond_2
    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected abstract getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;
.end method

.method protected abstract getMultiplePhoneTypeString()I
.end method

.method public getPage()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->currentPage:I

    return v0
.end method

.method public getPhoneType()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->phoneTypeParsed:I

    return v0
.end method

.method public getPhoneTypeOrder()I
    .locals 1

    .prologue
    .line 440
    iget v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->phoneTypeOrder:I

    return v0
.end method

.method protected getPhoneTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;
    .locals 2
    .param p1, "contactPhone"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 281
    const v0, 0x7f0a05bf

    .line 282
    .local v0, "strId":I
    iget v1, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    packed-switch v1, :pswitch_data_0

    .line 302
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 284
    :pswitch_1
    const v0, 0x7f0a05be

    .line 285
    goto :goto_0

    .line 287
    :pswitch_2
    const v0, 0x7f0a05bd

    .line 288
    goto :goto_0

    .line 290
    :pswitch_3
    const v0, 0x7f0a05c0

    .line 291
    goto :goto_0

    .line 293
    :pswitch_4
    const v0, 0x7f0a05bf

    .line 294
    goto :goto_0

    .line 296
    :pswitch_5
    const v0, 0x7f0a0624

    .line 297
    goto :goto_0

    .line 299
    :pswitch_6
    const v0, 0x7f0a0625

    goto :goto_0

    .line 282
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected abstract getSearchContactType()Lcom/vlingo/core/internal/contacts/ContactType;
.end method

.method public getStaticContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 1

    .prologue
    .line 423
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactMatch_static:Lcom/vlingo/core/internal/contacts/ContactMatch;

    return-object v0
.end method

.method public getTo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->to:Ljava/lang/String;

    return-object v0
.end method

.method public getVideo()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->video:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected abstract multiContactsSearched(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation
.end method

.method protected varargs abstract noContactsSearched([Ljava/lang/String;)V
.end method

.method public setPage(I)V
    .locals 0
    .param p1, "page"    # I

    .prologue
    .line 403
    iput p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->currentPage:I

    .line 404
    return-void
.end method

.method public setPhoneType(I)V
    .locals 0
    .param p1, "phoneTypeParsed"    # I

    .prologue
    .line 375
    iput p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->phoneTypeParsed:I

    .line 376
    return-void
.end method

.method public setPhoneType(Ljava/lang/String;)V
    .locals 1
    .param p1, "phoneType"    # Ljava/lang/String;

    .prologue
    .line 435
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneType(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->phoneTypeParsed:I

    .line 436
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeOrder(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setPhoneTypeOrder(I)V

    .line 437
    return-void
.end method

.method public setPhoneTypeOrder(I)V
    .locals 0
    .param p1, "phoneTypeOrder"    # I

    .prologue
    .line 444
    iput p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->phoneTypeOrder:I

    .line 445
    return-void
.end method

.method public setStaticContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "contactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 419
    sput-object p1, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->contactMatch_static:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 420
    return-void
.end method

.method public setTo(Ljava/lang/String;)V
    .locals 0
    .param p1, "to"    # Ljava/lang/String;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->to:Ljava/lang/String;

    .line 384
    return-void
.end method

.method public setVideo(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "video"    # Ljava/lang/Boolean;

    .prologue
    .line 431
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->video:Ljava/lang/Boolean;

    .line 432
    return-void
.end method

.method protected startContactSearch()V
    .locals 14

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 72
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startContactSearch"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const-string/jumbo v0, "VAC_CONTACT_MATCHER"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->startDLContactSearch()V

    .line 225
    :goto_0
    return-void

    .line 78
    :cond_0
    const-string/jumbo v0, "VAC_DRIVELINK"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 79
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "feature VAC_DRIVELINK enabled"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v12

    .line 82
    .local v12, "flowID":Ljava/lang/String;
    const-string/jumbo v0, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "DM_SMS_CONTACT_SEARCH_LIST"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    const-string/jumbo v0, "DM_LOCATION_CONTACT_SHARE_SEARCH_LIST"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "DM_LOCATION_CONTACT_REQUEST_SEARCH_LIST"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getNameIndex(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "DLListItemUtils.getNameIndex(getTo())"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-static {v12}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v13

    .line 88
    .local v13, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getVideo()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    if-eqz v13, :cond_2

    .line 90
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a013c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 91
    iput-boolean v7, v13, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    .line 95
    :cond_2
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneType()I

    move-result v0

    if-ltz v0, :cond_3

    .line 96
    if-eqz v13, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneType()I

    move-result v0

    iput v0, v13, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 100
    :cond_3
    const-string/jumbo v0, "DM_LIST_SELECTED"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    .end local v13    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_4
    invoke-static {v5}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatches(Ljava/util/List;)V

    .line 109
    .end local v12    # "flowID":Ljava/lang/String;
    :cond_5
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 111
    sget-object v0, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "new PhoneContactMatcher"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    new-instance v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;

    invoke-direct {v2, p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase$1;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;)V

    .line 223
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getSearchContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v3

    .line 224
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneType()I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_6

    new-array v4, v7, [I

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getPhoneType()I

    move-result v6

    aput v6, v4, v10

    :goto_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getTo()Ljava/lang/String;

    move-result-object v8

    const/high16 v9, 0x42c80000    # 100.0f

    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->getContactMatches()Ljava/util/List;

    move-result-object v11

    move-object v6, v5

    move-object v7, v5

    .line 112
    invoke-direct/range {v0 .. v11}, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    goto/16 :goto_0

    :cond_6
    move-object v4, v5

    .line 224
    goto :goto_1
.end method

.method protected updateContactMatchPhonesAvailable(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 8
    .param p1, "matchedContact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;"
        }
    .end annotation

    .prologue
    .line 247
    .local p2, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 248
    iget-object v2, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    .line 249
    iget-wide v3, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 250
    iget-object v5, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->lookupKey:Ljava/lang/String;

    .line 251
    iget-boolean v6, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    .line 247
    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 252
    .end local p1    # "matchedContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .local v0, "matchedContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {v0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 253
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 256
    return-object v0

    .line 253
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 254
    .local v7, "phone":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_0
.end method
