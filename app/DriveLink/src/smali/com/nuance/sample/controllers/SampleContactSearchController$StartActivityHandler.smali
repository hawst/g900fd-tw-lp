.class public abstract Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;
.super Ljava/lang/Object;
.source "SampleContactSearchController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleContactSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "StartActivityHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;


# direct methods
.method protected constructor <init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v3, 0x0

    .line 483
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;->getActivity()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_0

    .line 484
    invoke-static {}, Lcom/nuance/drivelink/handlers/DLUnsupportedDomainHandler;->showErrorTTS()V

    .line 492
    :goto_0
    return v3

    .line 487
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;->getActivity()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 488
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 489
    const-string/jumbo v1, "extra_prompt_string"

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;->getPrompt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 490
    const-string/jumbo v1, "extra_field_id"

    iget-object v2, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v2}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 491
    iget-object v1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$StartActivityHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected abstract getActivity()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method protected abstract getPrompt()Ljava/lang/String;
.end method
