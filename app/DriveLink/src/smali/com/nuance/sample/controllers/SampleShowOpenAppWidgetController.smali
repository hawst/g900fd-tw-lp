.class public Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;
.super Lcom/nuance/sample/controllers/SampleBaseDMController;
.source "SampleShowOpenAppWidgetController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;-><init>()V

    return-void
.end method

.method private getAppItemsWithPage(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;
    .locals 8
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    invoke-static {p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v3

    .line 80
    .local v3, "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ListControlData;->getCurrentPage()I

    move-result v5

    .line 81
    .local v5, "page":I
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ListControlData;->getPageSize()I

    move-result v6

    .line 82
    .local v6, "pageSize":I
    invoke-static {p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v1

    .line 83
    .local v1, "appDetailsStoredList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v2, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    add-int/lit8 v7, v5, -0x1

    mul-int v4, v7, v6

    .local v4, "i":I
    :goto_0
    mul-int v7, v5, v6

    if-ge v4, v7, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-lt v4, v7, :cond_1

    .line 88
    :cond_0
    return-object v2

    .line 85
    :cond_1
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 86
    .local v0, "app":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x0

    .line 33
    const-string/jumbo v5, "Command"

    invoke-static {p1, v5, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "command":Ljava/lang/String;
    const-string/jumbo v5, "next"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "prev"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 35
    :cond_0
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v1

    .line 36
    .local v1, "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    const/4 v3, 0x0

    .line 37
    .local v3, "errorPageChange":Ljava/lang/String;
    const-string/jumbo v5, "next"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 39
    :try_start_0
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    if-eqz v3, :cond_1

    .line 51
    invoke-virtual {p0, v3}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->showAndSay(Ljava/lang/String;)V

    .line 54
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a0293

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 55
    .local v4, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 56
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v6, 0x0

    invoke-direct {p0, p2}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->getAppItemsWithPage(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v7

    invoke-interface {p2, v5, v6, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 57
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->startRecording()V

    .line 63
    .end local v1    # "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    .end local v3    # "errorPageChange":Ljava/lang/String;
    .end local v4    # "text":Ljava/lang/String;
    :cond_2
    return v8

    .line 40
    .restart local v1    # "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    .restart local v3    # "errorPageChange":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 41
    .local v2, "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    const-string/jumbo v3, "There are no more items. "

    .line 43
    goto :goto_0

    .line 45
    .end local v2    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    :cond_3
    :try_start_1
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V
    :try_end_1
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 46
    :catch_1
    move-exception v2

    .line 47
    .restart local v2    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    const-string/jumbo v3, "There are no more items. "

    goto :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;I)V

    .line 101
    :goto_1
    return-void

    .line 95
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    move-object v0, p2

    .line 98
    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 99
    .local v0, "ai":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    invoke-virtual {p0, v1, v2}, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    invoke-virtual {v1, v0}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->queue()V

    goto :goto_1
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method
