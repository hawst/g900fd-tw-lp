.class public Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;
.super Ljava/lang/Object;
.source "SampleContactSearchController.java"

# interfaces
.implements Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SampleContactSearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ContactTypeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;


# direct methods
.method protected constructor <init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 21
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 369
    const-string/jumbo v12, "Which"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    .line 370
    .local v11, "which":Ljava/lang/String;
    const-string/jumbo v12, "Command"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 371
    .local v2, "command":Ljava/lang/String;
    if-eqz v11, :cond_2

    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactMatches()Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 373
    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactMatches()Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 374
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactMatchesCurrentPage()Ljava/util/List;

    move-result-object v12

    invoke-static {v12, v11}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-static {v12}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 375
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    const/4 v13, 0x1

    iput v13, v12, Lcom/nuance/sample/controllers/SampleContactSearchController;->currentPage:I

    .line 381
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchController;->followWithSingleContact()V

    .line 437
    :cond_1
    :goto_0
    const/4 v12, 0x0

    :goto_1
    return v12

    .line 382
    :cond_2
    const-string/jumbo v12, "next"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    const-string/jumbo v12, "prev"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 383
    :cond_3
    invoke-static {}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactMatches()Ljava/util/List;

    move-result-object v3

    .line 384
    .local v3, "contactMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const-string/jumbo v6, ""

    .line 385
    .local v6, "errorPageChange":Ljava/lang/String;
    if-nez v3, :cond_4

    .line 386
    const-string/jumbo v6, "There are no items available"

    .line 398
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v12, v3}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactStringForCurrentPage(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    .line 399
    .local v4, "contactsStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v13}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v13

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    aput-object v6, v14, v15

    const/4 v15, 0x1

    aput-object v4, v14, v15

    const/4 v15, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v16

    const v17, 0x7f0a0068

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getTo()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto :goto_0

    .line 389
    .end local v4    # "contactsStr":Ljava/lang/String;
    :cond_4
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v13

    invoke-virtual {v12, v2, v13}, Lcom/nuance/sample/controllers/SampleContactSearchController;->changePage(Ljava/lang/String;I)V
    :try_end_0
    .catch Lcom/nuance/sample/controllers/CannotMoveToNextPageException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/nuance/sample/controllers/PageException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    .line 390
    :catch_0
    move-exception v5

    .line 391
    .local v5, "e":Lcom/nuance/sample/controllers/CannotMoveToNextPageException;
    const-string/jumbo v6, "There are no more items. "

    goto :goto_2

    .line 392
    .end local v5    # "e":Lcom/nuance/sample/controllers/CannotMoveToNextPageException;
    :catch_1
    move-exception v5

    .line 393
    .local v5, "e":Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;
    const-string/jumbo v6, "You are currently at first page. "

    goto :goto_2

    .line 400
    .end local v3    # "contactMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v5    # "e":Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;
    .end local v6    # "errorPageChange":Ljava/lang/String;
    :cond_5
    if-eqz v11, :cond_6

    .line 401
    invoke-static {v11}, Lcom/nuance/drivelink/utils/DLListItemUtils;->getOrdinalIndex(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 402
    const-string/jumbo v12, "DM_LIST_ORDINAL"

    invoke-static {v12}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setFlowCommand(Ljava/lang/String;)V

    .line 403
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 406
    :cond_6
    const-string/jumbo v12, "To"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 407
    .local v10, "to":Ljava/lang/String;
    const-string/jumbo v12, "Pn"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 409
    .local v9, "pn":Ljava/lang/String;
    if-eqz v9, :cond_7

    if-nez v10, :cond_7

    .line 410
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    # invokes: Lcom/nuance/sample/controllers/SampleContactSearchController;->followWithPhoneNumber(Ljava/lang/String;)V
    invoke-static {v12, v9}, Lcom/nuance/sample/controllers/SampleContactSearchController;->access$0(Lcom/nuance/sample/controllers/SampleContactSearchController;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 412
    :cond_7
    if-eqz v10, :cond_8

    .line 413
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v12, v10}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setTo(Ljava/lang/String;)V

    .line 415
    :cond_8
    const-string/jumbo v12, "PhoneType"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    .line 416
    .local v8, "phoneType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getTo()Ljava/lang/String;

    move-result-object v1

    .line 417
    .local v1, "address":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_b

    .line 418
    if-eqz v8, :cond_9

    .line 419
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v12, v8}, Lcom/nuance/sample/controllers/SampleContactSearchController;->setPhoneType(Ljava/lang/String;)V

    .line 421
    :cond_9
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v7

    .line 422
    .local v7, "flowID":Ljava/lang/String;
    const-string/jumbo v12, "DM_SMS_INBOX"

    invoke-virtual {v7, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 423
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    # invokes: Lcom/nuance/sample/controllers/SampleContactSearchController;->startInboxSearch()V
    invoke-static {v12}, Lcom/nuance/sample/controllers/SampleContactSearchController;->access$1(Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    goto/16 :goto_0

    .line 425
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v12}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startContactSearch()V

    goto/16 :goto_0

    .line 428
    .end local v7    # "flowID":Ljava/lang/String;
    :cond_b
    const-string/jumbo v12, "VAC_DRIVELINK"

    invoke-static {v12}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 429
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v7

    .line 430
    .restart local v7    # "flowID":Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowParams(Ljava/lang/String;)Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    move-result-object v12

    invoke-static {v7, v12}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startDmFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto/16 :goto_0

    .line 432
    .end local v7    # "flowID":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    invoke-virtual {v13}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v13

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/controllers/SampleContactSearchController$ContactTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SampleContactSearchController;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getContext()Landroid/content/Context;

    move-result-object v16

    const v17, 0x7f0a05e8

    invoke-virtual/range {v16 .. v17}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/nuance/sample/controllers/SampleContactSearchController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 394
    .end local v1    # "address":Ljava/lang/String;
    .end local v8    # "phoneType":Ljava/lang/String;
    .end local v9    # "pn":Ljava/lang/String;
    .end local v10    # "to":Ljava/lang/String;
    .restart local v3    # "contactMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .restart local v6    # "errorPageChange":Ljava/lang/String;
    :catch_2
    move-exception v12

    goto/16 :goto_2
.end method
