.class public Lcom/nuance/sample/controllers/SampleLocalSearchController;
.super Lcom/nuance/sample/controllers/SampleBaseDMController;
.source "SampleLocalSearchController.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/nuance/sample/controllers/SampleLocalSearchController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/controllers/SampleLocalSearchController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleBaseDMController;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 28
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 29
    sget-object v4, Lcom/nuance/sample/controllers/SampleLocalSearchController;->TAG:Ljava/lang/String;

    .line 30
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "SampleLocalSearchController handling action="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 31
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 30
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 29
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const-string/jumbo v4, "Search"

    invoke-static {p1, v4, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "search":Ljava/lang/String;
    sget-object v4, Lcom/nuance/sample/controllers/SampleLocalSearchController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "SampleLocalSearchController search="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const-string/jumbo v4, "Text"

    invoke-static {p1, v4, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 48
    .local v3, "text":Ljava/lang/String;
    sget-object v4, Lcom/nuance/sample/controllers/SampleLocalSearchController;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "SampleLocalSearchController text="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/nuance/sample/controllers/SampleLocalSearchController;->mContext:Landroid/content/Context;

    .line 53
    if-nez v1, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 55
    const v5, 0x7f0a060b

    .line 54
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "systemTurnText":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->showAndSay(Ljava/lang/String;)V

    .line 88
    :goto_0
    return v7

    .line 58
    .end local v2    # "systemTurnText":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "gas"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 59
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "parking"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 60
    :cond_1
    const/4 v0, 0x0

    .line 61
    .local v0, "cheapest":Z
    if-eqz v3, :cond_2

    .line 65
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "cheap"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 66
    const/4 v0, 0x1

    .line 69
    :cond_2
    const/4 v2, 0x0

    .line 70
    .restart local v2    # "systemTurnText":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 71
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 72
    const v5, 0x7f0a060c

    new-array v6, v8, [Ljava/lang/Object;

    .line 73
    aput-object v1, v6, v7

    .line 71
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 78
    :goto_1
    invoke-virtual {p0, v2}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->showAndSay(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 80
    const-string/jumbo v5, "Samsung TBD query local search CP here"

    .line 79
    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 81
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 76
    const v5, 0x7f0a060d

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v1, v6, v7

    .line 75
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 83
    .end local v0    # "cheapest":Z
    .end local v2    # "systemTurnText":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 84
    const v5, 0x7f0a060e

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v1, v6, v7

    .line 83
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 85
    .restart local v2    # "systemTurnText":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/nuance/sample/controllers/SampleLocalSearchController;->showAndSay(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    const-class v0, Lcom/sec/android/automotive/drivelink/location/LocationActivity;

    return-object v0
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method
