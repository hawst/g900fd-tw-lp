.class public Lcom/nuance/sample/controllers/SampleDialPageController;
.super Lcom/nuance/sample/controllers/SampleContactSearchController;
.source "SampleDialPageController.java"


# static fields
.field private static final MODE:Ljava/lang/String; = "Mode"

.field private static final TAG:Ljava/lang/String;

.field private static final VIDEO_MODE_PARAM:Ljava/lang/String; = "videophone"


# instance fields
.field private addVideoNotSupportMessage:Z

.field private video:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/nuance/sample/controllers/SampleDialPageController;

    .line 49
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 48
    sput-object v0, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;-><init>()V

    .line 53
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->video:Ljava/lang/Boolean;

    .line 54
    iput-boolean v1, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    .line 47
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private extendArrayWithVideoCallPrompt([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "prompts"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 539
    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    .line 540
    .local v0, "fullPrompts":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 541
    const v2, 0x7f0a05f2

    .line 540
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 542
    const/4 v1, 0x1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 543
    return-object v0
.end method


# virtual methods
.method protected followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 17
    .param p1, "matchedContact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 79
    const v3, 0x7f0a0115

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 80
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 78
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 82
    .local v12, "prompt":Ljava/lang/String;
    const-string/jumbo v2, "VAC_DRIVELINK"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 101
    new-instance v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v10}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 102
    .local v10, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    move-object/from16 v0, p1

    iput-object v0, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 104
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[followAfterContactResolution]getVideo : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getVideo()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    .line 106
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[followAfterContactResolution]addVideoNotSupportMessage : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 107
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 106
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[followAfterContactResolution]getPhoneType : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 108
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v16

    .line 113
    .local v16, "type":I
    invoke-static {}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v8

    .line 114
    .local v8, "c":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v9

    .line 116
    .local v9, "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v9, :cond_0

    .line 118
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "!!!!!!!!!cl size = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "!!!!!!!!!contact match = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/vlingo/core/internal/contacts/ContactMatch;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "!!!!!!!!!contact match phone data = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :goto_0
    if-nez v9, :cond_6

    .line 127
    if-eqz p1, :cond_4

    .line 130
    invoke-static {}, Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceSingleton;->getDriveLinkServiceInterface()Lcom/sec/android/automotive/drivelink/framework/iface/DriveLinkServiceInterface;

    move-result-object v2

    .line 129
    check-cast v2, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;

    .line 130
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceInterfaceImp;->getDriveLinkServiceProvider()Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;

    move-result-object v2

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 130
    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/automotive/drivelink/framework/DriveLinkServiceProvider;->getPhoneNumberList(Landroid/content/Context;J)Ljava/util/ArrayList;

    move-result-object v7

    .line 133
    .local v7, "mNumberUserPhoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    new-instance v2, Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/framework/common/util/PhoneNumberOrderTypeComparator;-><init>()V

    invoke-static {v7, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 135
    new-instance v1, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 136
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 137
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 135
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/automotive/drivelink/framework/manager/content/contact/DLContactImp;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 140
    .local v1, "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/FlowSet;->getVideoCallSlotPrompt(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 148
    invoke-static {v1}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getContacMatch(Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v13

    .line 149
    .local v13, "temp":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v13}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v14

    .line 151
    .local v14, "tempData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-lt v11, v2, :cond_1

    .line 169
    :goto_2
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "!!!!!!!!!!!! flowParams.mUserPhoneTypeIndex = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v2, 0x0

    iput v2, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserIndex:I

    .line 173
    iput-object v13, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 175
    const-string/jumbo v2, "DM_DIAL"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10, v3}, Lcom/nuance/sample/controllers/SampleDialPageController;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    .line 302
    .end local v1    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v7    # "mNumberUserPhoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v8    # "c":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v9    # "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v10    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v11    # "i":I
    .end local v13    # "temp":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v14    # "tempData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v16    # "type":I
    :goto_3
    return-void

    .line 124
    .restart local v8    # "c":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v9    # "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .restart local v10    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .restart local v16    # "type":I
    :cond_0
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "!!!!!!!!!!!!!!!cl == null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 152
    .restart local v1    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .restart local v7    # "mNumberUserPhoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .restart local v11    # "i":I
    .restart local v13    # "temp":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v14    # "tempData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_1
    sget-object v3, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[followAfterContactResolution] type("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ") : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 153
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 152
    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v2

    if-ltz v2, :cond_2

    .line 156
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v2

    move/from16 v0, v16

    if-ne v2, v0, :cond_3

    .line 158
    iput v11, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_2

    .line 162
    :cond_2
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v2, v2, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 163
    iput v11, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_2

    .line 151
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 179
    .end local v1    # "d":Lcom/sec/android/automotive/drivelink/framework/iface/data/DLContact;
    .end local v7    # "mNumberUserPhoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/automotive/drivelink/framework/iface/data/DLPhoneNumber;>;"
    .end local v11    # "i":I
    .end local v13    # "temp":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v14    # "tempData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_4
    const/4 v15, 0x0

    .line 180
    .local v15, "tts":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0115

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 186
    :goto_4
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v12, v3, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v15, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/nuance/sample/controllers/SampleDialPageController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_3

    .line 183
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0115

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    goto :goto_4

    .line 202
    .end local v15    # "tts":Ljava/lang/String;
    :cond_6
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[followAfterContactResolution]"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_5
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-lt v11, v2, :cond_7

    .line 220
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v2

    if-ltz v2, :cond_a

    iget v2, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_a

    .line 221
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-ne v11, v2, :cond_a

    .line 224
    packed-switch v16, :pswitch_data_0

    .line 238
    const v13, 0x7f0a035d

    .line 242
    .local v13, "temp":I
    :goto_7
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "!!!!!!!!!phone type = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    move-object/from16 v0, p1

    iput-object v0, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 248
    const v3, 0x7f0a0138

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 249
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 250
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 247
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 252
    const-string/jumbo v2, "DM_DIAL_TYPE"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10, v3}, Lcom/nuance/sample/controllers/SampleDialPageController;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    goto/16 :goto_3

    .line 204
    .end local v13    # "temp":I
    :cond_7
    sget-object v3, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[followAfterContactResolution] type("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ") : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 205
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 204
    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v2

    if-ltz v2, :cond_8

    .line 208
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v2

    move/from16 v0, v16

    if-ne v2, v0, :cond_9

    .line 210
    iput v11, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto/16 :goto_6

    .line 214
    :cond_8
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    iget v2, v2, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    .line 215
    iput v11, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto/16 :goto_6

    .line 203
    :cond_9
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_5

    .line 226
    :pswitch_0
    const v13, 0x7f0a035c

    .line 227
    .restart local v13    # "temp":I
    goto/16 :goto_7

    .line 229
    .end local v13    # "temp":I
    :pswitch_1
    const v13, 0x7f0a035d

    .line 230
    .restart local v13    # "temp":I
    goto/16 :goto_7

    .line 232
    .end local v13    # "temp":I
    :pswitch_2
    const v13, 0x7f0a035e

    .line 233
    .restart local v13    # "temp":I
    goto/16 :goto_7

    .line 235
    .end local v13    # "temp":I
    :pswitch_3
    const v13, 0x7f0a035f

    .line 236
    .restart local v13    # "temp":I
    goto/16 :goto_7

    .line 255
    .end local v13    # "temp":I
    :cond_a
    iget v2, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_b

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-ne v11, v2, :cond_b

    .line 256
    sget-object v2, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "[followAfterContactResolution]"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const/4 v11, 0x0

    :goto_8
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-lt v11, v2, :cond_d

    .line 264
    :goto_9
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-ne v11, v2, :cond_b

    .line 265
    const/4 v2, 0x0

    iput v2, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    .line 269
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    if-eqz v2, :cond_c

    .line 270
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    .line 271
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 272
    const v3, 0x7f0a013c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 273
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 271
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 276
    :cond_c
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    .line 277
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 276
    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 278
    const-string/jumbo v2, "CM01"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 280
    const-string/jumbo v2, "DM_DIAL"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v12, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10, v3}, Lcom/nuance/sample/controllers/SampleDialPageController;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    goto/16 :goto_3

    .line 258
    :cond_d
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    .line 259
    iput v11, v10, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserPhoneTypeIndex:I

    goto :goto_9

    .line 257
    :cond_e
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 282
    .end local v8    # "c":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v9    # "cl":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v10    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v11    # "i":I
    .end local v16    # "type":I
    :cond_f
    const/4 v15, 0x0

    .line 283
    .restart local v15    # "tts":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 284
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isJapaneseString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 285
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 287
    const v3, 0x7f0a0115

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 289
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 286
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 298
    :goto_a
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 299
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v12, v3, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v15, v4, v5

    .line 298
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/nuance/sample/controllers/SampleDialPageController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_3

    .line 291
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 293
    const v3, 0x7f0a0115

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 295
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 292
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 291
    goto :goto_a

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getActionConfirmationKeys()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451
    invoke-super {p0}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionConfirmationKeys()Ljava/util/List;

    move-result-object v0

    .line 452
    .local v0, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v1, "execute"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453
    return-object v0
.end method

.method protected getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 417
    const-string/jumbo v0, "logs"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    new-instance v0, Lcom/nuance/sample/controllers/SampleDialPageController$2;

    invoke-direct {v0, p0, p0}, Lcom/nuance/sample/controllers/SampleDialPageController$2;-><init>(Lcom/nuance/sample/controllers/SampleDialPageController;Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    .line 446
    :goto_0
    return-object v0

    .line 431
    :cond_0
    const-string/jumbo v0, "recommendations"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 432
    new-instance v0, Lcom/nuance/sample/controllers/SampleDialPageController$3;

    invoke-direct {v0, p0, p0}, Lcom/nuance/sample/controllers/SampleDialPageController$3;-><init>(Lcom/nuance/sample/controllers/SampleDialPageController;Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    goto :goto_0

    .line 446
    :cond_1
    invoke-super {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController;->getActionHandler(Ljava/lang/String;)Lcom/nuance/sample/controllers/SampleContactSearchController$ActionHandler;

    move-result-object v0

    goto :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    const-class v0, Lcom/sec/android/automotive/drivelink/phone/PhoneActivity;

    return-object v0
.end method

.method protected getContactFieldID()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 373
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getContactNotFoundString()I
    .locals 1

    .prologue
    .line 553
    const v0, 0x7f0a0038

    return v0
.end method

.method protected getContactRequestString()I
    .locals 1

    .prologue
    .line 383
    const v0, 0x7f0a00b9

    return v0
.end method

.method protected getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 378
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_TYPE:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getMultiplePhoneTypeString()I
    .locals 1

    .prologue
    .line 548
    const v0, 0x7f0a006b

    return v0
.end method

.method protected getSearchContactType()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 368
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method public getVideo()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->video:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected multiContactsSearched(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactst":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 339
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 341
    .local v0, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object p1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mContactChoices:Ljava/util/List;

    .line 342
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getTo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchedText:Ljava/lang/String;

    .line 344
    const-string/jumbo v1, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-static {v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getPromptString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 345
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getTo()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 343
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 347
    sget-object v1, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[multiContactsSearched] getVideo : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getVideo()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    sget-object v1, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[multiContactsSearched] addVideoNotSupportMessage : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 349
    iget-boolean v3, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 348
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    sget-object v1, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[multiContactsSearched] getTo : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getTo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v1

    if-ltz v1, :cond_0

    .line 353
    sget-object v1, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[multiContactsSearched] getType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getPhoneType()I

    move-result v1

    iput v1, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mSearchListType:I

    .line 357
    :cond_0
    iget-boolean v1, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    if-eqz v1, :cond_1

    .line 358
    iput-boolean v4, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    .line 359
    iput-boolean v5, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->isVideoCall:Z

    .line 363
    :cond_1
    const-string/jumbo v1, "DM_DIAL_CONTACT_SEARCH_LIST"

    .line 362
    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 364
    return-void
.end method

.method protected varargs noContactsSearched([Ljava/lang/String;)V
    .locals 8
    .param p1, "prompts"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 313
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 314
    .local v0, "buf":Ljava/lang/StringBuffer;
    array-length v6, p1

    move v4, v5

    :goto_0
    if-lt v4, v6, :cond_1

    .line 319
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 321
    .local v2, "prompt":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v1}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 322
    .local v1, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    iput-object v2, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPrePromptString:Ljava/lang/String;

    .line 324
    sget-object v4, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "getVideo : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getVideo()Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    sget-object v4, Lcom/nuance/sample/controllers/SampleDialPageController;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "addVideoNotSupportMessage : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-boolean v4, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    if-eqz v4, :cond_0

    .line 328
    iput-boolean v5, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    .line 329
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 330
    const v5, 0x7f0a013b

    .line 329
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 333
    :cond_0
    const-string/jumbo v4, "DM_DIAL_CONTACT"

    invoke-static {v4, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 335
    return-void

    .line 314
    .end local v1    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v2    # "prompt":Ljava/lang/String;
    :cond_1
    aget-object v3, p1, v4

    .line 315
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    if-eqz v7, :cond_2

    .line 316
    const-string/jumbo v7, ". "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 317
    :cond_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 314
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected runFinalAction()Z
    .locals 6

    .prologue
    .line 388
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getVideo()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 389
    .local v0, "askedForVideo":Z
    const/4 v1, 0x0

    .line 395
    .local v1, "placeVideoCall":Z
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContactData()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->getNormalizedAddress()Ljava/lang/String;

    move-result-object v3

    .line 396
    new-instance v4, Lcom/nuance/sample/controllers/SampleDialPageController$1;

    invoke-direct {v4, p0}, Lcom/nuance/sample/controllers/SampleDialPageController$1;-><init>(Lcom/nuance/sample/controllers/SampleDialPageController;)V

    .line 411
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    .line 395
    invoke-static {v2, v3, v4, v5, v1}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V

    .line 412
    const/4 v2, 0x1

    return v2
.end method

.method protected saveCustomParams(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 3
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 58
    const-string/jumbo v1, "Mode"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "mode":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    const-string/jumbo v1, "videophone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/nuance/sample/controllers/SampleDialPageController;->setVideo(Ljava/lang/Boolean;)V

    .line 61
    const-string/jumbo v1, "videophone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    .line 63
    :cond_0
    return-void
.end method

.method public setVideo(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "video"    # Ljava/lang/Boolean;

    .prologue
    .line 462
    iput-object p1, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->video:Ljava/lang/Boolean;

    .line 463
    return-void
.end method

.method protected startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 15
    .param p1, "fieldId"    # Lcom/nuance/sample/CCFieldIds;
    .param p2, "prompts"    # [Ljava/lang/String;
    .param p3, "tts"    # [Ljava/lang/String;

    .prologue
    .line 468
    iget-boolean v12, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    if-eqz v12, :cond_1

    .line 469
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/nuance/sample/controllers/SampleDialPageController;->addVideoNotSupportMessage:Z

    .line 470
    new-instance v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v4}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 471
    .local v4, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v12

    .line 472
    const v13, 0x7f0a013b

    .line 471
    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v4, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 474
    const-string/jumbo v12, "DM_DIAL_CONTACT"

    const/4 v13, 0x0

    invoke-virtual {p0, v12, v4, v13}, Lcom/nuance/sample/controllers/SampleDialPageController;->startActivityIfNeeded(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;[Ljava/lang/String;)V

    .line 536
    .end local v4    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_0
    :goto_0
    return-void

    .line 479
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 481
    .local v1, "buf":Ljava/lang/StringBuffer;
    move-object/from16 v0, p2

    array-length v13, v0

    const/4 v12, 0x0

    :goto_1
    if-lt v12, v13, :cond_3

    .line 485
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    .line 487
    .local v9, "prompt":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuffer;

    .end local v1    # "buf":Ljava/lang/StringBuffer;
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 489
    .restart local v1    # "buf":Ljava/lang/StringBuffer;
    move-object/from16 v0, p3

    array-length v13, v0

    const/4 v12, 0x0

    :goto_2
    if-lt v12, v13, :cond_4

    .line 493
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    .line 495
    .local v11, "ttsStr":Ljava/lang/String;
    const-string/jumbo v12, "VAC_DRIVELINK"

    invoke-static {v12}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 496
    invoke-virtual/range {p1 .. p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getFlowID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 497
    .local v3, "flowID":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    .line 499
    .local v2, "curFlowID":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/nuance/sample/CCFieldIds;->name()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "CC_DIAL_CONTACT"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const-string/jumbo v12, "DM_DIAL_CONTACT_SEARCH_LIST"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 500
    const-string/jumbo v3, "DM_DIAL_CONTACT"

    .line 503
    :cond_2
    if-eqz v3, :cond_5

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_5

    .line 504
    new-instance v5, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v5}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 505
    .local v5, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {v3, v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    goto :goto_0

    .line 481
    .end local v2    # "curFlowID":Ljava/lang/String;
    .end local v3    # "flowID":Ljava/lang/String;
    .end local v5    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v9    # "prompt":Ljava/lang/String;
    .end local v11    # "ttsStr":Ljava/lang/String;
    :cond_3
    aget-object v10, p2, v12

    .line 482
    .local v10, "str":Ljava/lang/String;
    const-string/jumbo v14, ". "

    invoke-virtual {v1, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 483
    invoke-virtual {v1, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 481
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 489
    .end local v10    # "str":Ljava/lang/String;
    .restart local v9    # "prompt":Ljava/lang/String;
    :cond_4
    aget-object v10, p3, v12

    .line 490
    .restart local v10    # "str":Ljava/lang/String;
    const-string/jumbo v14, ". "

    invoke-virtual {v1, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 491
    invoke-virtual {v1, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 489
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 507
    .end local v10    # "str":Ljava/lang/String;
    .restart local v2    # "curFlowID":Ljava/lang/String;
    .restart local v3    # "flowID":Ljava/lang/String;
    .restart local v11    # "ttsStr":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0, v9}, Lcom/nuance/sample/controllers/SampleDialPageController;->showAndSay(Ljava/lang/String;)V

    .line 508
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 509
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->startRecording()V

    goto/16 :goto_0

    .line 516
    .end local v2    # "curFlowID":Ljava/lang/String;
    .end local v3    # "flowID":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->isCalledFromMain()Z

    move-result v8

    .line 517
    .local v8, "isCalledFromMain":Z
    if-eqz v8, :cond_7

    .line 518
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getActivityClass()Ljava/lang/Class;

    move-result-object v6

    .line 520
    .local v6, "getClase":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;>;"
    if-eqz v6, :cond_0

    .line 521
    new-instance v7, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v7, v12, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 523
    .local v7, "intent":Landroid/content/Intent;
    if-eqz v7, :cond_0

    .line 524
    const/high16 v12, 0x10000000

    invoke-virtual {v7, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 525
    const-string/jumbo v12, "extra_prompt_string"

    invoke-virtual {v7, v12, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 526
    const-string/jumbo v12, "extra_tts_string"

    invoke-virtual {v7, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    const-string/jumbo v12, "extra_field_id"

    invoke-virtual/range {p1 .. p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 528
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v7}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 532
    .end local v6    # "getClase":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;>;"
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_7
    invoke-virtual {p0, v9, v11}, Lcom/nuance/sample/controllers/SampleDialPageController;->showAndSay(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 534
    invoke-virtual {p0}, Lcom/nuance/sample/controllers/SampleDialPageController;->startRecording()V

    goto/16 :goto_0
.end method
