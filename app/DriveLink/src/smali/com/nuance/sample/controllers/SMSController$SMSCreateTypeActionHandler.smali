.class final Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;
.super Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;
.source "SMSController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/controllers/SMSController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SMSCreateTypeActionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/controllers/SMSController;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/controllers/SMSController;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SampleContactSearchController$CreateTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SampleContactSearchController;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/controllers/SMSController;Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;-><init>(Lcom/nuance/sample/controllers/SMSController;)V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 10
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 52
    const-string/jumbo v4, "Message"

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/nuance/sample/controllers/SMSController;->setMessage(Ljava/lang/String;)V

    .line 53
    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    const-string/jumbo v5, "To"

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/sample/controllers/SMSController;->setTo(Ljava/lang/String;)V

    .line 54
    const-string/jumbo v4, "Pn"

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "phoneNumber":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 58
    const-string/jumbo v4, "PhoneType"

    .line 57
    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    .line 59
    .local v8, "phoneType":Ljava/lang/String;
    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    sget-object v5, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-static {v5, v8}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneType(Lcom/vlingo/core/internal/contacts/ContactType;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/nuance/sample/controllers/SMSController;->setPhoneType(I)V

    .line 61
    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SMSController;->getTo()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 62
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    .line 63
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 62
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 64
    const-string/jumbo v4, "CM02"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 65
    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    iget-object v5, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v5}, Lcom/nuance/sample/controllers/SMSController;->getContactFieldID()Lcom/nuance/sample/CCFieldIds;

    move-result-object v5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v7, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v7}, Lcom/nuance/sample/controllers/SMSController;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 66
    iget-object v9, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v9}, Lcom/nuance/sample/controllers/SMSController;->getContactRequestString()I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    .line 65
    invoke-virtual {v4, v5, v3}, Lcom/nuance/sample/controllers/SMSController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    .line 82
    .end local v8    # "phoneType":Ljava/lang/String;
    :goto_0
    return v6

    .line 68
    .restart local v8    # "phoneType":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v4}, Lcom/nuance/sample/controllers/SMSController;->startContactSearch()V

    move v6, v3

    .line 69
    goto :goto_0

    .line 72
    .end local v8    # "phoneType":Ljava/lang/String;
    :cond_1
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 73
    const-wide/16 v3, 0x0

    move-object v5, v2

    .line 72
    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 74
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 75
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v3, v0

    move-object v5, v1

    move v7, v6

    .line 74
    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 76
    .local v2, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 77
    iget-object v3, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v3, v1}, Lcom/nuance/sample/controllers/SMSController;->setTo(Ljava/lang/String;)V

    .line 78
    invoke-static {v0}, Lcom/nuance/sample/controllers/SMSController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 79
    invoke-static {v2}, Lcom/nuance/sample/controllers/SMSController;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 80
    iget-object v3, p0, Lcom/nuance/sample/controllers/SMSController$SMSCreateTypeActionHandler;->this$0:Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {v3, v0}, Lcom/nuance/sample/controllers/SMSController;->followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0
.end method
