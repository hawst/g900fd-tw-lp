.class public Lcom/nuance/sample/settings/SampleAppSettings;
.super Lcom/vlingo/core/internal/settings/Settings;
.source "SampleAppSettings.java"


# static fields
.field public static final KEY_SAMSUNG_MULTI_ENGINE_ENABLE:Ljava/lang/String; = "samsung_multi_engine_enable"

.field public static final KEY_SAMSUNG_WAKEUP_ENGINE_ENABLE:Ljava/lang/String; = "samsung_wakeup_engine_enable"

.field public static final KEY_STREAM_VOLUME:Ljava/lang/String; = "stream_volume"

.field public static final KEY_SYSTEM_VOLUME:Ljava/lang/String; = "system_volume"

.field private static final TAG:Ljava/lang/String;

.field private static log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/nuance/sample/settings/SampleAppSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/settings/SampleAppSettings;->TAG:Ljava/lang/String;

    .line 31
    const-class v0, Lcom/nuance/sample/settings/SampleAppSettings;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/Settings;-><init>()V

    return-void
.end method

.method public static getSVoiceLocale()Ljava/util/Locale;
    .locals 5

    .prologue
    .line 156
    const-string/jumbo v2, "language"

    .line 157
    const-string/jumbo v3, "en-US"

    .line 156
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "language":Ljava/lang/String;
    invoke-static {v0}, Lcom/nuance/sample/settings/SampleAppSettings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 160
    .local v1, "voiceLocale":Ljava/util/Locale;
    const-string/jumbo v2, "Language"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getVoiceLocale - key_language :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 161
    const-string/jumbo v4, ",  voiceLocale :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 160
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    return-object v1
.end method

.method public static initCustomWaekupSetting()V
    .locals 4

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 200
    .local v0, "isCusotmWakeup":Z
    sget-object v1, Lcom/nuance/sample/settings/SampleAppSettings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "initCustomWaekupSetting"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const-string/jumbo v1, "samsung_wakeup_engine_enable"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 202
    return-void
.end method

.method public static initSeamlessSetting()V
    .locals 4

    .prologue
    .line 191
    const/4 v0, 0x0

    .line 193
    .local v0, "isSeamless":Z
    sget-object v1, Lcom/nuance/sample/settings/SampleAppSettings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "initSeamlessSetting"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string/jumbo v1, "seamless_wakeup"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 195
    return-void
.end method

.method public static initVolumeSetting()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 128
    sget-object v0, Lcom/nuance/sample/settings/SampleAppSettings;->TAG:Ljava/lang/String;

    .line 129
    const-string/jumbo v1, "initVolumeSetting - value setting KEY_SYSTEM_VOLUME / KEY_STREAM_VOLUME : -1"

    .line 128
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const-string/jumbo v0, "system_volume"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 132
    const-string/jumbo v0, "stream_volume"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 133
    return-void
.end method

.method public static languageUpdateConfiguration()V
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->getSVoiceLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/nuance/sample/settings/SampleAppSettings;->updateVoiceLocale(Ljava/util/Locale;)Ljava/util/Locale;

    .line 153
    return-void
.end method

.method public static resetVolumeNormal(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-static {p0}, Lcom/nuance/sample/settings/SampleAppSettings;->setStreamVolumeNormal(Landroid/content/Context;)V

    .line 124
    invoke-static {p0}, Lcom/nuance/sample/settings/SampleAppSettings;->setSystemVolumeNormal(Landroid/content/Context;)V

    .line 125
    return-void
.end method

.method public static setStreamVolumeMute(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 72
    if-eqz p0, :cond_0

    .line 73
    const-string/jumbo v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 74
    .local v0, "audioManager":Landroid/media/AudioManager;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 75
    const/4 v1, -0x1

    .line 76
    .local v1, "streamVolume":I
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 82
    :goto_0
    sget-object v2, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "----- MUSIC IS ACTIVE (ConnectedCar) SET VOLUME MUTE -------"

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 83
    const-string/jumbo v2, "stream_volume"

    invoke-static {v2, v1}, Lcom/nuance/sample/settings/SampleAppSettings;->setInt(Ljava/lang/String;I)V

    .line 85
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 86
    invoke-virtual {v0, v6, v4, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 95
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v1    # "streamVolume":I
    :cond_0
    :goto_1
    return-void

    .line 79
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    .restart local v1    # "streamVolume":I
    :cond_1
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {v0, v5, v4, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_1

    .line 92
    .end local v1    # "streamVolume":I
    :cond_3
    sget-object v2, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "----- MUSIC IS NOT ACTIVE (ConnectedCar) = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "-------"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static setStreamVolumeNormal(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 98
    if-eqz p0, :cond_0

    .line 99
    const-string/jumbo v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 100
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v2, "stream_volume"

    invoke-static {v2, v3}, Lcom/nuance/sample/settings/SampleAppSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 101
    .local v1, "streamVolume":I
    if-eq v1, v3, :cond_0

    .line 102
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 107
    :goto_0
    const-string/jumbo v2, "stream_volume"

    invoke-static {v2, v3}, Lcom/nuance/sample/settings/SampleAppSettings;->setInt(Ljava/lang/String;I)V

    .line 110
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v1    # "streamVolume":I
    :cond_0
    return-void

    .line 105
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    .restart local v1    # "streamVolume":I
    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public static setSystemVolumeMute(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 60
    sget-object v2, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "--------Entered setSystemVolumeToMinimum---------"

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 61
    const-string/jumbo v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 62
    .local v0, "audioManager":Landroid/media/AudioManager;
    if-eqz p0, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 64
    .local v1, "systemVolume":I
    const-string/jumbo v2, "system_volume"

    invoke-static {v2, v1}, Lcom/nuance/sample/settings/SampleAppSettings;->setInt(Ljava/lang/String;I)V

    .line 65
    invoke-virtual {v0, v5, v4, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 68
    .end local v1    # "systemVolume":I
    :cond_0
    sget-object v2, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "------Exited setSystemVolumeToMinimum-------"

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public static setSystemVolumeNormal(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, -0x1

    .line 42
    sget-object v2, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "--------Entered setSystemVolumeToNormal---------"

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 43
    if-eqz p0, :cond_0

    .line 44
    const-string/jumbo v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 45
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v2, "system_volume"

    invoke-static {v2, v4}, Lcom/nuance/sample/settings/SampleAppSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 46
    .local v1, "systemVolume":I
    if-eq v1, v4, :cond_0

    .line 47
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 48
    const-string/jumbo v2, "system_volume"

    invoke-static {v2, v4}, Lcom/nuance/sample/settings/SampleAppSettings;->setInt(Ljava/lang/String;I)V

    .line 52
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v1    # "systemVolume":I
    :cond_0
    sget-object v2, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "------Exited setSystemVolumeToNormal-------"

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public static setVolumeMute(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    invoke-static {p0}, Lcom/nuance/sample/settings/SampleAppSettings;->setStreamVolumeMute(Landroid/content/Context;)V

    .line 114
    invoke-static {p0}, Lcom/nuance/sample/settings/SampleAppSettings;->setSystemVolumeMute(Landroid/content/Context;)V

    .line 115
    return-void
.end method

.method public static setVolumeNormal(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    invoke-static {p0}, Lcom/nuance/sample/settings/SampleAppSettings;->setStreamVolumeNormal(Landroid/content/Context;)V

    .line 119
    invoke-static {p0}, Lcom/nuance/sample/settings/SampleAppSettings;->setSystemVolumeNormal(Landroid/content/Context;)V

    .line 120
    return-void
.end method

.method public static updateVoiceLocale(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 4
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 167
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 168
    .local v0, "config":Landroid/content/res/Configuration;
    iput-object p0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 170
    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 172
    sget-object v1, Lcom/nuance/sample/settings/SampleAppSettings;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[VACSample] languageUpdateConfiguration : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 173
    const-string/jumbo v3, "config.locale"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 174
    const-string/jumbo v1, "Language"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setVoiceLocale - locale :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 175
    const-string/jumbo v3, ",  config.locale :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 178
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->fontScale:F

    .line 177
    iput v1, v0, Landroid/content/res/Configuration;->fontScale:F

    .line 180
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v1

    .line 181
    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 184
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 185
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 182
    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 187
    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    return-object v1
.end method
