.class public Lcom/nuance/sample/settings/SettingsLanguageScreen;
.super Landroid/app/Activity;
.source "SettingsLanguageScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;
    }
.end annotation


# static fields
.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"

.field public static isLaunchBySettings:Z

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private arrayAdapter:Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;

.field private mListView:Landroid/widget/ListView;

.field private m_languageApplication:Landroid/preference/ListPreference;

.field private settableLanguageDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private settableLanguages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/nuance/sample/settings/SettingsLanguageScreen;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->isLaunchBySettings:Z

    .line 50
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->arrayAdapter:Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;

    .line 45
    iput-object v0, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    .line 47
    iput-object v0, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguages:Ljava/util/ArrayList;

    .line 48
    iput-object v0, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/settings/SettingsLanguageScreen;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguageDescriptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/nuance/sample/settings/SettingsLanguageScreen;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguages:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "vlingoLanguage"    # Ljava/lang/String;

    .prologue
    .line 245
    const-string/jumbo v0, "v-es-LA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    const-string/jumbo p0, "es-US"

    .line 251
    .end local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 248
    .restart local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, "v-es-NA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    const-string/jumbo p0, "es-US"

    goto :goto_0
.end method


# virtual methods
.method initPreference()V
    .locals 8

    .prologue
    .line 131
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v4

    .line 133
    .local v4, "supportedLanguages":[Ljava/lang/CharSequence;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguageDescriptions()[Ljava/lang/CharSequence;

    move-result-object v3

    .line 134
    .local v3, "supportedLanguageDescriptions":[Ljava/lang/CharSequence;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguages:Ljava/util/ArrayList;

    .line 135
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 137
    const/4 v2, 0x1

    .line 138
    .local v2, "showAllLanguages":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v4

    if-lt v0, v5, :cond_0

    .line 151
    return-void

    .line 139
    :cond_0
    aget-object v5, v4, v0

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "language":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 141
    iget-object v5, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguages:Ljava/util/ArrayList;

    aget-object v6, v4, v0

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v5, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 143
    aget-object v6, v3, v0

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_1
    sget-object v5, Lcom/nuance/sample/settings/SettingsLanguageScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "not adding \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\', desc \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 148
    aget-object v7, v3, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 146
    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x1

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 59
    .local v3, "intent":Landroid/content/Intent;
    const v11, 0x7f03002d

    invoke-virtual {p0, v11}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->setContentView(I)V

    .line 62
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->initPreference()V

    .line 64
    new-instance v11, Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;

    invoke-direct {v11, p0, p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;-><init>(Lcom/nuance/sample/settings/SettingsLanguageScreen;Landroid/app/Activity;)V

    iput-object v11, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->arrayAdapter:Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;

    .line 65
    const v11, 0x7f09012c

    invoke-virtual {p0, v11}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ListView;

    iput-object v11, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    .line 66
    iget-object v11, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    iget-object v12, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->arrayAdapter:Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;

    invoke-virtual {v11, v12}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    iget-object v11, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 69
    iget-object v11, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    invoke-virtual {v11, v13}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 71
    const-string/jumbo v11, "language"

    const-string/jumbo v12, "en-US"

    invoke-static {v11, v12}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 72
    .local v4, "language":Ljava/lang/String;
    iget-object v11, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    iget-object v12, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguages:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v12

    invoke-virtual {v11, v12, v13}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 74
    iget-object v11, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    new-instance v12, Lcom/nuance/sample/settings/SettingsLanguageScreen$1;

    invoke-direct {v12, p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen$1;-><init>(Lcom/nuance/sample/settings/SettingsLanguageScreen;)V

    invoke-virtual {v11, v12}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 84
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    .line 85
    .local v8, "titlebar":Landroid/app/ActionBar;
    if-eqz v8, :cond_0

    .line 86
    const-string/jumbo v11, "Change language"

    invoke-virtual {v8, v11}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 91
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 92
    .local v5, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string/jumbo v12, "privateFlags"

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 93
    .local v6, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string/jumbo v12, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    .line 94
    .local v7, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string/jumbo v12, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 96
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v10, 0x0

    .local v10, "valueofFlagsEnableStatusBar":I
    const/4 v9, 0x0

    .line 97
    .local v9, "valueofFlagsDisableTray":I
    invoke-virtual {v6, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 98
    invoke-virtual {v7, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v10

    .line 99
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    .line 101
    or-int/2addr v0, v10

    .line 102
    or-int/2addr v0, v9

    .line 104
    invoke-virtual {v6, v5, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 105
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 114
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v5    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v6    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v7    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v9    # "valueofFlagsDisableTray":I
    .end local v10    # "valueofFlagsEnableStatusBar":I
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v2

    .line 108
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    sget-object v11, Lcom/nuance/sample/settings/SettingsLanguageScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "NoSuchFieldException "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ", continuing"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 109
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/Exception;
    sget-object v11, Lcom/nuance/sample/settings/SettingsLanguageScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Exception "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ", continuing"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onLanguageChanged()V
    .locals 5

    .prologue
    .line 178
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 184
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/nuance/sample/settings/SettingsLanguageScreen$2;

    invoke-direct {v4, p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen$2;-><init>(Lcom/nuance/sample/settings/SettingsLanguageScreen;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 189
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 191
    :cond_0
    const-string/jumbo v3, "language"

    .line 192
    const-string/jumbo v4, "en-US"

    .line 191
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 197
    .local v2, "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 198
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    .line 199
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 207
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onLanguageChanged()V

    .line 209
    invoke-static {v2}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 210
    .local v1, "langToChange":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 211
    const-string/jumbo v4, "voicetalk_language"

    .line 210
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 212
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->sendBroadcast(Landroid/content/Intent;)V

    .line 214
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishDialog()V

    .line 215
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->finish()V

    .line 216
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 118
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 122
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 120
    :pswitch_0
    invoke-virtual {p0}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->finish()V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 155
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 157
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "language":Ljava/lang/String;
    iget-object v1, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguages:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 159
    return-void
.end method
