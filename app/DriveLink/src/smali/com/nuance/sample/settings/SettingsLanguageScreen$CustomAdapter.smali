.class Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SettingsLanguageScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/settings/SettingsLanguageScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomAdapter"
.end annotation


# instance fields
.field context:Landroid/app/Activity;

.field final synthetic this$0:Lcom/nuance/sample/settings/SettingsLanguageScreen;


# direct methods
.method constructor <init>(Lcom/nuance/sample/settings/SettingsLanguageScreen;Landroid/app/Activity;)V
    .locals 2
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;->this$0:Lcom/nuance/sample/settings/SettingsLanguageScreen;

    .line 223
    const v0, 0x7f030088

    # getter for: Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguageDescriptions:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->access$0(Lcom/nuance/sample/settings/SettingsLanguageScreen;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 224
    iput-object p2, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;->context:Landroid/app/Activity;

    .line 225
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 229
    iget-object v5, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 230
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030088

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 231
    .local v4, "row":Landroid/view/View;
    const v5, 0x7f09027c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 232
    .local v2, "label":Landroid/widget/TextView;
    const v5, 0x7f09027d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 233
    .local v0, "icon":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;->this$0:Lcom/nuance/sample/settings/SettingsLanguageScreen;

    # getter for: Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguageDescriptions:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->access$0(Lcom/nuance/sample/settings/SettingsLanguageScreen;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    const-string/jumbo v5, "language"

    const-string/jumbo v6, "en-US"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 235
    .local v3, "language":Ljava/lang/String;
    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/nuance/sample/settings/SettingsLanguageScreen$CustomAdapter;->this$0:Lcom/nuance/sample/settings/SettingsLanguageScreen;

    # getter for: Lcom/nuance/sample/settings/SettingsLanguageScreen;->settableLanguages:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/nuance/sample/settings/SettingsLanguageScreen;->access$1(Lcom/nuance/sample/settings/SettingsLanguageScreen;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 236
    const v5, 0x7f02041e

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 240
    :goto_0
    return-object v4

    .line 238
    :cond_0
    const v5, 0x7f02041d

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
