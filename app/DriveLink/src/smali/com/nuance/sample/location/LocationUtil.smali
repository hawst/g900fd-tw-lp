.class public Lcom/nuance/sample/location/LocationUtil;
.super Ljava/lang/Object;
.source "LocationUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static getAllItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/location/LocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/location/LocationItem;>;"
    invoke-static {}, Lcom/nuance/sample/location/LocationUtil;->getContactItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 14
    invoke-static {}, Lcom/nuance/sample/location/LocationUtil;->getEventItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 15
    invoke-static {}, Lcom/nuance/sample/location/LocationUtil;->getFavoriteItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 16
    return-object v0
.end method

.method public static getContactItems()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/nuance/sample/location/LocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/location/LocationContactItem;>;"
    new-instance v1, Lcom/nuance/sample/location/LocationContactItem;

    const-string/jumbo v2, "Bill Jones"

    .line 23
    const-string/jumbo v3, "401 Main Street Worcester MA"

    invoke-direct {v1, v2, v3}, Lcom/nuance/sample/location/LocationContactItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    return-object v0
.end method

.method public static getEventItems()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/nuance/sample/location/LocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/location/LocationEventItem;>;"
    new-instance v1, Lcom/nuance/sample/location/LocationEventItem;

    const-string/jumbo v2, "morning meeting"

    const-string/jumbo v3, "Sally Blvd"

    invoke-direct {v1, v2, v3}, Lcom/nuance/sample/location/LocationEventItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    return-object v0
.end method

.method public static getFavoriteItems()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/nuance/sample/location/LocationItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 37
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/location/LocationFavoriteItem;>;"
    new-instance v1, Lcom/nuance/sample/location/LocationFavoriteItem;

    const-string/jumbo v2, "Home"

    const-string/jumbo v3, "19 Shepard Cambridge MA"

    invoke-direct {v1, v2, v3}, Lcom/nuance/sample/location/LocationFavoriteItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    return-object v0
.end method
