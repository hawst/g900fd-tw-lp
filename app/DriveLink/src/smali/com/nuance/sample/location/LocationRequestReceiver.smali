.class public Lcom/nuance/sample/location/LocationRequestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationRequestReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 19
    const-string/jumbo v1, "type"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 20
    .local v0, "type":Ljava/lang/String;
    const-string/jumbo v1, "contact"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 24
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 25
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 27
    const v2, 0x7f0a05f5

    new-array v3, v6, [Ljava/lang/Object;

    .line 28
    const-string/jumbo v4, "contact"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 26
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 29
    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestShareConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 30
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 21
    invoke-virtual {p0, p2, v1, v2, v3}, Lcom/nuance/sample/location/LocationRequestReceiver;->startFlow(Landroid/content/Intent;Ljava/lang/String;Lcom/nuance/sample/controllers/SampleLocationController$Mode;Lcom/nuance/sample/CCFieldIds;)V

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    const-string/jumbo v1, "group"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 35
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 36
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 38
    const v2, 0x7f0a05fe

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 39
    const-string/jumbo v4, "contact"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 40
    const-string/jumbo v4, "location"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 37
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 41
    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupJoinConfirmation:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 42
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 32
    invoke-virtual {p0, p2, v1, v2, v3}, Lcom/nuance/sample/location/LocationRequestReceiver;->startFlow(Landroid/content/Intent;Ljava/lang/String;Lcom/nuance/sample/controllers/SampleLocationController$Mode;Lcom/nuance/sample/CCFieldIds;)V

    goto :goto_0

    .line 43
    :cond_2
    const-string/jumbo v1, "destination"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 47
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 48
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 50
    const v2, 0x7f0a05ff

    new-array v3, v6, [Ljava/lang/Object;

    .line 51
    const-string/jumbo v4, "location"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 49
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 52
    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->DestinationChanged:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 53
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 44
    invoke-virtual {p0, p2, v1, v2, v3}, Lcom/nuance/sample/location/LocationRequestReceiver;->startFlow(Landroid/content/Intent;Ljava/lang/String;Lcom/nuance/sample/controllers/SampleLocationController$Mode;Lcom/nuance/sample/CCFieldIds;)V

    goto :goto_0

    .line 54
    :cond_3
    const-string/jumbo v1, "request_approved"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 58
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 59
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 60
    const v2, 0x7f0a0607

    new-array v3, v6, [Ljava/lang/Object;

    .line 61
    const-string/jumbo v4, "contact"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 60
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 62
    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->RequestApproved:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 63
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 55
    invoke-virtual {p0, p2, v1, v2, v3}, Lcom/nuance/sample/location/LocationRequestReceiver;->startFlow(Landroid/content/Intent;Ljava/lang/String;Lcom/nuance/sample/controllers/SampleLocationController$Mode;Lcom/nuance/sample/CCFieldIds;)V

    goto/16 :goto_0

    .line 64
    :cond_4
    const-string/jumbo v1, "expire"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 69
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 70
    const v2, 0x7f0a0604

    new-array v3, v6, [Ljava/lang/Object;

    .line 71
    const-string/jumbo v4, "contact"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 70
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 72
    sget-object v2, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->GroupLocationExpiration:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    .line 73
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 65
    invoke-virtual {p0, p2, v1, v2, v3}, Lcom/nuance/sample/location/LocationRequestReceiver;->startFlow(Landroid/content/Intent;Ljava/lang/String;Lcom/nuance/sample/controllers/SampleLocationController$Mode;Lcom/nuance/sample/CCFieldIds;)V

    goto/16 :goto_0
.end method

.method protected startFlow(Landroid/content/Intent;Ljava/lang/String;Lcom/nuance/sample/controllers/SampleLocationController$Mode;Lcom/nuance/sample/CCFieldIds;)V
    .locals 3
    .param p1, "arg1"    # Landroid/content/Intent;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "mode"    # Lcom/nuance/sample/controllers/SampleLocationController$Mode;
    .param p4, "fieldId"    # Lcom/nuance/sample/CCFieldIds;

    .prologue
    .line 79
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 81
    .local v0, "dialogFlow":Lcom/vlingo/core/facade/dialogflow/IDialogFlow;
    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 82
    invoke-interface {v0, p2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 83
    invoke-static {p3}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    .line 85
    invoke-virtual {p4}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 87
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 88
    const-string/jumbo v2, "[stop] : LocationRequestReceiver - startFlow()"

    .line 87
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 90
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 91
    return-void
.end method
