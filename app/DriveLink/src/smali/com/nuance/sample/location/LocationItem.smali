.class public Lcom/nuance/sample/location/LocationItem;
.super Ljava/lang/Object;
.source "LocationItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/nuance/sample/location/LocationItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final address:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object p1, p0, Lcom/nuance/sample/location/LocationItem;->title:Ljava/lang/String;

    .line 8
    iput-object p2, p0, Lcom/nuance/sample/location/LocationItem;->address:Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/nuance/sample/location/LocationItem;)I
    .locals 2
    .param p1, "arg0"    # Lcom/nuance/sample/location/LocationItem;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/nuance/sample/location/LocationItem;->address:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/nuance/sample/location/LocationItem;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/nuance/sample/location/LocationItem;

    invoke-virtual {p0, p1}, Lcom/nuance/sample/location/LocationItem;->compareTo(Lcom/nuance/sample/location/LocationItem;)I

    move-result v0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/nuance/sample/location/LocationItem;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/nuance/sample/location/LocationItem;->title:Ljava/lang/String;

    return-object v0
.end method
