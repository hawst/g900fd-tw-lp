.class public Lcom/nuance/sample/safereader/SampleMessagesController;
.super Lcom/nuance/sample/controllers/SMSController;
.source "SampleMessagesController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/safereader/SampleMessagesController$State;
    }
.end annotation


# instance fields
.field private COMMA:Ljava/lang/String;

.field private PERIOD:Ljava/lang/String;

.field private SPACE:Ljava/lang/String;

.field private capped:Z

.field currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

.field private log:Lcom/vlingo/core/facade/logging/ILogger;

.field protected messageAlertQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field

.field private messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

.field protected state:Lcom/nuance/sample/safereader/SampleMessagesController$State;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SMSController;-><init>()V

    .line 47
    const-class v0, Lcom/nuance/sample/safereader/SampleMessagesController;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    .line 379
    sget-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONTACT:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->state:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    .line 58
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->PERIOD:Ljava/lang/String;

    .line 59
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->COMMA:Ljava/lang/String;

    .line 62
    const-string/jumbo v0, " "

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->SPACE:Ljava/lang/String;

    .line 63
    return-void
.end method

.method private displayMessage()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 269
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    if-eqz v7, :cond_1

    .line 270
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 271
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 272
    .local v1, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-direct {p0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "body":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 274
    .local v3, "name":Ljava/lang/String;
    const/4 v4, 0x0

    .line 275
    .local v4, "nameSpoken":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 276
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 278
    :cond_0
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 280
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v7, ""

    invoke-direct {v2, v0, v3, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    .local v2, "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    new-instance v6, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 282
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v7

    .line 281
    invoke-direct {v6, v3, v7}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    .local v6, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 288
    new-instance v7, Ljava/lang/StringBuilder;

    .line 289
    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v9, v11, [Ljava/lang/Object;

    .line 290
    aput-object v4, v9, v10

    invoke-static {v8, v9}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 291
    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 292
    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 293
    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->COMMA:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 294
    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 296
    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v9, v11, [Ljava/lang/Object;

    .line 297
    aput-object v0, v9, v10

    .line 295
    invoke-static {v8, v9}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 288
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 298
    .local v5, "promptSpoken":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v7

    .line 299
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 300
    iget-object v9, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v9

    iget-object v11, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v11}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v11

    .line 298
    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    .line 302
    invoke-virtual {p0, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->showAndSay(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v7

    .line 306
    sget-object v8, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    .line 307
    invoke-virtual {v8}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 306
    invoke-static {v8}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v8

    .line 304
    invoke-interface {v7, v8}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 308
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->startRecording()V

    .line 310
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 313
    .end local v0    # "body":Ljava/lang/String;
    .end local v1    # "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v2    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "nameSpoken":Ljava/lang/String;
    .end local v5    # "promptSpoken":Ljava/lang/String;
    .end local v6    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_1
    return-void
.end method

.method private displayMultipleMessages(Ljava/util/Queue;Z)V
    .locals 6
    .param p2, "requestCollapse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "queue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v3, 0x1

    .line 498
    if-eqz p2, :cond_1

    .line 500
    const-string/jumbo v2, "multi.widget.client.collapse"

    .line 499
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 501
    if-eqz v2, :cond_1

    move v1, v3

    .line 502
    .local v1, "collapse":Z
    :goto_0
    new-instance v2, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-direct {v2, p1, v1}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;-><init>(Ljava/util/Queue;Z)V

    iput-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    .line 503
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    if-eqz v2, :cond_0

    .line 504
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 506
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    .line 507
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v0

    .line 508
    .local v0, "a":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    .line 509
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v2

    .line 508
    iput-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 510
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    if-eqz v2, :cond_0

    .line 511
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 512
    invoke-direct {p0, p1}, Lcom/nuance/sample/safereader/SampleMessagesController;->systemTurnForAllFromSender(Ljava/util/Queue;)V

    .line 516
    :goto_1
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->startRecording()V

    .line 517
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageShowWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 518
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v5

    .line 517
    invoke-interface {v2, v3, v4, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 521
    .end local v0    # "a":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    :cond_0
    return-void

    .line 498
    .end local v1    # "collapse":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 514
    .restart local v0    # "a":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    .restart local v1    # "collapse":Z
    :cond_2
    invoke-direct {p0, p1}, Lcom/nuance/sample/safereader/SampleMessagesController;->systemTurnForAllMesages(Ljava/util/Queue;)V

    goto :goto_1
.end method

.method private displayMultipleMessagesFromSender(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
    .locals 3
    .param p1, "aSMSMMSMessage"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 263
    .line 264
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    .line 263
    invoke-direct {p0, v1, v2}, Lcom/nuance/sample/safereader/SampleMessagesController;->getNewQueueForSender(Ljava/lang/String;Ljava/util/Queue;)Ljava/util/Queue;

    move-result-object v0

    .line 265
    .local v0, "newAlertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->displayMultipleMessages(Ljava/util/Queue;Z)V

    .line 266
    return-void
.end method

.method private getBodyShown(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "contactName"    # Ljava/lang/String;
    .param p2, "fromReadout"    # Z
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 445
    if-eqz p2, :cond_0

    .line 446
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v3, [Ljava/lang/Object;

    .line 447
    aput-object p3, v1, v2

    .line 445
    invoke-static {v0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 449
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 450
    aput-object p1, v1, v2

    aput-object p3, v1, v3

    .line 448
    invoke-static {v0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;
    .locals 1
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 561
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    .line 562
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 561
    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getDisplayableMessageText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMessagesForWidget(Ljava/util/Queue;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)",
            "Ljava/util/List",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 567
    .local p1, "alerts":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 568
    .local v4, "msgList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    if-eqz p1, :cond_1

    .line 569
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Queue;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 591
    :cond_1
    return-object v4

    .line 570
    :cond_2
    invoke-interface {p1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 571
    .local v1, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->isSMS()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->isMMS()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 572
    :cond_3
    move-object v3, v1

    .line 574
    .local v3, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-direct {p0, v3}, Lcom/nuance/sample/safereader/SampleMessagesController;->getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v2

    .line 575
    .local v2, "displayBody":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v5

    .line 578
    .local v5, "name":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 579
    const-string/jumbo v6, ""

    .line 578
    invoke-direct {v0, v2, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    .local v0, "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setId(J)V

    .line 581
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "SMS"

    if-ne v6, v7, :cond_5

    .line 582
    const-string/jumbo v6, "SMS"

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    .line 587
    :cond_4
    :goto_1
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 583
    :cond_5
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "MMS"

    if-ne v6, v7, :cond_4

    .line 584
    const-string/jumbo v6, "MMS"

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getNewQueueForSender(Ljava/lang/String;Ljava/util/Queue;)Ljava/util/Queue;
    .locals 4
    .param p1, "senderName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    .local p2, "oldQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-interface {p2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 184
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 186
    .local v2, "newQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 191
    return-object v2

    .line 187
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 188
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 189
    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getSenderText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;
    .locals 2
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 549
    if-nez p1, :cond_1

    .line 550
    const-string/jumbo v0, ""

    .line 557
    :cond_0
    :goto_0
    return-object v0

    .line 552
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 554
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 555
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getSenderTtsText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;
    .locals 3
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 536
    if-nez p1, :cond_0

    .line 537
    const-string/jumbo v1, ""

    .line 545
    :goto_0
    return-object v1

    .line 539
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 540
    .local v0, "name":Ljava/lang/String;
    const/4 v1, 0x0

    .line 541
    .local v1, "nameSpoken":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 542
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 544
    :cond_1
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 545
    goto :goto_0
.end method

.method private handleShowUnreadMessagesWidget()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 595
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 597
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "alertQueue(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 598
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 599
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-le v2, v5, :cond_1

    .line 600
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-direct {p0, v2, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->displayMultipleMessages(Ljava/util/Queue;Z)V

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 603
    :cond_1
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    iput-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 604
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->displayMessage()V

    goto :goto_0

    .line 608
    :cond_2
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 609
    .local v0, "errorMsg":Ljava/lang/String;
    invoke-virtual {p0, v0, v0}, Lcom/nuance/sample/safereader/SampleMessagesController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const-string/jumbo v1, "No or empty alert list."

    .line 612
    .local v1, "msg":Ljava/lang/String;
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 613
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/DLApplication;->getCurrentActivity()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v2

    invoke-static {v2, v1, v5}, Lcom/sec/android/automotive/drivelink/util/DLToast;->makeText(Landroid/app/Activity;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    .line 614
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private markAsRead(Ljava/util/Queue;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 524
    .local p1, "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-interface {p1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 526
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v3

    .line 527
    .local v3, "provider":Lcom/vlingo/core/internal/messages/SMSMMSProvider;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    .line 528
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 529
    .local v1, "context":Landroid/content/Context;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 533
    return-void

    .line 530
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 531
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v1, v4, v5, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0
.end method

.method private next()V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method private promptForMessage()V
    .locals 3

    .prologue
    .line 341
    sget-object v1, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_MESSAGE:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->state:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    .line 343
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 344
    const v2, 0x7f0a0025

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method private systemTurnForAllFromSender(Ljava/util/Queue;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "queue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 455
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-gtz v2, :cond_0

    .line 477
    :goto_0
    return-void

    .line 458
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 459
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 461
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 462
    invoke-interface {p1}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    .line 463
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v2

    .line 462
    invoke-direct {p0, v2}, Lcom/nuance/sample/safereader/SampleMessagesController;->getSenderText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v7

    .line 460
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 458
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "displayString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 465
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 467
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 468
    invoke-interface {p1}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    .line 469
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v2

    .line 468
    invoke-direct {p0, v2}, Lcom/nuance/sample/safereader/SampleMessagesController;->getSenderTtsText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v7

    .line 466
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 464
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 470
    .local v1, "msgAnnounceString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 471
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    .line 474
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_MSGCHOOSE:Lcom/nuance/sample/CCFieldIds;

    .line 475
    invoke-virtual {v3}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 474
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    .line 472
    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 476
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private systemTurnForAllMesages(Ljava/util/Queue;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 480
    .local p1, "queue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-gtz v2, :cond_0

    .line 494
    :goto_0
    return-void

    .line 484
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 485
    invoke-interface {p1}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 483
    invoke-static {v2, v3}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 486
    .local v0, "displayString":Ljava/lang/String;
    move-object v1, v0

    .line 487
    .local v1, "msgAnnounceString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 488
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    .line 491
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_MSGCHOOSE:Lcom/nuance/sample/CCFieldIds;

    .line 492
    invoke-virtual {v3}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 491
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    .line 489
    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 493
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public callSender()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 195
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    if-eqz v4, :cond_1

    .line 196
    new-instance v4, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;

    .line 197
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-direct {v4, v5, v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    .line 196
    invoke-virtual {p0, v4}, Lcom/nuance/sample/safereader/SampleMessagesController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 199
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 198
    invoke-static {v4}, Lcom/vlingo/core/internal/util/PhoneUtil;->turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V

    .line 200
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v2

    .line 203
    .local v2, "name":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 205
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "no name for sender, using address"

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 206
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "address":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 209
    aput-object v2, v5, v7

    .line 207
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "displayText":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 212
    invoke-static {v0}, Lcom/vlingo/core/internal/util/DialUtil;->getTTSForAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 210
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 222
    .end local v0    # "address":Ljava/lang/String;
    .local v3, "ttsText":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    .line 225
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    .line 224
    invoke-static {v4, v5, p0, v6}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 230
    .end local v1    # "displayText":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "ttsText":Ljava/lang/String;
    :goto_1
    return-void

    .line 215
    .restart local v2    # "name":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 216
    aput-object v2, v5, v7

    .line 214
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 218
    .restart local v1    # "displayText":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 219
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 217
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "ttsText":Ljava/lang/String;
    goto :goto_0

    .line 228
    .end local v1    # "displayText":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "ttsText":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "This should not be possible, callingSender from a null alert."

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected displayMessageBody(Z)V
    .locals 9
    .param p1, "fromReadout"    # Z

    .prologue
    .line 386
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    if-nez v6, :cond_0

    .line 441
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 391
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Unexpected alert type of "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 392
    const-string/jumbo v8, ".  Ignoring alert."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 391
    invoke-interface {v6, v7}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0

    .line 395
    :cond_1
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 401
    .local v2, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "body":Ljava/lang/String;
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 403
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 404
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 405
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 412
    :cond_2
    :goto_1
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 413
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 415
    :cond_3
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    .line 419
    .local v4, "name":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 420
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 424
    :cond_4
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v6, ""

    invoke-direct {v3, v0, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    .local v3, "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    new-instance v5, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 426
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    .line 425
    invoke-direct {v5, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    .local v5, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 429
    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, p1, v0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getBodyShown(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 432
    .local v1, "bodyShown":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->showAndSay(Ljava/lang/String;)V

    .line 433
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    .line 436
    sget-object v7, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    .line 437
    invoke-virtual {v7}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 436
    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v7

    .line 434
    invoke-interface {v6, v7}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 438
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->startRecording()V

    .line 439
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_0

    .line 407
    .end local v1    # "bodyShown":Ljava/lang/String;
    .end local v3    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_5
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 3
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SMSController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 82
    if-eqz p1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "executeAction: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 85
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ShowUnreadMessagesWidget"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->handleShowUnreadMessagesWidget()V

    .line 114
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 90
    :cond_1
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ShowWCISWidget"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->handleShowUnreadMessagesWidget()V

    goto :goto_0

    .line 92
    :cond_2
    const-string/jumbo v0, "repeat"

    .line 93
    const-string/jumbo v1, "Command"

    invoke-interface {p1, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 93
    if-eqz v0, :cond_3

    .line 94
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->handleShowUnreadMessagesWidget()V

    goto :goto_0

    .line 96
    :cond_3
    const-string/jumbo v0, "no"

    .line 97
    const-string/jumbo v1, "Command"

    invoke-interface {p1, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 97
    if-eqz v0, :cond_4

    .line 98
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    .line 99
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 100
    const v2, 0x7f0a004d

    .line 99
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 101
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    goto :goto_0

    .line 103
    :cond_4
    if-eqz p1, :cond_5

    .line 104
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "LPAction"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 105
    invoke-virtual {p0, p1}, Lcom/nuance/sample/safereader/SampleMessagesController;->handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z

    goto :goto_0

    .line 107
    :cond_5
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "SafereaderReply"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0, p1}, Lcom/nuance/sample/safereader/SampleMessagesController;->handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z

    goto :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 622
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 120
    if-eqz p1, :cond_1

    .line 121
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 122
    const-string/jumbo v8, "com.vlingo.core.internal.dialogmanager.Choice"

    .line 121
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    .line 122
    if-eqz v7, :cond_5

    .line 124
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v8, "handleIntent() intent action = com.vlingo.core.internal.dialogmanager.Choice"

    invoke-interface {v7, v8}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 127
    const-string/jumbo v7, "id"

    .line 128
    const-wide/16 v8, -0x1

    .line 127
    invoke-virtual {p1, v7, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 130
    .local v4, "id":J
    const-string/jumbo v7, "item_count"

    const/4 v8, -0x1

    .line 129
    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 133
    .local v1, "count":I
    const-string/jumbo v7, "message_type"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 135
    .local v6, "type":Ljava/lang/String;
    const-string/jumbo v7, "from_read_messages"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 137
    .local v3, "fromReadMessagesString":Ljava/lang/String;
    const/4 v2, 0x0

    .line 138
    .local v2, "fromReadMessages":Z
    if-eqz v3, :cond_0

    .line 140
    const-string/jumbo v7, "true"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 143
    :cond_0
    const-string/jumbo v7, "SMS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 144
    const-string/jumbo v7, "MMS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 146
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Unknown message type of "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    .line 175
    .end local v1    # "count":I
    .end local v2    # "fromReadMessages":Z
    .end local v3    # "fromReadMessagesString":Ljava/lang/String;
    .end local v4    # "id":J
    .end local v6    # "type":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 149
    .restart local v1    # "count":I
    .restart local v2    # "fromReadMessages":Z
    .restart local v3    # "fromReadMessagesString":Ljava/lang/String;
    .restart local v4    # "id":J
    .restart local v6    # "type":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v7

    .line 150
    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v7, v4, v5, v6, v8}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->findbyId(JLjava/lang/String;Ljava/util/LinkedList;)Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v0

    .line 151
    .local v0, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-nez v0, :cond_3

    .line 153
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "handleIntent() could not locate message with ID = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 154
    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 153
    invoke-interface {v7, v8}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_3
    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 158
    const/4 v7, 0x1

    if-le v1, v7, :cond_4

    if-eqz v2, :cond_4

    .line 160
    invoke-direct {p0, v0}, Lcom/nuance/sample/safereader/SampleMessagesController;->displayMultipleMessagesFromSender(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V

    goto :goto_0

    .line 163
    :cond_4
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->displayMessage()V

    goto :goto_0

    .line 165
    .end local v0    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "count":I
    .end local v2    # "fromReadMessages":Z
    .end local v3    # "fromReadMessagesString":Ljava/lang/String;
    .end local v4    # "id":J
    .end local v6    # "type":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 166
    const-string/jumbo v8, "com.vlingo.core.internal.dialogmanager.Call"

    .line 165
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    .line 166
    if-eqz v7, :cond_6

    .line 167
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 168
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->callSender()V

    goto :goto_0

    .line 169
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 170
    const-string/jumbo v8, "com.vlingo.core.internal.dialogmanager.Reply"

    .line 169
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    .line 170
    if-eqz v7, :cond_1

    .line 171
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 172
    const-string/jumbo v7, ""

    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleMessagesController;->reply(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 6
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 317
    const-string/jumbo v5, "Action"

    invoke-static {p1, v5, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "actionValue":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 320
    const-string/jumbo v5, "safereader:read"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 321
    const/4 v2, 0x1

    .line 322
    .local v2, "readout":Z
    invoke-virtual {p0, v2}, Lcom/nuance/sample/safereader/SampleMessagesController;->displayMessageBody(Z)V

    .line 337
    .end local v2    # "readout":Z
    :goto_0
    return v3

    .line 324
    :cond_0
    const-string/jumbo v5, "safereader:call"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 325
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->callSender()V

    goto :goto_0

    .line 327
    :cond_1
    const-string/jumbo v5, "safereader:reply"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 329
    const-string/jumbo v5, "Text"

    .line 328
    invoke-static {p1, v5, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 330
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/nuance/sample/safereader/SampleMessagesController;->reply(Ljava/lang/String;)V

    goto :goto_0

    .line 332
    .end local v1    # "message":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "safereader:next"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 333
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->next()V

    goto :goto_0

    :cond_3
    move v3, v4

    .line 337
    goto :goto_0
.end method

.method protected handleNeedMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "body"    # Ljava/lang/String;
    .param p2, "contact"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    .line 350
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 351
    invoke-static {v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v0

    .line 352
    if-eqz v0, :cond_1

    .line 353
    :cond_0
    invoke-virtual {p0, p2, p3, p1}, Lcom/nuance/sample/safereader/SampleMessagesController;->promptForConfirmation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_1
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->promptForMessage()V

    goto :goto_0
.end method

.method protected promptForConfirmation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "contact"    # Ljava/lang/String;
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 361
    sget-object v4, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    iput-object v4, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->state:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    .line 363
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0a0024

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 364
    .local v1, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-direct {v2, p3, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    .local v2, "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-direct {v3, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    .local v3, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeSendButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->cancelButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 372
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5, v0, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 373
    return-void
.end method

.method public reply(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    if-eqz v1, :cond_1

    .line 239
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 241
    .local v0, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v2

    .line 243
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 241
    invoke-virtual {p0, v1, v2, v3}, Lcom/nuance/sample/safereader/SampleMessagesController;->handleNeedMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 245
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    .line 246
    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 245
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    .line 244
    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 260
    .end local v0    # "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unexpected alert type of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->currentAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 250
    const-string/jumbo v3, ".  Ignoring alert."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 249
    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMessagesController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    .line 256
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/nuance/sample/safereader/SampleMessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 255
    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 258
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v2, "This should not be possible, replying to a null alert."

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 628
    const/4 v0, 0x0

    return v0
.end method

.method public setCapped(Z)V
    .locals 0
    .param p1, "capped"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->capped:Z

    .line 72
    return-void
.end method

.method public setMessageAlertQueue(Ljava/util/LinkedList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    iput-object p1, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/sample/safereader/SampleMessagesController;->capped:Z

    .line 68
    return-void
.end method
