.class Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;
.super Ljava/lang/Object;
.source "SampleAlertReadoutController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/safereader/SampleAlertReadoutController;->send()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;


# direct methods
.method constructor <init>(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionAbort()V
    .locals 0

    .prologue
    .line 348
    return-void
.end method

.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 332
    const-string/jumbo v1, "cancelled"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 333
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    # invokes: Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->access$0(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Landroid/content/Context;

    move-result-object v1

    .line 334
    const v2, 0x7f0a0022

    .line 333
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 341
    .local v0, "msg":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    # invokes: Lcom/nuance/sample/safereader/SampleAlertReadoutController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->access$1(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 342
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    # invokes: Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->access$2(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 343
    return-void

    .line 338
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    # invokes: Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->access$0(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Landroid/content/Context;

    move-result-object v1

    .line 339
    const v2, 0x7f0a004d

    .line 338
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "msg":Ljava/lang/String;
    goto :goto_0
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 320
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    # invokes: Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->access$0(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Landroid/content/Context;

    move-result-object v1

    .line 321
    const v2, 0x7f0a004c

    .line 320
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    # invokes: Lcom/nuance/sample/safereader/SampleAlertReadoutController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->access$1(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 325
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;->this$0:Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    # invokes: Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->access$2(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 326
    return-void
.end method
