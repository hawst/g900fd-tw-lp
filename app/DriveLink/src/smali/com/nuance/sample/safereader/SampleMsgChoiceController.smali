.class public Lcom/nuance/sample/safereader/SampleMsgChoiceController;
.super Lcom/nuance/sample/safereader/SampleAlertReadoutController;
.source "SampleMsgChoiceController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;-><init>()V

    .line 29
    const-class v0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 26
    return-void
.end method


# virtual methods
.method call()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 116
    move-object v1, p0

    .line 117
    .local v1, "dma":Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v2, :cond_0

    .line 118
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    .line 120
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    .line 119
    invoke-static {v2, v3, v1, v4, v5}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V

    .line 129
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 122
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 123
    .local v0, "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    .line 124
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    .line 123
    invoke-static {v2, v3, v1, v4, v5}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V

    goto :goto_0

    .line 127
    .end local v0    # "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_1
    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "This should not be possible, replying to a null alert."

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected cleanup()V
    .locals 0

    .prologue
    .line 242
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->cleanup()V

    .line 243
    return-void
.end method

.method protected decline()Z
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->decline()Z

    move-result v0

    return v0
.end method

.method protected displaySingleMessage(Z)V
    .locals 0
    .param p1, "initial"    # Z

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    .line 106
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v4, 0x0

    .line 47
    if-eqz p1, :cond_0

    .line 48
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "ProcessCommand"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    const-string/jumbo v2, "Command"

    .line 49
    invoke-static {p1, v2, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "param":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v2, "cancel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    .line 53
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "tts":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->showAndSay(Ljava/lang/String;)V

    .line 55
    const/4 v2, 0x1

    .line 59
    .end local v0    # "param":Ljava/lang/String;
    .end local v1    # "tts":Ljava/lang/String;
    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v2

    goto :goto_0
.end method

.method protected getActionConfirmationKeys()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getActionConfirmationKeys()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getActionDeclineKeys()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getActionDeclineKeys()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method protected getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;
    .locals 1

    .prologue
    .line 194
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getRules()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 5
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    const-string/jumbo v4, "Action"

    invoke-static {p1, v4, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "actionValue":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v2

    .line 79
    :cond_1
    const-string/jumbo v4, "safereader:call"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    .line 81
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->call()V

    move v2, v3

    .line 82
    goto :goto_0

    .line 83
    :cond_2
    const-string/jumbo v4, "repeat"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 84
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 85
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    .line 86
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 87
    .local v1, "msgAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/nuance/sample/safereader/SampleMsgChoiceController;->showAndSay(Ljava/lang/String;)V

    .end local v1    # "msgAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_3
    move v2, v3

    .line 90
    goto :goto_0
.end method

.method protected isCalledFromMain()Z
    .locals 1

    .prologue
    .line 200
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isCalledFromMain()Z

    move-result v0

    return v0
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isSilentMode()Z

    move-result v0

    return v0
.end method

.method protected isWithoutAskingMsg()Z
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v0

    return v0
.end method

.method protected processCommand(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 249
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->processCommand(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method reply(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->reply(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public resolveConfirmation(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->resolveConfirmation(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v0

    return v0
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->runFinalAction()Z

    move-result v0

    return v0
.end method

.method protected runFinalActionAndCloseFlow()Z
    .locals 1

    .prologue
    .line 230
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->runFinalActionAndCloseFlow()Z

    move-result v0

    return v0
.end method

.method public setCurrentContactSearch(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentContactSearch"    # Ljava/lang/String;

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setCurrentContactSearch(Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public setSenderQueue(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setSenderQueue(Ljava/util/HashMap;)V

    .line 35
    return-void
.end method

.method public setShowMessageBody(Z)V
    .locals 0
    .param p1, "showMessageBody"    # Z

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setShowMessageBody(Z)V

    .line 41
    return-void
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setSilentMode(Z)V

    .line 141
    return-void
.end method

.method protected show(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->show(Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method protected showAndSay(I)V
    .locals 0
    .param p1, "prompt"    # I

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showAndSay(I)V

    .line 165
    return-void
.end method

.method protected showAndSay(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-super {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showAndSay(Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method protected varargs startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldId"    # Lcom/nuance/sample/CCFieldIds;
    .param p2, "prompts"    # [Ljava/lang/String;

    .prologue
    .line 206
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startActivityIfNeeded(Lcom/nuance/sample/CCFieldIds;[Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method protected startRecording()V
    .locals 0

    .prologue
    .line 188
    invoke-super {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startRecording()V

    .line 189
    return-void
.end method
