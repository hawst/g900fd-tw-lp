.class final enum Lcom/nuance/sample/safereader/SampleMessagesController$State;
.super Ljava/lang/Enum;
.source "SampleMessagesController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/safereader/SampleMessagesController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/safereader/SampleMessagesController$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/safereader/SampleMessagesController$State;

.field public static final enum NEED_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

.field public static final enum NEED_CONTACT:Lcom/nuance/sample/safereader/SampleMessagesController$State;

.field public static final enum NEED_CONTACT_REFINEMENT:Lcom/nuance/sample/safereader/SampleMessagesController$State;

.field public static final enum NEED_MESSAGE:Lcom/nuance/sample/safereader/SampleMessagesController$State;

.field public static final enum NEED_REDIAL_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

.field public static final enum NEED_TYPE:Lcom/nuance/sample/safereader/SampleMessagesController$State;

.field public static final enum POST_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376
    new-instance v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    const-string/jumbo v1, "NEED_CONTACT"

    invoke-direct {v0, v1, v3}, Lcom/nuance/sample/safereader/SampleMessagesController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONTACT:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    new-instance v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    const-string/jumbo v1, "NEED_CONTACT_REFINEMENT"

    invoke-direct {v0, v1, v4}, Lcom/nuance/sample/safereader/SampleMessagesController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONTACT_REFINEMENT:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    new-instance v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    const-string/jumbo v1, "NEED_TYPE"

    invoke-direct {v0, v1, v5}, Lcom/nuance/sample/safereader/SampleMessagesController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_TYPE:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    new-instance v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    const-string/jumbo v1, "NEED_MESSAGE"

    invoke-direct {v0, v1, v6}, Lcom/nuance/sample/safereader/SampleMessagesController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_MESSAGE:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    new-instance v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    const-string/jumbo v1, "NEED_CONFIRMATION"

    invoke-direct {v0, v1, v7}, Lcom/nuance/sample/safereader/SampleMessagesController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    new-instance v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    const-string/jumbo v1, "POST_CONFIRMATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/safereader/SampleMessagesController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->POST_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    new-instance v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    const-string/jumbo v1, "NEED_REDIAL_CONFIRMATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/safereader/SampleMessagesController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_REDIAL_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    .line 375
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/nuance/sample/safereader/SampleMessagesController$State;

    sget-object v1, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONTACT:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONTACT_REFINEMENT:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_TYPE:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_MESSAGE:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/nuance/sample/safereader/SampleMessagesController$State;->POST_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/nuance/sample/safereader/SampleMessagesController$State;->NEED_REDIAL_CONFIRMATION:Lcom/nuance/sample/safereader/SampleMessagesController$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->ENUM$VALUES:[Lcom/nuance/sample/safereader/SampleMessagesController$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 375
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/safereader/SampleMessagesController$State;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/safereader/SampleMessagesController$State;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/safereader/SampleMessagesController$State;->ENUM$VALUES:[Lcom/nuance/sample/safereader/SampleMessagesController$State;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/safereader/SampleMessagesController$State;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
