.class public Lcom/nuance/sample/safereader/VACBackgroundHandler;
.super Ljava/lang/Object;
.source "VACBackgroundHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;


# static fields
.field private static final FOCUS_DEFAULT_BACKGROUND:I = 0x0

.field private static final FOCUS_FOREGROUND:I = 0x1

.field private static isInProcess:Z

.field private static log:Lcom/vlingo/core/facade/logging/ILogger;

.field private static mFocus:I

.field private static mInstance:Lcom/nuance/sample/safereader/VACBackgroundHandler;


# instance fields
.field private alerts:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private isSilentMode:Z

.field private listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    const-class v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;

    .line 39
    const-string/jumbo v1, "VLG_SafeReaderBackgroundHandler: "

    .line 37
    invoke-static {v0, v1}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 43
    sput-boolean v2, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    .line 45
    sput v2, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mFocus:I

    .line 46
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isSilentMode:Z

    .line 62
    iput-object p0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 63
    iget-object v0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 64
    return-void
.end method

.method private generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;
    .locals 9
    .param p2, "totalMessages"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "senders":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const-string/jumbo v3, ""

    .line 206
    .local v3, "names":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v1

    .line 208
    .local v1, "max":I
    const/4 v0, 0x0

    .line 209
    .local v0, "index":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 232
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    .line 234
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 235
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    .line 233
    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 237
    .local v2, "multiSenderSpokenText":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 238
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 237
    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 239
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 240
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 239
    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 242
    return-object v2

    .line 211
    .end local v2    # "multiSenderSpokenText":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    .line 210
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 211
    if-nez v4, :cond_2

    .line 212
    if-eqz v0, :cond_1

    .line 213
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 214
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 213
    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 215
    add-int/lit8 v4, v1, -0x1

    if-ne v0, v4, :cond_1

    .line 216
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 217
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 216
    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 219
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 220
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 219
    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 223
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 224
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 223
    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 225
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 226
    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    .line 225
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 228
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method public static getInstance()Lcom/nuance/sample/safereader/VACBackgroundHandler;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mInstance:Lcom/nuance/sample/safereader/VACBackgroundHandler;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;

    invoke-direct {v0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;-><init>()V

    sput-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mInstance:Lcom/nuance/sample/safereader/VACBackgroundHandler;

    .line 56
    const/4 v0, 0x0

    sput v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mFocus:I

    .line 58
    :cond_0
    sget-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mInstance:Lcom/nuance/sample/safereader/VACBackgroundHandler;

    return-object v0
.end method

.method private isDrivingModeOffered()Z
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x1

    return v0
.end method

.method private isKeygaurdSecureActive()Z
    .locals 3

    .prologue
    .line 347
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 346
    check-cast v0, Landroid/app/KeyguardManager;

    .line 348
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 348
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V
    .locals 9
    .param p1, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 261
    sget-object v4, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "readAlert()"

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 262
    if-eqz p1, :cond_3

    .line 263
    sput-boolean v8, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    .line 265
    sget-object v4, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Type of alert is:  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 266
    const-string/jumbo v6, "isSilentMode() "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isSilentMode()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 265
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "MMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 269
    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "SMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v0, p1

    .line 270
    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 271
    .local v0, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v1

    .line 272
    .local v1, "messageSender":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getMessageText()Ljava/lang/String;

    move-result-object v2

    .line 275
    .local v2, "msgTxt":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 277
    invoke-direct {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isKeygaurdSecureActive()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isDrivingModeOffered()Z

    move-result v4

    if-nez v4, :cond_1

    .line 279
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    .line 281
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    .line 282
    aput-object v1, v6, v7

    aput-object v2, v6, v8

    .line 280
    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 300
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "messageSender":Ljava/lang/String;
    .end local v2    # "msgTxt":Ljava/lang/String;
    :goto_0
    return-void

    .line 285
    .restart local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v1    # "messageSender":Ljava/lang/String;
    .restart local v2    # "msgTxt":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    .line 287
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v8, [Ljava/lang/Object;

    .line 288
    aput-object v1, v6, v7

    .line 286
    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 294
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "messageSender":Ljava/lang/String;
    .end local v2    # "msgTxt":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Unknown alert type "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 295
    const-string/jumbo v6, "ignoring the alert notification."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 294
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_3
    sput-boolean v7, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    goto :goto_0
.end method

.method private readAlerts()V
    .locals 3

    .prologue
    .line 247
    sget-object v1, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v2, "readAlerts()"

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 248
    iget-object v1, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    .line 249
    const/4 v1, 0x1

    sput-boolean v1, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    .line 250
    iget-object v1, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    .line 251
    iget-object v2, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    .line 250
    invoke-direct {p0, v1, v2}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "prompt":Ljava/lang/String;
    iget-object v1, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 253
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSRequest;->getMessageReadback(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 257
    .end local v0    # "prompt":Ljava/lang/String;
    :goto_0
    return-void

    .line 255
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    goto :goto_0
.end method


# virtual methods
.method public getAlerts()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    return-object v0
.end method

.method public declared-synchronized getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 74
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    sput v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mFocus:I

    .line 75
    invoke-static {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 76
    invoke-static {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 77
    iput-object p1, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleAlert(Ljava/util/LinkedList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const/4 v1, 0x0

    .line 115
    .local v1, "focusIsForeground":Z
    monitor-enter p0

    .line 116
    :try_start_0
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isFocusForeground()Z

    move-result v1

    .line 115
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isSilentMode()Z

    move-result v3

    if-nez v3, :cond_7

    .line 120
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 122
    sget-object v3, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v4, "handleAlert(), app not in FG so announce TTS only"

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 123
    if-eqz p1, :cond_6

    .line 125
    sget-object v3, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v4, "Alerts is not null so update alert list."

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 127
    iget-object v3, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    if-nez v3, :cond_0

    .line 129
    sget-object v3, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v4, "Initialize the alerts list."

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 131
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    .line 134
    :cond_0
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 142
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 143
    sget-boolean v3, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    if-nez v3, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 145
    sget-object v3, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v4, "Phone is not in use and we aren\'t currently processing thus we can process the alert queue."

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 146
    invoke-direct {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isKeygaurdSecureActive()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-direct {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isDrivingModeOffered()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 147
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v3

    if-nez v3, :cond_4

    .line 148
    new-instance v2, Landroid/content/Intent;

    .line 149
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 150
    const-class v4, Lcom/nuance/sample/MainActivity;

    .line 148
    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .local v2, "launchIntent":Landroid/content/Intent;
    const-string/jumbo v3, "ACTION_SAFEREADER_LAUNCH"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 154
    const-string/jumbo v3, "EXTRA_MESSAGE_LIST"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 156
    const-string/jumbo v3, "EXTRA_TIME_SENT"

    .line 157
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 156
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 159
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 158
    invoke-static {v3, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 160
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    .line 185
    .end local v2    # "launchIntent":Landroid/content/Intent;
    :cond_2
    :goto_1
    return-void

    .line 115
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 134
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 135
    .local v0, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    iget-object v4, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 137
    sget-object v4, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Adding alert with id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 138
    iget-object v4, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 162
    .end local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    :cond_4
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_5

    .line 163
    invoke-direct {p0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->readAlerts()V

    goto :goto_1

    .line 165
    :cond_5
    iget-object v3, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-direct {p0, v3}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    goto :goto_1

    .line 171
    :cond_6
    sget-object v3, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v4, "No-op, null alert."

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 173
    :cond_7
    if-eqz v1, :cond_2

    .line 180
    sget-object v3, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v4, "background listener registered with foreground listener"

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    .line 182
    new-instance v3, Ljava/lang/IllegalStateException;

    .line 183
    const-string/jumbo v4, "background listener registered with foreground listener"

    .line 182
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public handleAlert(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 0
    .param p2, "arg1"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ")V"
        }
    .end annotation

    .prologue
    .line 360
    .local p1, "arg0":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    invoke-virtual {p0, p1}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->handleAlert(Ljava/util/LinkedList;)V

    .line 361
    return-void
.end method

.method public isFocus()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 107
    sget v1, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mFocus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFocusForeground()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 90
    sget v1, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mFocus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x1

    return v0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 337
    sget-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestCancelled isInProcess="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 339
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    .line 341
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 340
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v0

    .line 342
    invoke-interface {v0}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    .line 343
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 311
    sget-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestDidPlay isInProcess="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-direct {p0, v0}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    .line 321
    :goto_0
    return-void

    .line 316
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    .line 318
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 317
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v0

    .line 319
    invoke-interface {v0}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 326
    sget-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestIgnored isInProcess="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 328
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    .line 330
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 329
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v0

    .line 331
    invoke-interface {v0}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    .line 332
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 305
    sget-object v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestWillPlay isInProcess="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/nuance/sample/safereader/VACBackgroundHandler;->isInProcess:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 306
    return-void
.end method

.method public readoutDelay()J
    .locals 3

    .prologue
    .line 189
    const-string/jumbo v0, "safereader.delay"

    const-wide/16 v1, 0x7d0

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 68
    iput-object v1, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 69
    sput-object v1, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mInstance:Lcom/nuance/sample/safereader/VACBackgroundHandler;

    .line 70
    return-void
.end method

.method public declared-synchronized releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 82
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mFocus:I

    .line 83
    invoke-static {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 84
    invoke-static {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 85
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->stopSafeReading()V

    .line 86
    iput-object p0, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setAlerts(Ljava/util/LinkedList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iput-object p1, p0, Lcom/nuance/sample/safereader/VACBackgroundHandler;->alerts:Ljava/util/LinkedList;

    .line 104
    return-void
.end method

.method public setIsFocusForeground(I)V
    .locals 0
    .param p1, "isFocus"    # I

    .prologue
    .line 95
    sput p1, Lcom/nuance/sample/safereader/VACBackgroundHandler;->mFocus:I

    .line 96
    return-void
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "isSilentMode"    # Z

    .prologue
    .line 199
    return-void
.end method
