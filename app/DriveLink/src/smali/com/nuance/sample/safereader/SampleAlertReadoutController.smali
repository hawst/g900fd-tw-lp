.class public Lcom/nuance/sample/safereader/SampleAlertReadoutController;
.super Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;
.source "SampleAlertReadoutController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;
    }
.end annotation


# static fields
.field private static final PATTERN_FOR_RUSSIAN_1:Ljava/util/regex/Pattern;

.field private static final PATTERN_FOR_RUSSIAN_2:Ljava/util/regex/Pattern;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected alertQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field

.field protected currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

.field private currentContactSearch:Ljava/lang/String;

.field private currentOrdinalSearch:Ljava/lang/String;

.field private currentPage:I

.field protected currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

.field private isDialingEnabled:Z

.field private isSilentMode:Z

.field private log:Lcom/vlingo/core/facade/logging/ILogger;

.field private messagePosition:I

.field mt:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

.field protected senderList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation
.end field

.field protected senderQueue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation
.end field

.field private showMessageBody:Z

.field protected state:Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    .line 71
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 70
    sput-object v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->TAG:Ljava/lang/String;

    .line 84
    const-string/jumbo v0, "[^1]?(2|3|4)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 83
    sput-object v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->PATTERN_FOR_RUSSIAN_1:Ljava/util/regex/Pattern;

    .line 87
    const-string/jumbo v0, "^\\d*[^1]1$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 86
    sput-object v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->PATTERN_FOR_RUSSIAN_2:Ljava/util/regex/Pattern;

    .line 87
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;-><init>()V

    .line 67
    const-class v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 76
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 77
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    .line 89
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 90
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    .line 1178
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->mt:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 1205
    sget-object v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;->NEED_CONTACT:Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->state:Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;

    .line 64
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private displayInitialSingleMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 10
    .param p1, "message"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "mtForWidget"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .prologue
    const/4 v9, 0x0

    .line 721
    const/4 v1, 0x0

    .line 725
    .local v1, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v5

    .line 724
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 726
    .local v3, "name":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-eqz v5, :cond_2

    .line 727
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    .line 728
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 729
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v5

    .line 730
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v6

    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v8

    .line 729
    invoke-virtual {v5, v0, v6, v7, v8}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    .line 732
    iget-boolean v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isDialingEnabled:Z

    if-eqz v5, :cond_1

    .line 733
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 737
    .local v2, "displaySingleText":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 746
    .end local v0    # "context":Landroid/content/Context;
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 747
    const-string/jumbo v6, " Address "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " Content "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 748
    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 746
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 750
    .local v4, "spokenMessage":Ljava/lang/String;
    invoke-virtual {p0, v2, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showAndSay(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v5

    .line 754
    sget-object v6, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    .line 755
    invoke-virtual {v6}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 754
    invoke-static {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v6

    .line 752
    invoke-interface {v5, v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 756
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startRecording()V

    .line 758
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isReadMessageBodyEnabled()Z

    move-result v5

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-eqz v5, :cond_4

    .line 759
    :cond_0
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v5, v6, v1, p2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 765
    :goto_2
    return-void

    .line 735
    .end local v2    # "displaySingleText":Ljava/lang/String;
    .end local v4    # "spokenMessage":Ljava/lang/String;
    .restart local v0    # "context":Landroid/content/Context;
    :cond_1
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "displaySingleText":Ljava/lang/String;
    goto :goto_0

    .line 739
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "displaySingleText":Ljava/lang/String;
    :cond_2
    iget-boolean v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isDialingEnabled:Z

    if-eqz v5, :cond_3

    .line 740
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 741
    .restart local v2    # "displaySingleText":Ljava/lang/String;
    goto :goto_1

    .line 742
    .end local v2    # "displaySingleText":Ljava/lang/String;
    :cond_3
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "displaySingleText":Ljava/lang/String;
    goto/16 :goto_1

    .line 762
    .restart local v4    # "spokenMessage":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadbackBodyHidden:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v5, v6, v1, p2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_2
.end method

.method private displayMultipleMessages(Z)V
    .locals 9
    .param p1, "initial"    # Z

    .prologue
    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 768
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    if-nez v3, :cond_1

    .line 852
    :cond_0
    :goto_0
    return-void

    .line 771
    :cond_1
    const/4 v0, 0x0

    .line 772
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-boolean v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-eqz v3, :cond_2

    .line 773
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 777
    :cond_2
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 778
    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayNameOrAddress()Ljava/lang/String;

    move-result-object v3

    .line 777
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 779
    .local v2, "ttsName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "ru-RU"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 780
    iget-boolean v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isDialingEnabled:Z

    if-eqz v3, :cond_4

    .line 781
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_3

    .line 783
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 784
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 785
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 786
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    .line 785
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 782
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 834
    .local v1, "spokenMultiText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showAndSay(Ljava/lang/String;)V

    .line 835
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    .line 838
    sget-object v4, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_MSGCHOOSE:Lcom/nuance/sample/CCFieldIds;

    .line 839
    invoke-virtual {v4}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 838
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    .line 836
    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 840
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startRecording()V

    .line 842
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 843
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getMessagesForWidget(Ljava/util/LinkedList;)Ljava/util/List;

    move-result-object v5

    .line 842
    invoke-interface {v3, v4, v0, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 845
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    .line 846
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v3, v4, v0, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_0

    .line 789
    .end local v1    # "spokenMultiText":Ljava/lang/String;
    :cond_3
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 790
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 791
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 792
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    .line 791
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 788
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 794
    .restart local v1    # "spokenMultiText":Ljava/lang/String;
    goto :goto_1

    .line 795
    .end local v1    # "spokenMultiText":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_5

    .line 797
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 798
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 799
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 800
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    .line 799
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 796
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 801
    .restart local v1    # "spokenMultiText":Ljava/lang/String;
    goto/16 :goto_1

    .line 803
    .end local v1    # "spokenMultiText":Ljava/lang/String;
    :cond_5
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 804
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 805
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 806
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    .line 805
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 802
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 809
    .restart local v1    # "spokenMultiText":Ljava/lang/String;
    goto/16 :goto_1

    .line 810
    .end local v1    # "spokenMultiText":Ljava/lang/String;
    :cond_6
    iget-boolean v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isDialingEnabled:Z

    if-eqz v3, :cond_8

    .line 811
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_7

    .line 813
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 814
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 812
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 815
    .restart local v1    # "spokenMultiText":Ljava/lang/String;
    goto/16 :goto_1

    .line 817
    .end local v1    # "spokenMultiText":Ljava/lang/String;
    :cond_7
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 818
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 816
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 820
    .restart local v1    # "spokenMultiText":Ljava/lang/String;
    goto/16 :goto_1

    .line 821
    .end local v1    # "spokenMultiText":Ljava/lang/String;
    :cond_8
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_9

    .line 823
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 824
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 822
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 825
    .restart local v1    # "spokenMultiText":Ljava/lang/String;
    goto/16 :goto_1

    .line 827
    .end local v1    # "spokenMultiText":Ljava/lang/String;
    :cond_9
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 828
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    .line 826
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "spokenMultiText":Ljava/lang/String;
    goto/16 :goto_1

    .line 848
    :cond_a
    sget-object v3, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->TAG:Ljava/lang/String;

    .line 849
    const-string/jumbo v4, "Alert Queue is not null or empty but senderList is, this should not be possible."

    .line 848
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private displayMultipleSenders(Z)V
    .locals 9
    .param p1, "initial"    # Z

    .prologue
    const/16 v4, 0x1e

    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 855
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 930
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    const/4 v0, 0x0

    .line 859
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-boolean v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-eqz v3, :cond_2

    .line 860
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 865
    :cond_2
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getTotalMessagesFromSenderQueue(Ljava/util/HashMap;)I

    move-result v2

    .line 867
    .local v2, "totalMessages":I
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-le v3, v4, :cond_6

    .line 868
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    .line 869
    const-string/jumbo v4, "ru-RU"

    .line 868
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 869
    if-eqz v3, :cond_4

    .line 870
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_3

    .line 872
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 873
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 874
    invoke-direct {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 871
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 922
    .local v1, "spokenMultiSenderText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showAndSay(Ljava/lang/String;)V

    .line 923
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    .line 926
    sget-object v4, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_SENDERCHOOSE:Lcom/nuance/sample/CCFieldIds;

    .line 927
    invoke-virtual {v4}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 926
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    .line 924
    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 928
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startRecording()V

    .line 929
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v3, v4, v0, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0

    .line 877
    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_3
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 878
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 879
    invoke-direct {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 876
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 881
    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto :goto_1

    .line 882
    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_5

    .line 884
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 885
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 883
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 886
    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto :goto_1

    .line 888
    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_5
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 889
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 887
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 892
    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto/16 :goto_1

    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-le v3, v4, :cond_a

    .line 893
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    .line 894
    const-string/jumbo v4, "ru-RU"

    .line 893
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 894
    if-eqz v3, :cond_8

    .line 895
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_7

    .line 897
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 898
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 899
    invoke-direct {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 896
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 900
    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto/16 :goto_1

    .line 902
    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_7
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v5, [Ljava/lang/Object;

    .line 903
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 904
    invoke-direct {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 901
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 906
    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto/16 :goto_1

    .line 907
    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_8
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_9

    .line 909
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 910
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 908
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 911
    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto/16 :goto_1

    .line 913
    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_9
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v8, [Ljava/lang/Object;

    .line 914
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 912
    invoke-static {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 917
    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto/16 :goto_1

    .line 919
    .end local v1    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_a
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 918
    invoke-direct {p0, v3, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "spokenMultiSenderText":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method private displayNonInitialSingleMessage(ZLcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 8
    .param p1, "hasNext"    # Z
    .param p2, "mtForWidget"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .prologue
    const/4 v5, 0x0

    .line 683
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 684
    .local v1, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    if-eqz p1, :cond_1

    .line 685
    iget-boolean v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isDialingEnabled:Z

    if-eqz v4, :cond_0

    .line 686
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 698
    .local v2, "displaySingleText":Ljava/lang/String;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 699
    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Address "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 700
    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Content "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 701
    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 698
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 702
    .local v3, "spokenSingleText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    .line 703
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 704
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v4

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v5

    .line 705
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v7

    .line 704
    invoke-virtual {v4, v0, v5, v6, v7}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    .line 707
    invoke-virtual {p0, v2, v3}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showAndSay(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    .line 711
    sget-object v5, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    .line 712
    invoke-virtual {v5}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 711
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    .line 709
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 713
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startRecording()V

    .line 715
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5, v1, p2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 717
    return-void

    .line 688
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "displaySingleText":Ljava/lang/String;
    .end local v3    # "spokenSingleText":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 690
    .restart local v2    # "displaySingleText":Ljava/lang/String;
    goto :goto_0

    .line 691
    .end local v2    # "displaySingleText":Ljava/lang/String;
    :cond_1
    iget-boolean v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isDialingEnabled:Z

    if-eqz v4, :cond_2

    .line 692
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 693
    .restart local v2    # "displaySingleText":Ljava/lang/String;
    goto/16 :goto_0

    .line 694
    .end local v2    # "displaySingleText":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "displaySingleText":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private forward(Ljava/lang/String;)V
    .locals 3
    .param p1, "contactName"    # Ljava/lang/String;

    .prologue
    .line 1247
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1248
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 1249
    .local v0, "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->handleForwardMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    .end local v0    # "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_0
    :goto_0
    return-void

    .line 1250
    :cond_1
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1251
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 1252
    .restart local v0    # "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->handleForwardMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;
    .locals 1
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ")",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1293
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1294
    .local v0, "singleAlertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1295
    return-object v0
.end method

.method private generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;
    .locals 13
    .param p2, "totalMessages"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "senders":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1301
    const-string/jumbo v3, ""

    .line 1303
    .local v3, "names":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v1

    .line 1304
    .local v1, "max":I
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v4

    .line 1306
    .local v4, "pageSize":I
    const/4 v0, 0x0

    .line 1307
    .local v0, "index":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 1323
    const/16 v5, 0x1e

    if-le v1, v5, :cond_5

    .line 1324
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getDrivingModeWidgetMax()I

    move-result v1

    .line 1325
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v5

    .line 1326
    const-string/jumbo v6, "ru-RU"

    .line 1325
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1326
    if-eqz v5, :cond_4

    .line 1328
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 1330
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    .line 1331
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    aput-object v3, v7, v11

    .line 1332
    invoke-direct {p0, p2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    .line 1329
    invoke-interface {v5, v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1358
    .local v2, "multiSenderSpokenText":Ljava/lang/String;
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    .line 1359
    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1358
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1360
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    .line 1361
    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1360
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1362
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isWithoutAskingMsg()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1363
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1364
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    .line 1366
    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_command_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 1365
    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    .line 1363
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1369
    :cond_0
    return-object v2

    .line 1308
    .end local v2    # "multiSenderSpokenText":Ljava/lang/String;
    :cond_1
    iget v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v5, v4

    if-lt v0, v5, :cond_3

    iget v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    mul-int/2addr v5, v4

    if-ge v0, v5, :cond_3

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 1309
    if-eqz v0, :cond_2

    .line 1310
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1311
    add-int/lit8 v5, v1, -0x1

    if-ne v0, v5, :cond_2

    .line 1312
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1314
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1317
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1318
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayNameOrAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1320
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 1335
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 1337
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v12, [Ljava/lang/Object;

    .line 1338
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    aput-object v3, v7, v11

    .line 1336
    invoke-interface {v5, v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1340
    .restart local v2    # "multiSenderSpokenText":Ljava/lang/String;
    goto/16 :goto_1

    .line 1341
    .end local v2    # "multiSenderSpokenText":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v5

    .line 1342
    const-string/jumbo v6, "ru-RU"

    .line 1341
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1342
    if-eqz v5, :cond_6

    .line 1344
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 1346
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v12, [Ljava/lang/Object;

    .line 1347
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v3, v7, v10

    .line 1348
    invoke-direct {p0, p2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    .line 1345
    invoke-interface {v5, v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1349
    .restart local v2    # "multiSenderSpokenText":Ljava/lang/String;
    goto/16 :goto_1

    .line 1351
    .end local v2    # "multiSenderSpokenText":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    .line 1353
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v11, [Ljava/lang/Object;

    .line 1354
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v3, v7, v10

    .line 1352
    invoke-interface {v5, v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1350
    .restart local v2    # "multiSenderSpokenText":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method private getMessagesForWidget(Ljava/util/LinkedList;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1265
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1266
    .local v2, "msgList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    if-eqz p1, :cond_1

    .line 1267
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1276
    :cond_1
    return-object v2

    .line 1267
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1268
    .local v1, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1270
    iget-boolean v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-nez v3, :cond_3

    .line 1271
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    .line 1270
    :goto_1
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageTypeFromAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;Z)Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    move-result-object v0

    .line 1272
    .local v0, "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1271
    .end local v0    # "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private getPhraseNewMessagesRussian(I)Ljava/lang/String;
    .locals 2
    .param p1, "number"    # I

    .prologue
    const/4 v1, 0x0

    .line 1378
    sget-object v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->PATTERN_FOR_RUSSIAN_1:Ljava/util/regex/Pattern;

    invoke-direct {p0, p1, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isNewMessagesForRussian(ILjava/util/regex/Pattern;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1379
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1382
    :goto_0
    return-object v0

    .line 1380
    :cond_0
    sget-object v0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->PATTERN_FOR_RUSSIAN_2:Ljava/util/regex/Pattern;

    invoke-direct {p0, p1, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isNewMessagesForRussian(ILjava/util/regex/Pattern;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1381
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1382
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private handleContact(Ljava/lang/String;)Z
    .locals 5
    .param p1, "contactName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 604
    iput-object p1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 607
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    .line 638
    :goto_0
    return v1

    .line 611
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 612
    .local v0, "filteredSenderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, p1, v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageQueueByContactName(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    .line 613
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 615
    iput-boolean v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 617
    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    .line 618
    iput v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    .line 619
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 620
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 621
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;

    .line 622
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-le v3, v1, :cond_2

    .line 623
    invoke-direct {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleSenders(Z)V

    goto :goto_0

    .line 626
    :cond_2
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 627
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 628
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 627
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 629
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 630
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 631
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 632
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v1

    .line 631
    invoke-interface {v3, v4, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 634
    :cond_3
    invoke-virtual {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    move v1, v2

    .line 635
    goto :goto_0

    :cond_4
    move v1, v2

    .line 638
    goto/16 :goto_0
.end method

.method private handleForwardMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    .line 1258
    invoke-virtual {p0, p2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setTo(Ljava/lang/String;)V

    .line 1259
    invoke-static {p1}, Lcom/nuance/sample/controllers/SMSController;->setMessage(Ljava/lang/String;)V

    .line 1260
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startContactSearch()V

    .line 1261
    return-void
.end method

.method private handleWhich(Ljava/lang/String;)Z
    .locals 8
    .param p1, "which"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 537
    iput-object p1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    .line 539
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 542
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 544
    iget-object v6, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    .line 545
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMaxDisplayNumber()I

    move-result v7

    .line 544
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 542
    invoke-virtual {v3, v5, v6}, Ljava/util/LinkedList;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 547
    .local v1, "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 550
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-static {v3, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 556
    .local v0, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_0
    if-nez v0, :cond_1

    .line 558
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidMessages(Z)V

    move v3, v4

    .line 599
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :goto_1
    return v3

    .line 554
    .restart local v1    # "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_0
    invoke-static {v1, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .restart local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    goto :goto_0

    .line 563
    :cond_1
    iput-boolean v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 566
    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 568
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-static {p1, v3}, Lcom/vlingo/core/internal/util/OrdinalUtil;->ordinalToInt(Ljava/lang/String;I)I

    move-result v3

    .line 567
    iput v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    .line 569
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 570
    invoke-direct {p0, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v7

    .line 569
    invoke-interface {v3, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 571
    invoke-virtual {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    move v3, v4

    .line 572
    goto :goto_1

    .line 574
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_2
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 577
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {p0, v3}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 578
    .local v2, "ordinalSender":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    if-nez v2, :cond_3

    .line 580
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidMessages(Z)V

    move v3, v4

    .line 581
    goto :goto_1

    .line 585
    :cond_3
    iput-boolean v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 588
    iput-object v2, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 589
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 590
    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    .line 589
    iput-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 591
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 592
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 593
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 594
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v3, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v3}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v3

    .line 593
    invoke-interface {v6, v7, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 596
    :cond_4
    invoke-virtual {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    move v3, v4

    .line 597
    goto/16 :goto_1

    .end local v2    # "ordinalSender":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    :cond_5
    move v3, v5

    .line 599
    goto/16 :goto_1
.end method

.method private isNewMessagesForRussian(ILjava/util/regex/Pattern;)Z
    .locals 2
    .param p1, "number"    # I
    .param p2, "pattern"    # Ljava/util/regex/Pattern;

    .prologue
    .line 1373
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1374
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method

.method private multipleMessages(Z)V
    .locals 2
    .param p1, "initial"    # Z

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1285
    invoke-direct {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleMessages(Z)V

    .line 1290
    :goto_0
    return-void

    .line 1287
    :cond_0
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1288
    invoke-virtual {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    goto :goto_0
.end method

.method private next()V
    .locals 3

    .prologue
    .line 1077
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1080
    iget v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    .line 1083
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Current msg position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1084
    const-string/jumbo v2, ". Alert Queue size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1083
    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 1085
    iget v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    if-ltz v0, :cond_1

    .line 1086
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    iget v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1087
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1088
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 1089
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v0

    .line 1088
    invoke-interface {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 1091
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    .line 1096
    :goto_0
    return-void

    .line 1095
    :cond_1
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidNextMessage()V

    goto :goto_0
.end method

.method private noValidMessages(Z)V
    .locals 11
    .param p1, "initial"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 936
    if-eqz p1, :cond_2

    .line 940
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 942
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 943
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 941
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 945
    .local v1, "displayNoneFound":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 946
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 944
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 947
    .local v4, "spokenNoneFound":Ljava/lang/String;
    iput-object v10, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 961
    :goto_0
    invoke-virtual {p0, v1, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->reset()V

    .line 1032
    .end local v1    # "displayNoneFound":Ljava/lang/String;
    .end local v4    # "spokenNoneFound":Ljava/lang/String;
    :goto_1
    return-void

    .line 948
    :cond_0
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 950
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 951
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 949
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 953
    .restart local v1    # "displayNoneFound":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 954
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 952
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 955
    .restart local v4    # "spokenNoneFound":Ljava/lang/String;
    iput-object v10, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    goto :goto_0

    .line 957
    .end local v1    # "displayNoneFound":Ljava/lang/String;
    .end local v4    # "spokenNoneFound":Ljava/lang/String;
    :cond_1
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 958
    .restart local v1    # "displayNoneFound":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "spokenNoneFound":Ljava/lang/String;
    goto :goto_0

    .line 968
    .end local v1    # "displayNoneFound":Ljava/lang/String;
    .end local v4    # "spokenNoneFound":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    .line 970
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-boolean v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-eqz v5, :cond_3

    .line 972
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 975
    :cond_3
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 977
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 978
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 976
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 980
    .local v2, "noneFoundShown":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 981
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 979
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 982
    .local v3, "noneFoundSpoken":Ljava/lang/String;
    iput-object v10, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 1002
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v5

    if-nez v5, :cond_7

    .line 1003
    invoke-virtual {p0, v2, v3, v9, v10}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 1014
    :goto_3
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1015
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v5, v6, v0, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_1

    .line 983
    .end local v2    # "noneFoundShown":Ljava/lang/String;
    .end local v3    # "noneFoundSpoken":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 985
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 986
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 984
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 988
    .restart local v2    # "noneFoundShown":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 989
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    .line 987
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 990
    .restart local v3    # "noneFoundSpoken":Ljava/lang/String;
    iput-object v10, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    goto :goto_2

    .line 995
    .end local v2    # "noneFoundShown":Ljava/lang/String;
    .end local v3    # "noneFoundSpoken":Ljava/lang/String;
    :cond_6
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 996
    const-string/jumbo v7, ""

    aput-object v7, v6, v8

    .line 994
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 998
    .restart local v2    # "noneFoundShown":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    .line 999
    const-string/jumbo v7, ""

    aput-object v7, v6, v8

    .line 997
    invoke-static {v5, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "noneFoundSpoken":Ljava/lang/String;
    goto :goto_2

    .line 1005
    :cond_7
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1006
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    goto :goto_3

    .line 1008
    :cond_8
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 1009
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    goto :goto_3

    .line 1019
    :cond_9
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1020
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    .line 1021
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v5, v6, v0, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_1

    .line 1023
    :cond_a
    sget-object v5, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->TAG:Ljava/lang/String;

    .line 1024
    const-string/jumbo v6, "Alert Queue is not null or empty but senderList is, this should not be possible."

    .line 1023
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1027
    :cond_b
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    .line 1028
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 1029
    iget-object v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getMessagesForWidget(Ljava/util/LinkedList;)Ljava/util/List;

    move-result-object v7

    .line 1027
    invoke-interface {v5, v6, v0, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_1
.end method

.method private noValidNextMessage()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1041
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1042
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_verbose:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1048
    .local v0, "displayNoNext":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    if-eqz v4, :cond_1

    .line 1049
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayNameOrAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1051
    .local v3, "ttsName":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 1052
    aput-object v3, v5, v7

    .line 1050
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1069
    .end local v3    # "ttsName":Ljava/lang/String;
    .local v2, "spokenNoNext":Ljava/lang/String;
    :goto_1
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v8, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 1070
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->reset()V

    .line 1071
    return-void

    .line 1044
    .end local v0    # "displayNoNext":Ljava/lang/String;
    .end local v2    # "spokenNoNext":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_regular:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "displayNoNext":Ljava/lang/String;
    goto :goto_0

    .line 1053
    :cond_1
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_3

    .line 1055
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1056
    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    .line 1055
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    .line 1056
    if-nez v4, :cond_2

    .line 1057
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 1063
    .local v1, "name":Ljava/lang/String;
    :goto_2
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 1064
    aput-object v1, v5, v7

    .line 1062
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1065
    .restart local v2    # "spokenNoNext":Ljava/lang/String;
    goto :goto_1

    .line 1059
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "spokenNoNext":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1060
    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 1059
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "name":Ljava/lang/String;
    goto :goto_2

    .line 1067
    .end local v1    # "name":Ljava/lang/String;
    :cond_3
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    const-string/jumbo v6, ""

    aput-object v6, v5, v7

    .line 1066
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenNoNext":Ljava/lang/String;
    goto :goto_1
.end method

.method private previous()V
    .locals 3

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1102
    iget v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    .line 1105
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Current msg position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1106
    const-string/jumbo v2, ". Alert Queue size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1105
    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 1107
    iget v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    if-ltz v0, :cond_1

    .line 1108
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    iget v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 1109
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1110
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 1111
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v0

    .line 1110
    invoke-interface {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 1113
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    .line 1118
    :goto_0
    return-void

    .line 1117
    :cond_1
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidNextMessage()V

    goto :goto_0
.end method

.method private promptForMessage()V
    .locals 3

    .prologue
    .line 1165
    sget-object v1, Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;->NEED_MESSAGE:Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;

    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->state:Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;

    .line 1167
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1168
    const v2, 0x7f0a0025

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1169
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 1171
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    .line 1172
    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1171
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    .line 1170
    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 1174
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    return-void
.end method

.method private send()V
    .locals 6

    .prologue
    .line 300
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    .line 301
    .local v1, "cm":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v1, :cond_1

    .line 302
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Sending message to: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 303
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactMatch()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v5

    iget-object v5, v5, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 304
    const-string/jumbo v5, "; Message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "; Phone: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 302
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 305
    .local v2, "msg":Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactData()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v0

    .line 306
    .local v0, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v0, :cond_0

    .line 307
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactData()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v5

    iget-object v5, v5, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 309
    :cond_0
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v4, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 312
    .end local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v2    # "msg":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    .line 311
    invoke-static {v4, v5}, Lcom/vlingo/core/facade/CoreRegistrar;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v3

    check-cast v3, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    .line 313
    .local v3, "sendMessageAction":Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getContactData()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v4

    iget-object v4, v4, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->address(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    move-result-object v4

    .line 314
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->message(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    move-result-object v4

    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v4

    .line 315
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v4

    .line 316
    new-instance v5, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;

    invoke-direct {v5, p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController$1;-><init>(Lcom/nuance/sample/safereader/SampleAlertReadoutController;)V

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v4

    .line 349
    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    .line 350
    return-void
.end method


# virtual methods
.method call()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1208
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_1

    .line 1209
    new-instance v4, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;

    .line 1210
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-direct {v4, v5, v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/BaseAcceptedText;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    .line 1209
    invoke-virtual {p0, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 1212
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 1211
    invoke-static {v4}, Lcom/vlingo/core/internal/util/PhoneUtil;->turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V

    .line 1213
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v2

    .line 1216
    .local v2, "name":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1218
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "no name for sender, using address"

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 1219
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 1221
    .local v0, "address":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 1222
    aput-object v2, v5, v7

    .line 1220
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1224
    .local v1, "displayText":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 1225
    invoke-static {v0}, Lcom/vlingo/core/internal/util/DialUtil;->getTTSForAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 1223
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1235
    .end local v0    # "address":Ljava/lang/String;
    .local v3, "ttsText":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 1237
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    .line 1238
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    .line 1237
    invoke-static {v4, v5, p0, v6}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 1243
    .end local v1    # "displayText":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "ttsText":Ljava/lang/String;
    :goto_1
    return-void

    .line 1228
    .restart local v2    # "name":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 1229
    aput-object v2, v5, v7

    .line 1227
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1231
    .restart local v1    # "displayText":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    .line 1232
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 1230
    invoke-static {v4, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "ttsText":Ljava/lang/String;
    goto :goto_0

    .line 1241
    .end local v1    # "displayText":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "ttsText":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "This should not be possible, callingSender from a null alert."

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public changePage(Ljava/lang/String;I)V
    .locals 3
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "maxItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/sample/controllers/PageException;
        }
    .end annotation

    .prologue
    .line 1422
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v0

    .line 1423
    .local v0, "pageSize":I
    const-string/jumbo v1, "next"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1424
    iget v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    mul-int/2addr v1, v0

    if-lt v1, p2, :cond_0

    .line 1425
    new-instance v1, Lcom/nuance/sample/controllers/CannotMoveToNextPageException;

    invoke-direct {v1}, Lcom/nuance/sample/controllers/CannotMoveToNextPageException;-><init>()V

    throw v1

    .line 1427
    :cond_0
    iget v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    .line 1429
    :cond_1
    const-string/jumbo v1, "prev"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1430
    iget v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_2

    .line 1431
    new-instance v1, Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;

    invoke-direct {v1}, Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;-><init>()V

    throw v1

    .line 1433
    :cond_2
    iget v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    .line 1435
    :cond_3
    return-void
.end method

.method protected displaySingleMessage(Z)V
    .locals 6
    .param p1, "initial"    # Z

    .prologue
    .line 650
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_0

    .line 651
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 656
    .local v1, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    const/4 v0, 0x0

    .line 657
    .local v0, "hasNext":Z
    const/4 v3, 0x0

    .line 660
    .local v3, "unreadCount":I
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 661
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    iget v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->hasNext(Ljava/util/LinkedList;I)Z

    move-result v0

    .line 662
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 663
    iget v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    .line 662
    invoke-static {v4, v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getUnreadCount(Ljava/util/LinkedList;I)I

    move-result v3

    .line 666
    :cond_2
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 667
    iget-boolean v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-nez v4, :cond_3

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x0

    .line 665
    :goto_1
    invoke-static {v5, v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageTypeFromAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;Z)Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    move-result-object v2

    .line 668
    .local v2, "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setUnreadCount(I)V

    .line 670
    if-eqz p1, :cond_4

    .line 671
    invoke-direct {p0, v1, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayInitialSingleMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V

    goto :goto_0

    .line 667
    .end local v2    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    :cond_3
    const/4 v4, 0x1

    goto :goto_1

    .line 673
    .restart local v2    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    :cond_4
    invoke-direct {p0, v0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayNonInitialSingleMessage(ZLcom/vlingo/core/internal/dialogmanager/types/MessageType;)V

    goto :goto_0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 10
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 114
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/controllers/SampleContactSearchControllerBase;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 115
    sget-object v5, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v5

    iput-boolean v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isDialingEnabled:Z

    .line 116
    const/4 v0, 0x0

    .line 117
    .local v0, "command":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 118
    const-string/jumbo v5, "Command"

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 121
    .local v3, "fieldId":Ljava/lang/String;
    const-string/jumbo v5, "vp_car_readback_senderchoose"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string/jumbo v5, "next"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string/jumbo v5, "prev"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 122
    :cond_1
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 123
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 124
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 125
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;

    .line 126
    const-string/jumbo v2, ""

    .line 128
    .local v2, "errorPageChange":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-virtual {p0, v0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->changePage(Ljava/lang/String;I)V
    :try_end_0
    .catch Lcom/nuance/sample/controllers/CannotMoveToNextPageException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/nuance/sample/controllers/PageException; {:try_start_0 .. :try_end_0} :catch_2

    .line 134
    :goto_0
    const-string/jumbo v5, ""

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 135
    invoke-virtual {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showAndSay(Ljava/lang/String;)V

    .line 138
    :cond_2
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-le v5, v7, :cond_3

    .line 139
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleSenders(Z)V

    move v5, v6

    .line 296
    .end local v2    # "errorPageChange":Ljava/lang/String;
    :goto_1
    return v5

    .line 129
    .restart local v2    # "errorPageChange":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Lcom/nuance/sample/controllers/CannotMoveToNextPageException;
    const-string/jumbo v2, "There are no more items. "

    goto :goto_0

    .line 131
    .end local v1    # "e":Lcom/nuance/sample/controllers/CannotMoveToNextPageException;
    :catch_1
    move-exception v1

    .line 132
    .local v1, "e":Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;
    const-string/jumbo v2, "You are currently at first page. "

    goto :goto_0

    .line 145
    .end local v1    # "e":Lcom/nuance/sample/controllers/CannotMoveToPreviousPageException;
    .end local v2    # "errorPageChange":Ljava/lang/String;
    :cond_3
    if-nez p1, :cond_8

    .line 146
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v5, :cond_4

    .line 147
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    move v5, v6

    .line 148
    goto :goto_1

    .line 150
    :cond_4
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    .line 151
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->multipleMessages(Z)V

    move v5, v6

    .line 152
    goto :goto_1

    .line 154
    :cond_5
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    .line 156
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 157
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 158
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;

    .line 160
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-le v5, v7, :cond_6

    .line 161
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleSenders(Z)V

    move v5, v6

    .line 162
    goto :goto_1

    .line 164
    :cond_6
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 165
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 166
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/LinkedList;

    .line 165
    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 168
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 169
    iget-object v9, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 168
    invoke-interface {v5, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 171
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->multipleMessages(Z)V

    move v5, v6

    .line 172
    goto :goto_1

    .line 175
    :cond_7
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidMessages(Z)V

    :goto_2
    move v5, v6

    .line 296
    goto/16 :goto_1

    .line 177
    :cond_8
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v8, "ShowUnreadMessagesWidget"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 178
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v5, :cond_9

    .line 179
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    move v5, v6

    .line 180
    goto/16 :goto_1

    .line 182
    :cond_9
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    .line 183
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->multipleMessages(Z)V

    move v5, v6

    .line 184
    goto/16 :goto_1

    .line 186
    :cond_a
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_c

    .line 188
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 189
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 190
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;

    .line 192
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-le v5, v7, :cond_b

    .line 193
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleSenders(Z)V

    move v5, v6

    .line 194
    goto/16 :goto_1

    .line 196
    :cond_b
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 197
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 198
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/LinkedList;

    .line 197
    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 200
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 201
    iget-object v9, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 200
    invoke-interface {v5, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 203
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->multipleMessages(Z)V

    move v5, v6

    .line 204
    goto/16 :goto_1

    .line 207
    :cond_c
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidMessages(Z)V

    goto/16 :goto_2

    .line 212
    :cond_d
    if-eqz p1, :cond_e

    .line 213
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v8, "LPAction"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    move v5, v6

    .line 215
    goto/16 :goto_1

    .line 216
    :cond_e
    if-eqz p1, :cond_f

    .line 217
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v8, "SafereaderReply"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 218
    const-string/jumbo v5, "Text"

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 220
    .local v4, "message":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->reply(Ljava/lang/String;)V

    move v5, v6

    .line 221
    goto/16 :goto_1

    .line 222
    .end local v4    # "message":Ljava/lang/String;
    :cond_f
    const-string/jumbo v5, "repeat"

    .line 223
    const-string/jumbo v8, "Command"

    invoke-interface {p1, v8}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 222
    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    .line 223
    if-eqz v5, :cond_13

    .line 224
    iput-boolean v7, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 226
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v5, :cond_11

    .line 227
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    :cond_10
    :goto_3
    move v5, v7

    .line 239
    goto/16 :goto_1

    .line 229
    :cond_11
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_12

    .line 232
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 233
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    goto :goto_3

    .line 236
    :cond_12
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v5, :cond_10

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_10

    .line 237
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleSenders(Z)V

    goto :goto_3

    .line 240
    :cond_13
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getActionDeclineKeys()Ljava/util/List;

    move-result-object v5

    .line 241
    const-string/jumbo v8, "Command"

    invoke-interface {p1, v8}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 240
    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 241
    if-eqz v5, :cond_14

    .line 242
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v5

    .line 243
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 244
    const v7, 0x7f0a004d

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 242
    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->decline()Z

    move-result v5

    goto/16 :goto_1

    .line 246
    :cond_14
    const-string/jumbo v5, "next"

    .line 247
    const-string/jumbo v8, "Command"

    invoke-interface {p1, v8}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 250
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->next()V

    move v5, v7

    .line 251
    goto/16 :goto_1

    .line 252
    :cond_15
    const-string/jumbo v5, "prev"

    .line 253
    const-string/jumbo v8, "Command"

    invoke-interface {p1, v8}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 256
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->previous()V

    move v5, v7

    .line 257
    goto/16 :goto_1

    .line 258
    :cond_16
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v8, "SafereaderReply"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 259
    invoke-virtual {p0, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z

    goto/16 :goto_2

    .line 260
    :cond_17
    const-string/jumbo v5, "send"

    .line 261
    const-string/jumbo v8, "Command"

    invoke-interface {p1, v8}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 262
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->send()V

    goto/16 :goto_2

    .line 264
    :cond_18
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v5, :cond_19

    .line 265
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    move v5, v6

    .line 266
    goto/16 :goto_1

    .line 268
    :cond_19
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v5, :cond_1a

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1a

    .line 269
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->multipleMessages(Z)V

    move v5, v6

    .line 270
    goto/16 :goto_1

    .line 272
    :cond_1a
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v5, :cond_1c

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1c

    .line 274
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 275
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    iget-object v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 276
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;

    .line 278
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-le v5, v7, :cond_1b

    .line 279
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleSenders(Z)V

    move v5, v6

    .line 280
    goto/16 :goto_1

    .line 282
    :cond_1b
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 283
    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 284
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/LinkedList;

    .line 283
    iput-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 286
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 287
    iget-object v9, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 286
    invoke-interface {v5, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 289
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->multipleMessages(Z)V

    move v5, v6

    .line 290
    goto/16 :goto_1

    .line 293
    :cond_1c
    invoke-direct {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidMessages(Z)V

    goto/16 :goto_2

    .line 133
    .restart local v2    # "errorPageChange":Ljava/lang/String;
    :catch_2
    move-exception v5

    goto/16 :goto_0
.end method

.method protected followAfterContactResolution(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 4
    .param p1, "contactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v3, 0x0

    .line 1449
    invoke-static {p1}, Lcom/nuance/sample/controllers/SMSController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 1450
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-static {v1}, Lcom/nuance/sample/controllers/SMSController;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 1451
    invoke-static {}, Lcom/nuance/sample/controllers/SMSController;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 1453
    .local v0, "body":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1454
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v1, v1, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {p0, v2, v1, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->promptForConfirmation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1458
    :goto_0
    return-void

    .line 1456
    :cond_1
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->promptForMessage()V

    goto :goto_0
.end method

.method protected getActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/automotive/drivelink/common/base/BaseVoiceActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1412
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getContactFieldID()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 1467
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getContactNotFoundString()I
    .locals 1

    .prologue
    .line 1487
    const v0, 0x7f0a0039

    return v0
.end method

.method protected getContactRequestString()I
    .locals 1

    .prologue
    .line 1477
    const v0, 0x7f0a0026

    return v0
.end method

.method protected getContactTypeFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 1472
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_TYPE:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method public getListCurrentPage(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1438
    .local p1, "sourceList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1439
    .local v3, "resultMatches":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v2

    .line 1440
    .local v2, "pageSize":I
    iget v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    .line 1441
    .local v1, "page":I
    add-int/lit8 v4, v1, -0x1

    mul-int v0, v4, v2

    .local v0, "i":I
    :goto_0
    mul-int v4, v1, v2

    if-ge v0, v4, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 1444
    :cond_0
    return-object v3

    .line 1442
    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1441
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getMultiplePhoneTypeString()I
    .locals 1

    .prologue
    .line 1482
    const v0, 0x7f0a05f3

    return v0
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1406
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1401
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSearchContactType()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 1462
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 355
    if-nez p1, :cond_1

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 360
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Readout"

    .line 359
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 360
    if-eqz v4, :cond_2

    .line 361
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 364
    iput-boolean v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 365
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    goto :goto_0

    .line 366
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 367
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Reply"

    .line 366
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 367
    if-eqz v4, :cond_3

    .line 368
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    goto :goto_0

    .line 370
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 371
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Next"

    .line 370
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 371
    if-eqz v4, :cond_4

    .line 372
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 373
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->next()V

    goto :goto_0

    .line 374
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 375
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Forward"

    .line 374
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 375
    if-eqz v4, :cond_5

    .line 376
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 377
    const-string/jumbo v4, ""

    invoke-direct {p0, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->forward(Ljava/lang/String;)V

    goto :goto_0

    .line 378
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 379
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Call"

    .line 378
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 379
    if-eqz v4, :cond_6

    .line 380
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 381
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->call()V

    goto :goto_0

    .line 382
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 383
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Choice"

    .line 382
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 383
    if-nez v4, :cond_7

    .line 384
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 385
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.DirectReply"

    .line 384
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 385
    if-eqz v4, :cond_0

    .line 387
    :cond_7
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "handleIntent() intent action = com.vlingo.core.internal.dialogmanager.Choice"

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 389
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 390
    const-string/jumbo v4, "id"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 393
    .local v1, "position":I
    const-string/jumbo v4, "message_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 395
    .local v3, "type":Ljava/lang/String;
    if-eq v1, v6, :cond_8

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 397
    :cond_8
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "handleIntent() invalid position "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 398
    const-string/jumbo v6, " or type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 397
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 403
    :cond_9
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    .line 404
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 405
    .local v0, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-nez v0, :cond_a

    .line 407
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "handleIntent() could not locate message at position = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 408
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 407
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 414
    :cond_a
    iput-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 415
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 416
    invoke-direct {p0, v0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v6

    .line 415
    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 417
    iput-boolean v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 418
    iput v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->messagePosition:I

    .line 420
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 421
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.DirectReply"

    .line 420
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 421
    if-nez v4, :cond_0

    .line 424
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    goto/16 :goto_0

    .line 427
    .end local v0    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_b
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_10

    .line 429
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 430
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    if-nez v4, :cond_c

    .line 432
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "handleIntent() could not locate sender at position = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 433
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 432
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 436
    :cond_c
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 437
    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v2

    .line 436
    check-cast v2, Ljava/util/LinkedList;

    .line 438
    .local v2, "selectedMessageQueue":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    if-eqz v2, :cond_d

    .line 439
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 441
    :cond_d
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "handleIntent() sender had no messages."

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 444
    :cond_e
    iput-object v2, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 445
    iput-boolean v8, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 446
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 447
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 448
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 449
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v4, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v4

    .line 448
    invoke-interface {v5, v6, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 452
    :cond_f
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 453
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.DirectReply"

    .line 452
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 453
    if-nez v4, :cond_0

    .line 456
    invoke-virtual {p0, v7}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    goto/16 :goto_0

    .line 460
    .end local v2    # "selectedMessageQueue":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_10
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "handleIntent() called without an alertQueue or senderQueue."

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 8
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 467
    const-string/jumbo v6, "Action"

    invoke-static {p1, v6, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 469
    .local v0, "actionValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 532
    :goto_0
    return v4

    .line 471
    :cond_0
    const-string/jumbo v6, "safereader:call"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 472
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->call()V

    move v4, v5

    .line 473
    goto :goto_0

    .line 474
    :cond_1
    const-string/jumbo v6, "safereader:reply"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 475
    const-string/jumbo v6, "Text"

    invoke-static {p1, v6, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 477
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->reply(Ljava/lang/String;)V

    move v4, v5

    .line 478
    goto :goto_0

    .line 479
    .end local v2    # "message":Ljava/lang/String;
    :cond_2
    const-string/jumbo v6, "message:next"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 482
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->next()V

    move v4, v5

    .line 483
    goto :goto_0

    .line 484
    :cond_3
    const-string/jumbo v6, "sms:forward"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 486
    const-string/jumbo v6, "Contact"

    invoke-static {p1, v6, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 487
    .local v1, "contactName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->forward(Ljava/lang/String;)V

    move v4, v5

    .line 488
    goto :goto_0

    .line 489
    .end local v1    # "contactName":Ljava/lang/String;
    :cond_4
    const-string/jumbo v6, "message:choose"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 491
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v6

    .line 494
    sget-object v7, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_MSGCHOOSE:Lcom/nuance/sample/CCFieldIds;

    .line 495
    invoke-virtual {v7}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 494
    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v7

    .line 492
    invoke-interface {v6, v7}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 496
    const-string/jumbo v6, "Which"

    invoke-static {p1, v6, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 498
    .local v3, "which":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 499
    invoke-direct {p0, v3}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->handleWhich(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v4, v5

    .line 500
    goto :goto_0

    .line 506
    :cond_5
    const-string/jumbo v6, "Contact"

    .line 505
    invoke-static {p1, v6, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 507
    .restart local v1    # "contactName":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 508
    invoke-direct {p0, v1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->handleContact(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v4, v5

    .line 509
    goto/16 :goto_0

    .line 513
    :cond_6
    invoke-direct {p0, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->noValidMessages(Z)V

    move v4, v5

    .line 514
    goto/16 :goto_0

    .line 517
    .end local v1    # "contactName":Ljava/lang/String;
    .end local v3    # "which":Ljava/lang/String;
    :cond_7
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isReadMessageBodyEnabled()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 518
    iput-boolean v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 525
    :cond_8
    :goto_1
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_b

    .line 526
    invoke-virtual {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displaySingleMessage(Z)V

    :cond_9
    :goto_2
    move v4, v5

    .line 532
    goto/16 :goto_0

    .line 520
    :cond_a
    iget-boolean v6, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    if-nez v6, :cond_8

    .line 522
    iput-boolean v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    goto :goto_1

    .line 527
    :cond_b
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_c

    .line 528
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleMessages(Z)V

    goto :goto_2

    .line 529
    :cond_c
    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 530
    invoke-direct {p0, v5}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->displayMultipleSenders(Z)V

    goto :goto_2
.end method

.method protected handleNeedMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "body"    # Ljava/lang/String;
    .param p2, "contact"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1144
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const-wide/16 v3, 0x0

    move-object v1, p2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 1146
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v2, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v3, v0

    move-object v5, p3

    move v7, v6

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 1148
    .local v2, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 1149
    invoke-static {v0}, Lcom/nuance/sample/controllers/SMSController;->setContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 1150
    invoke-static {v2}, Lcom/nuance/sample/controllers/SMSController;->setContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 1153
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1154
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    .line 1155
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1154
    invoke-static {v1}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v1

    .line 1155
    if-eqz v1, :cond_1

    .line 1156
    :cond_0
    invoke-virtual {p0, p2, p3, p1}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->promptForConfirmation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    :goto_0
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->startRecording()V

    .line 1162
    return-void

    .line 1158
    :cond_1
    invoke-direct {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->promptForMessage()V

    goto :goto_0
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 1387
    iget-boolean v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isSilentMode:Z

    return v0
.end method

.method protected isWithoutAskingMsg()Z
    .locals 1

    .prologue
    .line 642
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isViewCoverOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 642
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected multiContactsSearched(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1508
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method protected varargs noContactsSearched([Ljava/lang/String;)V
    .locals 0
    .param p1, "prompts"    # [Ljava/lang/String;

    .prologue
    .line 1501
    return-void
.end method

.method protected processCommand(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 1493
    const/4 v0, 0x0

    return v0
.end method

.method protected promptForConfirmation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "contact"    # Ljava/lang/String;
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 1182
    sget-object v3, Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;->NEED_CONFIRMATION:Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;

    iput-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->state:Lcom/nuance/sample/safereader/SampleAlertReadoutController$State;

    .line 1184
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0024

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1185
    .local v1, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-direct {v3, p3, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->mt:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 1187
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-direct {v2, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    .local v2, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->mt:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 1190
    const/4 v0, 0x0

    .line 1191
    .local v0, "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeSendButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->cancelButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 1192
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    .line 1194
    sget-object v4, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

    .line 1195
    invoke-virtual {v4}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 1194
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    .line 1193
    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 1197
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v5, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->mt:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-interface {v3, v4, v0, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 1199
    return-void
.end method

.method reply(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 1122
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v1, :cond_1

    .line 1123
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 1125
    .local v0, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 1126
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 1125
    invoke-virtual {p0, p1, v1, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->handleNeedMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    .end local v0    # "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_0
    return-void

    .line 1130
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unexpected alert type of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1131
    const-string/jumbo v3, ".  Ignoring alert."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1130
    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0

    .line 1135
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    .line 1137
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1136
    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 1139
    iget-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v2, "This should not be possible, replying to a null alert."

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->warn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected runFinalAction()Z
    .locals 1

    .prologue
    .line 1418
    const/4 v0, 0x0

    return v0
.end method

.method public setCurrentContactSearch(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentContactSearch"    # Ljava/lang/String;

    .prologue
    .line 1395
    iput-object p1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 1396
    return-void
.end method

.method public setSenderQueue(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const/4 v1, 0x0

    .line 95
    iput-object p1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    .line 97
    const/4 v0, 0x1

    iput v0, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentPage:I

    .line 98
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 99
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 100
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 101
    iput-object v1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 102
    return-void
.end method

.method public setShowMessageBody(Z)V
    .locals 0
    .param p1, "showMessageBody"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->showMessageBody:Z

    .line 106
    return-void
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 1391
    iput-boolean p1, p0, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->isSilentMode:Z

    .line 1392
    return-void
.end method
