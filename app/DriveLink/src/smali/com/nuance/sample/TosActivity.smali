.class public Lcom/nuance/sample/TosActivity;
.super Landroid/app/Activity;
.source "TosActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activity:Landroid/app/Activity;

.field private notificationManager:Lcom/nuance/sample/notification/NotificationPopUpManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/nuance/sample/TosActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/TosActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/nuance/sample/TosActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/nuance/sample/TosActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/nuance/sample/TosActivity;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/TosActivity;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/nuance/sample/TosActivity;->sendHello()V

    return-void
.end method

.method static synthetic access$3(Lcom/nuance/sample/TosActivity;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/nuance/sample/TosActivity;->startMainActivity()V

    return-void
.end method

.method private sendHello()V
    .locals 2

    .prologue
    .line 118
    const-string/jumbo v0, "hello_request_complete"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    invoke-static {}, Lcom/vlingo/core/internal/util/SayHello;->sendHello()Z

    .line 122
    :cond_0
    return-void
.end method

.method private startMainActivity()V
    .locals 3

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/nuance/sample/TosActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/nuance/sample/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 97
    invoke-static {}, Lcom/nuance/sample/SampleAppApplication;->getInstance()Lcom/nuance/sample/SampleAppApplication;

    move-result-object v1

    invoke-virtual {p0}, Lcom/nuance/sample/TosActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/nuance/sample/SampleAppApplication;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 98
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 30
    sget-object v6, Lcom/nuance/sample/TosActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "onCreate()"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onAppLaunched()V

    .line 33
    const v6, 0x7f030078

    invoke-virtual {p0, v6}, Lcom/nuance/sample/TosActivity;->setContentView(I)V

    .line 34
    iput-object p0, p0, Lcom/nuance/sample/TosActivity;->activity:Landroid/app/Activity;

    .line 35
    const-string/jumbo v6, "tos_accepted"

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 37
    invoke-direct {p0}, Lcom/nuance/sample/TosActivity;->sendHello()V

    .line 38
    iget-object v5, p0, Lcom/nuance/sample/TosActivity;->activity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    .line 39
    invoke-direct {p0}, Lcom/nuance/sample/TosActivity;->startMainActivity()V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/lmtt/MdsoUtils;->isSlave()Z

    move-result v6

    if-nez v6, :cond_2

    .line 42
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "currentLanguage":Ljava/lang/String;
    move-object v3, v0

    .line 44
    .local v3, "originalLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v1

    .line 45
    .local v1, "currentLocale":Ljava/util/Locale;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 46
    .local v4, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 47
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v6

    array-length v7, v6

    :goto_1
    if-lt v5, v7, :cond_3

    .line 53
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 54
    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;)V

    .line 62
    .end local v0    # "currentLanguage":Ljava/lang/String;
    .end local v1    # "currentLocale":Ljava/util/Locale;
    .end local v3    # "originalLanguage":Ljava/lang/String;
    .end local v4    # "systemLocale":Ljava/util/Locale;
    :cond_2
    new-instance v5, Lcom/nuance/sample/notification/NotificationPopUpManager;

    new-instance v6, Lcom/nuance/sample/TosActivity$1;

    invoke-direct {v6, p0}, Lcom/nuance/sample/TosActivity$1;-><init>(Lcom/nuance/sample/TosActivity;)V

    .line 84
    new-instance v7, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    invoke-direct {v7}, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;-><init>()V

    invoke-direct {v5, p0, v6, v7}, Lcom/nuance/sample/notification/NotificationPopUpManager;-><init>(Landroid/app/Activity;Landroid/os/Handler;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    .line 62
    iput-object v5, p0, Lcom/nuance/sample/TosActivity;->notificationManager:Lcom/nuance/sample/notification/NotificationPopUpManager;

    .line 86
    sget-object v5, Lcom/nuance/sample/TosActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "created new notification manager"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v5, p0, Lcom/nuance/sample/TosActivity;->notificationManager:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-static {v5}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->setNotificationPopUpManager(Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;)V

    .line 88
    invoke-static {}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->needNotifications()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    iget-object v5, p0, Lcom/nuance/sample/TosActivity;->notificationManager:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-virtual {v5}, Lcom/nuance/sample/notification/NotificationPopUpManager;->showNextNotification()V

    goto :goto_0

    .line 47
    .restart local v0    # "currentLanguage":Ljava/lang/String;
    .restart local v1    # "currentLocale":Ljava/util/Locale;
    .restart local v3    # "originalLanguage":Ljava/lang/String;
    .restart local v4    # "systemLocale":Ljava/util/Locale;
    :cond_3
    aget-object v2, v6, v5

    .line 48
    .local v2, "lang":Ljava/lang/CharSequence;
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 49
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 50
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 47
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->flagShown:Z

    .line 105
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 106
    return-void
.end method
