.class Lcom/nuance/sample/TosActivity$1;
.super Landroid/os/Handler;
.source "TosActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/TosActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/TosActivity;


# direct methods
.method constructor <init>(Lcom/nuance/sample/TosActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/TosActivity$1;->this$0:Lcom/nuance/sample/TosActivity;

    .line 62
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 64
    # getter for: Lcom/nuance/sample/TosActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/TosActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "NotificationPopUpManager handleMessage "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 83
    :goto_0
    :pswitch_0
    return-void

    .line 67
    :pswitch_1
    # getter for: Lcom/nuance/sample/TosActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/TosActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "NotificationPopUpManager ACCEPTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-static {v3}, Lcom/vlingo/core/internal/settings/Settings;->setTOSAccepted(Z)V

    .line 69
    const-string/jumbo v0, "iux_complete"

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 70
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onTosAccepted()V

    .line 71
    invoke-static {}, Lcom/nuance/sample/services/SampleServicesUtil;->getInstance()Lcom/nuance/sample/services/SampleServicesUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/services/SampleServicesUtil;->startLocalServices()Z

    .line 73
    iget-object v0, p0, Lcom/nuance/sample/TosActivity$1;->this$0:Lcom/nuance/sample/TosActivity;

    # getter for: Lcom/nuance/sample/TosActivity;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/nuance/sample/TosActivity;->access$1(Lcom/nuance/sample/TosActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 75
    iget-object v0, p0, Lcom/nuance/sample/TosActivity$1;->this$0:Lcom/nuance/sample/TosActivity;

    # invokes: Lcom/nuance/sample/TosActivity;->sendHello()V
    invoke-static {v0}, Lcom/nuance/sample/TosActivity;->access$2(Lcom/nuance/sample/TosActivity;)V

    .line 76
    iget-object v0, p0, Lcom/nuance/sample/TosActivity$1;->this$0:Lcom/nuance/sample/TosActivity;

    # invokes: Lcom/nuance/sample/TosActivity;->startMainActivity()V
    invoke-static {v0}, Lcom/nuance/sample/TosActivity;->access$3(Lcom/nuance/sample/TosActivity;)V

    goto :goto_0

    .line 79
    :pswitch_2
    # getter for: Lcom/nuance/sample/TosActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/TosActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "NotificationPopUpManager CANCELED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/nuance/sample/TosActivity$1;->this$0:Lcom/nuance/sample/TosActivity;

    # getter for: Lcom/nuance/sample/TosActivity;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/nuance/sample/TosActivity;->access$1(Lcom/nuance/sample/TosActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
