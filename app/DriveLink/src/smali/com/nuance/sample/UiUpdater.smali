.class public interface abstract Lcom/nuance/sample/UiUpdater;
.super Ljava/lang/Object;
.source "UiUpdater.java"


# virtual methods
.method public abstract displayError(Ljava/lang/CharSequence;)V
.end method

.method public abstract displaySystemTurn(Ljava/lang/CharSequence;)V
.end method

.method public abstract displayUserTurn(Ljava/lang/CharSequence;)V
.end method

.method public abstract displayWidgetContent(Ljava/lang/CharSequence;)V
.end method

.method public abstract getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
.end method

.method public abstract handleUserCancel()V
.end method

.method public abstract updateMicRMSChange(I)V
.end method

.method public abstract updateMicState(Lcom/nuance/sample/MicState;)V
.end method
