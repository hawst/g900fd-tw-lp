.class public Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SampleSendMessageAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;


# static fields
.field private static final log:Lcom/vlingo/core/facade/logging/ILogger;


# instance fields
.field private addresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    .line 24
    sput-object v0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 28
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->addresses:Ljava/util/List;

    .line 21
    return-void
.end method

.method static synthetic access$3()Lcom/vlingo/core/facade/logging/ILogger;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    return-object v0
.end method

.method static synthetic access$4(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->message:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->errString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private errString(I)Ljava/lang/String;
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 73
    packed-switch p1, :pswitch_data_0

    .line 79
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    .line 80
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_unknown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 79
    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 75
    :pswitch_1
    const-string/jumbo v0, "no-service"

    goto :goto_0

    .line 77
    :pswitch_2
    const-string/jumbo v0, "radio-off"

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendSMS(Ljava/lang/String;)V
    .locals 3
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->message:Ljava/lang/String;

    new-instance v2, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;

    invoke-direct {v2, p0}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;-><init>(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;)V

    invoke-static {v0, p1, v1, v2}, Lcom/vlingo/core/internal/util/SMSUtil;->sendSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;)V

    .line 70
    return-void
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->addresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    return-object p0
.end method

.method public bridge synthetic address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->address(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    move-result-object v0

    return-object v0
.end method

.method public addresses(Ljava/util/List;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "addresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->addresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 42
    return-object p0
.end method

.method public bridge synthetic addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->addresses(Ljava/util/List;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    move-result-object v0

    return-object v0
.end method

.method protected execute()V
    .locals 3

    .prologue
    .line 47
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->addresses:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 48
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->addresses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 54
    :goto_1
    return-void

    .line 48
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 49
    .local v0, "address":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->sendSMS(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    .end local v0    # "address":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "No address"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public message(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->message:Ljava/lang/String;

    .line 37
    return-object p0
.end method

.method public bridge synthetic message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->message(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    move-result-object v0

    return-object v0
.end method
