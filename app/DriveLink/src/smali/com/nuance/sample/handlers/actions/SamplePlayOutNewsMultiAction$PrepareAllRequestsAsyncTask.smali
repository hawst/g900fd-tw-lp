.class Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
.super Landroid/os/AsyncTask;
.source "SamplePlayOutNewsMultiAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrepareAllRequestsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;


# direct methods
.method constructor <init>(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v7, 0x0

    .line 77
    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$0()Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v3

    const-string/jumbo v4, "start preparing the rest of the requests."

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v3

    .line 80
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 81
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Ljava/util/ArrayList;

    .line 82
    iget-object v3, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;
    invoke-static {v3}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$1(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Ljava/util/LinkedList;

    move-result-object v3

    .line 81
    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 83
    .local v1, "clonedRequestsList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/audio/TTSRequest;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 92
    :cond_0
    return-object v7

    .line 83
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/audio/TTSRequest;

    .line 84
    .local v2, "ttsRequest":Lcom/vlingo/core/internal/audio/TTSRequest;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 87
    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$0()Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "preparing: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lcom/vlingo/core/internal/audio/TTSRequest;->body:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 90
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/TTSEngine;

    move-result-object v4

    .line 89
    invoke-virtual {v2, v0, v4}, Lcom/vlingo/core/internal/audio/TTSRequest;->prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSEngine;)Z

    goto :goto_0
.end method
