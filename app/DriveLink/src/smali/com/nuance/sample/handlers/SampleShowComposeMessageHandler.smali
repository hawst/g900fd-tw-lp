.class public Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleShowComposeMessageHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private allMatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field protected forwardedMessage:Ljava/lang/String;

.field log:Lcom/vlingo/core/facade/logging/ILogger;

.field protected messages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field

.field protected replyRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;

    .line 58
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 57
    sput-object v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->TAG:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    .line 54
    const-class v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 45
    return-void
.end method

.method private extractAddresses(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)Ljava/util/List;
    .locals 5
    .param p1, "messageType"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 218
    .local v0, "addresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v2

    .line 219
    .local v2, "recipients":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;>;"
    if-eqz v2, :cond_2

    .line 220
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 227
    .end local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-object v0

    .line 220
    .restart local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 221
    .local v1, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 222
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 227
    .end local v1    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private extractContactNames(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "recipients"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 233
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 235
    .local v2, "receiverName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->allMatches:Ljava/util/List;

    invoke-static {v5, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    .line 237
    .local v4, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 238
    .local v1, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    .line 239
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 248
    :cond_0
    if-eqz v1, :cond_1

    .line 249
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 253
    :cond_1
    return-object v2

    .line 239
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 240
    .local v3, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v6, v3, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v6, :cond_3

    .line 242
    iget-object v6, v3, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v6, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 244
    :cond_3
    const-string/jumbo v6, ""

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 249
    .end local v3    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    .local v0, "number":Ljava/lang/String;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private extractPhoneNumbers(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "recipients"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 258
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 260
    .local v2, "receiverAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->allMatches:Ljava/util/List;

    invoke-static {v5, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    .line 262
    .local v4, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 263
    .local v1, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    .line 264
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 268
    :cond_0
    if-eqz v1, :cond_1

    .line 269
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 273
    :cond_1
    return-object v2

    .line 264
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 265
    .local v3, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v6, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 269
    .end local v3    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 270
    .local v0, "number":Ljava/lang/String;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private sendEmail(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 2
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 209
    .local p3, "recipients":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    .line 211
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->contacts(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v0

    .line 212
    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->subject(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->queue()V

    .line 214
    :cond_0
    return-void
.end method

.method private sendSMS(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 5
    .param p1, "content"    # Ljava/lang/String;
    .param p2, "recipients"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-direct {p0, p2}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->extractAddresses(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)Ljava/util/List;

    move-result-object v2

    .line 191
    .local v2, "numbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 192
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    invoke-virtual {p0, v3, v4}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    .line 193
    invoke-interface {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->queue()V

    .line 196
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 197
    :try_start_0
    invoke-direct {p0, p2}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->extractAddresses(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)Ljava/util/List;

    move-result-object v1

    .line 198
    .local v1, "contactNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 199
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 205
    .end local v1    # "contactNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    return-void

    .line 199
    .restart local v1    # "contactNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 200
    .local v0, "contact":Ljava/lang/String;
    new-instance v4, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;

    invoke-direct {v4, p1, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 203
    .end local v0    # "contact":Ljava/lang/String;
    .end local v1    # "contactNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 326
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 327
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 328
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 329
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 319
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 320
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 321
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 322
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 27
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 67
    invoke-super/range {p0 .. p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 69
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v24

    const-string/jumbo v25, "car-sms-message"

    invoke-interface/range {v24 .. v25}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 71
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v24

    .line 72
    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 71
    invoke-interface/range {v24 .. v25}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/util/List;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->allMatches:Ljava/util/List;

    .line 74
    const-string/jumbo v24, "subject"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 75
    .local v21, "subject":Ljava/lang/String;
    const-string/jumbo v24, "message"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 76
    .local v22, "text":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->messages:Ljava/util/LinkedList;

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->messages:Ljava/util/LinkedList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_0

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getText()Ljava/lang/String;

    move-result-object v22

    .line 81
    :cond_0
    const-string/jumbo v24, "msgtype"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 82
    .local v14, "msgType":Ljava/lang/String;
    new-instance v13, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v24, ""

    const-string/jumbo v25, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-direct {v13, v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .local v13, "messageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    const-string/jumbo v24, "recipients"

    .line 85
    const/16 v25, 0x1

    .line 84
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v20

    .line 87
    .local v20, "recipients":Ljava/lang/String;
    const/16 v19, 0x0

    .line 88
    .local v19, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/16 v16, 0x0

    .line 89
    .local v16, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->allMatches:Ljava/util/List;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->allMatches:Ljava/util/List;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_1

    .line 92
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->allMatches:Ljava/util/List;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    .line 101
    :cond_1
    :try_start_1
    invoke-static/range {v20 .. v20}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v16

    .line 107
    if-eqz v19, :cond_3

    .line 108
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_2
    :goto_0
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-nez v25, :cond_8

    .line 120
    :cond_3
    if-eqz v16, :cond_4

    .line 121
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_1
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-nez v25, :cond_9

    .line 126
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->replyRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    move-object/from16 v24, v0

    if-eqz v24, :cond_5

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->replyRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 130
    :cond_5
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setSubject(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v13, v14}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    .line 133
    const/4 v7, 0x0

    .line 134
    .local v7, "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v24, "confirm"

    const/16 v25, 0x1

    .line 135
    const/16 v26, 0x0

    .line 134
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v10

    .line 136
    .local v10, "isConfirm":Z
    const-string/jumbo v24, "execute"

    const/16 v25, 0x1

    .line 137
    const/16 v26, 0x0

    .line 136
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v11

    .line 138
    .local v11, "isExecute":Z
    if-nez v10, :cond_a

    if-eqz v11, :cond_a

    const/4 v12, 0x1

    .line 139
    .local v12, "isExecuteStep":Z
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeSendButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v24

    .line 140
    invoke-virtual/range {v24 .. v24}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->cancelButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v5

    .line 141
    .local v5, "cancelButton":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    if-nez v12, :cond_b

    .line 142
    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v24

    .line 141
    invoke-static/range {v24 .. v24}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v24

    .line 142
    if-nez v24, :cond_b

    .line 143
    move-object v7, v5

    .line 151
    :cond_6
    :goto_3
    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v24

    const-string/jumbo v25, "email"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    sget-object v23, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeEmail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 153
    .local v23, "wk":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    :goto_4
    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move-object/from16 v2, p0

    invoke-interface {v0, v1, v7, v13, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 155
    const-string/jumbo v24, "execute"

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 156
    const-string/jumbo v24, "message"

    .line 157
    const/16 v25, 0x0

    .line 156
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 158
    .local v6, "content":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 159
    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_e

    .line 160
    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v6

    .line 168
    :cond_7
    :try_start_2
    const-string/jumbo v24, "msgtype"

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v24

    .line 169
    const-string/jumbo v25, "email"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 170
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v6, v2}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->sendEmail(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 174
    :goto_5
    const/16 v24, 0x1

    .line 181
    .end local v5    # "cancelButton":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v6    # "content":Ljava/lang/String;
    .end local v7    # "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v10    # "isConfirm":Z
    .end local v11    # "isExecute":Z
    .end local v12    # "isExecuteStep":Z
    .end local v23    # "wk":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    :goto_6
    return v24

    .line 93
    :catch_0
    move-exception v8

    .line 94
    .local v8, "e":Lorg/json/JSONException;
    sget-object v24, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->TAG:Ljava/lang/String;

    .line 95
    const-string/jumbo v25, "Error extracting json contacts and numbers from recipients parameter"

    .line 94
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/16 v24, 0x0

    goto :goto_6

    .line 102
    .end local v8    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v9

    .line 103
    .local v9, "e1":Lorg/json/JSONException;
    sget-object v24, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->TAG:Ljava/lang/String;

    .line 104
    const-string/jumbo v25, "Error extracting json numbers from recipients parameter"

    .line 103
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/16 v24, 0x0

    goto :goto_6

    .line 108
    .end local v9    # "e1":Lorg/json/JSONException;
    :cond_8
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 109
    .local v18, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v18, :cond_2

    .line 110
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 111
    .local v4, "address":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_2

    .line 112
    new-instance v17, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 113
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    move-object/from16 v25, v0

    .line 114
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    move-object/from16 v26, v0

    .line 112
    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .local v17, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    goto/16 :goto_0

    .line 121
    .end local v4    # "address":Ljava/lang/String;
    .end local v17    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    .end local v18    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_9
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 122
    .local v15, "number":Ljava/lang/String;
    new-instance v17, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    const-string/jumbo v25, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v15}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .restart local v17    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    goto/16 :goto_1

    .line 138
    .end local v15    # "number":Ljava/lang/String;
    .end local v17    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    .restart local v7    # "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .restart local v10    # "isConfirm":Z
    .restart local v11    # "isExecute":Z
    :cond_a
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 144
    .restart local v5    # "cancelButton":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .restart local v12    # "isExecuteStep":Z
    :cond_b
    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v24

    if-nez v24, :cond_c

    .line 145
    move-object v7, v5

    .line 146
    goto/16 :goto_3

    :cond_c
    invoke-virtual {v13}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_6

    .line 147
    move-object v7, v5

    goto/16 :goto_3

    .line 152
    :cond_d
    sget-object v23, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    goto/16 :goto_4

    .line 162
    .restart local v6    # "content":Ljava/lang/String;
    .restart local v23    # "wk":Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    :cond_e
    new-instance v24, Ljava/security/InvalidParameterException;

    .line 163
    const-string/jumbo v25, "Null or empty message on execute step."

    .line 162
    invoke-direct/range {v24 .. v25}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 172
    :cond_f
    :try_start_3
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v13}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->sendSMS(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_5

    .line 175
    :catch_2
    move-exception v8

    .line 176
    .restart local v8    # "e":Lorg/json/JSONException;
    new-instance v24, Ljava/security/InvalidParameterException;

    .line 177
    new-instance v25, Ljava/lang/StringBuilder;

    const-string/jumbo v26, "Invalid format for recipients action parameter: "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 178
    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 177
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 176
    invoke-direct/range {v24 .. v25}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 181
    .end local v6    # "content":Ljava/lang/String;
    .end local v8    # "e":Lorg/json/JSONException;
    :cond_10
    const/16 v24, 0x0

    goto/16 :goto_6
.end method

.method protected getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    const-string/jumbo v0, " "

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 283
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 284
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 285
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 311
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 287
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;-><init>()V

    .line 288
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 289
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 290
    const-string/jumbo v1, ""

    .line 291
    .local v1, "message":Ljava/lang/String;
    const-string/jumbo v2, "message"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 293
    const-string/jumbo v2, "message"

    .line 292
    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 295
    :cond_3
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;

    const-string/jumbo v2, "message"

    invoke-direct {v0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 298
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    .end local v1    # "message":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.SubjectChange"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 299
    const-string/jumbo v2, "subject"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 301
    const-string/jumbo v2, "subject"

    .line 300
    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 302
    .restart local v1    # "message":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;

    const-string/jumbo v2, "subject"

    invoke-direct {v0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    .restart local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 306
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    .end local v1    # "message":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.EditTextClicked"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 307
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    goto/16 :goto_0

    .line 309
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
