.class public Lcom/nuance/sample/handlers/SampleProcessCommandHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleProcessCommandHandler.java"


# static fields
.field private static final APP_DOMAIN:Ljava/lang/String; = "app"

.field public static final CANCEL:Ljava/lang/String; = "cancel"

.field public static final COMMAND_PARAM:Ljava/lang/String; = "Command"

.field public static final DOMAIN_PARAMETER:Ljava/lang/String; = "Domain"

.field public static final EXECUTE:Ljava/lang/String; = "execute"

.field public static final EXTEND:Ljava/lang/String; = "extend"

.field public static final IGNORE:Ljava/lang/String; = "ignore"

.field private static final LOCATION_DOMAIN:Ljava/lang/String; = "location"

.field public static final LOOKUP:Ljava/lang/String; = "lookup"

.field private static final MESSAGE_DOMAIN:Ljava/lang/String; = "message"

.field public static final MUSIC_DOMAIN:Ljava/lang/String; = "music"

.field public static final NAVIGATION_DOMAIN:Ljava/lang/String; = "navigation"

.field public static final NEXT:Ljava/lang/String; = "next"

.field public static final NO:Ljava/lang/String; = "no"

.field public static final NOP:Ljava/lang/String; = "nop"

.field public static final PAUSE:Ljava/lang/String; = "pause"

.field private static final PHONE_DOMAIN:Ljava/lang/String; = "phone"

.field public static final PLAY_ALL:Ljava/lang/String; = "PlayAll"

.field public static final PLAY_PREV:Ljava/lang/String; = "prev"

.field public static final PREV:Ljava/lang/String; = "previous"

.field private static final RADIO_DOMAIN:Ljava/lang/String; = "radio"

.field public static final READ:Ljava/lang/String; = "repeat"

.field public static final RESET:Ljava/lang/String; = "reset"

.field public static final RESTART:Ljava/lang/String; = "restart"

.field public static final RESUME:Ljava/lang/String; = "resume"

.field public static final ROUTE:Ljava/lang/String; = "route"

.field public static final ROUTE_NOP:Ljava/lang/String; = "nop"

.field private static final SAFEREADER_DOMAIN:Ljava/lang/String; = "safereader"

.field public static final SEND:Ljava/lang/String; = "send"

.field public static final SMS:Ljava/lang/String; = "sms"

.field public static final START:Ljava/lang/String; = "start"

.field public static final VOLUME_DOWN:Ljava/lang/String; = "volumedown"

.field public static final VOLUME_MUTE:Ljava/lang/String; = "mute"

.field public static final VOLUME_UNMUTE:Ljava/lang/String; = "unmute"

.field public static final VOLUME_UP:Ljava/lang/String; = "volumeup"

.field public static final YES:Ljava/lang/String; = "yes"

.field private static final log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 72
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    return-void
.end method

.method private getControllerForDomain(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/Controller;
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    const-string/jumbo v2, "Domain"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "domain":Ljava/lang/String;
    const-string/jumbo v2, "music"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    const-class v2, Lcom/nuance/sample/controllers/SampleMusicController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    .line 151
    :cond_0
    const-string/jumbo v2, "radio"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    const-class v2, Lcom/nuance/sample/controllers/SampleRadioController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 153
    goto :goto_0

    :cond_1
    const-string/jumbo v2, "phone"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 154
    const-class v2, Lcom/nuance/sample/controllers/SampleDialPageController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 155
    goto :goto_0

    :cond_2
    const-string/jumbo v2, "message"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 156
    const-class v2, Lcom/nuance/sample/controllers/SMSController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 157
    goto :goto_0

    :cond_3
    const-string/jumbo v2, "location"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 158
    const-class v2, Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 159
    goto :goto_0

    :cond_4
    const-string/jumbo v2, "app"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 160
    const-class v2, Lcom/nuance/sample/controllers/SampleShowOpenAppWidgetController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 161
    goto :goto_0

    :cond_5
    const-string/jumbo v2, "safereader"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 162
    const-class v2, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 163
    goto :goto_0

    :cond_6
    const-string/jumbo v2, "navigation"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 164
    const-class v2, Lcom/nuance/sample/controllers/SampleNavigationController;

    invoke-virtual {p0, v2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 165
    goto :goto_0

    .line 168
    :cond_7
    sget-object v2, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "no controller found by domain, try to find one in state"

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 169
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .restart local v0    # "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    goto/16 :goto_0
.end method

.method private isRepeatCommand(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 142
    const-string/jumbo v1, "Command"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "command":Ljava/lang/String;
    const-string/jumbo v1, "nop"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private processVolumeCommands(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 124
    .line 125
    const-string/jumbo v6, "Command"

    .line 124
    invoke-static {p1, v6, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "command":Ljava/lang/String;
    invoke-static {v0}, Lcom/nuance/sample/music/VolumeAction;->getActionByKey(Ljava/lang/String;)Lcom/nuance/sample/music/VolumeAction;

    move-result-object v3

    .line 127
    .local v3, "volumeAction":Lcom/nuance/sample/music/VolumeAction;
    if-eqz v3, :cond_1

    .line 128
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v6

    .line 129
    invoke-interface {v6}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 130
    const v7, 0x7f0a0838

    new-array v8, v4, [Ljava/lang/Object;

    aput-object v0, v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "msg":Ljava/lang/String;
    invoke-interface {p2, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v3}, Lcom/nuance/sample/music/VolumeAction;->execute()Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "errorMsg":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 134
    invoke-interface {p2, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .end local v1    # "errorMsg":Ljava/lang/String;
    .end local v2    # "msg":Ljava/lang/String;
    :cond_0
    :goto_0
    return v4

    :cond_1
    move v4, v5

    goto :goto_0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v4, 0x0

    .line 77
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 79
    invoke-direct {p0, p1}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->isRepeatCommand(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 81
    sget-object v5, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v6, "executeAction for repeat handled as invalid"

    invoke-interface {v5, v6}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 82
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a03c6

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "msg":Ljava/lang/String;
    invoke-interface {p2, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    .line 119
    .end local v3    # "msg":Ljava/lang/String;
    :goto_0
    return v4

    .line 88
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getControllerForDomain(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v1

    .line 89
    .local v1, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    if-eqz v1, :cond_1

    .line 90
    invoke-virtual {v1, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v4

    goto :goto_0

    .line 92
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    .line 93
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "currentFieldId":Ljava/lang/String;
    sget-object v5, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v5}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 95
    if-nez v5, :cond_2

    .line 96
    sget-object v5, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v5}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 97
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 98
    sget-object v5, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v5}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 99
    if-eqz v5, :cond_3

    .line 100
    :cond_2
    const-class v4, Lcom/nuance/sample/controllers/SampleLocationController;

    invoke-virtual {p0, v4}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v1

    .line 101
    invoke-virtual {v1, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v4

    goto :goto_0

    .line 103
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->processVolumeCommands(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 104
    const-string/jumbo v3, "no context to handle action"

    .line 105
    .restart local v3    # "msg":Ljava/lang/String;
    const-string/jumbo v5, "Command"

    invoke-interface {p1, v5}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "command":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 107
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 109
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, " in domain for action "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 110
    invoke-interface {p2, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v6}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 112
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    goto/16 :goto_0

    .line 115
    .end local v0    # "command":Ljava/lang/String;
    .end local v3    # "msg":Ljava/lang/String;
    :cond_5
    sget-object v5, Lcom/nuance/sample/handlers/SampleProcessCommandHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v6, "executeAction processed volume"

    invoke-interface {v5, v6}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
