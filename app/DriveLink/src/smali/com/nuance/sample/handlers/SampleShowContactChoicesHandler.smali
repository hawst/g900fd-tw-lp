.class public Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleShowContactChoicesHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public choiceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public displayedChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;

    .line 21
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 20
    sput-object v0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    .line 24
    iput-object v0, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;

    .line 18
    return-void
.end method

.method private getContactNames(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 65
    .local v1, "counter":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 65
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 66
    .local v2, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v4, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    add-int/lit8 v1, v1, 0x1

    .line 68
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-eq v1, v4, :cond_0

    .line 69
    const-string/jumbo v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 29
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 30
    const-string/jumbo v3, "choices"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 32
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getChoicesAsInts(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    .line 39
    check-cast v0, Ljava/util/List;

    .line 41
    .local v0, "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;

    if-eqz v3, :cond_0

    if-nez v0, :cond_1

    .line 42
    :cond_0
    sget-object v3, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 43
    const-string/jumbo v4, "SampleShowContactChoicesHandler.executeAction no matches"

    .line 42
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    .end local v0    # "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :goto_1
    return v6

    .line 33
    :catch_0
    move-exception v2

    .line 34
    .local v2, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 35
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "DialogDataUtil.ChoiceUtil displayList: JSONException"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 36
    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 34
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 47
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v0    # "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;

    .line 46
    invoke-static {v0, v3}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->extractByPosition(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    .line 48
    sget-object v3, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "SampleShowContactChoicesHandler.executeAction got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 49
    iget-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " choices"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 48
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->getContactNames(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "contactNames":Ljava/lang/String;
    sget-object v3, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "SampleShowContactChoicesHandler.executeAction names \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 52
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 51
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoText(Ljava/lang/String;)V

    goto :goto_1

    .line 56
    .end local v0    # "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v1    # "contactNames":Ljava/lang/String;
    :cond_2
    sget-object v3, Lcom/nuance/sample/handlers/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 57
    const-string/jumbo v4, "SampleShowContactChoicesHandler.executeAction unexpected parameter"

    .line 56
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 78
    return-void
.end method
