.class public Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SamplePlayMusicViaIntentAction.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$music$PlayMusicType:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private moodType:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private playMusicType:Lcom/nuance/sample/music/PlayMusicType;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$music$PlayMusicType()[I
    .locals 3

    .prologue
    .line 20
    sget-object v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->$SWITCH_TABLE$com$nuance$sample$music$PlayMusicType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/music/PlayMusicType;->values()[Lcom/nuance/sample/music/PlayMusicType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->MOOD:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_3
    :try_start_3
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_4
    :try_start_4
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_5
    :try_start_5
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    :try_start_6
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PLAYALL:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_7
    :try_start_7
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_8
    :try_start_8
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_9
    :try_start_9
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->SONGLIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_a
    :try_start_a
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_b
    sput-object v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->$SWITCH_TABLE$com$nuance$sample$music$PlayMusicType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_9

    :catch_3
    move-exception v1

    goto :goto_8

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    .line 22
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 21
    sput-object v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method private getPlayMusicTypeStr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 233
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->$SWITCH_TABLE$com$nuance$sample$music$PlayMusicType()[I

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 247
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 235
    :pswitch_1
    const-string/jumbo v0, "album"

    goto :goto_0

    .line 237
    :pswitch_2
    const-string/jumbo v0, "artist"

    goto :goto_0

    .line 239
    :pswitch_3
    const-string/jumbo v0, "playlist"

    goto :goto_0

    .line 241
    :pswitch_4
    const-string/jumbo v0, "title"

    goto :goto_0

    .line 243
    :pswitch_5
    const-string/jumbo v0, "playlist"

    goto :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method protected execute()V
    .locals 24

    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getPlayMusicTypeStr()Ljava/lang/String;

    move-result-object v23

    .line 47
    .local v23, "typeStr":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    .line 48
    .local v22, "tooOld":Ljava/lang/Boolean;
    const/4 v13, 0x1

    .line 49
    .local v13, "isAppCarModeEnabled":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 50
    const/16 v21, 0x0

    .line 53
    .local v21, "squareDB":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 54
    new-instance v14, Landroid/content/Intent;

    .line 55
    const-string/jumbo v2, "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

    .line 54
    invoke-direct {v14, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 56
    .local v14, "moodIntent":Landroid/content/Intent;
    const-string/jumbo v2, "extra_type"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    if-nez v13, :cond_0

    .line 59
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 60
    const-string/jumbo v3, "Music player intent!  Launch music square extra: launchMusicSquare"

    .line 59
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string/jumbo v2, "launchMusicSquare"

    .line 63
    const/4 v3, 0x1

    .line 62
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :cond_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 67
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "content://com.sec.music/music_square/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 67
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 68
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 66
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v21

    .line 74
    :goto_0
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v21, :cond_3

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_3

    .line 75
    :cond_1
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 76
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Music player intent!  Action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 77
    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 78
    const-string/jumbo v4, " moodType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 79
    invoke-virtual {v14}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 80
    const-string/jumbo v5, "extra_type"

    .line 79
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 75
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v14}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 91
    :goto_1
    if-eqz v21, :cond_2

    .line 92
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 214
    .end local v14    # "moodIntent":Landroid/content/Intent;
    .end local v21    # "squareDB":Landroid/database/Cursor;
    :cond_2
    :goto_2
    return-void

    .line 69
    .restart local v14    # "moodIntent":Landroid/content/Intent;
    .restart local v21    # "squareDB":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 70
    .local v9, "e":Ljava/lang/SecurityException;
    :try_start_3
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 71
    const-string/jumbo v3, "Music APK is too old to query count of mood song"

    .line 70
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    goto :goto_0

    .line 84
    .end local v9    # "e":Ljava/lang/SecurityException;
    :cond_3
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 85
    const-string/jumbo v3, "Music player intent! Music Square is not activated"

    .line 84
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 87
    .end local v14    # "moodIntent":Landroid/content/Intent;
    :catch_1
    move-exception v9

    .line 88
    .local v9, "e":Landroid/content/ActivityNotFoundException;
    :try_start_4
    const-string/jumbo v15, "Activity could not be found."

    .line 89
    .local v15, "msg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v15}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 91
    if-eqz v21, :cond_2

    .line 92
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 93
    const/16 v21, 0x0

    goto :goto_2

    .line 90
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v15    # "msg":Ljava/lang/String;
    :catchall_0
    move-exception v2

    .line 91
    if-eqz v21, :cond_4

    .line 92
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 93
    const/16 v21, 0x0

    .line 95
    :cond_4
    throw v2

    .line 96
    .end local v21    # "squareDB":Landroid/database/Cursor;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 97
    invoke-static/range {v23 .. v23}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 101
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 102
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getPlayIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    .line 103
    .local v19, "playIntent":Landroid/content/Intent;
    if-nez v13, :cond_6

    .line 104
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 105
    const-string/jumbo v3, "Music player intent!  Launch music player extra added: launchMusicPlayer"

    .line 104
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const-string/jumbo v2, "launchMusicPlayer"

    const/4 v3, 0x1

    .line 107
    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 110
    :cond_6
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 111
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Music player intent!  Action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 112
    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " typeString: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 113
    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 111
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 110
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_5
    .catch Landroid/content/ActivityNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_2

    .line 116
    .end local v19    # "playIntent":Landroid/content/Intent;
    :catch_2
    move-exception v9

    .line 117
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    const-string/jumbo v15, "Activity could not be found."

    .line 118
    .restart local v15    # "msg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v15}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 119
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v15    # "msg":Ljava/lang/String;
    :catch_3
    move-exception v11

    .line 120
    .local v11, "iae":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    .line 121
    const-string/jumbo v3, "Illegal argument exception in target activity raised"

    .line 120
    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 123
    .end local v11    # "iae":Ljava/lang/IllegalArgumentException;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v2, v3, :cond_9

    .line 124
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 125
    new-instance v19, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.play"

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .restart local v19    # "playIntent":Landroid/content/Intent;
    if-nez v13, :cond_8

    .line 127
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 128
    const-string/jumbo v3, "Music player intent!  Launch music player extra added: launchMusicPlayer"

    .line 127
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const-string/jumbo v2, "launchMusicPlayer"

    .line 131
    const/4 v3, 0x1

    .line 130
    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 134
    :cond_8
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Music player intent!  Action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 134
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_6
    .catch Landroid/content/ActivityNotFoundException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_2

    .line 140
    :catch_4
    move-exception v10

    .line 141
    .local v10, "ex":Landroid/content/ActivityNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Activity "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 142
    const-string/jumbo v3, " could not be found."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 141
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 143
    .restart local v15    # "msg":Ljava/lang/String;
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    invoke-static {v2, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 146
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 147
    .local v8, "context":Landroid/content/Context;
    new-instance v12, Landroid/content/Intent;

    const-class v2, Lcom/nuance/sample/domains/music/MusicMainActivity;

    invoke-direct {v12, v8, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 148
    .local v12, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v12, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 149
    invoke-static {v8, v12}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 151
    .end local v8    # "context":Landroid/content/Context;
    .end local v10    # "ex":Landroid/content/ActivityNotFoundException;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v15    # "msg":Ljava/lang/String;
    .end local v19    # "playIntent":Landroid/content/Intent;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v2, v3, :cond_a

    .line 152
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 153
    new-instance v18, Landroid/content/Intent;

    .line 154
    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.pause"

    .line 153
    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 155
    .local v18, "pauseIntent":Landroid/content/Intent;
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Music player intent!  Action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 155
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_7
    .catch Landroid/content/ActivityNotFoundException; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_2

    .line 160
    :catch_5
    move-exception v9

    .line 161
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 163
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v18    # "pauseIntent":Landroid/content/Intent;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v2, v3, :cond_c

    .line 164
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 165
    new-instance v16, Landroid/content/Intent;

    .line 166
    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.playnext"

    .line 165
    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .local v16, "nextIntent":Landroid/content/Intent;
    if-nez v13, :cond_b

    .line 168
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 169
    const-string/jumbo v3, "Music player intent!  Launch music player extra added: launchMusicPlayer"

    .line 168
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string/jumbo v2, "launchMusicPlayer"

    .line 172
    const/4 v3, 0x1

    .line 171
    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 174
    :cond_b
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 175
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Music player intent!  Action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 174
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_8
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_2

    .line 180
    :catch_6
    move-exception v9

    .line 181
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 184
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v16    # "nextIntent":Landroid/content/Intent;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    if-ne v2, v3, :cond_e

    .line 185
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 186
    new-instance v20, Landroid/content/Intent;

    .line 187
    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.playprevious"

    .line 186
    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    .local v20, "previousIntent":Landroid/content/Intent;
    if-nez v13, :cond_d

    .line 189
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 190
    const-string/jumbo v3, "Music player intent!  Launch music player extra added: launchMusicPlayer"

    .line 189
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string/jumbo v2, "launchMusicPlayer"

    const/4 v3, 0x1

    .line 192
    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 195
    :cond_d
    sget-object v2, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->TAG:Ljava/lang/String;

    .line 196
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Music player intent!  Action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 197
    invoke-virtual/range {v20 .. v20}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 196
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 195
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    const/high16 v2, 0x10000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 201
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-static {v2, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_9
    .catch Landroid/content/ActivityNotFoundException; {:try_start_9 .. :try_end_9} :catch_7

    goto/16 :goto_2

    .line 203
    :catch_7
    move-exception v9

    .line 204
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 209
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v20    # "previousIntent":Landroid/content/Intent;
    :cond_e
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    .line 211
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 210
    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v17

    .line 212
    .local v17, "noMatchMsg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public getPlayIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p1, "typeStr"    # Ljava/lang/String;

    .prologue
    .line 217
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.intent.action.PLAY_VIA"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 218
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "extra_type"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    iget-object v2, p0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 222
    const v4, 0x7f0a0295

    .line 221
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 222
    if-eqz v2, :cond_0

    .line 223
    const-string/jumbo v1, "Quick list"

    .line 228
    .local v1, "playlistNameLocalized":Ljava/lang/String;
    :goto_0
    const-string/jumbo v2, "extra_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    return-object v0

    .line 225
    .end local v1    # "playlistNameLocalized":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->name:Ljava/lang/String;

    .restart local v1    # "playlistNameLocalized":Ljava/lang/String;
    goto :goto_0
.end method

.method public name(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->name:Ljava/lang/String;

    .line 29
    return-object p0
.end method

.method public playMusicCharacteristic(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;
    .locals 0
    .param p1, "moodType"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;
    .locals 0
    .param p1, "playMusicType"    # Lcom/nuance/sample/music/PlayMusicType;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType:Lcom/nuance/sample/music/PlayMusicType;

    .line 35
    return-object p0
.end method
