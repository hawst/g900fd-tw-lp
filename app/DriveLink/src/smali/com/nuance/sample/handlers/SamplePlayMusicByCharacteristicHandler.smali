.class public Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SamplePlayMusicByCharacteristicHandler.java"


# static fields
.field private static final MOOD_TYPE_STRING_IDS_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/samsung/music/IntentsAndExtras$MoodType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v1, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;

    .line 27
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 26
    sput-object v1, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/util/HashMap;

    .line 32
    const/4 v1, 0x4

    .line 31
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 33
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Lcom/samsung/music/IntentsAndExtras$MoodType;Ljava/lang/Integer;>;"
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->CALM:Lcom/samsung/music/IntentsAndExtras$MoodType;

    const v2, 0x7f0a07ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->EXCITING:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 35
    const v2, 0x7f0a07ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 34
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->JOYFUL:Lcom/samsung/music/IntentsAndExtras$MoodType;

    const v2, 0x7f0a07ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->PASSIONATE:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 38
    const v2, 0x7f0a07ef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 37
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sput-object v0, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->MOOD_TYPE_STRING_IDS_MAP:Ljava/util/Map;

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getMoodTypeFromAction(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/samsung/music/IntentsAndExtras$MoodType;
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 93
    .line 94
    const-string/jumbo v1, "Characteristic"

    const/4 v2, 0x0

    .line 93
    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "characteristic":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getMoodTypeByName(Ljava/lang/String;)Lcom/samsung/music/IntentsAndExtras$MoodType;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x0

    .line 45
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 46
    const-string/jumbo v6, "Query"

    invoke-static {p1, v6, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 47
    .local v4, "query":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->getMoodTypeFromAction(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/samsung/music/IntentsAndExtras$MoodType;

    move-result-object v1

    .line 48
    .local v1, "moodType":Lcom/samsung/music/IntentsAndExtras$MoodType;
    sget-object v6, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->MOOD_TYPE_STRING_IDS_MAP:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 49
    .local v3, "playingstringId":Ljava/lang/Integer;
    if-nez v3, :cond_0

    .line 50
    sget-object v5, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Invalid mood type.  Mood type = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    new-instance v5, Ljava/lang/IllegalArgumentException;

    .line 52
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Invalid mood type.  Mood type = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 51
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 55
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    .line 56
    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "playingString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 59
    sget-object v6, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "action = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " moodType = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 60
    const-string/jumbo v8, " query = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 59
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    sget-object v6, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    if-eq v1, v6, :cond_2

    .line 62
    new-instance v6, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;

    invoke-direct {v6, p0, v1}, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;-><init>(Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;Lcom/samsung/music/IntentsAndExtras$MoodType;)V

    invoke-static {v1, v6, p2}, Lcom/nuance/sample/music/MusicLauncher;->launchMusicSquare(Lcom/samsung/music/IntentsAndExtras$MoodType;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 89
    :cond_1
    :goto_0
    return v5

    .line 85
    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 86
    const-class v5, Lcom/nuance/sample/controllers/SampleMusicController;

    invoke-virtual {p0, v5}, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/controllers/SampleMusicController;

    .line 87
    .local v0, "controller":Lcom/nuance/sample/controllers/SampleMusicController;
    invoke-virtual {v0, p1, p2}, Lcom/nuance/sample/controllers/SampleMusicController;->executePlayAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v5

    goto :goto_0
.end method
