.class public Lcom/nuance/sample/handlers/clientdm/SampleDialPageHandler;
.super Lcom/nuance/sample/handlers/clientdm/SamplePageHandler;
.source "SampleDialPageHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/nuance/sample/handlers/clientdm/SamplePageHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const v11, 0x7f0a01c7

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 31
    const-string/jumbo v8, "Action"

    invoke-static {p1, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, "dialAction":Ljava/lang/String;
    const-string/jumbo v8, "To"

    invoke-static {p1, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 36
    .local v6, "name":Ljava/lang/String;
    const-string/jumbo v8, "redial"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 37
    move-object v1, v6

    .line 40
    :cond_0
    const-string/jumbo v8, "redial"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 41
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v8

    .line 42
    invoke-interface {v8}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    .local v0, "applicationContext":Landroid/content/Context;
    invoke-static {v0, v10, v9}, Lcom/nuance/sample/util/calllog/CallLogUtil;->getLastNCalls(Landroid/content/Context;IZ)Ljava/util/Vector;

    move-result-object v4

    .line 45
    .local v4, "lastNCalls":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    invoke-virtual {v4}, Ljava/util/Vector;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 46
    const-string/jumbo v8, "DLPhraseSpotter"

    .line 47
    const-string/jumbo v10, "[stop] : SampleDialPageHandler - executeAction()"

    .line 46
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v8

    .line 58
    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 56
    invoke-interface {v8, v10}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    move v8, v9

    .line 117
    .end local v0    # "applicationContext":Landroid/content/Context;
    .end local v4    # "lastNCalls":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    :goto_0
    return v8

    .line 64
    .restart local v0    # "applicationContext":Landroid/content/Context;
    .restart local v4    # "lastNCalls":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v8

    if-lt v3, v8, :cond_3

    .line 80
    :cond_2
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v8

    if-ne v3, v8, :cond_5

    .line 89
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v8

    .line 91
    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 90
    invoke-interface {v8, v10}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    move v8, v9

    .line 93
    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/nuance/sample/util/calllog/LoggedCall;

    invoke-virtual {v8}, Lcom/nuance/sample/util/calllog/LoggedCall;->getContactNumber()Ljava/lang/String;

    move-result-object v7

    .line 73
    .local v7, "phoneNumber":Ljava/lang/String;
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_4

    .line 74
    const-string/jumbo v8, "-1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 75
    const-string/jumbo v8, "-2"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 64
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 95
    .end local v7    # "phoneNumber":Ljava/lang/String;
    :cond_5
    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/nuance/sample/util/calllog/LoggedCall;

    .line 97
    .local v5, "loggedCall":Lcom/nuance/sample/util/calllog/LoggedCall;
    const-string/jumbo v8, "[CarMode] [SampleDialPageHandler]"

    .line 98
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "got last logged call name : "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 99
    invoke-virtual {v5}, Lcom/nuance/sample/util/calllog/LoggedCall;->getContactName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 98
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 97
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string/jumbo v8, "[CarMode] [SampleDialPageHandler]"

    .line 101
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "got last logged call num : "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 102
    invoke-virtual {v5}, Lcom/nuance/sample/util/calllog/LoggedCall;->getContactNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 101
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 100
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    new-instance v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v2}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 108
    .local v2, "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {v5}, Lcom/nuance/drivelink/utils/DLContactVacUtils;->getDlCallLog(Lcom/nuance/sample/util/calllog/LoggedCall;)Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    move-result-object v8

    .line 107
    iput-object v8, v2, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mUserCallLog:Lcom/sec/android/automotive/drivelink/framework/iface/data/DLCallLog;

    .line 110
    const-string/jumbo v8, "DM_DIAL"

    invoke-static {v8, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    move v8, v10

    .line 113
    goto/16 :goto_0

    .line 117
    .end local v0    # "applicationContext":Landroid/content/Context;
    .end local v2    # "flowParams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    .end local v3    # "i":I
    .end local v4    # "lastNCalls":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    .end local v5    # "loggedCall":Lcom/nuance/sample/util/calllog/LoggedCall;
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/clientdm/SamplePageHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v8

    goto/16 :goto_0
.end method

.method protected getControllerClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nuance/sample/controllers/SampleBaseDMController;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    const-class v0, Lcom/nuance/sample/controllers/SampleDialPageController;

    return-object v0
.end method
