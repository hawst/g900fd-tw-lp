.class Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;
.super Ljava/lang/Object;
.source "SamplePlayOutNewsMultiAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayOutNewsAudioPlaybackListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;-><init>(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "request cancelled, reason="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "msg":Ljava/lang/String;
    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$0()Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$5(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$5(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->cancel(Z)Z

    .line 170
    :cond_0
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 171
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$7(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 172
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$6(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    .line 173
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 136
    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$0()Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v1

    const-string/jumbo v2, "onRequestDidPlay"

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$1(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    .line 138
    .local v0, "newRequest":Lcom/vlingo/core/internal/audio/TTSRequest;
    if-eqz v0, :cond_0

    .line 139
    invoke-static {v0, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 146
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 143
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$7(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 144
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$6(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "request ignored, reason="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "msg":Ljava/lang/String;
    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$0()Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 153
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$5(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$5(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->cancel(Z)Z

    .line 156
    :cond_0
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 157
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$7(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$6(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    .line 159
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v3, 0x0

    .line 102
    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$0()Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    const-string/jumbo v1, "onRequestWillPlay"

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 103
    new-instance v0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener$1;

    invoke-direct {v0, p0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener$1;-><init>(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;)V

    .line 121
    const-wide/16 v1, 0x5dc

    .line 103
    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 123
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->isPlayingFirst:Z
    invoke-static {v0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$2(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    invoke-static {v0, v3}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$3(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;Z)V

    .line 125
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    new-instance v1, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    iget-object v2, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    invoke-direct {v1, v2}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;-><init>(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)V

    invoke-static {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$4(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;)V

    .line 126
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$5(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 127
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->access$6(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v0

    .line 128
    const/4 v1, 0x3

    .line 129
    const/4 v2, 0x2

    .line 127
    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->requestAudioFocus(II)V

    .line 131
    :cond_0
    return-void
.end method
