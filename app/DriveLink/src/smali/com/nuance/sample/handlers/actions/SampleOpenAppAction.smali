.class public Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;
.super Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;
.source "SampleOpenAppAction.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;-><init>()V

    return-void
.end method


# virtual methods
.method public appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;
    .locals 0
    .param p1, "appInfo"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 27
    return-object p0
.end method

.method public bridge synthetic appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    move-result-object v0

    return-object v0
.end method

.method protected execute()V
    .locals 3

    .prologue
    .line 32
    sget-object v1, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "execute()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    if-eqz v1, :cond_0

    .line 40
    :try_start_0
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 41
    iget-object v2, p0, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v2

    .line 40
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 42
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "Activity could not be found."

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method
