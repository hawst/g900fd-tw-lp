.class public Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SamplePlayOutNewsMultiAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;,
        Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    }
.end annotation


# static fields
.field private static log:Lcom/vlingo/core/facade/logging/ILogger;


# instance fields
.field private isPlayingFirst:Z

.field private playbackListener:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

.field private prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

.field private requests:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/audio/TTSRequest;",
            ">;"
        }
    .end annotation
.end field

.field private ttsStrings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    .line 27
    sput-object v0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->isPlayingFirst:Z

    .line 25
    return-void
.end method

.method static synthetic access$0()Lcom/vlingo/core/facade/logging/ILogger;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    return-object v0
.end method

.method static synthetic access$1(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->isPlayingFirst:Z

    return v0
.end method

.method static synthetic access$3(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;Z)V
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->isPlayingFirst:Z

    return-void
.end method

.method static synthetic access$4(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    return-void
.end method

.method static synthetic access$5(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    return-object v0
.end method

.method static synthetic access$6(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method private playTts()V
    .locals 7

    .prologue
    .line 48
    sget-object v4, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "doOneTts"

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 49
    iget-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 51
    :cond_0
    sget-object v4, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "ttsStrings is empty or null.  Aborting news readout."

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v4

    const-string/jumbo v5, "No news found."

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 71
    :cond_1
    return-void

    .line 56
    :cond_2
    iget-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 57
    .local v3, "tts":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/audio/TTSRequest;->getResult(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v2

    .line 59
    .local v2, "request":Lcom/vlingo/core/internal/audio/TTSRequest;
    new-instance v4, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;-><init>(Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;)V

    iput-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->playbackListener:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

    .line 61
    sget-object v4, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "calling AudioPlayerProxy.play for \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 62
    iget-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->playbackListener:Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 64
    const/4 v1, 0x0

    .line 65
    .local v1, "listSize":I
    iget-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 66
    iget-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    .line 68
    :cond_3
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 69
    iget-object v5, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/audio/TTSRequest;->getResult(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected execute()V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v1, "PlayOutNewsAction.execute"

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->playTts()V

    .line 44
    return-void
.end method

.method public tts(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "ttses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    .line 37
    return-void
.end method
