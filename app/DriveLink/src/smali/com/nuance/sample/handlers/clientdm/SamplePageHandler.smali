.class public abstract Lcom/nuance/sample/handlers/clientdm/SamplePageHandler;
.super Lcom/nuance/sample/handlers/clientdm/SampleHandler;
.source "SamplePageHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/nuance/sample/handlers/clientdm/SampleHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/clientdm/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 26
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SamplePageHandler;->getControllerClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/handlers/clientdm/SamplePageHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/controllers/SampleBaseDMController;

    invoke-virtual {v0, p1, p2}, Lcom/nuance/sample/controllers/SampleBaseDMController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method protected abstract getControllerClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nuance/sample/controllers/SampleBaseDMController;",
            ">;"
        }
    .end annotation
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 20
    return-void
.end method
