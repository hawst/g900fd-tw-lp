.class Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;
.super Ljava/lang/Object;
.source "SampleSendMessageAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->sendSMS(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;


# direct methods
.method constructor <init>(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;->this$0:Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSMSSent(ZI)V
    .locals 3
    .param p1, "error"    # Z
    .param p2, "errCode"    # I

    .prologue
    .line 62
    # getter for: Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->log:Lcom/vlingo/core/facade/logging/ILogger;
    invoke-static {}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->access$3()Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onSMSSent "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;->this$0:Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    # getter for: Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->message:Ljava/lang/String;
    invoke-static {v2}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->access$4(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 63
    if-nez p1, :cond_1

    .line 64
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;->this$0:Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v0}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->access$5(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 68
    :goto_1
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;->this$0:Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v0}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->access$5(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction$1;->this$0:Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    # invokes: Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->errString(I)Ljava/lang/String;
    invoke-static {v1, p2}, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;->access$6(Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1
.end method
