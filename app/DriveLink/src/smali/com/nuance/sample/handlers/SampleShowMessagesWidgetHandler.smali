.class public Lcom/nuance/sample/handlers/SampleShowMessagesWidgetHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleShowMessagesWidgetHandler.java"


# instance fields
.field private log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    .line 29
    const-class v0, Lcom/nuance/sample/handlers/SampleShowMessagesWidgetHandler;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/handlers/SampleShowMessagesWidgetHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 26
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 11
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 37
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 39
    iget-object v8, p0, Lcom/nuance/sample/handlers/SampleShowMessagesWidgetHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "ShowUnreadMessagesWidgetHandler.executeAction("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 40
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 39
    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 42
    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v3

    .line 41
    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 43
    .local v3, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    const-class v8, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    invoke-virtual {p0, v8}, Lcom/nuance/sample/handlers/SampleShowMessagesWidgetHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v3

    .line 44
    const/16 v4, 0x1e

    .line 45
    .local v4, "maxToReturn":I
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v8

    .line 46
    invoke-virtual {v8, v4}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAllNewAlerts(I)Ljava/util/LinkedList;

    move-result-object v1

    .line 48
    .local v1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->createSMSMMSSenderQueueMap(Ljava/util/LinkedList;)Ljava/util/HashMap;

    move-result-object v6

    .line 49
    .local v6, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const-string/jumbo v7, ""

    .line 51
    .local v7, "systemTurn":Ljava/lang/String;
    const-string/jumbo v8, "Contact"

    .line 52
    const/4 v9, 0x0

    .line 51
    invoke-static {p1, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "contactName":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 54
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v6, v2, v8}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageQueueByContactName(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v6

    .line 55
    new-instance v1, Ljava/util/LinkedList;

    .end local v1    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 56
    .restart local v1    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 61
    :cond_0
    if-eqz v6, :cond_1

    .line 62
    iget-object v8, p0, Lcom/nuance/sample/handlers/SampleShowMessagesWidgetHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "alerts size="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 63
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "<msg size is = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 67
    :cond_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    move-object v8, v3

    .line 77
    check-cast v8, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    invoke-virtual {v8, v2}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setCurrentContactSearch(Ljava/lang/String;)V

    move-object v8, v3

    .line 78
    check-cast v8, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    invoke-virtual {v8, v6}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setSenderQueue(Ljava/util/HashMap;)V

    .line 79
    invoke-virtual {v3, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v8

    return v8

    .line 56
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 57
    .local v5, "readOutType":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 67
    .end local v5    # "readOutType":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 68
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz v0, :cond_2

    .line 69
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v10, "\naddress = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 70
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 71
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v10, ", msg body ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 72
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method
