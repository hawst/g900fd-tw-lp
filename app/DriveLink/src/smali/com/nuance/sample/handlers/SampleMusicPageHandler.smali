.class public Lcom/nuance/sample/handlers/SampleMusicPageHandler;
.super Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;
.source "SampleMusicPageHandler.java"


# static fields
.field private static final ARTIST_PARAM:Ljava/lang/String; = "Artist"

.field private static final QUERY_PARAM:Ljava/lang/String; = "Query"

.field private static final TAG:Ljava/lang/String; = "[SampleMusicPageHandler]"


# instance fields
.field private prompt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/handlers/SampleMusicPageHandler;->prompt:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 11
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const-wide/16 v9, 0x258

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 40
    .line 41
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v4, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 42
    const/4 v1, 0x0

    .line 44
    .local v1, "query":Ljava/lang/String;
    const-string/jumbo v4, "VAC_DRIVELINK"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 45
    invoke-virtual {p0, p1}, Lcom/nuance/sample/handlers/SampleMusicPageHandler;->getQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/lang/String;

    move-result-object v1

    .line 50
    :goto_0
    if-eqz v1, :cond_0

    .line 51
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "DM_MAIN"

    if-ne v4, v5, :cond_1

    .line 52
    :cond_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    .line 53
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/DLApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 52
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/Logging;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelink/common/Logging;

    .line 54
    const-string/jumbo v4, "CM04"

    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/Logging;->insertLog(Ljava/lang/String;)V

    .line 56
    :cond_1
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 58
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "DM_MAIN"

    if-ne v4, v5, :cond_7

    .line 61
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    if-nez v4, :cond_4

    .line 125
    :cond_2
    :goto_1
    return v3

    .line 47
    :cond_3
    const-string/jumbo v4, "Query"

    invoke-static {p1, v4, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 65
    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    .line 66
    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCheckListSettingMusicList()Z

    move-result v2

    .line 67
    .local v2, "value":Z
    const-string/jumbo v4, "[SampleMusicPageHandler]"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "setCheckMusicList value + "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    if-eqz v2, :cond_2

    .line 71
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getHomeMode()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "HOME_MODE_MULTIWINDOW"

    if-ne v4, v5, :cond_5

    .line 72
    new-instance v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;

    invoke-direct {v0}, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;-><init>()V

    .line 73
    .local v0, "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v4

    .line 74
    invoke-interface {v4}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 75
    const v5, 0x7f0a004f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 73
    iput-object v4, p0, Lcom/nuance/sample/handlers/SampleMusicPageHandler;->prompt:Ljava/lang/String;

    .line 76
    iget-object v4, p0, Lcom/nuance/sample/handlers/SampleMusicPageHandler;->prompt:Ljava/lang/String;

    iput-object v4, v0, Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;->mPromptString:Ljava/lang/String;

    .line 78
    const-string/jumbo v4, "DM_MUSIC_PLAYER"

    .line 77
    invoke-static {v4, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 79
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setCallMultiwindow(Z)V

    goto :goto_1

    .line 82
    .end local v0    # "flowPrams":Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;
    :cond_5
    const-string/jumbo v4, "DM_MUSIC_PLAYER"

    .line 81
    invoke-static {v4, v8}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIntegratedFlow(Ljava/lang/String;Lcom/sec/android/automotive/drivelink/common/flow/FlowParams;)V

    .line 83
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 85
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 86
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->ismUserPausePriority()Z

    move-result v4

    if-nez v4, :cond_6

    .line 87
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 88
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4, v7, v7, v7}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 89
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 91
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/nuance/sample/handlers/SampleMusicPageHandler$1;

    invoke-direct {v5, p0}, Lcom/nuance/sample/handlers/SampleMusicPageHandler$1;-><init>(Lcom/nuance/sample/handlers/SampleMusicPageHandler;)V

    invoke-virtual {v4, v5, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 104
    :cond_6
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4, v7, v7, v7}, Lcom/sec/android/automotive/drivelink/music/MusicService;->pausePlayer(ZZZ)V

    .line 105
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/automotive/drivelink/music/MusicService;->setMusicPlayerVolume()V

    .line 106
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 107
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/nuance/sample/handlers/SampleMusicPageHandler$2;

    invoke-direct {v5, p0}, Lcom/nuance/sample/handlers/SampleMusicPageHandler$2;-><init>(Lcom/nuance/sample/handlers/SampleMusicPageHandler;)V

    invoke-virtual {v4, v5, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 119
    .end local v2    # "value":Z
    :cond_7
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "DM_MUSIC_PLAYER"

    if-ne v4, v5, :cond_2

    .line 120
    const-string/jumbo v3, "Music:Play"

    iput-object v3, p0, Lcom/nuance/sample/handlers/SampleMusicPageHandler;->type:Ljava/lang/String;

    .line 121
    invoke-super {p0, p1, p2}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v3

    goto/16 :goto_1

    .line 125
    :cond_8
    invoke-super {p0, p1, p2}, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v3

    goto/16 :goto_1
.end method
