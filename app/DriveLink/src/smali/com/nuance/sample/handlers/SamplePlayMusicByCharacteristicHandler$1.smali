.class Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;
.super Ljava/lang/Object;
.source "SamplePlayMusicByCharacteristicHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;

.field private final synthetic val$moodType:Lcom/samsung/music/IntentsAndExtras$MoodType;


# direct methods
.method constructor <init>(Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;Lcom/samsung/music/IntentsAndExtras$MoodType;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;->this$0:Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;

    iput-object p2, p0, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;->val$moodType:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionAbort()V
    .locals 5

    .prologue
    .line 80
    # getter for: Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 81
    const-string/jumbo v1, "MusicLauncher launch %s moodtype actionAbort()"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 82
    iget-object v4, p0, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;->val$moodType:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v4}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 80
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    return-void
.end method

.method public actionFail(Ljava/lang/String;)V
    .locals 5
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 72
    # getter for: Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 74
    const-string/jumbo v1, "MusicLauncher launch %s moodtype actionFail(\'%s\')"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 75
    iget-object v4, p0, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;->val$moodType:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v4}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 73
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return-void
.end method

.method public actionSuccess()V
    .locals 5

    .prologue
    .line 65
    # getter for: Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;->access$0()Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string/jumbo v1, "MusicLauncher launch %s moodtype actionSuccess()"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 67
    iget-object v4, p0, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler$1;->val$moodType:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v4}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 65
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    return-void
.end method
