.class public Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleShowCallWidgetHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private allMatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field protected forwardRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

.field log:Lcom/vlingo/core/facade/logging/ILogger;

.field protected messages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;

    .line 42
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 41
    sput-object v0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->TAG:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    .line 45
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 35
    return-void
.end method

.method private placeCall(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;Z)V
    .locals 7
    .param p1, "recipient"    # Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    .param p2, "videoCall"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 148
    if-nez p1, :cond_0

    .line 167
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->turnOnSpeakerIfRequired()V

    .line 153
    move-object v0, p0

    .line 154
    .local v0, "dma":Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "nameOrAddress":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 158
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v3

    .line 159
    invoke-interface {v3}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 160
    const v4, 0x7f0a00aa

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 161
    .local v2, "prompt":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->finishDialog()V

    .line 164
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    .line 165
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    .line 164
    invoke-static {v3, v4, v0, v5, p2}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V

    goto :goto_0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 182
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 183
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 184
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 185
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 175
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 176
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 177
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 178
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 20
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 54
    invoke-super/range {p0 .. p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v17

    .line 57
    sget-object v18, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 56
    invoke-interface/range {v17 .. v18}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/List;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->allMatches:Ljava/util/List;

    .line 59
    const-string/jumbo v17, "recipient"

    .line 60
    const/16 v18, 0x0

    .line 59
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v15

    .line 62
    .local v15, "recipients":Ljava/lang/String;
    const/4 v14, 0x0

    .line 63
    .local v14, "recipientContactData":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v11, 0x0

    .line 65
    .local v11, "rawPhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 66
    .local v12, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-static {v15}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 67
    const-string/jumbo v17, "{}"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 68
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->allMatches:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->allMatches:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_7

    .line 71
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->allMatches:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v15}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRecipientContacts(Ljava/util/List;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v14

    .line 73
    invoke-static {v15}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 90
    :cond_0
    :goto_0
    if-eqz v14, :cond_2

    .line 91
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_1
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_8

    .line 102
    :cond_2
    if-eqz v11, :cond_3

    .line 103
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_9

    .line 107
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->forwardRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 108
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->forwardRecipient:Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 111
    :cond_4
    const-string/jumbo v17, "mode"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 112
    .local v9, "mode":Ljava/lang/String;
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 113
    const-string/jumbo v17, "videophone"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 112
    const/16 v16, 0x1

    .line 115
    .local v16, "videoCall":Z
    :goto_3
    const-string/jumbo v17, "confirm"

    const/16 v18, 0x1

    .line 116
    const/16 v19, 0x0

    .line 115
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v6

    .line 117
    .local v6, "isConfirm":Z
    const-string/jumbo v17, "execute"

    const/16 v18, 0x1

    .line 118
    const/16 v19, 0x0

    .line 117
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v7

    .line 119
    .local v7, "isExecute":Z
    if-nez v6, :cond_b

    if-eqz v7, :cond_b

    const/4 v8, 0x1

    .line 121
    .local v8, "isExecuteStep":Z
    :goto_4
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 122
    sget-object v17, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCallWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/16 v18, 0x0

    .line 123
    const/16 v19, 0x0

    .line 122
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-interface {v0, v1, v2, v12, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 126
    :cond_5
    const-string/jumbo v17, "execute"

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 129
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    move-object/from16 v17, v0

    const-string/jumbo v18, "[LatencyCheck] placeCall in ShowCallWidgetHandler"

    invoke-interface/range {v17 .. v18}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 130
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v12, v1}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->placeCall(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;Z)V

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 138
    :cond_6
    :goto_5
    const/16 v17, 0x0

    .end local v6    # "isConfirm":Z
    .end local v7    # "isExecute":Z
    .end local v8    # "isExecuteStep":Z
    .end local v9    # "mode":Ljava/lang/String;
    .end local v16    # "videoCall":Z
    :goto_6
    return v17

    .line 74
    :catch_0
    move-exception v5

    .line 75
    .local v5, "e":Lorg/json/JSONException;
    sget-object v17, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->TAG:Ljava/lang/String;

    .line 76
    const-string/jumbo v18, "Error extracting json contacts and numbers from recipients parameter"

    .line 75
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const/16 v17, 0x0

    goto :goto_6

    .line 82
    .end local v5    # "e":Lorg/json/JSONException;
    :cond_7
    :try_start_2
    invoke-static {v15}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getMessageRawPhoneNumbers(Ljava/lang/String;)Ljava/util/Set;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v11

    .line 81
    goto/16 :goto_0

    .line 83
    :catch_1
    move-exception v5

    .line 84
    .restart local v5    # "e":Lorg/json/JSONException;
    sget-object v17, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->TAG:Ljava/lang/String;

    .line 85
    const-string/jumbo v18, "Error extracting json contacts and numbers from recipients parameter"

    .line 84
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/16 v17, 0x0

    goto :goto_6

    .line 91
    .end local v5    # "e":Lorg/json/JSONException;
    :cond_8
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 92
    .local v13, "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v13, :cond_1

    .line 93
    iget-object v4, v13, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 94
    .local v4, "address":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 95
    new-instance v12, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 96
    .end local v12    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    iget-object v0, v13, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 97
    iget-object v0, v13, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 95
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v12, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v12    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    goto/16 :goto_1

    .line 103
    .end local v4    # "address":Ljava/lang/String;
    .end local v13    # "recipientContact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_9
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 104
    .local v10, "number":Ljava/lang/String;
    new-instance v12, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .end local v12    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    const-string/jumbo v18, ""

    move-object/from16 v0, v18

    invoke-direct {v12, v0, v10}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v12    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    goto/16 :goto_2

    .line 112
    .end local v10    # "number":Ljava/lang/String;
    .restart local v9    # "mode":Ljava/lang/String;
    :cond_a
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 119
    .restart local v6    # "isConfirm":Z
    .restart local v7    # "isExecute":Z
    .restart local v16    # "videoCall":Z
    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 134
    .restart local v8    # "isExecuteStep":Z
    :catch_2
    move-exception v5

    .line 135
    .restart local v5    # "e":Lorg/json/JSONException;
    sget-object v17, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;->TAG:Ljava/lang/String;

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method protected turnOnSpeakerIfRequired()V
    .locals 1

    .prologue
    .line 142
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 143
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 142
    invoke-static {v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V

    .line 144
    return-void
.end method
