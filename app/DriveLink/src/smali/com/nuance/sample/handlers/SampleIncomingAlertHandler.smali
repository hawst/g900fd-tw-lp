.class public Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;
.super Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;
.source "SampleIncomingAlertHandler.java"


# instance fields
.field alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

.field alerts:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private isSilentMode:Z

.field private log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isSilentMode"    # Z

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;-><init>()V

    .line 23
    const-class v0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 31
    iput-boolean p1, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->isSilentMode:Z

    .line 32
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 41
    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "SafeReaderHandler.executeAction: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 42
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 44
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    .line 43
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 45
    .local v1, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 46
    .local v2, "messageAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    iget-object v3, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->alerts:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 52
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 53
    const-class v3, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    invoke-virtual {p0, v3}, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v1

    move-object v3, v1

    .line 54
    check-cast v3, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    .line 55
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->isSilentMode()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setSilentMode(Z)V

    move-object v3, v1

    .line 56
    check-cast v3, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    .line 58
    invoke-static {v2}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->createSMSMMSSenderQueueMap(Ljava/util/LinkedList;)Ljava/util/HashMap;

    move-result-object v4

    .line 57
    invoke-virtual {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setSenderQueue(Ljava/util/HashMap;)V

    move-object v3, v1

    .line 59
    check-cast v3, Lcom/nuance/sample/safereader/SampleAlertReadoutController;

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isReadMessageBodyEnabled()Z

    move-result v4

    .line 60
    invoke-virtual {v3, v4}, Lcom/nuance/sample/safereader/SampleAlertReadoutController;->setShowMessageBody(Z)V

    .line 62
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v3, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 64
    invoke-virtual {v1, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 67
    :cond_1
    const/4 v3, 0x0

    return v3

    .line 46
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 47
    .local v0, "alertItem":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 48
    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .end local v0    # "alertItem":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/Intent;
    .param p2, "arg1"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 91
    return-void
.end method

.method public init(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V
    .locals 0
    .param p1, "safeReaderAlert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 76
    return-void
.end method

.method public init(Ljava/util/Queue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iput-object p1, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->alerts:Ljava/util/Queue;

    .line 72
    return-void
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->isSilentMode:Z

    return v0
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->isSilentMode:Z

    .line 84
    return-void
.end method
