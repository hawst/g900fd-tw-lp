.class public Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SamplePlaySongListAction.java"


# instance fields
.field private songList:[J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 27
    iget-object v2, p0, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->songList:[J

    if-eqz v2, :cond_0

    .line 30
    :try_start_0
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->getPlayIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 33
    iget-object v3, p0, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->songList:[J

    invoke-virtual {p0, v3}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->getLaunchIntent([J)Landroid/content/Intent;

    move-result-object v3

    .line 32
    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 34
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 40
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->resourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    .line 42
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 41
    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "noMatchMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLaunchIntent([J)Landroid/content/Intent;
    .locals 3
    .param p1, "songList"    # [J

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.android.music/launchplayer"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string/jumbo v1, "musicplayer.action"

    const-string/jumbo v2, "launchplayer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string/jumbo v1, "list"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 52
    const-string/jumbo v1, "list_position"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 53
    return-object v0
.end method

.method public getPlayIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Landroid/content/Intent;

    .line 58
    const-string/jumbo v1, "com.sec.android.app.music.musicservicecommand.play"

    .line 57
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "playIntent":Landroid/content/Intent;
    return-object v0
.end method

.method public songList([J)Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;
    .locals 0
    .param p1, "songList"    # [J

    .prologue
    .line 21
    iput-object p1, p0, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->songList:[J

    .line 22
    return-object p0
.end method
