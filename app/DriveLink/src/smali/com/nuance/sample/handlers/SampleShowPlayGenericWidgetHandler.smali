.class public Lcom/nuance/sample/handlers/SampleShowPlayGenericWidgetHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleShowPlayGenericWidgetHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 2
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 19
    .line 20
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    .line 19
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 24
    .local v0, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    instance-of v1, v0, Lcom/nuance/sample/controllers/SampleMusicController;

    if-nez v1, :cond_0

    .line 25
    const-class v1, Lcom/nuance/sample/controllers/SampleMusicController;

    invoke-virtual {p0, v1}, Lcom/nuance/sample/handlers/SampleShowPlayGenericWidgetHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 27
    :cond_0
    check-cast v0, Lcom/nuance/sample/controllers/SampleMusicController;

    .end local v0    # "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    invoke-virtual {v0, p1, p2}, Lcom/nuance/sample/controllers/SampleMusicController;->executePlayAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    return v1
.end method
