.class public Lcom/nuance/sample/handlers/clientdm/SampleMessageMemoBodyCheatHandler;
.super Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;
.source "SampleMessageMemoBodyCheatHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 18
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 19
    const-string/jumbo v2, "memo"

    invoke-interface {p1, v2}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 20
    .local v0, "memo":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleMessageMemoBodyCheatHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 21
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 22
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 23
    const v3, 0x7f0a0024

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 24
    .local v1, "prompt":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 25
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    .line 26
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 27
    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 26
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    .line 25
    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 28
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleMessageMemoBodyCheatHandler;->startListening()V

    .line 29
    const/4 v2, 0x0

    return v2
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 35
    return-void
.end method
