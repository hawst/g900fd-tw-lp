.class public Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleShowContactTypeChoicesHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field choiceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field mappedContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field matchContactData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;

    .line 31
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 30
    sput-object v0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->TAG:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    return-void
.end method

.method private getContactNames(Ljava/util/Set;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 119
    .local p1, "types":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 121
    .local v1, "counter":I
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 128
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 121
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 122
    .local v2, "type":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    add-int/lit8 v1, v1, 0x1

    .line 124
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v4

    if-eq v1, v4, :cond_0

    .line 125
    const-string/jumbo v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private matchData(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "contactData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v3, "matchedContacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :try_start_0
    iget-object v4, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->choiceIds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 115
    :goto_1
    return-object v3

    .line 98
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    .local v0, "choice":Ljava/lang/String;
    const/16 v4, 0x2e

    invoke-static {v0, v4}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "choices":[Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 100
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 103
    .end local v0    # "choice":Ljava/lang/String;
    .end local v1    # "choices":[Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 104
    .local v2, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->TAG:Ljava/lang/String;

    .line 105
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Expected Contact Data contactData to be of form <contact_id>.<address_id>: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 105
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 104
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    new-instance v4, Ljava/security/InvalidParameterException;

    .line 108
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Expected Contact Data contactData to be of form <contact_id>.<address_id>: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 108
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 107
    invoke-direct {v4, v5}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 110
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v4, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->TAG:Ljava/lang/String;

    .line 112
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Expected Contact Data contactData to be of form <contact_id>.<address_id>: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 112
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 111
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x0

    .line 41
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 43
    const-string/jumbo v5, "choices"

    const/4 v6, 0x1

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    .line 46
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v5

    const-string/jumbo v6, "contact"

    invoke-interface {v5, v6}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 49
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getChoicesAsStrings(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->choiceIds:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    iget-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->choiceIds:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->choiceIds:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 57
    :cond_0
    const-string/jumbo v5, "SampleShowContactTypeChoicesHandler.executeAction"

    .line 58
    const-string/jumbo v6, "no matches"

    .line 57
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :goto_0
    return v8

    .line 50
    :catch_0
    move-exception v4

    .line 51
    .local v4, "e":Lorg/json/JSONException;
    const-string/jumbo v5, "DialogDataUtil.ChoiceUtil"

    .line 52
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "displayList: JSONException"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 51
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 62
    .end local v4    # "e":Lorg/json/JSONException;
    :cond_1
    iget-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->choiceIds:Ljava/util/List;

    .line 63
    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 62
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->splitAddressFromContactId(Ljava/lang/String;)[I

    move-result-object v5

    aget v0, v5, v8

    .line 67
    .local v0, "contactId":I
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v3

    .line 66
    check-cast v3, Ljava/util/List;

    .line 70
    .local v3, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {v3, v0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactMatchFromId(Ljava/util/List;I)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v5

    .line 69
    iput-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 71
    new-instance v5, Ljava/util/ArrayList;

    .line 72
    iget-object v6, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->contactMatch:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 71
    invoke-direct {p0, v5}, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->matchData(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->matchContactData:Ljava/util/List;

    .line 74
    const/4 v1, 0x0

    .line 75
    .local v1, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v5, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->matchContactData:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    .line 77
    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->matchContactData:Ljava/util/List;

    .line 78
    const-string/jumbo v7, "call"

    .line 76
    invoke-static {v5, v6, v7}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 79
    const-string/jumbo v5, "SampleShowContactTypeChoicesHandler.executeAction"

    .line 80
    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "matchContactData has "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 79
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :goto_1
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->getContactNames(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "contactNames":Ljava/lang/String;
    const-string/jumbo v5, "SampleShowContactTypeChoicesHandler.executeAction"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "names \'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 87
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v2, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 82
    .end local v2    # "contactNames":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "SampleShowContactTypeChoicesHandler.executeAction"

    .line 83
    const-string/jumbo v6, "matchContactData is empty"

    .line 82
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 134
    return-void
.end method
