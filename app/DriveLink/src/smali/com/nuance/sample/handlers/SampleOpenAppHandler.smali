.class public Lcom/nuance/sample/handlers/SampleOpenAppHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleOpenAppHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static cachedApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;

    .line 37
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 36
    sput-object v0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->TAG:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    return-void
.end method

.method private getAppItemsWithPage(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;
    .locals 8
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    invoke-static {p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v3

    .line 182
    .local v3, "controlData":Lcom/vlingo/core/internal/util/ListControlData;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ListControlData;->getCurrentPage()I

    move-result v5

    .line 183
    .local v5, "page":I
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ListControlData;->getPageSize()I

    move-result v6

    .line 184
    .local v6, "pageSize":I
    invoke-static {p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v1

    .line 185
    .local v1, "appDetailsStoredList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v2, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    add-int/lit8 v7, v5, -0x1

    mul-int v4, v7, v6

    .local v4, "i":I
    :goto_0
    mul-int v7, v5, v6

    if-ge v4, v7, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-lt v4, v7, :cond_1

    .line 190
    :cond_0
    return-object v2

    .line 187
    :cond_1
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 188
    .local v0, "app":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private launchApp(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ordinal"    # Ljava/lang/String;
    .param p3, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f0a00cc

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 76
    sget-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "launchApp() for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", ordinal "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 79
    sget-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    invoke-static {v4, p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 81
    .local v0, "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    if-eqz v0, :cond_0

    .line 82
    iput-object p4, p0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 85
    invoke-interface {p4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    .line 86
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 87
    new-array v5, v8, [Ljava/lang/Object;

    .line 88
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 87
    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 89
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    invoke-virtual {p0, v4, v5}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    .line 91
    invoke-virtual {v4, v0}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->queue()V

    .line 93
    sget-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 94
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 95
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 156
    .end local v0    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    :goto_0
    return-void

    .line 97
    .end local v3    # "text":Ljava/lang/String;
    .restart local v0    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    :cond_0
    const-string/jumbo v3, "Open App domain not yet handling ordinals"

    .line 98
    .restart local v3    # "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    sget-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->TAG:Ljava/lang/String;

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 106
    .end local v0    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    .end local v3    # "text":Ljava/lang/String;
    :cond_1
    new-instance v2, Lcom/vlingo/core/internal/util/OpenAppUtil;

    invoke-direct {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;-><init>()V

    .line 107
    .local v2, "oau":Lcom/vlingo/core/internal/util/OpenAppUtil;
    sget-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    invoke-virtual {v2, p1, v4}, Lcom/vlingo/core/internal/util/OpenAppUtil;->buildMatchingAppList(Ljava/lang/String;Ljava/util/List;)V

    .line 108
    invoke-interface {p4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 109
    .local v1, "context":Landroid/content/Context;
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    .line 110
    new-instance v4, Lcom/vlingo/core/internal/util/ListControlData;

    sget-object v5, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {p4, v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 111
    sget-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    invoke-static {p4, v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 113
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 117
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 118
    const v5, 0x7f0a0035

    .line 117
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 119
    .restart local v3    # "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v10}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    goto :goto_0

    .line 122
    .end local v3    # "text":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v8, :cond_3

    .line 129
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 130
    const v5, 0x7f0a0293

    .line 129
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 131
    .restart local v3    # "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 132
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-direct {p0, p4}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getAppItemsWithPage(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v5

    invoke-interface {p4, v4, v10, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 133
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_LAUNCHAPP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->startRecording(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 142
    .end local v3    # "text":Ljava/lang/String;
    :cond_3
    iput-object p4, p0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 144
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 145
    .restart local v0    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    invoke-interface {p4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 146
    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v7

    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 147
    .restart local v3    # "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showUserText(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    invoke-virtual {p0, v4, v5}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    .line 150
    invoke-virtual {v4, v0}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->queue()V

    .line 152
    sget-object v4, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 153
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 154
    invoke-static {p4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    goto/16 :goto_0
.end method

.method private startRecording(Ljava/lang/String;)V
    .locals 2
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 65
    const-string/jumbo v0, "DLPhraseSpotter"

    .line 66
    const-string/jumbo v1, "[stop] : SampleOpenAppHandler - startRecording()"

    .line 65
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 69
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    .line 68
    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 70
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 71
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 72
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 47
    sget-object v3, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "executeAction() for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 49
    const-string/jumbo v3, "Which"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "ordinal":Ljava/lang/String;
    const-string/jumbo v3, "Name"

    invoke-static {p1, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 52
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->cachedApps:Ljava/util/List;

    .line 53
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v3

    .line 54
    invoke-interface {v3}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 55
    const v4, 0x7f0a0030

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    sget-object v3, Lcom/nuance/sample/CCFieldIds;->VP_CAR_MAIN_LAUNCHAPPNAME:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v3}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->startRecording(Ljava/lang/String;)V

    .line 61
    .end local v2    # "text":Ljava/lang/String;
    :goto_0
    return v6

    .line 59
    :cond_0
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->launchApp(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 168
    sget-object v1, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "handleIntent(Intent) for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v1, p0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    if-eqz v1, :cond_1

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    iget-object v2, p0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 171
    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 170
    invoke-static {v2, v1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;I)V

    .line 178
    :goto_1
    return-void

    .line 172
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 174
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    move-object v0, p2

    .line 175
    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 176
    .local v0, "ai":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    invoke-virtual {p0, v0}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    goto :goto_1
.end method

.method public launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V
    .locals 2
    .param p1, "appInfo"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 159
    sget-object v0, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "launchApp(OpenAppUtil)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/handlers/SampleOpenAppHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    invoke-virtual {v0, p1}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;->queue()V

    .line 163
    return-void
.end method
