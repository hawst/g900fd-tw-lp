.class public Lcom/nuance/sample/handlers/SampleShowServerMessageHandler;
.super Lcom/nuance/sample/handlers/SampleHandler;
.source "SampleShowServerMessageHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/nuance/sample/handlers/SampleShowServerMessageHandler;

    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 10
    sput-object v0, Lcom/nuance/sample/handlers/SampleShowServerMessageHandler;->TAG:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/nuance/sample/handlers/SampleHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v2, 0x0

    .line 19
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/SampleHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 20
    const-string/jumbo v1, "Message"

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    :goto_0
    return v2

    .line 24
    :cond_0
    invoke-interface {p2, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
