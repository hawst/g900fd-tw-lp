.class public Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;
.super Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;
.source "SampleShowContactChoicesHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public choiceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public displayedChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;

    .line 24
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 23
    sput-object v0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    .line 27
    iput-object v0, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;

    .line 22
    return-void
.end method

.method private getContactNames(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 96
    .local v1, "counter":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 103
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 96
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 97
    .local v2, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v4, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    add-int/lit8 v1, v1, 0x1

    .line 99
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-eq v1, v4, :cond_0

    .line 100
    const-string/jumbo v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 32
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/handlers/clientdm/SampleWithWidgetHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 33
    const-string/jumbo v7, "choices"

    invoke-static {p1, v7, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 34
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v7

    .line 35
    invoke-interface {v7}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 36
    .local v1, "context":Landroid/content/Context;
    sget-object v7, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 37
    const-string/jumbo v8, "SampleShowContactChoicesHandler.executeAction called for \'choices\'"

    .line 36
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getChoicesAsInts(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    .line 46
    check-cast v0, Ljava/util/List;

    .line 48
    .local v0, "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;

    if-eqz v7, :cond_0

    if-nez v0, :cond_2

    .line 49
    :cond_0
    sget-object v7, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 50
    const-string/jumbo v8, "SampleShowContactChoicesHandler.executeAction no matches"

    .line 49
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    .end local v0    # "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v1    # "context":Landroid/content/Context;
    :cond_1
    :goto_1
    return v10

    .line 40
    .restart local v1    # "context":Landroid/content/Context;
    :catch_0
    move-exception v2

    .line 41
    .local v2, "e":Lorg/json/JSONException;
    sget-object v7, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 42
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "DialogDataUtil.ChoiceUtil displayList: JSONException"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 43
    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 42
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 41
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v0    # "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_2
    sget-object v7, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "SampleShowContactChoicesHandler dialog state had "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 54
    iget-object v9, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " contact matches"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 53
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iget-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->choiceIds:Ljava/util/List;

    .line 55
    invoke-static {v0, v7}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->extractByPosition(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    .line 57
    iget-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_3

    .line 59
    const v7, 0x7f0a0038

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 61
    .local v6, "prompt":Ljava/lang/String;
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    .line 62
    .local v3, "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 63
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 64
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->startListening()V

    goto :goto_1

    .line 65
    .end local v3    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .end local v6    # "prompt":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-eq v7, v11, :cond_1

    .line 69
    const v7, 0x7f0a0052

    new-array v8, v11, [Ljava/lang/Object;

    .line 70
    iget-object v9, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    .line 68
    invoke-virtual {v1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 71
    .restart local v6    # "prompt":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    iget-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v4, v7, :cond_4

    .line 79
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    .line 80
    .restart local v3    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 82
    invoke-virtual {p0}, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->startListening()V

    goto/16 :goto_1

    .line 72
    .end local v3    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    :cond_4
    iget-object v7, p0, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->displayedChoices:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 73
    .local v5, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 74
    const v8, 0x7f0a0192

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 75
    iget-object v8, v5, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 76
    const v8, 0x7f0a00c7

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 73
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 71
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 87
    .end local v0    # "allObjects":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v1    # "context":Landroid/content/Context;
    .end local v4    # "i":I
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "prompt":Ljava/lang/String;
    :cond_5
    sget-object v7, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;->TAG:Ljava/lang/String;

    .line 88
    const-string/jumbo v8, "SampleShowContactChoicesHandler.executeAction unexpected parameter"

    .line 87
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 109
    return-void
.end method
