.class public Lcom/nuance/sample/handlers/SampleLPActionHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;
.source "SampleLPActionHandler.java"


# instance fields
.field private log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;-><init>()V

    .line 17
    const-class v0, Lcom/nuance/sample/handlers/SampleLPActionHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/handlers/SampleLPActionHandler;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 14
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 22
    iget-object v2, p0, Lcom/nuance/sample/handlers/SampleLPActionHandler;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "SampleLPActionHandler.executeAction: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 23
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    .line 24
    .local v1, "result":Z
    invoke-virtual {p0, p1}, Lcom/nuance/sample/handlers/SampleLPActionHandler;->getController(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    .line 25
    .local v0, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    .line 28
    .end local v1    # "result":Z
    :cond_0
    return v1
.end method

.method public getController(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/dialogmanager/Controller;
    .locals 3
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 32
    const-string/jumbo v1, "Action"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "actionValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 34
    const-string/jumbo v1, "screen:off"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    const-class v1, Lcom/nuance/sample/controllers/SampleScreenController;

    invoke-virtual {p0, v1}, Lcom/nuance/sample/handlers/SampleLPActionHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v1

    .line 38
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
