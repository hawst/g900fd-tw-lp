.class enum Lcom/nuance/sample/music/VolumeAction$1;
.super Lcom/nuance/sample/music/VolumeAction;
.source "VolumeAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/music/VolumeAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4000
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "$anonymous0"    # Ljava/lang/String;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/nuance/sample/music/VolumeAction;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/nuance/sample/music/VolumeAction;)V

    .line 1
    return-void
.end method


# virtual methods
.method public execute()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementCurrentStreamVolume()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    .line 21
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_minimum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 20
    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 23
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
