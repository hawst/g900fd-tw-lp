.class public final Lcom/nuance/sample/music/MusicLauncher;
.super Ljava/lang/Object;
.source "MusicLauncher.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static launchList([JLcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 2
    .param p0, "songList"    # [J
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 36
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 37
    const-class v1, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;

    .line 36
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;

    .line 37
    invoke-virtual {v0, p0}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->songList([J)Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;

    move-result-object v0

    .line 38
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    .line 39
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    .line 40
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    .line 41
    return-void
.end method

.method public static launchMusicPlayer(Lcom/nuance/sample/music/PlayMusicType;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 2
    .param p0, "type"    # Lcom/nuance/sample/music/PlayMusicType;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .param p3, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 25
    const-class v1, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    .line 24
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    .line 26
    invoke-virtual {v0, p0}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->name(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    move-result-object v0

    .line 27
    invoke-interface {p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    .line 29
    invoke-virtual {v0, p3}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    .line 30
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->userLoggingEngine()Lcom/vlingo/core/facade/logging/IUserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "music-play-list"

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/IUserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public static launchMusicSquare(Lcom/samsung/music/IntentsAndExtras$MoodType;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 2
    .param p0, "moodType"    # Lcom/samsung/music/IntentsAndExtras$MoodType;
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 47
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 48
    const-class v1, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    .line 47
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->getInstance(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    .line 49
    invoke-virtual {p0}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicCharacteristic(Ljava/lang/String;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->MOOD:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->playMusicType(Lcom/nuance/sample/music/PlayMusicType;)Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;

    move-result-object v0

    .line 51
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/handlers/actions/SamplePlayMusicViaIntentAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    .line 52
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    .line 54
    return-void
.end method
