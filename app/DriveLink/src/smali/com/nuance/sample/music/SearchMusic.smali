.class public Lcom/nuance/sample/music/SearchMusic;
.super Ljava/lang/Object;
.source "SearchMusic.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$music$PlayMusicType:[I = null

.field private static final ALBUM_ART_URI_PREFIX:Ljava/lang/String; = "content://media/external/audio/albumart/"

.field private static MAX_COUNT:I = 0x0

.field private static final QUICK_LIST_DEFAULT_VALUE:Ljava/lang/String; = "Quick list"

.field private static final SEARCH_MUSIC_MAX:I = 0x1e

.field private static log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$music$PlayMusicType()[I
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->$SWITCH_TABLE$com$nuance$sample$music$PlayMusicType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/music/PlayMusicType;->values()[Lcom/nuance/sample/music/PlayMusicType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->MOOD:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_3
    :try_start_3
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_4
    :try_start_4
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_5
    :try_start_5
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    :try_start_6
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PLAYALL:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_7
    :try_start_7
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_8
    :try_start_8
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_9
    :try_start_9
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->SONGLIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_a
    :try_start_a
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_b
    sput-object v0, Lcom/nuance/sample/music/SearchMusic;->$SWITCH_TABLE$com$nuance$sample$music$PlayMusicType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_9

    :catch_3
    move-exception v1

    goto :goto_8

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/nuance/sample/music/SearchMusic;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 31
    const/4 v0, 0x5

    sput v0, Lcom/nuance/sample/music/SearchMusic;->MAX_COUNT:I

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addConditionWithoutSpaceIfNeed(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;I)V
    .locals 2
    .param p0, "builder"    # Ljava/lang/StringBuilder;
    .param p1, "nameField"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/nuance/sample/music/PlayMusicType;
    .param p4, "currentConditionOffset"    # I

    .prologue
    .line 199
    invoke-static {}, Lcom/nuance/sample/music/SearchMusic;->isKoreanInSettings()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const-string/jumbo v0, "("

    invoke-virtual {p0, p4, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string/jumbo v0, ") OR ("

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    invoke-static {p1}, Lcom/nuance/sample/music/SearchMusic;->conditionWithoutSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p2, v1, p3}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string/jumbo v0, ")"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_0
    return-void
.end method

.method private static augmentQuery(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 950
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 951
    .local v1, "words":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 952
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 961
    :goto_1
    return-object v1

    .line 952
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 953
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 954
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 953
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/StringUtils;->containsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 954
    if-eqz v3, :cond_0

    .line 955
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 959
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0, "words"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 966
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 967
    .local v0, "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 968
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 973
    .end local v0    # "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    return-object v0

    .line 968
    .restart local v0    # "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    aget-object v1, p0, v2

    .line 969
    .local v1, "word":Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/nuance/sample/music/SearchMusic;->augmentQuery(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 968
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 973
    .end local v1    # "word":Ljava/lang/String;
    :cond_1
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method private static buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;
    .locals 7
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "searchType"    # Lcom/nuance/sample/music/PlayMusicType;

    .prologue
    .line 808
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->stringIsNonAsciiWithCase(Ljava/lang/String;)Z

    move-result v1

    .line 809
    .local v1, "isNonAsciiWithCase":Z
    const/4 v2, 0x0

    .line 811
    .local v2, "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 812
    invoke-static {}, Lcom/nuance/sample/music/SearchMusic;->$SWITCH_TABLE$com$nuance$sample$music$PlayMusicType()[I

    move-result-object v5

    invoke-virtual {p3}, Lcom/nuance/sample/music/PlayMusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 830
    :pswitch_0
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 831
    .restart local v2    # "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 837
    :cond_0
    :goto_0
    if-eqz p2, :cond_3

    .line 838
    if-eqz v1, :cond_2

    .line 839
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v5, v2}, Lcom/nuance/sample/music/SearchMusic;->augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 841
    .local v4, "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 842
    invoke-static {p0, v4}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    .line 859
    .end local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-object v5

    .line 814
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getAlbumNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 815
    goto :goto_0

    .line 818
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getArtistNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 819
    goto :goto_0

    .line 822
    :pswitch_3
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getPlaylistNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 823
    goto :goto_0

    .line 826
    :pswitch_4
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSongNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 827
    goto :goto_0

    .line 844
    .restart local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 847
    .end local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 850
    :cond_3
    const-string/jumbo v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 851
    .local v3, "parts":[Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 852
    invoke-static {v3, v2}, Lcom/nuance/sample/music/SearchMusic;->augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 853
    .local v0, "augmentedParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 854
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 856
    :cond_4
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 859
    .end local v0    # "augmentedParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 812
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static byAlbum(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/nuance/sample/music/SearchMusic;->getAlbumList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 331
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "album"

    .line 332
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    .line 331
    invoke-static {v0, p0, p1, v1}, Lcom/nuance/sample/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static byArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/nuance/sample/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 313
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "artist"

    .line 314
    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 313
    invoke-static {v0, p0, p1, v1}, Lcom/nuance/sample/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static byArtistAndAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumName"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 208
    const/4 v3, 0x5

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v1, v6

    const-string/jumbo v3, "album_id"

    aput-object v3, v1, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "artist"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "title"

    aput-object v4, v1, v3

    const/4 v3, 0x4

    .line 209
    const-string/jumbo v4, "album"

    aput-object v4, v1, v3

    .line 211
    .local v1, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "artist"

    .line 212
    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 211
    invoke-static {v3, p2, v5, v4}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 213
    .local v2, "where":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "artist"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v2, v3, p2, v4, v6}, Lcom/nuance/sample/music/SearchMusic;->addConditionWithoutSpaceIfNeed(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;I)V

    .line 214
    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 216
    .local v0, "offset":I
    const-string/jumbo v3, "album"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v3, p1, v5, v4}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string/jumbo v3, "album"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v2, v3, p1, v4, v0}, Lcom/nuance/sample/music/SearchMusic;->addConditionWithoutSpaceIfNeed(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;I)V

    .line 219
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v1, v3}, Lcom/nuance/sample/music/SearchMusic;->byWhereClauseAndProjection(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    return-object v3
.end method

.method private static byEverything(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isLastAttempt"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    const/4 v9, 0x0

    .line 137
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 138
    .local v12, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "album_id"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    .line 139
    const-string/jumbo v2, "artist"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "title"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "album"

    aput-object v2, v3, v1

    .line 140
    .local v3, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "artist"

    const/4 v5, 0x1

    .line 141
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    move-object/from16 v0, p1

    invoke-static {v2, v0, v5, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 142
    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 143
    const-string/jumbo v2, "title"

    const/4 v5, 0x1

    .line 144
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    .line 143
    move-object/from16 v0, p1

    invoke-static {v2, v0, v5, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 145
    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 146
    const-string/jumbo v2, "album"

    const/4 v5, 0x1

    .line 147
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    .line 146
    move-object/from16 v0, p1

    invoke-static {v2, v0, v5, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 140
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 150
    .local v4, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 151
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    .line 152
    const-string/jumbo v6, "title_key"

    .line 150
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 154
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v13, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v9, :cond_1

    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    sget-object v1, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "** cursor.size()---"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 161
    :cond_0
    const-string/jumbo v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 160
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 163
    .local v14, "songId":Ljava/lang/Long;
    const-string/jumbo v1, "album_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 162
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 165
    .local v7, "albumId":Ljava/lang/String;
    const-string/jumbo v1, "artist"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 164
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 167
    .local v8, "artist":Ljava/lang/String;
    const-string/jumbo v1, "title"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 166
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 168
    .local v15, "title":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "content://media/external/audio/albumart/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 169
    .local v11, "imageUri":Landroid/net/Uri;
    new-instance v1, Lcom/nuance/sample/music/MusicDetails;

    .line 170
    invoke-direct {v1, v14, v15, v8, v11}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 169
    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    sget-object v1, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Artist---"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 173
    sget-object v1, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Title ---"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 174
    sget-object v1, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "ALBUM_ID ---"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 177
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_0

    .line 184
    .end local v7    # "albumId":Ljava/lang/String;
    .end local v8    # "artist":Ljava/lang/String;
    .end local v11    # "imageUri":Landroid/net/Uri;
    .end local v14    # "songId":Ljava/lang/Long;
    .end local v15    # "title":Ljava/lang/String;
    :cond_1
    if-eqz v9, :cond_2

    .line 185
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v12, v13

    .line 189
    .end local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_3
    :goto_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 194
    :cond_4
    return-object v12

    .line 180
    :catch_0
    move-exception v10

    .line 182
    .local v10, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v1, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 184
    if-eqz v9, :cond_3

    .line 185
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 183
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    .line 184
    :goto_2
    if-eqz v9, :cond_5

    .line 185
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 187
    :cond_5
    throw v1

    .line 183
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catchall_1
    move-exception v1

    move-object v12, v13

    .end local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_2

    .line 180
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v10

    move-object v12, v13

    .end local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_1
.end method

.method public static byPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/nuance/sample/music/SearchMusic;->getPlaylistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 363
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/nuance/sample/music/SearchMusic;->searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static byTitle(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/nuance/sample/music/SearchMusic;->getTitleList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static byTitleAndAlbum(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .param p2, "albumName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 238
    const/4 v3, 0x5

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v1, v6

    const-string/jumbo v3, "album_id"

    aput-object v3, v1, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "artist"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "title"

    aput-object v4, v1, v3

    const/4 v3, 0x4

    .line 239
    const-string/jumbo v4, "album"

    aput-object v4, v1, v3

    .line 241
    .local v1, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "album"

    .line 242
    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    .line 241
    invoke-static {v3, p2, v5, v4}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 243
    .local v2, "where":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "album"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v2, v3, p2, v4, v6}, Lcom/nuance/sample/music/SearchMusic;->addConditionWithoutSpaceIfNeed(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;I)V

    .line 244
    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 246
    .local v0, "offset":I
    const-string/jumbo v3, "title"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v3, p1, v5, v4}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const-string/jumbo v3, "title"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v2, v3, p1, v4, v0}, Lcom/nuance/sample/music/SearchMusic;->addConditionWithoutSpaceIfNeed(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;I)V

    .line 249
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v1, v3}, Lcom/nuance/sample/music/SearchMusic;->byWhereClauseAndProjection(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    return-object v3
.end method

.method public static byTitleAndArtist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .param p2, "artistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 223
    const/4 v3, 0x5

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v1, v6

    const-string/jumbo v3, "album_id"

    aput-object v3, v1, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "artist"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "title"

    aput-object v4, v1, v3

    const/4 v3, 0x4

    .line 224
    const-string/jumbo v4, "album"

    aput-object v4, v1, v3

    .line 226
    .local v1, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "artist"

    .line 227
    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 226
    invoke-static {v3, p2, v5, v4}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 228
    .local v2, "where":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "artist"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v2, v3, p2, v4, v6}, Lcom/nuance/sample/music/SearchMusic;->addConditionWithoutSpaceIfNeed(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;I)V

    .line 229
    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 231
    .local v0, "offset":I
    const-string/jumbo v3, "title"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v3, p1, v5, v4}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const-string/jumbo v3, "title"

    sget-object v4, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v2, v3, p1, v4, v0}, Lcom/nuance/sample/music/SearchMusic;->addConditionWithoutSpaceIfNeed(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;I)V

    .line 234
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v1, v3}, Lcom/nuance/sample/music/SearchMusic;->byWhereClauseAndProjection(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    return-object v3
.end method

.method private static byWhereClauseAndProjection(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "where"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    const/4 v8, 0x0

    .line 255
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 258
    .local v11, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 259
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    .line 260
    const-string/jumbo v5, "title_key"

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 258
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 262
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v12, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v8, :cond_1

    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "** cursor.size()---"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 269
    :cond_0
    const-string/jumbo v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 268
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 271
    .local v13, "songId":Ljava/lang/Long;
    const-string/jumbo v0, "album_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 270
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 273
    .local v6, "albumId":Ljava/lang/String;
    const-string/jumbo v0, "artist"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 272
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 275
    .local v7, "artist":Ljava/lang/String;
    const-string/jumbo v0, "title"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 274
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 276
    .local v14, "title":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://media/external/audio/albumart/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 277
    .local v10, "imageUri":Landroid/net/Uri;
    new-instance v0, Lcom/nuance/sample/music/MusicDetails;

    .line 278
    invoke-direct {v0, v13, v14, v7, v10}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 277
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Artist---"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 281
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Title ---"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 282
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ALBUM_ID ---"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 285
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 292
    .end local v6    # "albumId":Ljava/lang/String;
    .end local v7    # "artist":Ljava/lang/String;
    .end local v10    # "imageUri":Landroid/net/Uri;
    .end local v13    # "songId":Ljava/lang/Long;
    .end local v14    # "title":Ljava/lang/String;
    :cond_1
    if-eqz v8, :cond_2

    .line 293
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v11, v12

    .line 297
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_3
    :goto_0
    return-object v11

    .line 288
    :catch_0
    move-exception v9

    .line 290
    .local v9, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 292
    if-eqz v8, :cond_3

    .line 293
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 291
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    .line 292
    :goto_2
    if-eqz v8, :cond_4

    .line 293
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 295
    :cond_4
    throw v0

    .line 291
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catchall_1
    move-exception v0

    move-object v11, v12

    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_2

    .line 288
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v9

    move-object v11, v12

    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_1
.end method

.method private static conditionWithoutSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "nameField"    # Ljava/lang/String;

    .prologue
    .line 982
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "REPLACE(`"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "`,\' \',\'\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAlbumList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 526
    const/4 v13, 0x0

    .line 527
    .local v13, "cursor":Landroid/database/Cursor;
    const/4 v15, 0x0

    .line 528
    .local v15, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "album"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    .line 529
    const-string/jumbo v3, "artist"

    aput-object v3, v4, v2

    .line 530
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v5, 0x0

    .line 533
    .local v5, "where":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/nuance/sample/music/SearchMusic;->isKoreanInSettings()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 534
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") OR ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "album"

    invoke-static {v3}, Lcom/nuance/sample/music/SearchMusic;->conditionWithoutSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 535
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    .line 534
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v3, v0, v1, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 535
    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 534
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 539
    :cond_0
    :try_start_0
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "TableName : + "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 541
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 542
    sget-object v3, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    .line 543
    const-string/jumbo v7, "album_key"

    .line 541
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 545
    if-eqz v13, :cond_2

    .line 547
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "** cursor.size()---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 549
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 550
    const/4 v12, 0x0

    .line 551
    .local v12, "count":I
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v16, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_1
    :try_start_1
    const-string/jumbo v2, "_id"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 553
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 556
    .local v10, "albumId":Ljava/lang/Long;
    const-string/jumbo v2, "album"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 555
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 558
    .local v11, "albumName":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 557
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 559
    .local v9, "albumArtist":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "content://media/external/audio/albumart/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 562
    .local v8, "albumArtUri":Landroid/net/Uri;
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Album ID---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 563
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Album Name ---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 565
    new-instance v2, Lcom/nuance/sample/music/MusicDetails;

    .line 566
    invoke-direct {v2, v10, v11, v9, v8}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 565
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 567
    add-int/lit8 v12, v12, 0x1

    .line 568
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x1e

    .line 552
    if-lt v12, v2, :cond_1

    move-object/from16 v15, v16

    .line 574
    .end local v8    # "albumArtUri":Landroid/net/Uri;
    .end local v9    # "albumArtist":Ljava/lang/String;
    .end local v10    # "albumId":Ljava/lang/Long;
    .end local v11    # "albumName":Ljava/lang/String;
    .end local v12    # "count":I
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_2
    :goto_1
    if-eqz v13, :cond_3

    .line 575
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 579
    :cond_3
    :goto_2
    return-object v15

    .line 531
    .end local v5    # "where":Ljava/lang/String;
    :cond_4
    const-string/jumbo v2, "album"

    .line 532
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    .line 531
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v3}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 570
    .restart local v5    # "where":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 572
    .local v14, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_2
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 574
    if-eqz v13, :cond_3

    .line 575
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 573
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 574
    :goto_4
    if-eqz v13, :cond_5

    .line 575
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 577
    :cond_5
    throw v2

    .line 573
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "count":I
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_4

    .line 570
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v14

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_3

    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v8    # "albumArtUri":Landroid/net/Uri;
    .restart local v9    # "albumArtist":Ljava/lang/String;
    .restart local v10    # "albumId":Ljava/lang/Long;
    .restart local v11    # "albumName":Ljava/lang/String;
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_6
    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_1
.end method

.method public static getArtistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    const/16 v17, 0x0

    .line 448
    .local v17, "cursor":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 449
    .local v18, "cursorAlbumArt":Landroid/database/Cursor;
    const/16 v21, 0x0

    .line 450
    .local v21, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "artist"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    .line 451
    const-string/jumbo v3, "artist_key"

    aput-object v3, v4, v2

    .line 452
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v5, 0x0

    .line 455
    .local v5, "where":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/nuance/sample/music/SearchMusic;->isKoreanInSettings()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 456
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") OR ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "artist"

    invoke-static {v3}, Lcom/nuance/sample/music/SearchMusic;->conditionWithoutSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v3, v0, v1, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 459
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 460
    sget-object v3, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 461
    const/4 v6, 0x0

    const-string/jumbo v7, "artist_key"

    .line 459
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 463
    if-eqz v17, :cond_4

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 465
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "** cursor.size()---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 467
    const/16 v16, 0x0

    .line 468
    .local v16, "count":I
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v22, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_1
    :try_start_1
    const-string/jumbo v2, "_id"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 470
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 473
    .local v14, "artistId":Ljava/lang/Long;
    const-string/jumbo v2, "artist"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 472
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 475
    .local v13, "artist":Ljava/lang/String;
    const-string/jumbo v2, "artist_key"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 474
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 478
    .local v15, "artistKey":Ljava/lang/String;
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Artist ID---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 479
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Artist Name ---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 482
    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "album_id"

    aput-object v3, v8, v2

    .line 483
    .local v8, "projectionAlbumArt":[Ljava/lang/String;
    const-string/jumbo v2, "artist_key"

    .line 484
    const/4 v3, 0x1

    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 483
    invoke-static {v2, v15, v3, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    .line 485
    sget-object v20, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 486
    .local v20, "imageUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 487
    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 488
    const/4 v10, 0x0

    .line 489
    const-string/jumbo v11, "artist_key"

    move-object v9, v5

    .line 486
    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 490
    if-eqz v18, :cond_3

    .line 491
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 494
    const-string/jumbo v2, "album_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 493
    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 496
    .local v12, "albumId":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "content://media/external/audio/albumart/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    .line 499
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ALBUM_ID ---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 501
    .end local v12    # "albumId":Ljava/lang/String;
    :cond_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 504
    :cond_3
    new-instance v2, Lcom/nuance/sample/music/MusicDetails;

    .line 505
    move-object/from16 v0, v20

    invoke-direct {v2, v14, v13, v13, v0}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 504
    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 506
    add-int/lit8 v16, v16, 0x1

    .line 507
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0x1e

    .line 469
    move/from16 v0, v16

    if-lt v0, v2, :cond_1

    move-object/from16 v21, v22

    .line 513
    .end local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .end local v13    # "artist":Ljava/lang/String;
    .end local v14    # "artistId":Ljava/lang/Long;
    .end local v15    # "artistKey":Ljava/lang/String;
    .end local v16    # "count":I
    .end local v20    # "imageUri":Landroid/net/Uri;
    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_4
    :goto_1
    if-eqz v17, :cond_5

    .line 514
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 516
    :cond_5
    if-eqz v18, :cond_6

    .line 517
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 521
    :cond_6
    :goto_2
    return-object v21

    .line 453
    .end local v5    # "where":Ljava/lang/String;
    :cond_7
    const-string/jumbo v2, "artist"

    .line 454
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 453
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v3}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 509
    .restart local v5    # "where":Ljava/lang/String;
    :catch_0
    move-exception v19

    .line 511
    .local v19, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_2
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 513
    if-eqz v17, :cond_8

    .line 514
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 516
    :cond_8
    if-eqz v18, :cond_6

    .line 517
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 512
    .end local v19    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 513
    :goto_4
    if-eqz v17, :cond_9

    .line 514
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 516
    :cond_9
    if-eqz v18, :cond_a

    .line 517
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 519
    :cond_a
    throw v2

    .line 512
    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v16    # "count":I
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v21, v22

    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_4

    .line 509
    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v19

    move-object/from16 v21, v22

    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_3

    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .restart local v13    # "artist":Ljava/lang/String;
    .restart local v14    # "artistId":Ljava/lang/Long;
    .restart local v15    # "artistKey":Ljava/lang/String;
    .restart local v20    # "imageUri":Landroid/net/Uri;
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_b
    move-object/from16 v21, v22

    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_1
.end method

.method private static getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/nuance/sample/music/PlayMusicType;)Ljava/util/List;
    .locals 6
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchCondition"    # Ljava/lang/String;
    .param p3, "searchType"    # Lcom/nuance/sample/music/PlayMusicType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/nuance/sample/music/PlayMusicType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 369
    invoke-static {p0, p1, p2, v5, p3}, Lcom/nuance/sample/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    .line 371
    .local v0, "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 384
    .end local v0    # "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :goto_0
    return-object v0

    .line 375
    .restart local v0    # "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_0
    const-string/jumbo v3, " "

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 376
    .local v1, "parts":[Ljava/lang/String;
    array-length v3, v1

    if-le v3, v5, :cond_1

    .line 377
    const-string/jumbo v3, " "

    const-string/jumbo v4, "%"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 378
    invoke-static {p0, p1, p2, v5, p3}, Lcom/nuance/sample/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/util/List;

    move-result-object v2

    .line 380
    .local v2, "secondResult":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    move-object v0, v2

    .line 381
    goto :goto_0

    .line 385
    .end local v2    # "secondResult":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_1
    const/4 v3, 0x0

    .line 384
    invoke-static {p0, p1, p2, v3, p3}, Lcom/nuance/sample/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/util/List;
    .locals 19
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchCondition"    # Ljava/lang/String;
    .param p3, "exactSearch"    # Z
    .param p4, "searchType"    # Lcom/nuance/sample/music/PlayMusicType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/nuance/sample/music/PlayMusicType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391
    const/4 v12, 0x0

    .line 392
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v15, 0x0

    .line 393
    .local v15, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "_id"

    aput-object v5, v6, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "album_id"

    aput-object v5, v6, v4

    const/4 v4, 0x2

    .line 394
    const-string/jumbo v5, "artist"

    aput-object v5, v6, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "title"

    aput-object v5, v6, v4

    .line 395
    .local v6, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static {v0, v1, v2, v3}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v7

    .line 399
    .local v7, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 400
    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    .line 401
    const-string/jumbo v9, "title_key"

    .line 399
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 403
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v16, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v12, :cond_1

    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 406
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "** cursor.size()---"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 410
    :cond_0
    const-string/jumbo v4, "_id"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 409
    invoke-interface {v12, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 412
    .local v17, "songId":Ljava/lang/Long;
    const-string/jumbo v4, "album_id"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 411
    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 414
    .local v10, "albumId":Ljava/lang/String;
    const-string/jumbo v4, "artist"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 413
    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 416
    .local v11, "artist":Ljava/lang/String;
    const-string/jumbo v4, "title"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 415
    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 417
    .local v18, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "content://media/external/audio/albumart/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 418
    .local v14, "imageUri":Landroid/net/Uri;
    new-instance v4, Lcom/nuance/sample/music/MusicDetails;

    .line 419
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v1, v11, v14}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 418
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Artist---"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 422
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Title ---"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 423
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "ALBUM_ID ---"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 426
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    .line 433
    .end local v10    # "albumId":Ljava/lang/String;
    .end local v11    # "artist":Ljava/lang/String;
    .end local v14    # "imageUri":Landroid/net/Uri;
    .end local v17    # "songId":Ljava/lang/Long;
    .end local v18    # "title":Ljava/lang/String;
    :cond_1
    if-eqz v12, :cond_2

    .line 434
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object/from16 v15, v16

    .line 437
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_3
    :goto_0
    invoke-interface {v15}, Ljava/util/List;->size()I

    .line 442
    return-object v15

    .line 429
    :catch_0
    move-exception v13

    .line 431
    .local v13, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 433
    if-eqz v12, :cond_3

    .line 434
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 432
    .end local v13    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 433
    :goto_2
    if-eqz v12, :cond_4

    .line 434
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 436
    :cond_4
    throw v4

    .line 432
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catchall_1
    move-exception v4

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_2

    .line 429
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v13

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_1
.end method

.method public static getGenericList(Landroid/content/Context;Ljava/lang/String;Z)Landroid/util/Pair;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Landroid/util/Pair",
            "<",
            "Lcom/nuance/sample/music/PlayMusicType;",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 79
    invoke-static {p0, p1, p2}, Lcom/nuance/sample/music/SearchMusic;->getPlaylistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 80
    .local v0, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    new-instance v1, Landroid/util/Pair;

    .line 82
    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 81
    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 102
    :goto_0
    return-object v1

    .line 86
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/nuance/sample/music/SearchMusic;->getTitleList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 88
    new-instance v1, Landroid/util/Pair;

    .line 89
    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    .line 88
    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/nuance/sample/music/SearchMusic;->getAlbumList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 95
    new-instance v1, Landroid/util/Pair;

    .line 96
    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    .line 95
    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 101
    :cond_2
    invoke-static {p0, p1, p2}, Lcom/nuance/sample/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 102
    new-instance v1, Landroid/util/Pair;

    .line 103
    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 102
    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static getPlaylistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 740
    const/4 v9, 0x0

    .line 741
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 742
    .local v11, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "name"

    aput-object v3, v4, v2

    .line 745
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v5, 0x0

    .line 748
    .local v5, "where":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/nuance/sample/music/SearchMusic;->isKoreanInSettings()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 749
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") OR ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "name"

    invoke-static {v3}, Lcom/nuance/sample/music/SearchMusic;->conditionWithoutSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 750
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 749
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v3, v0, v1, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 750
    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 749
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 753
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 754
    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 755
    const/4 v6, 0x0

    const-string/jumbo v7, "name"

    .line 753
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 760
    if-eqz v9, :cond_3

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 762
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "** cursor.size()---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 764
    const/4 v8, 0x0

    .line 765
    .local v8, "count":I
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 768
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v12, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_1
    :try_start_1
    const-string/jumbo v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 767
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 772
    .local v13, "playlistId":Ljava/lang/Long;
    const-string/jumbo v2, "name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 771
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 774
    .local v14, "playlistName":Ljava/lang/String;
    const-string/jumbo v2, "Quick list"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 776
    const v2, 0x7f0a0295

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 781
    .local v15, "playlistNameLocalized":Ljava/lang/String;
    :goto_1
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Playlist ID---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 782
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Playlist Name ---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 785
    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/nuance/sample/music/SearchMusic;->searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    .line 787
    .local v16, "songList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v16, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_2

    .line 788
    new-instance v2, Lcom/nuance/sample/music/MusicDetails;

    .line 789
    const-string/jumbo v3, ""

    sget-object v6, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-direct {v2, v13, v15, v3, v6}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 788
    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 790
    add-int/lit8 v8, v8, 0x1

    .line 792
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x1e

    .line 766
    if-lt v8, v2, :cond_1

    move-object v11, v12

    .line 798
    .end local v8    # "count":I
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .end local v13    # "playlistId":Ljava/lang/Long;
    .end local v14    # "playlistName":Ljava/lang/String;
    .end local v15    # "playlistNameLocalized":Ljava/lang/String;
    .end local v16    # "songList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_3
    :goto_2
    if-eqz v9, :cond_4

    .line 799
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 802
    :cond_4
    :goto_3
    return-object v11

    .line 746
    .end local v5    # "where":Ljava/lang/String;
    :cond_5
    const-string/jumbo v2, "name"

    .line 747
    sget-object v3, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 746
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v3}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 778
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v8    # "count":I
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v13    # "playlistId":Ljava/lang/Long;
    .restart local v14    # "playlistName":Ljava/lang/String;
    :cond_6
    move-object v15, v14

    .restart local v15    # "playlistNameLocalized":Ljava/lang/String;
    goto :goto_1

    .line 794
    .end local v8    # "count":I
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .end local v13    # "playlistId":Ljava/lang/Long;
    .end local v14    # "playlistName":Ljava/lang/String;
    .end local v15    # "playlistNameLocalized":Ljava/lang/String;
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_0
    move-exception v10

    .line 796
    .local v10, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_2
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 798
    if-eqz v9, :cond_4

    .line 799
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 797
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 798
    :goto_5
    if-eqz v9, :cond_7

    .line 799
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 801
    :cond_7
    throw v2

    .line 797
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v8    # "count":I
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object v11, v12

    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_5

    .line 794
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v10

    move-object v11, v12

    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_4

    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v13    # "playlistId":Ljava/lang/Long;
    .restart local v14    # "playlistName":Ljava/lang/String;
    .restart local v15    # "playlistNameLocalized":Ljava/lang/String;
    .restart local v16    # "songList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_8
    move-object v11, v12

    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_2
.end method

.method public static getTitleList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 584
    const/4 v11, 0x0

    .line 585
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 586
    .local v14, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "album_id"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    .line 587
    const-string/jumbo v3, "artist"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "title"

    aput-object v3, v4, v2

    .line 589
    .local v4, "projection":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 590
    const-string/jumbo v5, "is_music=1"

    .line 602
    .local v5, "where":Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 603
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    .line 604
    const-string/jumbo v7, "title_key"

    .line 602
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 606
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 607
    .end local v14    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v15, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v11, :cond_2

    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 609
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "** cursor.size()---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 611
    const/4 v10, 0x0

    .line 614
    .local v10, "count":I
    :cond_1
    const-string/jumbo v2, "_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 613
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 616
    .local v16, "songId":Ljava/lang/Long;
    const-string/jumbo v2, "album_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 615
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 618
    .local v8, "albumId":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 617
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 620
    .local v9, "artist":Ljava/lang/String;
    const-string/jumbo v2, "title"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 619
    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 621
    .local v17, "title":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "content://media/external/audio/albumart/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 624
    .local v13, "imageUri":Landroid/net/Uri;
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Artist---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 625
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Title ---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 626
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ALBUM_ID ---"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 628
    new-instance v2, Lcom/nuance/sample/music/MusicDetails;

    .line 629
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v2, v0, v1, v9, v13}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 628
    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 630
    add-int/lit8 v10, v10, 0x1

    .line 631
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0x1e

    .line 612
    if-lt v10, v2, :cond_1

    .line 637
    .end local v8    # "albumId":Ljava/lang/String;
    .end local v9    # "artist":Ljava/lang/String;
    .end local v10    # "count":I
    .end local v13    # "imageUri":Landroid/net/Uri;
    .end local v16    # "songId":Ljava/lang/Long;
    .end local v17    # "title":Ljava/lang/String;
    :cond_2
    if-eqz v11, :cond_3

    .line 638
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v14, v15

    .line 641
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v14    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_4
    :goto_1
    invoke-interface {v14}, Ljava/util/List;->size()I

    .line 645
    return-object v14

    .line 592
    .end local v5    # "where":Ljava/lang/String;
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "title"

    .line 593
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v3, v0, v1, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 594
    const-string/jumbo v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 595
    const-string/jumbo v3, "is_music"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "=1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 592
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 596
    .restart local v5    # "where":Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/music/SearchMusic;->isKoreanInSettings()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 597
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") OR ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "title"

    invoke-static {v3}, Lcom/nuance/sample/music/SearchMusic;->conditionWithoutSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 598
    sget-object v6, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    .line 597
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v3, v0, v1, v6}, Lcom/nuance/sample/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/nuance/sample/music/PlayMusicType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 598
    const-string/jumbo v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "is_music"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "=1)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 597
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 633
    :catch_0
    move-exception v12

    .line 635
    .local v12, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    sget-object v2, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 637
    if-eqz v11, :cond_4

    .line 638
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 636
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 637
    :goto_3
    if-eqz v11, :cond_6

    .line 638
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 640
    :cond_6
    throw v2

    .line 636
    .end local v14    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object v14, v15

    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v14    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_3

    .line 633
    .end local v14    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v12

    move-object v14, v15

    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v14    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_2
.end method

.method public static isAnyMusic(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 107
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "_id"

    aput-object v0, v2, v8

    const-string/jumbo v0, "album_id"

    aput-object v0, v2, v9

    const/4 v0, 0x2

    .line 108
    const-string/jumbo v1, "artist"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "title"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "album"

    aput-object v1, v2, v0

    .line 109
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "is_music != 0 "

    .line 110
    .local v3, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 112
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 113
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 114
    const/4 v4, 0x0

    const-string/jumbo v5, "title_key"

    .line 112
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 115
    if-nez v6, :cond_0

    move v0, v8

    .line 130
    :goto_0
    return v0

    .line 117
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v9, :cond_1

    .line 118
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 119
    goto :goto_0

    .line 121
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v9

    .line 122
    goto :goto_0

    .line 124
    :catch_0
    move-exception v7

    .line 126
    .local v7, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V

    .line 127
    if-eqz v6, :cond_2

    .line 128
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v8

    .line 130
    goto :goto_0
.end method

.method private static isKoreanInSettings()Z
    .locals 2

    .prologue
    .line 978
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ko-KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static populateMusicMappings(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 864
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSongNameList()Ljava/util/ArrayList;

    move-result-object v12

    .line 865
    .local v12, "songNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getAlbumNameList()Ljava/util/ArrayList;

    move-result-object v7

    .line 866
    .local v7, "albumNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getArtistNameList()Ljava/util/ArrayList;

    move-result-object v9

    .line 868
    .local v9, "artistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "artist"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "title"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    .line 869
    const-string/jumbo v1, "album"

    aput-object v1, v2, v0

    .line 870
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 871
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 872
    const-string/jumbo v5, "title_key"

    move-object v4, v3

    .line 870
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 874
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_5

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 876
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "** cursor.size()---"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 880
    :cond_0
    const-string/jumbo v0, "title"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 879
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 882
    .local v11, "songName":Ljava/lang/String;
    const-string/jumbo v0, "album"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 881
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 884
    .local v6, "albumName":Ljava/lang/String;
    const-string/jumbo v0, "artist"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 883
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 886
    .local v8, "artistName":Ljava/lang/String;
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 888
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Adding song name --- "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 889
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 891
    :cond_1
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 893
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Adding album name --- "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 894
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 896
    :cond_2
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 898
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Adding artist name --- "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 899
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 901
    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 907
    .end local v6    # "albumName":Ljava/lang/String;
    .end local v8    # "artistName":Ljava/lang/String;
    .end local v11    # "songName":Ljava/lang/String;
    :goto_0
    if-eqz v10, :cond_4

    .line 908
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 909
    const/4 v10, 0x0

    .line 913
    :cond_4
    invoke-static {p0}, Lcom/nuance/sample/music/SearchMusic;->populatePlaylistMappings(Landroid/content/Context;)V

    .line 914
    return-void

    .line 904
    :cond_5
    :try_start_1
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v1, "No songs in library."

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 906
    :catchall_0
    move-exception v0

    .line 907
    if-eqz v10, :cond_6

    .line 908
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 909
    const/4 v10, 0x0

    .line 911
    :cond_6
    throw v0
.end method

.method private static populatePlaylistMappings(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 917
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getPlaylistNameList()Ljava/util/ArrayList;

    move-result-object v8

    .line 919
    .local v8, "playlistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "name"

    aput-object v1, v2, v0

    .line 920
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 921
    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 922
    const-string/jumbo v5, "name"

    move-object v4, v3

    .line 920
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 924
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 926
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "** cursor.size()---"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 930
    :cond_0
    const-string/jumbo v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 929
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 932
    .local v7, "playlistName":Ljava/lang/String;
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 934
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Adding playlist --- "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 935
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 943
    .end local v7    # "playlistName":Ljava/lang/String;
    :goto_0
    if-eqz v6, :cond_2

    .line 944
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 945
    :cond_2
    const/4 v6, 0x0

    .line 947
    return-void

    .line 940
    :cond_3
    :try_start_1
    sget-object v0, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v1, "No playlists in library."

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 942
    :catchall_0
    move-exception v0

    .line 943
    if-eqz v6, :cond_4

    .line 944
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 945
    :cond_4
    const/4 v6, 0x0

    .line 946
    throw v0
.end method

.method private static searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 650
    const/16 v18, 0x0

    .line 651
    .local v18, "cursor":Landroid/database/Cursor;
    const/16 v21, 0x0

    .line 652
    .local v21, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    const-string/jumbo v4, "name"

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 654
    .local v7, "where":Ljava/lang/String;
    const-string/jumbo v9, "LENGTH(name)"

    .line 657
    .local v9, "sortOrder":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 658
    sget-object v5, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 657
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 668
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "** cursor.size()---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 669
    if-eqz v18, :cond_1

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 671
    const-string/jumbo v4, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 670
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    .line 676
    .local v23, "playlist_id":Ljava/lang/Long;
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "playlist_id --"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 678
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 680
    const/4 v4, 0x5

    new-array v12, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "_id"

    aput-object v5, v12, v4

    const/4 v4, 0x1

    .line 681
    const-string/jumbo v5, "artist"

    aput-object v5, v12, v4

    const/4 v4, 0x2

    .line 682
    const-string/jumbo v5, "title"

    aput-object v5, v12, v4

    const/4 v4, 0x3

    .line 683
    const-string/jumbo v5, "audio_id"

    aput-object v5, v12, v4

    const/4 v4, 0x4

    .line 684
    const-string/jumbo v5, "album_id"

    aput-object v5, v12, v4

    .line 686
    .local v12, "projection":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 687
    const-string/jumbo v4, "external"

    .line 688
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 687
    invoke-static {v4, v5, v6}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v11

    .line 689
    const-string/jumbo v13, "is_music != 0 "

    const/4 v14, 0x0

    .line 690
    const-string/jumbo v15, "play_order"

    .line 686
    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 697
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Size of songs in playlist *** *** ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 698
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 697
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 700
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 701
    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .local v22, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 705
    :cond_0
    const-string/jumbo v4, "audio_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 704
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    .line 708
    .local v24, "songId":Ljava/lang/Long;
    const-string/jumbo v4, "album_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 707
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 711
    .local v16, "albumId":Ljava/lang/String;
    const-string/jumbo v4, "artist"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 710
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 714
    .local v17, "artist":Ljava/lang/String;
    const-string/jumbo v4, "title"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 713
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 715
    .local v25, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "content://media/external/audio/albumart/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    .line 716
    .local v20, "imageUri":Landroid/net/Uri;
    new-instance v4, Lcom/nuance/sample/music/MusicDetails;

    .line 717
    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/nuance/sample/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 716
    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 719
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Artist---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 720
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Title ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 721
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "ALBUM_ID ---"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->info(Ljava/lang/String;)V

    .line 724
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v21, v22

    .line 731
    .end local v12    # "projection":[Ljava/lang/String;
    .end local v16    # "albumId":Ljava/lang/String;
    .end local v17    # "artist":Ljava/lang/String;
    .end local v20    # "imageUri":Landroid/net/Uri;
    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .end local v23    # "playlist_id":Ljava/lang/Long;
    .end local v24    # "songId":Ljava/lang/Long;
    .end local v25    # "title":Ljava/lang/String;
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_1
    :goto_0
    if-eqz v18, :cond_2

    .line 732
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 735
    :cond_2
    :goto_1
    return-object v21

    .line 727
    :catch_0
    move-exception v19

    .line 729
    .local v19, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    sget-object v4, Lcom/nuance/sample/music/SearchMusic;->log:Lcom/vlingo/core/facade/logging/ILogger;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/logging/ILogger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 731
    if-eqz v18, :cond_2

    .line 732
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 730
    .end local v19    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 731
    :goto_3
    if-eqz v18, :cond_3

    .line 732
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 734
    :cond_3
    throw v4

    .line 730
    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v12    # "projection":[Ljava/lang/String;
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v23    # "playlist_id":Ljava/lang/Long;
    :catchall_1
    move-exception v4

    move-object/from16 v21, v22

    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_3

    .line 727
    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :catch_1
    move-exception v19

    move-object/from16 v21, v22

    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_2

    .end local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :cond_4
    move-object/from16 v21, v22

    .end local v22    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    .restart local v21    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    goto :goto_0
.end method
