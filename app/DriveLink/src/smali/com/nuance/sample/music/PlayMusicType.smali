.class public abstract enum Lcom/nuance/sample/music/PlayMusicType;
.super Ljava/lang/Enum;
.source "PlayMusicType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/music/PlayMusicType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALBUM:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum ARTIST:Lcom/nuance/sample/music/PlayMusicType;

.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum MOOD:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum NEXT:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum PAUSE:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum PLAY:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum PLAYALL:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum SONGLIST:Lcom/nuance/sample/music/PlayMusicType;

.field public static final enum TITLE:Lcom/nuance/sample/music/PlayMusicType;


# instance fields
.field private resourceId:Lcom/vlingo/core/internal/ResourceIdProvider$string;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 11
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$1;

    const-string/jumbo v1, "ALBUM"

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-direct {v0, v1, v5, v2}, Lcom/nuance/sample/music/PlayMusicType$1;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    .line 17
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$2;

    const-string/jumbo v1, "ARTIST"

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-direct {v0, v1, v6, v2}, Lcom/nuance/sample/music/PlayMusicType$2;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 23
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$3;

    const-string/jumbo v1, "NEXT"

    invoke-direct {v0, v1, v7, v4}, Lcom/nuance/sample/music/PlayMusicType$3;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    .line 29
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$4;

    const-string/jumbo v1, "PAUSE"

    invoke-direct {v0, v1, v8, v4}, Lcom/nuance/sample/music/PlayMusicType$4;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    .line 35
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$5;

    const-string/jumbo v1, "PLAY"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v4}, Lcom/nuance/sample/music/PlayMusicType$5;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    .line 41
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$6;

    const-string/jumbo v1, "PLAYALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, Lcom/nuance/sample/music/PlayMusicType$6;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->PLAYALL:Lcom/nuance/sample/music/PlayMusicType;

    .line 47
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$7;

    const-string/jumbo v1, "PLAYLIST"

    const/4 v2, 0x6

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/music/PlayMusicType$7;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 53
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$8;

    const-string/jumbo v1, "PREVIOUS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4}, Lcom/nuance/sample/music/PlayMusicType$8;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    .line 59
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$9;

    const-string/jumbo v1, "TITLE"

    const/16 v2, 0x8

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TITLE_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/music/PlayMusicType$9;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    .line 65
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$10;

    const-string/jumbo v1, "SONGLIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v4}, Lcom/nuance/sample/music/PlayMusicType$10;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->SONGLIST:Lcom/nuance/sample/music/PlayMusicType;

    .line 71
    new-instance v0, Lcom/nuance/sample/music/PlayMusicType$11;

    const-string/jumbo v1, "MOOD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4}, Lcom/nuance/sample/music/PlayMusicType$11;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->MOOD:Lcom/nuance/sample/music/PlayMusicType;

    .line 10
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/nuance/sample/music/PlayMusicType;

    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ALBUM:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->ARTIST:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->NEXT:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/nuance/sample/music/PlayMusicType;->PAUSE:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PLAY:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PLAYALL:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PLAYLIST:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->PREVIOUS:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->TITLE:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->SONGLIST:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/nuance/sample/music/PlayMusicType;->MOOD:Lcom/nuance/sample/music/PlayMusicType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/sample/music/PlayMusicType;->ENUM$VALUES:[Lcom/nuance/sample/music/PlayMusicType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 0
    .param p3, "resourceId"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 81
    iput-object p3, p0, Lcom/nuance/sample/music/PlayMusicType;->resourceId:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 82
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;Lcom/nuance/sample/music/PlayMusicType;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/nuance/sample/music/PlayMusicType;-><init>(Ljava/lang/String;ILcom/vlingo/core/internal/ResourceIdProvider$string;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/music/PlayMusicType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/music/PlayMusicType;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/music/PlayMusicType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/music/PlayMusicType;->ENUM$VALUES:[Lcom/nuance/sample/music/PlayMusicType;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/music/PlayMusicType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public abstract getMusicList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/music/MusicDetails;",
            ">;"
        }
    .end annotation
.end method

.method public getResourceId()Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/nuance/sample/music/PlayMusicType;->resourceId:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    return-object v0
.end method
