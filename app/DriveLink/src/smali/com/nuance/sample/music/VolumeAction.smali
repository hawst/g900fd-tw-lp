.class public abstract enum Lcom/nuance/sample/music/VolumeAction;
.super Ljava/lang/Enum;
.source "VolumeAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/music/VolumeAction;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DOWN:Lcom/nuance/sample/music/VolumeAction;

.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/music/VolumeAction;

.field public static final enum MUTE:Lcom/nuance/sample/music/VolumeAction;

.field public static final enum UNMUTE:Lcom/nuance/sample/music/VolumeAction;

.field public static final enum UP:Lcom/nuance/sample/music/VolumeAction;

.field private static final VOLUME_ACTIONS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/nuance/sample/music/VolumeAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private key:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v3, Lcom/nuance/sample/music/VolumeAction$1;

    const-string/jumbo v4, "UP"

    const-string/jumbo v5, "volumeup"

    invoke-direct {v3, v4, v2, v5}, Lcom/nuance/sample/music/VolumeAction$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/nuance/sample/music/VolumeAction;->UP:Lcom/nuance/sample/music/VolumeAction;

    .line 26
    new-instance v3, Lcom/nuance/sample/music/VolumeAction$2;

    const-string/jumbo v4, "DOWN"

    const-string/jumbo v5, "volumedown"

    invoke-direct {v3, v4, v6, v5}, Lcom/nuance/sample/music/VolumeAction$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/nuance/sample/music/VolumeAction;->DOWN:Lcom/nuance/sample/music/VolumeAction;

    .line 36
    new-instance v3, Lcom/nuance/sample/music/VolumeAction$3;

    const-string/jumbo v4, "MUTE"

    const-string/jumbo v5, "mute"

    invoke-direct {v3, v4, v7, v5}, Lcom/nuance/sample/music/VolumeAction$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/nuance/sample/music/VolumeAction;->MUTE:Lcom/nuance/sample/music/VolumeAction;

    .line 46
    new-instance v3, Lcom/nuance/sample/music/VolumeAction$4;

    const-string/jumbo v4, "UNMUTE"

    const-string/jumbo v5, "unmute"

    invoke-direct {v3, v4, v8, v5}, Lcom/nuance/sample/music/VolumeAction$4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/nuance/sample/music/VolumeAction;->UNMUTE:Lcom/nuance/sample/music/VolumeAction;

    const/4 v3, 0x4

    new-array v3, v3, [Lcom/nuance/sample/music/VolumeAction;

    sget-object v4, Lcom/nuance/sample/music/VolumeAction;->UP:Lcom/nuance/sample/music/VolumeAction;

    aput-object v4, v3, v2

    sget-object v4, Lcom/nuance/sample/music/VolumeAction;->DOWN:Lcom/nuance/sample/music/VolumeAction;

    aput-object v4, v3, v6

    sget-object v4, Lcom/nuance/sample/music/VolumeAction;->MUTE:Lcom/nuance/sample/music/VolumeAction;

    aput-object v4, v3, v7

    sget-object v4, Lcom/nuance/sample/music/VolumeAction;->UNMUTE:Lcom/nuance/sample/music/VolumeAction;

    aput-object v4, v3, v8

    sput-object v3, Lcom/nuance/sample/music/VolumeAction;->ENUM$VALUES:[Lcom/nuance/sample/music/VolumeAction;

    .line 62
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 63
    .local v1, "actionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/nuance/sample/music/VolumeAction;>;"
    invoke-static {}, Lcom/nuance/sample/music/VolumeAction;->values()[Lcom/nuance/sample/music/VolumeAction;

    move-result-object v3

    array-length v4, v3

    :goto_0
    if-lt v2, v4, :cond_0

    .line 66
    sput-object v1, Lcom/nuance/sample/music/VolumeAction;->VOLUME_ACTIONS:Ljava/util/Map;

    .line 67
    return-void

    .line 63
    :cond_0
    aget-object v0, v3, v2

    .line 64
    .local v0, "action":Lcom/nuance/sample/music/VolumeAction;
    invoke-virtual {v0}, Lcom/nuance/sample/music/VolumeAction;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput-object p3, p0, Lcom/nuance/sample/music/VolumeAction;->key:Ljava/lang/String;

    .line 73
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/nuance/sample/music/VolumeAction;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/nuance/sample/music/VolumeAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static getActionByKey(Ljava/lang/String;)Lcom/nuance/sample/music/VolumeAction;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 80
    sget-object v0, Lcom/nuance/sample/music/VolumeAction;->VOLUME_ACTIONS:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/music/VolumeAction;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/music/VolumeAction;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/music/VolumeAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/music/VolumeAction;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/music/VolumeAction;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/music/VolumeAction;->ENUM$VALUES:[Lcom/nuance/sample/music/VolumeAction;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/music/VolumeAction;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public abstract execute()Ljava/lang/String;
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/nuance/sample/music/VolumeAction;->key:Ljava/lang/String;

    return-object v0
.end method
