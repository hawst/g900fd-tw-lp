.class public Lcom/nuance/sample/music/SamsungMusicPlusUtil;
.super Ljava/lang/Object;
.source "SamsungMusicPlusUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/MusicPlusUtil;


# static fields
.field private static synthetic $SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType:[I = null

.field private static final DEFAULT_ALBUM_ART_URI_PREFIX:Ljava/lang/String; = "content://media/external/audio/albumart/"

.field private static final DEFAULT_MUSIC_SQUARE_URL:Ljava/lang/String; = "content://com.sec.music/music_square/music_square/"

.field private static final LOCAL_MUSICPLUS_ALBUM_ART_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/albumart/"

.field private static final LOCAL_MUSICPLUS_ALBUM_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/album"

.field private static final LOCAL_MUSICPLUS_ARTIST_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/artists"

.field private static final LOCAL_MUSICPLUS_GENERAL_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/media"

.field private static final LOCAL_MUSICPLUS_PLAYLIST_URI:Ljava/lang/String; = "content://com.samsung.musicplus/audio/playlists"

.field private static final MUSICPLUS_SQUARE_CONTENT_URI:Ljava/lang/String; = "content://com.samsung.musicplus/mood_"

.field private static final SAMSUNG_MUSICPLUS_AUTHORITY:Ljava/lang/String; = "com.samsung.musicplus"


# direct methods
.method static synthetic $SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType()[I
    .locals 3

    .prologue
    .line 11
    sget-object v0, Lcom/nuance/sample/music/SamsungMusicPlusUtil;->$SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->values()[Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/nuance/sample/music/SamsungMusicPlusUtil;->$SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFavoriteMusicIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMusicAppVersion()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public getMusicFavorite()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMusicMoodUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "mood"    # Ljava/lang/String;

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/nuance/sample/music/SamsungMusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://com.samsung.musicplus/mood_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://com.sec.music/music_square/music_square/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicPlaylistUri(J)Landroid/net/Uri;
    .locals 2
    .param p1, "playlist_id"    # J

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/music/SamsungMusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://com.samsung.musicplus/audio/playlists/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 80
    const-string/jumbo v1, "/members"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "external"

    invoke-static {v0, p1, p2}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;
    .locals 3
    .param p1, "Music"    # Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    .prologue
    .line 42
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/nuance/sample/music/SamsungMusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v0

    .line 43
    .local v0, "usingLocalMusicDB":Z
    invoke-static {}, Lcom/nuance/sample/music/SamsungMusicPlusUtil;->$SWITCH_TABLE$com$vlingo$core$internal$util$MusicPlusUtil$PlayType()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 65
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 45
    :pswitch_0
    if-eqz v0, :cond_0

    .line 46
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/media"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 47
    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 49
    :pswitch_1
    if-eqz v0, :cond_1

    .line 50
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/artists"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 51
    :cond_1
    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 53
    :pswitch_2
    if-eqz v0, :cond_2

    .line 54
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/album"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 55
    :cond_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 57
    :pswitch_3
    if-eqz v0, :cond_3

    .line 58
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/playlists"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 59
    :cond_3
    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 61
    :pswitch_4
    if-eqz v0, :cond_4

    .line 62
    const-string/jumbo v1, "content://com.samsung.musicplus/audio/albumart/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 63
    :cond_4
    const-string/jumbo v1, "content://media/external/audio/albumart/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public usingLocalMusicDB(Z)Z
    .locals 4
    .param p1, "usingMood"    # Z

    .prologue
    .line 31
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 33
    const-string/jumbo v3, "com.samsung.musicplus"

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 34
    .local v0, "provider":Landroid/content/ContentProviderClient;
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 35
    .local v1, "ret":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 36
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 37
    :cond_0
    return v1

    .line 34
    .end local v1    # "ret":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
