.class public Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;
.super Ljava/lang/Object;
.source "OnClickHomeScreenButtonListenerImpl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private mButtonType:Lcom/nuance/sample/util/MainActivityButtonType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 20
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 19
    sput-object v0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->TAG:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/nuance/sample/util/MainActivityButtonType;Landroid/content/Context;)V
    .locals 0
    .param p1, "buttonType"    # Lcom/nuance/sample/util/MainActivityButtonType;
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->context:Landroid/content/Context;

    .line 28
    iput-object p1, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->mButtonType:Lcom/nuance/sample/util/MainActivityButtonType;

    .line 29
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 33
    sget-object v2, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onClick("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->mButtonType:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {v4}, Lcom/nuance/sample/util/MainActivityButtonType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 36
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 37
    .local v0, "applicationContext":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->mButtonType:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {v3}, Lcom/nuance/sample/util/MainActivityButtonType;->getNextActivityClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 39
    const-string/jumbo v2, "extra_prompt_string"

    .line 40
    iget-object v3, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->mButtonType:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {v3}, Lcom/nuance/sample/util/MainActivityButtonType;->getPromptId()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 39
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string/jumbo v2, "extra_field_id"

    iget-object v3, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->mButtonType:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {v3}, Lcom/nuance/sample/util/MainActivityButtonType;->getFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v3

    .line 42
    invoke-virtual {v3}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 41
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    iget-object v2, p0, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;->context:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 44
    return-void
.end method
