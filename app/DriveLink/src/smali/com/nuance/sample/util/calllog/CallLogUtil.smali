.class public Lcom/nuance/sample/util/calllog/CallLogUtil;
.super Ljava/lang/Object;
.source "CallLogUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/nuance/sample/util/calllog/CallLogUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLastNCalls(Landroid/content/Context;IZ)Ljava/util/Vector;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .param p2, "missedOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZ)",
            "Ljava/util/Vector",
            "<",
            "Lcom/nuance/sample/util/calllog/LoggedCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    const/4 v8, 0x0

    .line 78
    .local v8, "c":Landroid/database/Cursor;
    new-instance v15, Ljava/util/Vector;

    invoke-direct {v15}, Ljava/util/Vector;-><init>()V

    .line 79
    .local v15, "missedCalls":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "name"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    .line 80
    const-string/jumbo v3, "number"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "date"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "new"

    aput-object v3, v4, v2

    .line 84
    .local v4, "projection":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 85
    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 87
    if-eqz p2, :cond_4

    const-string/jumbo v5, "type = ?"

    .line 89
    :goto_0
    if-eqz p2, :cond_5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 90
    const/16 v21, 0x3

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v6, v7

    .line 92
    :goto_1
    const-string/jumbo v7, "date DESC "

    .line 85
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 98
    invoke-static {v8}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 99
    const-string/jumbo v20, ""

    .line 100
    .local v20, "priorNumber":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 101
    const/4 v14, 0x0

    .line 102
    .local v14, "index":I
    const-string/jumbo v2, "name"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 103
    .local v17, "nameIdx":I
    const-string/jumbo v2, "date"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 104
    .local v10, "dateIdx":I
    const-string/jumbo v2, "number"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 106
    .local v19, "numberIdx":I
    :cond_0
    move/from16 v0, v17

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 107
    .local v16, "name":Ljava/lang/String;
    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 108
    .local v18, "number":Ljava/lang/String;
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 109
    .local v11, "dateMS":J
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "CallLogUtil found missed call from "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 110
    const-string/jumbo v5, " on "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 109
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1, v11, v12}, Lcom/nuance/sample/util/calllog/LoggedCall;->newInstance(Ljava/lang/String;Ljava/lang/String;J)Lcom/nuance/sample/util/calllog/LoggedCall;

    move-result-object v9

    .line 113
    .local v9, "call":Lcom/nuance/sample/util/calllog/LoggedCall;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 114
    invoke-virtual {v15, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_1
    move-object/from16 v20, v18

    .line 117
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 105
    move/from16 v0, p1

    if-lt v14, v0, :cond_0

    .line 123
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 125
    .end local v9    # "call":Lcom/nuance/sample/util/calllog/LoggedCall;
    .end local v10    # "dateIdx":I
    .end local v11    # "dateMS":J
    .end local v14    # "index":I
    .end local v16    # "name":Ljava/lang/String;
    .end local v17    # "nameIdx":I
    .end local v18    # "number":Ljava/lang/String;
    .end local v19    # "numberIdx":I
    .end local v20    # "priorNumber":Ljava/lang/String;
    :cond_3
    :goto_2
    return-object v15

    .line 88
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 91
    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 93
    :catch_0
    move-exception v13

    .line 94
    .local v13, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "CallLogUtil call data base query failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 119
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v20    # "priorNumber":Ljava/lang/String;
    :cond_6
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "CallLogUtil no missed calls"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method public static getLastNMissedNewCalls(Landroid/content/Context;I)Ljava/util/Vector;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/Vector",
            "<",
            "Lcom/nuance/sample/util/calllog/LoggedCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    const/4 v8, 0x0

    .line 141
    .local v8, "c":Landroid/database/Cursor;
    new-instance v15, Ljava/util/Vector;

    invoke-direct {v15}, Ljava/util/Vector;-><init>()V

    .line 142
    .local v15, "missedCalls":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "name"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    .line 143
    const-string/jumbo v3, "number"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "date"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "new"

    aput-object v3, v4, v2

    .line 147
    .local v4, "projection":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 148
    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 150
    const-string/jumbo v5, "type = ? AND new = ?"

    .line 153
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 154
    const/16 v21, 0x3

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v6, v7

    const/4 v7, 0x1

    .line 155
    const-string/jumbo v21, "1"

    aput-object v21, v6, v7

    .line 156
    const-string/jumbo v7, "date DESC "

    .line 148
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 162
    invoke-static {v8}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 163
    const-string/jumbo v20, ""

    .line 164
    .local v20, "priorNumber":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 165
    const/4 v14, 0x0

    .line 166
    .local v14, "index":I
    const-string/jumbo v2, "name"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 167
    .local v17, "nameIdx":I
    const-string/jumbo v2, "date"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 168
    .local v10, "dateIdx":I
    const-string/jumbo v2, "number"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 170
    .local v19, "numberIdx":I
    :cond_0
    move/from16 v0, v17

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 171
    .local v16, "name":Ljava/lang/String;
    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 172
    .local v18, "number":Ljava/lang/String;
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 173
    .local v11, "dateMS":J
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "CallLogUtil found missed new call from "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 174
    const-string/jumbo v5, " on "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1, v11, v12}, Lcom/nuance/sample/util/calllog/LoggedCall;->newInstance(Ljava/lang/String;Ljava/lang/String;J)Lcom/nuance/sample/util/calllog/LoggedCall;

    move-result-object v9

    .line 177
    .local v9, "call":Lcom/nuance/sample/util/calllog/LoggedCall;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 178
    invoke-virtual {v15, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_1
    move-object/from16 v20, v18

    .line 181
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 169
    move/from16 v0, p1

    if-lt v14, v0, :cond_0

    .line 187
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 189
    .end local v9    # "call":Lcom/nuance/sample/util/calllog/LoggedCall;
    .end local v10    # "dateIdx":I
    .end local v11    # "dateMS":J
    .end local v14    # "index":I
    .end local v16    # "name":Ljava/lang/String;
    .end local v17    # "nameIdx":I
    .end local v18    # "number":Ljava/lang/String;
    .end local v19    # "numberIdx":I
    .end local v20    # "priorNumber":Ljava/lang/String;
    :cond_3
    :goto_0
    return-object v15

    .line 157
    :catch_0
    move-exception v13

    .line 158
    .local v13, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "CallLogUtil call data base query failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 183
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v20    # "priorNumber":Ljava/lang/String;
    :cond_4
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "CallLogUtil no missed new calls"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getLastNOutCalls(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/nuance/sample/util/calllog/LoggedCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    const/4 v8, 0x0

    .line 19
    .local v8, "c":Landroid/database/Cursor;
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 20
    .local v19, "outCalls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "name"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    .line 21
    const-string/jumbo v3, "number"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "date"

    aput-object v3, v4, v2

    .line 25
    .local v4, "projection":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 26
    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 28
    const-string/jumbo v5, "type = ?"

    .line 29
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 30
    const/16 v21, 0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v6, v7

    .line 31
    const-string/jumbo v7, "date DESC "

    .line 26
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 37
    invoke-static {v8}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 38
    const-string/jumbo v20, ""

    .line 39
    .local v20, "priorNumber":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 40
    const/4 v14, 0x0

    .line 41
    .local v14, "index":I
    const-string/jumbo v2, "name"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 42
    .local v15, "nameIdx":I
    const-string/jumbo v2, "date"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 43
    .local v10, "dateIdx":I
    const-string/jumbo v2, "number"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 45
    .local v18, "numberIdx":I
    :cond_0
    invoke-interface {v8, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 46
    .local v16, "nameOrAddress":Ljava/lang/String;
    move/from16 v0, v18

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 47
    .local v17, "number":Ljava/lang/String;
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 48
    .local v11, "dateMS":J
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "CallLogUtil found out call to "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 49
    const-string/jumbo v5, " on "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 48
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v11, v12}, Lcom/nuance/sample/util/calllog/LoggedCall;->newInstance(Ljava/lang/String;Ljava/lang/String;J)Lcom/nuance/sample/util/calllog/LoggedCall;

    move-result-object v9

    .line 52
    .local v9, "call":Lcom/nuance/sample/util/calllog/LoggedCall;
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 53
    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_1
    move-object/from16 v20, v17

    .line 56
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 44
    move/from16 v0, p1

    if-lt v14, v0, :cond_0

    .line 58
    .end local v9    # "call":Lcom/nuance/sample/util/calllog/LoggedCall;
    .end local v10    # "dateIdx":I
    .end local v11    # "dateMS":J
    .end local v14    # "index":I
    .end local v15    # "nameIdx":I
    .end local v16    # "nameOrAddress":Ljava/lang/String;
    .end local v17    # "number":Ljava/lang/String;
    .end local v18    # "numberIdx":I
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 60
    .end local v20    # "priorNumber":Ljava/lang/String;
    :cond_3
    :goto_0
    return-object v19

    .line 32
    :catch_0
    move-exception v13

    .line 33
    .local v13, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/nuance/sample/util/calllog/CallLogUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "CallLogUtil call data base query failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
