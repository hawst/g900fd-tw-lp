.class public Lcom/nuance/sample/util/calllog/LoggedCall;
.super Ljava/lang/Object;
.source "LoggedCall.java"

# interfaces
.implements Lcom/nuance/sample/util/ContactProvider;


# instance fields
.field private dateOfCall:J

.field private nameOfCaller:Ljava/lang/String;

.field private numberForCaller:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "dateMS"    # J

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->nameOfCaller:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->numberForCaller:Ljava/lang/String;

    .line 29
    iput-wide p3, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->dateOfCall:J

    .line 30
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;J)Lcom/nuance/sample/util/calllog/LoggedCall;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "dateMS"    # J

    .prologue
    .line 11
    new-instance v0, Lcom/nuance/sample/util/calllog/LoggedCall;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/nuance/sample/util/calllog/LoggedCall;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0
.end method


# virtual methods
.method public getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->nameOfCaller:Ljava/lang/String;

    return-object v0
.end method

.method public getContactNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->numberForCaller:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->dateOfCall:J

    return-wide v0
.end method

.method public setNumberForCaller(Ljava/lang/String;)V
    .locals 0
    .param p1, "numberForCaller"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->numberForCaller:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->dateOfCall:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->numberForCaller:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/util/calllog/LoggedCall;->nameOfCaller:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
