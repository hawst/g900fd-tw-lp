.class public Lcom/nuance/sample/util/message/Message;
.super Ljava/lang/Object;
.source "Message.java"

# interfaces
.implements Lcom/nuance/sample/util/ContactProvider;


# instance fields
.field private body:Ljava/lang/String;

.field private contactId:Ljava/lang/Long;

.field private contactName:Ljava/lang/String;

.field private contactNumber:Ljava/lang/String;

.field private msgId:Ljava/lang/Long;

.field private subject:Ljava/lang/String;

.field private timestamp:Ljava/lang/Long;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getContactId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->contactId:Ljava/lang/Long;

    return-object v0
.end method

.method public getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->contactName:Ljava/lang/String;

    return-object v0
.end method

.method public getContactNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->contactNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getMsgId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->msgId:Ljava/lang/Long;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->timestamp:Ljava/lang/Long;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/nuance/sample/util/message/Message;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->body:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setContactId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "contactId"    # Ljava/lang/Long;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->contactId:Ljava/lang/Long;

    .line 21
    return-void
.end method

.method public setContactName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->contactName:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setContactNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->contactNumber:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setMsgId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "msgId"    # Ljava/lang/Long;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->msgId:Ljava/lang/Long;

    .line 45
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->subject:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setTimestamp(Ljava/lang/Long;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/Long;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->timestamp:Ljava/lang/Long;

    .line 79
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/nuance/sample/util/message/Message;->type:Ljava/lang/String;

    .line 71
    return-void
.end method
