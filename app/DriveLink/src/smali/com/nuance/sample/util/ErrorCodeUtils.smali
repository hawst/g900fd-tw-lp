.class public Lcom/nuance/sample/util/ErrorCodeUtils;
.super Ljava/lang/Object;
.source "ErrorCodeUtils.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionErrors:[I

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionErrors()[I
    .locals 3

    .prologue
    .line 14
    sget-object v0, Lcom/nuance/sample/util/ErrorCodeUtils;->$SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionErrors:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->values()[Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_AUDIO:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_CLIENT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_INSUFFICENT_PERMISSIONS:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_RECOGNIZER_BUSY:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SERVER:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lcom/nuance/sample/util/ErrorCodeUtils;->$SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionErrors:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/nuance/sample/util/ErrorCodeUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/util/ErrorCodeUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLocalizedMessageForErrorCode(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Landroid/content/Context;)Lcom/nuance/sample/util/ErrorCode;
    .locals 5
    .param p0, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    invoke-static {}, Lcom/nuance/sample/util/ErrorCodeUtils;->$SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionErrors()[I

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 50
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04df

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v4}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 52
    .local v0, "result":Lcom/nuance/sample/util/ErrorCode;
    :goto_0
    return-object v0

    .line 23
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_0
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e0

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 24
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 26
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_1
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e2

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v4}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 27
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 29
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_2
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a00bd

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v4}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 30
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 32
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_3
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e4

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 33
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 35
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_4
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e5

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 36
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 38
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_5
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e6

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 39
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 41
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_6
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e7

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 42
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 44
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_7
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e8

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 45
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 47
    .end local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    :pswitch_8
    new-instance v0, Lcom/nuance/sample/util/ErrorCode;

    const v1, 0x7f0a04e1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/nuance/sample/util/ErrorCode;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V

    .line 48
    .restart local v0    # "result":Lcom/nuance/sample/util/ErrorCode;
    goto :goto_0

    .line 21
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static shouldReportError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)Z
    .locals 3
    .param p0, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .prologue
    const/4 v0, 0x0

    .line 56
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    if-ne v1, p0, :cond_0

    .line 58
    sget-object v1, Lcom/nuance/sample/util/ErrorCodeUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "EPD got VLRecognitionErrors.ERROR_SPEECH_TIMEOUT"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :goto_0
    return v0

    .line 61
    :cond_0
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    if-ne v1, p0, :cond_1

    .line 63
    sget-object v1, Lcom/nuance/sample/util/ErrorCodeUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "EPD got VLRecognitionErrors.ERROR_NO_MATCH"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 66
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    if-ne v1, p0, :cond_2

    .line 67
    sget-object v1, Lcom/nuance/sample/util/ErrorCodeUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "EPD got VLRecognitionErrors.ERROR_NETWORK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 70
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
