.class public Lcom/nuance/sample/util/ErrorCode;
.super Ljava/lang/Object;
.source "ErrorCode.java"


# instance fields
.field private mErrorId:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

.field private mErrorString:Ljava/lang/String;

.field private mReportByText:Z


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "errorId"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "errorString"    # Ljava/lang/String;
    .param p3, "reportByText"    # Z

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/nuance/sample/util/ErrorCode;->mErrorId:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 20
    iput-object p2, p0, Lcom/nuance/sample/util/ErrorCode;->mErrorString:Ljava/lang/String;

    .line 21
    iput-boolean p3, p0, Lcom/nuance/sample/util/ErrorCode;->mReportByText:Z

    .line 22
    return-void
.end method


# virtual methods
.method public getErrorId()Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/nuance/sample/util/ErrorCode;->mErrorId:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    return-object v0
.end method

.method public getErrorString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/nuance/sample/util/ErrorCode;->mErrorString:Ljava/lang/String;

    return-object v0
.end method

.method public isReportByText()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/nuance/sample/util/ErrorCode;->mReportByText:Z

    return v0
.end method

.method public setErrorId(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)V
    .locals 0
    .param p1, "mErrorId"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/nuance/sample/util/ErrorCode;->mErrorId:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 30
    return-void
.end method

.method public setErrorString(Ljava/lang/String;)V
    .locals 0
    .param p1, "mErrorString"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/nuance/sample/util/ErrorCode;->mErrorString:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setReportByText(Z)V
    .locals 0
    .param p1, "mReportByText"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/nuance/sample/util/ErrorCode;->mReportByText:Z

    .line 46
    return-void
.end method
