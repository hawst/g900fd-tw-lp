.class public Lcom/nuance/sample/util/message/MessageUtil;
.super Ljava/lang/Object;
.source "MessageUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLastNInMessages(Landroid/content/Context;I)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/util/message/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    const/4 v3, 0x1

    invoke-static {p0, v3, p1}, Lcom/vlingo/core/internal/util/SMSUtil;->getRecentMessagesFromInbox(Landroid/content/Context;ZI)Ljava/util/ArrayList;

    move-result-object v1

    .line 16
    .local v1, "recentMessagesFromInbox":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .local v0, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/util/message/Message;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 20
    return-object v0

    .line 17
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    .line 18
    .local v2, "textMessage":Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    invoke-static {v2}, Lcom/nuance/sample/util/message/MessageUtil;->transformMessage(Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;)Lcom/nuance/sample/util/message/Message;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getLastNOutMessages(Landroid/content/Context;I)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/util/message/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    const/4 v3, 0x1

    invoke-static {p0, v3, p1}, Lcom/vlingo/core/internal/util/SMSUtil;->getRecentMessagesFromInbox(Landroid/content/Context;ZI)Ljava/util/ArrayList;

    move-result-object v1

    .line 26
    .local v1, "recentMessagesFromInbox":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 27
    .local v0, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/util/message/Message;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 30
    return-object v0

    .line 27
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    .line 28
    .local v2, "textMessage":Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    invoke-static {v2}, Lcom/nuance/sample/util/message/MessageUtil;->transformMessage(Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;)Lcom/nuance/sample/util/message/Message;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected static transformMessage(Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;)Lcom/nuance/sample/util/message/Message;
    .locals 3
    .param p0, "textMessage"    # Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    .prologue
    .line 34
    new-instance v0, Lcom/nuance/sample/util/message/Message;

    invoke-direct {v0}, Lcom/nuance/sample/util/message/Message;-><init>()V

    .line 35
    .local v0, "message":Lcom/nuance/sample/util/message/Message;
    iget-wide v1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->contactId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setContactId(Ljava/lang/Long;)V

    .line 36
    iget-object v1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setContactName(Ljava/lang/String;)V

    .line 37
    iget-object v1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setContactNumber(Ljava/lang/String;)V

    .line 38
    iget-object v1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setBody(Ljava/lang/String;)V

    .line 39
    iget-wide v1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setMsgId(Ljava/lang/Long;)V

    .line 40
    iget-object v1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setSubject(Ljava/lang/String;)V

    .line 41
    iget-wide v1, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->date:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setTimestamp(Ljava/lang/Long;)V

    .line 42
    const-string/jumbo v1, "1"

    iget-object v2, p0, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const-string/jumbo v1, "SMS"

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setType(Ljava/lang/String;)V

    .line 47
    :goto_0
    return-object v0

    .line 45
    :cond_0
    const-string/jumbo v1, "MMS"

    invoke-virtual {v0, v1}, Lcom/nuance/sample/util/message/Message;->setType(Ljava/lang/String;)V

    goto :goto_0
.end method
