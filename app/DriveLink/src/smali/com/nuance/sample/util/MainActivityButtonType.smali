.class public final enum Lcom/nuance/sample/util/MainActivityButtonType;
.super Ljava/lang/Enum;
.source "MainActivityButtonType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/util/MainActivityButtonType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/util/MainActivityButtonType;

.field public static final enum LOCATION:Lcom/nuance/sample/util/MainActivityButtonType;

.field public static final enum MESSAGE:Lcom/nuance/sample/util/MainActivityButtonType;

.field public static final enum MUSIC:Lcom/nuance/sample/util/MainActivityButtonType;

.field public static final enum PHONE:Lcom/nuance/sample/util/MainActivityButtonType;


# instance fields
.field private fieldId:Lcom/nuance/sample/CCFieldIds;

.field private nextActivityClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/nuance/sample/domains/ScreenActivity;",
            ">;"
        }
    .end annotation
.end field

.field private promptId:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/nuance/sample/util/MainActivityButtonType;

    const-string/jumbo v1, "PHONE"

    sget-object v3, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    const v4, 0x7f0a00b9

    .line 18
    const-class v5, Lcom/nuance/sample/domains/phone/PhoneSuggestionsActivity;

    invoke-direct/range {v0 .. v5}, Lcom/nuance/sample/util/MainActivityButtonType;-><init>(Ljava/lang/String;ILcom/nuance/sample/CCFieldIds;ILjava/lang/Class;)V

    .line 17
    sput-object v0, Lcom/nuance/sample/util/MainActivityButtonType;->PHONE:Lcom/nuance/sample/util/MainActivityButtonType;

    .line 18
    new-instance v3, Lcom/nuance/sample/util/MainActivityButtonType;

    const-string/jumbo v4, "MESSAGE"

    sget-object v6, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    .line 19
    const v7, 0x7f0a0026

    const-class v8, Lcom/nuance/sample/domains/message/MessageSuggestionsActivity;

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/nuance/sample/util/MainActivityButtonType;-><init>(Ljava/lang/String;ILcom/nuance/sample/CCFieldIds;ILjava/lang/Class;)V

    .line 18
    sput-object v3, Lcom/nuance/sample/util/MainActivityButtonType;->MESSAGE:Lcom/nuance/sample/util/MainActivityButtonType;

    .line 19
    new-instance v3, Lcom/nuance/sample/util/MainActivityButtonType;

    const-string/jumbo v4, "MUSIC"

    .line 20
    sget-object v6, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_HOME:Lcom/nuance/sample/CCFieldIds;

    const v7, 0x7f0a060f

    .line 21
    const-class v8, Lcom/nuance/sample/domains/music/MusicMainActivity;

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/nuance/sample/util/MainActivityButtonType;-><init>(Ljava/lang/String;ILcom/nuance/sample/CCFieldIds;ILjava/lang/Class;)V

    .line 19
    sput-object v3, Lcom/nuance/sample/util/MainActivityButtonType;->MUSIC:Lcom/nuance/sample/util/MainActivityButtonType;

    .line 21
    new-instance v3, Lcom/nuance/sample/util/MainActivityButtonType;

    const-string/jumbo v4, "LOCATION"

    sget-object v6, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_HOME:Lcom/nuance/sample/CCFieldIds;

    .line 22
    const v7, 0x7f0a066e

    const-class v8, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/nuance/sample/util/MainActivityButtonType;-><init>(Ljava/lang/String;ILcom/nuance/sample/CCFieldIds;ILjava/lang/Class;)V

    .line 21
    sput-object v3, Lcom/nuance/sample/util/MainActivityButtonType;->LOCATION:Lcom/nuance/sample/util/MainActivityButtonType;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/nuance/sample/util/MainActivityButtonType;

    sget-object v1, Lcom/nuance/sample/util/MainActivityButtonType;->PHONE:Lcom/nuance/sample/util/MainActivityButtonType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/nuance/sample/util/MainActivityButtonType;->MESSAGE:Lcom/nuance/sample/util/MainActivityButtonType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/nuance/sample/util/MainActivityButtonType;->MUSIC:Lcom/nuance/sample/util/MainActivityButtonType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/nuance/sample/util/MainActivityButtonType;->LOCATION:Lcom/nuance/sample/util/MainActivityButtonType;

    aput-object v1, v0, v11

    sput-object v0, Lcom/nuance/sample/util/MainActivityButtonType;->ENUM$VALUES:[Lcom/nuance/sample/util/MainActivityButtonType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/nuance/sample/CCFieldIds;ILjava/lang/Class;)V
    .locals 0
    .param p3, "fieldId"    # Lcom/nuance/sample/CCFieldIds;
    .param p4, "promptId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/sample/CCFieldIds;",
            "I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nuance/sample/domains/ScreenActivity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p5, "nextActivityClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/nuance/sample/domains/ScreenActivity;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput-object p3, p0, Lcom/nuance/sample/util/MainActivityButtonType;->fieldId:Lcom/nuance/sample/CCFieldIds;

    .line 31
    iput p4, p0, Lcom/nuance/sample/util/MainActivityButtonType;->promptId:I

    .line 32
    iput-object p5, p0, Lcom/nuance/sample/util/MainActivityButtonType;->nextActivityClass:Ljava/lang/Class;

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/util/MainActivityButtonType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/util/MainActivityButtonType;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/util/MainActivityButtonType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/util/MainActivityButtonType;->ENUM$VALUES:[Lcom/nuance/sample/util/MainActivityButtonType;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/nuance/sample/util/MainActivityButtonType;->fieldId:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method public getNextActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nuance/sample/domains/ScreenActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/nuance/sample/util/MainActivityButtonType;->nextActivityClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getPromptId()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/nuance/sample/util/MainActivityButtonType;->promptId:I

    return v0
.end method
