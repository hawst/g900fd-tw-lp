.class public Lcom/nuance/sample/services/SampleServicesUtil;
.super Lcom/vlingo/midas/services/ServicesUtil;
.source "SampleServicesUtil.java"


# static fields
.field private static smInstance:Lcom/nuance/sample/services/SampleServicesUtil;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/midas/services/ServicesUtil;-><init>()V

    .line 18
    return-void
.end method

.method public static getInstance()Lcom/nuance/sample/services/SampleServicesUtil;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/nuance/sample/services/SampleServicesUtil;->smInstance:Lcom/nuance/sample/services/SampleServicesUtil;

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Lcom/nuance/sample/services/SampleServicesUtil;

    invoke-direct {v0}, Lcom/nuance/sample/services/SampleServicesUtil;-><init>()V

    sput-object v0, Lcom/nuance/sample/services/SampleServicesUtil;->smInstance:Lcom/nuance/sample/services/SampleServicesUtil;

    .line 14
    :cond_0
    sget-object v0, Lcom/nuance/sample/services/SampleServicesUtil;->smInstance:Lcom/nuance/sample/services/SampleServicesUtil;

    return-object v0
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
