.class public final enum Lcom/nuance/sample/CCFieldIds;
.super Ljava/lang/Enum;
.source "CCFieldIds.java"

# interfaces
.implements Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/CCFieldIds;",
        ">;",
        "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;"
    }
.end annotation


# static fields
.field public static final enum CC_DIAL_CONFIRM:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_DIAL_TYPE:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_LOCATION_HOME:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_LOCATION_TYPE:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_MAIN:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_MUSIC_HOME:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_MUSIC_PLAY_FOLDER:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_MUSIC_PLAY_TITLE_CHOICE:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_SMS_TYPE:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_VP_CAR_READBACK_MSGCHOOSE:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_VP_CAR_READBACK_SENDERCHOOSE:Lcom/nuance/sample/CCFieldIds;

.field public static final enum CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/CCFieldIds;

.field public static final enum VP_CAR_MAIN_LAUNCHAPPNAME:Lcom/nuance/sample/CCFieldIds;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_MAIN"

    const-string/jumbo v2, "cc_dm_home"

    invoke-direct {v0, v1, v4, v2}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    .line 12
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_SMS_CONTACT"

    .line 13
    const-string/jumbo v2, "cc_dm_sms_home"

    invoke-direct {v0, v1, v5, v2}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_SMS_MSG"

    const-string/jumbo v2, "dm_sms_msg"

    invoke-direct {v0, v1, v6, v2}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_SMS_MAIN"

    .line 14
    const-string/jumbo v2, "cc_dm_sms_confirm"

    invoke-direct {v0, v1, v7, v2}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_SMS_TYPE"

    const-string/jumbo v2, "cc_dm_sms_type"

    invoke-direct {v0, v1, v8, v2}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_TYPE:Lcom/nuance/sample/CCFieldIds;

    .line 16
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_DIAL_CONTACT"

    const/4 v2, 0x5

    .line 17
    const-string/jumbo v3, "cc_dm_dial_home"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_DIAL_TYPE"

    const/4 v2, 0x6

    const-string/jumbo v3, "cc_dm_dial_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_TYPE:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_DIAL_CONFIRM"

    const/4 v2, 0x7

    .line 18
    const-string/jumbo v3, "cc_dm_dial_confirm"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 20
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_LOCATION_HOME"

    const/16 v2, 0x8

    .line 21
    const-string/jumbo v3, "cc_dm_location_home"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_HOME:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_LOCATION_CONTACT"

    const/16 v2, 0x9

    .line 22
    const-string/jumbo v3, "cc_dm_location_contact_disambig"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_LOCATION_ROUTE_CONFIRM"

    const/16 v2, 0xa

    .line 23
    const-string/jumbo v3, "cc_dm_location_route_confirm"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_LOCATION_REQUEST_CONFIRM"

    const/16 v2, 0xb

    .line 24
    const-string/jumbo v3, "cc_dm_location_request_confirm"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_LOCATION_SHARE_CONFIRM"

    const/16 v2, 0xc

    .line 25
    const-string/jumbo v3, "cc_dm_location_share_confirm"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_LOCATION_TYPE"

    const/16 v2, 0xd

    .line 26
    const-string/jumbo v3, "cc_dm_location_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_TYPE:Lcom/nuance/sample/CCFieldIds;

    .line 28
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_MUSIC_HOME"

    const/16 v2, 0xe

    .line 29
    const-string/jumbo v3, "cc_vp_car_music_home"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_HOME:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_MUSIC_PLAY_FOLDER"

    const/16 v2, 0xf

    .line 30
    const-string/jumbo v3, "cc_vp_car_music_playfolderchoice"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_PLAY_FOLDER:Lcom/nuance/sample/CCFieldIds;

    .line 31
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_MUSIC_PLAY_TITLE_CHOICE"

    const/16 v2, 0x10

    .line 33
    const-string/jumbo v3, "vp_car_music_playtitlechoice"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_PLAY_TITLE_CHOICE:Lcom/nuance/sample/CCFieldIds;

    .line 34
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_VP_CAR_SAFEREADER_READBACKMSG"

    const/16 v2, 0x11

    .line 35
    const-string/jumbo v3, "vp_car_safereader_readbackmsg"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_VP_CAR_READBACK_SENDERCHOOSE"

    const/16 v2, 0x12

    .line 36
    const-string/jumbo v3, "vp_car_readback_senderchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_SENDERCHOOSE:Lcom/nuance/sample/CCFieldIds;

    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "CC_VP_CAR_READBACK_MSGCHOOSE"

    const/16 v2, 0x13

    .line 37
    const-string/jumbo v3, "vp_car_readback_msgchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_MSGCHOOSE:Lcom/nuance/sample/CCFieldIds;

    .line 39
    new-instance v0, Lcom/nuance/sample/CCFieldIds;

    const-string/jumbo v1, "VP_CAR_MAIN_LAUNCHAPPNAME"

    const/16 v2, 0x14

    const-string/jumbo v3, "vp_car_main_launchappname"

    invoke-direct {v0, v1, v2, v3}, Lcom/nuance/sample/CCFieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->VP_CAR_MAIN_LAUNCHAPPNAME:Lcom/nuance/sample/CCFieldIds;

    .line 9
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/nuance/sample/CCFieldIds;

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    aput-object v1, v0, v4

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    aput-object v1, v0, v5

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    aput-object v1, v0, v6

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

    aput-object v1, v0, v7

    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_TYPE:Lcom/nuance/sample/CCFieldIds;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_TYPE:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_HOME:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_TYPE:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_HOME:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_PLAY_FOLDER:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_MUSIC_PLAY_TITLE_CHOICE:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_SENDERCHOOSE:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_READBACK_MSGCHOOSE:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/nuance/sample/CCFieldIds;->VP_CAR_MAIN_LAUNCHAPPNAME:Lcom/nuance/sample/CCFieldIds;

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/sample/CCFieldIds;->ENUM$VALUES:[Lcom/nuance/sample/CCFieldIds;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput-object p3, p0, Lcom/nuance/sample/CCFieldIds;->value:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public static generateFieldIdsMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    new-instance v0, Ljava/util/HashMap;

    .line 62
    const/16 v1, 0x1e

    .line 61
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 64
    .local v0, "mFieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MAIN:Lcom/nuance/sample/CCFieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 68
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 67
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_ROUTE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 70
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 69
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_REQUEST_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 72
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 71
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_SHARE_CONFIRM:Lcom/nuance/sample/CCFieldIds;

    .line 74
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 73
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_MSG:Lcom/nuance/sample/CCFieldIds;

    .line 77
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 76
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    .line 81
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    .line 80
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/CCFieldIds;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/CCFieldIds;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->ENUM$VALUES:[Lcom/nuance/sample/CCFieldIds;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/CCFieldIds;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/nuance/sample/CCFieldIds;->value:Ljava/lang/String;

    return-object v0
.end method
