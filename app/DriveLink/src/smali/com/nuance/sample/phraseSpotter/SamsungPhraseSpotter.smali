.class public Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;
.super Ljava/lang/Object;
.source "SamsungPhraseSpotter.java"

# interfaces
.implements Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;


# static fields
.field private static final SAMSUNG_CHUNK_LENGTH_MS:I = 0xa

.field private static final SAMSUNG_SEAMLESS_TIMEOUT_MS:I = 0x190

.field public static UDTAlwaysAPrecog:Ljava/lang/String;

.field public static UDTAlwaysAPsearch:Ljava/lang/String;


# instance fields
.field private final NUM_MODELS:I

.field private VElib:Lcom/samsung/voiceshell/VoiceEngine;

.field public consoleInitReturn:J

.field public consoleResult:Ljava/lang/String;

.field public isSensoryUDTSIDamFileExist:Z

.field public isSensoryUDTSIDsoFileExist:Z

.field private final log:Lcom/vlingo/core/facade/logging/ILogger;

.field public mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

.field private final sensoryUDTSIDSoFilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    sput-object v0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->UDTAlwaysAPrecog:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    sput-object v0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->UDTAlwaysAPsearch:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string/jumbo v0, "/system/lib/libSensoryUDTSIDEngine.so"

    iput-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->sensoryUDTSIDSoFilePath:Ljava/lang/String;

    .line 35
    iput-boolean v1, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    .line 36
    iput-boolean v1, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDamFileExist:Z

    .line 38
    iput-object v2, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleInitReturn:J

    .line 40
    iput-object v2, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    .line 43
    const-class v0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 45
    iput-object v2, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    .line 46
    const/4 v0, 0x5

    iput v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->NUM_MODELS:I

    .line 49
    return-void
.end method

.method public static getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 1

    .prologue
    .line 200
    invoke-static {}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->chooseAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v0

    .line 199
    invoke-static {v0}, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->getPhraseSpotterParameters(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    return-object v0
.end method

.method public static getPhraseSpotterParameters(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 5
    .param p0, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 209
    const-string/jumbo v3, "samsung_wakeup_engine_enable"

    const/4 v4, 0x0

    .line 208
    invoke-static {v3, v4}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 209
    if-nez v3, :cond_0

    .line 211
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v3

    .line 212
    invoke-interface {v3}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 211
    invoke-static {v3}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getWakeupCoreParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v1

    .line 213
    .local v1, "coreParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 214
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    .line 213
    invoke-direct {v0, v3, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V

    .line 219
    .end local v1    # "coreParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    .local v0, "builder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    :goto_0
    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setChunkLength(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 220
    const/16 v3, 0x190

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setSeamlessTimeout(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 221
    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setAudioSourceType(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 222
    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->build()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    .line 223
    .local v2, "samsungSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    return-object v2

    .line 216
    .end local v0    # "builder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .end local v2    # "samsungSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 217
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    .line 216
    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>(Ljava/lang/String;)V

    .restart local v0    # "builder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 106
    iget-boolean v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    if-eqz v0, :cond_1

    .line 108
    iget-wide v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleInitReturn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    iget-wide v1, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleInitReturn:J

    invoke-virtual {v0, v1, v2}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotClose(J)V

    .line 111
    :cond_0
    iput-object v4, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 119
    :cond_1
    :goto_0
    return-void

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    if-eqz v0, :cond_3

    .line 115
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v0}, Lcom/samsung/voiceshell/VoiceEngine;->terminateVerify()I

    .line 117
    :cond_3
    iput-object v4, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    goto :goto_0
.end method

.method public getDeltaD()I
    .locals 1

    .prologue
    .line 190
    const/16 v0, -0xc8

    return v0
.end method

.method public getDeltaS()I
    .locals 1

    .prologue
    .line 195
    const/16 v0, 0x32

    return v0
.end method

.method public getSpottedPhraseScore()F
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public init()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "NumSets":I
    const/4 v3, 0x5

    new-array v2, v3, [I

    .line 55
    .local v2, "mExist":[I
    const/4 v1, 0x2

    .line 57
    .local v1, "WType":I
    const-string/jumbo v3, "/system/lib/libSensoryUDTSIDEngine.so"

    invoke-virtual {p0, v3}, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isFileExist(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    .line 59
    sget-object v3, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->UDTAlwaysAPrecog:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isFileExist(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    sget-object v3, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->UDTAlwaysAPsearch:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isFileExist(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    .line 59
    :goto_0
    iput-boolean v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDamFileExist:Z

    .line 62
    iget-boolean v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v3, :cond_2

    .line 63
    invoke-static {}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngineWrapper;->getInstance()Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 66
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v4, "secMM: Sensory VoiceEngine intiated in PhraseSpotterControl!!"

    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 68
    iget-boolean v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDamFileExist:Z

    if-eqz v3, :cond_0

    .line 69
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 70
    sget-object v4, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->UDTAlwaysAPrecog:Ljava/lang/String;

    sget-object v5, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->UDTAlwaysAPsearch:Ljava/lang/String;

    .line 69
    invoke-virtual {v3, v4, v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotInit(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleInitReturn:J

    .line 101
    :cond_0
    :goto_1
    return-void

    .line 60
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 74
    :cond_2
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    .line 77
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "secMM: VoiceEngine intiated in PhraseSpotterControl!!"

    invoke-interface {v3, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 80
    const/4 v1, 0x3

    .line 84
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    sget-object v5, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    invoke-virtual {v3, v5, v1, v2}, Lcom/samsung/voiceshell/VoiceEngine;->checkFileExistence(Ljava/lang/String;I[I)I

    move-result v0

    .line 86
    if-eqz v0, :cond_0

    .line 88
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v5, "secMM: Models exist .... initiate verify!"

    invoke-interface {v3, v5}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 90
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3}, Lcom/samsung/voiceshell/VoiceEngine;->terminateVerify()I

    .line 91
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v5, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v5, v5, Lcom/samsung/voiceshell/VoiceEngine;->m_UBMpath_default:Ljava/lang/String;

    sget-object v6, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    invoke-virtual {v3, v5, v6, v1}, Lcom/samsung/voiceshell/VoiceEngine;->initializeVerify(Ljava/lang/String;Ljava/lang/String;I)I

    .line 94
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3, v4}, Lcom/samsung/voiceshell/VoiceEngine;->setMode(I)V

    .line 95
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v4, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v4, v4, Lcom/samsung/voiceshell/VoiceEngine;->m_UBMpath_default:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/voiceshell/VoiceEngine;->setAdaptationModelPath(Ljava/lang/String;)V

    .line 97
    iget-object v3, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3}, Lcom/samsung/voiceshell/VoiceEngine;->startVerify()I

    goto :goto_1
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 232
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 233
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    const/4 v1, 0x1

    .line 236
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public phrasespotPipe(Ljava/nio/ByteBuffer;J)Ljava/lang/String;
    .locals 1
    .param p1, "b"    # Ljava/nio/ByteBuffer;
    .param p2, "rate"    # J

    .prologue
    .line 228
    const/4 v0, 0x0

    return-object v0
.end method

.method public processShortArray([SII)Ljava/lang/String;
    .locals 12
    .param p1, "audioData"    # [S
    .param p2, "offset"    # I
    .param p3, "size"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    .line 123
    new-array v5, v11, [S

    .line 124
    .local v5, "CommandType":[S
    new-array v4, v11, [S

    .line 125
    .local v4, "frameLeftNumber":[S
    int-to-short v0, p2

    aput-short v0, v4, v6

    .line 127
    iget-boolean v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    if-eqz v0, :cond_0

    .line 129
    iget-wide v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleInitReturn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 131
    iget-wide v1, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleInitReturn:J

    int-to-long v4, p3

    const-wide/16 v6, 0x3e80

    move-object v3, p1

    .line 130
    invoke-virtual/range {v0 .. v7}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotPipe(J[SJJ)Ljava/lang/String;

    .end local v4    # "frameLeftNumber":[S
    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 136
    iput-object v10, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    .line 137
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v10

    .line 139
    goto :goto_0

    .line 143
    .restart local v4    # "frameLeftNumber":[S
    :cond_2
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    sget-object v3, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    move-object v1, p1

    move v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/voiceshell/VoiceEngine;->processBuffer([SILjava/lang/String;[S[S)I

    move-result v9

    .line 147
    .local v9, "iResult":I
    if-ne v9, v11, :cond_3

    .line 148
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/voiceshell/VoiceEngine;->setMode(I)V

    .line 151
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v1, "secMM: Adaptation start"

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    .line 153
    iget-object v1, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v1, v1, Lcom/samsung/voiceshell/VoiceEngine;->m_UBMpath_default:Ljava/lang/String;

    sget-object v2, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    .line 152
    invoke-virtual {v0, v1, v2}, Lcom/samsung/voiceshell/VoiceEngine;->performContinuousAdaptation(Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 155
    .local v8, "contADAPTreturn":I
    iget-object v0, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: Adaptation end contADAPTreturn : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 157
    aget-short v0, v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .end local v8    # "contADAPTreturn":I
    :cond_3
    move-object v0, v10

    .line 159
    goto :goto_0
.end method

.method public useSeamlessFeature(Ljava/lang/String;)Z
    .locals 4
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    const-string/jumbo v3, "seamless_wakeup"

    invoke-static {v3, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 173
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 174
    .local v0, "iResult":I
    if-ne v0, v1, :cond_0

    .line 178
    iget-object v2, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v3, "secMM: Customized KWD spotted"

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 179
    iget-object v2, p0, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/voiceshell/VoiceEngine;->setMode(I)V

    .line 185
    .end local v0    # "iResult":I
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method
