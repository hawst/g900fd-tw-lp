.class public Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;
.super Ljava/lang/Object;
.source "PhraseSpotterUtil.java"


# static fields
.field private static final CG_DIR:Ljava/io/File;

.field private static final DEFAULT_DELTA_D:I = 0x0

.field private static final DEFAULT_DELTA_S:I = 0x32

.field public static final SEAMLESS_TIMEOUT_MS:I = 0x190

.field private static final WAKE_UP_FILES_EXTERNAL_STORAGE:Ljava/lang/String; = "/system/wakeupdata/sensory"

.field private static final log:Lcom/vlingo/core/facade/logging/ILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const-class v0, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    .line 28
    sput-object v0, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 31
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 33
    const-string/jumbo v1, "phrasespotter"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 31
    sput-object v0, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->CG_DIR:Ljava/io/File;

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static chooseAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->SPOTTER_REGULAR:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    return-object v0
.end method

.method private static createFileIfNecessary(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "cgResName"    # Ljava/lang/String;

    .prologue
    .line 111
    sget-object v10, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "createFileIfNecessary() cgResName="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v10

    .line 114
    invoke-interface {v10}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 116
    .local v2, "context":Landroid/content/Context;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v11, ".raw"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "cgFileName":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    sget-object v10, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->CG_DIR:Ljava/io/File;

    invoke-direct {v4, v10, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 118
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 119
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_0

    .line 121
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    .line 164
    :goto_0
    return-object v10

    .line 124
    :cond_0
    sget-object v10, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Deleting 0-length file: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 130
    :cond_1
    const-string/jumbo v10, "raw"

    .line 131
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 130
    invoke-virtual {p0, p1, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 132
    .local v9, "resID":I
    if-nez v9, :cond_2

    .line 134
    sget-object v10, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Error getting resource id for CG resource name: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 135
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 134
    invoke-interface {v10, v11}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 136
    const/4 v10, 0x0

    goto :goto_0

    .line 139
    :cond_2
    invoke-virtual {p0, v9}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v7

    .line 140
    .local v7, "is":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 142
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .local v6, "fos":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    :try_start_1
    new-array v0, v10, [B

    .line 145
    .local v0, "buffer":[B
    :goto_1
    invoke-virtual {v7, v0}, Ljava/io/InputStream;->read([B)I

    move-result v8

    .local v8, "length":I
    if-gtz v8, :cond_3

    .line 148
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 149
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 155
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    .line 160
    :goto_2
    :try_start_3
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 161
    :catch_0
    move-exception v11

    goto :goto_0

    .line 146
    :cond_3
    const/4 v10, 0x0

    :try_start_4
    invoke-virtual {v6, v0, v10, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 150
    .end local v0    # "buffer":[B
    .end local v8    # "length":I
    :catch_1
    move-exception v3

    move-object v5, v6

    .line 152
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v3, "e":Ljava/io/IOException;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :goto_3
    :try_start_5
    sget-object v10, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "Error writing CG file to data dir: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 155
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_5

    .line 160
    :goto_4
    :try_start_7
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 164
    :goto_5
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 153
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 155
    :goto_6
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_8

    .line 160
    :goto_7
    :try_start_9
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    .line 163
    :goto_8
    throw v10

    .line 156
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "length":I
    :catch_2
    move-exception v11

    goto :goto_2

    .line 157
    :catch_3
    move-exception v11

    goto :goto_2

    .line 156
    .end local v0    # "buffer":[B
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "length":I
    .restart local v3    # "e":Ljava/io/IOException;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v10

    goto :goto_4

    .line 157
    :catch_5
    move-exception v10

    goto :goto_4

    .line 161
    :catch_6
    move-exception v10

    goto :goto_5

    .line 156
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v11

    goto :goto_7

    .line 157
    :catch_8
    move-exception v11

    goto :goto_7

    .line 161
    :catch_9
    move-exception v11

    goto :goto_8

    .line 153
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 150
    :catch_a
    move-exception v3

    goto :goto_3
.end method

.method public static getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 48
    invoke-static {}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->chooseAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    return-object v0
.end method

.method public static getPhraseSpotterParameters(Landroid/content/res/Resources;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getWakeupCoreParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v0

    .line 56
    .local v0, "csParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 57
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-direct {v1, v2, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V

    .line 58
    .local v1, "pspBuilder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setSeamlessTimeout(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 59
    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setAudioSourceType(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 60
    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->build()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    return-object v2
.end method

.method public static getWakeupCoreParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    .locals 12
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 80
    .line 81
    const v5, 0x7f0a051b

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 82
    .local v9, "settingValue":Ljava/lang/String;
    invoke-static {p0, v9}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->createFileIfNecessary(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 85
    .local v6, "absoluteFilePath":Ljava/lang/String;
    sget-object v5, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Using CG file: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v10}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 87
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v8

    .line 88
    .local v8, "language":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    invoke-direct {v0, v8, v6}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .local v0, "cspBuilder":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;
    const-string/jumbo v5, "phrasespot_beam"

    .line 92
    const/high16 v10, 0x41a00000    # 20.0f

    .line 91
    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v1

    .line 93
    .local v1, "beam":F
    const-string/jumbo v5, "phrasespot_absbeam"

    .line 94
    const/high16 v10, 0x42200000    # 40.0f

    .line 93
    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v2

    .line 95
    .local v2, "absbeam":F
    const-string/jumbo v5, "phrasespot_aoffset"

    .line 96
    const/4 v10, 0x0

    .line 95
    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v3

    .line 97
    .local v3, "aoffset":F
    const-string/jumbo v5, "phrasespot_delay"

    .line 98
    const/high16 v10, 0x42c80000    # 100.0f

    .line 97
    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v4

    .line 101
    .local v4, "delay":F
    const-string/jumbo v5, "/system/wakeupdata/sensory"

    .line 100
    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->setSensoryParams(FFFFLjava/lang/String;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .line 102
    const/4 v5, 0x0

    const/16 v10, 0x32

    invoke-virtual {v0, v5, v10}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->setDeltas(II)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .line 104
    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->build()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v7

    .line 106
    .local v7, "csParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    return-object v7
.end method

.method public static isCustomWakeupWordPhrase(Ljava/lang/String;)Z
    .locals 2
    .param p0, "phrase"    # Ljava/lang/String;

    .prologue
    .line 65
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method
