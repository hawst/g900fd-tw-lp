.class Lcom/nuance/sample/SampleAppApplication$1;
.super Ljava/lang/Object;
.source "SampleAppApplication.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/SampleAppApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/SampleAppApplication;


# direct methods
.method constructor <init>(Lcom/nuance/sample/SampleAppApplication;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/SampleAppApplication$1;->this$0:Lcom/nuance/sample/SampleAppApplication;

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 127
    if-nez p2, :cond_0

    .line 138
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/nuance/sample/SampleAppApplication$1;->this$0:Lcom/nuance/sample/SampleAppApplication;

    # getter for: Lcom/nuance/sample/SampleAppApplication;->SAMPLE_APP_SERVER_KEYS:Ljava/util/List;
    invoke-static {v0}, Lcom/nuance/sample/SampleAppApplication;->access$0(Lcom/nuance/sample/SampleAppApplication;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    invoke-static {}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getInstance()Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->updateServerInfo(Lcom/vlingo/core/internal/util/CoreServerInfo;)V

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/nuance/sample/SampleAppApplication$1;->this$0:Lcom/nuance/sample/SampleAppApplication;

    invoke-virtual {v0}, Lcom/nuance/sample/SampleAppApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 135
    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 136
    const-string/jumbo v2, "SETTINGS/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 137
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 137
    const/4 v2, 0x0

    .line 133
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
