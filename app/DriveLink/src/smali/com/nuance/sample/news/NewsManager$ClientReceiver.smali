.class Lcom/nuance/sample/news/NewsManager$ClientReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NewsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/news/NewsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClientReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/news/NewsManager;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/news/NewsManager;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/nuance/sample/news/NewsManager$ClientReceiver;->this$0:Lcom/nuance/sample/news/NewsManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/news/NewsManager;Lcom/nuance/sample/news/NewsManager$ClientReceiver;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/nuance/sample/news/NewsManager$ClientReceiver;-><init>(Lcom/nuance/sample/news/NewsManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 149
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 151
    const-string/jumbo v1, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.YONHAPNEWS_DATE_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/nuance/sample/news/NewsManager$ClientReceiver;->this$0:Lcom/nuance/sample/news/NewsManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/nuance/sample/news/NewsManager;->access$6(Lcom/nuance/sample/news/NewsManager;Z)V

    .line 153
    const-string/jumbo v1, "NewsManager"

    .line 154
    const-string/jumbo v2, "Receive Yonhap Data sync"

    .line 153
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v1, p0, Lcom/nuance/sample/news/NewsManager$ClientReceiver;->this$0:Lcom/nuance/sample/news/NewsManager;

    # getter for: Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;
    invoke-static {v1}, Lcom/nuance/sample/news/NewsManager;->access$1(Lcom/nuance/sample/news/NewsManager;)Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/nuance/sample/news/NewsManager$ClientReceiver;->this$0:Lcom/nuance/sample/news/NewsManager;

    # getter for: Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;
    invoke-static {v1}, Lcom/nuance/sample/news/NewsManager;->access$1(Lcom/nuance/sample/news/NewsManager;)Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/nuance/sample/news/NewsManager$ClientReceiver;->this$0:Lcom/nuance/sample/news/NewsManager;

    # getter for: Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;
    invoke-static {v1}, Lcom/nuance/sample/news/NewsManager;->access$1(Lcom/nuance/sample/news/NewsManager;)Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->signal()V

    .line 159
    :cond_0
    return-void
.end method
