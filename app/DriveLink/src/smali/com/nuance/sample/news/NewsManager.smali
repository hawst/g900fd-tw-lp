.class public Lcom/nuance/sample/news/NewsManager;
.super Ljava/lang/Object;
.source "NewsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/news/NewsManager$ClientReceiver;,
        Lcom/nuance/sample/news/NewsManager$FetchNewsThread;
    }
.end annotation


# static fields
.field private static final Flip_SStoken:Ljava/lang/String; = "819b8dcb"

.field private static final NEWS:Ljava/lang/String; = "news"

.field private static final NewsDaemonPackageName:Ljava/lang/String; = "com.sec.android.daemonapp"

.field private static final PartialNewsDaemonPackageName:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews"

.field private static _instance:Lcom/nuance/sample/news/NewsManager;

.field private static mYonHapReceiver:Landroid/content/BroadcastReceiver;


# instance fields
.field private bUsingYonhapNews:Z

.field private currentIndex:I

.field private mCurrentLocale:Ljava/util/Locale;

.field private mError:Lflipboard/api/FlipManager$ErrorMessage;

.field private mFetchListener:Lflipboard/api/FlipFetchListener;

.field private final mManager:Lflipboard/api/FlipManager;

.field private mRequestedLocale:Ljava/util/Locale;

.field private mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

.field private final newsItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/news/NewsItem;",
            ">;"
        }
    .end annotation
.end field

.field private requestTypeFlags:I

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/sample/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    .line 38
    iput v1, p0, Lcom/nuance/sample/news/NewsManager;->currentIndex:I

    .line 40
    iput-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 41
    iput-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    .line 42
    sget-object v0, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mError:Lflipboard/api/FlipManager$ErrorMessage;

    .line 43
    iput v1, p0, Lcom/nuance/sample/news/NewsManager;->requestTypeFlags:I

    .line 44
    iput-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    .line 45
    iput-boolean v1, p0, Lcom/nuance/sample/news/NewsManager;->bUsingYonhapNews:Z

    .line 47
    new-instance v0, Lcom/nuance/sample/news/NewsManager$1;

    invoke-direct {v0, p0}, Lcom/nuance/sample/news/NewsManager$1;-><init>(Lcom/nuance/sample/news/NewsManager;)V

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mFetchListener:Lflipboard/api/FlipFetchListener;

    .line 168
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 168
    invoke-static {v0}, Lflipboard/api/FlipManager;->getInstance(Landroid/content/Context;)Lflipboard/api/FlipManager;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    .line 171
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/news/NewsManager;Lflipboard/api/FlipManager$ErrorMessage;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/nuance/sample/news/NewsManager;->mError:Lflipboard/api/FlipManager$ErrorMessage;

    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/news/NewsManager;)Lcom/nuance/sample/news/NewsManager$FetchNewsThread;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/news/NewsManager;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/nuance/sample/news/NewsManager;->requestTypeFlags:I

    return v0
.end method

.method static synthetic access$3(Lcom/nuance/sample/news/NewsManager;)V
    .locals 0

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->getFlipNewsData()V

    return-void
.end method

.method static synthetic access$4(Lcom/nuance/sample/news/NewsManager;)V
    .locals 0

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->getYonhapNewsData()V

    return-void
.end method

.method static synthetic access$5(Lcom/nuance/sample/news/NewsManager;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method static synthetic access$6(Lcom/nuance/sample/news/NewsManager;Z)V
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/nuance/sample/news/NewsManager;->bUsingYonhapNews:Z

    return-void
.end method

.method public static deinit()V
    .locals 2

    .prologue
    .line 272
    invoke-static {}, Lcom/nuance/sample/news/NewsManager;->isNewsDaemonAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/nuance/sample/news/NewsManager;->startYonhapNewsDaemon(Z)V

    .line 275
    sget-object v0, Lcom/nuance/sample/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 276
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 277
    sget-object v1, Lcom/nuance/sample/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 280
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/sample/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    .line 285
    :cond_1
    return-void
.end method

.method private declared-synchronized getFlipNewsData()V
    .locals 6

    .prologue
    .line 383
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    const-string/jumbo v3, "news"

    .line 384
    iget-object v4, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    sget-object v5, Lflipboard/api/FlipManager$ContentType;->TEXT:Lflipboard/api/FlipManager$ContentType;

    .line 383
    invoke-virtual {v2, v3, v4, v5}, Lflipboard/api/FlipManager;->getItemsForFeed(Ljava/lang/String;Ljava/util/Locale;Lflipboard/api/FlipManager$ContentType;)Ljava/util/List;

    move-result-object v1

    .line 386
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Lflipboard/api/FlipboardItem;>;"
    const-string/jumbo v2, "NewsManager-flipNews"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " refreshNewsData "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 387
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 386
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 390
    iget-object v2, p0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 391
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 395
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/nuance/sample/news/NewsManager;->updateCurrent(I)Lcom/nuance/sample/news/NewsItem;

    move-result-object v2

    if-nez v2, :cond_0

    .line 396
    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    :cond_0
    monitor-exit p0

    return-void

    .line 391
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/api/FlipboardItem;

    .line 392
    .local v0, "item":Lflipboard/api/FlipboardItem;
    iget-object v3, p0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    new-instance v4, Lcom/nuance/sample/news/NewsItem;

    invoke-direct {v4, v0}, Lcom/nuance/sample/news/NewsItem;-><init>(Lflipboard/api/FlipboardItem;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383
    .end local v0    # "item":Lflipboard/api/FlipboardItem;
    .end local v1    # "items":Ljava/util/List;, "Ljava/util/List<Lflipboard/api/FlipboardItem;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public static declared-synchronized getInstance()Lcom/nuance/sample/news/NewsManager;
    .locals 2

    .prologue
    .line 337
    const-class v1, Lcom/nuance/sample/news/NewsManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/nuance/sample/news/NewsManager;->_instance:Lcom/nuance/sample/news/NewsManager;

    if-nez v0, :cond_0

    .line 338
    new-instance v0, Lcom/nuance/sample/news/NewsManager;

    invoke-direct {v0}, Lcom/nuance/sample/news/NewsManager;-><init>()V

    sput-object v0, Lcom/nuance/sample/news/NewsManager;->_instance:Lcom/nuance/sample/news/NewsManager;

    .line 340
    :cond_0
    sget-object v0, Lcom/nuance/sample/news/NewsManager;->_instance:Lcom/nuance/sample/news/NewsManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getYonhapNewsData()V
    .locals 20

    .prologue
    .line 405
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 406
    .local v9, "ctx":Landroid/content/Context;
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    .line 407
    .local v19, "typeCategory":Ljava/lang/String;
    const/4 v10, 0x0

    .line 409
    .local v10, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 411
    if-eqz v9, :cond_1

    .line 412
    :try_start_0
    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    .line 413
    .local v16, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "NEWS_CATEGORY"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 414
    const-string/jumbo v1, " = ?"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 416
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 417
    sget-object v2, Lcom/nuance/sample/news/YonhapNewsDaemon$YonhapNewsColumns;->TABLE_URI:Landroid/net/Uri;

    .line 418
    sget-object v3, Lcom/nuance/sample/news/YonhapNewsDaemon$YonhapNewsColumns;->CONTENTS_COLS:[Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 419
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v19, v5, v6

    .line 420
    const-string/jumbo v6, "NEWS_CATEGORY ASC, NEWS_PUBDATE DESC"

    .line 416
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 422
    new-instance v14, Lcom/nuance/sample/news/YonhapNewsContainer;

    invoke-direct {v14}, Lcom/nuance/sample/news/YonhapNewsContainer;-><init>()V

    .line 424
    .local v14, "news":Lcom/nuance/sample/news/YonhapNewsContainer;
    if-eqz v10, :cond_0

    .line 425
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 427
    const-string/jumbo v1, "NEWS_LINK"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 429
    .local v13, "linkColumn":I
    const-string/jumbo v1, "NEWS_TIME"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 431
    .local v17, "timeIndex":I
    const-string/jumbo v1, "NEWS_CATEGORY"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 433
    .local v7, "categoryIndex":I
    const-string/jumbo v1, "NEWS_TITLE"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 435
    .local v18, "titleIndex":I
    const-string/jumbo v1, "NEWS_IMAGEURL"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 437
    .local v12, "imageUrlIndex":I
    const-string/jumbo v1, "NEWS_IMAGEDATA"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 439
    .local v11, "imageDataIndex":I
    const-string/jumbo v1, "NEWS_CONTENTTEXT"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 441
    .local v8, "contentTextIndex":I
    const-string/jumbo v1, "NEWS_DATE"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 442
    .local v15, "newsDateIndex":I
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 460
    .end local v7    # "categoryIndex":I
    .end local v8    # "contentTextIndex":I
    .end local v11    # "imageDataIndex":I
    .end local v12    # "imageUrlIndex":I
    .end local v13    # "linkColumn":I
    .end local v15    # "newsDateIndex":I
    .end local v17    # "timeIndex":I
    .end local v18    # "titleIndex":I
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/nuance/sample/news/NewsManager;->updateCurrent(I)Lcom/nuance/sample/news/NewsItem;

    move-result-object v1

    if-nez v1, :cond_1

    .line 461
    invoke-direct/range {p0 .. p0}, Lcom/nuance/sample/news/NewsManager;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    .end local v14    # "news":Lcom/nuance/sample/news/YonhapNewsContainer;
    .end local v16    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    if-eqz v10, :cond_2

    .line 465
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 469
    :cond_2
    return-void

    .line 443
    .restart local v7    # "categoryIndex":I
    .restart local v8    # "contentTextIndex":I
    .restart local v11    # "imageDataIndex":I
    .restart local v12    # "imageUrlIndex":I
    .restart local v13    # "linkColumn":I
    .restart local v14    # "news":Lcom/nuance/sample/news/YonhapNewsContainer;
    .restart local v15    # "newsDateIndex":I
    .restart local v16    # "sb":Ljava/lang/StringBuffer;
    .restart local v17    # "timeIndex":I
    .restart local v18    # "titleIndex":I
    :cond_3
    :try_start_1
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsLink:Ljava/lang/String;

    .line 444
    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsTime:Ljava/lang/Long;

    .line 445
    invoke-interface {v10, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsCategory:Ljava/lang/String;

    .line 446
    move/from16 v0, v18

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsTitle:Ljava/lang/String;

    .line 447
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsImageUrl:Ljava/lang/String;

    .line 448
    invoke-interface {v10, v11}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsImageData:[B

    .line 450
    invoke-interface {v10, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 449
    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsContentText:Ljava/lang/String;

    .line 451
    invoke-interface {v10, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v14, Lcom/nuance/sample/news/YonhapNewsContainer;->NewsPublishTime:Ljava/lang/Long;

    .line 453
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    new-instance v2, Lcom/nuance/sample/news/NewsItem;

    invoke-direct {v2, v14}, Lcom/nuance/sample/news/NewsItem;-><init>(Lcom/nuance/sample/news/YonhapNewsContainer;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 456
    :try_start_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 463
    .end local v7    # "categoryIndex":I
    .end local v8    # "contentTextIndex":I
    .end local v11    # "imageDataIndex":I
    .end local v12    # "imageUrlIndex":I
    .end local v13    # "linkColumn":I
    .end local v14    # "news":Lcom/nuance/sample/news/YonhapNewsContainer;
    .end local v15    # "newsDateIndex":I
    .end local v16    # "sb":Ljava/lang/StringBuffer;
    .end local v17    # "timeIndex":I
    .end local v18    # "titleIndex":I
    :catchall_0
    move-exception v1

    .line 464
    if-eqz v10, :cond_4

    .line 465
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 467
    :cond_4
    throw v1

    .line 453
    .restart local v7    # "categoryIndex":I
    .restart local v8    # "contentTextIndex":I
    .restart local v11    # "imageDataIndex":I
    .restart local v12    # "imageUrlIndex":I
    .restart local v13    # "linkColumn":I
    .restart local v14    # "news":Lcom/nuance/sample/news/YonhapNewsContainer;
    .restart local v15    # "newsDateIndex":I
    .restart local v16    # "sb":Ljava/lang/StringBuffer;
    .restart local v17    # "timeIndex":I
    .restart local v18    # "titleIndex":I
    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public static init()V
    .locals 4

    .prologue
    .line 253
    invoke-static {}, Lcom/nuance/sample/news/NewsManager;->isNewsDaemonAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    new-instance v0, Lcom/nuance/sample/news/NewsManager$ClientReceiver;

    invoke-static {}, Lcom/nuance/sample/news/NewsManager;->getInstance()Lcom/nuance/sample/news/NewsManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/news/NewsManager$ClientReceiver;-><init>(Lcom/nuance/sample/news/NewsManager;Lcom/nuance/sample/news/NewsManager$ClientReceiver;)V

    sput-object v0, Lcom/nuance/sample/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    .line 257
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 260
    sget-object v1, Lcom/nuance/sample/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    .line 261
    new-instance v2, Landroid/content/IntentFilter;

    .line 262
    const-string/jumbo v3, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.YONHAPNEWS_DATE_SYNC"

    .line 261
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 263
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/nuance/sample/news/NewsManager;->startYonhapNewsDaemon(Z)V

    .line 266
    :cond_0
    invoke-static {}, Lcom/nuance/sample/news/NewsManager;->getInstance()Lcom/nuance/sample/news/NewsManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/nuance/sample/news/NewsManager;->isFlipAvailable()Z

    .line 269
    return-void
.end method

.method private isFlipAvailable()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    invoke-virtual {v0}, Lflipboard/api/FlipManager;->isFlipboardAvailable()Z

    move-result v0

    return v0
.end method

.method private static isNewsDaemonAvailable()Z
    .locals 6

    .prologue
    .line 288
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    .line 289
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 290
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    .line 292
    .local v3, "ret":Z
    :try_start_0
    const-string/jumbo v4, "com.sec.android.daemonapp"

    .line 293
    const/16 v5, 0x80

    .line 292
    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    const/4 v3, 0x1

    :goto_0
    move v4, v3

    .line 329
    :goto_1
    return v4

    .line 299
    :catch_0
    move-exception v0

    .line 301
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string/jumbo v4, "com.sec.android.daemonapp.ap.yonhapnews"

    .line 302
    const/16 v5, 0x80

    .line 301
    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 303
    const/4 v3, 0x1

    goto :goto_0

    .line 304
    :catch_1
    move-exception v1

    .line 305
    .local v1, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 306
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private isYonhapAvailable()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 232
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v3

    iput-object v3, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 234
    iget-object v3, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    sget-object v4, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v1

    .line 237
    :cond_1
    iget-boolean v3, p0, Lcom/nuance/sample/news/NewsManager;->bUsingYonhapNews:Z

    if-eqz v3, :cond_2

    move v1, v2

    .line 238
    goto :goto_0

    .line 239
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 240
    .local v0, "ctx":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 241
    const-string/jumbo v4, "yonhap_daemon_service_key_service_status"

    .line 240
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_0

    move v1, v2

    .line 242
    goto :goto_0
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 360
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/nuance/sample/news/NewsManager;->currentIndex:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    monitor-exit p0

    return-void

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static startYonhapNewsDaemon(Z)V
    .locals 9
    .param p0, "onOff"    # Z

    .prologue
    .line 473
    const-string/jumbo v1, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.SERVICE_ON_OFF"

    .line 475
    .local v1, "ACTION_YONNEWS_SERVICE_ON_OFF":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v7, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.SERVICE_ON_OFF"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 477
    .local v3, "intent":Landroid/content/Intent;
    if-nez p0, :cond_0

    .line 478
    const-string/jumbo v7, "START"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 483
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    .line 484
    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 485
    .local v2, "appContext":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 486
    .local v6, "packageName":Ljava/lang/String;
    const-string/jumbo v7, "PACKAGE"

    invoke-virtual {v3, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    const-string/jumbo v7, "CP"

    const-string/jumbo v8, "YonhapNews"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 489
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 491
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v7, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 492
    .local v5, "intent3":Landroid/content/Intent;
    invoke-virtual {v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 494
    const-string/jumbo v0, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    .line 495
    .local v0, "ACTION_DAEMON_NEWS_REFRESH":Ljava/lang/String;
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v7, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 496
    .local v4, "intent2":Landroid/content/Intent;
    const-string/jumbo v7, "PACKAGE"

    invoke-virtual {v4, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 497
    const-string/jumbo v7, "CP"

    const-string/jumbo v8, "YonhapNews"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 498
    invoke-virtual {v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 499
    return-void

    .line 480
    .end local v0    # "ACTION_DAEMON_NEWS_REFRESH":Ljava/lang/String;
    .end local v2    # "appContext":Landroid/content/Context;
    .end local v4    # "intent2":Landroid/content/Intent;
    .end local v5    # "intent3":Landroid/content/Intent;
    .end local v6    # "packageName":Ljava/lang/String;
    :cond_0
    const-string/jumbo v7, "START"

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private declared-synchronized updateCurrent(I)Lcom/nuance/sample/news/NewsItem;
    .locals 4
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 345
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 346
    iget-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 356
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 348
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    iget-object v3, p0, Lcom/nuance/sample/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 351
    iget v2, p0, Lcom/nuance/sample/news/NewsManager;->currentIndex:I

    add-int v0, v2, p1

    .line 352
    .local v0, "index":I
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 353
    iput v0, p0, Lcom/nuance/sample/news/NewsManager;->currentIndex:I

    .line 354
    iget-object v1, p0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/sample/news/NewsItem;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 345
    .end local v0    # "index":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public fetchNews()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 175
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 176
    sget-object v0, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mError:Lflipboard/api/FlipManager$ErrorMessage;

    .line 178
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    if-eqz v0, :cond_0

    .line 179
    const-string/jumbo v0, "NewsManager"

    const-string/jumbo v1, "mThread is not null!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_0
    new-instance v0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    invoke-direct {v0, p0}, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;-><init>(Lcom/nuance/sample/news/NewsManager;)V

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    .line 183
    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->isYonhapAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    iput v9, p0, Lcom/nuance/sample/news/NewsManager;->requestTypeFlags:I

    .line 190
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    .line 191
    new-instance v7, Landroid/content/Intent;

    .line 192
    const-string/jumbo v0, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    .line 191
    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    .local v7, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 194
    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 195
    const-string/jumbo v0, "NewsManager"

    .line 196
    const-string/jumbo v1, "request start yonhap request thread"

    .line 195
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mThread:Lcom/nuance/sample/news/NewsManager$FetchNewsThread;

    invoke-virtual {v0}, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->start()V

    move v0, v9

    .line 223
    :goto_1
    return v0

    .line 197
    :cond_2
    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->isFlipAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/nuance/sample/news/NewsManager;->requestTypeFlags:I

    .line 208
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    const-string/jumbo v1, "819b8dcb"

    const-string/jumbo v2, "news"

    .line 209
    sget-object v3, Lflipboard/api/FlipManager$ContentType;->TEXT:Lflipboard/api/FlipManager$ContentType;

    iget-object v4, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 210
    iget-object v5, p0, Lcom/nuance/sample/news/NewsManager;->mFetchListener:Lflipboard/api/FlipFetchListener;

    .line 208
    invoke-virtual/range {v0 .. v5}, Lflipboard/api/FlipManager;->fetchFlipboardItems(Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;Ljava/util/Locale;Lflipboard/api/FlipFetchListener;)V

    .line 211
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    const-string/jumbo v0, "NewsManager"

    .line 219
    const-string/jumbo v1, "request start flip request thread"

    .line 218
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 214
    :catch_0
    move-exception v6

    .line 215
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v8

    .line 216
    goto :goto_1
.end method

.method public getCurrentNews()Lcom/nuance/sample/news/NewsItem;
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/nuance/sample/news/NewsManager;->updateCurrent(I)Lcom/nuance/sample/news/NewsItem;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getFirstNews()Lcom/nuance/sample/news/NewsItem;
    .locals 1

    .prologue
    .line 364
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->reset()V

    .line 365
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/nuance/sample/news/NewsManager;->updateCurrent(I)Lcom/nuance/sample/news/NewsItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNextNews()Lcom/nuance/sample/news/NewsItem;
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/nuance/sample/news/NewsManager;->updateCurrent(I)Lcom/nuance/sample/news/NewsItem;

    move-result-object v0

    return-object v0
.end method

.method public getPrevNews()Lcom/nuance/sample/news/NewsItem;
    .locals 1

    .prologue
    .line 373
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/nuance/sample/news/NewsManager;->updateCurrent(I)Lcom/nuance/sample/news/NewsItem;

    move-result-object v0

    return-object v0
.end method

.method public getRequestedLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public declared-synchronized hasCachedNews()Z
    .locals 1

    .prologue
    .line 333
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->isYonhapAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/nuance/sample/news/NewsManager;->isFlipAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 401
    iput-object p1, p0, Lcom/nuance/sample/news/NewsManager;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 402
    return-void
.end method
