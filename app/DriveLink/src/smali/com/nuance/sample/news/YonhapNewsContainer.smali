.class public Lcom/nuance/sample/news/YonhapNewsContainer;
.super Ljava/lang/Object;
.source "YonhapNewsContainer.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final TYPE_MAINNEWS:I = 0x1

.field public static final TYPE_PRIMENEWS:I = 0x2

.field public static final TYPE_SEPERATE:I = 0x3

.field public static final TYPE_URGENCYNEWS:I


# instance fields
.field public NewsCategory:Ljava/lang/String;

.field public NewsContentText:Ljava/lang/String;

.field public NewsCredit:Ljava/lang/String;

.field public NewsDate:Ljava/lang/Long;

.field public NewsID:Ljava/lang/String;

.field public NewsImageData:[B

.field public NewsImageUrl:Ljava/lang/String;

.field public NewsIndex:Ljava/lang/String;

.field public NewsLang:Ljava/lang/String;

.field public NewsLink:Ljava/lang/String;

.field public NewsPublishTime:Ljava/lang/Long;

.field public NewsRegion:Ljava/lang/String;

.field public NewsTime:Ljava/lang/Long;

.field public NewsTitle:Ljava/lang/String;

.field public NewsXml:Ljava/lang/String;

.field public RowId:I

.field public UpdateState:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method
