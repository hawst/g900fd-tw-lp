.class final Lcom/nuance/sample/news/NewsManager$FetchNewsThread;
.super Ljava/lang/Thread;
.source "NewsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/news/NewsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FetchNewsThread"
.end annotation


# instance fields
.field private final syncObj:Ljava/lang/Object;

.field final synthetic this$0:Lcom/nuance/sample/news/NewsManager;


# direct methods
.method public constructor <init>(Lcom/nuance/sample/news/NewsManager;)V
    .locals 1

    .prologue
    .line 79
    iput-object p1, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->this$0:Lcom/nuance/sample/news/NewsManager;

    .line 80
    const-string/jumbo v0, "FetchNewsThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    .line 81
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/news/NewsManager$FetchNewsThread;)Lcom/nuance/sample/news/NewsManager;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->this$0:Lcom/nuance/sample/news/NewsManager;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 91
    const/16 v1, 0x2710

    .line 92
    .local v1, "waitTime":I
    const/4 v2, 0x0

    .line 93
    .local v2, "wasInterrupted":Z
    const-string/jumbo v3, "NewsManagerThread"

    const-string/jumbo v4, "start waiting thread"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v4, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    monitor-enter v4

    .line 97
    :try_start_0
    iget-object v3, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    const-wide/16 v5, 0x2710

    invoke-virtual {v3, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :goto_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    const-string/jumbo v3, "NewsManagerThread"

    .line 108
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Not waiting; interrupted=="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 107
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v3, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->this$0:Lcom/nuance/sample/news/NewsManager;

    # getter for: Lcom/nuance/sample/news/NewsManager;->requestTypeFlags:I
    invoke-static {v3}, Lcom/nuance/sample/news/NewsManager;->access$2(Lcom/nuance/sample/news/NewsManager;)I

    move-result v3

    if-nez v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->this$0:Lcom/nuance/sample/news/NewsManager;

    # invokes: Lcom/nuance/sample/news/NewsManager;->getFlipNewsData()V
    invoke-static {v3}, Lcom/nuance/sample/news/NewsManager;->access$3(Lcom/nuance/sample/news/NewsManager;)V

    .line 130
    :goto_1
    new-instance v3, Lcom/nuance/sample/news/NewsManager$FetchNewsThread$1;

    invoke-direct {v3, p0}, Lcom/nuance/sample/news/NewsManager$FetchNewsThread$1;-><init>(Lcom/nuance/sample/news/NewsManager$FetchNewsThread;)V

    invoke-static {v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 142
    const-string/jumbo v3, "NewsManagerThread"

    .line 143
    const-string/jumbo v4, "maybe interrupted thread ----- "

    .line 142
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 102
    const/4 v2, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 128
    :cond_0
    iget-object v3, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->this$0:Lcom/nuance/sample/news/NewsManager;

    # invokes: Lcom/nuance/sample/news/NewsManager;->getYonhapNewsData()V
    invoke-static {v3}, Lcom/nuance/sample/news/NewsManager;->access$4(Lcom/nuance/sample/news/NewsManager;)V

    goto :goto_1
.end method

.method public signal()V
    .locals 2

    .prologue
    .line 84
    iget-object v1, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/nuance/sample/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 84
    monitor-exit v1

    .line 87
    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
