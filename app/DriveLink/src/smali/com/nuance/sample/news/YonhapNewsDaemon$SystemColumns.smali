.class public final Lcom/nuance/sample/news/YonhapNewsDaemon$SystemColumns;
.super Ljava/lang/Object;
.source "YonhapNewsDaemon.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/news/YonhapNewsDaemon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SystemColumns"
.end annotation


# static fields
.field public static final KEY_APP_SERVICE_STATUS:Ljava/lang/String; = "yonhap_daemon_service_key_app_service_status"

.field public static final KEY_AUTOREFRESH_INTERVAL:Ljava/lang/String; = "yonhap_daemon_service_key_autorefresh_interval"

.field public static final KEY_AUTOREFRESH_TIME:Ljava/lang/String; = "yonhap_daemon_service_key_autorefresh_time"

.field public static final KEY_CHARGING_NOTICE:Ljava/lang/String; = "yonhap_daemon_service_key_charging_notice"

.field public static final KEY_SERVICE_STATUS:Ljava/lang/String; = "yonhap_daemon_service_key_service_status"

.field public static final KEY_SET_DEFAULT_NEWS:Ljava/lang/String; = "yonhap_daemon_service_key_set_default_news"

.field public static final YONHAP_UUID:Ljava/lang/String; = "X-Client-UUID"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    return-void
.end method
