.class public Lcom/nuance/sample/domains/music/MusicMainActivity;
.super Lcom/nuance/sample/domains/ScreenActivity;
.source "MusicMainActivity.java"


# static fields
.field public static final EXTRA_PROMPT_BOOLEAN:Ljava/lang/String; = "extra_prompt_boolean"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/nuance/sample/domains/music/MusicMainActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/domains/music/MusicMainActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/nuance/sample/domains/ScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lcom/nuance/sample/domains/music/MusicMainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleUserCancel()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p0}, Lcom/nuance/sample/domains/music/MusicMainActivity;->finish()V

    .line 89
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    sget-object v0, Lcom/nuance/sample/domains/music/MusicMainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    const v0, 0x7f030080

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/music/MusicMainActivity;->setContentView(I)V

    .line 34
    invoke-super {p0, p1}, Lcom/nuance/sample/domains/ScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/nuance/sample/domains/music/MusicMainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onDestroy()V

    .line 83
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/nuance/sample/domains/music/MusicMainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onPause()V

    .line 74
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 43
    sget-object v3, Lcom/nuance/sample/domains/music/MusicMainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onResume()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onResume()V

    .line 46
    invoke-virtual {p0}, Lcom/nuance/sample/domains/music/MusicMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 47
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "extra_prompt_string"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 48
    .local v2, "prompt":Ljava/lang/String;
    const-string/jumbo v3, "extra_field_id"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "fieldId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 50
    const-string/jumbo v3, "extra_field_id"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    .line 52
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    .line 51
    invoke-interface {v3, v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 54
    :cond_0
    if-eqz v2, :cond_1

    .line 56
    const-string/jumbo v3, "extra_prompt_boolean"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    const-string/jumbo v3, "extra_prompt_string"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, v2}, Lcom/nuance/sample/domains/music/MusicMainActivity;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 60
    invoke-virtual {p0}, Lcom/nuance/sample/domains/music/MusicMainActivity;->startListening()V

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v3

    .line 63
    const-wide/16 v4, 0x258

    invoke-virtual {v3, v4, v5}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto :goto_0
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 113
    return-void
.end method
