.class Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;
.super Ljava/lang/Object;
.source "LocationSuggestionsActivity.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DMActionListenerStub"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;


# direct methods
.method private constructor <init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;-><init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)V

    return-void
.end method


# virtual methods
.method public actionAbort()V
    .locals 2

    .prologue
    .line 81
    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DMActionListenerStub actionAbort()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    return-void
.end method

.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 76
    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "DMActionListenerStub actionFail(\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return-void
.end method

.method public actionSuccess()V
    .locals 2

    .prologue
    .line 71
    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DMActionListenerStub actionSuccess()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    return-void
.end method
