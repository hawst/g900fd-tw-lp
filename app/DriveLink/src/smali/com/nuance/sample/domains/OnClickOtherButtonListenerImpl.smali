.class public Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;
.super Ljava/lang/Object;
.source "OnClickOtherButtonListenerImpl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private fieldID:Lcom/nuance/sample/CCFieldIds;

.field private mActivityToLaunchClass:Ljava/lang/Class;

.field private prompt:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;

    .line 14
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 13
    sput-object v0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->TAG:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;Lcom/nuance/sample/CCFieldIds;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "clz"    # Ljava/lang/Class;
    .param p3, "prompt"    # Ljava/lang/String;
    .param p4, "fieldIds"    # Lcom/nuance/sample/CCFieldIds;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->context:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->mActivityToLaunchClass:Ljava/lang/Class;

    .line 25
    iput-object p3, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->prompt:Ljava/lang/String;

    .line 26
    iput-object p4, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->fieldID:Lcom/nuance/sample/CCFieldIds;

    .line 27
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 31
    sget-object v1, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->mActivityToLaunchClass:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 34
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 35
    iget-object v1, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->prompt:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 36
    const-string/jumbo v1, "extra_prompt_string"

    iget-object v2, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->prompt:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->fieldID:Lcom/nuance/sample/CCFieldIds;

    if-eqz v1, :cond_1

    .line 39
    const-string/jumbo v1, "extra_field_id"

    iget-object v2, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->fieldID:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    :cond_1
    iget-object v1, p0, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;->context:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 42
    return-void
.end method
