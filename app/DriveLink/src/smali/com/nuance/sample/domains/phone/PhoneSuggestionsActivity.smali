.class public Lcom/nuance/sample/domains/phone/PhoneSuggestionsActivity;
.super Lcom/nuance/sample/domains/phone/PhoneScreenActivity;
.source "PhoneSuggestionsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActionButton()I
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f090272

    return v0
.end method

.method protected getActionButtonActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/nuance/sample/domains/phone/PhoneLogsActivity;

    return-object v0
.end method

.method protected getItems(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/util/calllog/LoggedCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    const/16 v1, 0x32

    invoke-static {p1, v1}, Lcom/nuance/sample/util/calllog/CallLogUtil;->getLastNOutCalls(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 28
    .local v0, "outCalls":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "found "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 29
    const-string/jumbo v3, " out calls"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 28
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    return-object v0
.end method

.method protected getLayout()I
    .locals 1

    .prologue
    .line 16
    const v0, 0x7f030083

    return v0
.end method

.method protected getListView()I
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f09026f

    return v0
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;->onPause()V

    .line 49
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;->onResume()V

    .line 57
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 76
    return-void
.end method
