.class public Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;
.super Landroid/widget/BaseAdapter;
.source "PhoneAndMessageScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RowAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;


# direct methods
.method public constructor <init>(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 165
    iget-object v1, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    # getter for: Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;
    invoke-static {v1}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->access$0(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 166
    .local v0, "toReturn":I
    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 167
    const/4 v0, 0x3

    .line 169
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 174
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 179
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 184
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->getCount()I

    move-result v4

    if-ge p1, v4, :cond_0

    .line 185
    iget-object v4, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    .line 186
    iget-object v5, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    invoke-virtual {v5}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getListItemView()I

    move-result v5

    .line 185
    invoke-static {v4, v5, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 187
    .local v2, "row":Landroid/view/View;
    iget-object v4, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    # getter for: Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;
    invoke-static {v4}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->access$0(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/util/ContactProvider;

    .line 188
    .local v0, "item":Lcom/nuance/sample/util/ContactProvider;, "TE;"
    if-eqz v0, :cond_0

    .line 189
    const v4, 0x7f09026c

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 190
    .local v3, "text1":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 191
    invoke-interface {v0}, Lcom/nuance/sample/util/ContactProvider;->getContactName()Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 193
    new-instance v4, Ljava/lang/StringBuilder;

    .line 194
    iget-object v5, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    invoke-virtual {v5}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    add-int/lit8 v6, p1, 0x1

    .line 193
    invoke-static {v5, v6}, Lcom/nuance/sample/util/OrdinalUtil;->getDisplayedOrdinal(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 195
    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 196
    invoke-interface {v0}, Lcom/nuance/sample/util/ContactProvider;->getContactName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 197
    const-string/jumbo v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 198
    invoke-interface {v0}, Lcom/nuance/sample/util/ContactProvider;->getContactNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 193
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 205
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "added loggedCall "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 204
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    .end local v0    # "item":Lcom/nuance/sample/util/ContactProvider;, "TE;"
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "row":Landroid/view/View;
    .end local v3    # "text1":Landroid/widget/TextView;
    :cond_0
    return-object v2

    .line 200
    .restart local v0    # "item":Lcom/nuance/sample/util/ContactProvider;, "TE;"
    .restart local v1    # "name":Ljava/lang/String;
    .restart local v2    # "row":Landroid/view/View;
    .restart local v3    # "text1":Landroid/widget/TextView;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    .line 201
    iget-object v5, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    invoke-virtual {v5}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    add-int/lit8 v6, p1, 0x1

    .line 200
    invoke-static {v5, v6}, Lcom/nuance/sample/util/OrdinalUtil;->getDisplayedOrdinal(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 202
    const-string/jumbo v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Lcom/nuance/sample/util/ContactProvider;->getContactNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 200
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
