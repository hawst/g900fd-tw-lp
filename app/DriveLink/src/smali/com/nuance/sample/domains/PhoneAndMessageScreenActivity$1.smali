.class Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;
.super Ljava/lang/Object;
.source "PhoneAndMessageScreenActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$tagName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    iput-object p2, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;->val$tagName:Ljava/lang/String;

    iput-object p3, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;->val$context:Landroid/content/Context;

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;->val$tagName:Ljava/lang/String;

    const-string/jumbo v1, "calls list onItemClick()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v1, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    iget-object v0, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;->this$0:Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;

    # getter for: Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;
    invoke-static {v0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->access$0(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/util/ContactProvider;

    iget-object v2, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v0, v2}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->handleItemClick(Lcom/nuance/sample/util/ContactProvider;Landroid/content/Context;)V

    .line 68
    return-void
.end method
