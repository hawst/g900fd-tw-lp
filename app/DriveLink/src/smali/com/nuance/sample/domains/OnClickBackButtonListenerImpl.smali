.class public Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;
.super Ljava/lang/Object;
.source "OnClickBackButtonListenerImpl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private handler:Lcom/nuance/sample/domains/BackButtonHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;

    .line 17
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 16
    sput-object v0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;->TAG:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/nuance/sample/domains/BackButtonHandler;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "h"    # Lcom/nuance/sample/domains/BackButtonHandler;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;->context:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;->handler:Lcom/nuance/sample/domains/BackButtonHandler;

    .line 25
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 29
    sget-object v0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onClick()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;->handler:Lcom/nuance/sample/domains/BackButtonHandler;

    invoke-interface {v1}, Lcom/nuance/sample/domains/BackButtonHandler;->getBackFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 31
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    .line 30
    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 33
    iget-object v0, p0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;->handler:Lcom/nuance/sample/domains/BackButtonHandler;

    invoke-interface {v0}, Lcom/nuance/sample/domains/BackButtonHandler;->finish()V

    .line 34
    return-void
.end method
