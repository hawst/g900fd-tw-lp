.class Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;
.super Ljava/lang/Object;
.source "LocationSuggestionsActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

.field private final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    iput-object p2, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;->val$context:Landroid/content/Context;

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$0()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "destinations_list onItemClick()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v3, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;
    invoke-static {v3}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$1(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/sample/location/LocationItem;

    .line 117
    .local v1, "destination":Lcom/nuance/sample/location/LocationItem;
    invoke-virtual {v1}, Lcom/nuance/sample/location/LocationItem;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "address":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->finishDialog()V

    .line 120
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-direct {v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;-><init>()V

    .line 121
    iget-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->context(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v3

    .line 122
    new-instance v4, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;

    iget-object v5, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;-><init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;)V

    .line 121
    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->listener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v2

    .line 120
    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    .line 133
    .local v2, "intentAction":Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    .line 135
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "google.navigation:q="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 136
    const-string/jumbo v5, " "

    const-string/jumbo v6, "+"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 135
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 134
    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    .line 137
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->readyToExec()V

    .line 138
    return-void
.end method
