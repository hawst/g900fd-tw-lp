.class public abstract Lcom/nuance/sample/domains/ScreenActivity;
.super Landroid/app/Activity;
.source "ScreenActivity.java"

# interfaces
.implements Lcom/nuance/sample/MicStateMaster;
.implements Lcom/nuance/sample/UiUpdater;
.implements Lcom/nuance/sample/domains/BackButtonHandler;
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field public static final EXTRA_FIELD_ID:Ljava/lang/String; = "extra_field_id"

.field public static final EXTRA_PROMPT_STRING:Ljava/lang/String; = "extra_prompt_string"

.field public static final EXTRA_TTS_STRING:Ljava/lang/String; = "extra_tts_string"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected final LIST_ITEM_MAX:I

.field private mDisplayView:Landroid/widget/TextView;

.field private mEnergyBar:Landroid/widget/ProgressBar;

.field private mHomeButton:Landroid/widget/Button;

.field private mMicButton:Landroid/widget/Button;

.field private mMicOnClickListener:Lcom/nuance/sample/OnClickMicListenerImpl;

.field private mMicState:Lcom/nuance/sample/MicState;

.field private mOnHomeClickListener:Landroid/view/View$OnClickListener;

.field private mScrollView:Landroid/widget/ScrollView;

.field private safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

.field private widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 56
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/nuance/sample/domains/ScreenActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/nuance/sample/domains/ScreenActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 60
    const/4 v0, 0x3

    iput v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->LIST_ITEM_MAX:I

    .line 79
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicState:Lcom/nuance/sample/MicState;

    .line 56
    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/domains/ScreenActivity;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method private updateUi(Ljava/lang/CharSequence;I)V
    .locals 5
    .param p1, "cs"    # Ljava/lang/CharSequence;
    .param p2, "color"    # I

    .prologue
    .line 353
    new-instance v0, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 354
    .local v0, "ssb":Landroid/text/SpannableStringBuilder;
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 355
    const/16 v4, 0x21

    .line 354
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 356
    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mDisplayView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 357
    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mDisplayView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 359
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mScrollView:Landroid/widget/ScrollView;

    if-eqz v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mScrollView:Landroid/widget/ScrollView;

    new-instance v2, Lcom/nuance/sample/domains/ScreenActivity$1;

    invoke-direct {v2, p0}, Lcom/nuance/sample/domains/ScreenActivity$1;-><init>(Lcom/nuance/sample/domains/ScreenActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 366
    :cond_1
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 314
    const/high16 v0, -0x10000

    invoke-direct {p0, p1, v0}, Lcom/nuance/sample/domains/ScreenActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 315
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "System (voice agent) said: \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/domains/ScreenActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 331
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "User said: \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, -0xbbbbbc

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/domains/ScreenActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 323
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 339
    const v0, -0xffff01

    invoke-direct {p0, p1, v0}, Lcom/nuance/sample/domains/ScreenActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 340
    return-void
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 213
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "finish()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 215
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isInDmFlowChanging()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 218
    :cond_0
    return-void
.end method

.method public getBackFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 477
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 228
    const-string/jumbo v1, "samsung_wakeup_engine_enable"

    .line 227
    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 228
    if-eqz v1, :cond_0

    .line 229
    sget-object v1, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    .line 230
    const-string/jumbo v2, "initPhraseSpotter Samsung Seamless and Samsung Multiple Wake-up"

    .line 231
    const/4 v3, 0x0

    .line 229
    invoke-static {v1, v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 232
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 233
    const-class v2, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;

    .line 232
    invoke-static {v1, v2}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 235
    invoke-static {}, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 249
    .local v0, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_0
    return-object v0

    .line 237
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_0
    const-string/jumbo v1, "samsung_multi_engine_enable"

    .line 236
    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 237
    if-eqz v1, :cond_1

    .line 238
    sget-object v1, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    .line 239
    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless and Samsung Multiple Wake-up"

    .line 238
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 241
    const-class v2, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;

    .line 240
    invoke-static {v1, v2}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 243
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 244
    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0

    .line 245
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_1
    sget-object v1, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless Only"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 246
    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0
.end method

.method protected initPhraseSpotter()V
    .locals 2

    .prologue
    .line 221
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 222
    return-void
.end method

.method public onBluetoothServiceConnected()V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    const v0, 0x7f090255

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 105
    const v0, 0x7f090256

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mDisplayView:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f090252

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicButton:Landroid/widget/Button;

    .line 109
    new-instance v0, Lcom/nuance/sample/OnClickMicListenerImpl;

    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicButton:Landroid/widget/Button;

    invoke-direct {v0, p0, v1}, Lcom/nuance/sample/OnClickMicListenerImpl;-><init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicOnClickListener:Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 110
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicOnClickListener:Lcom/nuance/sample/OnClickMicListenerImpl;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    const v0, 0x7f09025e

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mHomeButton:Landroid/widget/Button;

    .line 113
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mHomeButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 114
    new-instance v0, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;

    .line 115
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/nuance/sample/domains/OnClickBackButtonListenerImpl;-><init>(Landroid/content/Context;Lcom/nuance/sample/domains/BackButtonHandler;)V

    .line 114
    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mOnHomeClickListener:Landroid/view/View$OnClickListener;

    .line 116
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mHomeButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mOnHomeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    :cond_0
    const v0, 0x7f090251

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 123
    new-instance v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;-><init>(Landroid/content/Context;Lcom/nuance/sample/UiUpdater;)V

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 124
    invoke-static {}, Lcom/nuance/sample/MainActivity;->getDialogFlowManager()Lcom/nuance/sample/coreaccess/DialogFlowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 125
    iget-object v2, p0, Lcom/nuance/sample/domains/ScreenActivity;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 124
    invoke-virtual {v0, v1, p0, v2}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->stealFlow(Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 126
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 266
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 206
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 209
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 178
    sget-object v1, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPause()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 181
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 182
    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-static {}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getInstance()Lcom/nuance/sample/safereader/VACBackgroundHandler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->unregisterAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 184
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 186
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 187
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 188
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 192
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 193
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "extra_field_id"

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    .line 194
    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 193
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    invoke-static {}, Lcom/nuance/sample/SampleAppApplication;->getInstance()Lcom/nuance/sample/SampleAppApplication;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/nuance/sample/SampleAppApplication;->setInForeground(Z)V

    .line 198
    invoke-static {p0}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->uninitSettings(Landroid/content/Context;)V

    .line 199
    return-void
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 2
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 398
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseDetected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 2
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 403
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 406
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 407
    return-void
.end method

.method public onPhraseSpotterStarted()V
    .locals 2

    .prologue
    .line 423
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotter started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 2

    .prologue
    .line 418
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotter stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 279
    invoke-static {p1}, Lcom/nuance/sample/MainActivity;->prepareDeleteDataMenuItem(Landroid/view/Menu;)V

    .line 281
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->finish()V

    .line 394
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 382
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "completed to play tts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 389
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 376
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "tts will be played"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 133
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {}, Lcom/nuance/sample/SampleAppApplication;->getInstance()Lcom/nuance/sample/SampleAppApplication;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/nuance/sample/SampleAppApplication;->setInForeground(Z)V

    .line 135
    invoke-static {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    .line 136
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-static {}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getInstance()Lcom/nuance/sample/safereader/VACBackgroundHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->registerAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 137
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->setPriority(Z)V

    .line 138
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->startSafeReading()V

    .line 139
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 141
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v0

    .line 142
    sget-object v1, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v1}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-static {}, Lcom/nuance/sample/MainActivity;->getDialogFlowManager()Lcom/nuance/sample/coreaccess/DialogFlowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 144
    iget-object v2, p0, Lcom/nuance/sample/domains/ScreenActivity;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 143
    invoke-virtual {v0, v1, p0, v2}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->stealFlow(Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 153
    :goto_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

    .line 152
    invoke-static {p0, v0, v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->initSettings(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 155
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 156
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 157
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 159
    invoke-static {v3}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 160
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 161
    return-void

    .line 146
    :cond_0
    invoke-static {}, Lcom/nuance/sample/MainActivity;->getDialogFlowManager()Lcom/nuance/sample/coreaccess/DialogFlowManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->stopFlow()V

    .line 147
    invoke-static {}, Lcom/nuance/sample/MainActivity;->getDialogFlowManager()Lcom/nuance/sample/coreaccess/DialogFlowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 148
    iget-object v2, p0, Lcom/nuance/sample/domains/ScreenActivity;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 147
    invoke-virtual {v0, v1, p0, v2}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->initFlow(Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 149
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->updateMicState(Lcom/nuance/sample/MicState;)V

    goto :goto_0
.end method

.method public onScoConnected()V
    .locals 2

    .prologue
    .line 460
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[LatencyCheck] onScoConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->setVolumeControlStream(I)V

    .line 462
    return-void
.end method

.method public onScoDisconnected()V
    .locals 2

    .prologue
    .line 466
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[LatencyCheck] onScoDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/ScreenActivity;->setVolumeControlStream(I)V

    .line 468
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->disconnectScoByIdle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 470
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 471
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 473
    :cond_0
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 0
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 414
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 165
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 166
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 170
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 171
    return-void
.end method

.method protected processIncomingIntent()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 427
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 428
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "extra_prompt_string"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 429
    .local v2, "prompt":Ljava/lang/String;
    const-string/jumbo v4, "extra_tts_string"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 430
    .local v3, "tts":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 431
    move-object v3, v2

    .line 433
    :cond_0
    const-string/jumbo v4, "extra_field_id"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 434
    .local v0, "fieldId":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 435
    const-string/jumbo v4, "extra_field_id"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    .line 437
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    .line 436
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 439
    :cond_1
    if-eqz v2, :cond_2

    .line 441
    const-string/jumbo v4, "extra_prompt_string"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 442
    const-string/jumbo v4, "extra_tts_string"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 444
    invoke-virtual {p0, v2}, Lcom/nuance/sample/domains/ScreenActivity;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 445
    invoke-virtual {p0}, Lcom/nuance/sample/domains/ScreenActivity;->startListening()V

    .line 450
    :goto_0
    return-void

    .line 447
    :cond_2
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v4

    .line 448
    const-wide/16 v5, 0x258

    invoke-virtual {v4, v5, v6}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto :goto_0
.end method

.method public startListening()V
    .locals 4

    .prologue
    .line 87
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 88
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v0

    .line 89
    .local v0, "success":Z
    sget-object v1, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "startUserFlow returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 4
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v3, 0x0

    .line 290
    sget-object v0, Lcom/nuance/sample/domains/ScreenActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "updateMicState() from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iput-object p1, p0, Lcom/nuance/sample/domains/ScreenActivity;->mMicState:Lcom/nuance/sample/MicState;

    .line 292
    invoke-static {}, Lcom/nuance/sample/domains/ScreenActivity;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 307
    :goto_0
    return-void

    .line 295
    :pswitch_0
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 296
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 300
    :pswitch_1
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 304
    :pswitch_2
    iget-object v0, p0, Lcom/nuance/sample/domains/ScreenActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
