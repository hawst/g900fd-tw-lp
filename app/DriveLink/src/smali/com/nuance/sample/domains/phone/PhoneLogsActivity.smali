.class public Lcom/nuance/sample/domains/phone/PhoneLogsActivity;
.super Lcom/nuance/sample/domains/phone/PhoneScreenActivity;
.source "PhoneLogsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActionButton()I
    .locals 1

    .prologue
    .line 22
    const v0, 0x7f090271

    return v0
.end method

.method protected getActionButtonActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/nuance/sample/domains/phone/PhoneSuggestionsActivity;

    return-object v0
.end method

.method public getBackFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getItems(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/util/calllog/LoggedCall;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v4, 0x32

    .line 28
    const/4 v1, 0x1

    .line 30
    .local v1, "newOnly":Z
    invoke-virtual {p0}, Lcom/nuance/sample/domains/phone/PhoneLogsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 29
    invoke-static {v2, v4}, Lcom/nuance/sample/util/calllog/CallLogUtil;->getLastNMissedNewCalls(Landroid/content/Context;I)Ljava/util/Vector;

    move-result-object v0

    .line 31
    .local v0, "missedCalls":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/util/calllog/LoggedCall;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/nuance/sample/domains/phone/PhoneLogsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 33
    const/4 v3, 0x1

    .line 32
    invoke-static {v2, v4, v3}, Lcom/nuance/sample/util/calllog/CallLogUtil;->getLastNCalls(Landroid/content/Context;IZ)Ljava/util/Vector;

    move-result-object v0

    .line 34
    const/4 v1, 0x0

    .line 36
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "found "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 37
    const-string/jumbo v4, " missed calls, newOnly="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 36
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    return-object v0
.end method

.method protected getLayout()I
    .locals 1

    .prologue
    .line 17
    const v0, 0x7f030082

    return v0
.end method

.method protected getListView()I
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f09026b

    return v0
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;->onPause()V

    .line 57
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 64
    invoke-super {p0}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;->onResume()V

    .line 65
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 89
    return-void
.end method
