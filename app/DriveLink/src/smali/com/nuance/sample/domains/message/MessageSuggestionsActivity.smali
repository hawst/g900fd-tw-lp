.class public Lcom/nuance/sample/domains/message/MessageSuggestionsActivity;
.super Lcom/nuance/sample/domains/message/MessageScreenActivity;
.source "MessageSuggestionsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/nuance/sample/domains/message/MessageScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActionButton()I
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f09026d

    return v0
.end method

.method protected getActionButtonActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/nuance/sample/domains/message/MessageInboxActivity;

    return-object v0
.end method

.method protected getItems(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/util/message/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/nuance/sample/util/message/MessageUtil;->getLastNOutMessages(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getLayout()I
    .locals 1

    .prologue
    .line 20
    const v0, 0x7f03007f

    return v0
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->onPause()V

    .line 39
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->onResume()V

    .line 47
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 66
    return-void
.end method
