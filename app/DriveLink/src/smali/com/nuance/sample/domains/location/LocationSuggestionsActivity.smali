.class public Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;
.super Lcom/nuance/sample/domains/ScreenActivity;
.source "LocationSuggestionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$DMActionListenerStub;,
        Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;,
        Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private destinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/location/LocationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mOnClickListener:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;

.field private mShareButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    .line 34
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 33
    sput-object v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/nuance/sample/domains/ScreenActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 6

    .prologue
    .line 241
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;-><init>()V

    .line 242
    .local v0, "builder":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 243
    .local v1, "generalSpeakables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const v4, 0x7f0a05ce

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    const v4, 0x7f090266

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->generalSpeakables(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    .line 246
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->build()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v3

    .line 248
    .local v3, "toReturn":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    iget-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 251
    iget-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 255
    :cond_0
    return-object v3

    .line 251
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/nuance/sample/location/LocationItem;

    .line 252
    .local v2, "item":Lcom/nuance/sample/location/LocationItem;
    invoke-virtual {v2}, Lcom/nuance/sample/location/LocationItem;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->addListItem(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleUserCancel()V
    .locals 2

    .prologue
    .line 235
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleUserCancel()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-virtual {p0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->finish()V

    .line 237
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    sget-object v4, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "onCreate()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {p0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 92
    .local v1, "context":Landroid/content/Context;
    const v4, 0x7f03007c

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->setContentView(I)V

    .line 93
    invoke-super {p0, p1}, Lcom/nuance/sample/domains/ScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    const v4, 0x7f090266

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->mShareButton:Landroid/widget/Button;

    .line 96
    new-instance v4, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;

    invoke-direct {v4, p0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;-><init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)V

    iput-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->mOnClickListener:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;

    .line 97
    iget-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->mShareButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->mOnClickListener:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v4, 0x7f090267

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 99
    iget-object v5, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->mOnClickListener:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;

    .line 98
    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const/4 v3, 0x3

    .line 103
    .local v3, "numDestinations":I
    invoke-static {}, Lcom/nuance/sample/location/LocationUtil;->getAllItems()Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;

    .line 104
    iget-object v4, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 105
    sget-object v4, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "found "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " destinations"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const v4, 0x7f090268

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 108
    .local v2, "lv":Landroid/widget/ListView;
    if-eqz v2, :cond_0

    .line 109
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 110
    new-instance v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;

    invoke-direct {v0, p0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;-><init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)V

    .line 111
    .local v0, "adapter":Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 112
    new-instance v4, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;

    invoke-direct {v4, p0, v1}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$1;-><init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;Landroid/content/Context;)V

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 142
    .end local v0    # "adapter":Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 229
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onDestroy()V

    .line 231
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 269
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 220
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onPause()V

    .line 222
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 207
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onResume()V

    .line 210
    iget-object v0, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->mShareButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 212
    invoke-virtual {p0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->processIncomingIntent()V

    .line 213
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 277
    return-void
.end method
