.class public abstract Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;
.super Lcom/nuance/sample/domains/ScreenActivity;
.source "PhoneAndMessageScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/nuance/sample/util/ContactProvider;",
        ">",
        "Lcom/nuance/sample/domains/ScreenActivity;"
    }
.end annotation


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    .local p0, "this":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>;"
    invoke-direct {p0}, Lcom/nuance/sample/domains/ScreenActivity;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;

    .line 28
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected abstract getActionButton()I
.end method

.method protected abstract getActionButtonActivityClass()Ljava/lang/Class;
.end method

.method protected abstract getFieldId()Lcom/nuance/sample/CCFieldIds;
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 6

    .prologue
    .line 109
    .local p0, "this":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>;"
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;-><init>()V

    .line 110
    .local v0, "builder":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v1, "generalSpeakables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const v4, 0x7f0a05ce

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getActionButton()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->generalSpeakables(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    .line 114
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->build()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v3

    .line 116
    .local v3, "toReturn":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    iget-object v4, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 119
    iget-object v4, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 123
    :cond_0
    return-object v3

    .line 119
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/nuance/sample/util/ContactProvider;

    .line 120
    .local v2, "item":Lcom/nuance/sample/util/ContactProvider;, "TE;"
    invoke-interface {v2}, Lcom/nuance/sample/util/ContactProvider;->getContactName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->addListItem(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract getItems(Landroid/content/Context;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end method

.method protected abstract getLayout()I
.end method

.method protected abstract getListItemView()I
.end method

.method protected abstract getListView()I
.end method

.method protected abstract getPrompt()Ljava/lang/String;
.end method

.method protected abstract handleItemClick(Lcom/nuance/sample/util/ContactProvider;Landroid/content/Context;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation
.end method

.method public handleUserCancel()V
    .locals 2

    .prologue
    .line 35
    .local p0, "this":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "handleUserCancel()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->finish()V

    .line 37
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    .local p0, "this":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 45
    .local v4, "tagName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 47
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v5, "onCreate()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getLayout()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->setContentView(I)V

    .line 50
    invoke-super {p0, p1}, Lcom/nuance/sample/domains/ScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getActionButton()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 53
    .local v3, "mActionButton":Landroid/widget/Button;
    new-instance v5, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;

    .line 54
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getActionButtonActivityClass()Ljava/lang/Class;

    move-result-object v7

    .line 55
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getPrompt()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getFieldId()Lcom/nuance/sample/CCFieldIds;

    move-result-object v9

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/nuance/sample/domains/OnClickOtherButtonListenerImpl;-><init>(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;Lcom/nuance/sample/CCFieldIds;)V

    .line 53
    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    invoke-virtual {p0, v1}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getItems(Landroid/content/Context;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->items:Ljava/util/List;

    .line 58
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getListView()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 59
    .local v2, "lv":Landroid/widget/ListView;
    if-eqz v2, :cond_0

    .line 60
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 61
    new-instance v0, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;

    invoke-direct {v0, p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;-><init>(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;)V

    .line 62
    .local v0, "adapter":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>.RowAdapter;"
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    new-instance v5, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;

    invoke-direct {v5, p0, v4, v1}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$1;-><init>(Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 72
    .end local v0    # "adapter":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity$RowAdapter;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>.RowAdapter;"
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 103
    .local p0, "this":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onDestroy()V

    .line 105
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 93
    .local p0, "this":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onPause()V

    .line 96
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 79
    .local p0, "this":Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;, "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity<TE;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onResume()V

    .line 82
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->getActionButton()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    invoke-virtual {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->processIncomingIntent()V

    .line 86
    return-void
.end method
