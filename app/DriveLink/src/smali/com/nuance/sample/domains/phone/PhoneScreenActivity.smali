.class public abstract Lcom/nuance/sample/domains/phone/PhoneScreenActivity;
.super Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;
.source "PhoneScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity",
        "<",
        "Lcom/nuance/sample/util/calllog/LoggedCall;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_DIAL_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getListItemView()I
    .locals 1

    .prologue
    .line 24
    const v0, 0x7f030081

    return v0
.end method

.method protected getPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f0a00b9

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic handleItemClick(Lcom/nuance/sample/util/ContactProvider;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/nuance/sample/util/calllog/LoggedCall;

    invoke-virtual {p0, p1, p2}, Lcom/nuance/sample/domains/phone/PhoneScreenActivity;->handleItemClick(Lcom/nuance/sample/util/calllog/LoggedCall;Landroid/content/Context;)V

    return-void
.end method

.method protected handleItemClick(Lcom/nuance/sample/util/calllog/LoggedCall;Landroid/content/Context;)V
    .locals 6
    .param p1, "item"    # Lcom/nuance/sample/util/calllog/LoggedCall;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-virtual {p1}, Lcom/nuance/sample/util/calllog/LoggedCall;->getContactNumber()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "address":Ljava/lang/String;
    const v3, 0x7f0a00aa

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 31
    aput-object v0, v4, v5

    .line 30
    invoke-virtual {p2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "prompt":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 33
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->finishDialog()V

    .line 34
    invoke-static {v0}, Lcom/vlingo/core/internal/util/DialUtil;->getDialIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 35
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p2, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 36
    return-void
.end method
