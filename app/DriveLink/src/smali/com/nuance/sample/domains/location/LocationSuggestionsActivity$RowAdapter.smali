.class public Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;
.super Landroid/widget/BaseAdapter;
.source "LocationSuggestionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RowAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;


# direct methods
.method public constructor <init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private getCappedCount()I
    .locals 2

    .prologue
    .line 150
    iget-object v1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;
    invoke-static {v1}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$1(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 151
    .local v0, "toReturn":I
    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 152
    const/4 v0, 0x3

    .line 154
    :cond_0
    return v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;->getCappedCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 158
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 162
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 172
    invoke-direct {p0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;->getCappedCount()I

    move-result v6

    if-ge p1, v6, :cond_3

    .line 173
    iget-object v6, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    .line 174
    const v7, 0x7f030079

    .line 173
    invoke-static {v6, v7, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 175
    .local v2, "row":Landroid/view/View;
    iget-object v6, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$RowAdapter;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->destinations:Ljava/util/List;
    invoke-static {v6}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$1(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/sample/location/LocationItem;

    .line 176
    .local v1, "destination":Lcom/nuance/sample/location/LocationItem;
    if-eqz v1, :cond_3

    .line 178
    const v6, 0x7f09025b

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 177
    check-cast v3, Landroid/widget/TextView;

    .line 179
    .local v3, "text1":Landroid/widget/TextView;
    if-eqz v3, :cond_1

    .line 180
    invoke-virtual {v1}, Lcom/nuance/sample/location/LocationItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 181
    .local v5, "title":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 182
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :cond_0
    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$0()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "added destination "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    .end local v5    # "title":Ljava/lang/String;
    :cond_1
    const v6, 0x7f09025c

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 186
    check-cast v4, Landroid/widget/TextView;

    .line 188
    .local v4, "text2":Landroid/widget/TextView;
    if-eqz v4, :cond_3

    .line 189
    invoke-virtual {v1}, Lcom/nuance/sample/location/LocationItem;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "address":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 191
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    :cond_2
    # getter for: Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->access$0()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "added destination "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "destination":Lcom/nuance/sample/location/LocationItem;
    .end local v2    # "row":Landroid/view/View;
    .end local v3    # "text1":Landroid/widget/TextView;
    .end local v4    # "text2":Landroid/widget/TextView;
    :cond_3
    return-object v2
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method
