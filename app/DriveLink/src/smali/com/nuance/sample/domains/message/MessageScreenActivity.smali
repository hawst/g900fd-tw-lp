.class public abstract Lcom/nuance/sample/domains/message/MessageScreenActivity;
.super Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;
.source "MessageScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity",
        "<",
        "Lcom/nuance/sample/util/message/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->displayWidgetContent(Ljava/lang/CharSequence;)V

    .line 50
    return-void
.end method

.method protected getFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getListItemView()I
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f03007e

    return v0
.end method

.method protected getListView()I
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f09026f

    return v0
.end method

.method protected getPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const v0, 0x7f0a0026

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic handleItemClick(Lcom/nuance/sample/util/ContactProvider;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/nuance/sample/util/message/Message;

    invoke-virtual {p0, p1, p2}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->handleItemClick(Lcom/nuance/sample/util/message/Message;Landroid/content/Context;)V

    return-void
.end method

.method protected handleItemClick(Lcom/nuance/sample/util/message/Message;Landroid/content/Context;)V
    .locals 10
    .param p1, "item"    # Lcom/nuance/sample/util/message/Message;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const-string/jumbo v1, ""

    invoke-virtual {p0, v1}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->displayWidgetContent(Ljava/lang/CharSequence;)V

    .line 58
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 60
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {p1}, Lcom/nuance/sample/util/message/Message;->getMsgId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 61
    invoke-virtual {p1}, Lcom/nuance/sample/util/message/Message;->getTimestamp()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/nuance/sample/util/message/Message;->getContactNumber()Ljava/lang/String;

    move-result-object v5

    .line 62
    invoke-virtual {p1}, Lcom/nuance/sample/util/message/Message;->getContactName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/nuance/sample/util/message/Message;->getBody()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/nuance/sample/util/message/Message;->getSubject()Ljava/lang/String;

    move-result-object v8

    .line 63
    invoke-virtual {p1}, Lcom/nuance/sample/util/message/Message;->getType()Ljava/lang/String;

    move-result-object v9

    .line 60
    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .local v0, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 68
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    .line 69
    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    .line 67
    invoke-interface {v1, v2, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startSafeReaderFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V

    .line 70
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v1, 0x7f09026e

    .line 22
    invoke-super {p0, p1}, Lcom/nuance/sample/domains/PhoneAndMessageScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0, v1}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 24
    .local v0, "emulateButton":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0, v1}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 26
    new-instance v2, Lcom/nuance/sample/domains/message/MessageScreenActivity$1;

    invoke-direct {v2, p0}, Lcom/nuance/sample/domains/message/MessageScreenActivity$1;-><init>(Lcom/nuance/sample/domains/message/MessageScreenActivity;)V

    .line 25
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    :cond_0
    return-void
.end method

.method public startListening()V
    .locals 2

    .prologue
    .line 83
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 89
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 90
    return-void
.end method
