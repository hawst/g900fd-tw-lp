.class public Lcom/nuance/sample/domains/message/MessageSimulationFragment;
.super Landroid/app/DialogFragment;
.source "MessageSimulationFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const-wide/16 v1, 0x1

    .line 55
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 57
    .local v10, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 103
    :goto_0
    invoke-virtual {p0}, Lcom/nuance/sample/domains/message/MessageSimulationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 104
    return-void

    .line 59
    :pswitch_0
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 60
    const-string/jumbo v5, "12345678"

    const-string/jumbo v6, "John"

    const-string/jumbo v7, "Test"

    const-string/jumbo v8, "Test Body"

    .line 61
    const-string/jumbo v9, "MMS"

    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_READBACKMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 68
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    .line 64
    invoke-interface {v0, v10, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->handleAlert(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto :goto_0

    .line 71
    :pswitch_1
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 72
    const-string/jumbo v5, "12345678"

    const-string/jumbo v6, "John"

    const-string/jumbo v7, "Test"

    const-string/jumbo v8, "Test Body 1"

    .line 73
    const-string/jumbo v9, "MMS"

    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    const-wide/16 v1, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 75
    const-string/jumbo v5, "12345678"

    const-string/jumbo v6, "John"

    const-string/jumbo v7, "Test 2"

    const-string/jumbo v8, "Test Body 2"

    .line 76
    const-string/jumbo v9, "MMS"

    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 77
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    const-wide/16 v1, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 78
    const-string/jumbo v5, "12345678"

    const-string/jumbo v6, "John"

    const-string/jumbo v7, "Test 3"

    const-string/jumbo v8, "Test Body 3"

    .line 79
    const-string/jumbo v9, "MMS"

    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    const-wide/16 v1, 0x4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 81
    const-string/jumbo v5, "12345679"

    const-string/jumbo v6, "Joseph"

    const-string/jumbo v7, "Test 4"

    const-string/jumbo v8, "Test Body 4"

    .line 82
    const-string/jumbo v9, "MMS"

    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 83
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    const-wide/16 v1, 0x5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 84
    const-string/jumbo v5, "12345679"

    const-string/jumbo v6, "Joseph"

    const-string/jumbo v7, "Test 5"

    const-string/jumbo v8, "Test Body 5"

    .line 85
    const-string/jumbo v9, "MMS"

    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 89
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_MSGCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 90
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    .line 86
    invoke-interface {v0, v10, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->handleAlert(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 94
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v11

    .line 96
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_READBACKMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 97
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v12

    .line 98
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 99
    const-string/jumbo v5, "12345678"

    const-string/jumbo v6, "John"

    const-string/jumbo v7, "Test"

    const-string/jumbo v8, "Test Body"

    .line 100
    const-string/jumbo v9, "MMS"

    .line 98
    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-interface {v11, v12, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startSafeReaderFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V

    goto/16 :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x7f090273
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    .line 32
    const v1, 0x7f030084

    .line 33
    const/4 v2, 0x0

    .line 31
    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 34
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f090273

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 35
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v1, 0x7f090274

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 37
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const v1, 0x7f090275

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 39
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    invoke-virtual {p0}, Lcom/nuance/sample/domains/message/MessageSimulationFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 41
    return-object v0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 48
    invoke-virtual {p0}, Lcom/nuance/sample/domains/message/MessageSimulationFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 49
    .local v0, "window":Landroid/view/Window;
    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 50
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 51
    return-void
.end method

.method protected startFlow(Ljava/lang/String;Lcom/nuance/sample/CCFieldIds;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "fieldId"    # Lcom/nuance/sample/CCFieldIds;

    .prologue
    .line 107
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 109
    .local v0, "dialogFlow":Lcom/vlingo/core/facade/dialogflow/IDialogFlow;
    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 110
    invoke-interface {v0, p1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 113
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 114
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 115
    return-void
.end method
