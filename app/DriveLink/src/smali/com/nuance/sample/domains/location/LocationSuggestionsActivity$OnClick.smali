.class public Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;
.super Ljava/lang/Object;
.source "LocationSuggestionsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OnClick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;


# direct methods
.method public constructor <init>(Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 65
    :goto_0
    return-void

    .line 45
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 47
    iget-object v1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    const v2, 0x7f0a05eb

    invoke-virtual {v1, v2}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "prompt":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    invoke-virtual {v1, v0}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 50
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    .line 52
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_CONTACT:Lcom/nuance/sample/CCFieldIds;

    .line 53
    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    .line 50
    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->manageClientFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 56
    sget-object v1, Lcom/nuance/sample/controllers/SampleLocationController$Mode;->Share:Lcom/nuance/sample/controllers/SampleLocationController$Mode;

    invoke-static {v1}, Lcom/nuance/sample/controllers/SampleLocationController;->setMode(Lcom/nuance/sample/controllers/SampleLocationController$Mode;)V

    .line 57
    iget-object v1, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    invoke-virtual {v1}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->startListening()V

    goto :goto_0

    .line 60
    .end local v0    # "prompt":Ljava/lang/String;
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 61
    new-instance v1, Lcom/nuance/sample/domains/location/LocationSimulationFragment;

    invoke-direct {v1}, Lcom/nuance/sample/domains/location/LocationSimulationFragment;-><init>()V

    iget-object v2, p0, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity$OnClick;->this$0:Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;

    invoke-virtual {v2}, Lcom/nuance/sample/domains/location/LocationSuggestionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 62
    const-string/jumbo v3, "simulation"

    .line 61
    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/domains/location/LocationSimulationFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x7f090266
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
