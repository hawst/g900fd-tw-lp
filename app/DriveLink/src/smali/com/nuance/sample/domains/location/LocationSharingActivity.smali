.class public Lcom/nuance/sample/domains/location/LocationSharingActivity;
.super Lcom/nuance/sample/domains/ScreenActivity;
.source "LocationSharingActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/nuance/sample/domains/location/LocationSharingActivity;

    .line 15
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 14
    sput-object v0, Lcom/nuance/sample/domains/location/LocationSharingActivity;->TAG:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/nuance/sample/domains/ScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getBackFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_LOCATION_HOME:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleUserCancel()V
    .locals 2

    .prologue
    .line 58
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSharingActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "handleUserCancel()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-virtual {p0}, Lcom/nuance/sample/domains/location/LocationSharingActivity;->finish()V

    .line 60
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSharingActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    const v0, 0x7f03007c

    invoke-virtual {p0, v0}, Lcom/nuance/sample/domains/location/LocationSharingActivity;->setContentView(I)V

    .line 24
    invoke-super {p0, p1}, Lcom/nuance/sample/domains/ScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSharingActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onDestroy()V

    .line 54
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSharingActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onPause()V

    .line 45
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/nuance/sample/domains/location/LocationSharingActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    invoke-super {p0}, Lcom/nuance/sample/domains/ScreenActivity;->onResume()V

    .line 35
    invoke-virtual {p0}, Lcom/nuance/sample/domains/location/LocationSharingActivity;->processIncomingIntent()V

    .line 36
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 90
    return-void
.end method
