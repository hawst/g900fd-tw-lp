.class public Lcom/nuance/sample/domains/message/MessageInboxActivity;
.super Lcom/nuance/sample/domains/message/MessageScreenActivity;
.source "MessageInboxActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/nuance/sample/domains/message/MessageScreenActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActionButton()I
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f090269

    return v0
.end method

.method protected getActionButtonActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/nuance/sample/domains/message/MessageSuggestionsActivity;

    return-object v0
.end method

.method public getBackFieldId()Lcom/nuance/sample/CCFieldIds;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_SMS_CONTACT:Lcom/nuance/sample/CCFieldIds;

    return-object v0
.end method

.method protected getItems(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/sample/util/message/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/nuance/sample/util/message/MessageUtil;->getLastNInMessages(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getLayout()I
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f03007d

    return v0
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->onPause()V

    .line 40
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Lcom/nuance/sample/domains/message/MessageScreenActivity;->onResume()V

    .line 48
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 72
    return-void
.end method
