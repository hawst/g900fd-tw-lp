.class Lcom/nuance/sample/MainActivity$3;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/MainActivity;->onPhraseSpotterStopped()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/MainActivity;


# direct methods
.method constructor <init>(Lcom/nuance/sample/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/MainActivity$3;->this$0:Lcom/nuance/sample/MainActivity;

    .line 676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 678
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v4

    .line 679
    invoke-interface {v4}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 681
    .local v0, "baseContext":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    .line 683
    .local v2, "isBtHeadsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v1

    .line 684
    .local v1, "isBluetoothAudioSupported":Z
    if-eqz v2, :cond_0

    .line 685
    if-nez v1, :cond_2

    .line 686
    :cond_0
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v4

    .line 687
    const/4 v5, 0x3

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->requestAudioFocus(II)V

    .line 696
    :goto_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    .line 697
    const/4 v5, 0x0

    .line 696
    invoke-interface {v4, v5}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v3

    .line 698
    .local v3, "success":Z
    # getter for: Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/MainActivity;->access$2()Ljava/lang/String;

    move-result-object v4

    .line 699
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "onClick() mMicState IDLE, startUserFlow returned "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 700
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 699
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 698
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    if-nez v3, :cond_1

    .line 703
    # getter for: Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/nuance/sample/MainActivity;->access$2()Ljava/lang/String;

    move-result-object v4

    .line 704
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "onClick() mMicState IDLE, startUserFlow error, returned "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 705
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 704
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 703
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    :cond_1
    return-void

    .line 690
    .end local v3    # "success":Z
    :cond_2
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v4

    .line 692
    const/4 v5, 0x6

    .line 691
    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->requestAudioFocus(II)V

    .line 694
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startScoOnStartRecognition()V

    goto :goto_0
.end method
