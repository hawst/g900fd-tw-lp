.class public Lcom/nuance/sample/OnClickMicListenerImpl;
.super Ljava/lang/Object;
.source "OnClickMicListenerImpl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mMicStateInquirer:Lcom/nuance/sample/MicStateMaster;

.field private mPromptString:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 17
    sget-object v0, Lcom/nuance/sample/OnClickMicListenerImpl;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/nuance/sample/OnClickMicListenerImpl;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/nuance/sample/OnClickMicListenerImpl;

    .line 19
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 18
    sput-object v0, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/nuance/sample/MicStateMaster;Landroid/view/View;)V
    .locals 3
    .param p1, "mMicStateInquirer"    # Lcom/nuance/sample/MicStateMaster;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mPromptString:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mMicStateInquirer:Lcom/nuance/sample/MicStateMaster;

    .line 28
    sget-object v0, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setClickable(false):"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setClickable(Z)V

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/nuance/sample/MicStateMaster;Ljava/lang/String;Landroid/view/View;)V
    .locals 3
    .param p1, "mMicStateInquirer"    # Lcom/nuance/sample/MicStateMaster;
    .param p2, "prompt"    # Ljava/lang/String;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mPromptString:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mMicStateInquirer:Lcom/nuance/sample/MicStateMaster;

    .line 36
    iput-object p2, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mPromptString:Ljava/lang/String;

    .line 37
    sget-object v0, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setClickable(false):"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/view/View;->setClickable(Z)V

    .line 39
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 43
    sget-object v1, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick()"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    sget-object v1, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setClickable(false):"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-virtual {p1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 48
    invoke-static {}, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;->resetVoiceMusicSearchTryCount()V

    .line 50
    invoke-static {}, Lcom/nuance/sample/OnClickMicListenerImpl;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mMicStateInquirer:Lcom/nuance/sample/MicStateMaster;

    invoke-interface {v2}, Lcom/nuance/sample/MicStateMaster;->getMicState()Lcom/nuance/sample/MicState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 52
    :pswitch_0
    iget-object v1, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mPromptString:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 53
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mPromptString:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 54
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/OnClickMicListenerImpl;->mPromptString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/nuance/drivelink/DLAppUiUpdater;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 57
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->isListening()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 58
    sget-object v1, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick() : PhraseSpoteer is listening"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 60
    const-string/jumbo v2, "[stop] : OnClickMicListenerImpl - onClick()"

    .line 59
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->stopPhraseSpotting()V

    .line 62
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 63
    invoke-virtual {v1, v5}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->setStartRecoOnSpotterStop(Z)V

    goto :goto_0

    .line 85
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v0

    .line 86
    .local v0, "success":Z
    sget-object v1, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onClick() mMicState IDLE, startUserFlow returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 86
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    if-nez v0, :cond_0

    .line 90
    sget-object v1, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onClick() mMicState IDLE, startUserFlow error, returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 91
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 90
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 97
    .end local v0    # "success":Z
    :pswitch_1
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 98
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/nuance/drivelink/DLAppUiUpdater;->setCancelByTouch(Z)V

    .line 102
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 103
    sget-object v1, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick() mMicState LISTENING, so called endpointReco"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 106
    :pswitch_2
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 110
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 111
    sget-object v1, Lcom/nuance/sample/OnClickMicListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onClick() mMicState THINKING, so called cancelTurn"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
