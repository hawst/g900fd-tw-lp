.class public final enum Lcom/nuance/sample/MicState;
.super Ljava/lang/Enum;
.source "MicState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/nuance/sample/MicState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/nuance/sample/MicState;

.field public static final enum IDLE:Lcom/nuance/sample/MicState;

.field public static final enum LISTENING:Lcom/nuance/sample/MicState;

.field public static final enum THINKING:Lcom/nuance/sample/MicState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lcom/nuance/sample/MicState;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/MicState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    new-instance v0, Lcom/nuance/sample/MicState;

    const-string/jumbo v1, "LISTENING"

    invoke-direct {v0, v1, v3}, Lcom/nuance/sample/MicState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    new-instance v0, Lcom/nuance/sample/MicState;

    const-string/jumbo v1, "THINKING"

    invoke-direct {v0, v1, v4}, Lcom/nuance/sample/MicState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/nuance/sample/MicState;

    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/nuance/sample/MicState;->ENUM$VALUES:[Lcom/nuance/sample/MicState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/nuance/sample/MicState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public static values()[Lcom/nuance/sample/MicState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/nuance/sample/MicState;->ENUM$VALUES:[Lcom/nuance/sample/MicState;

    array-length v1, v0

    new-array v2, v1, [Lcom/nuance/sample/MicState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
