.class public Lcom/nuance/sample/SampleAppApplication;
.super Landroid/app/Application;
.source "SampleAppApplication.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/VlingoApplicationInterface;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final c_SVN:Ljava/lang/String; = "11111"

.field private static instance:Lcom/nuance/sample/SampleAppApplication;


# instance fields
.field private SAMPLE_APP_SERVER_KEYS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private inDmFlowChanging:Z

.field private inForeground:Z

.field private mAppVersion:Ljava/lang/String;

.field private mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

.field private preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/nuance/sample/SampleAppApplication;

    .line 34
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 33
    sput-object v0, Lcom/nuance/sample/SampleAppApplication;->TAG:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/sample/SampleAppApplication;->instance:Lcom/nuance/sample/SampleAppApplication;

    .line 56
    new-instance v0, Lcom/nuance/sample/coreaccess/SampleAppValues;

    new-instance v1, Lcom/nuance/sample/SampleAppApplication;

    invoke-direct {v1}, Lcom/nuance/sample/SampleAppApplication;-><init>()V

    invoke-direct {v0, v1}, Lcom/nuance/sample/coreaccess/SampleAppValues;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->setInterface(Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;)V

    .line 57
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 42
    iput-boolean v2, p0, Lcom/nuance/sample/SampleAppApplication;->inForeground:Z

    .line 43
    iput-boolean v2, p0, Lcom/nuance/sample/SampleAppApplication;->inDmFlowChanging:Z

    .line 44
    const-string/jumbo v0, "11.0.11111"

    iput-object v0, p0, Lcom/nuance/sample/SampleAppApplication;->mAppVersion:Ljava/lang/String;

    .line 46
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 47
    const-string/jumbo v1, "SERVER_NAME"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "SERVICES_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 48
    const-string/jumbo v2, "EVENTLOG_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "HELLO_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 49
    const-string/jumbo v2, "LMTT_HOST_NAME"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/SampleAppApplication;->SAMPLE_APP_SERVER_KEYS:Ljava/util/List;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/SampleAppApplication;)Ljava/util/List;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/nuance/sample/SampleAppApplication;->SAMPLE_APP_SERVER_KEYS:Ljava/util/List;

    return-object v0
.end method

.method private ensureCacheEmpty(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    sget-object v1, Lcom/nuance/sample/SampleAppApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "tts cache car_iux_tts_cacheing_required set to false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Landroid/content/Context;Z)I

    move-result v0

    .line 147
    .local v0, "numDeleted":I
    sget-object v1, Lcom/nuance/sample/SampleAppApplication;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "tts cache !cachesAreIsEmpty, so purged "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " historically cached TTS files"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return-void
.end method

.method private ensureCacheEmptyAndOff()V
    .locals 2

    .prologue
    .line 161
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 162
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->setTtsCachingOff()V

    .line 163
    invoke-direct {p0, v0}, Lcom/nuance/sample/SampleAppApplication;->ensureCacheEmpty(Landroid/content/Context;)V

    .line 164
    return-void
.end method

.method public static getInstance()Lcom/nuance/sample/SampleAppApplication;
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/nuance/sample/SampleAppApplication;->instance:Lcom/nuance/sample/SampleAppApplication;

    return-object v0
.end method

.method private initClientSpecificSettings(Landroid/content/SharedPreferences$Editor;)V
    .locals 3
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    const/4 v2, 0x1

    .line 168
    const-string/jumbo v1, "key_midas_first_run"

    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 169
    .local v0, "firstRun":Z
    if-eqz v0, :cond_0

    .line 170
    const-string/jumbo v1, "seamless_wakeup"

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 171
    const-string/jumbo v1, "SEND_WAKEUP_WORD_HEADERS"

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 172
    const-string/jumbo v1, "obey_device_location_settings"

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 173
    const-string/jumbo v1, "key_midas_first_run"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 175
    :cond_0
    invoke-direct {p0}, Lcom/nuance/sample/SampleAppApplication;->ensureCacheEmptyAndOff()V

    .line 176
    return-void
.end method

.method private isSvoiceThroughIux()Z
    .locals 2

    .prologue
    .line 82
    const-string/jumbo v0, "iux_complete"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public isAppInForeground()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/nuance/sample/SampleAppApplication;->inForeground:Z

    return v0
.end method

.method public isInDmFlowChanging()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/nuance/sample/SampleAppApplication;->inDmFlowChanging:Z

    return v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 112
    sget-object v1, Lcom/nuance/sample/SampleAppApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 115
    sput-object p0, Lcom/nuance/sample/SampleAppApplication;->instance:Lcom/nuance/sample/SampleAppApplication;

    .line 117
    new-instance v1, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    iget-object v2, p0, Lcom/nuance/sample/SampleAppApplication;->mAppVersion:Ljava/lang/String;

    invoke-direct {v1, p0, p0, v2}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/nuance/sample/SampleAppApplication;->mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    .line 118
    iget-object v1, p0, Lcom/nuance/sample/SampleAppApplication;->mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->initCore()V

    .line 120
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 121
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0, v0}, Lcom/nuance/sample/SampleAppApplication;->initClientSpecificSettings(Landroid/content/SharedPreferences$Editor;)V

    .line 123
    new-instance v1, Lcom/nuance/sample/SampleAppApplication$1;

    invoke-direct {v1, p0}, Lcom/nuance/sample/SampleAppApplication$1;-><init>(Lcom/nuance/sample/SampleAppApplication;)V

    iput-object v1, p0, Lcom/nuance/sample/SampleAppApplication;->preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 140
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/SampleAppApplication;->preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 142
    return-void
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 180
    sget-object v0, Lcom/nuance/sample/SampleAppApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onTerminate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lcom/nuance/sample/SampleAppApplication;->mSampleAppCoreManager:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->destroyCore()V

    .line 182
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 183
    return-void
.end method

.method public setInDmFlowChanging(Z)V
    .locals 0
    .param p1, "inDmFlowChanging"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/nuance/sample/SampleAppApplication;->inDmFlowChanging:Z

    .line 74
    return-void
.end method

.method public setInForeground(Z)V
    .locals 0
    .param p1, "inForeground"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/nuance/sample/SampleAppApplication;->inForeground:Z

    .line 66
    return-void
.end method

.method public startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 77
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/nuance/sample/SampleAppApplication;->setInDmFlowChanging(Z)V

    .line 78
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 79
    return-void
.end method

.method public startMainActivity()V
    .locals 4

    .prologue
    .line 87
    sget-object v2, Lcom/nuance/sample/SampleAppApplication;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "startMainActivity()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 89
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 90
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0}, Lcom/nuance/sample/SampleAppApplication;->isSvoiceThroughIux()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 92
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/nuance/sample/MainActivity;

    .line 91
    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    .local v1, "i":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 94
    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/SampleAppApplication;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    .line 103
    return-void

    .line 101
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "SVoice not yet through IUX"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public startMainService(Z)V
    .locals 0
    .param p1, "closeApplication"    # Z

    .prologue
    .line 108
    return-void
.end method
