.class public Lcom/nuance/sample/debugsettings/ServerPreference;
.super Lcom/vlingo/core/internal/debug/ServerPreferenceValue;
.source "ServerPreference.java"


# instance fields
.field private editText:Landroid/widget/EditText;

.field private final server_list_id:I

.field servers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private spinner:Landroid/widget/Spinner;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/nuance/sample/debugsettings/ServerPreference;->setPersistent(Z)V

    .line 40
    const v1, 0x7f030085

    invoke-virtual {p0, v1}, Lcom/nuance/sample/debugsettings/ServerPreference;->setDialogLayoutResource(I)V

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->servers:Ljava/util/ArrayList;

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 45
    .local v0, "r":Landroid/content/res/Resources;
    const-string/jumbo v1, "SERVER_NAME"

    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const v1, 0x7f070012

    iput v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->server_list_id:I

    .line 56
    :goto_0
    iget-object v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->servers:Ljava/util/ArrayList;

    iget v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->server_list_id:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 57
    return-void

    .line 47
    :cond_0
    const-string/jumbo v1, "EVENTLOG_HOST_NAME"

    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 48
    const v1, 0x7f070013

    iput v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->server_list_id:I

    goto :goto_0

    .line 49
    :cond_1
    const-string/jumbo v1, "HELLO_HOST_NAME"

    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 50
    const v1, 0x7f070014

    iput v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->server_list_id:I

    goto :goto_0

    .line 51
    :cond_2
    const-string/jumbo v1, "LMTT_HOST_NAME"

    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 52
    const v1, 0x7f070015

    iput v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->server_list_id:I

    goto :goto_0

    .line 54
    :cond_3
    const v1, 0x7f070016

    iput v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->server_list_id:I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/nuance/sample/debugsettings/ServerPreference;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/debugsettings/ServerPreference;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/debugsettings/ServerPreference;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 61
    const v2, 0x7f090276

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->editText:Landroid/widget/EditText;

    .line 62
    const v2, 0x7f090277

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->spinner:Landroid/widget/Spinner;

    .line 64
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 66
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    .line 67
    iget-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->editText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 70
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->server_list_id:I

    .line 71
    const v4, 0x1090008

    .line 69
    invoke-static {v2, v3, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 72
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v2, 0x109000a

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 74
    iget-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 76
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->updateSpinner()V

    .line 78
    iget-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->spinner:Landroid/widget/Spinner;

    new-instance v3, Lcom/nuance/sample/debugsettings/ServerPreference$1;

    invoke-direct {v3, p0}, Lcom/nuance/sample/debugsettings/ServerPreference$1;-><init>(Lcom/nuance/sample/debugsettings/ServerPreference;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 93
    iget-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->editText:Landroid/widget/EditText;

    new-instance v3, Lcom/nuance/sample/debugsettings/ServerPreference$2;

    invoke-direct {v3, p0}, Lcom/nuance/sample/debugsettings/ServerPreference$2;-><init>(Lcom/nuance/sample/debugsettings/ServerPreference;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 108
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;->onBindDialogView(Landroid/view/View;)V

    .line 109
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 3
    .param p1, "positiveResult"    # Z

    .prologue
    .line 121
    if-eqz p1, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 123
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/ServerPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 124
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 126
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;->onDialogClosed(Z)V

    .line 127
    return-void
.end method

.method protected setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    .line 136
    return-void
.end method

.method protected updateSpinner()V
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->servers:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 114
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/debugsettings/ServerPreference;->spinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 117
    return-void
.end method
