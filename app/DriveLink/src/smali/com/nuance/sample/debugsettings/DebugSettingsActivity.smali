.class public Lcom/nuance/sample/debugsettings/DebugSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "DebugSettingsActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field public static final DEBUG_MENU:I = 0x3e8

.field static log:Lcom/vlingo/core/internal/logging/Logger;

.field public static showDebugSettings:Z


# instance fields
.field private changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

.field private mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

.field private m_Carrier:Landroid/preference/EditTextPreference;

.field private m_CarrierCountry:Landroid/preference/EditTextPreference;

.field private m_fake_lat:Landroid/preference/EditTextPreference;

.field private m_fake_long:Landroid/preference/EditTextPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 36
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->showDebugSettings:Z

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;-><init>()V

    iput-object v0, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    .line 32
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/debugsettings/DebugSettingsActivity;)Landroid/preference/EditTextPreference;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

    return-object v0
.end method


# virtual methods
.method enableFakeLatLong(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 157
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_CarrierCountry:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 158
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_Carrier:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 159
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_fake_lat:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 160
    iget-object v0, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_fake_long:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 161
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 165
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    .line 166
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 167
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/nuance/sample/MainActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 168
    const-string/jumbo v1, "key"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    invoke-virtual {p0, v0}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 170
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->finish()V

    .line 172
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    .line 52
    sget-object v6, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v7, "onCreate()"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 53
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const/high16 v6, 0x7f050000

    invoke-virtual {p0, v6}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->addPreferencesFromResource(I)V

    .line 56
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 57
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 59
    const-string/jumbo v6, "key"

    const-string/jumbo v7, "gottajibboo"

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 60
    .local v4, "key":Ljava/lang/String;
    const-string/jumbo v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 61
    sput-boolean v9, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->showDebugSettings:Z

    .line 64
    .end local v4    # "key":Ljava/lang/String;
    :cond_0
    sget-boolean v6, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->showDebugSettings:Z

    if-nez v6, :cond_1

    .line 66
    sget-object v6, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v7, "did not find key in extras, finish()"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->finish()V

    .line 71
    :cond_1
    new-instance v6, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v7, "CARRIER_COUNTRY"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/EditTextPreference;

    iput-object v6, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_CarrierCountry:Landroid/preference/EditTextPreference;

    .line 72
    new-instance v6, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v7, "CARRIER"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/EditTextPreference;

    iput-object v6, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_Carrier:Landroid/preference/EditTextPreference;

    .line 73
    new-instance v6, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v7, "FAKE_LAT"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/EditTextPreference;

    iput-object v6, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_fake_lat:Landroid/preference/EditTextPreference;

    .line 74
    new-instance v6, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v7, "FAKE_LONG"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/EditTextPreference;

    iput-object v6, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->m_fake_long:Landroid/preference/EditTextPreference;

    .line 77
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "FAKE_LAT_LONG"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    new-instance v7, Lcom/nuance/sample/debugsettings/DebugSettingsActivity$1;

    invoke-direct {v7, p0}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity$1;-><init>(Lcom/nuance/sample/debugsettings/DebugSettingsActivity;)V

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    .line 82
    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 84
    .local v2, "fakeLocation":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-virtual {p0, v6}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->enableFakeLatLong(Z)V

    .line 86
    new-instance v6, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v7, "DEVICE_MODEL"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    .line 87
    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/EditTextPreference;

    .line 86
    iput-object v6, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

    .line 88
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "FAKE_DEVICE_MODEL"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    .line 89
    new-instance v7, Lcom/nuance/sample/debugsettings/DebugSettingsActivity$2;

    invoke-direct {v7, p0}, Lcom/nuance/sample/debugsettings/DebugSettingsActivity$2;-><init>(Lcom/nuance/sample/debugsettings/DebugSettingsActivity;)V

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    .line 94
    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 96
    .local v1, "fakeDeviceModelCheckboxChecked":Landroid/preference/CheckBoxPreference;
    iget-object v6, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-virtual {v6, v7}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 98
    invoke-static {}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getInstance()Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    move-result-object v5

    .line 100
    .local v5, "servers":Lcom/nuance/sample/coreaccess/identification/ServerDetails;
    new-instance v6, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v7, "SERVER_NAME"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getASRHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 101
    new-instance v6, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v7, "EVENTLOG_HOST_NAME"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getLogHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 102
    new-instance v6, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v7, "HELLO_HOST_NAME"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getHelloHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 103
    new-instance v6, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v7, "SERVICES_HOST_NAME"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getVCSHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 105
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "https.asr_enabled"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 106
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "https.log_enabled"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 107
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "https.hello_enabled"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 108
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "https.vcs_enabled"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 109
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "asr.http.keep_alive"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 110
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "seamless_wakeup"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "seamless_wakeup"

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 111
    new-instance v6, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v7, "SEND_WAKEUP_WORD_HEADERS"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v6, p0, v7}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 113
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 119
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 131
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 132
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 133
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 124
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 126
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 140
    if-nez p2, :cond_0

    .line 154
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v1, p2}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    sget-object v1, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onSharedPreferenceChanged() key="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 147
    const-string/jumbo v3, ", calling onPreferenceUpdated"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 146
    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v1, p2}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;

    .line 149
    .local v0, "action":Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;
    invoke-interface {v0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;->onPreferenceUpdated()V

    goto :goto_0

    .line 152
    .end local v0    # "action":Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;
    :cond_1
    sget-object v1, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onSharedPreferenceChanged() key="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", ignored"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method
