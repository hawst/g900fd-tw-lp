.class Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest$1;
.super Ljava/lang/Object;
.source "PhoneContactFetchAndSortRequest.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getContactMatchComparator()Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactMatch;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;


# direct methods
.method constructor <init>(Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest$1;->this$0:Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;

    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 4
    .param p1, "leftMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "rightMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 286
    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v2

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v0

    .line 288
    :cond_1
    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v2

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    move v0, v1

    .line 289
    goto :goto_0

    .line 292
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v2

    .line 293
    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 295
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v0

    .line 296
    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v2

    if-ge v0, v2, :cond_3

    move v0, v1

    .line 297
    goto :goto_0

    .line 299
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    check-cast p2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, p1, p2}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest$1;->compare(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v0

    return v0
.end method
