.class public Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;
.super Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
.source "PhoneContactFetchAndSortRequest.java"


# static fields
.field private static final CONTACT_SCORE_THRESHOLD:F = 10.0f

.field public static final MAX_SCORE:F = 20.0f

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;

    .line 27
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 26
    sput-object v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->TAG:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p4, "requestedTypes"    # [I
    .param p5, "callback"    # Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V

    .line 36
    return-void
.end method

.method private breakAllWords(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    .local v1, "newName":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 257
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 250
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    .line 251
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    .line 250
    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 251
    if-eqz v2, :cond_1

    .line 252
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_1
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private breakAtPosition(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 262
    .local v1, "newName":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 269
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 263
    :cond_0
    if-ne v0, p2, :cond_1

    .line 264
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 266
    :cond_1
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private computeChineseScore(FLcom/vlingo/core/internal/contacts/ContactMatch;)F
    .locals 10
    .param p1, "score"    # F
    .param p2, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/high16 v9, 0x41a00000    # 20.0f

    .line 216
    iget-object v4, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 217
    .local v4, "name":Ljava/lang/String;
    move v0, p1

    .line 218
    .local v0, "chineseScore":F
    cmpl-float v7, v0, v9

    if-ltz v7, :cond_0

    move v1, v0

    .line 244
    .end local v0    # "chineseScore":F
    .local v1, "chineseScore":F
    :goto_0
    return v1

    .line 222
    .end local v1    # "chineseScore":F
    .restart local v0    # "chineseScore":F
    :cond_0
    invoke-direct {p0, v4}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->breakAllWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 223
    .local v5, "newName":Ljava/lang/String;
    invoke-direct {p0, p2, v5}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 224
    .local v6, "newScore":F
    cmpl-float v7, v6, v0

    if-lez v7, :cond_1

    .line 225
    move v0, v6

    .line 227
    :cond_1
    cmpg-float v7, v0, v9

    if-gez v7, :cond_3

    .line 228
    const-string/jumbo v7, " "

    const-string/jumbo v8, ""

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 229
    invoke-direct {p0, p2, v5}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 230
    cmpl-float v7, v6, v0

    if-lez v7, :cond_2

    .line 231
    move v0, v6

    .line 233
    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x4

    if-le v7, v8, :cond_4

    const/4 v3, 0x3

    .line 234
    .local v3, "maxBreakPosition":I
    :goto_1
    move v2, v3

    .local v2, "i":I
    :goto_2
    if-gez v2, :cond_5

    .end local v2    # "i":I
    .end local v3    # "maxBreakPosition":I
    :cond_3
    move v1, v0

    .line 244
    .end local v0    # "chineseScore":F
    .restart local v1    # "chineseScore":F
    goto :goto_0

    .line 233
    .end local v1    # "chineseScore":F
    .restart local v0    # "chineseScore":F
    :cond_4
    const/4 v3, 0x1

    goto :goto_1

    .line 235
    .restart local v2    # "i":I
    .restart local v3    # "maxBreakPosition":I
    :cond_5
    cmpg-float v7, v0, v9

    if-gez v7, :cond_6

    .line 236
    invoke-direct {p0, v4, v2}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->breakAtPosition(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 237
    invoke-direct {p0, p2, v5}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v6

    .line 238
    cmpl-float v7, v6, v0

    if-lez v7, :cond_6

    .line 239
    move v0, v6

    .line 234
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_2
.end method

.method private computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;)F
    .locals 4
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 89
    iget-object v0, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 90
    .local v0, "name":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F

    move-result v1

    .line 91
    .local v1, "score":F
    sget-object v2, Lcom/vlingo/core/internal/util/StringUtils;->CHINESE_UNICODE_BLOCKS:Ljava/util/Set;

    .line 92
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 91
    invoke-static {v3}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 92
    if-eqz v2, :cond_0

    .line 93
    invoke-direct {p0, v1, p1}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeChineseScore(FLcom/vlingo/core/internal/contacts/ContactMatch;)F

    move-result v1

    .line 95
    :cond_0
    return v1
.end method

.method private computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)F
    .locals 8
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, ", &/+.-"

    invoke-static {v5, v6}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "contactWords":[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeScore([Ljava/lang/String;)F

    move-result v4

    .line 104
    .local v4, "score":F
    const-string/jumbo v5, "contacts.use_other_names"

    .line 105
    const/4 v6, 0x1

    .line 104
    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 105
    if-eqz v5, :cond_1

    .line 107
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasExtraNames()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 108
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getExtraNames()Ljava/util/List;

    move-result-object v3

    .line 109
    .local v3, "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 121
    .end local v3    # "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return v4

    .line 109
    .restart local v3    # "extraNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 111
    .local v1, "extraName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ", &/+."

    invoke-static {v6, v7}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-direct {p0, v0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeScore([Ljava/lang/String;)F

    move-result v2

    .line 113
    .local v2, "extraNameScore":F
    const/high16 v6, -0x40000000    # -2.0f

    add-float/2addr v2, v6

    .line 114
    cmpg-float v6, v4, v2

    if-gez v6, :cond_0

    .line 115
    move v4, v2

    .line 116
    iput-object v1, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNameUsed:Ljava/lang/String;

    goto :goto_0
.end method

.method private computeScore([Ljava/lang/String;)F
    .locals 13
    .param p1, "contactWords"    # [Ljava/lang/String;

    .prologue
    const/high16 v12, -0x40800000    # -1.0f

    .line 125
    const/4 v9, 0x0

    .line 130
    .local v9, "score":F
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getMatchString()Ljava/util/List;

    move-result-object v7

    .line 131
    .local v7, "matchString":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    new-array v10, v11, [Z

    .line 132
    .local v10, "wasWordUsed":[Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-lt v3, v11, :cond_0

    .line 135
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    array-length v11, p1

    if-lt v5, v11, :cond_1

    .line 178
    const/4 v3, 0x0

    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-lt v3, v11, :cond_e

    .line 182
    return v9

    .line 133
    .end local v5    # "j":I
    :cond_0
    const/4 v11, 0x0

    aput-boolean v11, v10, v3

    .line 132
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 136
    .restart local v5    # "j":I
    :cond_1
    const/4 v1, 0x0

    .line 137
    .local v1, "bestMatchScore":I
    const/4 v0, -0x1

    .line 138
    .local v0, "bestMatchIndex":I
    aget-object v2, p1, v5

    .line 139
    .local v2, "contactWord":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_2

    .line 135
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 141
    :cond_2
    const/4 v3, 0x0

    :goto_4
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-lt v3, v11, :cond_3

    .line 171
    if-ltz v0, :cond_d

    .line 172
    int-to-float v11, v1

    add-float/2addr v9, v11

    .line 173
    const/4 v11, 0x1

    aput-boolean v11, v10, v0

    goto :goto_3

    .line 142
    :cond_3
    aget-boolean v11, v10, v3

    if-nez v11, :cond_7

    .line 143
    const/4 v4, 0x0

    .line 144
    .local v4, "isMatchingPosition":Z
    if-nez v3, :cond_4

    if-eqz v5, :cond_5

    :cond_4
    if-eqz v3, :cond_6

    if-eqz v5, :cond_6

    .line 145
    :cond_5
    const/4 v4, 0x1

    .line 147
    :cond_6
    const/4 v6, 0x0

    .line 148
    .local v6, "matchScore":I
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 149
    .local v8, "matchWord":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_8

    .line 141
    .end local v4    # "isMatchingPosition":Z
    .end local v6    # "matchScore":I
    .end local v8    # "matchWord":Ljava/lang/String;
    :cond_7
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 152
    .restart local v4    # "isMatchingPosition":Z
    .restart local v6    # "matchScore":I
    .restart local v8    # "matchWord":Ljava/lang/String;
    :cond_8
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 153
    if-eqz v4, :cond_a

    .line 154
    const/16 v6, 0x14

    .line 165
    :cond_9
    :goto_6
    if-le v6, v1, :cond_7

    .line 166
    move v1, v6

    .line 167
    move v0, v3

    goto :goto_5

    .line 156
    :cond_a
    const/16 v6, 0x12

    .line 158
    goto :goto_6

    :cond_b
    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 159
    if-eqz v4, :cond_c

    .line 160
    const/16 v6, 0xa

    .line 161
    goto :goto_6

    .line 162
    :cond_c
    const/16 v6, 0x8

    goto :goto_6

    .line 175
    .end local v4    # "isMatchingPosition":Z
    .end local v6    # "matchScore":I
    .end local v8    # "matchWord":Ljava/lang/String;
    :cond_d
    add-float/2addr v9, v12

    goto :goto_3

    .line 179
    .end local v0    # "bestMatchIndex":I
    .end local v1    # "bestMatchScore":I
    .end local v2    # "contactWord":Ljava/lang/String;
    :cond_e
    aget-boolean v11, v10, v3

    if-nez v11, :cond_f

    .line 180
    add-float/2addr v9, v12

    .line 178
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private static split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "delimiters"    # Ljava/lang/String;

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "count":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    .line 188
    .local v7, "len":I
    const/4 v6, 0x1

    .line 190
    .local v6, "lastWasDelimiter":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v7, :cond_0

    .line 199
    new-array v0, v1, [Ljava/lang/String;

    .line 200
    .local v0, "arr":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 201
    .local v3, "index":I
    const/4 v5, -0x1

    .line 202
    .local v5, "lastStart":I
    const/4 v2, 0x0

    move v4, v3

    .end local v3    # "index":I
    .local v4, "index":I
    :goto_1
    if-le v2, v7, :cond_3

    .line 212
    return-object v0

    .line 191
    .end local v0    # "arr":[Ljava/lang/String;
    .end local v4    # "index":I
    .end local v5    # "lastStart":I
    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_1

    .line 192
    const/4 v6, 0x1

    .line 190
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 194
    :cond_1
    if-eqz v6, :cond_2

    .line 195
    add-int/lit8 v1, v1, 0x1

    .line 196
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 203
    .restart local v0    # "arr":[Ljava/lang/String;
    .restart local v4    # "index":I
    .restart local v5    # "lastStart":I
    :cond_3
    if-eq v2, v7, :cond_4

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_5

    .line 204
    :cond_4
    if-ltz v5, :cond_7

    .line 205
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "index":I
    .restart local v3    # "index":I
    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v4

    .line 207
    :goto_3
    const/4 v5, -0x1

    .line 202
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    .end local v3    # "index":I
    .restart local v4    # "index":I
    goto :goto_1

    .line 208
    :cond_5
    if-gez v5, :cond_6

    .line 209
    move v5, v2

    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_4

    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_6
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_4

    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_7
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_3
.end method


# virtual methods
.method protected getContactMatchComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283
    new-instance v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest$1;

    invoke-direct {v0, p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest$1;-><init>(Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;)V

    return-object v0
.end method

.method protected getContactMatches(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "contactType"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    sget-object v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getContactMatches()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    .line 277
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p3}, Lcom/vlingo/core/internal/contacts/ContactType;->getLookupTypes()Ljava/util/EnumSet;

    move-result-object v2

    .line 278
    invoke-virtual {p3}, Lcom/vlingo/core/internal/contacts/ContactType;->keepUnrelatedContacts()Z

    move-result v3

    .line 276
    invoke-interface {v0, v1, p2, v2, v3}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPruningThreshold()F
    .locals 1

    .prologue
    .line 307
    const/high16 v0, 0x41200000    # 10.0f

    return v0
.end method

.method protected scoreContacts()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 40
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactType;->keepUnrelatedContacts()Z

    move-result v4

    .line 41
    .local v4, "mixedForPhone":Z
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getBestContact()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    .line 42
    .local v0, "bestContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getMatches()Ljava/util/List;

    move-result-object v3

    .line 43
    .local v3, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 44
    const/4 v2, 0x0

    .line 45
    .local v2, "foundBest":Z
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 78
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_8

    .line 83
    invoke-virtual {p0, v0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->setBestContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 84
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->matchExtraSortedContacts()V

    .line 86
    .end local v2    # "foundBest":Z
    :cond_2
    return-void

    .line 45
    .restart local v2    # "foundBest":Z
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 46
    .local v1, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-string/jumbo v6, "new_contact_match_algo"

    invoke-static {v6, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 47
    if-nez v6, :cond_4

    .line 48
    invoke-direct {p0, v1}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->computeScore(Lcom/vlingo/core/internal/contacts/ContactMatch;)F

    move-result v6

    iput v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 55
    :cond_4
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactType;->getPreferredTarget()I

    move-result v6

    .line 56
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getRequestedTypes()[I

    move-result-object v7

    .line 55
    invoke-virtual {v1, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeScores(I[I)V

    .line 61
    if-eqz v4, :cond_5

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_5

    .line 62
    iget v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    const v7, 0x3dcccccd    # 0.1f

    mul-float/2addr v6, v7

    iput v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 65
    :cond_5
    if-eqz v0, :cond_6

    if-eqz v2, :cond_6

    .line 66
    iget v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v7, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_0

    .line 67
    :cond_6
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getMru()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v6

    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 68
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getMru()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v6

    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getContactType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScoresForData(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 70
    if-eqz v2, :cond_7

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->compareTo(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v6

    if-ne v6, v8, :cond_0

    .line 71
    :cond_7
    move-object v0, v1

    .line 72
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->sortDetails()V

    .line 73
    const/4 v2, 0x1

    goto :goto_0

    .line 78
    .end local v1    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 79
    .restart local v1    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget v6, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v7, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getPruningThreshold()F

    move-result v8

    sub-float/2addr v7, v8

    cmpl-float v6, v6, v7

    if-lez v6, :cond_1

    .line 80
    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;->getSortedContacts()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method
