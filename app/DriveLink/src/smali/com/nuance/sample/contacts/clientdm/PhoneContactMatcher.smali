.class public Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;
.super Lcom/vlingo/core/internal/contacts/ContactMatcherBase;
.source "PhoneContactMatcher.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/core/internal/contacts/ContactMatchListener;
    .param p3, "actionType"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p4, "phoneTypes"    # [I
    .param p5, "emailTypes"    # [I
    .param p6, "socialTypes"    # [I
    .param p7, "addressTypes"    # [I
    .param p8, "query"    # Ljava/lang/String;
    .param p9, "confidenceScore"    # F
    .param p10, "autoActionType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/contacts/ContactMatchListener;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            "[I[I[I[I",
            "Ljava/lang/String;",
            "FI",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .line 29
    .local p11, "superList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-direct/range {p0 .. p11}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected getContactFetchAndSortRequest()Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
    .locals 6

    .prologue
    .line 35
    sget-object v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sac cr getContactFetchAndSortRequest()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    new-instance v0, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;

    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;->getQuery()Ljava/lang/String;

    move-result-object v2

    .line 38
    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p0}, Lcom/nuance/sample/contacts/clientdm/PhoneContactMatcher;->getPhoneTypes()[I

    move-result-object v4

    move-object v5, p0

    .line 37
    invoke-direct/range {v0 .. v5}, Lcom/nuance/sample/contacts/clientdm/PhoneContactFetchAndSortRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V

    return-object v0
.end method
