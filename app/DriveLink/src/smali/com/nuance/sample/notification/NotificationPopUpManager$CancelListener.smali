.class Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/notification/NotificationPopUpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CancelListener"
.end annotation


# instance fields
.field private notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

.field final synthetic this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;


# direct methods
.method public constructor <init>(Lcom/nuance/sample/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V
    .locals 0
    .param p2, "notificationPopUp"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    iput-object p2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 207
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 212
    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$2()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Notification was canceled:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->actionRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->decline()V

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->exitOnDecline()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/nuance/sample/notification/NotificationPopUpManager;->sendResult(I)V

    .line 225
    :goto_1
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->accept()V

    goto :goto_0

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$0(Lcom/nuance/sample/notification/NotificationPopUpManager;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 222
    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;
    invoke-static {}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$1()Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->removeNotificationFromContainer(Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    .line 223
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->showNextNotification()V

    goto :goto_1
.end method
