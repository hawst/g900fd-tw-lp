.class Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/notification/NotificationPopUpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeclineListener"
.end annotation


# instance fields
.field private notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

.field final synthetic this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;


# direct methods
.method public constructor <init>(Lcom/nuance/sample/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V
    .locals 0
    .param p2, "notificationPopUp"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 186
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 190
    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$2()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Notification was declined:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->decline()V

    .line 192
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->exitOnDecline()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/nuance/sample/notification/NotificationPopUpManager;->sendResult(I)V

    .line 199
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$0(Lcom/nuance/sample/notification/NotificationPopUpManager;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 196
    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;
    invoke-static {}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$1()Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->removeNotificationFromContainer(Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    .line 197
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->showNextNotification()V

    goto :goto_0
.end method
