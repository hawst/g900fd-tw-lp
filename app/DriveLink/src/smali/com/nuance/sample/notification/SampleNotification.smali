.class public Lcom/nuance/sample/notification/SampleNotification;
.super Lcom/vlingo/core/internal/notification/NotificationPopUp;
.source "SampleNotification.java"


# instance fields
.field private flagShown:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "notificationText"    # Ljava/lang/String;
    .param p3, "positiveButton"    # Ljava/lang/String;
    .param p4, "negativeButton"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 11
    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p3

    move v8, v7

    .line 12
    invoke-direct/range {v0 .. v8}, Lcom/vlingo/core/internal/notification/NotificationPopUp;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 7
    iput-boolean v2, p0, Lcom/nuance/sample/notification/SampleNotification;->flagShown:Z

    .line 13
    return-void
.end method


# virtual methods
.method public accept()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 17
    iput-boolean v0, p0, Lcom/nuance/sample/notification/SampleNotification;->flagShown:Z

    .line 18
    sput-boolean v0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->flagShown:Z

    .line 19
    return-void
.end method

.method public decline()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/sample/notification/SampleNotification;->flagShown:Z

    .line 24
    return-void
.end method

.method public isAccepted()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/nuance/sample/notification/SampleNotification;->flagShown:Z

    return v0
.end method
