.class Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/notification/NotificationPopUpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AcceptListener"
.end annotation


# instance fields
.field private log:Lcom/vlingo/core/internal/logging/Logger;

.field private notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

.field final synthetic this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;


# direct methods
.method public constructor <init>(Lcom/nuance/sample/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V
    .locals 1
    .param p2, "notificationPopUp"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    const-class v0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 165
    iput-object p2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 166
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Notification was accepted:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->accept()V

    .line 172
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->existDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$0(Lcom/nuance/sample/notification/NotificationPopUpManager;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 175
    :cond_0
    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;
    invoke-static {}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$1()Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0, v1}, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->removeNotificationFromContainer(Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    .line 176
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->showNextNotification()V

    .line 177
    return-void
.end method
