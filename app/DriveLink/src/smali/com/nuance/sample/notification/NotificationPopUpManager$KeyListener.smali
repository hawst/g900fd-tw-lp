.class Lcom/nuance/sample/notification/NotificationPopUpManager$KeyListener;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/notification/NotificationPopUpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KeyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;


# direct methods
.method constructor <init>(Lcom/nuance/sample/notification/NotificationPopUpManager;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager$KeyListener;->this$0:Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 233
    # getter for: Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/nuance/sample/notification/NotificationPopUpManager;->access$2()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Key on dialog: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 234
    const/16 v0, 0x54

    if-eq p2, v0, :cond_0

    .line 235
    const/16 v0, 0x52

    if-ne p2, v0, :cond_1

    .line 236
    :cond_0
    const/4 v0, 0x1

    .line 238
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
