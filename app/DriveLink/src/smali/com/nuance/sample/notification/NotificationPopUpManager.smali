.class public Lcom/nuance/sample/notification/NotificationPopUpManager;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;,
        Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;,
        Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;,
        Lcom/nuance/sample/notification/NotificationPopUpManager$KeyListener;
    }
.end annotation


# static fields
.field public static final ACCEPTED:I = 0x1

.field public static final CANCELED:I = 0x3

.field public static final DECLINED:I = 0x2

.field private static container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

.field private static log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field LOG_TAG:Ljava/lang/String;

.field private activityHandler:Landroid/os/Handler;

.field private context:Landroid/app/Activity;

.field private currentDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/nuance/sample/notification/NotificationPopUpManager;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    .line 30
    sput-object v0, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V
    .locals 1
    .param p1, "ctx"    # Landroid/app/Activity;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string/jumbo v0, "NotificationPopUpManager"

    iput-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->LOG_TAG:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 39
    iput-object p2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->activityHandler:Landroid/os/Handler;

    .line 40
    iput-object p1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    .line 41
    new-instance v0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    invoke-direct {v0, p3}, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    sput-object v0, Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/notification/NotificationPopUpManager;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1()Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    return-object v0
.end method

.method static synthetic access$2()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method private buildAlertNotificationDialog(Lcom/vlingo/core/internal/notification/NotificationPopUp;)Landroid/app/AlertDialog;
    .locals 4
    .param p1, "ntfc"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 142
    sget-object v1, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v2, "Building notification to show"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 143
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 144
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 145
    new-instance v1, Lcom/nuance/sample/notification/NotificationDialogView;

    iget-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    .line 146
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNotificationText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/nuance/sample/notification/NotificationDialogView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 147
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getPositiveButton()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getPositiveButton()Ljava/lang/String;

    move-result-object v1

    .line 149
    new-instance v2, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;

    invoke-direct {v2, p0, p1}, Lcom/nuance/sample/notification/NotificationPopUpManager$AcceptListener;-><init>(Lcom/nuance/sample/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    .line 148
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 151
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNegativeButton()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 152
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNegativeButton()Ljava/lang/String;

    move-result-object v1

    .line 153
    new-instance v2, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;

    invoke-direct {v2, p0, p1}, Lcom/nuance/sample/notification/NotificationPopUpManager$DeclineListener;-><init>(Lcom/nuance/sample/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    .line 152
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 155
    :cond_1
    new-instance v1, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;

    invoke-direct {v1, p0, p1}, Lcom/nuance/sample/notification/NotificationPopUpManager$CancelListener;-><init>(Lcom/nuance/sample/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    new-instance v1, Lcom/nuance/sample/notification/NotificationPopUpManager$KeyListener;

    invoke-direct {v1, p0}, Lcom/nuance/sample/notification/NotificationPopUpManager$KeyListener;-><init>(Lcom/nuance/sample/notification/NotificationPopUpManager;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 157
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private setShowingNotifications(Z)Z
    .locals 1
    .param p1, "showingNotifications"    # Z

    .prologue
    .line 65
    const-string/jumbo v0, "showing_notifications"

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 67
    return p1
.end method


# virtual methods
.method public dismissNotifications()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 138
    :cond_0
    return-void
.end method

.method public existDialog()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    :cond_0
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasNotifications(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)Z
    .locals 1
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 49
    new-instance v0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    invoke-direct {v0, p1}, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    sput-object v0, Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    .line 50
    invoke-virtual {p0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->needNotifications()Z

    move-result v0

    return v0
.end method

.method public needNotifications()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 54
    const-string/jumbo v1, "tos_accepted"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    .local v0, "showingNotifications":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 57
    return v0

    .line 54
    .end local v0    # "showingNotifications":Z
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public sendResult(I)V
    .locals 1
    .param p1, "result"    # I

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 130
    iget-object v0, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->activityHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 131
    return-void
.end method

.method public setContext(Landroid/app/Activity;)V
    .locals 0
    .param p1, "ctx"    # Landroid/app/Activity;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    .line 46
    return-void
.end method

.method public showNextNotification()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 78
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v3, "Trying to show next available notification"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 84
    iget-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v3, "Failed to show notification activity is finishing"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;)V

    .line 87
    invoke-direct {p0, v5}, Lcom/nuance/sample/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 121
    :goto_0
    return-void

    .line 91
    :cond_0
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    invoke-virtual {v2}, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->needNextNotification()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/nuance/sample/notification/NotificationPopUpManager;->existDialog()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v3, "Already showing notification"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 94
    invoke-direct {p0, v4}, Lcom/nuance/sample/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    goto :goto_0

    .line 98
    :cond_1
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->container:Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;

    invoke-virtual {v2}, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->getNextNotification()Lcom/vlingo/core/internal/notification/NotificationPopUp;

    move-result-object v1

    .line 99
    .local v1, "nextNotification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    if-eqz v1, :cond_2

    .line 100
    invoke-direct {p0, v4}, Lcom/nuance/sample/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 101
    invoke-direct {p0, v1}, Lcom/nuance/sample/notification/NotificationPopUpManager;->buildAlertNotificationDialog(Lcom/vlingo/core/internal/notification/NotificationPopUp;)Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 103
    :try_start_0
    iget-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Have notification to show. Showing notification: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 114
    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to show notification, stackTrace: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 108
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 107
    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;)V

    .line 109
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/nuance/sample/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 110
    invoke-direct {p0, v5}, Lcom/nuance/sample/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    goto :goto_0

    .line 118
    .end local v0    # "e":Landroid/view/WindowManager$BadTokenException;
    :cond_2
    sget-object v2, Lcom/nuance/sample/notification/NotificationPopUpManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v3, "No notifications to show, sending msg to continue work"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0, v4}, Lcom/nuance/sample/notification/NotificationPopUpManager;->sendResult(I)V

    goto :goto_0
.end method

.method public showNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp;Landroid/app/Activity;)V
    .locals 0
    .param p1, "notification"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 126
    return-void
.end method

.method public showingNotifications()Z
    .locals 2

    .prologue
    .line 61
    const-string/jumbo v0, "showing_notifications"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
