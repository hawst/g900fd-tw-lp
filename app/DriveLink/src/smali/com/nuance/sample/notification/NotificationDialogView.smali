.class Lcom/nuance/sample/notification/NotificationDialogView;
.super Landroid/widget/LinearLayout;
.source "NotificationDialogView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "notificationText"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 17
    invoke-virtual {p0}, Lcom/nuance/sample/notification/NotificationDialogView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030086

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 18
    invoke-direct {p0, p2}, Lcom/nuance/sample/notification/NotificationDialogView;->init(Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method private init(Ljava/lang/String;)V
    .locals 2
    .param p1, "notificationText"    # Ljava/lang/String;

    .prologue
    .line 23
    const v1, 0x7f09027b

    invoke-virtual {p0, v1}, Lcom/nuance/sample/notification/NotificationDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 33
    .local v0, "m_tosLink":Landroid/widget/TextView;
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 34
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 35
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 36
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-void
.end method
