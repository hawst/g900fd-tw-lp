.class public Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;
.super Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;
.source "SampleNotificationPopUpContainer.java"


# static fields
.field public static flagShown:Z


# instance fields
.field private currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->flagShown:Z

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V
    .locals 1
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    .line 20
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->NONE_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    iput-object v0, p0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 18
    return-void
.end method


# virtual methods
.method public getNextNotification()Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .locals 7

    .prologue
    .line 24
    const/4 v1, 0x0

    .line 25
    .local v1, "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    sget-boolean v5, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->flagShown:Z

    if-nez v5, :cond_0

    .line 26
    iget-object v5, p0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->context:Landroid/content/Context;

    const v6, 0x7f0a0839

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 27
    .local v4, "title":Ljava/lang/String;
    iget-object v5, p0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->context:Landroid/content/Context;

    .line 28
    const v6, 0x7f0a083a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "notificationText":Ljava/lang/String;
    iget-object v5, p0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->context:Landroid/content/Context;

    const v6, 0x7f0a0675

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 30
    .local v3, "positiveButton":Ljava/lang/String;
    iget-object v5, p0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->context:Landroid/content/Context;

    .line 31
    const v6, 0x7f0a083c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "negativeButton":Ljava/lang/String;
    new-instance v1, Lcom/nuance/sample/notification/SampleNotification;

    .end local v1    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    invoke-direct {v1, v4, v2, v3, v0}, Lcom/nuance/sample/notification/SampleNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .end local v0    # "negativeButton":Ljava/lang/String;
    .end local v2    # "notificationText":Ljava/lang/String;
    .end local v3    # "positiveButton":Ljava/lang/String;
    .end local v4    # "title":Ljava/lang/String;
    .restart local v1    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    :cond_0
    return-object v1
.end method

.method public needNextNotification()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 39
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->NONE_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 40
    .local v0, "s":Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;
    const-string/jumbo v2, "tos_accepted"

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->TOS_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 44
    :cond_0
    iget-object v2, p0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    if-eq v2, v0, :cond_1

    .line 45
    const/4 v1, 0x1

    .line 46
    :cond_1
    return v1
.end method

.method public removeNotificationFromContainer(Lcom/vlingo/core/internal/notification/NotificationPopUp;)V
    .locals 2
    .param p1, "notification"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 50
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getType()Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/nuance/sample/notification/SampleNotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 53
    :cond_0
    return-void
.end method
