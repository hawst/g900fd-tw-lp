.class public Lcom/nuance/sample/MainActivity;
.super Lcom/nuance/sample/SampleBaseActivity;
.source "MainActivity.java"

# interfaces
.implements Lcom/nuance/sample/MicStateMaster;
.implements Lcom/nuance/sample/UiUpdater;
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/MainActivity$UiFocusBroadcastReceiver;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$nuance$sample$MicState:[I = null

.field public static final ACTION_SAFEREADER_LAUNCH:Ljava/lang/String; = "ACTION_SAFEREADER_LAUNCH"

.field public static final EXTRA_MESSAGE_LIST:Ljava/lang/String; = "EXTRA_MESSAGE_LIST"

.field public static final EXTRA_TIME_SENT:Ljava/lang/String; = "EXTRA_TIME_SENT"

.field private static final TAG:Ljava/lang/String;

.field private static actionLog:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation
.end field

.field private static mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;


# instance fields
.field private mDisplayView:Landroid/widget/TextView;

.field private mEnergyBar:Landroid/widget/ProgressBar;

.field private mLocationButton:Landroid/widget/Button;

.field private mLocationOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

.field private mMessageButton:Landroid/widget/Button;

.field private mMessageOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

.field private mMicButton:Landroid/widget/Button;

.field private mMicOnClickListener:Lcom/nuance/sample/OnClickMicListenerImpl;

.field private mMicState:Lcom/nuance/sample/MicState;

.field private mMusicButton:Landroid/widget/Button;

.field private mMusicOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

.field private mPhoneButton:Landroid/widget/Button;

.field private mPhoneOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mWigetFactory:Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

.field private messages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

.field private uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static synthetic $SWITCH_TABLE$com$nuance$sample$MicState()[I
    .locals 3

    .prologue
    .line 89
    sget-object v0, Lcom/nuance/sample/MainActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/nuance/sample/MicState;->values()[Lcom/nuance/sample/MicState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-virtual {v1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/nuance/sample/MainActivity;->$SWITCH_TABLE$com$nuance$sample$MicState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    const-class v0, Lcom/nuance/sample/MainActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/nuance/sample/MainActivity;->actionLog:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/nuance/sample/SampleBaseActivity;-><init>()V

    .line 108
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/nuance/sample/MainActivity;->mMicState:Lcom/nuance/sample/MicState;

    .line 89
    return-void
.end method

.method static synthetic access$1(Lcom/nuance/sample/MainActivity;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getActionLog()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    sget-object v0, Lcom/nuance/sample/MainActivity;->actionLog:Ljava/util/List;

    return-object v0
.end method

.method public static getDialogFlowManager()Lcom/nuance/sample/coreaccess/DialogFlowManager;
    .locals 1

    .prologue
    .line 718
    sget-object v0, Lcom/nuance/sample/MainActivity;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    return-object v0
.end method

.method private isApplicationBroughtToBackground()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 763
    const-string/jumbo v3, "activity"

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 764
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 765
    .local v1, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 766
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 767
    .local v2, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    .line 772
    .end local v2    # "topActivity":Landroid/content/ComponentName;
    :goto_0
    return v3

    :cond_0
    move v3, v5

    goto :goto_0
.end method

.method public static prepareDeleteDataMenuItem(Landroid/view/Menu;)V
    .locals 6
    .param p0, "menu"    # Landroid/view/Menu;

    .prologue
    .line 445
    const v3, 0x7f0903a0

    invoke-interface {p0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 446
    .local v1, "item":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 447
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSpeakerID()Ljava/lang/String;

    move-result-object v2

    .line 448
    .local v2, "speakerID":Ljava/lang/String;
    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 449
    .local v0, "isEnabled":Z
    :goto_0
    sget-object v4, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onPrepareOptionsMenu: speakerID is "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 450
    const-string/jumbo v5, "; setting \'Delete All Personal Data\' to "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 451
    if-eqz v0, :cond_2

    const-string/jumbo v3, "enabled"

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 449
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 453
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 455
    .end local v0    # "isEnabled":Z
    .end local v2    # "speakerID":Ljava/lang/String;
    :cond_0
    return-void

    .line 448
    .restart local v2    # "speakerID":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 451
    .restart local v0    # "isEnabled":Z
    :cond_2
    const-string/jumbo v3, "disabled"

    goto :goto_1
.end method

.method private processSafeReaderMessages(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 229
    .line 230
    const-string/jumbo v2, "EXTRA_MESSAGE_LIST"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 229
    check-cast v1, Ljava/util/ArrayList;

    .line 231
    .local v1, "replyMessages":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/nuance/sample/MainActivity;->messages:Ljava/util/LinkedList;

    .line 233
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->messages:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 237
    new-instance v0, Ljava/util/LinkedList;

    .line 238
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->messages:Ljava/util/LinkedList;

    .line 237
    invoke-direct {v0, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 239
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->handleAlert(Ljava/util/LinkedList;)V

    .line 240
    const/4 v2, 0x1

    return v2
.end method

.method private updateUi(Ljava/lang/CharSequence;I)V
    .locals 5
    .param p1, "cs"    # Ljava/lang/CharSequence;
    .param p2, "color"    # I

    .prologue
    .line 608
    new-instance v0, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 609
    .local v0, "ssb":Landroid/text/SpannableStringBuilder;
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 610
    const/16 v4, 0x21

    .line 609
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 611
    iget-object v1, p0, Lcom/nuance/sample/MainActivity;->mDisplayView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 612
    iget-object v1, p0, Lcom/nuance/sample/MainActivity;->mScrollView:Landroid/widget/ScrollView;

    new-instance v2, Lcom/nuance/sample/MainActivity$1;

    invoke-direct {v2, p0}, Lcom/nuance/sample/MainActivity$1;-><init>(Lcom/nuance/sample/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 617
    return-void
.end method


# virtual methods
.method public displayError(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 574
    const/high16 v0, -0x10000

    invoke-direct {p0, p1, v0}, Lcom/nuance/sample/MainActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 575
    return-void
.end method

.method public displaySystemTurn(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "System (voice agent) said: \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/MainActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 591
    return-void
.end method

.method public displayUserTurn(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "User said: \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, -0xbbbbbc

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/MainActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 583
    return-void
.end method

.method public displayWidgetContent(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 599
    const v0, -0xffff01

    invoke-direct {p0, p1, v0}, Lcom/nuance/sample/MainActivity;->updateUi(Ljava/lang/CharSequence;I)V

    .line 600
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 4

    .prologue
    .line 558
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;-><init>()V

    .line 559
    .local v0, "builder":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 560
    .local v1, "generalSpeakables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const v3, 0x7f0a05c8

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 561
    const v3, 0x7f0a05c9

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 562
    const v3, 0x7f0a05ca

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 563
    const v3, 0x7f0a05cb

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->generalSpeakables(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    .line 565
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->build()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v2

    .line 566
    .local v2, "toReturn":Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    return-object v2
.end method

.method public getMicState()Lcom/nuance/sample/MicState;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->mMicState:Lcom/nuance/sample/MicState;

    return-object v0
.end method

.method public getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 382
    const-string/jumbo v1, "samsung_wakeup_engine_enable"

    .line 381
    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 382
    if-eqz v1, :cond_0

    .line 383
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    .line 384
    const-string/jumbo v2, "initPhraseSpotter Samsung Seamless and Samsung Multiple Wake-up"

    .line 385
    const/4 v3, 0x0

    .line 383
    invoke-static {v1, v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 386
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 387
    const-class v2, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;

    .line 386
    invoke-static {v1, v2}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 389
    invoke-static {}, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;->getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 403
    .local v0, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_0
    return-object v0

    .line 391
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_0
    const-string/jumbo v1, "samsung_multi_engine_enable"

    .line 390
    invoke-static {v1, v2}, Lcom/nuance/sample/settings/SampleAppSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 391
    if-eqz v1, :cond_1

    .line 392
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    .line 393
    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless and Samsung Multiple Wake-up"

    .line 392
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 395
    const-class v2, Lcom/nuance/sample/phraseSpotter/SamsungPhraseSpotter;

    .line 394
    invoke-static {v1, v2}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 397
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 398
    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0

    .line 399
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_1
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless Only"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/nuance/sample/phraseSpotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 400
    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0
.end method

.method public handleUserCancel()V
    .locals 0

    .prologue
    .line 642
    return-void
.end method

.method protected initPhraseSpotter()V
    .locals 2

    .prologue
    .line 375
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 376
    return-void
.end method

.method public onBluetoothServiceConnected()V
    .locals 0

    .prologue
    .line 731
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 127
    sget-object v3, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-super {p0, p1}, Lcom/nuance/sample/SampleBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    const v3, 0x7f030078

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->setContentView(I)V

    .line 132
    new-instance v3, Lcom/nuance/sample/MainActivity$UiFocusBroadcastReceiver;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/nuance/sample/MainActivity$UiFocusBroadcastReceiver;-><init>(Lcom/nuance/sample/MainActivity;Lcom/nuance/sample/MainActivity$UiFocusBroadcastReceiver;)V

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 133
    iget-object v3, p0, Lcom/nuance/sample/MainActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    .line 134
    const-string/jumbo v5, "com.nuance.sample.ACTION_UIFOCUS_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0, v3, v4}, Lcom/nuance/sample/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 135
    const v3, 0x7f090255

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 136
    const v3, 0x7f090256

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mDisplayView:Landroid/widget/TextView;

    .line 139
    const v3, 0x7f090252

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mMicButton:Landroid/widget/Button;

    .line 141
    iget-object v3, p0, Lcom/nuance/sample/MainActivity;->mMicButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/nuance/sample/MainActivity;->mMicOnClickListener:Lcom/nuance/sample/OnClickMicListenerImpl;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    const v3, 0x7f090257

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mPhoneButton:Landroid/widget/Button;

    .line 147
    new-instance v3, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 148
    sget-object v4, Lcom/nuance/sample/util/MainActivityButtonType;->PHONE:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;-><init>(Lcom/nuance/sample/util/MainActivityButtonType;Landroid/content/Context;)V

    .line 147
    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mPhoneOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 149
    iget-object v3, p0, Lcom/nuance/sample/MainActivity;->mPhoneButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/nuance/sample/MainActivity;->mPhoneOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    const v3, 0x7f0901a0

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mMessageButton:Landroid/widget/Button;

    .line 152
    new-instance v3, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 153
    sget-object v4, Lcom/nuance/sample/util/MainActivityButtonType;->MESSAGE:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;-><init>(Lcom/nuance/sample/util/MainActivityButtonType;Landroid/content/Context;)V

    .line 152
    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mMessageOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 154
    iget-object v3, p0, Lcom/nuance/sample/MainActivity;->mMessageButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/nuance/sample/MainActivity;->mMessageOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    const v3, 0x7f090258

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mMusicButton:Landroid/widget/Button;

    .line 157
    new-instance v3, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 158
    sget-object v4, Lcom/nuance/sample/util/MainActivityButtonType;->MUSIC:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;-><init>(Lcom/nuance/sample/util/MainActivityButtonType;Landroid/content/Context;)V

    .line 157
    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mMusicOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 159
    iget-object v3, p0, Lcom/nuance/sample/MainActivity;->mMusicButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/nuance/sample/MainActivity;->mMusicOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v3, 0x7f090259

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mLocationButton:Landroid/widget/Button;

    .line 162
    new-instance v3, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 163
    sget-object v4, Lcom/nuance/sample/util/MainActivityButtonType;->LOCATION:Lcom/nuance/sample/util/MainActivityButtonType;

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;-><init>(Lcom/nuance/sample/util/MainActivityButtonType;Landroid/content/Context;)V

    .line 162
    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mLocationOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    .line 164
    iget-object v3, p0, Lcom/nuance/sample/MainActivity;->mLocationButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/nuance/sample/MainActivity;->mLocationOnClickListener:Lcom/nuance/sample/OnClickHomeScreenButtonListenerImpl;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v3, 0x7f090251

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 172
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v3

    .line 173
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    .line 172
    invoke-interface {v3, v4, p0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 175
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->initPhraseSpotter()V

    .line 176
    new-instance v3, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, p0}, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;-><init>(Landroid/content/Context;Lcom/nuance/sample/UiUpdater;)V

    iput-object v3, p0, Lcom/nuance/sample/MainActivity;->mWigetFactory:Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

    .line 177
    new-instance v3, Lcom/nuance/sample/coreaccess/DialogFlowManager;

    iget-object v4, p0, Lcom/nuance/sample/MainActivity;->mWigetFactory:Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

    iget-object v5, p0, Lcom/nuance/sample/MainActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-direct {v3, v4, v5, p0, p0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;-><init>(Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Landroid/content/Context;)V

    sput-object v3, Lcom/nuance/sample/MainActivity;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    .line 179
    sget-object v3, Lcom/nuance/sample/MainActivity;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    invoke-virtual {v3}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->initFlow()V

    .line 181
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 182
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 183
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 184
    const-string/jumbo v3, "key"

    const-string/jumbo v4, "gottajibboocc"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 185
    .local v2, "key":Ljava/lang/String;
    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 186
    const/4 v3, 0x1

    sput-boolean v3, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->showDebugSettings:Z

    .line 192
    .end local v2    # "key":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 188
    .restart local v2    # "key":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    sput-boolean v3, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->showDebugSettings:Z

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    .line 419
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 420
    const v1, 0x7f09039f

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 421
    .local v0, "item":Landroid/view/MenuItem;
    sget-boolean v1, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->showDebugSettings:Z

    if-eqz v1, :cond_0

    .line 422
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 427
    :goto_0
    return v3

    .line 424
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 359
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerDestroy()V

    .line 362
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/nuance/sample/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 366
    :cond_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    .line 367
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    .line 366
    invoke-interface {v0, v1, p0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 369
    sget-object v0, Lcom/nuance/sample/MainActivity;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->destroyFlow()V

    .line 370
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;->showDebugSettings:Z

    .line 371
    invoke-super {p0}, Lcom/nuance/sample/SampleBaseActivity;->onDestroy()V

    .line 372
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 754
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 760
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 197
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 198
    packed-switch p1, :pswitch_data_0

    .line 206
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/nuance/sample/SampleBaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 201
    :pswitch_0
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->openOptionsMenu()V

    goto :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 220
    const-string/jumbo v0, "ACTION_SAFEREADER_LAUNCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppValues;->isTOSAccepted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-direct {p0, p1}, Lcom/nuance/sample/MainActivity;->processSafeReaderMessages(Landroid/content/Intent;)Z

    .line 225
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v11, 0x1

    .line 459
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onOptionsItemSelected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 513
    invoke-super {p0, p1}, Lcom/nuance/sample/SampleBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 462
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getCoreVersion()Ljava/lang/String;

    move-result-object v10

    .line 463
    .local v10, "vacVersion":Ljava/lang/String;
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "SampleAppCoreManager VACVersion \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 465
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    move v1, v11

    .line 466
    goto :goto_0

    .line 468
    .end local v10    # "vacVersion":Ljava/lang/String;
    :pswitch_1
    invoke-static {}, Lcom/nuance/sample/MainActivity;->getActionLog()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    .line 469
    .local v9, "size":I
    if-lez v9, :cond_0

    .line 470
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 471
    invoke-static {}, Lcom/nuance/sample/MainActivity;->getActionLog()Ljava/util/List;

    move-result-object v1

    add-int/lit8 v3, v9, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/recognition/VLAction;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 470
    invoke-static {v2, v1, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 472
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    move v1, v11

    .line 474
    goto :goto_0

    .line 480
    .end local v9    # "size":I
    :pswitch_2
    invoke-static {}, Lcom/nuance/sample/MainActivity;->getActionLog()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v6, v1, -0x1

    .local v6, "i":I
    :goto_1
    if-gez v6, :cond_1

    move v1, v11

    .line 485
    goto :goto_0

    .line 481
    :cond_1
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 482
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/nuance/sample/MainActivity;->getActionLog()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/recognition/VLAction;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 481
    invoke-static {v2, v1, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 483
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 480
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    .line 487
    .end local v6    # "i":I
    :pswitch_3
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 488
    .local v7, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/nuance/sample/debugsettings/DebugSettingsActivity;

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 489
    invoke-virtual {p0, v7}, Lcom/nuance/sample/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 490
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->finish()V

    move v1, v11

    .line 491
    goto/16 :goto_0

    .line 493
    .end local v7    # "intent":Landroid/content/Intent;
    :pswitch_4
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Delete All Personal Data menu option selected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    new-instance v0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    .line 500
    const v1, 0x7f0a0842

    invoke-virtual {p0, v1}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 501
    const v2, 0x7f0a0840

    invoke-virtual {p0, v2}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 502
    const v3, 0x7f0a062e

    invoke-virtual {p0, v3}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 503
    const v4, 0x7f0a0675

    invoke-virtual {p0, v4}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 504
    const v5, 0x7f0a0841

    invoke-virtual {p0, v5}, Lcom/nuance/sample/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 499
    invoke-direct/range {v0 .. v5}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    .local v0, "confirmDialog":Lcom/vlingo/midas/datadeletion/ConfirmDialog;
    invoke-virtual {v0, p0}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->showConfirmDialog(Landroid/content/Context;)V

    move v1, v11

    .line 506
    goto/16 :goto_0

    .line 508
    .end local v0    # "confirmDialog":Lcom/vlingo/midas/datadeletion/ConfirmDialog;
    :pswitch_5
    new-instance v8, Landroid/content/Intent;

    const-class v1, Lcom/nuance/sample/settings/SettingsLanguageScreen;

    invoke-direct {v8, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 509
    .local v8, "languageIntent":Landroid/content/Intent;
    const-string/jumbo v1, "is_from_svoice"

    invoke-virtual {v8, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 510
    invoke-virtual {p0, v8}, Lcom/nuance/sample/MainActivity;->startActivity(Landroid/content/Intent;)V

    move v1, v11

    .line 511
    goto/16 :goto_0

    .line 460
    :pswitch_data_0
    .packed-switch 0x7f09039c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 313
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onPause()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-super {p0}, Lcom/nuance/sample/SampleBaseActivity;->onPause()V

    .line 317
    invoke-static {v3}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->onBackgrounded(Z)V

    .line 318
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 320
    iget-object v1, p0, Lcom/nuance/sample/MainActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    if-eqz v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/nuance/sample/MainActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-static {}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getInstance()Lcom/nuance/sample/safereader/VACBackgroundHandler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->unregisterAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 323
    :cond_0
    sget-object v1, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[mute] onPause / setVolumeNormal"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-static {p0}, Lcom/nuance/sample/settings/SampleAppSettings;->resetVolumeNormal(Landroid/content/Context;)V

    .line 328
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_TURN_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 329
    const-class v2, Lcom/nuance/sample/handlers/clientdm/QuietUnhandledDomainHandler;

    .line 328
    invoke-static {v1, v2}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 330
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 332
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 333
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 334
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 336
    invoke-static {}, Lcom/nuance/sample/SampleAppApplication;->getInstance()Lcom/nuance/sample/SampleAppApplication;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/nuance/sample/SampleAppApplication;->setInForeground(Z)V

    .line 337
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 338
    sget-object v1, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-virtual {p0, v1}, Lcom/nuance/sample/MainActivity;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 350
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 351
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "extra_field_id"

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    return-void
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 2
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 646
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseDetected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 2
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 651
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->cancleSeuduleSpotter()V

    .line 654
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 655
    return-void
.end method

.method public onPhraseSpotterStarted()V
    .locals 2

    .prologue
    .line 714
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotter started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 2

    .prologue
    .line 671
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "phraseSpotter stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    .line 673
    invoke-virtual {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->isStartRecoOnSpotterStop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v0

    .line 675
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->setStartRecoOnSpotterStop(Z)V

    .line 676
    new-instance v0, Lcom/nuance/sample/MainActivity$3;

    invoke-direct {v0, p0}, Lcom/nuance/sample/MainActivity$3;-><init>(Lcom/nuance/sample/MainActivity;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 710
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 432
    invoke-static {p1}, Lcom/nuance/sample/MainActivity;->prepareDeleteDataMenuItem(Landroid/view/Menu;)V

    .line 434
    invoke-super {p0, p1}, Lcom/nuance/sample/SampleBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 636
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "AudioRequest was cancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 626
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "AudioRequest completed to play tts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 631
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "AudioRequest ignored to play tts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 621
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "AudioRequest (tts) will be played"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 249
    sget-object v2, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onResume()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    new-instance v2, Landroid/content/Intent;

    .line 252
    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 251
    invoke-static {p0, v5, v2}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->onForegrounded(Landroid/content/Context;ZLandroid/content/Intent;)Z

    move-result v2

    .line 252
    if-nez v2, :cond_0

    .line 253
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 256
    :cond_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 257
    .local v1, "lastSafereaderTime":Ljava/util/Date;
    const-string/jumbo v2, "car_safereader_last_saferead_time"

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/nuance/sample/settings/SampleAppSettings;->setLong(Ljava/lang/String;J)V

    .line 258
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppValues;->isTOSAccepted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 259
    invoke-static {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/sample/MainActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    .line 260
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-static {}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getInstance()Lcom/nuance/sample/safereader/VACBackgroundHandler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->registerAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 261
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->setPriority(Z)V

    .line 262
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->safereader:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->startSafeReading()V

    .line 263
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 266
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onUiShown()V

    .line 268
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/nuance/sample/uifocus/SampleUiFocusActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v5, v2}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->onForegrounded(Landroid/content/Context;ZLandroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 269
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->endpointReco()V

    .line 272
    :cond_2
    invoke-static {}, Lcom/nuance/sample/SampleAppApplication;->getInstance()Lcom/nuance/sample/SampleAppApplication;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/nuance/sample/SampleAppApplication;->setInForeground(Z)V

    .line 274
    invoke-static {}, Lcom/nuance/sample/settings/SampleAppSettings;->languageUpdateConfiguration()V

    .line 276
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v2

    const-wide/16 v3, 0x258

    invoke-virtual {v2, v3, v4}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    .line 278
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 279
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 280
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppValues;->isTOSAccepted()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 281
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->mMicButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 282
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->mPhoneButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 283
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->mMessageButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 284
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->mMusicButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 285
    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->mLocationButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 292
    :goto_0
    sget-object v2, Lcom/nuance/sample/MainActivity;->mDialogFlowManager:Lcom/nuance/sample/coreaccess/DialogFlowManager;

    iget-object v3, p0, Lcom/nuance/sample/MainActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/nuance/sample/MainActivity;->mWigetFactory:Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

    invoke-virtual {v2, v3, p0, v4}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->stealFlow(Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 294
    invoke-static {v5}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 295
    invoke-super {p0}, Lcom/nuance/sample/SampleBaseActivity;->onResume()V

    .line 296
    return-void

    .line 287
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/nuance/sample/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/nuance/sample/TosActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 288
    .local v0, "intent":Landroid/content/Intent;
    const v2, 0x10004000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 289
    invoke-static {}, Lcom/nuance/sample/SampleAppApplication;->getInstance()Lcom/nuance/sample/SampleAppApplication;

    move-result-object v2

    invoke-virtual {v2, p0, v0}, Lcom/nuance/sample/SampleAppApplication;->startInternalActivity(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onScoConnected()V
    .locals 2

    .prologue
    .line 735
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[LatencyCheck] onScoConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/nuance/sample/MainActivity;->setVolumeControlStream(I)V

    .line 737
    return-void
.end method

.method public onScoDisconnected()V
    .locals 2

    .prologue
    .line 741
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[LatencyCheck] onScoDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/nuance/sample/MainActivity;->setVolumeControlStream(I)V

    .line 743
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->disconnectScoByIdle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 744
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 745
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 746
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 748
    :cond_0
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 660
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->stopPhraseSpotting()V

    .line 662
    new-instance v0, Lcom/nuance/sample/MainActivity$2;

    invoke-direct {v0, p0, p2}, Lcom/nuance/sample/MainActivity$2;-><init>(Lcom/nuance/sample/MainActivity;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 667
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0}, Lcom/nuance/sample/SampleBaseActivity;->onStart()V

    .line 211
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 300
    invoke-super {p0}, Lcom/nuance/sample/SampleBaseActivity;->onStop()V

    .line 303
    invoke-direct {p0}, Lcom/nuance/sample/MainActivity;->isApplicationBroughtToBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-static {}, Lcom/nuance/sample/services/SampleServicesUtil;->getInstance()Lcom/nuance/sample/services/SampleServicesUtil;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/sample/services/SampleServicesUtil;->stopSafereaderService(Z)V

    .line 306
    :cond_0
    return-void
.end method

.method public onTaskWaitingToStart(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 2
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 723
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onTaskWaitingToStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 725
    return-void
.end method

.method public updateMicRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 780
    return-void
.end method

.method public updateMicState(Lcom/nuance/sample/MicState;)V
    .locals 4
    .param p1, "newState"    # Lcom/nuance/sample/MicState;

    .prologue
    const/4 v3, 0x0

    .line 523
    sget-object v0, Lcom/nuance/sample/MainActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "updateMicState() from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/sample/MainActivity;->mMicState:Lcom/nuance/sample/MicState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iput-object p1, p0, Lcom/nuance/sample/MainActivity;->mMicState:Lcom/nuance/sample/MicState;

    .line 525
    invoke-static {}, Lcom/nuance/sample/MainActivity;->$SWITCH_TABLE$com$nuance$sample$MicState()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/nuance/sample/MicState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 540
    :goto_0
    return-void

    .line 528
    :pswitch_0
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 529
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 533
    :pswitch_1
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 537
    :pswitch_2
    iget-object v0, p0, Lcom/nuance/sample/MainActivity;->mEnergyBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 525
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
