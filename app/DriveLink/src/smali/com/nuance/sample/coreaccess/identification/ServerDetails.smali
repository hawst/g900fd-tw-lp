.class public Lcom/nuance/sample/coreaccess/identification/ServerDetails;
.super Ljava/lang/Object;
.source "ServerDetails.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/CoreServerInfo;


# static fields
.field public static final DEFAULT_ASR_SERVER_HOST:Ljava/lang/String; = "samsungcckasr.vlingo.com"

.field public static final DEFAULT_HELLO_SERVER_HOST:Ljava/lang/String; = "samsungcckasr.vlingo.com"

.field public static final DEFAULT_LMTT_SERVER_HOST:Ljava/lang/String; = "samsungtlmtt.vlingo.com"

.field public static final DEFAULT_LOG_SERVER_HOST:Ljava/lang/String; = "samsungcckasr.vlingo.com"

.field public static final DEFAULT_TTS_SERVER_HOST:Ljava/lang/String;

.field public static final DEFAULT_VCS_SERVER_HOST:Ljava/lang/String; = "samsungcckvcs.vlingo.com"

.field private static smInstance:Lcom/nuance/sample/coreaccess/identification/ServerDetails;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->DEFAULT_TTS_SERVER_HOST:Ljava/lang/String;

    .line 16
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static getInstance()Lcom/nuance/sample/coreaccess/identification/ServerDetails;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->smInstance:Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    invoke-direct {v0}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;-><init>()V

    sput-object v0, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->smInstance:Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    .line 25
    :cond_0
    sget-object v0, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->smInstance:Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    return-object v0
.end method

.method private getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-static {p1, p2}, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->getNduHost(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 61
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getASRHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    const-string/jumbo v0, "SERVER_NAME"

    const-string/jumbo v1, "samsungcckasr.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHelloHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    const-string/jumbo v0, "HELLO_HOST_NAME"

    .line 56
    const-string/jumbo v1, "samsungcckasr.vlingo.com"

    .line 55
    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLMTTHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    const-string/jumbo v0, "LMTT_HOST_NAME"

    const-string/jumbo v1, "samsungtlmtt.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLogHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    const-string/jumbo v0, "EVENTLOG_HOST_NAME"

    const-string/jumbo v1, "samsungcckasr.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTTSHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVCSHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    const-string/jumbo v0, "SERVICES_HOST_NAME"

    const-string/jumbo v1, "samsungcckvcs.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
