.class public Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;
.super Ljava/lang/Object;
.source "SampleAppPhraseSpotterManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;
    }
.end annotation


# static fields
.field private static final DELAY_TIME:J = 0x190L

.field private static final MSG_START_SPOTTER:I = 0x0

.field private static final MSG_START_SPOTTER_DELAYED_PROC:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static singletonObject:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;


# instance fields
.field private mHandler:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

.field private mMicState:Lcom/nuance/sample/MicState;

.field private startRecoOnSpotterStop:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    .line 24
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 23
    sput-object v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->TAG:Ljava/lang/String;

    .line 30
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-object v0, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->mMicState:Lcom/nuance/sample/MicState;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->startRecoOnSpotterStop:Z

    .line 54
    new-instance v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

    .line 55
    invoke-direct {v0, p0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;-><init>(Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;)V

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->mHandler:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

    .line 35
    return-void
.end method

.method public static getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->singletonObject:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    invoke-direct {v0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;-><init>()V

    sput-object v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->singletonObject:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    .line 41
    :cond_0
    sget-object v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->singletonObject:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    return-object v0
.end method


# virtual methods
.method public cancleSeuduleSpotter()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->mHandler:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->removeMessages(I)V

    .line 76
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->mHandler:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->removeMessages(I)V

    .line 78
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->stopPhraseSpotting()V

    .line 79
    return-void
.end method

.method public isStartRecoOnSpotterStop()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->startRecoOnSpotterStop:Z

    return v0
.end method

.method public scheduleStartSpotter(J)V
    .locals 3
    .param p1, "delay"    # J

    .prologue
    const/4 v2, 0x0

    .line 59
    sget-object v0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "[mute] scheduleStartSpotter / setVolumeNormal"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/nuance/sample/settings/SampleAppSettings;->setVolumeNormal(Landroid/content/Context;)V

    .line 68
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->mHandler:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

    invoke-virtual {v0, v2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->removeMessages(I)V

    .line 69
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->mHandler:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->removeMessages(I)V

    .line 71
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->mHandler:Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;

    invoke-virtual {v0, v2, p1, p2}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 72
    return-void
.end method

.method public setStartRecoOnSpotterStop(Z)V
    .locals 0
    .param p1, "startRecoOnSpotterStop"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->startRecoOnSpotterStop:Z

    .line 52
    return-void
.end method
