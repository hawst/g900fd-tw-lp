.class public Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;
.super Ljava/lang/Object;
.source "DialogFlowListenerImpl.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;


# static fields
.field private static synthetic $SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

.field private static synthetic $SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionStates:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mEnergyBar:Landroid/widget/ProgressBar;

.field private mErrorCode:Lcom/nuance/sample/util/ErrorCode;

.field private mRecoState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

.field private mUiUpdater:Lcom/nuance/sample/UiUpdater;


# direct methods
.method static synthetic $SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->$SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->values()[Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->$SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionStates()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->$SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->values()[Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->GETTING_READY:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->$SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;

    .line 36
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 35
    sput-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/nuance/sample/UiUpdater;Landroid/widget/ProgressBar;)V
    .locals 0
    .param p1, "mUiUpdater"    # Lcom/nuance/sample/UiUpdater;
    .param p2, "mEnergyBar"    # Landroid/widget/ProgressBar;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    .line 45
    iput-object p2, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 46
    return-void
.end method


# virtual methods
.method public enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V
    .locals 0
    .param p1, "domain"    # Lcom/vlingo/core/internal/domain/DomainName;

    .prologue
    .line 291
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    invoke-interface {v0}, Lcom/nuance/sample/UiUpdater;->getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    move-result-object v0

    return-object v0
.end method

.method public onASRRecorderClosed()V
    .locals 3

    .prologue
    .line 281
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    .line 282
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 283
    .local v0, "context":Landroid/content/Context;
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[mute] onASRRecorderClosed() / setVolumeNormal"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    invoke-static {v0}, Lcom/nuance/sample/settings/SampleAppSettings;->setVolumeNormal(Landroid/content/Context;)V

    .line 285
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 2

    .prologue
    .line 275
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "ASR_mMicStream is created"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    return-void
.end method

.method public onInterceptStartReco()Z
    .locals 2

    .prologue
    .line 227
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onInterceptStartReco()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public onRecoCancelled()V
    .locals 3

    .prologue
    .line 219
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onRecoCancelled()"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    .line 221
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 222
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/nuance/sample/settings/SampleAppSettings;->setVolumeNormal(Landroid/content/Context;)V

    .line 223
    return-void
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 269
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 3
    .param p1, "startTone"    # Z

    .prologue
    .line 245
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[mute] onRecoToneStopped() / setVolumeMute"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getHomeMode()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "HOME_MODE_MULTIWINDOW"

    if-ne v1, v2, :cond_0

    .line 249
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    .line 250
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 251
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/nuance/sample/settings/SampleAppSettings;->setVolumeMute(Landroid/content/Context;)V

    .line 264
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    return-void
.end method

.method public onResultsNoAction()V
    .locals 2

    .prologue
    .line 95
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onResultsNoAction()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void
.end method

.method public showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V
    .locals 4
    .param p1, "newState"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .prologue
    .line 123
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "showDialogFlowStateChange() newState="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->$SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState()[I

    move-result-object v1

    .line 124
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 127
    :pswitch_0
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "showDialogFlowStateChange() newState="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 129
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 131
    .local v0, "context":Landroid/content/Context;
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    .line 132
    const-string/jumbo v2, "[mute] showDialogFlowStateChange(), idle / setVolumeNormal"

    .line 131
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-static {v0}, Lcom/nuance/sample/settings/SampleAppSettings;->setVolumeNormal(Landroid/content/Context;)V

    .line 134
    iget-object v1, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    sget-object v2, Lcom/nuance/sample/MicState;->IDLE:Lcom/nuance/sample/MicState;

    invoke-interface {v1, v2}, Lcom/nuance/sample/UiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 136
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isInDmFlowChanging()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 137
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    .line 138
    const-string/jumbo v2, "retrun : Don\'t call stopScoOnIdle in DmFlowChanging!!"

    .line 23
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 145
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    .line 146
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "showDialogFlowStateChange() don\'t abandon focus - dmFlow:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 147
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isInDmFlowChanging()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 148
    const-string/jumbo v3, ", Bluetooth SCO on:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 149
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 146
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 145
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_1
    :goto_2
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isAppInForeground()Z

    move-result v1

    if-nez v1, :cond_4

    .line 158
    const-string/jumbo v1, "DLPhraseSpotter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[cancel] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 160
    const-string/jumbo v3, " - showDialogFlowStateChange() - app is background"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 140
    :cond_2
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Stop Bluetooth sco"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    goto :goto_1

    .line 152
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentActiviy()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "PrepareDialActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 153
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->audioFocusManager(Landroid/content/Context;)Lcom/vlingo/core/facade/audio/IAudioFocusManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/audio/IAudioFocusManager;->abandonAudioFocus()V

    goto :goto_2

    .line 164
    :cond_4
    const-string/jumbo v1, "DM_DIAL"

    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentFlow()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 165
    const-string/jumbo v1, "DLPhraseSpotter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[cancel] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 167
    const-string/jumbo v3, " - showDialogFlowStateChange() - DM_DIAL"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 165
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 171
    :cond_5
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getCurrentActiviy()Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/common/base/BaseActivity;->getActivityFlowID()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    .line 172
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 173
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[cancel] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 175
    const-string/jumbo v3, " - showDialogFlowStateChange() - activity flowID : null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 173
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 179
    :cond_6
    const-string/jumbo v1, "DLPhraseSpotter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "[start] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 181
    const-string/jumbo v3, " - showDialogFlowStateChange()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 179
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-static {}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->getIntance()Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    move-result-object v1

    .line 183
    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;->scheduleStartSpotter(J)V

    goto/16 :goto_0

    .line 187
    .end local v0    # "context":Landroid/content/Context;
    :pswitch_1
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 190
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelink/music/MusicService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Force set Audio Focus!!!"

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-static {}, Lcom/sec/android/automotive/drivelink/music/MUSIC;->getService()Lcom/sec/android/automotive/drivelink/music/MusicService;

    move-result-object v1

    .line 193
    const/4 v2, -0x2

    .line 192
    invoke-virtual {v1, v2}, Lcom/sec/android/automotive/drivelink/music/MusicService;->onAudioFocusChange(I)V

    goto/16 :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 4
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "showError() error="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "errorMsg":Ljava/lang/String;
    sget-object v2, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 55
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v2, "VAC_DRIVELINK"

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    invoke-static {p1, v0}, Lcom/nuance/sample/util/ErrorCodeUtils;->getLocalizedMessageForErrorCode(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Landroid/content/Context;)Lcom/nuance/sample/util/ErrorCode;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mErrorCode:Lcom/nuance/sample/util/ErrorCode;

    .line 57
    sget-object v2, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    iget-object v3, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mErrorCode:Lcom/nuance/sample/util/ErrorCode;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-static {p1}, Lcom/nuance/sample/util/ErrorCodeUtils;->shouldReportError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/nuance/drivelink/DLAppUiUpdater;->setTTSWillPlay(Z)V

    .line 60
    iget-object v2, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    iget-object v3, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mErrorCode:Lcom/nuance/sample/util/ErrorCode;

    invoke-virtual {v3}, Lcom/nuance/sample/util/ErrorCode;->getErrorString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/nuance/sample/UiUpdater;->displayError(Ljava/lang/CharSequence;)V

    .line 61
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mErrorCode:Lcom/nuance/sample/util/ErrorCode;

    invoke-virtual {v3}, Lcom/nuance/sample/util/ErrorCode;->getErrorString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->tts(Ljava/lang/String;)V

    .line 64
    :cond_0
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setInDmFlowChanging(Z)V

    .line 66
    :cond_1
    return-void
.end method

.method public showRMSChange(I)V
    .locals 3
    .param p1, "rmsValue"    # I

    .prologue
    .line 77
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "showRMSChange() rmsValue="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const-string/jumbo v0, "VAC_DRIVELINK"

    invoke-static {v0}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isTrue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    invoke-interface {v0, p1}, Lcom/nuance/sample/UiUpdater;->updateMicRMSChange(I)V

    .line 86
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method public showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V
    .locals 2
    .param p1, "results"    # Lcom/vlingo/core/internal/logging/EventLog;

    .prologue
    .line 90
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "showReceivedResults()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return-void
.end method

.method public showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 4
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    .line 102
    sget-object v1, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "showRecoStateChange() newState="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mRecoState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .line 104
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    .line 105
    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 107
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->$SWITCH_TABLE$com$vlingo$sdk$recognition$VLRecognitionStates()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 119
    :goto_0
    return-void

    .line 109
    :pswitch_0
    iget-object v1, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    sget-object v2, Lcom/nuance/sample/MicState;->LISTENING:Lcom/nuance/sample/MicState;

    invoke-interface {v1, v2}, Lcom/nuance/sample/UiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    .line 110
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->setIsWakeup(Landroid/content/Context;Z)V

    goto :goto_0

    .line 115
    :pswitch_1
    iget-object v1, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    sget-object v2, Lcom/nuance/sample/MicState;->THINKING:Lcom/nuance/sample/MicState;

    invoke-interface {v1, v2}, Lcom/nuance/sample/UiUpdater;->updateMicState(Lcom/nuance/sample/MicState;)V

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    .line 211
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "showUserText() text="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    if-eqz p1, :cond_0

    const-string/jumbo v0, "null"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    invoke-interface {v0, p1}, Lcom/nuance/sample/UiUpdater;->displayUserTurn(Ljava/lang/CharSequence;)V

    .line 215
    :cond_0
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 203
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "showVlingoText() text="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    if-eqz p1, :cond_0

    const-string/jumbo v0, "null"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    invoke-interface {v0, p1}, Lcom/nuance/sample/UiUpdater;->displaySystemTurn(Ljava/lang/CharSequence;)V

    .line 207
    :cond_0
    return-void
.end method

.method public showWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
    .locals 3
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 71
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "showWarning() warning="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public userCancel()V
    .locals 2

    .prologue
    .line 233
    sget-object v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "userCancel()"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    invoke-interface {v0}, Lcom/nuance/sample/UiUpdater;->handleUserCancel()V

    .line 235
    return-void
.end method
