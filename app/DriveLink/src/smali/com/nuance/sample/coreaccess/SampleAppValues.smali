.class public Lcom/nuance/sample/coreaccess/SampleAppValues;
.super Ljava/lang/Object;
.source "SampleAppValues.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;


# static fields
.field private static final log:Lcom/vlingo/core/facade/logging/ILogger;

.field static mWindowManager:Landroid/view/IWindowManager;

.field static supportServiceManager:Z


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/nuance/sample/coreaccess/SampleAppValues;->mWindowManager:Landroid/view/IWindowManager;

    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/nuance/sample/coreaccess/SampleAppValues;->supportServiceManager:Z

    .line 66
    const-class v0, Lcom/nuance/sample/coreaccess/SampleAppValues;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    .line 65
    sput-object v0, Lcom/nuance/sample/coreaccess/SampleAppValues;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/SampleAppValues;->context:Landroid/content/Context;

    .line 70
    return-void
.end method

.method public static isTOSAccepted()Z
    .locals 1

    .prologue
    .line 382
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->termsManager()Lcom/vlingo/core/facade/terms/ITermsManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/terms/ITermsManager;->isTOSAccepted()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public disableAppCarMode()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public disablePhoneDrivingMode()V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppValues;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->disablePhoneDrivingMode(Landroid/content/Context;)V

    .line 215
    return-void
.end method

.method public enableAppCarMode()V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public enablePhoneDrivingMode()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppValues;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->enablePhoneDrivingMode(Landroid/content/Context;)V

    .line 226
    return-void
.end method

.method public getADMController()Lcom/vlingo/core/internal/util/ADMController;
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAssets()Landroid/content/res/AssetManager;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppValues;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    return-object v0
.end method

.method public getCarModePackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 481
    const-string/jumbo v0, "com.sec.android.automotive.drivelink"

    return-object v0
.end method

.method public getClientAudioValues()Lcom/vlingo/core/facade/audio/ClientAudioValues;
    .locals 1

    .prologue
    .line 414
    new-instance v0, Lcom/nuance/sample/coreaccess/SampleAppValues$1;

    invoke-direct {v0, p0}, Lcom/nuance/sample/coreaccess/SampleAppValues$1;-><init>(Lcom/nuance/sample/coreaccess/SampleAppValues;)V

    return-object v0
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppValues;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method public getContactProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 466
    invoke-static {}, Lcom/vlingo/core/facade/lmtt/MdsoUtils;->getMasterContactProviderAuthority()Ljava/lang/String;

    move-result-object v0

    .line 467
    .local v0, "masterAuthority":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 468
    const-string/jumbo v0, "com.vlingo.midas.contacts.contentprovider.cc"

    .line 470
    .end local v0    # "masterAuthority":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppValues;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 435
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v0}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDrivingModeWidgetMax()I
    .locals 1

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/nuance/sample/coreaccess/SampleAppValues;->getMaxDisplayNumber()I

    move-result v0

    return v0
.end method

.method public getFmRadioApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 283
    invoke-static {}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getInstance()Lcom/nuance/sample/safereader/VACBackgroundHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 285
    return-void
.end method

.method public getHomeAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    const-string/jumbo v0, "car_nav_home_address"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchingClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 119
    const-class v0, Lcom/nuance/sample/MainActivity;

    return-object v0
.end method

.method public getMaxDisplayNumber()I
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x3

    return v0
.end method

.method public getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMmsBodyText(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 136
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/facade/CoreRegistrar;->getAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v3

    .line 137
    .local v3, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v4, 0x0

    .line 138
    .local v4, "mmsUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    const/4 v1, 0x0

    .line 139
    .local v1, "body":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 141
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/util/MsgUtil;

    move-object v4, v0

    .line 142
    if-eqz v4, :cond_0

    .line 143
    invoke-interface {v4, p1}, Lcom/vlingo/core/internal/util/MsgUtil;->getMmsTextByCursor(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 152
    :cond_0
    :goto_0
    return-object v1

    .line 145
    :catch_0
    move-exception v2

    .line 146
    .local v2, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 147
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v2

    .line 148
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMmsSubject(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 130
    const-string/jumbo v0, "sub"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegularWidgetMax()I
    .locals 1

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/nuance/sample/coreaccess/SampleAppValues;->getMaxDisplayNumber()I

    move-result v0

    return v0
.end method

.method public getRelativePriority()B
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x2

    return v0
.end method

.method public getSafeReaderHandler(ZLjava/util/LinkedList;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    .locals 3
    .param p1, "isSilentHandler"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;"
        }
    .end annotation

    .prologue
    .line 268
    .local p2, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const/4 v0, 0x0

    .line 269
    .local v0, "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v1

    .line 270
    sget-object v2, Lcom/nuance/sample/CCFieldIds;->CC_VP_CAR_SAFEREADER_READBACKMSG:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v2}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    new-instance v0, Lcom/nuance/sample/handlers/SampleMsgChoiceHandler;

    .end local v0    # "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    invoke-direct {v0, p1}, Lcom/nuance/sample/handlers/SampleMsgChoiceHandler;-><init>(Z)V

    .restart local v0    # "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    move-object v1, v0

    .line 272
    check-cast v1, Lcom/nuance/sample/handlers/SampleMsgChoiceHandler;

    invoke-virtual {v1, p2}, Lcom/nuance/sample/handlers/SampleMsgChoiceHandler;->init(Ljava/util/Queue;)V

    .line 278
    :goto_0
    return-object v0

    .line 274
    :cond_0
    new-instance v0, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;

    .end local v0    # "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    invoke-direct {v0, p1}, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;-><init>(Z)V

    .restart local v0    # "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    move-object v1, v0

    .line 275
    check-cast v1, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;

    invoke-virtual {v1, p2}, Lcom/nuance/sample/handlers/SampleIncomingAlertHandler;->init(Ljava/util/Queue;)V

    goto :goto_0
.end method

.method public getSettingsProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 461
    const-string/jumbo v0, "com.vlingo.client.vlingoconfigprovider.cc"

    return-object v0
.end method

.method public getWakeLockManager()Lcom/vlingo/core/internal/display/WakeLockManager;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAppCarModeEnabled()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    return v0
.end method

.method public isBlockingMode()Z
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    return v0
.end method

.method public isChinesePhone()Z
    .locals 1

    .prologue
    .line 492
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isChinesePhone()Z

    move-result v0

    return v0
.end method

.method public isCurrentModelNotExactlySynchronizedWithPlayingTts()Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method public isDSPSeamlessWakeupEnabled()Z
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x0

    return v0
.end method

.method public isEmbeddedBrowserUsedForSearchResults()Z
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    return v0
.end method

.method public isEyesFree()Z
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x1

    return v0
.end method

.method public isGearProviderExistOnPhone()Z
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x0

    return v0
.end method

.method public isIUXComplete()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method public isInDmFlowChanging()Z
    .locals 1

    .prologue
    .line 440
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isInDmFlowChanging()Z

    move-result v0

    return v0
.end method

.method public isLanguageChangeAllowed()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public isMdsoApplication()Z
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x1

    return v0
.end method

.method public isMessageReadbackFlowEnabled()Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    return v0
.end method

.method public isMessagingLocked()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public isMiniTabletDevice()Z
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public isPhoneDrivingModeEnabled()Z
    .locals 2

    .prologue
    .line 197
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->isSystemDrivingModeEnabled()Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 202
    :goto_0
    return v1

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 202
    invoke-virtual {p0}, Lcom/nuance/sample/coreaccess/SampleAppValues;->isAppCarModeEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method public isReadMessageBodyEnabled()Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x1

    return v0
.end method

.method public isSeamless()Z
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x0

    return v0
.end method

.method public isTalkbackOn()Z
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppValues;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/utils/TalkbackUtils;->isTalkbackEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isTutorialMode()Z
    .locals 1

    .prologue
    .line 450
    const/4 v0, 0x0

    return v0
.end method

.method public isVideoCallingSupported()Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    return v0
.end method

.method public isViewCoverOpened()Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 352
    const/4 v1, 0x1

    .line 353
    .local v1, "covered":Z
    sget-object v5, Lcom/nuance/sample/coreaccess/SampleAppValues;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v5, :cond_1

    .line 355
    :try_start_0
    const-string/jumbo v5, "android.os.ServiceManager"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 356
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v5, "getService"

    .line 357
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    .line 356
    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 358
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 359
    const-string/jumbo v8, "window"

    aput-object v8, v6, v7

    .line 358
    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 360
    .local v4, "result":Ljava/lang/Object;
    instance-of v5, v4, Landroid/os/IBinder;

    if-eqz v5, :cond_0

    .line 362
    check-cast v4, Landroid/os/IBinder;

    .end local v4    # "result":Ljava/lang/Object;
    invoke-static {v4}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v5

    .line 361
    sput-object v5, Lcom/nuance/sample/coreaccess/SampleAppValues;->mWindowManager:Landroid/view/IWindowManager;

    .line 366
    :cond_0
    const/4 v5, 0x1

    sput-boolean v5, Lcom/nuance/sample/coreaccess/SampleAppValues;->supportServiceManager:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "get":Ljava/lang/reflect/Method;
    :cond_1
    :goto_0
    return v1

    .line 367
    :catch_0
    move-exception v3

    .line 368
    .local v3, "ignored":Ljava/lang/Throwable;
    sput-boolean v9, Lcom/nuance/sample/coreaccess/SampleAppValues;->supportServiceManager:Z

    goto :goto_0
.end method

.method public releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 290
    invoke-static {}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getInstance()Lcom/nuance/sample/safereader/VACBackgroundHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/nuance/sample/safereader/VACBackgroundHandler;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 292
    return-void
.end method

.method public setInDmFlowChanging(Z)V
    .locals 2
    .param p1, "inDmFlowChanging"    # Z

    .prologue
    .line 445
    const-string/jumbo v0, "setInDmFlowChanging"

    const-string/jumbo v1, "Core Calls - invlid"

    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    return-void
.end method

.method public shouldIncomingMessagesReadout()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 177
    const/4 v0, 0x1

    .line 179
    .local v0, "isIUXCompleted":Z
    const-string/jumbo v3, "tos_accepted"

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 182
    .local v1, "isTOSAccepted":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/nuance/sample/coreaccess/SampleAppValues;->isPhoneDrivingModeEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public shouldSetWideBandSpeechToUse16khzVoice()Z
    .locals 1

    .prologue
    .line 399
    const/4 v0, 0x0

    return v0
.end method

.method public showViewCoverUi()V
    .locals 0

    .prologue
    .line 388
    return-void
.end method

.method public supportsSVoiceAssociatedServiceOnly()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public updateCurrentLocale(Landroid/content/res/Resources;)V
    .locals 0
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 255
    return-void
.end method

.method public useGoogleSearch()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    return v0
.end method
