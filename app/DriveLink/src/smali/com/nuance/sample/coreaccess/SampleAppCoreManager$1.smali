.class Lcom/nuance/sample/coreaccess/SampleAppCoreManager$1;
.super Ljava/lang/Object;
.source "SampleAppCoreManager.java"

# interfaces
.implements Lcom/vlingo/core/facade/IClientInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->initCore()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;


# direct methods
.method constructor <init>(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$1;->this$0:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string/jumbo v0, "com.samsung.android.cc.kproject"

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    const-string/jumbo v0, "SamsungCCKProject"

    return-object v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const-string/jumbo v0, "99999"

    return-object v0
.end method

.method public getRecognitionMode()Lcom/vlingo/core/facade/RecognitionMode;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/vlingo/core/facade/RecognitionMode;->CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

    return-object v0
.end method

.method public getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$1;->this$0:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    # getter for: Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mResIdProvider:Lcom/vlingo/core/internal/ResourceIdProvider;
    invoke-static {v0}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->access$0(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    return-object v0
.end method

.method public getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServerDetails()Lcom/vlingo/core/internal/util/CoreServerInfo;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$1;->this$0:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    # getter for: Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mServerDetails:Lcom/nuance/sample/coreaccess/identification/ServerDetails;
    invoke-static {v0}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->access$2(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    move-result-object v0

    return-object v0
.end method

.method public getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$1;->this$0:Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    # getter for: Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    invoke-static {v0}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->access$1(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v0

    return-object v0
.end method

.method public isUseEmbeddedDialogManager()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method
