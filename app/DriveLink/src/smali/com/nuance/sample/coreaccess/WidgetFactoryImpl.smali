.class public Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;
.super Ljava/lang/Object;
.source "WidgetFactoryImpl.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;


# static fields
.field private static synthetic $SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

.field private static final TAG:Ljava/lang/String;

.field public static msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mUiUpdater:Lcom/nuance/sample/UiUpdater;


# direct methods
.method static synthetic $SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey()[I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->$SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->values()[Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_36

    :goto_1
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_35

    :goto_2
    :try_start_2
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AnswerQuestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_34

    :goto_3
    :try_start_3
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeEmail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_33

    :goto_4
    :try_start_4
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_32

    :goto_5
    :try_start_5
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeSocialStatus:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_31

    :goto_6
    :try_start_6
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_30

    :goto_7
    :try_start_7
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2f

    :goto_8
    :try_start_8
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2e

    :goto_9
    :try_start_9
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2d

    :goto_a
    :try_start_a
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2c

    :goto_b
    :try_start_b
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2b

    :goto_c
    :try_start_c
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2a

    :goto_d
    :try_start_d
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_29

    :goto_e
    :try_start_e
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Map:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_28

    :goto_f
    :try_start_f
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_27

    :goto_10
    :try_start_10
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MemoList:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_26

    :goto_11
    :try_start_11
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_25

    :goto_12
    :try_start_12
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadbackBodyHidden:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_24

    :goto_13
    :try_start_13
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageDisplay:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_23

    :goto_14
    :try_start_14
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_22

    :goto_15
    :try_start_15
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageShowWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_21

    :goto_16
    :try_start_16
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_20

    :goto_17
    :try_start_17
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_1f

    :goto_18
    :try_start_18
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Notification:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_1e

    :goto_19
    :try_start_19
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_1d

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_1c

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ScheduleEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_1b

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_1a

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_19

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowAlarmChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_18

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_17

    :goto_20
    :try_start_20
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x31

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_16

    :goto_21
    :try_start_21
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x30

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_15

    :goto_22
    :try_start_22
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCallWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_14

    :goto_23
    :try_start_23
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowChatbotWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_13

    :goto_24
    :try_start_24
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowClock:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_12

    :goto_25
    :try_start_25
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_11

    :goto_26
    :try_start_26
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactLookup:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_10

    :goto_27
    :try_start_27
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactTypeChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_f

    :goto_28
    :try_start_28
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_e

    :goto_29
    :try_start_29
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_29
    .catch Ljava/lang/NoSuchFieldError; {:try_start_29 .. :try_end_29} :catch_d

    :goto_2a
    :try_start_2a
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowLocalSearchWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_2a} :catch_c

    :goto_2b
    :try_start_2b
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNavWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x2a

    aput v2, v0, v1
    :try_end_2b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2b .. :try_end_2b} :catch_b

    :goto_2c
    :try_start_2c
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNaverWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x2d

    aput v2, v0, v1
    :try_end_2c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2c .. :try_end_2c} :catch_a

    :goto_2d
    :try_start_2d
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTaskChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x2b

    aput v2, v0, v1
    :try_end_2d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2d .. :try_end_2d} :catch_9

    :goto_2e
    :try_start_2e
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTrueKnowledgeWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x34

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2e .. :try_end_2e} :catch_8

    :goto_2f
    :try_start_2f
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWCISWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x2c

    aput v2, v0, v1
    :try_end_2f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2f .. :try_end_2f} :catch_7

    :goto_30
    :try_start_30
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x2f

    aput v2, v0, v1
    :try_end_30
    .catch Ljava/lang/NoSuchFieldError; {:try_start_30 .. :try_end_30} :catch_6

    :goto_31
    :try_start_31
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x2e

    aput v2, v0, v1
    :try_end_31
    .catch Ljava/lang/NoSuchFieldError; {:try_start_31 .. :try_end_31} :catch_5

    :goto_32
    :try_start_32
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWolframWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x32

    aput v2, v0, v1
    :try_end_32
    .catch Ljava/lang/NoSuchFieldError; {:try_start_32 .. :try_end_32} :catch_4

    :goto_33
    :try_start_33
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWorldTime:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x33

    aput v2, v0, v1
    :try_end_33
    .catch Ljava/lang/NoSuchFieldError; {:try_start_33 .. :try_end_33} :catch_3

    :goto_34
    :try_start_34
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SocialNetworkChoice:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x35

    aput v2, v0, v1
    :try_end_34
    .catch Ljava/lang/NoSuchFieldError; {:try_start_34 .. :try_end_34} :catch_2

    :goto_35
    :try_start_35
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->UnsupportedSuggestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x36

    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_35} :catch_1

    :goto_36
    :try_start_36
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->WebBrowser:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/16 v2, 0x37

    aput v2, v0, v1
    :try_end_36
    .catch Ljava/lang/NoSuchFieldError; {:try_start_36 .. :try_end_36} :catch_0

    :goto_37
    sput-object v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->$SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_37

    :catch_1
    move-exception v1

    goto :goto_36

    :catch_2
    move-exception v1

    goto :goto_35

    :catch_3
    move-exception v1

    goto :goto_34

    :catch_4
    move-exception v1

    goto :goto_33

    :catch_5
    move-exception v1

    goto :goto_32

    :catch_6
    move-exception v1

    goto :goto_31

    :catch_7
    move-exception v1

    goto :goto_30

    :catch_8
    move-exception v1

    goto :goto_2f

    :catch_9
    move-exception v1

    goto :goto_2e

    :catch_a
    move-exception v1

    goto :goto_2d

    :catch_b
    move-exception v1

    goto/16 :goto_2c

    :catch_c
    move-exception v1

    goto/16 :goto_2b

    :catch_d
    move-exception v1

    goto/16 :goto_2a

    :catch_e
    move-exception v1

    goto/16 :goto_29

    :catch_f
    move-exception v1

    goto/16 :goto_28

    :catch_10
    move-exception v1

    goto/16 :goto_27

    :catch_11
    move-exception v1

    goto/16 :goto_26

    :catch_12
    move-exception v1

    goto/16 :goto_25

    :catch_13
    move-exception v1

    goto/16 :goto_24

    :catch_14
    move-exception v1

    goto/16 :goto_23

    :catch_15
    move-exception v1

    goto/16 :goto_22

    :catch_16
    move-exception v1

    goto/16 :goto_21

    :catch_17
    move-exception v1

    goto/16 :goto_20

    :catch_18
    move-exception v1

    goto/16 :goto_1f

    :catch_19
    move-exception v1

    goto/16 :goto_1e

    :catch_1a
    move-exception v1

    goto/16 :goto_1d

    :catch_1b
    move-exception v1

    goto/16 :goto_1c

    :catch_1c
    move-exception v1

    goto/16 :goto_1b

    :catch_1d
    move-exception v1

    goto/16 :goto_1a

    :catch_1e
    move-exception v1

    goto/16 :goto_19

    :catch_1f
    move-exception v1

    goto/16 :goto_18

    :catch_20
    move-exception v1

    goto/16 :goto_17

    :catch_21
    move-exception v1

    goto/16 :goto_16

    :catch_22
    move-exception v1

    goto/16 :goto_15

    :catch_23
    move-exception v1

    goto/16 :goto_14

    :catch_24
    move-exception v1

    goto/16 :goto_13

    :catch_25
    move-exception v1

    goto/16 :goto_12

    :catch_26
    move-exception v1

    goto/16 :goto_11

    :catch_27
    move-exception v1

    goto/16 :goto_10

    :catch_28
    move-exception v1

    goto/16 :goto_f

    :catch_29
    move-exception v1

    goto/16 :goto_e

    :catch_2a
    move-exception v1

    goto/16 :goto_d

    :catch_2b
    move-exception v1

    goto/16 :goto_c

    :catch_2c
    move-exception v1

    goto/16 :goto_b

    :catch_2d
    move-exception v1

    goto/16 :goto_a

    :catch_2e
    move-exception v1

    goto/16 :goto_9

    :catch_2f
    move-exception v1

    goto/16 :goto_8

    :catch_30
    move-exception v1

    goto/16 :goto_7

    :catch_31
    move-exception v1

    goto/16 :goto_6

    :catch_32
    move-exception v1

    goto/16 :goto_5

    :catch_33
    move-exception v1

    goto/16 :goto_4

    :catch_34
    move-exception v1

    goto/16 :goto_3

    :catch_35
    move-exception v1

    goto/16 :goto_2

    :catch_36
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->TAG:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/nuance/sample/UiUpdater;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uiUpdater"    # Lcom/nuance/sample/UiUpdater;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    .line 35
    return-void
.end method


# virtual methods
.method public setEavesdropper(Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionEavesdropper;)V
    .locals 0
    .param p1, "ed"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionEavesdropper;

    .prologue
    .line 134
    return-void
.end method

.method public showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 18
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    .local p3, "object":Ljava/lang/Object;, "TT;"
    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "should show widget for key="

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ", fieldId="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 41
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 40
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 42
    .local v8, "m":Ljava/lang/String;
    sget-object v15, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->TAG:Ljava/lang/String;

    invoke-static {v15, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-static {}, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->$SWITCH_TABLE$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey()[I

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v16

    aget v15, v15, v16

    sparse-switch v15, :sswitch_data_0

    .line 130
    .end local p3    # "object":Ljava/lang/Object;, "TT;"
    :cond_0
    :goto_0
    return-void

    .line 56
    .restart local p3    # "object":Ljava/lang/Object;, "TT;"
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    .line 57
    const-string/jumbo v16, "[OPEN APP DISAMB WIDGET GOES HERE]"

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    move-object/from16 v3, p3

    .line 58
    check-cast v3, Ljava/util/List;

    .line 59
    .local v3, "appInfos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v15

    if-ge v4, v15, :cond_0

    .line 60
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 61
    .local v2, "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    .line 59
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 65
    .end local v2    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    .end local v3    # "appInfos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .end local v4    # "i":I
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    const-string/jumbo v16, "[MULTIPLE MSG WIDGET GOES HERE]"

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 68
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    const-string/jumbo v16, "[MULTIPLE MSG WIDGET GOES HERE]"

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 71
    :sswitch_3
    check-cast p3, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .end local p3    # "object":Ljava/lang/Object;, "TT;"
    sput-object p3, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 72
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "Contact: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v17, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 73
    const-string/jumbo v17, "; Address: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, "; Content: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 74
    sget-object v17, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 72
    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 77
    .restart local p3    # "object":Ljava/lang/Object;, "TT;"
    :sswitch_4
    check-cast p3, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .end local p3    # "object":Ljava/lang/Object;, "TT;"
    sput-object p3, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 78
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "Contact: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v17, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 79
    const-string/jumbo v17, "; Address: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->msg:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 78
    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .restart local p3    # "object":Ljava/lang/Object;, "TT;"
    :sswitch_5
    move-object/from16 v9, p3

    .line 82
    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 83
    .local v9, "messageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "Contact: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, "; Address: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 85
    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, "; Content: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 86
    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 83
    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 89
    .end local v9    # "messageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    const-string/jumbo v16, "Help goes here"

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_7
    move-object/from16 v11, p3

    .line 92
    check-cast v11, Ljava/util/List;

    .line 93
    .local v11, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    if-eqz v11, :cond_0

    .line 94
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-lt v15, v0, :cond_0

    .line 95
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v15

    if-ge v4, v15, :cond_0

    .line 96
    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/nuance/sample/music/MusicDetails;

    .line 97
    .local v10, "musicDetails":Lcom/nuance/sample/music/MusicDetails;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string/jumbo v17, "artist:"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 98
    invoke-virtual {v10}, Lcom/nuance/sample/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, ", title:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 99
    invoke-virtual {v10}, Lcom/nuance/sample/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 97
    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    .line 95
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 105
    .end local v4    # "i":I
    .end local v10    # "musicDetails":Lcom/nuance/sample/music/MusicDetails;
    .end local v11    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/nuance/sample/music/MusicDetails;>;"
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    const-string/jumbo v16, "[MEMO WIDGET GOES HERE]"

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_9
    move-object/from16 v14, p3

    .line 108
    check-cast v14, Lcom/nuance/sample/news/NewsItem;

    .line 109
    .local v14, "tts":Lcom/nuance/sample/news/NewsItem;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    invoke-virtual {v14}, Lcom/nuance/sample/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .end local v14    # "tts":Lcom/nuance/sample/news/NewsItem;
    :sswitch_a
    move-object/from16 v1, p3

    .line 112
    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    .line 113
    .local v1, "adaptor":Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    instance-of v15, v1, Lcom/vlingo/midas/naver/NaverAdaptor;

    if-eqz v15, :cond_1

    move-object v12, v1

    .line 114
    check-cast v12, Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 115
    .local v12, "naverAdaptor":Lcom/vlingo/midas/naver/NaverAdaptor;
    invoke-virtual {v12}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v13

    .line 116
    .local v13, "parser":Lcom/vlingo/midas/naver/NaverXMLParser;
    iget-object v6, v13, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    .line 117
    .local v6, "itemList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Hashtable;

    .line 118
    .local v5, "item":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    move-object/from16 v16, v0

    invoke-virtual {v5}, Ljava/util/Hashtable;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-interface/range {v16 .. v17}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 122
    .end local v5    # "item":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "itemList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v12    # "naverAdaptor":Lcom/vlingo/midas/naver/NaverAdaptor;
    .end local v13    # "parser":Lcom/vlingo/midas/naver/NaverXMLParser;
    :cond_1
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_4
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCount()I

    move-result v15

    if-ge v4, v15, :cond_0

    .line 123
    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getItem(I)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v7

    .line 124
    .local v7, "listing":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/nuance/sample/coreaccess/WidgetFactoryImpl;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/nuance/sample/UiUpdater;->displayWidgetContent(Ljava/lang/CharSequence;)V

    .line 122
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_5
        0x10 -> :sswitch_8
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x17 -> :sswitch_3
        0x18 -> :sswitch_4
        0x1a -> :sswitch_0
        0x1b -> :sswitch_7
        0x22 -> :sswitch_9
        0x29 -> :sswitch_a
        0x2c -> :sswitch_6
    .end sparse-switch
.end method
