.class public Lcom/nuance/sample/coreaccess/BorrowedAppIdUtils;
.super Ljava/lang/Object;
.source "BorrowedAppIdUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static registerVersionSpecificHandlersWorker(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 149
    .local v0, "ver":I
    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 150
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v1, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 152
    :cond_0
    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 154
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v1, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 155
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_APPOINTMENTS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v1, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 157
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_APPOINTMENT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 156
    invoke-static {v1, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 158
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CREATE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v1, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 160
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v1, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 162
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_EDIT_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v1, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 166
    :cond_1
    return-void
.end method

.method public static setupBorrowedAppMappings(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ADDRESS_BOOK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 30
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CONTACT_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 31
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->MAP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 32
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 33
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 34
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_ALARMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 35
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_ALARM_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 36
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_COMPOSE_MEMO:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 37
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CREATE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 39
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_CREATE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 40
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 42
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_DELETE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 43
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_EDIT_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 44
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_EDIT_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 45
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_MEMO_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 46
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_NAVIGATION:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 47
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_TITLE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 49
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_ARTIST:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 50
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_ALBUM:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 51
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 52
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_MUSIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 53
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_GENERIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 55
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_TASKS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 56
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_TASK_CHOICES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 58
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_UNREAD_MESSAGES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 60
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_WCIS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 61
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 63
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->PLAY_MUSIC_BY_CHARACTERISTIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 62
    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 64
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CONFIRM:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 65
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_WEB_SEARCH_BUTTON:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 66
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 68
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_NAV:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 69
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_BAIDU_MAP:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 70
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHATBOT_SING:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 71
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHATBOT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 72
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVER_CONTENT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 73
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 74
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME_KOREAN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 75
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVIGATE_LOCAL:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 76
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAMSUNG_NAVIGATE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 77
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RECORD_VOICE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 78
    return-void
.end method

.method public static setupStandardMappings(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    invoke-static {p0}, Lcom/nuance/sample/coreaccess/BorrowedAppIdUtils;->setupStandardMappingsWorker(Ljava/lang/Class;)V

    .line 24
    return-void
.end method

.method private static setupStandardMappingsWorker(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ADDRESS_BOOK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 88
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ANSWER_QUESTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 89
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CALENDAR_READBACK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 90
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CALL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 91
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 92
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CONTACT_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 93
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DATE_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 94
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DEFAULT_WEB_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 95
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIALOG_CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 96
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 97
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->EVENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 98
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->EXECUTE_UNKNOWN_DEF_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 100
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->INTENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 101
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 102
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LP_ACTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 103
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->MAP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 104
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 105
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME_KOREAN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 106
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAMSUNG_NAVIGATE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 107
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->PLAY_MEDIA:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 108
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->POPULATE_TEXTBOX:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 109
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 110
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAFEREADER_REPLY:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 111
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SEARCH_WEB_PROMPT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 112
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 113
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_CONFIG:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 114
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 115
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_TURN_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 116
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 117
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 118
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 119
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_COMPOSE_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 120
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 121
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_TYPE_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 123
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_FORWARD_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 124
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 125
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_0PEN_APP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 126
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_REPLY_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 127
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 128
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SYSTEM_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 129
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_USER_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 130
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SMS_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 131
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SOCIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 132
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SPEAK_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 133
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->UPDATE_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 134
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_LOCAL_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 135
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 136
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WEB_SEARCH_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 137
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_WEB_SEARCH_BUTTON:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0, p0}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 138
    invoke-static {p0}, Lcom/nuance/sample/coreaccess/BorrowedAppIdUtils;->registerVersionSpecificHandlersWorker(Ljava/lang/Class;)V

    .line 139
    return-void
.end method
