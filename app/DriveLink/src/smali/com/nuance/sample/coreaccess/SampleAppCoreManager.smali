.class public Lcom/nuance/sample/coreaccess/SampleAppCoreManager;
.super Ljava/lang/Object;
.source "SampleAppCoreManager.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private LOCATION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field private LOCATION_SHARING:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

.field private mAudioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

.field private mContext:Landroid/content/Context;

.field private mResIdProvider:Lcom/vlingo/core/internal/ResourceIdProvider;

.field private mServerDetails:Lcom/nuance/sample/coreaccess/identification/ServerDetails;

.field private mTts:Landroid/speech/tts/TextToSpeech;

.field private mVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;

    .line 70
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 69
    sput-object v0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->TAG:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vlingoApplicationInterface"    # Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    .param p3, "appVersion"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "XXX"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->LOCATION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 73
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-string/jumbo v1, "YYY"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->LOCATION_SHARING:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 79
    iput-object v2, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mAudioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .line 80
    iput-object v2, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 85
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mContext:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    .line 87
    invoke-static {}, Lcom/nuance/sample/coreaccess/identification/ServerDetails;->getInstance()Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mServerDetails:Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    .line 88
    new-instance v0, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;

    invoke-direct {v0}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;-><init>()V

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mResIdProvider:Lcom/vlingo/core/internal/ResourceIdProvider;

    .line 89
    return-void
.end method

.method static synthetic access$0(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)Lcom/vlingo/core/internal/ResourceIdProvider;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mResIdProvider:Lcom/vlingo/core/internal/ResourceIdProvider;

    return-object v0
.end method

.method static synthetic access$1(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mVlingoApplicationInterface:Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    return-object v0
.end method

.method static synthetic access$2(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)Lcom/nuance/sample/coreaccess/identification/ServerDetails;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mServerDetails:Lcom/nuance/sample/coreaccess/identification/ServerDetails;

    return-object v0
.end method

.method static synthetic access$3()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public destroyCore()V
    .locals 0

    .prologue
    .line 92
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->destroyCore()V

    .line 93
    return-void
.end method

.method public initCore()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 99
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getCoreVersion()Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "VACVersion":Ljava/lang/String;
    sget-object v3, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "SampleAppCoreManager VACVersion \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    new-instance v3, Lcom/nuance/sample/coreaccess/SampleAppValues;

    iget-object v4, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/nuance/sample/coreaccess/SampleAppValues;-><init>(Landroid/content/Context;)V

    invoke-static {v3}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->setInterface(Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;)V

    .line 105
    new-instance v2, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    invoke-direct {v2}, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;-><init>()V

    .line 106
    .local v2, "factory":Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
    const-string/jumbo v1, "99999"

    .line 108
    .local v1, "c_SVN":Ljava/lang/String;
    iget-object v3, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$1;

    invoke-direct {v4, p0}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$1;-><init>(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)V

    .line 154
    invoke-static {}, Lcom/nuance/sample/CCFieldIds;->generateFieldIdsMap()Ljava/util/Map;

    move-result-object v5

    .line 108
    invoke-static {v3, v2, v4, v5}, Lcom/vlingo/core/facade/CoreManager;->initCore(Landroid/content/Context;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Lcom/vlingo/core/facade/IClientInfo;Ljava/util/Map;)V

    .line 160
    const-string/jumbo v3, "location_enabled"

    invoke-static {v3, v6}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 165
    const-string/jumbo v3, "custom_tone_encoding"

    const-string/jumbo v4, "PCM_22k"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string/jumbo v3, "use_audiotrack_tone_player"

    invoke-static {v3, v6}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 167
    const-string/jumbo v3, "FIELD_ID"

    sget-object v4, Lcom/nuance/sample/coreaccess/identification/Identifiers;->DEFAULT_FIELD_ID:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string/jumbo v3, "use_mediasync_tone_approach"

    invoke-static {v3, v6}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 170
    const-string/jumbo v3, "validate_launch_intent_version"

    invoke-static {v3, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 172
    const-string/jumbo v3, "auto_dial"

    const-string/jumbo v4, "confident"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string/jumbo v3, "use_network_tts"

    invoke-static {v3, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 179
    sget-object v3, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->NoiseCancel:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v4, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 190
    const-class v3, Lcom/nuance/drivelink/handlers/DLUnsupportedDomainHandler;

    invoke-static {v3}, Lcom/nuance/sample/coreaccess/BorrowedAppIdUtils;->setupStandardMappings(Ljava/lang/Class;)V

    .line 191
    const-class v3, Lcom/nuance/drivelink/handlers/DLUnsupportedDomainHandler;

    invoke-static {v3}, Lcom/nuance/sample/coreaccess/BorrowedAppIdUtils;->setupBorrowedAppMappings(Ljava/lang/Class;)V

    .line 194
    invoke-static {}, Lcom/vlingo/core/facade/CoreRegistrar;->setUpStandardActionMappings()V

    .line 207
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 212
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_TURN_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 213
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetTurnParamsHandler;

    .line 212
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 216
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_CONFIG:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 217
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetConfigHandler;

    .line 216
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 220
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SYSTEM_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 221
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowSystemTurnHandler;

    .line 220
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 224
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_USER_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 225
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowUserTurnHandler;

    .line 224
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 228
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 229
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/CancelActionHandler;

    .line 228
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 233
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIALOG_CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 234
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleDialogCancelHandler;

    .line 233
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 236
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 237
    const-class v4, Lcom/nuance/sample/handlers/SampleShowServerMessageHandler;

    .line 236
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 244
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->PROCESS_COMMAND:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 245
    const-class v4, Lcom/nuance/drivelink/handlers/DLProcessCommandHandler;

    .line 244
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 249
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->UNSUPPORTED_DOMAIN:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 250
    const-class v4, Lcom/nuance/drivelink/handlers/DLRecognitionFailedHandler;

    .line 249
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 257
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_0PEN_APP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 258
    const-class v4, Lcom/nuance/drivelink/handlers/DLUnsupportedDomainHandler;

    .line 257
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 259
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->INTENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 260
    const-class v4, Lcom/nuance/drivelink/handlers/DLUnsupportedDomainHandler;

    .line 259
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 262
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SMS_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 263
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleSMSPageHandler;

    .line 262
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 264
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 265
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleDialPageHandler;

    .line 264
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 267
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 268
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleShowContactChoicesHandler;

    .line 267
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 269
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_TYPE_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 270
    const-class v4, Lcom/nuance/sample/handlers/SampleShowContactTypeChoicesHandler;

    .line 269
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 271
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 272
    const-class v4, Lcom/nuance/sample/handlers/clientdm/QuietUnhandledDomainHandler;

    .line 271
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 277
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 278
    const-class v4, Lcom/nuance/sample/handlers/actions/SampleOpenAppAction;

    .line 277
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 279
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 280
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    .line 279
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 281
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VOICE_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 282
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    .line 281
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 283
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 284
    const-class v4, Lcom/nuance/sample/handlers/actions/SampleSendMessageAction;

    .line 283
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 304
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 305
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleShowCallWidgetHandler;

    .line 304
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 308
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->MAP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MapHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 309
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_NAVIGATION:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 310
    const-class v4, Lcom/nuance/sample/handlers/SampleShowNavWidgetHandler;

    .line 309
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 315
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAMSUNG_NAVIGATE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 316
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleLocationPageHandler;

    .line 315
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 319
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->MUSIC_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 320
    const-class v4, Lcom/nuance/sample/handlers/SampleMusicPageHandler;

    .line 319
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 321
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_GENERIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 322
    const-class v4, Lcom/nuance/drivelink/handlers/DLShowPlayMusicWidgetHandler;

    .line 321
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 323
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->PLAY_MUSIC_BY_CHARACTERISTIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 324
    const-class v4, Lcom/nuance/sample/handlers/SamplePlayMusicByCharacteristicHandler;

    .line 323
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 327
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_COMPOSE_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 328
    const-class v4, Lcom/nuance/sample/handlers/SampleShowComposeMessageHandler;

    .line 327
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 331
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_UNREAD_MESSAGES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 332
    const-class v4, Lcom/nuance/drivelink/handlers/DLShowMessagesWidgetHandler;

    .line 331
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 339
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_USER_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 340
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowUserTurnHandler;

    .line 339
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 348
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SYSTEM_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 349
    const-class v4, Lcom/nuance/drivelink/handlers/DLShowMessagesWidgetHandler;

    .line 348
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 350
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 351
    const-class v4, Lcom/nuance/sample/handlers/clientdm/QuietUnhandledDomainHandler;

    .line 350
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 354
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_MESSAGE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 355
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;

    .line 354
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 356
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 357
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;

    .line 356
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 358
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_REPLY_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 359
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeReplyMessageHandler;

    .line 358
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 362
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LP_ACTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 363
    const-class v4, Lcom/nuance/drivelink/handlers/DLLpActionHandler;

    .line 362
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 364
    sget-object v3, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v4, Lcom/oem/msg/OemMSGUtil;

    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 367
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAFEREADER_REPLY:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 368
    const-class v4, Lcom/nuance/drivelink/handlers/DLSafeReaderReply;

    .line 367
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 369
    sget-object v3, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->AudioSourceSelector:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 370
    const-class v4, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;

    .line 369
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 375
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 376
    const-class v4, Lcom/nuance/sample/handlers/actions/SamplePlaySongListAction;

    .line 375
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 379
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    .line 380
    const-class v4, Lcom/nuance/drivelink/handlers/actions/DLPlayMusicServiceAction;

    .line 379
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 382
    sget-object v3, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->BDeviceUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    .line 383
    const-class v4, Lcom/oem/util/OemWatchDeviceUtil;

    .line 382
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerAdapter(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 386
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_LOCAL_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .line 387
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleLocalSearchHandler;

    .line 386
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 388
    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_WCIS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    .line 389
    const-class v4, Lcom/nuance/sample/handlers/clientdm/SampleShowWCISWidgetHandler;

    .line 388
    invoke-static {v3, v4}, Lcom/vlingo/core/facade/CoreRegistrar;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 395
    iget-object v3, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mAudioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    if-nez v3, :cond_0

    .line 396
    new-instance v3, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$2;

    invoke-direct {v3, p0}, Lcom/nuance/sample/coreaccess/SampleAppCoreManager$2;-><init>(Lcom/nuance/sample/coreaccess/SampleAppCoreManager;)V

    iput-object v3, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mAudioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .line 409
    :cond_0
    iget-object v3, p0, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->mAudioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    invoke-static {v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->addListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 412
    return-void
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 443
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 431
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 437
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 416
    sget-object v2, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onRequestWillPlay "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    .line 419
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 420
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->ttsEngine(Landroid/content/Context;)Lcom/vlingo/core/facade/tts/ITTSEngine;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/tts/ITTSEngine;->getTTS()Landroid/speech/tts/TextToSpeech;

    move-result-object v1

    .line 421
    .local v1, "tts":Landroid/speech/tts/TextToSpeech;
    if-eqz v1, :cond_0

    .line 422
    sget-object v2, Lcom/nuance/sample/coreaccess/SampleAppCoreManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "tts speak parameter : 2 "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    const-string/jumbo v2, ""

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 425
    :cond_0
    return-void
.end method
