.class public Lcom/nuance/sample/coreaccess/identification/Identifiers;
.super Ljava/lang/Object;
.source "Identifiers.java"


# static fields
.field public static final APP_DISTRIBUTION_CHANNEL:Ljava/lang/String; = "Preinstall Free"

.field public static final APP_ID:Ljava/lang/String; = "com.samsung.android.cc.kproject"

.field public static final APP_NAME:Ljava/lang/String; = "SamsungCCKProject"

.field public static final APP_VERSION_ROOT:Ljava/lang/String; = "11.0"

.field public static final DEFAULT_FIELD_ID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/nuance/sample/CCFieldIds;->CC_MAIN:Lcom/nuance/sample/CCFieldIds;

    invoke-virtual {v0}, Lcom/nuance/sample/CCFieldIds;->getValue()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/nuance/sample/coreaccess/identification/Identifiers;->DEFAULT_FIELD_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
