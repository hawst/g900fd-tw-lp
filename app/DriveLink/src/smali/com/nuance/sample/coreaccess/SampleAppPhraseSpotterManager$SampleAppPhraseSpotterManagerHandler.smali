.class Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;
.super Landroid/os/Handler;
.source "SampleAppPhraseSpotterManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SampleAppPhraseSpotterManagerHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;)V
    .locals 1
    .param p1, "out"    # Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 85
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 86
    return-void
.end method

.method private isCanDisconnectSco()Z
    .locals 3

    .prologue
    .line 172
    const/4 v0, 0x1

    .line 181
    .local v0, "disconnect":Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->getIsCanceledtoMain()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 183
    const-string/jumbo v2, "[isCanDisconnectSco FALSE] IsCanceledtoMain true"

    .line 182
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const/4 v0, 0x0

    .line 187
    :cond_0
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/drivelink/DLAppUiUpdater;->isTTSPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 189
    const-string/jumbo v2, "[isCanDisconnectSco FALSE] isTTSPlaying true"

    .line 188
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v0, 0x0

    .line 193
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v1

    if-nez v1, :cond_2

    .line 194
    const-string/jumbo v1, "DLPhraseSpotter"

    .line 195
    const-string/jumbo v2, "[isCanDisconnectSco FALSE] isIdle false"

    .line 194
    invoke-static {v1, v2}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const/4 v0, 0x0

    .line 199
    :cond_2
    return v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v6, 0x190

    .line 89
    iget-object v4, p0, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;

    .line 90
    .local v3, "o":Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    .line 92
    .local v0, "isBluetoothAudioOn":Z
    invoke-static {}, Lcom/sec/android/automotive/drivelink/location/map/MapFactory;->newNavigationMap()Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;

    move-result-object v2

    .line 93
    .local v2, "navigation":Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;
    const/4 v1, 0x0

    .line 95
    .local v1, "naviPkgName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 96
    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelink/location/map/navi/NavigationMap;->getNaviPackageName()Ljava/lang/String;

    move-result-object v1

    .line 99
    :cond_0
    if-eqz v3, :cond_1

    .line 100
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 169
    :cond_1
    :goto_0
    return-void

    .line 102
    :pswitch_0
    if-eqz v0, :cond_2

    .line 103
    const-string/jumbo v4, "DLPhraseSpotter"

    const-string/jumbo v5, "[start dealy] BT audioOn"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v4, 0x1

    .line 104
    invoke-virtual {p0, v4, v6, v7}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 109
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->isIdle()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 110
    invoke-static {}, Lcom/nuance/drivelink/DLAppUiUpdater;->getInstance()Lcom/nuance/drivelink/DLAppUiUpdater;

    move-result-object v4

    invoke-virtual {v4}, Lcom/nuance/drivelink/DLAppUiUpdater;->isAppInForeground()Z

    move-result v4

    if-nez v4, :cond_3

    .line 111
    const-string/jumbo v4, "BG_WAKEUP_AP"

    .line 112
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v5

    .line 113
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getWakeupState()Ljava/lang/String;

    move-result-object v5

    .line 112
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 113
    if-nez v4, :cond_3

    .line 115
    const-string/jumbo v4, "DLPhraseSpotter"

    .line 116
    const-string/jumbo v5, "[start cancel] app is background"

    .line 115
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 121
    :cond_3
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    .line 122
    const-string/jumbo v5, "com.sec.android.automotive.drivelink"

    .line 120
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->IsAppRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    .line 122
    if-nez v4, :cond_4

    .line 124
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    .line 123
    invoke-static {v4, v1}, Lcom/sec/android/automotive/drivelink/common/DLTaskObserverService;->IsAppRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    .line 125
    if-nez v4, :cond_4

    .line 126
    const-string/jumbo v4, "BG_WAKEUP_AP"

    .line 127
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v5

    .line 128
    invoke-virtual {v5}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getWakeupState()Ljava/lang/String;

    move-result-object v5

    .line 127
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 128
    if-nez v4, :cond_4

    .line 129
    const-string/jumbo v4, "DLPhraseSpotter"

    const-string/jumbo v5, "[start cancel] :  app is notRunning"

    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 135
    :cond_4
    invoke-static {}, Lcom/sec/android/automotive/drivelink/DLApplication;->getInstance()Lcom/sec/android/automotive/drivelink/DLApplication;

    move-result-object v4

    .line 134
    invoke-static {v4}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->isCallStateIdle(Landroid/content/Context;)Z

    move-result v4

    .line 135
    if-nez v4, :cond_5

    .line 136
    const-string/jumbo v4, "DLPhraseSpotter"

    .line 137
    const-string/jumbo v5, "[start cancel] Call state is not idle"

    .line 136
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 141
    :cond_5
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->phraseSpotter()Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;->startPhraseSpotting()V

    goto/16 :goto_0

    .line 143
    :cond_6
    const-string/jumbo v4, "DLPhraseSpotter"

    .line 144
    const-string/jumbo v5, "[start cancel] dialogFlow is not idle "

    .line 143
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 150
    :pswitch_1
    const-string/jumbo v4, "DLPhraseSpotter"

    .line 151
    const-string/jumbo v5, "[start dealy] MSG_START_SPOTTER_DELAYED_PROC"

    .line 150
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    if-eqz v0, :cond_7

    .line 154
    invoke-direct {p0}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->isCanDisconnectSco()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 155
    const-string/jumbo v4, "DLPhraseSpotter"

    .line 156
    const-string/jumbo v5, "[start dealy] force stopSco"

    .line 155
    invoke-static {v4, v5}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 161
    :cond_7
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v6, v7}, Lcom/nuance/sample/coreaccess/SampleAppPhraseSpotterManager$SampleAppPhraseSpotterManagerHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
