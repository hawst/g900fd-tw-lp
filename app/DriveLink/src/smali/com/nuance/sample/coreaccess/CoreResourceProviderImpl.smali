.class public Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;
.super Lcom/vlingo/core/internal/BaseResourceIdProvider;
.source "CoreResourceProviderImpl.java"

# interfaces
.implements Lcom/vlingo/core/internal/ResourceIdProvider;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;

    .line 25
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 24
    sput-object v0, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->TAG:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    const v5, 0x7f0a00b7

    const v4, 0x7f0a0049

    const v3, 0x7f0a003d

    const v2, 0x7f0a002d

    const v1, 0x7f0a00fc

    .line 87
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_opening_app:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    const v1, 0x7f0a00cc

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 630
    :goto_0
    return-object v0

    .line 91
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 93
    const v1, 0x7f0a00b8

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 96
    const v1, 0x7f0a00b3

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 98
    :cond_2
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 100
    const v1, 0x7f0a0118

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 102
    :cond_3
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 103
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 104
    invoke-virtual {v0, v5, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 105
    :cond_4
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 106
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 107
    const v1, 0x7f0a0192

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_5
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tomorrow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 109
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 110
    const v1, 0x7f0a00b4

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 111
    :cond_6
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_general:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 112
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 113
    const v1, 0x7f0a00b5

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 115
    :cond_7
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 116
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 117
    invoke-virtual {v0, v5, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 118
    :cond_8
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_current:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 119
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 120
    const v1, 0x7f0a00b0

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 122
    :cond_9
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_display:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 123
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 124
    const v1, 0x7f0a00b1

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 126
    :cond_a
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 127
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 128
    const v1, 0x7f0a00d0

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 130
    :cond_b
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 131
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 132
    const v1, 0x7f0a00e4

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 134
    :cond_c
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_WHICH_CONTACT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 136
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 137
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 138
    const v1, 0x7f0a0054

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 140
    :cond_d
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 141
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 142
    const v1, 0x7f0a005d

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 144
    :cond_e
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_no_match_openquote:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 145
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 146
    const v1, 0x7f0a0060

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 148
    :cond_f
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_birthday_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 149
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 150
    const v1, 0x7f0a005e

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 152
    :cond_10
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 153
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 154
    const v1, 0x7f0a005f

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 155
    :cond_11
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_single_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 156
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 157
    const v1, 0x7f0a0085

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 158
    :cond_12
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 159
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 160
    const v1, 0x7f0a0068

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 161
    :cond_13
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_time_at_present:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 162
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 163
    const v1, 0x7f0a011d

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 165
    :cond_14
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 166
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 167
    const v1, 0x7f0a00fd

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 169
    :cond_15
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 171
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 172
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 174
    const v1, 0x7f0a00fe

    .line 173
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 177
    :cond_16
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 179
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 180
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 182
    const v1, 0x7f0a00ff

    .line 181
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 185
    :cond_17
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_command_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 187
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 188
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 190
    const v1, 0x7f0a0100

    .line 189
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 193
    :cond_18
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_dm_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 195
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 196
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 197
    const v1, 0x7f0a0101

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 200
    :cond_19
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 202
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 203
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 205
    const v1, 0x7f0a0102

    .line 204
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 208
    :cond_1a
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 210
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 211
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 212
    const v1, 0x7f0a0104

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 215
    :cond_1b
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 217
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 218
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 219
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 222
    :cond_1c
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 224
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 225
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 226
    const v1, 0x7f0a00fa

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 228
    :cond_1d
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 230
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 232
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 235
    :cond_1e
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 236
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 237
    const v1, 0x7f0a00e5

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 239
    :cond_1f
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 241
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 242
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 243
    const v1, 0x7f0a00e6

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 246
    :cond_20
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_verbose:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 247
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 248
    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 250
    :cond_21
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_regular:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 251
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 252
    const v1, 0x7f0a00e8

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 254
    :cond_22
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 255
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 256
    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 258
    :cond_23
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 260
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 261
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 262
    const v1, 0x7f0a00ea

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 264
    :cond_24
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 266
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 267
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 268
    const v1, 0x7f0a00eb

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 271
    :cond_25
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 273
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 274
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 276
    const v1, 0x7f0a00ec

    .line 275
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 279
    :cond_26
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 281
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 282
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 284
    const v1, 0x7f0a00ed

    .line 283
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 287
    :cond_27
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 289
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 290
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 292
    const v1, 0x7f0a00ef

    .line 291
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 295
    :cond_28
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 297
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 298
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 300
    const v1, 0x7f0a00f0

    .line 299
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 303
    :cond_29
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 304
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 305
    const v1, 0x7f0a00f2

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 307
    :cond_2a
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 309
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 310
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 312
    const v1, 0x7f0a00f3

    .line 311
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 315
    :cond_2b
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 316
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 317
    const v1, 0x7f0a00f5

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 319
    :cond_2c
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 321
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 322
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 323
    const v1, 0x7f0a00f6

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 326
    :cond_2d
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 328
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 329
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 331
    const v1, 0x7f0a00f8

    .line 330
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 334
    :cond_2e
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 335
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 336
    const v1, 0x7f0a00f9

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 338
    :cond_2f
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 340
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 341
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 342
    const v1, 0x7f0a00fa

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 344
    :cond_30
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 346
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 347
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 348
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 351
    :cond_31
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 353
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 354
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 356
    const v1, 0x7f0a0105

    .line 355
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 359
    :cond_32
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 361
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 362
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 364
    const v1, 0x7f0a0106

    .line 363
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 367
    :cond_33
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 369
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 370
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 372
    const v1, 0x7f0a0108

    .line 371
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 375
    :cond_34
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 377
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 378
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 380
    const v1, 0x7f0a010a

    .line 379
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 383
    :cond_35
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 385
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 386
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 388
    const v1, 0x7f0a010b

    .line 387
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 391
    :cond_36
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 393
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 394
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 395
    const v1, 0x7f0a010d

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 398
    :cond_37
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 400
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 401
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 403
    const v1, 0x7f0a010e

    .line 402
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 406
    :cond_38
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 408
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 409
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 411
    const v1, 0x7f0a0110

    .line 410
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 414
    :cond_39
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 416
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 417
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 419
    const v1, 0x7f0a0111

    .line 418
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 422
    :cond_3a
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 424
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 425
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 427
    const v1, 0x7f0a0193

    .line 426
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 430
    :cond_3b
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 432
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 433
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 435
    const v1, 0x7f0a0194

    .line 434
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 438
    :cond_3c
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 440
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 441
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 443
    const v1, 0x7f0a0195

    .line 442
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 445
    :cond_3d
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 446
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 447
    const v1, 0x7f0a0070

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 449
    :cond_3e
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved_about:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 450
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 451
    const v1, 0x7f0a0071

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 453
    :cond_3f
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_multiple_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 454
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 455
    const v1, 0x7f0a0065

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 456
    :cond_40
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_not_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 457
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 458
    const v1, 0x7f0a0066

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 460
    :cond_41
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MEMO_SAVED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 461
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 462
    const v1, 0x7f0a00be

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 463
    :cond_42
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 464
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 465
    const v1, 0x7f0a00c8

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 466
    :cond_43
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 467
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 468
    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 470
    :cond_44
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 472
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 473
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 474
    invoke-virtual {v0, v4, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 478
    :cond_45
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 480
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 481
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 483
    const v1, 0x7f0a0041

    .line 482
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 486
    :cond_46
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 488
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 489
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 490
    invoke-virtual {v0, v4, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494
    :cond_47
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 496
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 497
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 499
    const v1, 0x7f0a0042

    .line 498
    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 502
    :cond_48
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 504
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 505
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 506
    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 509
    :cond_49
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 511
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 512
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 513
    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 515
    :cond_4a
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alert_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 516
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 517
    const v1, 0x7f0a001c

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 519
    :cond_4b
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ALBUMMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 521
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 522
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 523
    const v1, 0x7f0a0034

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 525
    :cond_4c
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 526
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 527
    const v1, 0x7f0a002c

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 529
    :cond_4d
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 531
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 532
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 533
    invoke-virtual {v0, v2, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 535
    :cond_4e
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 537
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 538
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 539
    invoke-virtual {v0, v3, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 543
    :cond_4f
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MUSIC_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 544
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 545
    const v1, 0x7f0a0032

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 547
    :cond_50
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 549
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 550
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 551
    invoke-virtual {v0, v2, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 553
    :cond_51
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 555
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 556
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 557
    invoke-virtual {v0, v3, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 561
    :cond_52
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TITLE_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 562
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 563
    const v1, 0x7f0a004f

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 565
    :cond_53
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ANYMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 566
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 567
    const v1, 0x7f0a0117

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 569
    :cond_54
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ARTISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 571
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 572
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 573
    const v1, 0x7f0a0037

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 576
    :cond_55
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 578
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 579
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 580
    const v1, 0x7f0a003a

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 582
    :cond_56
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_PLAYLISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 584
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 585
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 586
    const v1, 0x7f0a003b

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 589
    :cond_57
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_TITLEMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 591
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 592
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 593
    const v1, 0x7f0a003c

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 595
    :cond_58
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 596
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 597
    const v1, 0x7f0a004d

    invoke-virtual {v0, v1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 598
    :cond_59
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 599
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 600
    const v1, 0x7f0a00bc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 601
    :cond_5a
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_mic_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 602
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 603
    const v1, 0x7f0a00bd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 605
    :cond_5b
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 607
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 608
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 610
    const v1, 0x7f0a0046

    .line 609
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 612
    :cond_5c
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_single_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 614
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    .line 615
    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 616
    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 617
    :cond_5d
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 618
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0073

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 619
    :cond_5e
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 620
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a00c6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 621
    :cond_5f
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 622
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a020b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 623
    :cond_60
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 624
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a020c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 625
    :cond_61
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 626
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a020d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 629
    :cond_62
    sget-object v0, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "core string, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .prologue
    .line 70
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_names:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$array;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isCnPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 74
    :cond_0
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/ResourceIdProvider$array;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 76
    invoke-static {}, Lcom/sec/android/automotive/drivelink/util/CommonUtil;->isCnPhone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_2
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initArrayMap()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method protected initDrawableMap()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method protected initIdMap()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method protected initLayoutMap()V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method protected initRawMap()V
    .locals 4

    .prologue
    const v3, 0x7f060014

    const v2, 0x7f060012

    .line 59
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 60
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 61
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const v1, 0x7f060013

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 63
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 65
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const v1, 0x7f060015

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 66
    return-void
.end method

.method protected initStringMap()V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_required_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 45
    const v1, 0x7f0a0519

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 44
    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 46
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_fallback_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .line 47
    const v1, 0x7f0a051a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 46
    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 48
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const v1, 0x7f0a00bc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 49
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_mic_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const v1, 0x7f0a00bd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/sample/coreaccess/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 50
    return-void
.end method
