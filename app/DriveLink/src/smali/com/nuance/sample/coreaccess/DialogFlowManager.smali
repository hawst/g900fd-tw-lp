.class public Lcom/nuance/sample/coreaccess/DialogFlowManager;
.super Ljava/lang/Object;
.source "DialogFlowManager.java"


# instance fields
.field private logger:Lcom/vlingo/core/facade/logging/ILogger;

.field private mContext:Landroid/content/Context;

.field private mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

.field private mEnergyBar:Landroid/widget/ProgressBar;

.field private mUiUpdater:Lcom/nuance/sample/UiUpdater;

.field private mWidgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Landroid/content/Context;)V
    .locals 1
    .param p1, "mWidgetFactory"    # Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;
    .param p2, "mEnergyBar"    # Landroid/widget/ProgressBar;
    .param p3, "mUiUpdater"    # Lcom/nuance/sample/UiUpdater;
    .param p4, "mContext"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-class v0, Lcom/nuance/sample/coreaccess/DialogFlowManager;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->logger:Lcom/vlingo/core/facade/logging/ILogger;

    .line 34
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mWidgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 35
    iput-object p2, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 36
    iput-object p3, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    .line 37
    iput-object p4, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private generateUserProperties()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 93
    .local v1, "userProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "isInDrivingMode"

    .line 94
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 92
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :try_start_0
    const-string/jumbo v3, "isPhoneInDrivingMode"

    .line 101
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->isSystemDrivingModeEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 102
    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 99
    :goto_0
    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_1
    const-string/jumbo v2, "isEyesFree"

    .line 112
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 111
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string/jumbo v2, "videoCallSupported"

    .line 118
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string/jumbo v2, "offerAppCarMode"

    .line 124
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 122
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string/jumbo v2, "offerPhoneCarMode"

    .line 127
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 125
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    const-string/jumbo v2, "applicationType"

    .line 132
    const-string/jumbo v3, "full"

    .line 130
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    return-object v1

    .line 102
    :cond_0
    :try_start_1
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 103
    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    iget-object v2, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->logger:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Couldn\'t figure out if phone is in driving mode, error trace: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-interface {v2, v3}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public destroyFlow()V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->stopFlow()V

    .line 82
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 84
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerDestroy()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 86
    return-void
.end method

.method public initFlow()V
    .locals 5

    .prologue
    .line 42
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 43
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z

    .line 44
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelTurn()V

    .line 45
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    invoke-interface {v0, v1}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V

    .line 47
    new-instance v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;

    iget-object v1, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    .line 48
    iget-object v2, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mEnergyBar:Landroid/widget/ProgressBar;

    invoke-direct {v0, v1, v2}, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;-><init>(Lcom/nuance/sample/UiUpdater;Landroid/widget/ProgressBar;)V

    .line 47
    iput-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 49
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 50
    invoke-direct {p0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->generateUserProperties()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mWidgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 49
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 52
    :cond_0
    return-void
.end method

.method public initFlow(Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V
    .locals 0
    .param p1, "mEnergyBar"    # Landroid/widget/ProgressBar;
    .param p2, "mUiUpdater"    # Lcom/nuance/sample/UiUpdater;
    .param p3, "mWidgetFactory"    # Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 57
    iput-object p2, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    .line 58
    iput-object p3, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mWidgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 59
    invoke-virtual {p0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->initFlow()V

    .line 60
    return-void
.end method

.method public stealFlow(Landroid/widget/ProgressBar;Lcom/nuance/sample/UiUpdater;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V
    .locals 4
    .param p1, "mEnergyBar"    # Landroid/widget/ProgressBar;
    .param p2, "mUiUpdater"    # Lcom/nuance/sample/UiUpdater;
    .param p3, "mWidgetFactory"    # Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    if-eqz v0, :cond_0

    .line 65
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/flow/IntegratedFlowManager;->isInDmFlowChanging()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V

    .line 68
    :cond_0
    iput-object p1, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mEnergyBar:Landroid/widget/ProgressBar;

    .line 69
    iput-object p2, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mUiUpdater:Lcom/nuance/sample/UiUpdater;

    .line 70
    iput-object p3, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mWidgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    .line 71
    new-instance v0, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;

    invoke-direct {v0, p2, p1}, Lcom/nuance/sample/coreaccess/DialogFlowListenerImpl;-><init>(Lcom/nuance/sample/UiUpdater;Landroid/widget/ProgressBar;)V

    iput-object v0, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 72
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/nuance/sample/coreaccess/DialogFlowManager;->mDialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    .line 73
    invoke-direct {p0}, Lcom/nuance/sample/coreaccess/DialogFlowManager;->generateUserProperties()Ljava/util/Map;

    move-result-object v3

    .line 72
    invoke-interface {v0, v1, v2, v3, p3}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->stealFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 74
    return-void
.end method

.method public stopFlow()V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->dialogFlow()Lcom/vlingo/core/facade/dialogflow/IDialogFlow;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/dialogflow/IDialogFlow;->cancelDialog()V

    .line 78
    return-void
.end method
