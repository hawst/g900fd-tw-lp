.class public Lcom/android/DomParser/dom/smil/SmilPlayer;
.super Ljava/lang/Object;
.source "SmilPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;,
        Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;,
        Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final LOCAL_LOGV:Z = false

.field public static final MEDIA_TIME_UPDATED_EVENT:Ljava/lang/String; = "mediaTimeUpdated"

.field private static final TAG:Ljava/lang/String; = "Mms/SmilPlayer"

.field private static final TIMESLICE:I = 0xc8


# instance fields
.field private mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

.field private mActiveElements:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/w3c/dom/smil/ElementTime;",
            ">;"
        }
    .end annotation
.end field

.field private mAllEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentElement:I

.field private mCurrentSlide:I

.field private mCurrentTime:J

.field private mMediaTimeUpdatedEvent:Lorg/w3c/dom/events/Event;

.field private mPlayerThread:Ljava/lang/Thread;

.field private mRoot:Lorg/w3c/dom/smil/ElementTime;

.field private mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

.field private sTimelineEntryComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Lcom/android/DomParser/dom/smil/SmilPlayer$1;

    invoke-direct {v0, p0}, Lcom/android/DomParser/dom/smil/SmilPlayer$1;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;)V

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->sTimelineEntryComparator:Ljava/util/Comparator;

    .line 82
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->INITIALIZED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    .line 84
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NO_ACTIVE_ACTION:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 247
    return-void
.end method

.method private declared-synchronized actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V
    .locals 2
    .param p1, "entry"    # Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .prologue
    .line 529
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getAction()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 545
    :goto_0
    monitor-exit p0

    return-void

    .line 533
    :pswitch_0
    :try_start_1
    invoke-virtual {p1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getElement()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->beginElement()Z

    .line 534
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getElement()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 529
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 539
    :pswitch_1
    :try_start_2
    invoke-virtual {p1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getElement()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->endElement()Z

    .line 540
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getElement()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized actionNext()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .locals 1

    .prologue
    .line 624
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->stopCurrentSlide()V

    .line 625
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->loadNextSlide()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized actionPause()V
    .locals 1

    .prologue
    .line 634
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->pauseActiveElements()V

    .line 635
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PAUSED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    .line 636
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NO_ACTIVE_ACTION:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 637
    monitor-exit p0

    return-void

    .line 634
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized actionPrev()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .locals 1

    .prologue
    .line 629
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->stopCurrentSlide()V

    .line 630
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->loadPrevSlide()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 629
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized actionReload()V
    .locals 1

    .prologue
    .line 649
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->reloadActiveSlide()V

    .line 650
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NO_ACTIVE_ACTION:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 651
    monitor-exit p0

    return-void

    .line 649
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized actionStop()V
    .locals 2

    .prologue
    .line 640
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->endActiveElements()V

    .line 641
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    .line 642
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .line 643
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .line 644
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->STOPPED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    .line 645
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NO_ACTIVE_ACTION:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 646
    monitor-exit p0

    return-void

    .line 640
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized beginSmilDocument()V
    .locals 3

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 424
    .local v0, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 425
    monitor-exit p0

    return-void

    .line 423
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private dumpAllEntries()V
    .locals 2

    .prologue
    .line 773
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 775
    return-void

    .line 773
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    goto :goto_0
.end method

.method private declared-synchronized endActiveElements()V
    .locals 3

    .prologue
    .line 493
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 498
    monitor-exit p0

    return-void

    .line 494
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/ElementTime;

    .line 496
    .local v0, "element":Lorg/w3c/dom/smil/ElementTime;
    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->endElement()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 493
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .end local v0    # "element":Lorg/w3c/dom/smil/ElementTime;
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized getOffsetTime(Lorg/w3c/dom/smil/ElementTime;)D
    .locals 7
    .param p1, "element"    # Lorg/w3c/dom/smil/ElementTime;

    .prologue
    .line 428
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 429
    .local v1, "entrySize":I
    iget v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .local v2, "i":I
    :goto_0
    iget v3, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v3, :cond_0

    if-lt v2, v1, :cond_1

    .line 435
    :cond_0
    const-wide/high16 v3, -0x4010000000000000L    # -1.0

    :goto_1
    monitor-exit p0

    return-wide v3

    .line 430
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 431
    .local v0, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-virtual {v0}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getElement()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 432
    invoke-virtual {v0}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getOffsetTime()D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v3

    const-wide v5, 0x408f400000000000L    # 1000.0

    mul-double/2addr v3, v5

    goto :goto_1

    .line 429
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 428
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .end local v1    # "entrySize":I
    .end local v2    # "i":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private getParTimeline(Lorg/w3c/dom/smil/ElementParallelTimeContainer;DD)Ljava/util/ArrayList;
    .locals 27
    .param p1, "par"    # Lorg/w3c/dom/smil/ElementParallelTimeContainer;
    .param p2, "offset"    # D
    .param p4, "maxOffset"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/smil/ElementParallelTimeContainer;",
            "DD)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v26, "timeline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementParallelTimeContainer;->getBegin()Lorg/w3c/dom/smil/TimeList;

    move-result-object v24

    .line 100
    .local v24, "myBeginList":Lorg/w3c/dom/smil/TimeList;
    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Lorg/w3c/dom/smil/TimeList;->item(I)Lorg/w3c/dom/smil/Time;

    move-result-object v19

    .line 101
    .local v19, "begin":Lorg/w3c/dom/smil/Time;
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/smil/Time;->getResolvedOffset()D

    move-result-wide v12

    add-double v4, v12, p2

    .line 102
    .local v4, "beginOffset":D
    cmpl-double v3, v4, p4

    if-lez v3, :cond_0

    .line 142
    :goto_0
    return-object v26

    .line 106
    :cond_0
    new-instance v2, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move-object/from16 v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 107
    .local v2, "myBegin":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementParallelTimeContainer;->getEnd()Lorg/w3c/dom/smil/TimeList;

    move-result-object v25

    .line 114
    .local v25, "myEndList":Lorg/w3c/dom/smil/TimeList;
    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Lorg/w3c/dom/smil/TimeList;->item(I)Lorg/w3c/dom/smil/Time;

    move-result-object v22

    .line 115
    .local v22, "end":Lorg/w3c/dom/smil/Time;
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/smil/Time;->getResolvedOffset()D

    move-result-wide v12

    add-double v8, v12, p2

    .line 116
    .local v8, "endOffset":D
    cmpl-double v3, v8, p4

    if-lez v3, :cond_1

    .line 117
    move-wide/from16 v8, p4

    .line 119
    :cond_1
    new-instance v6, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    const/4 v11, 0x1

    move-object/from16 v7, p0

    move-object/from16 v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 121
    .local v6, "myEnd":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    move-wide/from16 p4, v8

    .line 123
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementParallelTimeContainer;->getTimeChildren()Lorg/w3c/dom/NodeList;

    move-result-object v21

    .line 124
    .local v21, "children":Lorg/w3c/dom/NodeList;
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_1
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    move/from16 v0, v23

    if-lt v0, v3, :cond_2

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/DomParser/dom/smil/SmilPlayer;->sTimelineEntryComparator:Ljava/util/Comparator;

    move-object/from16 v0, v26

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 133
    sub-double v12, v8, p2

    double-to-float v3, v12

    const/high16 v7, 0x447a0000    # 1000.0f

    mul-float/2addr v3, v7

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lorg/w3c/dom/smil/ElementParallelTimeContainer;->getActiveChildrenAt(F)Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 134
    .local v18, "activeChildrenAtEnd":Lorg/w3c/dom/NodeList;
    const/16 v23, 0x0

    :goto_2
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    move/from16 v0, v23

    if-lt v0, v3, :cond_3

    .line 140
    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    .end local v18    # "activeChildrenAtEnd":Lorg/w3c/dom/NodeList;
    :cond_2
    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/smil/ElementTime;

    .local v11, "child":Lorg/w3c/dom/smil/ElementTime;
    move-object/from16 v10, p0

    move-wide/from16 v12, p2

    move-wide/from16 v14, p4

    .line 126
    invoke-direct/range {v10 .. v15}, Lcom/android/DomParser/dom/smil/SmilPlayer;->getTimeline(Lorg/w3c/dom/smil/ElementTime;DD)Ljava/util/ArrayList;

    move-result-object v20

    .line 127
    .local v20, "childTimeline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 124
    add-int/lit8 v23, v23, 0x1

    goto :goto_1

    .line 135
    .end local v11    # "child":Lorg/w3c/dom/smil/ElementTime;
    .end local v20    # "childTimeline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    .restart local v18    # "activeChildrenAtEnd":Lorg/w3c/dom/NodeList;
    :cond_3
    new-instance v12, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v16

    check-cast v16, Lorg/w3c/dom/smil/ElementTime;

    .line 136
    const/16 v17, 0x1

    move-object/from16 v13, p0

    move-wide v14, v8

    invoke-direct/range {v12 .. v17}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 135
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    add-int/lit8 v23, v23, 0x1

    goto :goto_2
.end method

.method private getSeqTimeline(Lorg/w3c/dom/smil/ElementSequentialTimeContainer;DD)Ljava/util/ArrayList;
    .locals 29
    .param p1, "seq"    # Lorg/w3c/dom/smil/ElementSequentialTimeContainer;
    .param p2, "offset"    # D
    .param p4, "maxOffset"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/smil/ElementSequentialTimeContainer;",
            "DD)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v28, "timeline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    move-wide/from16 v26, p2

    .line 151
    .local v26, "orgOffset":D
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementSequentialTimeContainer;->getBegin()Lorg/w3c/dom/smil/TimeList;

    move-result-object v24

    .line 156
    .local v24, "myBeginList":Lorg/w3c/dom/smil/TimeList;
    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Lorg/w3c/dom/smil/TimeList;->item(I)Lorg/w3c/dom/smil/Time;

    move-result-object v19

    .line 157
    .local v19, "begin":Lorg/w3c/dom/smil/Time;
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/smil/Time;->getResolvedOffset()D

    move-result-wide v12

    add-double v4, v12, p2

    .line 158
    .local v4, "beginOffset":D
    cmpl-double v3, v4, p4

    if-lez v3, :cond_0

    .line 200
    :goto_0
    return-object v28

    .line 162
    :cond_0
    new-instance v2, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move-object/from16 v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 163
    .local v2, "myBegin":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementSequentialTimeContainer;->getEnd()Lorg/w3c/dom/smil/TimeList;

    move-result-object v25

    .line 170
    .local v25, "myEndList":Lorg/w3c/dom/smil/TimeList;
    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Lorg/w3c/dom/smil/TimeList;->item(I)Lorg/w3c/dom/smil/Time;

    move-result-object v22

    .line 171
    .local v22, "end":Lorg/w3c/dom/smil/Time;
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/smil/Time;->getResolvedOffset()D

    move-result-wide v12

    add-double v8, v12, p2

    .line 172
    .local v8, "endOffset":D
    cmpl-double v3, v8, p4

    if-lez v3, :cond_1

    .line 173
    move-wide/from16 v8, p4

    .line 175
    :cond_1
    new-instance v6, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    const/4 v11, 0x1

    move-object/from16 v7, p0

    move-object/from16 v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 177
    .local v6, "myEnd":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    move-wide/from16 p4, v8

    .line 180
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementSequentialTimeContainer;->getTimeChildren()Lorg/w3c/dom/NodeList;

    move-result-object v21

    .line 181
    .local v21, "children":Lorg/w3c/dom/NodeList;
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_1
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    move/from16 v0, v23

    if-lt v0, v3, :cond_2

    .line 191
    sub-double v12, v8, v26

    double-to-float v3, v12

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lorg/w3c/dom/smil/ElementSequentialTimeContainer;->getActiveChildrenAt(F)Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 192
    .local v18, "activeChildrenAtEnd":Lorg/w3c/dom/NodeList;
    const/16 v23, 0x0

    :goto_2
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    move/from16 v0, v23

    if-lt v0, v3, :cond_3

    .line 198
    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 182
    .end local v18    # "activeChildrenAtEnd":Lorg/w3c/dom/NodeList;
    :cond_2
    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/smil/ElementTime;

    .local v11, "child":Lorg/w3c/dom/smil/ElementTime;
    move-object/from16 v10, p0

    move-wide/from16 v12, p2

    move-wide/from16 v14, p4

    .line 183
    invoke-direct/range {v10 .. v15}, Lcom/android/DomParser/dom/smil/SmilPlayer;->getTimeline(Lorg/w3c/dom/smil/ElementTime;DD)Ljava/util/ArrayList;

    move-result-object v20

    .line 184
    .local v20, "childTimeline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 187
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    invoke-virtual {v3}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getOffsetTime()D

    move-result-wide p2

    .line 181
    add-int/lit8 v23, v23, 0x1

    goto :goto_1

    .line 193
    .end local v11    # "child":Lorg/w3c/dom/smil/ElementTime;
    .end local v20    # "childTimeline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    .restart local v18    # "activeChildrenAtEnd":Lorg/w3c/dom/NodeList;
    :cond_3
    new-instance v12, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v16

    check-cast v16, Lorg/w3c/dom/smil/ElementTime;

    .line 194
    const/16 v17, 0x1

    move-object/from16 v13, p0

    move-wide v14, v8

    invoke-direct/range {v12 .. v17}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 193
    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    add-int/lit8 v23, v23, 0x1

    goto :goto_2
.end method

.method private getTimeline(Lorg/w3c/dom/smil/ElementTime;DD)Ljava/util/ArrayList;
    .locals 17
    .param p1, "element"    # Lorg/w3c/dom/smil/ElementTime;
    .param p2, "offset"    # D
    .param p4, "maxOffset"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/smil/ElementTime;",
            "DD)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/w3c/dom/smil/ElementParallelTimeContainer;

    if-eqz v2, :cond_0

    move-object/from16 v2, p1

    .line 206
    check-cast v2, Lorg/w3c/dom/smil/ElementParallelTimeContainer;

    move-object/from16 v1, p0

    move-wide/from16 v3, p2

    move-wide/from16 v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/DomParser/dom/smil/SmilPlayer;->getParTimeline(Lorg/w3c/dom/smil/ElementParallelTimeContainer;DD)Ljava/util/ArrayList;

    move-result-object v16

    .line 241
    :goto_0
    return-object v16

    .line 207
    :cond_0
    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/w3c/dom/smil/ElementSequentialTimeContainer;

    if-eqz v2, :cond_1

    move-object/from16 v2, p1

    .line 208
    check-cast v2, Lorg/w3c/dom/smil/ElementSequentialTimeContainer;

    move-object/from16 v1, p0

    move-wide/from16 v3, p2

    move-wide/from16 v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/DomParser/dom/smil/SmilPlayer;->getSeqTimeline(Lorg/w3c/dom/smil/ElementSequentialTimeContainer;DD)Ljava/util/ArrayList;

    move-result-object v16

    goto :goto_0

    .line 211
    :cond_1
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 213
    .local v16, "timeline":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementTime;->getBegin()Lorg/w3c/dom/smil/TimeList;

    move-result-object v12

    .line 214
    .local v12, "beginList":Lorg/w3c/dom/smil/TimeList;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    invoke-interface {v12}, Lorg/w3c/dom/smil/TimeList;->getLength()I

    move-result v2

    if-lt v15, v2, :cond_2

    .line 226
    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/smil/ElementTime;->getEnd()Lorg/w3c/dom/smil/TimeList;

    move-result-object v14

    .line 227
    .local v14, "endList":Lorg/w3c/dom/smil/TimeList;
    const/4 v15, 0x0

    :goto_2
    invoke-interface {v14}, Lorg/w3c/dom/smil/TimeList;->getLength()I

    move-result v2

    if-lt v15, v2, :cond_4

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/DomParser/dom/smil/SmilPlayer;->sTimelineEntryComparator:Ljava/util/Comparator;

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    .line 215
    .end local v14    # "endList":Lorg/w3c/dom/smil/TimeList;
    :cond_2
    invoke-interface {v12, v15}, Lorg/w3c/dom/smil/TimeList;->item(I)Lorg/w3c/dom/smil/Time;

    move-result-object v11

    .line 216
    .local v11, "begin":Lorg/w3c/dom/smil/Time;
    invoke-interface {v11}, Lorg/w3c/dom/smil/Time;->getResolved()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 217
    invoke-interface {v11}, Lorg/w3c/dom/smil/Time;->getResolvedOffset()D

    move-result-wide v5

    add-double v3, v5, p2

    .line 218
    .local v3, "beginOffset":D
    cmpg-double v2, v3, p4

    if-gtz v2, :cond_3

    .line 219
    new-instance v1, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 220
    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    .line 219
    invoke-direct/range {v1 .. v6}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 221
    .local v1, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    .end local v1    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .end local v3    # "beginOffset":D
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 228
    .end local v11    # "begin":Lorg/w3c/dom/smil/Time;
    .restart local v14    # "endList":Lorg/w3c/dom/smil/TimeList;
    :cond_4
    invoke-interface {v14, v15}, Lorg/w3c/dom/smil/TimeList;->item(I)Lorg/w3c/dom/smil/Time;

    move-result-object v13

    .line 229
    .local v13, "end":Lorg/w3c/dom/smil/Time;
    invoke-interface {v13}, Lorg/w3c/dom/smil/Time;->getResolved()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 230
    invoke-interface {v13}, Lorg/w3c/dom/smil/Time;->getResolvedOffset()D

    move-result-wide v5

    add-double v7, v5, p2

    .line 231
    .local v7, "endOffset":D
    cmpg-double v2, v7, p4

    if-gtz v2, :cond_5

    .line 232
    new-instance v1, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 233
    const/4 v10, 0x1

    move-object v5, v1

    move-object/from16 v6, p0

    move-object/from16 v9, p1

    .line 232
    invoke-direct/range {v5 .. v10}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;-><init>(Lcom/android/DomParser/dom/smil/SmilPlayer;DLorg/w3c/dom/smil/ElementTime;I)V

    .line 234
    .restart local v1    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    .end local v1    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .end local v7    # "endOffset":D
    :cond_5
    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method private declared-synchronized isBeginOfSlide(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)Z
    .locals 1
    .param p1, "entry"    # Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .prologue
    .line 394
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 395
    invoke-virtual {p1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getElement()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v0

    instance-of v0, v0, Lcom/android/DomParser/dom/smil/SmilParElementImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized isNextAction()Z
    .locals 2

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NEXT:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized isPauseAction()Z
    .locals 2

    .prologue
    .line 280
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->PAUSE:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized isPrevAction()Z
    .locals 2

    .prologue
    .line 300
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->PREV:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized isReloadAction()Z
    .locals 2

    .prologue
    .line 292
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->RELOAD:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized isStopAction()Z
    .locals 2

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->STOP:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private loadNextSlide()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .locals 8

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 580
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 581
    .local v3, "totalEntries":I
    iget v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 591
    iget v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .line 592
    const/4 v0, 0x0

    .line 593
    .local v0, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    iget v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    if-ge v4, v3, :cond_0

    .line 594
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    iget v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 595
    .restart local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-virtual {v0}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getOffsetTime()D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-long v4, v4

    iput-wide v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    :cond_0
    move-object v1, v0

    .line 597
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .local v1, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :goto_1
    return-object v1

    .line 582
    .end local v1    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :cond_1
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 583
    .restart local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isBeginOfSlide(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 584
    iput v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .line 585
    iput v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .line 586
    invoke-virtual {v0}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getOffsetTime()D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-long v4, v4

    iput-wide v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    move-object v1, v0

    .line 587
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .restart local v1    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    goto :goto_1

    .line 581
    .end local v1    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .restart local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private loadPrevSlide()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .locals 9

    .prologue
    .line 601
    const/4 v3, 0x1

    .line 602
    .local v3, "skippedSlides":I
    const/4 v2, -0x1

    .line 603
    .local v2, "latestBeginEntryIndex":I
    iget v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .local v1, "i":I
    move v4, v3

    .end local v3    # "skippedSlides":I
    .local v4, "skippedSlides":I
    :goto_0
    if-gez v1, :cond_0

    .line 615
    const/4 v5, -0x1

    if-eq v2, v5, :cond_3

    .line 616
    iput v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .line 617
    iput v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .line 618
    iget-object v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    iget v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    move v3, v4

    .end local v4    # "skippedSlides":I
    .restart local v3    # "skippedSlides":I
    move-object v0, v5

    .line 620
    :goto_1
    return-object v0

    .line 604
    .end local v3    # "skippedSlides":I
    .restart local v4    # "skippedSlides":I
    :cond_0
    iget-object v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 605
    .local v0, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isBeginOfSlide(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 606
    move v2, v1

    .line 607
    add-int/lit8 v3, v4, -0x1

    .end local v4    # "skippedSlides":I
    .restart local v3    # "skippedSlides":I
    if-nez v4, :cond_2

    .line 608
    iput v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .line 609
    iput v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .line 610
    invoke-virtual {v0}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getOffsetTime()D

    move-result-wide v5

    const-wide v7, 0x408f400000000000L    # 1000.0

    mul-double/2addr v5, v7

    double-to-long v5, v5

    iput-wide v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    goto :goto_1

    .end local v3    # "skippedSlides":I
    .restart local v4    # "skippedSlides":I
    :cond_1
    move v3, v4

    .line 603
    .end local v4    # "skippedSlides":I
    .restart local v3    # "skippedSlides":I
    :cond_2
    add-int/lit8 v1, v1, -0x1

    move v4, v3

    .end local v3    # "skippedSlides":I
    .restart local v4    # "skippedSlides":I
    goto :goto_0

    .line 620
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :cond_3
    const/4 v0, 0x0

    move v3, v4

    .end local v4    # "skippedSlides":I
    .restart local v3    # "skippedSlides":I
    goto :goto_1
.end method

.method private declared-synchronized pauseActiveElements()V
    .locals 3

    .prologue
    .line 501
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_0

    .line 505
    monitor-exit p0

    return-void

    .line 502
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/ElementTime;

    .line 503
    .local v0, "element":Lorg/w3c/dom/smil/ElementTime;
    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->pauseElement()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .end local v0    # "element":Lorg/w3c/dom/smil/ElementTime;
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized reloadActiveSlide()V
    .locals 6

    .prologue
    .line 399
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 400
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->beginSmilDocument()V

    .line 405
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 406
    .local v3, "totalEntries":I
    iget v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    iget v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    if-ne v4, v5, :cond_1

    .line 407
    iget v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    if-ge v4, v3, :cond_0

    .line 408
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    iget v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 409
    .local v0, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V

    .line 419
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :cond_0
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->seekActiveMedia()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    monitor-exit p0

    return-void

    .line 412
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 413
    .local v1, "entrySize":I
    iget v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .local v2, "i":I
    :goto_0
    iget v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    if-ge v2, v4, :cond_0

    if-ge v2, v1, :cond_0

    .line 414
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 415
    .restart local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 399
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .end local v1    # "entrySize":I
    .end local v2    # "i":I
    .end local v3    # "totalEntries":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private declared-synchronized reloadCurrentEntry()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .locals 3

    .prologue
    .line 549
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 550
    .local v0, "allEntryCount":I
    iget v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    if-gt v0, v1, :cond_0

    .line 551
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 553
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 549
    .end local v0    # "allEntryCount":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized resumeActiveElements()V
    .locals 4

    .prologue
    .line 508
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 509
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 513
    monitor-exit p0

    return-void

    .line 510
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/ElementTime;

    .line 511
    .local v0, "element":Lorg/w3c/dom/smil/ElementTime;
    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->resumeElement()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 509
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 508
    .end local v0    # "element":Lorg/w3c/dom/smil/ElementTime;
    .end local v1    # "i":I
    .end local v2    # "size":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized seekActiveMedia()V
    .locals 6

    .prologue
    .line 439
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-gez v1, :cond_1

    .line 452
    :cond_0
    monitor-exit p0

    return-void

    .line 440
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/ElementTime;

    .line 441
    .local v0, "element":Lorg/w3c/dom/smil/ElementTime;
    instance-of v4, v0, Lcom/android/DomParser/dom/smil/SmilParElementImpl;

    if-nez v4, :cond_0

    .line 444
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->getOffsetTime(Lorg/w3c/dom/smil/ElementTime;)D

    move-result-wide v2

    .line 445
    .local v2, "offset":D
    const-wide/16 v4, 0x0

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_2

    iget-wide v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    long-to-double v4, v4

    cmpg-double v4, v2, v4

    if-gtz v4, :cond_2

    .line 449
    iget-wide v4, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    long-to-double v4, v4

    sub-double/2addr v4, v2

    double-to-float v4, v4

    invoke-interface {v0, v4}, Lorg/w3c/dom/smil/ElementTime;->seekElement(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .end local v0    # "element":Lorg/w3c/dom/smil/ElementTime;
    .end local v1    # "i":I
    .end local v2    # "offset":D
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private stopCurrentSlide()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 558
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 559
    .local v3, "skippedEntries":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;>;"
    iget-object v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 560
    .local v4, "totalEntries":I
    iget v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_0

    .line 576
    :goto_1
    return-void

    .line 563
    :cond_0
    iget-object v5, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 564
    .local v1, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-virtual {v1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getAction()I

    move-result v0

    .line 565
    .local v0, "action":I
    invoke-virtual {v1}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getElement()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v5

    instance-of v5, v5, Lcom/android/DomParser/dom/smil/SmilParElementImpl;

    if-eqz v5, :cond_1

    .line 566
    if-ne v0, v6, :cond_1

    .line 567
    invoke-direct {p0, v1}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V

    .line 568
    iput v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    goto :goto_1

    .line 570
    :cond_1
    if-ne v0, v6, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 571
    invoke-direct {p0, v1}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V

    .line 560
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 572
    :cond_3
    if-nez v0, :cond_2

    .line 573
    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private declared-synchronized waitForEntry(J)V
    .locals 10
    .param p1, "interval"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0xc8

    .line 456
    monitor-enter p0

    const-wide/16 v0, 0x0

    .line 458
    .local v0, "overhead":J
    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v6, p1, v6

    if-gtz v6, :cond_1

    .line 479
    :cond_0
    monitor-exit p0

    return-void

    .line 459
    :cond_1
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 460
    .local v4, "startAt":J
    const-wide/16 v6, 0xc8

    invoke-static {p1, p2, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 461
    .local v2, "sleep":J
    cmp-long v6, v0, v2

    if-gez v6, :cond_2

    .line 462
    sub-long v6, v2, v0

    invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 463
    iget-wide v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    add-long/2addr v6, v2

    iput-wide v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    .line 469
    :goto_1
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStopAction()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isReloadAction()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPauseAction()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isNextAction()Z

    move-result v6

    if-nez v6, :cond_0

    .line 470
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPrevAction()Z

    move-result v6

    if-nez v6, :cond_0

    .line 474
    iget-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mRoot:Lorg/w3c/dom/smil/ElementTime;

    check-cast v6, Lorg/w3c/dom/events/EventTarget;

    iget-object v7, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mMediaTimeUpdatedEvent:Lorg/w3c/dom/events/Event;

    invoke-interface {v6, v7}, Lorg/w3c/dom/events/EventTarget;->dispatchEvent(Lorg/w3c/dom/events/Event;)Z

    .line 476
    sub-long/2addr p1, v8

    .line 477
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    sub-long v0, v6, v2

    goto :goto_0

    .line 465
    :cond_2
    const-wide/16 v2, 0x0

    .line 466
    iget-wide v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    add-long/2addr v6, v0

    iput-wide v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 456
    .end local v2    # "sleep":J
    .end local v4    # "startAt":J
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method private declared-synchronized waitForWakeUp()V
    .locals 2

    .prologue
    .line 517
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStartAction()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStopAction()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isReloadAction()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isNextAction()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPrevAction()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 520
    :cond_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStartAction()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NO_ACTIVE_ACTION:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 522
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PLAYING:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 518
    :cond_2
    const-wide/16 v0, 0xc8

    :try_start_1
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 524
    :catch_0
    move-exception v0

    goto :goto_1

    .line 517
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized getCurrentPosition()I
    .locals 2

    .prologue
    .line 489
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    long-to-int v0, v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDuration()I
    .locals 2

    .prologue
    .line 482
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    # getter for: Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->mOffsetTime:D
    invoke-static {v0}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->access$0(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 485
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 482
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized init(Lorg/w3c/dom/smil/ElementTime;)V
    .locals 6
    .param p1, "root"    # Lorg/w3c/dom/smil/ElementTime;

    .prologue
    .line 304
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mRoot:Lorg/w3c/dom/smil/ElementTime;

    .line 305
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mRoot:Lorg/w3c/dom/smil/ElementTime;

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x43e0000000000000L    # 9.223372036854776E18

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/DomParser/dom/smil/SmilPlayer;->getTimeline(Lorg/w3c/dom/smil/ElementTime;DD)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    .line 306
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mRoot:Lorg/w3c/dom/smil/ElementTime;

    check-cast v0, Lorg/w3c/dom/events/DocumentEvent;

    const-string/jumbo v1, "Event"

    invoke-interface {v0, v1}, Lorg/w3c/dom/events/DocumentEvent;->createEvent(Ljava/lang/String;)Lorg/w3c/dom/events/Event;

    move-result-object v0

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mMediaTimeUpdatedEvent:Lorg/w3c/dom/events/Event;

    .line 307
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mMediaTimeUpdatedEvent:Lorg/w3c/dom/events/Event;

    const-string/jumbo v1, "mediaTimeUpdated"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lorg/w3c/dom/events/Event;->initEvent(Ljava/lang/String;ZZ)V

    .line 308
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    monitor-exit p0

    return-void

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isPausedState()Z
    .locals 2

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PAUSED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isPlayedState()Z
    .locals 2

    .prologue
    .line 268
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PLAYED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isPlayingState()Z
    .locals 2

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PLAYING:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isStartAction()Z
    .locals 2

    .prologue
    .line 284
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->START:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isStoppedState()Z
    .locals 2

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    sget-object v1, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->STOPPED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized next()V
    .locals 1

    .prologue
    .line 380
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPausedState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    :cond_0
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NEXT:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 382
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    :cond_1
    monitor-exit p0

    return-void

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pause()V
    .locals 1

    .prologue
    .line 338
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->PAUSE:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 340
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 341
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PAUSED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    :cond_0
    monitor-exit p0

    return-void

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized play()V
    .locals 2

    .prologue
    .line 313
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    .line 315
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    .line 316
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .line 317
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mPlayerThread:Ljava/lang/Thread;

    .line 318
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PLAYING:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    .line 319
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mPlayerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    :cond_0
    monitor-exit p0

    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized prev()V
    .locals 1

    .prologue
    .line 387
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPausedState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    :cond_0
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->PREV:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 389
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :cond_1
    monitor-exit p0

    return-void

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reload()V
    .locals 1

    .prologue
    .line 371
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPausedState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372
    :cond_0
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->RELOAD:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 373
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 374
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayedState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionReload()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 10

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStoppedState()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 734
    :cond_0
    :goto_0
    return-void

    .line 661
    :cond_1
    iget-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 664
    .local v5, "size":I
    const/4 v6, 0x0

    :try_start_0
    iput v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    :goto_1
    iget v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v6, v5, :cond_2

    .line 733
    :goto_2
    sget-object v6, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PLAYED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    iput-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    goto :goto_0

    .line 665
    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAllEntries:Ljava/util/ArrayList;

    iget v7, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    .line 666
    .local v0, "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isBeginOfSlide(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 667
    iget v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    iput v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentSlide:I

    .line 669
    :cond_3
    invoke-virtual {v0}, Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;->getOffsetTime()D

    move-result-wide v6

    const-wide v8, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v8

    double-to-long v2, v6

    .line 670
    .local v2, "offset":J
    :cond_4
    iget-wide v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    cmp-long v6, v2, v6

    if-gtz v6, :cond_5

    .line 727
    iput-wide v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    .line 728
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V

    .line 664
    iget v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentElement:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 730
    .end local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .end local v2    # "offset":J
    :catch_0
    move-exception v6

    goto :goto_2

    .line 672
    .restart local v0    # "entry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .restart local v2    # "offset":J
    :cond_5
    :try_start_2
    iget-wide v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    sub-long v6, v2, v6

    invoke-direct {p0, v6, v7}, Lcom/android/DomParser/dom/smil/SmilPlayer;->waitForEntry(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 676
    :cond_6
    :goto_3
    :try_start_3
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPauseAction()Z

    move-result v6

    if-nez v6, :cond_7

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStopAction()Z

    move-result v6

    if-nez v6, :cond_7

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isReloadAction()Z

    move-result v6

    if-nez v6, :cond_7

    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isNextAction()Z

    move-result v6

    if-nez v6, :cond_7

    .line 677
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPrevAction()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 678
    :cond_7
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPauseAction()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 679
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionPause()V

    .line 680
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->waitForWakeUp()V

    .line 683
    :cond_8
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStopAction()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 684
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionStop()V

    goto :goto_0

    .line 688
    :cond_9
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isReloadAction()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 689
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionReload()V

    .line 690
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->reloadCurrentEntry()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    move-result-object v0

    .line 691
    if-eqz v0, :cond_0

    .line 693
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPausedState()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 694
    sget-object v6, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->PAUSE:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 698
    :cond_a
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isNextAction()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 699
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionNext()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    move-result-object v1

    .line 700
    .local v1, "nextEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    if-eqz v1, :cond_b

    .line 701
    move-object v0, v1

    .line 703
    :cond_b
    iget-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    sget-object v7, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PAUSED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    if-ne v6, v7, :cond_e

    .line 704
    sget-object v6, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->PAUSE:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 705
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V

    .line 709
    :goto_4
    iget-wide v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    .line 712
    .end local v1    # "nextEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :cond_c
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPrevAction()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 713
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionPrev()Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;

    move-result-object v4

    .line 714
    .local v4, "prevEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    if-eqz v4, :cond_d

    .line 715
    move-object v0, v4

    .line 717
    :cond_d
    iget-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    sget-object v7, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;->PAUSED:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    if-ne v6, v7, :cond_f

    .line 718
    sget-object v6, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->PAUSE:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 719
    invoke-direct {p0, v0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionEntry(Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;)V

    .line 723
    :goto_5
    iget-wide v2, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mCurrentTime:J

    goto/16 :goto_3

    .line 707
    .end local v4    # "prevEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .restart local v1    # "nextEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :cond_e
    sget-object v6, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NO_ACTIVE_ACTION:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    goto :goto_4

    .line 721
    .end local v1    # "nextEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    .restart local v4    # "prevEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :cond_f
    sget-object v6, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->NO_ACTIVE_ACTION:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v6, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_5

    .line 673
    .end local v4    # "prevEntry":Lcom/android/DomParser/dom/smil/SmilPlayer$TimelineEntry;
    :catch_1
    move-exception v6

    goto/16 :goto_3
.end method

.method public declared-synchronized seek(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 328
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 335
    monitor-exit p0

    return-void

    .line 329
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mActiveElements:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/ElementTime;

    .line 333
    .local v0, "element":Lorg/w3c/dom/smil/ElementTime;
    int-to-float v3, p1

    invoke-interface {v0, v3}, Lorg/w3c/dom/smil/ElementTime;->seekElement(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 328
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 327
    .end local v0    # "element":Lorg/w3c/dom/smil/ElementTime;
    .end local v1    # "i":I
    .end local v2    # "size":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public setPlayerState(Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;)V
    .locals 0
    .param p1, "state"    # Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mState:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerState;

    .line 259
    return-void
.end method

.method public declared-synchronized start()V
    .locals 1

    .prologue
    .line 347
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPausedState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->resumeActiveElements()V

    .line 349
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->START:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 350
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 351
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayedState()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isStoppedState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    :cond_2
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->play()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 347
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 1

    .prologue
    .line 358
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPausedState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 359
    :cond_0
    sget-object v0, Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;->STOP:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilPlayer;->mAction:Lcom/android/DomParser/dom/smil/SmilPlayer$SmilPlayerAction;

    .line 360
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 361
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->isPlayedState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->actionStop()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 358
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopWhenReload()V
    .locals 1

    .prologue
    .line 367
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/DomParser/dom/smil/SmilPlayer;->endActiveElements()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    monitor-exit p0

    return-void

    .line 367
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
