.class public abstract Lcom/android/DomParser/dom/smil/ElementTimeImpl;
.super Ljava/lang/Object;
.source "ElementTimeImpl.java"

# interfaces
.implements Lorg/w3c/dom/smil/ElementTime;


# static fields
.field private static final FILLDEFAULT_ATTRIBUTE_NAME:Ljava/lang/String; = "fillDefault"

.field private static final FILL_ATTRIBUTE_NAME:Ljava/lang/String; = "fill"

.field private static final FILL_AUTO_ATTRIBUTE:Ljava/lang/String; = "auto"

.field private static final FILL_FREEZE_ATTRIBUTE:Ljava/lang/String; = "freeze"

.field private static final FILL_HOLD_ATTRIBUTE:Ljava/lang/String; = "hold"

.field private static final FILL_REMOVE_ATTRIBUTE:Ljava/lang/String; = "remove"

.field private static final FILL_TRANSITION_ATTRIBUTE:Ljava/lang/String; = "transition"

.field private static final INDEFINITE:Ljava/lang/String; = "indefinite"

.field private static final RESTART:Ljava/lang/String; = "restart"

.field private static final TAG:Ljava/lang/String; = "Mms/ElementTimeImpl"


# instance fields
.field final mSmilElement:Lorg/w3c/dom/smil/SMILElement;


# direct methods
.method constructor <init>(Lorg/w3c/dom/smil/SMILElement;)V
    .locals 0
    .param p1, "element"    # Lorg/w3c/dom/smil/SMILElement;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    .line 56
    return-void
.end method


# virtual methods
.method public getBegin()Lorg/w3c/dom/smil/TimeList;
    .locals 6

    .prologue
    .line 82
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v4, "begin"

    invoke-interface {v3, v4}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "beginTimeStringList":[Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v0, "beginTimeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/w3c/dom/smil/Time;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-lt v2, v3, :cond_1

    .line 94
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    new-instance v3, Lcom/android/DomParser/dom/smil/TimeImpl;

    const-string/jumbo v4, "0"

    const/16 v5, 0xff

    invoke-direct {v3, v4, v5}, Lcom/android/DomParser/dom/smil/TimeImpl;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_0
    new-instance v3, Lcom/android/DomParser/dom/smil/TimeListImpl;

    invoke-direct {v3, v0}, Lcom/android/DomParser/dom/smil/TimeListImpl;-><init>(Ljava/util/ArrayList;)V

    return-object v3

    .line 89
    :cond_1
    :try_start_0
    new-instance v3, Lcom/android/DomParser/dom/smil/TimeImpl;

    aget-object v4, v1, v2

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getBeginConstraints()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/android/DomParser/dom/smil/TimeImpl;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 90
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method getBeginConstraints()I
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0xff

    return v0
.end method

.method public getDur()F
    .locals 4

    .prologue
    .line 120
    const/4 v0, 0x0

    .line 122
    .local v0, "dur":F
    :try_start_0
    iget-object v2, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v3, "dur"

    invoke-interface {v2, v3}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 123
    .local v1, "durString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 124
    invoke-static {v1}, Lcom/android/DomParser/dom/smil/TimeImpl;->parseClockValue(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float v0, v2, v3

    .line 130
    .end local v1    # "durString":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 126
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getEnd()Lorg/w3c/dom/smil/TimeList;
    .locals 12

    .prologue
    .line 134
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v2, "endTimeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/w3c/dom/smil/Time;>;"
    iget-object v6, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v7, "end"

    invoke-interface {v6, v7}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 137
    .local v3, "endTimeStringList":[Ljava/lang/String;
    array-length v5, v3

    .line 138
    .local v5, "len":I
    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    const/4 v6, 0x0

    aget-object v6, v3, v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    .line 140
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v5, :cond_3

    .line 150
    .end local v4    # "i":I
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 152
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getDur()F

    move-result v1

    .line 154
    .local v1, "duration":F
    const/4 v6, 0x0

    cmpg-float v6, v1, v6

    if-gez v6, :cond_4

    .line 155
    new-instance v6, Lcom/android/DomParser/dom/smil/TimeImpl;

    const-string/jumbo v7, "indefinite"

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getEndConstraints()I

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/android/DomParser/dom/smil/TimeImpl;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    .end local v1    # "duration":F
    :cond_2
    new-instance v6, Lcom/android/DomParser/dom/smil/TimeListImpl;

    invoke-direct {v6, v2}, Lcom/android/DomParser/dom/smil/TimeListImpl;-><init>(Ljava/util/ArrayList;)V

    return-object v6

    .line 142
    .restart local v4    # "i":I
    :cond_3
    :try_start_0
    new-instance v6, Lcom/android/DomParser/dom/smil/TimeImpl;

    aget-object v7, v3, v4

    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getEndConstraints()I

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/android/DomParser/dom/smil/TimeImpl;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 158
    .end local v4    # "i":I
    .restart local v1    # "duration":F
    :cond_4
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getBegin()Lorg/w3c/dom/smil/TimeList;

    move-result-object v0

    .line 159
    .local v0, "begin":Lorg/w3c/dom/smil/TimeList;
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    invoke-interface {v0}, Lorg/w3c/dom/smil/TimeList;->getLength()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 161
    new-instance v6, Lcom/android/DomParser/dom/smil/TimeImpl;

    .line 163
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-interface {v0, v4}, Lorg/w3c/dom/smil/TimeList;->item(I)Lorg/w3c/dom/smil/Time;

    move-result-object v8

    invoke-interface {v8}, Lorg/w3c/dom/smil/Time;->getResolvedOffset()D

    move-result-wide v8

    float-to-double v10, v1

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "s"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 164
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getEndConstraints()I

    move-result v8

    .line 161
    invoke-direct {v6, v7, v8}, Lcom/android/DomParser/dom/smil/TimeImpl;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 143
    .end local v0    # "begin":Lorg/w3c/dom/smil/TimeList;
    .end local v1    # "duration":F
    :catch_0
    move-exception v6

    goto :goto_1
.end method

.method getEndConstraints()I
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0xff

    return v0
.end method

.method public getFill()S
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 173
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v5, "fill"

    invoke-interface {v4, v5}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "fill":Ljava/lang/String;
    const-string/jumbo v4, "freeze"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v1, v2

    .line 212
    :cond_0
    :goto_0
    return v1

    .line 176
    :cond_1
    const-string/jumbo v4, "remove"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v3

    .line 177
    goto :goto_0

    .line 178
    :cond_2
    const-string/jumbo v4, "hold"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    .line 180
    goto :goto_0

    .line 181
    :cond_3
    const-string/jumbo v4, "transition"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v1, v2

    .line 183
    goto :goto_0

    .line 184
    :cond_4
    const-string/jumbo v4, "auto"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 190
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getFillDefault()S

    move-result v1

    .line 191
    .local v1, "fillDefault":S
    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 206
    .end local v1    # "fillDefault":S
    :cond_5
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v5, "dur"

    invoke-interface {v4, v5}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_6

    .line 207
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v5, "end"

    invoke-interface {v4, v5}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_6

    .line 208
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v5, "repeatCount"

    invoke-interface {v4, v5}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_6

    .line 209
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v5, "repeatDur"

    invoke-interface {v4, v5}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_6

    move v1, v2

    .line 210
    goto :goto_0

    :cond_6
    move v1, v3

    .line 212
    goto :goto_0
.end method

.method public getFillDefault()S
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 217
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v5, "fillDefault"

    invoke-interface {v4, v5}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "fillDefault":Ljava/lang/String;
    const-string/jumbo v4, "remove"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 219
    const/4 v2, 0x0

    .line 245
    :cond_0
    :goto_0
    return v2

    .line 220
    :cond_1
    const-string/jumbo v4, "freeze"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 222
    const-string/jumbo v4, "auto"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 223
    goto :goto_0

    .line 224
    :cond_2
    const-string/jumbo v4, "hold"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 227
    const-string/jumbo v4, "transition"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getParentElementTime()Lorg/w3c/dom/smil/ElementTime;

    move-result-object v1

    .line 238
    .local v1, "parent":Lorg/w3c/dom/smil/ElementTime;
    if-nez v1, :cond_3

    move v2, v3

    .line 243
    goto :goto_0

    .line 245
    :cond_3
    check-cast v1, Lcom/android/DomParser/dom/smil/ElementTimeImpl;

    .end local v1    # "parent":Lorg/w3c/dom/smil/ElementTime;
    invoke-virtual {v1}, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->getFillDefault()S

    move-result v2

    goto :goto_0
.end method

.method abstract getParentElementTime()Lorg/w3c/dom/smil/ElementTime;
.end method

.method public getRepeatCount()F
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 251
    iget-object v4, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v5, "repeatCount"

    invoke-interface {v4, v5}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "repeatCount":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 254
    .local v2, "value":F
    cmpl-float v4, v2, v3

    if-lez v4, :cond_0

    .line 260
    .end local v2    # "value":F
    :goto_0
    return v2

    .restart local v2    # "value":F
    :cond_0
    move v2, v3

    .line 257
    goto :goto_0

    .line 259
    .end local v2    # "value":F
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NumberFormatException;
    move v2, v3

    .line 260
    goto :goto_0
.end method

.method public getRepeatDur()F
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 266
    :try_start_0
    iget-object v3, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v4, "repeatDur"

    invoke-interface {v3, v4}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/DomParser/dom/smil/TimeImpl;->parseClockValue(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 267
    .local v1, "repeatDur":F
    cmpl-float v3, v1, v2

    if-lez v3, :cond_0

    .line 273
    .end local v1    # "repeatDur":F
    :goto_0
    return v1

    .restart local v1    # "repeatDur":F
    :cond_0
    move v1, v2

    .line 270
    goto :goto_0

    .line 272
    .end local v1    # "repeatDur":F
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    move v1, v2

    .line 273
    goto :goto_0
.end method

.method public getRestart()S
    .locals 3

    .prologue
    .line 278
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v2, "restart"

    invoke-interface {v1, v2}, Lorg/w3c/dom/smil/SMILElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "restart":Ljava/lang/String;
    const-string/jumbo v1, "never"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    const/4 v1, 0x1

    .line 284
    :goto_0
    return v1

    .line 281
    :cond_0
    const-string/jumbo v1, "whenNotActive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 282
    const/4 v1, 0x2

    goto :goto_0

    .line 284
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBegin(Lorg/w3c/dom/smil/TimeList;)V
    .locals 3
    .param p1, "begin"    # Lorg/w3c/dom/smil/TimeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "begin"

    const-string/jumbo v2, "indefinite"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    return-void
.end method

.method public setDur(F)V
    .locals 4
    .param p1, "dur"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 296
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "dur"

    new-instance v2, Ljava/lang/StringBuilder;

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v3, p1

    float-to-int v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    return-void
.end method

.method public setEnd(Lorg/w3c/dom/smil/TimeList;)V
    .locals 3
    .param p1, "end"    # Lorg/w3c/dom/smil/TimeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "end"

    const-string/jumbo v2, "indefinite"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    return-void
.end method

.method public setFill(S)V
    .locals 3
    .param p1, "fill"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 305
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "fill"

    const-string/jumbo v2, "freeze"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "fill"

    const-string/jumbo v2, "remove"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setFillDefault(S)V
    .locals 3
    .param p1, "fillDefault"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 313
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "fillDefault"

    const-string/jumbo v2, "freeze"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "fillDefault"

    const-string/jumbo v2, "remove"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setRepeatCount(F)V
    .locals 3
    .param p1, "repeatCount"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 321
    const-string/jumbo v0, "indefinite"

    .line 322
    .local v0, "repeatCountString":Ljava/lang/String;
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 323
    invoke-static {p1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    .line 325
    :cond_0
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v2, "repeatCount"

    invoke-interface {v1, v2, v0}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    return-void
.end method

.method public setRepeatDur(F)V
    .locals 3
    .param p1, "repeatDur"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 329
    const-string/jumbo v0, "indefinite"

    .line 330
    .local v0, "repeatDurString":Ljava/lang/String;
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 333
    :cond_0
    iget-object v1, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v2, "repeatDur"

    invoke-interface {v1, v2, v0}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    return-void
.end method

.method public setRestart(S)V
    .locals 3
    .param p1, "restart"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 337
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "restart"

    const-string/jumbo v2, "never"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :goto_0
    return-void

    .line 339
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "restart"

    const-string/jumbo v2, "whenNotActive"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/ElementTimeImpl;->mSmilElement:Lorg/w3c/dom/smil/SMILElement;

    const-string/jumbo v1, "restart"

    const-string/jumbo v2, "always"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
