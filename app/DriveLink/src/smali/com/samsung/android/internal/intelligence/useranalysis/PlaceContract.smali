.class public final Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract;
.super Ljava/lang/Object;
.source "PlaceContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Intent;,
        Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$Place;,
        Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$PlaceColumns;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final PLACE_TABLE_NAME:Ljava/lang/String; = "place"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string/jumbo v0, "content://com.samsung.android.internal.intelligence.useranalysis"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
