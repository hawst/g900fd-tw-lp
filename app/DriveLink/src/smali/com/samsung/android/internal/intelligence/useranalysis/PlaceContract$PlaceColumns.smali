.class public interface abstract Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract$PlaceColumns;
.super Ljava/lang/Object;
.source "PlaceContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/intelligence/useranalysis/PlaceContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PlaceColumns"
.end annotation


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final BLUETOOTH_MAC_ADDRESS:Ljava/lang/String; = "bluetooth_mac_address"

.field public static final BLUETOOTH_NAME:Ljava/lang/String; = "bluetooth_name"

.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final EXTRA_DATA:Ljava/lang/String; = "extra_data"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LOCATION_TYPE:Ljava/lang/String; = "location_type"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MONITORING_STATUS:Ljava/lang/String; = "monitoring_status"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PROVIDER:Ljava/lang/String; = "provider"

.field public static final RADIUS:Ljava/lang/String; = "radius"

.field public static final TIMESTAMP_UTC:Ljava/lang/String; = "timestamp_utc"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final WIFI_BSSID:Ljava/lang/String; = "wifi_bssid"

.field public static final WIFI_NAME:Ljava/lang/String; = "wifi_name"
