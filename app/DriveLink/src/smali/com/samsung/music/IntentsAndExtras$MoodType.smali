.class public final enum Lcom/samsung/music/IntentsAndExtras$MoodType;
.super Ljava/lang/Enum;
.source "IntentsAndExtras.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/music/IntentsAndExtras;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MoodType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/music/IntentsAndExtras$MoodType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CALM:Lcom/samsung/music/IntentsAndExtras$MoodType;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/music/IntentsAndExtras$MoodType;

.field public static final enum EXCITING:Lcom/samsung/music/IntentsAndExtras$MoodType;

.field public static final enum JOYFUL:Lcom/samsung/music/IntentsAndExtras$MoodType;

.field private static final MOOD_TYPE_BY_NAME_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/music/IntentsAndExtras$MoodType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PASSIONATE:Lcom/samsung/music/IntentsAndExtras$MoodType;

.field public static final enum UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 36
    new-instance v1, Lcom/samsung/music/IntentsAndExtras$MoodType;

    const-string/jumbo v2, "JOYFUL"

    const-string/jumbo v3, "joyful"

    invoke-direct {v1, v2, v5, v3}, Lcom/samsung/music/IntentsAndExtras$MoodType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->JOYFUL:Lcom/samsung/music/IntentsAndExtras$MoodType;

    new-instance v1, Lcom/samsung/music/IntentsAndExtras$MoodType;

    const-string/jumbo v2, "PASSIONATE"

    const-string/jumbo v3, "passionate"

    invoke-direct {v1, v2, v6, v3}, Lcom/samsung/music/IntentsAndExtras$MoodType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->PASSIONATE:Lcom/samsung/music/IntentsAndExtras$MoodType;

    new-instance v1, Lcom/samsung/music/IntentsAndExtras$MoodType;

    const-string/jumbo v2, "EXCITING"

    const-string/jumbo v3, "exciting"

    invoke-direct {v1, v2, v7, v3}, Lcom/samsung/music/IntentsAndExtras$MoodType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->EXCITING:Lcom/samsung/music/IntentsAndExtras$MoodType;

    new-instance v1, Lcom/samsung/music/IntentsAndExtras$MoodType;

    const-string/jumbo v2, "CALM"

    const-string/jumbo v3, "calm"

    invoke-direct {v1, v2, v8, v3}, Lcom/samsung/music/IntentsAndExtras$MoodType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->CALM:Lcom/samsung/music/IntentsAndExtras$MoodType;

    new-instance v1, Lcom/samsung/music/IntentsAndExtras$MoodType;

    const-string/jumbo v2, "UNDEFINED"

    const-string/jumbo v3, ""

    invoke-direct {v1, v2, v4, v3}, Lcom/samsung/music/IntentsAndExtras$MoodType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/samsung/music/IntentsAndExtras$MoodType;

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->JOYFUL:Lcom/samsung/music/IntentsAndExtras$MoodType;

    aput-object v2, v1, v5

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->PASSIONATE:Lcom/samsung/music/IntentsAndExtras$MoodType;

    aput-object v2, v1, v6

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->EXCITING:Lcom/samsung/music/IntentsAndExtras$MoodType;

    aput-object v2, v1, v7

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->CALM:Lcom/samsung/music/IntentsAndExtras$MoodType;

    aput-object v2, v1, v8

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    aput-object v2, v1, v4

    sput-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->ENUM$VALUES:[Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 41
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/music/IntentsAndExtras$MoodType;>;"
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->CALM:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->CALM:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->EXCITING:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->EXCITING:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->JOYFUL:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->JOYFUL:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->PASSIONATE:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/music/IntentsAndExtras$MoodType;->PASSIONATE:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sput-object v0, Lcom/samsung/music/IntentsAndExtras$MoodType;->MOOD_TYPE_BY_NAME_MAP:Ljava/util/Map;

    .line 46
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, Lcom/samsung/music/IntentsAndExtras$MoodType;->name:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public static getMoodTypeByName(Ljava/lang/String;)Lcom/samsung/music/IntentsAndExtras$MoodType;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->MOOD_TYPE_BY_NAME_MAP:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 59
    .local v0, "result":Lcom/samsung/music/IntentsAndExtras$MoodType;
    if-eqz v0, :cond_0

    .end local v0    # "result":Lcom/samsung/music/IntentsAndExtras$MoodType;
    :goto_0
    return-object v0

    .restart local v0    # "result":Lcom/samsung/music/IntentsAndExtras$MoodType;
    :cond_0
    sget-object v0, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/music/IntentsAndExtras$MoodType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/music/IntentsAndExtras$MoodType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/music/IntentsAndExtras$MoodType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/music/IntentsAndExtras$MoodType;->ENUM$VALUES:[Lcom/samsung/music/IntentsAndExtras$MoodType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/music/IntentsAndExtras$MoodType;->name:Ljava/lang/String;

    return-object v0
.end method
