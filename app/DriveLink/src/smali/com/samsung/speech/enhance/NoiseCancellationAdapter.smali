.class public Lcom/samsung/speech/enhance/NoiseCancellationAdapter;
.super Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;
.source "NoiseCancellationAdapter.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioFilterAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;
    }
.end annotation


# static fields
.field static final SAMPLE_SIZE:I = 0x140

.field private static isCurrentUTTSaturated:Z

.field private static isDMCNS:Z

.field private static isPreviousUTTSaturated:Z

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;

.field private static prevDelayTDOA:I

.field private static toastedOnce:Z


# instance fields
.field private SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

.field aMultiMic:Lcom/DMC/MicNS/MultiMic;

.field private epdListener:Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

.field frm:Ljava/text/SimpleDateFormat;

.field private globalAudioSourceId:I

.field private isDRCon:Z

.field private isFirstFrame:Z

.field stats:[I

.field temp:[S

.field private final threadPriority:I

.field private final useSeperateThreadFromMicAnim:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    sput-boolean v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->toastedOnce:Z

    .line 44
    sput-boolean v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    .line 45
    sput-boolean v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    .line 55
    const/16 v0, -0x64

    sput v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I

    .line 56
    sput-boolean v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z

    .line 61
    const-class v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 34
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;-><init>()V

    .line 37
    invoke-direct {p0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->initializeVoiceEngine()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 42
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    .line 46
    iput-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    .line 47
    iput-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    .line 48
    iput-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->useSeperateThreadFromMicAnim:Z

    .line 49
    const/4 v0, -0x8

    iput v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->threadPriority:I

    .line 53
    iput-object v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->frm:Ljava/text/SimpleDateFormat;

    .line 58
    iput-object v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    return v0
.end method

.method static synthetic access$1()Lcom/vlingo/core/internal/logging/Logger;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-object v0
.end method

.method static synthetic access$2()Z
    .locals 1

    .prologue
    .line 56
    sget-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z

    return v0
.end method

.method static synthetic access$3(I)V
    .locals 0

    .prologue
    .line 55
    sput p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I

    return-void
.end method

.method static synthetic access$4(Z)V
    .locals 0

    .prologue
    .line 56
    sput-boolean p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z

    return-void
.end method

.method static synthetic access$5()I
    .locals 1

    .prologue
    .line 55
    sget v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I

    return v0
.end method

.method private declared-synchronized initializeVoiceEngine()Lcom/samsung/voiceshell/VoiceEngine;
    .locals 5

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 237
    :goto_0
    monitor-exit p0

    return-object v2

    .line 228
    :catch_0
    move-exception v0

    .line 230
    .local v0, "error":Ljava/lang/Throwable;
    :try_start_1
    const-string/jumbo v1, "Unable to get VoiceEngineWrapper"

    .line 231
    .local v1, "msg":Ljava/lang/String;
    sget-object v2, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ". Will skip using noise cancellation filter."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 232
    sget-boolean v2, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->toastedOnce:Z

    if-nez v2, :cond_0

    .line 233
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 235
    :cond_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->toastedOnce:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    const/4 v2, 0x0

    goto :goto_0

    .line 227
    .end local v0    # "error":Ljava/lang/Throwable;
    .end local v1    # "msg":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method


# virtual methods
.method public checkSaturation([SI)Z
    .locals 6
    .param p1, "audioData"    # [S
    .param p2, "sizeInShorts"    # I

    .prologue
    .line 373
    const/4 v1, 0x0

    .line 374
    .local v1, "isSaturated":Z
    const/4 v2, 0x0

    .line 375
    .local v2, "saturationCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-lt v0, v3, :cond_2

    .line 378
    const/16 v3, 0x64

    if-le v2, v3, :cond_0

    .line 379
    const/4 v1, 0x1

    .line 381
    :cond_0
    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 383
    sget-object v3, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "DRC : saturationCount :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 385
    :cond_1
    return v1

    .line 376
    :cond_2
    aget-short v3, p1, v0

    const/16 v4, 0x2000

    if-gt v3, v4, :cond_3

    aget-short v3, p1, v0

    const/16 v4, -0x2000

    if-ge v3, v4, :cond_4

    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 375
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public filter([SII)I
    .locals 10
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 136
    iget-object v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    if-nez v5, :cond_1

    .line 210
    :cond_0
    :goto_0
    return p3

    .line 141
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->filter([SII)I

    .line 146
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 147
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 151
    :cond_2
    new-array v4, p3, [S

    .line 152
    .local v4, "tmpAudioData":[S
    div-int/lit8 v5, p3, 0x2

    new-array v0, v5, [S

    .line 159
    .local v0, "AudioData_DMC":[S
    invoke-static {p1, v8, v4, v8, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 161
    const/4 v3, 0x0

    .line 163
    .local v3, "retValue":I
    iget v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    const/4 v6, 0x6

    if-ne v5, v6, :cond_8

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v5, p1, p3}, Lcom/samsung/voiceshell/VoiceEngine;->processNSFrame([SI)I

    move-result v3

    .line 178
    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->epdListener:Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

    invoke-virtual {v5, v3, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->processValue(II)V

    .line 180
    if-eqz v3, :cond_4

    .line 181
    sget-object v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "processNSFrame returned "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 185
    :cond_4
    iget-boolean v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    if-eqz v5, :cond_5

    .line 187
    sget-boolean v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    if-eqz v5, :cond_a

    iput-boolean v8, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    .line 190
    :goto_2
    sget-object v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "DRC : Previous Utterance Saturation = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v7, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 192
    sput-boolean v8, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    .line 193
    iput-boolean v8, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    .line 196
    :cond_5
    invoke-virtual {p0, p1, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->checkSaturation([SI)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    .line 197
    sget-boolean v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    if-eqz v5, :cond_6

    .line 198
    iput-boolean v8, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    .line 199
    sput-boolean v9, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    .line 201
    sget-object v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v6, "DRC : Saturation happens"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 204
    :cond_6
    iget-boolean v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    if-eqz v5, :cond_7

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v5, p1, p3}, Lcom/samsung/voiceshell/VoiceEngine;->processDRC([SI)I

    move-result v3

    .line 206
    :cond_7
    if-eqz v3, :cond_0

    .line 207
    sget-object v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "processDRC returned "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 165
    :cond_8
    iget v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    const/16 v6, 0x12

    if-ne v5, v6, :cond_9

    .line 166
    iget-object v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    div-int/lit8 v6, p3, 0x2

    int-to-short v6, v6

    invoke-virtual {v5, v4, v0, v6}, Lcom/DMC/MicNS/MultiMic;->processNSFrame([S[SS)I

    move-result v2

    .line 167
    .local v2, "ret":I
    iget-object v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    div-int/lit8 v6, p3, 0x2

    invoke-virtual {v5, v0, v6}, Lcom/samsung/voiceshell/VoiceEngine;->processEPDFrame([SI)I

    move-result v3

    .line 169
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, p3, :cond_3

    .line 170
    div-int/lit8 v5, v1, 0x2

    aget-short v5, v0, v5

    aput-short v5, p1, v1

    .line 171
    add-int/lit8 v5, v1, 0x1

    aput-short v8, p1, v5

    .line 169
    add-int/lit8 v1, v1, 0x2

    goto :goto_3

    .line 176
    .end local v1    # "i":I
    .end local v2    # "ret":I
    :cond_9
    iget-object v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v5, v4, p3}, Lcom/samsung/voiceshell/VoiceEngine;->processEPDFrame([SI)I

    move-result v3

    goto/16 :goto_1

    .line 188
    :cond_a
    iput-boolean v9, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    goto/16 :goto_2
.end method

.method public filter([SIII)I
    .locals 3
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I
    .param p4, "audioSourceId"    # I

    .prologue
    .line 219
    sget-object v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "audioSourceId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 220
    iput p4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    .line 222
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->filter([SII)I

    move-result v0

    return v0
.end method

.method public init(III)V
    .locals 6
    .param p1, "sampleRate"    # I
    .param p2, "speechEndpointTimeout"    # I
    .param p3, "noSpeechEndPointTimeout"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 70
    invoke-static {}, Lcom/DMC/MicNS/MultiMicWrapper;->getInstance()Lcom/DMC/MicNS/MultiMic;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    .line 73
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    if-nez v1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    if-eqz v1, :cond_0

    .line 81
    const-string/jumbo v1, "DMC"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "libDMC_2CH_NS.so ver : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    invoke-virtual {v3}, Lcom/DMC/MicNS/MultiMic;->GetVersion()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v1, -0x8

    invoke-super {p0, p1, p2, p3, v1}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->init(IIII)V

    .line 88
    const-string/jumbo v1, "VSG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "VSG: speechEndpointTimeout = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    sget-boolean v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    if-eqz v1, :cond_2

    .line 92
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    invoke-virtual {v1}, Lcom/DMC/MicNS/MultiMic;->NSFree()I

    move-result v1

    sput v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I

    .line 93
    const-string/jumbo v1, "DMC"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "VSG : Return TDOA : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " aMultiMic NS Free in Init + isDMCNS  : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_2
    const/16 v1, 0x80

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->stats:[I

    .line 97
    const/16 v1, 0x140

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->temp:[S

    .line 99
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "MMddHHmmss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->frm:Ljava/text/SimpleDateFormat;

    .line 100
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    iget-object v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->frm:Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sget v3, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I

    invoke-virtual {v1, v2, v4, v3}, Lcom/DMC/MicNS/MultiMic;->NSInit(III)I

    .line 101
    sput-boolean v5, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z

    .line 103
    const-string/jumbo v1, "DMC"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "VSG : Input TDOA : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;-><init>(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;III)V

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->epdListener:Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

    .line 106
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1}, Lcom/samsung/voiceshell/VoiceEngine;->initializeNS()I

    move-result v0

    .line 108
    .local v0, "retValue":I
    if-eqz v0, :cond_3

    .line 109
    sget-object v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "initializeNS returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 114
    :cond_3
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1}, Lcom/samsung/voiceshell/VoiceEngine;->initializeDRC()I

    move-result v0

    .line 116
    if-eqz v0, :cond_4

    .line 117
    sget-object v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "initializeDRC returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 122
    :cond_4
    sput-boolean v4, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    .line 124
    iput-boolean v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    .line 126
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->init()V

    goto/16 :goto_0
.end method

.method public quit()Z
    .locals 1

    .prologue
    .line 370
    invoke-super {p0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->quit()Z

    move-result v0

    return v0
.end method

.method public sink([SII)I
    .locals 2
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 353
    :try_start_0
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->stats:[I

    invoke-virtual {v0, p1, v1}, Lcom/samsung/voiceshell/VoiceEngine;->getSpectrum([S[I)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    :goto_0
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->stats:[I

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->notifyListeners([I)V

    .line 358
    const/4 v0, 0x0

    return v0

    .line 354
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public sink([SIII)I
    .locals 1
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I
    .param p4, "audioSourceId"    # I

    .prologue
    .line 364
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->sink([SII)I

    .line 365
    const/4 v0, 0x0

    return v0
.end method
