.class Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;
.super Ljava/lang/Object;
.source "NoiseCancellationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/speech/enhance/NoiseCancellationAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EPDListener"
.end annotation


# instance fields
.field private DEFAULT_ENDPOINT_MEDIUM:I

.field private final HANGOVER_DEFAULT:I

.field private final HANGOVER_LONG:I

.field private final HANGOVER_SWITCH_LIMIT:I

.field private SpeechFrameCount:J

.field private final samolesSilenceNoSpeechLimit:J

.field private samolesSilenceSpeechLimit:J

.field private final sampleRate:I

.field private samplesSilence:J

.field private speechWasDetected:Z

.field final synthetic this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

.field private use_dynamicHANGOVER:I


# direct methods
.method public constructor <init>(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;III)V
    .locals 10
    .param p2, "sampleRate"    # I
    .param p3, "speechEndpointTimeout"    # I
    .param p4, "noSpeechEndPointTimeout"    # I

    .prologue
    .line 257
    iput-object p1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    const/16 v6, 0x3e80

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->sampleRate:I

    .line 248
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    .line 250
    const/16 v6, 0x2ee

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->HANGOVER_DEFAULT:I

    .line 251
    const/16 v6, 0x708

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->HANGOVER_LONG:I

    .line 252
    const v6, 0xbb80

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->HANGOVER_SWITCH_LIMIT:I

    .line 254
    const-string/jumbo v6, "endpoint.time.speech.long"

    const/16 v7, 0x6d6

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->DEFAULT_ENDPOINT_MEDIUM:I

    .line 258
    int-to-long v6, p2

    int-to-long v8, p3

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    .line 259
    int-to-long v6, p2

    int-to-long v8, p4

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceNoSpeechLimit:J

    .line 260
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG: DEFAULT_ENDPOINT_MEDIUM is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->DEFAULT_ENDPOINT_MEDIUM:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->DEFAULT_ENDPOINT_MEDIUM:I

    if-ne p3, v6, :cond_0

    .line 262
    const/4 v6, 0x1

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    .line 263
    const-string/jumbo v6, "VSG"

    const-string/jumbo v7, "VSG: Using dynamic HANGOVER!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :goto_0
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    .line 270
    const-string/jumbo v6, "endpoint.time.speech.short"

    const/16 v7, 0x190

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 271
    .local v5, "epdShort":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG: SHORT = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const-string/jumbo v6, "endpoint.time.speech.medium"

    const/16 v7, 0x2ee

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 273
    .local v3, "epdMedium":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG: MEDIUM = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string/jumbo v6, "endpoint.time.speech.medium.long"

    const/16 v7, 0x4e2

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 275
    .local v4, "epdMediumLong":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG: MEDIUM_LONG = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    const-string/jumbo v6, "endpoint.time.speech.long"

    const/16 v7, 0x6d6

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 277
    .local v0, "epdLong":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG: LONG = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const-string/jumbo v6, "endpoint.time.speech.long.msg"

    const/16 v7, 0x8ca

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 279
    .local v2, "epdLongLong":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG: LONG_LONG = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v8, 0x6d6

    invoke-virtual {v6, v7, v8}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 282
    const-string/jumbo v6, "endpoint.time.speech.long"

    const/16 v7, 0x6d6

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 283
    .local v1, "epdLong1":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG1: LONG = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    return-void

    .line 266
    .end local v0    # "epdLong":I
    .end local v1    # "epdLong1":I
    .end local v2    # "epdLongLong":I
    .end local v3    # "epdMedium":I
    .end local v4    # "epdMediumLong":I
    .end local v5    # "epdShort":I
    :cond_0
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "VSG: Not using dynamic HANGOVER ... speechEndpointTimeout is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    goto/16 :goto_0
.end method


# virtual methods
.method public processValue(II)V
    .locals 7
    .param p1, "speechDetected"    # I
    .param p2, "samplesAnalyzed"    # I

    .prologue
    const-wide/16 v5, 0x640

    const/4 v4, 0x1

    .line 288
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I
    invoke-static {v0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$0(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;)I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    .line 289
    div-int/lit8 p2, p2, 0x2

    .line 292
    :cond_0
    if-eqz p1, :cond_3

    .line 294
    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$1()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Speech was detected:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 295
    iput-boolean v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    .line 296
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    .line 297
    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    .line 298
    const-string/jumbo v0, "VSG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "VSG: SpeechFrameCount is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    iget v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    if-ne v0, v4, :cond_1

    .line 300
    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    const-wide/32 v2, 0xbb80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 301
    const-wide/16 v0, 0x7080

    iput-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    .line 302
    const-string/jumbo v0, "VSG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "VSG: JAVA_HANGOVER is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_1
    :goto_0
    return-void

    .line 305
    :cond_2
    const-wide/16 v0, 0x2ee0

    iput-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    .line 306
    const-string/jumbo v0, "VSG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "VSG: JAVA_HANGOVER is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    :cond_3
    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    .line 312
    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$1()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Silence was detected. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " samples are in the silence already."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 314
    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    cmp-long v0, v0, v5

    if-gez v0, :cond_4

    iput-wide v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    .line 315
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    if-eqz v0, :cond_5

    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    .line 316
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceNoSpeechLimit:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 318
    :cond_6
    const-string/jumbo v0, "VSG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "VSG: We are over the sample limit for silence: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "samolesSilenceSpeechLimit : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "samolesSilenceNoSpeechLimit : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceNoSpeechLimit:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/vlingo/core/internal/debug/AutomationLog;->event(I)V

    .line 321
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->stopRecognition()V

    .line 323
    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z
    invoke-static {}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$2()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    iget-object v0, v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    iget-object v0, v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->aMultiMic:Lcom/DMC/MicNS/MultiMic;

    invoke-virtual {v0}, Lcom/DMC/MicNS/MultiMic;->NSFree()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$3(I)V

    .line 325
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$4(Z)V

    .line 327
    const-string/jumbo v0, "DMC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "VSG : Return TDOA : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->prevDelayTDOA:I
    invoke-static {}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$5()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " aMultiMic NS Free in EPD + isDMCNS  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDMCNS:Z
    invoke-static {}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$2()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
