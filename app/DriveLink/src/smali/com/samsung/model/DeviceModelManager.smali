.class public Lcom/samsung/model/DeviceModelManager;
.super Ljava/lang/Object;
.source "DeviceModelManager.java"


# static fields
.field private static log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/samsung/model/DeviceModelManager;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/samsung/model/DeviceModelManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isHDevice()Z
    .locals 1

    .prologue
    .line 26
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/model/DeviceModelManager;->isHDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isHDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "build_model"    # Ljava/lang/String;

    .prologue
    .line 15
    if-eqz p0, :cond_1

    .line 16
    const-string/jumbo v0, "SM-N900"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "ASH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Madrid"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    const-string/jumbo v0, "JS01"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SC-01F"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SCL22"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SC-02F"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    const-string/jumbo v0, "SM-G910"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    :cond_0
    const/4 v0, 0x1

    .line 22
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJDevice()Z
    .locals 1

    .prologue
    .line 39
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/model/DeviceModelManager;->isJDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isJDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "build_model"    # Ljava/lang/String;

    .prologue
    .line 31
    if-eqz p0, :cond_1

    .line 32
    const-string/jumbo v0, "L720T"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "GT-I9505"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    :cond_0
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNote4Device()Z
    .locals 1

    .prologue
    .line 58
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/model/DeviceModelManager;->isNote4Device(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isNote4Device(Ljava/lang/String;)Z
    .locals 4
    .param p0, "build_model"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 45
    const-string/jumbo v2, "FAKE_DEVICE_MODEL"

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 46
    const-string/jumbo v2, "DEVICE_MODEL"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "fakedDeviceModel":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 48
    const-string/jumbo v2, "TPDBG_FAKE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    const/4 v1, 0x1

    .line 54
    .end local v0    # "fakedDeviceModel":Ljava/lang/String;
    :cond_0
    return v1
.end method
