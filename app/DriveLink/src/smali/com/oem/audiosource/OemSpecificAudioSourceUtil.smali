.class public Lcom/oem/audiosource/OemSpecificAudioSourceUtil;
.super Ljava/lang/Object;
.source "OemSpecificAudioSourceUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioSourceUtil;


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static audioSourceSet:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;

    .line 29
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 28
    sput-object v0, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;->TAG:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAudioSourceSet()I
    .locals 1

    .prologue
    .line 152
    sget v0, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;->audioSourceSet:I

    return v0
.end method


# virtual methods
.method public chooseAudioSource(Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)I
    .locals 9
    .param p1, "type"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .param p2, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 45
    const/4 v1, 0x6

    .line 47
    .local v1, "audioSource":I
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v6

    .line 48
    invoke-interface {v6}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 50
    .local v2, "context":Landroid/content/Context;
    const-string/jumbo v6, "audio"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 49
    check-cast v0, Landroid/media/AudioManager;

    .line 52
    .local v0, "audioMgr":Landroid/media/AudioManager;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v3

    .line 54
    .local v3, "isBluetoothAudioOn":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v5

    .line 55
    .local v5, "isOnlyBDeviceConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    .line 57
    .local v4, "isHeadsetConnected":Z
    sget-object v6, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "chooseAudioSource - type:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", isBtOn:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 58
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", isBOnly:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 59
    const-string/jumbo v8, ", isHeadset:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 57
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isKmodel()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 62
    sget-object v6, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    if-ne p1, v6, :cond_5

    .line 64
    if-eqz v3, :cond_0

    .line 65
    if-eqz v5, :cond_1

    .line 66
    :cond_0
    const/16 v1, 0x12

    .line 69
    :cond_1
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 70
    const/4 v1, 0x6

    .line 89
    :cond_2
    :goto_0
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getCarModeState()I

    move-result v6

    if-eqz v6, :cond_3

    .line 90
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getCarModeState()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    .line 91
    :cond_3
    const-string/jumbo v6, "DLPhraseSpotter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "App is not foreground - check state!"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/receiver/TaskManagerReceiver;->getCarModeState()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 91
    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const/16 v1, 0x13

    .line 96
    :cond_4
    sget-object v6, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "chooseAudioSource - final seleted type:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return v1

    .line 72
    :cond_5
    sget-object v6, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    if-ne p1, v6, :cond_2

    .line 73
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 75
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v6

    if-nez v6, :cond_6

    .line 76
    if-nez v4, :cond_6

    .line 77
    const/16 v1, 0x13

    .line 81
    :cond_6
    const-string/jumbo v6, "BG_WAKEUP_AP"

    .line 82
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getIntance()Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/automotive/drivelink/common/DLWakeupManager;->getWakeupState()Ljava/lang/String;

    move-result-object v7

    .line 81
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 82
    if-eqz v6, :cond_2

    .line 83
    const/16 v1, 0x13

    goto :goto_0
.end method

.method public chooseChannelConfig(I)I
    .locals 2
    .param p1, "audioSource"    # I

    .prologue
    .line 140
    const/4 v0, 0x2

    .line 142
    .local v0, "audioChannel":I
    invoke-static {}, Lcom/sec/android/automotive/drivelink/common/DLFeatures;->isKmodel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    const/16 v1, 0x12

    if-ne p1, v1, :cond_0

    .line 144
    const/16 v0, 0xc

    .line 148
    :cond_0
    return v0
.end method

.method public chooseMicSampleRate()I
    .locals 11

    .prologue
    .line 103
    const/16 v2, 0x3e80

    .line 104
    .local v2, "SAMPLE_RATE_16KHZ":I
    const/16 v3, 0x1f40

    .line 105
    .local v3, "SAMPLE_RATE_8KHZ":I
    const/16 v1, 0x3e80

    .line 106
    .local v1, "DEFAULT_SAMPLE_RATE":I
    const/16 v0, 0x1f40

    .line 108
    .local v0, "BLUETOOTH_SAMPLE_RATE":I
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v6

    .line 109
    .local v6, "isBluetoothAudioOn":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    .line 111
    .local v5, "isAssociatedService":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v7

    .line 113
    .local v7, "isOnlyBDeviceConnected":Z
    const/16 v4, 0x3e80

    .line 115
    .local v4, "SampleRate":I
    sget-object v8, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "chooseMicSampleRate - isBtOn:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 116
    const-string/jumbo v10, ", isAsso:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", isBOnly:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 117
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 115
    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    if-eqz v6, :cond_0

    if-nez v5, :cond_0

    .line 120
    const/16 v4, 0x1f40

    .line 123
    :cond_0
    sget-object v8, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "chooseMicSampleRate - final selected : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/automotive/drivelink/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    return v4
.end method

.method public notifyAudioSourceSet(I)V
    .locals 0
    .param p1, "audioSource"    # I

    .prologue
    .line 166
    sput p1, Lcom/oem/audiosource/OemSpecificAudioSourceUtil;->audioSourceSet:I

    .line 167
    return-void
.end method
