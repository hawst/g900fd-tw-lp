.class public Lcom/oem/util/OemWatchDeviceUtil;
.super Ljava/lang/Object;
.source "OemWatchDeviceUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/BDeviceUtil;


# instance fields
.field private log:Lcom/vlingo/core/internal/logging/Logger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-class v0, Lcom/oem/util/OemWatchDeviceUtil;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/oem/util/OemWatchDeviceUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 24
    return-void
.end method


# virtual methods
.method public getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 5
    .param p1, "headset"    # Landroid/bluetooth/BluetoothHeadset;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 29
    const/4 v0, 0x0

    .line 30
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 32
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/bluetooth/BluetoothHeadset;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v0

    .line 35
    iget-object v2, p0, Lcom/oem/util/OemWatchDeviceUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "BT deviceType = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :cond_0
    :goto_0
    return-object v0

    .line 36
    :catch_0
    move-exception v1

    .line 38
    .local v1, "e":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/oem/util/OemWatchDeviceUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unable to get BT deviceType: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method
