.class public Lcom/oem/msg/OemMSGUtil;
.super Ljava/lang/Object;
.source "OemMSGUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/MsgUtil;


# static fields
.field private static final CID_COLON:Ljava/lang/String; = "cid:"

.field private static final CID_COLON_CAP:Ljava/lang/String; = "Cid:"

.field private static final MMS_STATUS_PROJECTION:[Ljava/lang/String;

.field private static final NEW_INCOMING_MM_CONSTRAINT:Ljava/lang/String; = "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132))"


# instance fields
.field private log:Lcom/vlingo/core/facade/logging/ILogger;

.field private pduPersister:Lcom/google/android/mms/pdu/PduPersister;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 50
    const-string/jumbo v2, "thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "sub_cs"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 51
    const-string/jumbo v2, "pri"

    aput-object v2, v0, v1

    .line 49
    sput-object v0, Lcom/oem/msg/OemMSGUtil;->MMS_STATUS_PROJECTION:[Ljava/lang/String;

    .line 60
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-class v0, Lcom/oem/msg/OemMSGUtil;

    invoke-static {v0}, Lcom/vlingo/core/facade/CoreManager;->logger(Ljava/lang/Class;)Lcom/vlingo/core/facade/logging/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/oem/msg/OemMSGUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    .line 64
    return-void
.end method

.method private escapeXML(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 209
    const-string/jumbo v0, "&lt;"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "&gt;"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private findPart(Lcom/google/android/mms/pdu/PduBody;Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;
    .locals 6
    .param p1, "pb"    # Lcom/google/android/mms/pdu/PduBody;
    .param p2, "src"    # Ljava/lang/String;

    .prologue
    .line 164
    const/4 v1, 0x0

    .line 165
    .local v1, "index":I
    const/4 v3, 0x0

    .line 166
    .local v3, "part":Lcom/google/android/mms/pdu/PduPart;
    const/4 v2, 0x0

    .line 167
    .local v2, "newSrc":Ljava/lang/String;
    const/4 v0, 0x0

    .line 168
    .local v0, "extention":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 169
    invoke-direct {p0, p2}, Lcom/oem/msg/OemMSGUtil;->escapeXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 170
    const-string/jumbo v4, "cid:"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 171
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    .line 172
    const-string/jumbo v5, "cid:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    .line 173
    const-string/jumbo v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 171
    invoke-virtual {p1, v4}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentId(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 202
    :cond_0
    :goto_0
    if-eqz v3, :cond_4

    .line 203
    return-object v3

    .line 174
    :cond_1
    const-string/jumbo v4, "Cid:"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 175
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    .line 176
    const-string/jumbo v5, "Cid:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    .line 177
    const-string/jumbo v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 175
    invoke-virtual {p1, v4}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentId(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 178
    goto :goto_0

    .line 179
    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    .line 180
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 179
    invoke-virtual {p1, v4}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentId(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 181
    if-nez v3, :cond_3

    .line 182
    invoke-virtual {p1, p2}, Lcom/google/android/mms/pdu/PduBody;->getPartByName(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 184
    :cond_3
    if-nez v3, :cond_0

    .line 185
    invoke-virtual {p1, p2}, Lcom/google/android/mms/pdu/PduBody;->getPartByFileName(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 186
    if-nez v3, :cond_0

    .line 187
    invoke-virtual {p1, p2}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentLocation(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 190
    if-nez v3, :cond_0

    .line 191
    const/16 v4, 0x2e

    invoke-virtual {p2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 192
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 193
    const/4 v4, 0x0

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-virtual {p1, v2}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentLocation(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    goto/16 :goto_0

    .line 205
    :cond_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "No part found for the model."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private getBodyTextLineFromPduBody(Landroid/content/Context;Lcom/google/android/mms/pdu/PduBody;Z)Ljava/lang/String;
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pb"    # Lcom/google/android/mms/pdu/PduBody;
    .param p3, "addAttachmentSlide"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/mms/MmsException;
        }
    .end annotation

    .prologue
    .line 68
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 69
    .local v3, "TempText":Ljava/lang/StringBuffer;
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/android/DomParser/dom/model/SmilHelper;->getDocument(Lcom/google/android/mms/pdu/PduBody;Landroid/content/Context;)Lorg/w3c/dom/smil/SMILDocument;

    move-result-object v7

    .line 72
    .local v7, "document":Lorg/w3c/dom/smil/SMILDocument;
    invoke-interface {v7}, Lorg/w3c/dom/smil/SMILDocument;->getBody()Lorg/w3c/dom/smil/SMILElement;

    move-result-object v6

    .line 73
    .local v6, "docBody":Lorg/w3c/dom/smil/SMILElement;
    invoke-interface {v6}, Lorg/w3c/dom/smil/SMILElement;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v17

    .line 74
    .local v17, "slideNodes":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v17 .. v17}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v18

    .line 76
    .local v18, "slidesNum":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move/from16 v0, v18

    if-lt v9, v0, :cond_0

    .line 132
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    return-object v22

    .line 77
    :cond_0
    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v15

    check-cast v15, Lorg/w3c/dom/smil/SMILParElement;

    .line 80
    .local v15, "par":Lorg/w3c/dom/smil/SMILParElement;
    invoke-interface {v15}, Lorg/w3c/dom/smil/SMILParElement;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v12

    .line 81
    .local v12, "mediaNodes":Lorg/w3c/dom/NodeList;
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    .line 83
    .local v13, "mediaNum":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_1
    if-lt v10, v13, :cond_1

    .line 76
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 85
    :cond_1
    invoke-interface {v12, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/oem/msg/OemMSGUtil;->getSmilMediaElement(Lorg/w3c/dom/Node;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v19

    .line 88
    .local v19, "sme":Lorg/w3c/dom/smil/SMILMediaElement;
    if-nez v19, :cond_3

    .line 83
    :cond_2
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 91
    :cond_3
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/smil/SMILMediaElement;->getTagName()Ljava/lang/String;

    move-result-object v21

    .line 92
    .local v21, "tag":Ljava/lang/String;
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/smil/SMILMediaElement;->getSrc()Ljava/lang/String;

    move-result-object v20

    .line 93
    .local v20, "src":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/oem/msg/OemMSGUtil;->findPart(Lcom/google/android/mms/pdu/PduBody;Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v16

    .line 95
    .local v16, "part":Lcom/google/android/mms/pdu/PduPart;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/mms/pdu/PduPart;->getCharset()I

    move-result v4

    .line 96
    .local v4, "charset":I
    const-string/jumbo v22, "text"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 100
    const/16 v22, 0x3e8

    move/from16 v0, v22

    if-ne v4, v0, :cond_4

    .line 101
    const/4 v4, 0x4

    .line 104
    :cond_4
    const/4 v11, 0x0

    .line 106
    .local v11, "mText":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v22

    if-eqz v22, :cond_5

    .line 107
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v5

    .line 109
    .local v5, "data":[B
    if-nez v4, :cond_6

    .line 110
    :try_start_0
    new-instance v11, Ljava/lang/String;

    .end local v11    # "mText":Ljava/lang/String;
    invoke-direct {v11, v5}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    .end local v5    # "data":[B
    .restart local v11    # "mText":Ljava/lang/String;
    :cond_5
    :goto_3
    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0xd

    const/16 v24, 0xa

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v11

    .line 126
    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    const-string/jumbo v22, " "

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 114
    .restart local v5    # "data":[B
    :cond_6
    :try_start_1
    invoke-static {v4}, Lcom/google/android/mms/pdu/CharacterSets;->getMimeName(I)Ljava/lang/String;

    move-result-object v14

    .line 115
    .local v14, "name":Ljava/lang/String;
    new-instance v11, Ljava/lang/String;

    .end local v11    # "mText":Ljava/lang/String;
    invoke-direct {v11, v5, v14}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v11    # "mText":Ljava/lang/String;
    goto :goto_3

    .line 117
    .end local v11    # "mText":Ljava/lang/String;
    .end local v14    # "name":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 118
    .local v8, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v5}, Ljava/lang/String;-><init>([B)V

    .restart local v11    # "mText":Ljava/lang/String;
    goto :goto_3
.end method

.method private getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/oem/msg/OemMSGUtil;->pduPersister:Lcom/google/android/mms/pdu/PduPersister;

    if-nez v0, :cond_0

    .line 137
    invoke-static {p1}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v0

    iput-object v0, p0, Lcom/oem/msg/OemMSGUtil;->pduPersister:Lcom/google/android/mms/pdu/PduPersister;

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/oem/msg/OemMSGUtil;->pduPersister:Lcom/google/android/mms/pdu/PduPersister;

    return-object v0
.end method

.method private getSmilMediaElement(Lorg/w3c/dom/Node;)Lorg/w3c/dom/smil/SMILMediaElement;
    .locals 6
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 144
    move-object v2, p1

    .line 148
    .local v2, "mNode":Lorg/w3c/dom/Node;
    const/4 v4, 0x0

    .line 150
    .local v4, "mSME":Lorg/w3c/dom/smil/SMILMediaElement;
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 151
    .local v3, "mNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 153
    .local v1, "mChildCount":I
    instance-of v5, v2, Lorg/w3c/dom/smil/SMILMediaElement;

    if-eqz v5, :cond_1

    move-object v4, v2

    .line 154
    check-cast v4, Lorg/w3c/dom/smil/SMILMediaElement;

    .line 160
    :cond_0
    return-object v4

    .line 156
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 157
    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/oem/msg/OemMSGUtil;->getSmilMediaElement(Lorg/w3c/dom/Node;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v4

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isSalesCodeChinaTelecom()Z
    .locals 2

    .prologue
    .line 375
    const-string/jumbo v0, "CTC"

    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public countAttachmentMMS(Landroid/database/Cursor;)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 380
    const/4 v0, 0x0

    return v0
.end method

.method public getMmsQueryCursor(J)Landroid/database/Cursor;
    .locals 12
    .param p1, "queryTimeMS"    # J

    .prologue
    const/4 v11, 0x0

    .line 260
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v2

    .line 261
    invoke-interface {v2}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 262
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 270
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v4, ""

    .line 271
    .local v4, "where":Ljava/lang/String;
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_1

    .line 272
    const-wide/16 v2, 0x3e8

    div-long v7, p1, v2

    .line 273
    .local v7, "adjustedQueryTimeMS":J
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132)) AND date > "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 274
    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 273
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 279
    .end local v7    # "adjustedQueryTimeMS":J
    :goto_0
    const-string/jumbo v2, "Temp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "[MMS readout] where : "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const/4 v9, 0x0

    .line 284
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    sget-object v3, Lcom/oem/msg/OemMSGUtil;->MMS_STATUS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    .line 285
    const-string/jumbo v6, "date asc"

    .line 283
    invoke-static/range {v0 .. v6}, Lcom/google/android/mms/util/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    :cond_0
    move-object v2, v9

    .line 293
    :goto_1
    return-object v2

    .line 276
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string/jumbo v4, "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132))"

    goto :goto_0

    .line 286
    .restart local v9    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v10

    .line 287
    .local v10, "e":Ljava/lang/Exception;
    if-eqz v9, :cond_0

    .line 288
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v2, v11

    .line 289
    goto :goto_1
.end method

.method public getMmsTextByCursor(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 215
    iget-object v8, p0, Lcom/oem/msg/OemMSGUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v9, "[MMS readout] getMmsTextByCursor"

    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 216
    const-string/jumbo v1, ""

    .line 217
    .local v1, "bodyText":Ljava/lang/String;
    const/4 v0, 0x1

    .line 218
    .local v0, "COLUMN_MSG_ID":I
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v8

    .line 219
    invoke-interface {v8}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 221
    .local v2, "context":Landroid/content/Context;
    const/4 v8, 0x1

    :try_start_0
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 222
    .local v4, "msgId":J
    sget-object v8, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    .line 223
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 226
    .local v6, "msgUri":Landroid/net/Uri;
    iget-object v8, p0, Lcom/oem/msg/OemMSGUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "[MMS readout] getMmsTextByCursor & msgId="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 228
    iget-object v8, p0, Lcom/oem/msg/OemMSGUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "[MMS readout] msgUri"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 230
    iget-object v8, p0, Lcom/oem/msg/OemMSGUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v9, "[MMS readout] call getPduPersister"

    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 231
    invoke-direct {p0, v2}, Lcom/oem/msg/OemMSGUtil;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/google/android/mms/pdu/PduPersister;->load(Landroid/net/Uri;)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v7

    .line 232
    .local v7, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-eqz v7, :cond_0

    instance-of v8, v7, Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    if-eqz v8, :cond_0

    .line 234
    iget-object v8, p0, Lcom/oem/msg/OemMSGUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    const-string/jumbo v9, "[MMS readout] pdu != null && pdu instanceof MultimediaMessagePdu"

    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 235
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 236
    check-cast v7, Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    .end local v7    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    invoke-virtual {v7}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->getBody()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v9

    const/4 v10, 0x0

    .line 235
    invoke-direct {p0, v2, v9, v10}, Lcom/oem/msg/OemMSGUtil;->getBodyTextLineFromPduBody(Landroid/content/Context;Lcom/google/android/mms/pdu/PduBody;Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    iget-object v8, p0, Lcom/oem/msg/OemMSGUtil;->log:Lcom/vlingo/core/facade/logging/ILogger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "[MMS readout]TempcreateFromPduBody & ReturnText="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 239
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 238
    invoke-interface {v8, v9}, Lcom/vlingo/core/facade/logging/ILogger;->debug(Ljava/lang/String;)V

    .line 240
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 242
    :cond_0
    const-string/jumbo v8, "Temp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Return_Value : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    .end local v4    # "msgId":J
    .end local v6    # "msgUri":Landroid/net/Uri;
    :goto_0
    const-string/jumbo v8, "\n"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 251
    const-string/jumbo v1, ""

    .line 254
    :cond_1
    return-object v1

    .line 243
    :catch_0
    move-exception v3

    .line 245
    .local v3, "e":Lcom/google/android/mms/MmsException;
    invoke-virtual {v3}, Lcom/google/android/mms/MmsException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 9
    .param p1, "destinationAddress"    # Ljava/lang/String;
    .param p2, "scAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 340
    .local p3, "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, "sentIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, "deliveryIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 342
    .local v0, "smsManager":Landroid/telephony/SmsManager;
    const-string/jumbo v1, "gsm.sim.currentcardstatus"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 343
    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 345
    .local v7, "defaultSlot":Z
    const-string/jumbo v1, "gsm.sim.currentcardstatus2"

    .line 344
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 345
    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 346
    .local v8, "secondarySlot":Z
    const-string/jumbo v1, "gsm.sim.currentnetwork"

    .line 347
    const/4 v2, 0x0

    .line 346
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 349
    .local v6, "currentNetwork":I
    invoke-direct {p0}, Lcom/oem/msg/OemMSGUtil;->isSalesCodeChinaTelecom()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 350
    if-eqz v7, :cond_1

    if-eqz v8, :cond_1

    .line 351
    const/4 v1, 0x1

    if-ne v6, v1, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 352
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 372
    :goto_0
    return-void

    :cond_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 355
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 358
    :cond_1
    if-eqz v7, :cond_2

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 359
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 361
    :cond_2
    if-eqz v8, :cond_3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 362
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 365
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_4
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 369
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .locals 9
    .param p1, "destinationAddress"    # Ljava/lang/String;
    .param p2, "scAddress"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "sentIntent"    # Landroid/app/PendingIntent;
    .param p5, "deliveryIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 301
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 303
    .local v0, "smsManager":Landroid/telephony/SmsManager;
    const-string/jumbo v1, "gsm.sim.currentcardstatus"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 304
    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 306
    .local v7, "defaultSlot":Z
    const-string/jumbo v1, "gsm.sim.currentcardstatus2"

    .line 305
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 306
    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 307
    .local v8, "secondarySlot":Z
    const-string/jumbo v1, "gsm.sim.currentnetwork"

    .line 308
    const/4 v2, 0x0

    .line 307
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 310
    .local v6, "currentNetwork":I
    invoke-direct {p0}, Lcom/oem/msg/OemMSGUtil;->isSalesCodeChinaTelecom()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 311
    if-eqz v7, :cond_1

    if-eqz v8, :cond_1

    .line 312
    const/4 v1, 0x1

    if-ne v6, v1, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 313
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 333
    :goto_0
    return-void

    :cond_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 316
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 319
    :cond_1
    if-eqz v7, :cond_2

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 320
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 322
    :cond_2
    if-eqz v8, :cond_3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 323
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    :cond_3
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 326
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    :cond_4
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 330
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method
