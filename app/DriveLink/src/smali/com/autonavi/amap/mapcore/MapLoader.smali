.class public Lcom/autonavi/amap/mapcore/MapLoader;
.super Ljava/lang/Object;
.source "MapLoader.java"


# instance fields
.field createtime:J

.field datasource:I

.field inRequest:Z

.field mCanceled:Z

.field mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

.field mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

.field mapLevel:I

.field public mapTiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/autonavi/amap/mapcore/MapSourceGridData;",
            ">;"
        }
    .end annotation
.end field

.field nextImgDataLength:I

.field recievedDataBuffer:[B

.field recievedDataSize:I

.field recievedHeader:Z


# direct methods
.method public constructor <init>(Lcom/autonavi/amap/mapcore/BaseMapCallImplement;Lcom/autonavi/amap/mapcore/MapCore;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    .line 31
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mCanceled:Z

    .line 35
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 36
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 37
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    .line 38
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->inRequest:Z

    .line 39
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    .line 81
    iput-object p1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    .line 82
    iput p3, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    .line 83
    iput-object p2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->createtime:J

    .line 85
    return-void
.end method

.method private processRecivedData()V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/16 v4, 0x8

    const/4 v6, 0x0

    .line 287
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    if-nez v0, :cond_1

    .line 288
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    if-lt v0, v4, :cond_0

    .line 290
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, v6}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 291
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    if-lt v0, v1, :cond_0

    .line 297
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, v6}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v0

    .line 298
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v5}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v3

    .line 300
    if-nez v3, :cond_6

    .line 301
    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 302
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileDataBmp([BII)V

    .line 344
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    iget-object v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget v3, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    iget v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v2, v6, v3}, Lcom/autonavi/amap/mapcore/Convert;->moveArray([BI[BII)V

    .line 348
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 349
    iput v6, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 350
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    goto :goto_0

    .line 304
    :cond_3
    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    if-ne v1, v5, :cond_4

    .line 305
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileDataVTmc([BII)V

    goto :goto_1

    .line 307
    :cond_4
    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_5

    .line 308
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedModels([BII)V

    goto :goto_1

    .line 311
    :cond_5
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileData([BII)V

    goto :goto_1

    .line 316
    :cond_6
    const/4 v2, 0x0

    .line 318
    :try_start_0
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    const/16 v5, 0x8

    invoke-direct {v4, v1, v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 320
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v1, v4}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 322
    const/16 v2, 0x80

    new-array v2, v2, [B

    .line 324
    :goto_2
    invoke-virtual {v1, v2}, Ljava/util/zip/GZIPInputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-le v4, v5, :cond_7

    .line 325
    const/4 v5, 0x0

    invoke-virtual {v0, v2, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 331
    :catch_0
    move-exception v0

    .line 332
    :goto_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 335
    if-eqz v1, :cond_2

    .line 336
    :try_start_3
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 338
    :catch_1
    move-exception v0

    goto :goto_1

    .line 327
    :cond_7
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 329
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileData([BII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 335
    if-eqz v1, :cond_2

    .line 336
    :try_start_5
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 338
    :catch_2
    move-exception v0

    goto :goto_1

    .line 334
    :catchall_0
    move-exception v0

    move-object v1, v2

    .line 335
    :goto_4
    if-eqz v1, :cond_8

    .line 336
    :try_start_6
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 340
    :cond_8
    :goto_5
    throw v0

    .line 338
    :catch_3
    move-exception v1

    goto :goto_5

    .line 334
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 331
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_3
.end method


# virtual methods
.method public OnException(I)V
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->OnMapLoaderError(I)V

    .line 92
    :cond_0
    return-void
.end method

.method public addReuqestTiles(Lcom/autonavi/amap/mapcore/MapSourceGridData;)V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    return-void
.end method

.method public destory()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 43
    iput-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    .line 44
    iput-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    .line 45
    sget-object v0, Lcom/autonavi/amap/mapcore/VTMCDataCache;->vtmcHs:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 46
    sget-object v0, Lcom/autonavi/amap/mapcore/VTMCDataCache;->vtmcList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 47
    return-void
.end method

.method public doRequest()V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/16 v10, 0x3ea

    .line 96
    iput-boolean v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->inRequest:Z

    .line 98
    invoke-virtual {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->isRequestValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 105
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 109
    if-eqz v0, :cond_d

    .line 110
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v4, :cond_5

    .line 112
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 114
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    move-result v0

    move-object v4, v2

    move v2, v0

    move-object v0, v1

    .line 127
    :goto_1
    const/4 v5, 0x0

    .line 131
    :try_start_0
    new-instance v6, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v6}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 132
    const/16 v7, 0x4e20

    invoke-static {v6, v7}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 133
    const/16 v7, 0x4e20

    invoke-static {v6, v7}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 135
    new-instance v7, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v7, v6}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 136
    invoke-interface {v7}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string/jumbo v8, "User-Agent"

    sget-object v9, Lcom/amap/api/mapcore/l;->c:Ljava/lang/String;

    invoke-interface {v6, v8, v9}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 138
    if-eqz v0, :cond_2

    .line 139
    new-instance v0, Lorg/apache/http/HttpHost;

    invoke-direct {v0, v4, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 140
    invoke-interface {v7}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string/jumbo v4, "http.route.default-proxy"

    invoke-interface {v2, v4, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 144
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->getGridParma()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getMapSvrAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "/amapsrv/MPS?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 148
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 150
    if-eqz v7, :cond_9

    .line 151
    invoke-interface {v7, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 152
    const/16 v2, 0xc8

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    if-ne v2, v4, :cond_8

    .line 155
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 156
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 158
    :try_start_1
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOpened(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 159
    const/16 v2, 0x800

    new-array v2, v2, [B

    .line 162
    :goto_2
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v6, -0x1

    if-le v4, v6, :cond_3

    .line 163
    invoke-virtual {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->isRequestValid()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-boolean v6, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mCanceled:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v6, :cond_6

    .line 181
    :cond_3
    :goto_3
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 182
    if-eqz v1, :cond_4

    .line 184
    :try_start_2
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 190
    :cond_4
    :goto_4
    if-eqz v0, :cond_0

    .line 192
    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto/16 :goto_0

    .line 118
    :cond_5
    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v4

    .line 119
    invoke-static {}, Landroid/net/Proxy;->getDefaultPort()I

    move-result v2

    .line 120
    if-eqz v4, :cond_c

    .line 121
    new-instance v0, Ljava/net/Proxy;

    sget-object v5, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v6, Ljava/net/InetSocketAddress;

    invoke-direct {v6, v4, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v0, v5, v6}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    goto/16 :goto_1

    .line 166
    :cond_6
    add-int/2addr v3, v4

    .line 167
    const/4 v6, 0x0

    :try_start_4
    invoke-virtual {p0, p0, v6, v2, v4}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionRecieveData(Lcom/autonavi/amap/mapcore/MapLoader;I[BI)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 177
    :catch_1
    move-exception v2

    .line 179
    :goto_5
    const/16 v2, 0x3ea

    :try_start_5
    invoke-virtual {p0, v2}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 181
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 182
    if-eqz v1, :cond_7

    .line 184
    :try_start_6
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 190
    :cond_7
    :goto_6
    if-eqz v0, :cond_0

    .line 192
    :try_start_7
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 193
    :catch_2
    move-exception v0

    .line 194
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto/16 :goto_0

    .line 170
    :cond_8
    const/16 v0, 0x3ea

    :try_start_8
    invoke-virtual {p0, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    :goto_7
    move-object v0, v1

    goto :goto_3

    .line 174
    :cond_9
    const/16 v0, 0x3ea

    invoke-virtual {p0, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_7

    .line 177
    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_5

    .line 185
    :catch_4
    move-exception v1

    .line 186
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_4

    .line 185
    :catch_5
    move-exception v1

    .line 186
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_6

    .line 181
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_8
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 182
    if-eqz v1, :cond_a

    .line 184
    :try_start_9
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 190
    :cond_a
    :goto_9
    if-eqz v2, :cond_b

    .line 192
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 196
    :cond_b
    :goto_a
    throw v0

    .line 185
    :catch_6
    move-exception v1

    .line 186
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_9

    .line 193
    :catch_7
    move-exception v1

    .line 194
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_a

    .line 181
    :catchall_1
    move-exception v2

    move-object v11, v2

    move-object v2, v0

    move-object v0, v11

    goto :goto_8

    :cond_c
    move-object v0, v1

    goto/16 :goto_1

    :cond_d
    move-object v0, v1

    move v2, v3

    move-object v4, v1

    goto/16 :goto_1
.end method

.method public getGridParma()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 50
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 52
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/MapSourceGridData;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapSourceGridData;->getGridName()Ljava/lang/String;

    move-result-object v0

    .line 54
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 52
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 57
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 61
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    if-nez v0, :cond_2

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "t=VMMV3&cp=1&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v2

    .line 59
    goto :goto_1

    .line 63
    :cond_2
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "t=VMMBLDV3&cp=1&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 65
    :cond_3
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "t=BMPBM&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 67
    :cond_4
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "t=BMTI&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 69
    :cond_5
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "t=TMCV&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 71
    :cond_6
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "t=VMMV3&type=mod&cp=0&mid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move-object v0, v2

    .line 75
    goto/16 :goto_1
.end method

.method public isRequestValid()Z
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isGridsInScreen(Ljava/util/ArrayList;I)Z

    move-result v0

    return v0
.end method

.method public onConnectionError(Lcom/autonavi/amap/mapcore/MapLoader;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public onConnectionOpened(Lcom/autonavi/amap/mapcore/MapLoader;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 214
    const/high16 v0, 0x40000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    .line 215
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 216
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 217
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    .line 219
    return-void
.end method

.method public onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 227
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    .line 228
    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 229
    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 230
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    iget-object v1, v1, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->tileProcessCtrl:Lcom/autonavi/amap/mapcore/d;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 241
    :cond_0
    return-void

    :cond_1
    move v1, v0

    .line 234
    :goto_0
    iget-object v0, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 235
    iget-object v0, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/MapSourceGridData;

    .line 236
    if-eqz v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    iget-object v2, v0, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->tileProcessCtrl:Lcom/autonavi/amap/mapcore/d;

    iget-object v0, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/MapSourceGridData;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapSourceGridData;->getKeyGridName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/autonavi/amap/mapcore/d;->a(Ljava/lang/String;)V

    .line 234
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public onConnectionRecieveData(Lcom/autonavi/amap/mapcore/MapLoader;I[BI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 245
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    invoke-static {p3, v4, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    add-int/2addr v0, p4

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 255
    iget-boolean v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    if-nez v0, :cond_3

    .line 256
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, v4}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v0

    .line 264
    if-eqz v0, :cond_2

    .line 265
    iput-boolean v5, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mCanceled:Z

    goto :goto_0

    .line 268
    :cond_2
    const/4 v0, 0x4

    .line 269
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v0}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 270
    const/16 v0, 0x8

    .line 273
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget-object v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v3, p4, -0x8

    invoke-static {v1, v0, v2, v4, v3}, Lcom/autonavi/amap/mapcore/Convert;->moveArray([BI[BII)V

    .line 275
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 276
    iput v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 277
    iput-boolean v5, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    .line 278
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    .line 283
    :cond_3
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    goto :goto_0
.end method

.method processRecivedModels([BII)V
    .locals 3

    .prologue
    .line 411
    .line 412
    add-int/lit8 v0, p2, 0x1

    aget-byte v1, p1, p2

    .line 413
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v0, v1}, Ljava/lang/String;-><init>([BII)V

    .line 415
    add-int/2addr v0, v1

    .line 416
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    :goto_0
    return-void

    .line 420
    :cond_0
    sub-int v0, p3, p2

    new-array v0, v0, [B

    .line 421
    const/4 v1, 0x0

    sub-int v2, p3, p2

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 423
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method

.method processRecivedTileData([BII)V
    .locals 3

    .prologue
    .line 358
    .line 359
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, p2}, Lcom/autonavi/amap/mapcore/Convert;->getShort([BI)S

    .line 360
    add-int/lit8 v0, p2, 0x2

    .line 361
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v0}, Lcom/autonavi/amap/mapcore/Convert;->getShort([BI)S

    .line 362
    add-int/lit8 v0, v0, 0x2

    .line 363
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v0}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 364
    add-int/lit8 v0, v0, 0x4

    .line 365
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    .line 366
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v1, v0}, Ljava/lang/String;-><init>([BII)V

    .line 368
    add-int/2addr v0, v1

    .line 370
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method

.method processRecivedTileDataBmp([BII)V
    .locals 3

    .prologue
    .line 377
    .line 378
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, p2}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 379
    add-int/lit8 v0, p2, 0x4

    .line 380
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    .line 381
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v1, v0}, Ljava/lang/String;-><init>([BII)V

    .line 382
    add-int/2addr v0, v1

    .line 383
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method

.method processRecivedTileDataVTmc([BII)V
    .locals 5

    .prologue
    .line 390
    .line 391
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, p2}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 392
    add-int/lit8 v0, p2, 0x4

    .line 393
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    .line 394
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v1, v0}, Ljava/lang/String;-><init>([BII)V

    .line 395
    add-int/2addr v0, v1

    .line 396
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 399
    :cond_0
    invoke-static {}, Lcom/autonavi/amap/mapcore/VTMCDataCache;->getInstance()Lcom/autonavi/amap/mapcore/VTMCDataCache;

    move-result-object v0

    .line 401
    sub-int v1, p3, p2

    new-array v1, v1, [B

    .line 402
    const/4 v3, 0x0

    sub-int v4, p3, p2

    invoke-static {p1, p2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 404
    invoke-virtual {v0, v2, v1}, Lcom/autonavi/amap/mapcore/VTMCDataCache;->putData(Ljava/lang/String;[B)V

    .line 406
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method
