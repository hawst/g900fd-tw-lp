.class public Lcom/bzbyte/util/CacheManager;
.super Ljava/lang/Object;
.source "CacheManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bzbyte/util/CacheManager$CacheWorker;
    }
.end annotation


# static fields
.field private static ivInst:Lcom/bzbyte/util/CacheManager;


# instance fields
.field private ivCaches:Lcom/bzbyte/util/WeakRefArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/bzbyte/util/CacheManager;

    invoke-direct {v0}, Lcom/bzbyte/util/CacheManager;-><init>()V

    sput-object v0, Lcom/bzbyte/util/CacheManager;->ivInst:Lcom/bzbyte/util/CacheManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v1, Lcom/bzbyte/util/WeakRefArrayList;

    invoke-direct {v1}, Lcom/bzbyte/util/WeakRefArrayList;-><init>()V

    iput-object v1, p0, Lcom/bzbyte/util/CacheManager;->ivCaches:Lcom/bzbyte/util/WeakRefArrayList;

    .line 20
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/bzbyte/util/CacheManager$CacheWorker;

    invoke-direct {v1, p0}, Lcom/bzbyte/util/CacheManager$CacheWorker;-><init>(Lcom/bzbyte/util/CacheManager;)V

    const-string/jumbo v2, "BZByteCacheCleanup"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 21
    .local v0, "th":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 22
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/bzbyte/util/CacheManager;)Lcom/bzbyte/util/WeakRefArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/bzbyte/util/CacheManager;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/bzbyte/util/CacheManager;->ivCaches:Lcom/bzbyte/util/WeakRefArrayList;

    return-object v0
.end method

.method public static getManager()Lcom/bzbyte/util/CacheManager;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/bzbyte/util/CacheManager;->ivInst:Lcom/bzbyte/util/CacheManager;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized add(Lcom/bzbyte/util/Cache;)V
    .locals 1
    .param p1, "cache"    # Lcom/bzbyte/util/Cache;

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/bzbyte/util/CacheManager;->ivCaches:Lcom/bzbyte/util/WeakRefArrayList;

    invoke-virtual {v0, p1}, Lcom/bzbyte/util/WeakRefArrayList;->add(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit p0

    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
